using System;
using System.Windows.Forms;



namespace ExpertSystem
{
	using Grammar;
	using Aux;
	using Storing;
	using Deduction;



	public partial class MainForm : Form
	{
		private ListOfRules _rules = new ListOfRules();
		private ListOfFacts _facts = new ListOfFacts();



		public MainForm( Storage storage )
		{
			InitializeComponent();

			_storage = storage;
		}


		private void AddRule( string strNewRule, bool replace, int index )
		{
			bool ok = ( strNewRule != string.Empty );

			Rule newRule = null;
			if ( ok )
			{
				try
				{
					newRule = new Rule( strNewRule );
				}
				catch ( GrammarException ex )
				{
					ok = false;
					MessageBox.Show( ex.Message, "������.",
						MessageBoxButtons.OK, MessageBoxIcon.Error );
				}
			}

			if ( ok && _rules.Contains( newRule ) )
			{
				ok = false;
				MessageBox.Show( "����� ������� ��� ����.", "��������������.",
					MessageBoxButtons.OK, MessageBoxIcon.Information );
			}

			if ( ok )
			{
				if ( !replace || index < 0 )
				{
					_rules.Add( newRule );
					listRules.Items.Add( newRule.Text );
					index = listRules.Items.Count - 1;
				}
				else
				{
					_rules[ index ] = newRule;
					listRules.Items[ index ] = newRule.Text;
				}

				listRules.SelectedIndex = index;

				textInputRule.Text = string.Empty;
			}

			if ( !ok )
				textInputRule.SelectAll();

			textInputRule.Focus();
		}

		private void AddFact( string strNewFact, bool replace, int index )
		{
			bool ok = ( strNewFact != string.Empty );

			Fact newFact = null;
			if ( ok )
			{
				try
				{
					newFact = new Fact( strNewFact );
				}
				catch ( GrammarException ex )
				{
					ok = false;
					MessageBox.Show( ex.Message, "������.",
						MessageBoxButtons.OK, MessageBoxIcon.Error );
				}
			}

			if ( ok && _facts.Contains( newFact ) )
			{
				ok = false;
				MessageBox.Show( "����� ���� ��� ����.", "��������������.",
					MessageBoxButtons.OK, MessageBoxIcon.Information );
			}

			if ( ok )
			{
				if ( !replace || index < 0 )
				{
					_facts.Add( newFact );
					listFacts.Items.Add( newFact.Text );
					index = listFacts.Items.Count - 1;
				}
				else
				{
					_facts[ index ] = newFact;
					listFacts.Items[ index ] = newFact.Text;
				}

				listFacts.SelectedIndex = index;

				textInputFact.Text = string.Empty;
			}

			if ( !ok )
				textInputFact.SelectAll();

			textInputFact.Focus();
		}


		private void buttonAddRule_Click( object sender, EventArgs e )
		{
			AddRule( textInputRule.Text.Trim(), false, -1 );
		}

		private void buttonAddFact_Click( object sender, EventArgs e )
		{
			AddFact( textInputFact.Text.Trim(), false, -1 );
		}


		private void buttonRemoveRule_Click( object sender, EventArgs e )
		{
			int removeIndex = listRules.SelectedIndex;

			if ( removeIndex >= 0 )
			{
				listRules.Items.RemoveAt( removeIndex );
				listRules.SelectedIndex = Math.Min( removeIndex, listRules.Items.Count - 1 );

				_rules.RemoveAt( removeIndex );
			}
		}

		private void buttonRemoveFact_Click( object sender, EventArgs e )
		{
			int removeIndex = listFacts.SelectedIndex;

			if ( removeIndex >= 0 )
			{
				listFacts.Items.RemoveAt( removeIndex );
				listFacts.SelectedIndex = Math.Min( removeIndex, listFacts.Items.Count - 1 );

				_facts.RemoveAt( removeIndex );
			}
		}


		private void textInputRule_KeyDown( object sender, KeyEventArgs e )
		{
			switch ( e.KeyCode )
			{
			case Keys.Enter:
				AddRule( textInputRule.Text.Trim(), e.Control, listRules.SelectedIndex );
				break;

			case Keys.Escape:
				textInputRule.Text = string.Empty;
				break;

			case Keys.Up:
				if ( listRules.SelectedIndex > 0 )
					listRules.SelectedIndex--;
				else
					listRules.SelectedIndex = 0;
				break;

			case Keys.Down:
				if ( listRules.SelectedIndex < listRules.Items.Count - 1 )
					listRules.SelectedIndex++;
				else
					listRules.SelectedIndex = listRules.Items.Count - 1;
				break;
			}
		}

		private void textInputFact_KeyDown( object sender, KeyEventArgs e )
		{
			switch ( e.KeyCode )
			{
			case Keys.Enter:
				AddFact( textInputFact.Text.Trim(), e.Control, listFacts.SelectedIndex );
				break;

			case Keys.Escape:
				textInputFact.Text = string.Empty;
				break;

			case Keys.Up:
				if ( listFacts.SelectedIndex > 0 )
					listFacts.SelectedIndex--;
				else
					listFacts.SelectedIndex = 0;
				break;

			case Keys.Down:
				if ( listFacts.SelectedIndex < listFacts.Items.Count - 1 )
					listFacts.SelectedIndex++;
				else
					listFacts.SelectedIndex = listFacts.Items.Count - 1;
				break;
			}
		}


		private void textInputGoal_KeyDown( object sender, KeyEventArgs e )
		{
			switch ( e.KeyCode )
			{
			case Keys.Enter:
				buttonExecute_Click( buttonExecute, EventArgs.Empty );
				break;

			case Keys.Escape:
				textInputGoal.Text = string.Empty;
				break;
			}
		}


		private void listRules_KeyDown( object sender, KeyEventArgs e )
		{
			switch ( e.KeyCode )
			{
			case Keys.Delete:
				if ( e.Control )
				{
					_rules.Clear();
					listRules.Items.Clear();
				}
				else
				{
					buttonRemoveRule_Click( buttonRemoveRule, EventArgs.Empty );
				}
				break;
			}
		}

		private void listFacts_KeyDown( object sender, KeyEventArgs e )
		{
			switch ( e.KeyCode )
			{
			case Keys.Delete:
				if ( e.Control )
				{
					_facts.Clear();
					listFacts.Items.Clear();
				}
				else
				{
					buttonRemoveFact_Click( buttonRemoveFact, EventArgs.Empty );
				}
				break;
			}
		}


		private void buttonExecute_Click( object sender, EventArgs e )
		{
			bool ok = true;
			Condition goal = null;
			try
			{
				goal = new Condition( textInputGoal.Text );
				textInputGoal.Text = goal.Text;
			}
			catch ( GrammarException ex )
			{
				ok = false;
				MessageBox.Show( ex.Message, "������.",
					MessageBoxButtons.OK, MessageBoxIcon.Error );
			}

			if ( ok )
			{
				if ( _deductor != null && _deductor.InProgress )
					_deductor.Cancel();

				Deductor.FeedbackDelegates delegates = new Deductor.FeedbackDelegates();
				delegates.OnComplete = OnDeductionComplete;
				delegates.OnRulesStackChange = OnRuleStackChanged;
				delegates.OnTrace = OnTrace;
				delegates.OnUpdateLastRule = OnUpdateRule;
				delegates.OnBeginTrace = OnBeginTrace;
				_deductor = new ReverseDeductor( _rules, _facts, delegates );
				//_deductor = new DirectDeductor( _rules, _facts, delegates );
				_deductor.Start( goal );
			}
		}


		private void buttonContinue_Click( object sender, EventArgs e )
		{
			if ( _deductor != null && _deductor.InProgress )
				_pause.Set();
			else
				buttonExecute_Click( buttonExecute, EventArgs.Empty );
		}

		private void checkTrace_CheckedChanged( object sender, EventArgs e )
		{
			checkUpdateRulesStack.Enabled = !checkTrace.Checked;
			if ( !checkTrace.Checked )
				buttonContinue_Click( buttonContinue, EventArgs.Empty );
		}


		private void listRules_SelectedIndexChanged( object sender, EventArgs e )
		{
			if ( listRules.SelectedIndex >= 0 )
				textInputRule.Text = listRules.SelectedItem as string;
		}

		private void listFacts_SelectedIndexChanged( object sender, EventArgs e )
		{
			if ( listFacts.SelectedIndex >= 0 )
				textInputFact.Text = listFacts.SelectedItem as string;
		}


		private void treeView_AfterSelect( object sender, TreeViewEventArgs e )
		{
			ConditionDNode node = e.Node.Tag as ConditionDNode;

			if ( node != null )
			{
				listFacts.SelectedIndex = ( node.State == State.UsesFact ? node.Index : -1 );
				listRules.SelectedIndex = ( node.State == State.UsesRule ? node.Index : -1 );
			}
			else
			{
				listFacts.SelectedIndex = -1;
				listRules.SelectedIndex = -1;
			}
		}

		private void checkExpandTree_CheckedChanged( object sender, EventArgs e )
		{
			if ( checkExpandTree.Checked )
				treeView.ExpandAll();
			else
				treeView.CollapseAll();
		}



		private void MainForm_Load( object sender, EventArgs e )
		{
			LoadTokens( listRules.Items, _storage.Rules, AddRule );
			LoadTokens( listFacts.Items, _storage.Facts, AddFact );
		}

		private void MainForm_FormClosing( object sender, FormClosingEventArgs e )
		{
			if ( _deductor != null && _deductor.InProgress )
				_deductor.Cancel();

			SaveTokens( _storage.Rules, listRules.Items );
			SaveTokens( _storage.Facts, listFacts.Items );
		}
	}
}