IF DB_ID( N'ExpertSystem' ) IS NOT NULL
	DROP DATABASE ExpertSystem

GO

CREATE DATABASE ExpertSystem
ON PRIMARY
(
	NAME = ExpertSystem_data,
	FILENAME = 'D:\university\sem07\��\lab\ExpertSystem\sql\ExpertSystem_data.mdf',
	SIZE = 3mb, MAXSIZE = 10mb, FILEGROWTH = 10%
)
LOG ON
(
	NAME = ExpertSystem_log,
	FILENAME = 'D:\university\sem07\��\lab\ExpertSystem\sql\ExpertSystem_log.ldf',
	SIZE = 1mb, MAXSIZE = 5mb, FILEGROWTH = 10%
)

GO