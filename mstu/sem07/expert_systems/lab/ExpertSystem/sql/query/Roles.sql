USE ExpertSystem


EXEC sp_addlogin @loginame = 'admin', @passwd = 'all'
EXEC sp_grantdbaccess @loginame = 'admin'
EXEC sp_addrole @rolename = 'Administrators'
EXEC sp_addrolemember @rolename = 'Administrators', @membername = 'admin'
GO

REVOKE ALL TO Administrators

GRANT SELECT ON Facts TO Administrators
GRANT DELETE ON Facts TO Administrators
GRANT UPDATE ON Facts TO Administrators
GRANT INSERT ON Facts TO Administrators

GRANT SELECT ON Rules TO Administrators
GRANT DELETE ON Rules TO Administrators
GRANT UPDATE ON Rules TO Administrators
GRANT INSERT ON Rules TO Administrators
GO


EXEC sp_addlogin @loginame = 'user', @passwd = 'select'
EXEC sp_grantdbaccess @loginame = 'user'
EXEC sp_addrole @rolename = 'Users'
EXEC sp_addrolemember @rolename = 'Users', @membername = 'user'
GO

REVOKE ALL TO Users

GRANT SELECT ON Facts TO Users

GRANT SELECT ON Rules TO Users
GO