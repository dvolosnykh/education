USE ExpertSystem

IF EXISTS ( SELECT name FROM sysobjects WHERE name = N'Rules' AND type = 'U' )
	DROP TABLE Rules
GO

CREATE TABLE Rules
(
	ID	int IDENTITY( 1, 1 ) PRIMARY KEY CLUSTERED,
	Text	varchar( 1024 )	null
)

GO