USE ExpertSystem

IF EXISTS ( SELECT name FROM sysobjects WHERE name = N'Facts' AND type = 'U' )
	DROP TABLE Facts
GO

CREATE TABLE Facts
(
	ID	int IDENTITY( 1, 1 ) PRIMARY KEY CLUSTERED,
	Text	varchar( 1024 )	null
)

GO