using System;
using System.IO;



namespace ExpertSystem.Storing
{
	using Grammar;


	public class FileTable : Table
	{
		public const string RulesFileName = "Rules.txt";
		public const string FactsFileName = "Facts.txt";

		protected string _filename;



		public FileTable( string filename )
		{
			_filename = filename;
		}


		public override string[] Load()
		{
			StreamReader reader = new StreamReader( _filename );
			string contents = reader.ReadToEnd();
			reader.Close();

			char[] separators = { '\r', '\n' };
			return contents.Split( separators, StringSplitOptions.RemoveEmptyEntries );
		}

		public override void Save( string[] lines )
		{
			StreamWriter writer = new StreamWriter( _filename );
			
			foreach ( string line in lines )
				writer.WriteLine( line );

			writer.Close();
		}
	}


	public class FileStorage : Storage
	{
		public FileStorage()
		{
			Rules = new FileTable( FileTable.RulesFileName );
			Facts = new FileTable( FileTable.FactsFileName );
		}
	}
}
