using System;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;



namespace ExpertSystem
{
	using Storing;



	public partial class AuthForm : Form
	{
		private DBStorage _db;


		public AuthForm( DBStorage db )
		{
			_db = db;

			InitializeComponent();
		}


		private void ConnectToDB()
		{
			string login = textLogin.Text;
			string password = textPassword.Text;

			for ( DialogResult choice = DialogResult.Retry;
				!_db.Connected && choice == DialogResult.Retry; )
			{
				try
				{
					_db.Connect( login, password );
				}
				catch ( SqlException ex )
				{
					choice = MessageBox.Show( ex.Message, "������ ��� ����������� � ��",
						MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation );
				}
			}

			if ( _db.Connected )
				base.Close();
		}

		private void AuthForm_FormClosing( object sender, FormClosingEventArgs e )
		{
			if ( _db.Connected )
			{
				DialogResult = DialogResult.Yes;
			}
			else
			{
				StringBuilder text = new StringBuilder();
				text.AppendLine( "���������� � �� �� �����������." );
				text.AppendLine();
				text.AppendLine( DialogResult.Yes + " - ��������� �������, �� �������� � �������;" );
				text.AppendLine( DialogResult.No + " - ��������� ������;" );
				text.AppendLine( DialogResult.Cancel + " - �������� � ���� �������." );

				DialogResult = MessageBox.Show( text.ToString(), "�������",
					MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning );

				e.Cancel = ( DialogResult == DialogResult.Cancel );
			}
		}

		private void textLogin_KeyDown( object sender, KeyEventArgs e )
		{
			switch ( e.KeyCode )
			{
			case Keys.Escape:	textLogin.Clear();	break;
			case Keys.Enter:	ConnectToDB();	    break;
			}
		}

		private void textPassword_KeyDown( object sender, KeyEventArgs e )
		{
			switch ( e.KeyCode )
			{
			case Keys.Escape:	textPassword.Clear();	break;
			case Keys.Enter:	ConnectToDB();	        break;
			}
		}
	}
}