using System;



namespace ExpertSystem.Storing
{
	public abstract class Table
	{
		public abstract string[] Load();
		public abstract void Save( string[] items );
	}


	public abstract class Storage
	{
		public Table Rules;
		public Table Facts;
	}
}
