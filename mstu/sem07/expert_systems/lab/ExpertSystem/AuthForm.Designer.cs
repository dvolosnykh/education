namespace ExpertSystem
{
	partial class AuthForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textLogin = new System.Windows.Forms.TextBox();
			this.textPassword = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point( 14, 15 );
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size( 35, 13 );
			this.label1.TabIndex = 1;
			this.label1.Text = "��� :";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point( 14, 41 );
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size( 51, 13 );
			this.label2.TabIndex = 2;
			this.label2.Text = "������ :";
			// 
			// textLogin
			// 
			this.textLogin.Location = new System.Drawing.Point( 71, 12 );
			this.textLogin.Name = "textLogin";
			this.textLogin.Size = new System.Drawing.Size( 100, 20 );
			this.textLogin.TabIndex = 0;
			this.textLogin.KeyDown += new System.Windows.Forms.KeyEventHandler( this.textLogin_KeyDown );
			// 
			// textPassword
			// 
			this.textPassword.Location = new System.Drawing.Point( 71, 38 );
			this.textPassword.Name = "textPassword";
			this.textPassword.PasswordChar = '*';
			this.textPassword.Size = new System.Drawing.Size( 100, 20 );
			this.textPassword.TabIndex = 1;
			this.textPassword.KeyDown += new System.Windows.Forms.KeyEventHandler( this.textPassword_KeyDown );
			// 
			// AuthForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ClientSize = new System.Drawing.Size( 186, 73 );
			this.Controls.Add( this.textPassword );
			this.Controls.Add( this.textLogin );
			this.Controls.Add( this.label2 );
			this.Controls.Add( this.label1 );
			this.MaximizeBox = false;
			this.Name = "AuthForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "���� � ��";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.AuthForm_FormClosing );
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textLogin;
		private System.Windows.Forms.TextBox textPassword;
	}
}