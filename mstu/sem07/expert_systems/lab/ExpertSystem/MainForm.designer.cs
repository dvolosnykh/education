namespace ExpertSystem
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.listRules = new System.Windows.Forms.ListBox();
			this.listFacts = new System.Windows.Forms.ListBox();
			this.groupInput = new System.Windows.Forms.GroupBox();
			this.labelGoal = new System.Windows.Forms.Label();
			this.labelFacts = new System.Windows.Forms.Label();
			this.labelRules = new System.Windows.Forms.Label();
			this.buttonExecute = new System.Windows.Forms.Button();
			this.textInputGoal = new System.Windows.Forms.TextBox();
			this.textInputFact = new System.Windows.Forms.TextBox();
			this.buttonAddFact = new System.Windows.Forms.Button();
			this.buttonRemoveFact = new System.Windows.Forms.Button();
			this.buttonRemoveRule = new System.Windows.Forms.Button();
			this.buttonAddRule = new System.Windows.Forms.Button();
			this.textInputRule = new System.Windows.Forms.TextBox();
			this.checkUpdateRulesStack = new System.Windows.Forms.CheckBox();
			this.listRulesStack = new System.Windows.Forms.ListBox();
			this.tabMechanisms = new System.Windows.Forms.TabControl();
			this.tabReversedLogicalDeduction = new System.Windows.Forms.TabPage();
			this.checkTrace = new System.Windows.Forms.CheckBox();
			this.buttonContinue = new System.Windows.Forms.Button();
			this.checkExpandTree = new System.Windows.Forms.CheckBox();
			this.labelRulesStack = new System.Windows.Forms.Label();
			this.labelDeductionTree = new System.Windows.Forms.Label();
			this.treeView = new System.Windows.Forms.TreeView();
			this.tabDirectProductionDeduction = new System.Windows.Forms.TabPage();
			this.groupInput.SuspendLayout();
			this.tabMechanisms.SuspendLayout();
			this.tabReversedLogicalDeduction.SuspendLayout();
			this.SuspendLayout();
			// 
			// listRules
			// 
			this.listRules.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.listRules.FormattingEnabled = true;
			this.listRules.HorizontalScrollbar = true;
			this.listRules.ItemHeight = 14;
			this.listRules.Location = new System.Drawing.Point( 7, 35 );
			this.listRules.Name = "listRules";
			this.listRules.Size = new System.Drawing.Size( 727, 158 );
			this.listRules.TabIndex = 1;
			this.listRules.SelectedIndexChanged += new System.EventHandler( this.listRules_SelectedIndexChanged );
			this.listRules.KeyDown += new System.Windows.Forms.KeyEventHandler( this.listRules_KeyDown );
			// 
			// listFacts
			// 
			this.listFacts.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.listFacts.FormattingEnabled = true;
			this.listFacts.HorizontalScrollbar = true;
			this.listFacts.ItemHeight = 14;
			this.listFacts.Location = new System.Drawing.Point( 740, 35 );
			this.listFacts.Name = "listFacts";
			this.listFacts.Size = new System.Drawing.Size( 264, 158 );
			this.listFacts.TabIndex = 3;
			this.listFacts.SelectedIndexChanged += new System.EventHandler( this.listFacts_SelectedIndexChanged );
			this.listFacts.KeyDown += new System.Windows.Forms.KeyEventHandler( this.listFacts_KeyDown );
			// 
			// groupInput
			// 
			this.groupInput.Controls.Add( this.labelGoal );
			this.groupInput.Controls.Add( this.labelFacts );
			this.groupInput.Controls.Add( this.labelRules );
			this.groupInput.Controls.Add( this.buttonExecute );
			this.groupInput.Controls.Add( this.textInputGoal );
			this.groupInput.Controls.Add( this.textInputFact );
			this.groupInput.Controls.Add( this.buttonAddFact );
			this.groupInput.Controls.Add( this.buttonRemoveFact );
			this.groupInput.Controls.Add( this.buttonRemoveRule );
			this.groupInput.Controls.Add( this.buttonAddRule );
			this.groupInput.Controls.Add( this.textInputRule );
			this.groupInput.Controls.Add( this.listFacts );
			this.groupInput.Controls.Add( this.listRules );
			this.groupInput.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupInput.ForeColor = System.Drawing.Color.Blue;
			this.groupInput.Location = new System.Drawing.Point( 12, 466 );
			this.groupInput.Name = "groupInput";
			this.groupInput.Size = new System.Drawing.Size( 1014, 267 );
			this.groupInput.TabIndex = 2;
			this.groupInput.TabStop = false;
			this.groupInput.Text = "�������� ������ :";
			// 
			// labelGoal
			// 
			this.labelGoal.AutoSize = true;
			this.labelGoal.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.labelGoal.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelGoal.Location = new System.Drawing.Point( 650, 243 );
			this.labelGoal.Name = "labelGoal";
			this.labelGoal.Size = new System.Drawing.Size( 39, 13 );
			this.labelGoal.TabIndex = 9;
			this.labelGoal.Text = "���� :";
			// 
			// labelFacts
			// 
			this.labelFacts.AutoSize = true;
			this.labelFacts.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.labelFacts.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelFacts.Location = new System.Drawing.Point( 737, 19 );
			this.labelFacts.Name = "labelFacts";
			this.labelFacts.Size = new System.Drawing.Size( 49, 13 );
			this.labelFacts.TabIndex = 9;
			this.labelFacts.Text = "����� :";
			// 
			// labelRules
			// 
			this.labelRules.AutoSize = true;
			this.labelRules.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.labelRules.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelRules.Location = new System.Drawing.Point( 6, 19 );
			this.labelRules.Name = "labelRules";
			this.labelRules.Size = new System.Drawing.Size( 57, 13 );
			this.labelRules.TabIndex = 9;
			this.labelRules.Text = "������� :";
			// 
			// buttonExecute
			// 
			this.buttonExecute.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.buttonExecute.ForeColor = System.Drawing.SystemColors.ControlText;
			this.buttonExecute.Location = new System.Drawing.Point( 919, 239 );
			this.buttonExecute.Name = "buttonExecute";
			this.buttonExecute.Size = new System.Drawing.Size( 85, 22 );
			this.buttonExecute.TabIndex = 5;
			this.buttonExecute.Text = "���������";
			this.buttonExecute.UseVisualStyleBackColor = true;
			this.buttonExecute.Click += new System.EventHandler( this.buttonExecute_Click );
			// 
			// textInputGoal
			// 
			this.textInputGoal.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textInputGoal.Location = new System.Drawing.Point( 695, 240 );
			this.textInputGoal.Name = "textInputGoal";
			this.textInputGoal.Size = new System.Drawing.Size( 218, 20 );
			this.textInputGoal.TabIndex = 4;
			this.textInputGoal.Text = "f( 1, 5 )";
			this.textInputGoal.KeyDown += new System.Windows.Forms.KeyEventHandler( this.textInputGoal_KeyDown );
			// 
			// textInputFact
			// 
			this.textInputFact.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textInputFact.Location = new System.Drawing.Point( 740, 198 );
			this.textInputFact.Name = "textInputFact";
			this.textInputFact.Size = new System.Drawing.Size( 219, 20 );
			this.textInputFact.TabIndex = 2;
			this.textInputFact.KeyDown += new System.Windows.Forms.KeyEventHandler( this.textInputFact_KeyDown );
			// 
			// buttonAddFact
			// 
			this.buttonAddFact.Location = new System.Drawing.Point( 689, 198 );
			this.buttonAddFact.Name = "buttonAddFact";
			this.buttonAddFact.Size = new System.Drawing.Size( 22, 22 );
			this.buttonAddFact.TabIndex = 1;
			this.buttonAddFact.TabStop = false;
			this.buttonAddFact.Text = "+";
			this.buttonAddFact.UseVisualStyleBackColor = true;
			this.buttonAddFact.Click += new System.EventHandler( this.buttonAddRule_Click );
			// 
			// buttonRemoveFact
			// 
			this.buttonRemoveFact.Location = new System.Drawing.Point( 982, 197 );
			this.buttonRemoveFact.Name = "buttonRemoveFact";
			this.buttonRemoveFact.Size = new System.Drawing.Size( 22, 22 );
			this.buttonRemoveFact.TabIndex = 7;
			this.buttonRemoveFact.TabStop = false;
			this.buttonRemoveFact.Text = "-";
			this.buttonRemoveFact.UseVisualStyleBackColor = true;
			this.buttonRemoveFact.Click += new System.EventHandler( this.buttonRemoveFact_Click );
			// 
			// buttonRemoveRule
			// 
			this.buttonRemoveRule.Location = new System.Drawing.Point( 712, 198 );
			this.buttonRemoveRule.Name = "buttonRemoveRule";
			this.buttonRemoveRule.Size = new System.Drawing.Size( 22, 22 );
			this.buttonRemoveRule.TabIndex = 3;
			this.buttonRemoveRule.TabStop = false;
			this.buttonRemoveRule.Text = "-";
			this.buttonRemoveRule.UseVisualStyleBackColor = true;
			this.buttonRemoveRule.Click += new System.EventHandler( this.buttonRemoveRule_Click );
			// 
			// buttonAddRule
			// 
			this.buttonAddRule.Location = new System.Drawing.Point( 960, 197 );
			this.buttonAddRule.Name = "buttonAddRule";
			this.buttonAddRule.Size = new System.Drawing.Size( 22, 22 );
			this.buttonAddRule.TabIndex = 5;
			this.buttonAddRule.TabStop = false;
			this.buttonAddRule.Text = "+";
			this.buttonAddRule.UseVisualStyleBackColor = true;
			this.buttonAddRule.Click += new System.EventHandler( this.buttonAddFact_Click );
			// 
			// textInputRule
			// 
			this.textInputRule.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textInputRule.Location = new System.Drawing.Point( 7, 199 );
			this.textInputRule.Name = "textInputRule";
			this.textInputRule.Size = new System.Drawing.Size( 681, 20 );
			this.textInputRule.TabIndex = 0;
			this.textInputRule.KeyDown += new System.Windows.Forms.KeyEventHandler( this.textInputRule_KeyDown );
			// 
			// checkUpdateRulesStack
			// 
			this.checkUpdateRulesStack.AutoSize = true;
			this.checkUpdateRulesStack.Checked = true;
			this.checkUpdateRulesStack.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkUpdateRulesStack.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.checkUpdateRulesStack.ForeColor = System.Drawing.SystemColors.ControlText;
			this.checkUpdateRulesStack.Location = new System.Drawing.Point( 919, 399 );
			this.checkUpdateRulesStack.Name = "checkUpdateRulesStack";
			this.checkUpdateRulesStack.Size = new System.Drawing.Size( 81, 17 );
			this.checkUpdateRulesStack.TabIndex = 1;
			this.checkUpdateRulesStack.Text = "���������";
			this.checkUpdateRulesStack.UseVisualStyleBackColor = true;
			// 
			// listRulesStack
			// 
			this.listRulesStack.Font = new System.Drawing.Font( "Courier New", 8.25F );
			this.listRulesStack.FormattingEnabled = true;
			this.listRulesStack.ItemHeight = 14;
			this.listRulesStack.Location = new System.Drawing.Point( 506, 25 );
			this.listRulesStack.Name = "listRulesStack";
			this.listRulesStack.Size = new System.Drawing.Size( 494, 368 );
			this.listRulesStack.TabIndex = 0;
			this.listRulesStack.TabStop = false;
			// 
			// tabMechanisms
			// 
			this.tabMechanisms.Controls.Add( this.tabReversedLogicalDeduction );
			this.tabMechanisms.Controls.Add( this.tabDirectProductionDeduction );
			this.tabMechanisms.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.tabMechanisms.Location = new System.Drawing.Point( 12, 12 );
			this.tabMechanisms.Name = "tabMechanisms";
			this.tabMechanisms.SelectedIndex = 0;
			this.tabMechanisms.Size = new System.Drawing.Size( 1014, 448 );
			this.tabMechanisms.TabIndex = 4;
			// 
			// tabReversedLogicalDeduction
			// 
			this.tabReversedLogicalDeduction.Controls.Add( this.checkTrace );
			this.tabReversedLogicalDeduction.Controls.Add( this.buttonContinue );
			this.tabReversedLogicalDeduction.Controls.Add( this.checkUpdateRulesStack );
			this.tabReversedLogicalDeduction.Controls.Add( this.listRulesStack );
			this.tabReversedLogicalDeduction.Controls.Add( this.checkExpandTree );
			this.tabReversedLogicalDeduction.Controls.Add( this.labelRulesStack );
			this.tabReversedLogicalDeduction.Controls.Add( this.labelDeductionTree );
			this.tabReversedLogicalDeduction.Controls.Add( this.treeView );
			this.tabReversedLogicalDeduction.Location = new System.Drawing.Point( 4, 22 );
			this.tabReversedLogicalDeduction.Name = "tabReversedLogicalDeduction";
			this.tabReversedLogicalDeduction.Padding = new System.Windows.Forms.Padding( 3 );
			this.tabReversedLogicalDeduction.Size = new System.Drawing.Size( 1006, 422 );
			this.tabReversedLogicalDeduction.TabIndex = 0;
			this.tabReversedLogicalDeduction.Text = "�������� ���������� �����";
			this.tabReversedLogicalDeduction.UseVisualStyleBackColor = true;
			// 
			// checkTrace
			// 
			this.checkTrace.AutoSize = true;
			this.checkTrace.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.checkTrace.Location = new System.Drawing.Point( 507, 399 );
			this.checkTrace.Name = "checkTrace";
			this.checkTrace.Size = new System.Drawing.Size( 98, 17 );
			this.checkTrace.TabIndex = 9;
			this.checkTrace.Text = "������������";
			this.checkTrace.UseVisualStyleBackColor = true;
			this.checkTrace.CheckedChanged += new System.EventHandler( this.checkTrace_CheckedChanged );
			// 
			// buttonContinue
			// 
			this.buttonContinue.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.buttonContinue.Location = new System.Drawing.Point( 610, 395 );
			this.buttonContinue.Name = "buttonContinue";
			this.buttonContinue.Size = new System.Drawing.Size( 75, 23 );
			this.buttonContinue.TabIndex = 8;
			this.buttonContinue.Text = "�����";
			this.buttonContinue.UseVisualStyleBackColor = true;
			this.buttonContinue.Click += new System.EventHandler( this.buttonContinue_Click );
			// 
			// checkExpandTree
			// 
			this.checkExpandTree.AutoSize = true;
			this.checkExpandTree.Checked = true;
			this.checkExpandTree.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkExpandTree.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.checkExpandTree.Location = new System.Drawing.Point( 9, 399 );
			this.checkExpandTree.Name = "checkExpandTree";
			this.checkExpandTree.Size = new System.Drawing.Size( 115, 17 );
			this.checkExpandTree.TabIndex = 7;
			this.checkExpandTree.Text = "�������� ������";
			this.checkExpandTree.UseVisualStyleBackColor = true;
			this.checkExpandTree.CheckedChanged += new System.EventHandler( this.checkExpandTree_CheckedChanged );
			// 
			// labelRulesStack
			// 
			this.labelRulesStack.AutoSize = true;
			this.labelRulesStack.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.labelRulesStack.ForeColor = System.Drawing.Color.Blue;
			this.labelRulesStack.Location = new System.Drawing.Point( 503, 6 );
			this.labelRulesStack.Name = "labelRulesStack";
			this.labelRulesStack.Size = new System.Drawing.Size( 89, 13 );
			this.labelRulesStack.TabIndex = 7;
			this.labelRulesStack.Text = "���� ������ :";
			// 
			// labelDeductionTree
			// 
			this.labelDeductionTree.AutoSize = true;
			this.labelDeductionTree.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.labelDeductionTree.ForeColor = System.Drawing.Color.Blue;
			this.labelDeductionTree.Location = new System.Drawing.Point( 6, 6 );
			this.labelDeductionTree.Name = "labelDeductionTree";
			this.labelDeductionTree.Size = new System.Drawing.Size( 108, 13 );
			this.labelDeductionTree.TabIndex = 7;
			this.labelDeductionTree.Text = "������ ������ :";
			// 
			// treeView
			// 
			this.treeView.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.treeView.Location = new System.Drawing.Point( 6, 25 );
			this.treeView.Name = "treeView";
			this.treeView.Size = new System.Drawing.Size( 494, 368 );
			this.treeView.TabIndex = 6;
			this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler( this.treeView_AfterSelect );
			// 
			// tabDirectProductionDeduction
			// 
			this.tabDirectProductionDeduction.Location = new System.Drawing.Point( 4, 22 );
			this.tabDirectProductionDeduction.Name = "tabDirectProductionDeduction";
			this.tabDirectProductionDeduction.Padding = new System.Windows.Forms.Padding( 3 );
			this.tabDirectProductionDeduction.Size = new System.Drawing.Size( 1006, 422 );
			this.tabDirectProductionDeduction.TabIndex = 1;
			this.tabDirectProductionDeduction.Text = "������ ����� � ������������� ������";
			this.tabDirectProductionDeduction.UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 1039, 747 );
			this.Controls.Add( this.tabMechanisms );
			this.Controls.Add( this.groupInput );
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.Text = "���������� �������";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.MainForm_FormClosing );
			this.Load += new System.EventHandler( this.MainForm_Load );
			this.groupInput.ResumeLayout( false );
			this.groupInput.PerformLayout();
			this.tabMechanisms.ResumeLayout( false );
			this.tabReversedLogicalDeduction.ResumeLayout( false );
			this.tabReversedLogicalDeduction.PerformLayout();
			this.ResumeLayout( false );

        }

        #endregion

		private System.Windows.Forms.ListBox listRules;
		private System.Windows.Forms.ListBox listFacts;
        private System.Windows.Forms.GroupBox groupInput;
		private System.Windows.Forms.TextBox textInputGoal;
        private System.Windows.Forms.Button buttonRemoveRule;
        private System.Windows.Forms.TextBox textInputRule;
		private System.Windows.Forms.Button buttonAddFact;
		private System.Windows.Forms.Button buttonExecute;
		private System.Windows.Forms.TextBox textInputFact;
		private System.Windows.Forms.Button buttonRemoveFact;
		private System.Windows.Forms.Button buttonAddRule;
		private System.Windows.Forms.Label labelRules;
		private System.Windows.Forms.Label labelGoal;
		private System.Windows.Forms.Label labelFacts;
		private System.Windows.Forms.CheckBox checkUpdateRulesStack;
		private System.Windows.Forms.ListBox listRulesStack;
		private System.Windows.Forms.TabControl tabMechanisms;
		private System.Windows.Forms.TabPage tabReversedLogicalDeduction;
		private System.Windows.Forms.TabPage tabDirectProductionDeduction;
		private System.Windows.Forms.CheckBox checkExpandTree;
		private System.Windows.Forms.Label labelDeductionTree;
		private System.Windows.Forms.TreeView treeView;
		private System.Windows.Forms.Label labelRulesStack;
		private System.Windows.Forms.CheckBox checkTrace;
		private System.Windows.Forms.Button buttonContinue;
    }
}

