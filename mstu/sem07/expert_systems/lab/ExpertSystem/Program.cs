using System;
using System.Windows.Forms;



namespace ExpertSystem
{
	using Storing;



	static class Program
	{
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault( false );


			DBStorage db = new DBStorage();

			AuthForm authForm = new AuthForm( db );
			DialogResult choice = authForm.ShowDialog();

			if ( choice == DialogResult.Yes )
			{
				Storage storage = null;
				if ( db.Connected )
					storage = db;
				else
					storage = new FileStorage();


				Application.Run( new MainForm( storage ) );


				if ( db.Connected )
					db.Disconnect();
			}
		}
	}
}