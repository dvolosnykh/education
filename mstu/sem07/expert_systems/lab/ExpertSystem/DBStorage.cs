using System;
using System.Data;
using System.Data.SqlClient;



namespace ExpertSystem.Storing
{
	public class DBTable : Table
	{
		public const string RulesTableName = "Rules";
		public const string FactsTableName = "Facts";

		protected SqlDataAdapter _adapter;



		public DBTable( string tableName, SqlConnection connection )
		{
			_adapter = BuildAdapter( tableName, connection );
		}


		public override string[] Load()
		{
			// load existing table.
			DataTable table = new DataTable();
			_adapter.Fill( table );

			string[] lines = new string[ table.Rows.Count ];
			for ( int i = 0; i < table.Rows.Count; i++ )
				lines[ i ] = table.Rows[ i ][ "Text" ] as string;

			return ( lines );
		}

		public override void Save( string[] lines )
		{
			// load existing table.
			DataTable table = new DataTable();
			_adapter.Fill( table );

			// mark all rows as deleted.
			for ( int i = 0; i < table.Rows.Count; i++ )
				table.Rows[ i ].Delete();

			// save data into new rows.
			for ( int i = 0; i < lines.Length; i++ )
			{
				DataRow row = table.NewRow();

				// parse here.
				row[ "Text" ] = lines[ i ];

				table.Rows.Add( row );
			}

			// ship modified table to database.
			_adapter.Update( table );
		}

/*		public override void Add( string strRule )
		{
			SqlCommand insertCmd = _adapter.InsertCommand;
			insertCmd.Parameters[ "@Text" ].Value = strRule;
			insertCmd.ExecuteNonQuery();
		}
*/
		private static SqlDataAdapter BuildAdapter( string tableName, SqlConnection connection )
		{
			SqlDataAdapter adapter = new SqlDataAdapter();
			adapter.SelectCommand = new SqlCommand( "SELECT * FROM " + tableName, connection );
			SqlCommandBuilder builder = new SqlCommandBuilder( adapter );
			adapter.DeleteCommand = builder.GetDeleteCommand( true );
			adapter.InsertCommand = builder.GetInsertCommand( true );
			adapter.UpdateCommand = builder.GetUpdateCommand( true );

			return ( adapter );
		}
	}


	public class DBStorage : Storage
	{
		private const string _dbName = "ExpertSystem";

		
		private SqlConnection _connection = new SqlConnection();


		public void Connect( string login, string password )
		{
			if ( Connected )
				Disconnect();

			SqlConnectionStringBuilder connectionStr = new SqlConnectionStringBuilder();
			connectionStr.UserID = login;
			connectionStr.Password = password;
			connectionStr.InitialCatalog = _dbName;
			connectionStr.DataSource = "(local)";
			connectionStr.ConnectTimeout = 1;

            _connection.ConnectionString = connectionStr.ToString();
			_connection.Open();

			Rules = new DBTable( DBTable.RulesTableName, _connection );
			Facts = new DBTable( DBTable.FactsTableName, _connection );
		}

		public void Disconnect()
		{
			if ( _connection != null )
				_connection.Close();
		}

		public bool Connected
		{
			get { return ( _connection != null && _connection.State != ConnectionState.Closed ); }
		}
	}
}
