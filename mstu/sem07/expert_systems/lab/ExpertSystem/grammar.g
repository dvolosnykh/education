header
{
	using System.Collections.Generic;
	using ExpertSystem.Grammar;
}
options
{
	language = "CSharp";
	namespace = "ExpertSystem";
}



class GrammarParser extends Parser;
options
{
	k = 20;
	defaultErrorHandler = false;
	buildAST = true;
}



public fact returns [ Condition f = new Condition() ]
{
	Predicate p;
	TermsList constants;
}
:
	p = predicate { f.Predicate = p; }
	TERMS_LIST_START
	constants = constantsList { f.TermsList = constants; }
	TERMS_LIST_END
;

protected constantsList returns [ TermsList constants = new TermsList() ]
{
	Constant c;
}
:
	c = constant { constants.Add( c ); }
	( SPLIT_TERMS c = constant { constants.Add( c ); } )+
;

public rule returns [ Rule r = new Rule() ]
{
	Title t;
}
:
	t = title { r.Title = t; }
	ASSIGN
	{ r.ConditionsList = new ConditionsList(); }
	expression[ r.ConditionsList ]
	{ r.RegisterVariables(); }
;

protected title returns [ Condition t ]
:
	t = condition
;

protected expression [ ref ConditionsList list ]
:
	(
		condition ( BINARY_OPERATOR expression[ list ] )?
		| UNARY_OPERATOR expression[ list ]
		| SUBEXPRESSION_START expression[ list ] SUBEXPRESSION_END
	)
	{ list.RegisterVariables(); }
;

protected condition returns [ Condition c = new Condition() ]
{
	Predicate p;
	TermsList terms;
}
:
	p = predicate { c.Predicate = p; }
	TERMS_LIST_START
	terms = termsList { c.TermsList = terms; }
	TERMS_LIST_END
;

protected predicate returns [ Predicate p = new Predicate() ]
:
	id : ID { p.Name = id.getText(); }
;

protected termsList returns [ TermsList list = new TermsList() ]
{
	Term t;
}
:
	t = term { list.Add( t ); }
	( SPLIT_TERMS t = term { list.Add( t ); } )+
;

protected term returns [ Term t ]
:
	t = variable
	| t = constant
;

protected variable returns [ Variable v = new Variable() ]
:
	VARIABLE_PREFIX
	id : ID { v.Name = id.getText(); }
;

protected constant returns [ Constant c = new Constant() ]
:
	v : VALUE { c.Value = v.getText(); }
;



class GrammarLexer extends Lexer;
options
{
	k = 1;
	testLiterals = true;
}

ASSIGN : ">";

protected LATIN_LETTER : 'A'..'Z' | 'a'..'z';
protected LETTER : LATIN_LETTER;
protected DIGIT : '0'..'9';
protected UNDERSCORE : '_';

VARIABLE_PREFIX : "$" { $setType(Token.SKIP); };
protected FIRST_SYMBOL : LETTER | UNDERSCORE;
protected ANY_SYMBOL : FIRST_SYMBOL | DIGIT;
ID : FIRST_SYMBOL ( ANY_SYMBOL )*;
protected NUMBER : ( DIGIT )+;
VALUE : ID | NUMBER;

TERMS_LIST_START : "(" { $setType(Token.SKIP); };
TERMS_LIST_END : ")" { $setType(Token.SKIP); };
SPLIT_TERMS : "," { $setType(Token.SKIP); };

SUBEXPRESSION_START : "(" { $setType(Token.SKIP); };
SUBEXPRESSION_END : ")" { $setType(Token.SKIP); };

protected NOT : "!";
protected AND : "&";
protected XOR : "^";
protected OR  : "|";
UNARY_OPERATOR: NOT;
BINARY_OPERATOR : AND | XOR | OR;

WS : ( ' ' | '\r' '\n' | '\n' | '\t' ) {$setType( Token.SKIP );};