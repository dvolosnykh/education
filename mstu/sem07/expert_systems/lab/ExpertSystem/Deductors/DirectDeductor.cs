using System;
using System.Collections.Generic;




namespace ExpertSystem.Deduction
{
	using Grammar;
	using Aux;
	using RPN;



	public class DirectDeductor : Deductor
	{
		public DirectDeductor( ListOfRules rules, ListOfFacts facts, FeedbackDelegates delegates )
			: base( rules, facts, delegates ) {}


		protected override Result Deduce( Condition goal, out ConditionDNode dTree )
		{
			dTree = new ConditionDNode( null );
			Result result = new Result();
			
			for ( bool repeat = true; repeat; )
			{
				int memTrueListCount = _trueList.Subgoals.Count;

				TryDeducingFromRule( dTree );

				repeat = _trueList.Subgoals.Count > memTrueListCount;
			}

			result.Reached = _trueList.Subgoals.Contains( goal );
			if ( result.Reached )
				dTree.Subgoal = goal;

			return ( result );
		}

		protected override Result AfterAllKnowledgesChecked( Result result, ConditionDNode dNode )
		{
			if ( !result.Reached )
			{
				result.Reached = false;
				result.More = false;
			}

			return ( result );
		}

		private Result TryDeducingFromRule( ConditionDNode dNode )
		{
			//if ( dNode.Reached )
			//	dNode.ClearChildren();

			Result result = new Result();

			// (*) main subgoal.
			//_subgoalsStack.Push( ( Condition )subgoal.Copy() );

			int ruleIndex = ( dNode.State == State.UsesRule ? dNode.Index + 1 : 0 );
			for ( ; ruleIndex < _rules.Count && !result.Reached; ruleIndex++ )
			{
				Rule usedRule = ( Rule )_rules[ ruleIndex ].Copy();

				do
				{
					// (**) temporary subgoals.
					int memorizeSubgoalsCount = _subgoalsStack.Count;

					// (***) current rule.
					_rulesStack.Push( usedRule );
					OnRulesStackChange();

					result = EvaluateChild( ref dNode.Child[ ChildIndex.First ], usedRule.Expression.RPNTree, false );

					// (***)
					_rulesStack.Pop();
					OnRulesStackChange();

					// (**)
					for ( ; _subgoalsStack.Count > memorizeSubgoalsCount; )
						_subgoalsStack.Pop();


					if ( !result.Reached )
					{
						result.FormsCycle = false;
						dNode.ClearChildren();
					}
					
					dNode.Subgoal = usedRule.Title;

					Memorize( result, dNode );
				}
				while ( result.Reached );
			}
			ruleIndex--;

			// (*)
			//_subgoalsStack.Pop();


			if ( result.Reached )
			{
				dNode.State = State.UsesRule;
				dNode.Index = ruleIndex;
				OnUpdateLastRule();

				result.More |= ( ruleIndex < _rules.Count - 1 );
			}
			else
			{
				result.More = false;
			}

			return ( result );
		}
	}
}
