using System;
using System.Collections.Generic;
using System.Threading;



namespace ExpertSystem.Deduction
{
	using Grammar;
	using Aux;
	using RPN;



	public enum State : int
	{
		New,
		UsesFact,
		UsesRule,

		// auxiliary
		NotChecked,
		Unreacheable
	}

	#region Nodes

	public abstract class DNode
	{
		private static int _idEnum = 1;
		public int ID;
		public DNode[] Child = new DNode[ ChildIndex.Total ];



		public DNode()
		{
			ID = _idEnum++;
		}


		public DNode Copy()
		{
			DNode dest = null;

			if ( this is OperatorDNode )
			{
				OperatorDNode operSrc = this as OperatorDNode;
				OperatorDNode operDest = new OperatorDNode( operSrc.Operator );
				dest = operDest;

				if ( Child[ ChildIndex.First ] != null )
					dest.Child[ ChildIndex.First ] = Child[ ChildIndex.First ].Copy();

				if ( operSrc.Operator.Binary && Child[ ChildIndex.Second ] != null )
					dest.Child[ ChildIndex.Second ] = Child[ ChildIndex.Second ].Copy();
			}
			else if ( this is ConditionDNode )
			{
				ConditionDNode condSrc = this as ConditionDNode;
				ConditionDNode condDest = new ConditionDNode( ( Condition )condSrc.Subgoal.Copy() );
				dest = condDest;

				condDest.Index = condSrc.Index;
				condDest.State = condSrc.State;

				if ( Child[ ChildIndex.First ] != null )
					dest.Child[ ChildIndex.First ] = Child[ ChildIndex.First ].Copy();
			}

			return ( dest );
		}


		public void ClearChildren()
		{
			Child[ ChildIndex.First ] = null;
			Child[ ChildIndex.Second ] = null;
		}

		public bool IsLeaf
		{
			get { return ( Child[ ChildIndex.First ] == null && Child[ ChildIndex.Second ] == null ); }
		}
	}

	public class ConditionDNode : DNode
	{
		public Condition Subgoal;
		public State State = State.New;
		public int Index;
		public bool More = true;	// has meaning only for this node.
		public bool Reached = false;
		public int ChangeID;



		public ConditionDNode( Condition subgoal )
		{
			Subgoal = subgoal;
		}


		public override string ToString()
		{
			return ( Subgoal.Text );
		}
	}

	public class OperatorDNode : DNode
	{
		public Operator Operator;
		public bool New = true;
		public bool Reverse = false;
		public bool RecoverFirst = false;
		public bool SecondReached = false;


		public OperatorDNode( Operator oper )
		{
			Operator = oper;
		}


		public override string ToString()
		{
			return ( Operator.Text );
		}
	}

	#endregion

	public class TrueList
	{
		public ListOfConditions Subgoals = new ListOfConditions();
		public List<ConditionDNode> Nodes = new List<ConditionDNode>();
	}

	public class SubgoalsStack : StackOf<Condition>
	{
		public int StartIndex = 0;


		public bool FormsCycle( Condition item )
		{
			bool formsCycle = false;
			for ( int i = StartIndex; i < Count && !formsCycle; i++ )
				formsCycle = item.EquivalentTo( this[ i ] );

			return ( formsCycle );
		}
	}

	public class Result
	{
		public bool Reached = false;
		public bool More = true;	// has meaning for the whole subtree.
		public bool FormsCycle = false;
	}



	public abstract class Deductor
	{
		private Thread _thread;

		protected ListOfRules _rules;
		private ListOfFacts _facts;

		protected SubgoalsStack _subgoalsStack = new SubgoalsStack();
		protected StackOfRules _rulesStack = new StackOfRules();
		private ListOfConditions _falseSubgoals = new ListOfConditions();
		protected TrueList _trueList = new TrueList();



		public Deductor( ListOfRules rules, ListOfFacts facts, FeedbackDelegates delegates )
		{
			_rules = rules;
			_facts = facts;

			_eventOnComplete += delegates.OnComplete;
			_eventOnTrace += delegates.OnTrace;
			_eventOnBeginTrace += delegates.OnBeginTrace;
			_eventOnRulesStackChange += delegates.OnRulesStackChange;
			_eventUpdateLastRule += delegates.OnUpdateLastRule;
		}


		#region Interface

		public bool InProgress
		{
			get { return ( _thread.IsAlive ); }
		}

		public void Cancel()
		{
			_thread.Abort();
		}

		#endregion


		public void Start( Condition goal )
		{
			_thread = new Thread( Run );
			_thread.Start( goal );
		}

		private void Run( object obj )
		{
			Condition goal = obj as Condition;


			ConditionDNode dTree;
			Result result = Deduce( goal, out dTree );
			if ( !result.Reached )
				dTree = null;

			OnComplete( result, dTree );
		}

		protected abstract Result Deduce( Condition goal, out ConditionDNode dTree );



		protected Result EvaluateSubgoal( ConditionDNode dNode )
		{
			if ( dNode.State == State.NotChecked ||
				dNode.State == State.Unreacheable )
			{
				dNode.State = State.New;
			}


			Rule usedRule = _rulesStack.Peek();
			if ( _rulesStack.Count > 0 )
			{
				if ( dNode.State == State.New )
				{
					dNode.ChangeID = usedRule.ChangeID;
				}
				else
				{
					usedRule.RevertVariables( dNode.ChangeID );
					OnUpdateLastRule();
				}
			}


			Result result = new Result();
			bool noVariables = dNode.Subgoal.NoVariables;

			if ( dNode.State == State.New )
			{
				result.FormsCycle = _subgoalsStack.FormsCycle( dNode.Subgoal );

				if ( result.FormsCycle )
				{
					result.More = false;
				}
				else if ( noVariables && IsKnowledge( dNode ) )
				{
					result.Reached = true;
					result.More = false;
				}
			}

			if ( !result.FormsCycle )
			{
				if ( dNode.State <= State.UsesFact && !result.Reached && !noVariables )
					result.Reached = TryReplacingByKnowledges( dNode );

				result = AfterAllKnowledgesChecked( result, dNode );
			}


			dNode.More = result.More;
			dNode.Reached = result.Reached;
			return ( result );
		}

		protected abstract Result AfterAllKnowledgesChecked( Result result, ConditionDNode dNode );

		private bool IsKnowledge( ConditionDNode dNode )
		{
			int index = _facts.IndexOf( dNode.Subgoal );
			bool reached = ( index >= 0 );
			if ( reached )
			{
				dNode.State = State.UsesFact;
				dNode.Index = index;
			}

			if ( !reached )
			{
				index = _trueList.Subgoals.IndexOf( dNode.Subgoal );
				reached = ( index >= 0 );
				if ( reached )
				{
					dNode.State = State.UsesFact;
					ConditionDNode template = _trueList.Nodes[ index ];
					dNode.State = template.State;
					dNode.Index = template.Index;
					dNode.Child[ ChildIndex.First ] = template.Child[ ChildIndex.First ];
				}
			}

			return ( reached );
		}

		private bool TryReplacingByKnowledges( ConditionDNode dNode )
		{
			int index = TryReplacingFromList( dNode, _facts );
			bool reached = ( index >= 0 );
			if ( reached )
			{
				dNode.State = State.UsesFact;
				dNode.Index = index;
			}

			if ( !reached )
			{
				index = TryReplacingFromList( dNode, _trueList.Subgoals );
				reached = ( index >= 0 );
				if ( reached )
				{
					ConditionDNode template = _trueList.Nodes[ index ];
					dNode.State = template.State;
					dNode.Index = template.Index;
					dNode.Child[ ChildIndex.First ] = template.Child[ ChildIndex.First ];
				}
			}

			return ( reached );
		}

		private int TryReplacingFromList( ConditionDNode dNode, ListOfConditions list )
		{
			Condition subgoal = dNode.Subgoal;


			Rule usedRule = _rulesStack.Peek();
			int memorizeChangeID = 0;
			if ( _rulesStack.Count > 0 )
				memorizeChangeID = usedRule.ChangeID;

			bool reached = false;
			int index = ( dNode.State == State.UsesFact ? dNode.Index + 1 : 0 );
			for ( ; index < list.Count && !reached; index++ )
			{
				Condition item = list[ index ];

				reached = subgoal.ReplaceableBy( item ) && !_subgoalsStack.FormsCycle( item );
				if ( reached && _rulesStack.Count > 0 )
				{
					usedRule.SetVariables( subgoal, list[ index ] );
					Condition title = usedRule.Title;

					bool titleIsFact = title.NoVariables && list.IndexOf( title ) >= 0;
					if ( reached = !titleIsFact )
					{
						Condition temp = _subgoalsStack.Pop();
						reached = !_subgoalsStack.FormsCycle( usedRule.Title );
						_subgoalsStack.Push( temp );
					}

					if ( !reached )
						usedRule.RevertVariables( memorizeChangeID );
				}
			}
			index--;

			if ( reached )
			{
				if ( _subgoalsStack.Peek() != usedRule.Title )
					_subgoalsStack.Push( usedRule.Title );

				OnUpdateLastRule();
			}

			return ( reached ? index : -1 );
		}


		protected void Memorize( Result result, ConditionDNode dNode )
		{
			Condition subgoal = dNode.Subgoal;

			if ( result.Reached )
			{
				if ( !( _trueList.Subgoals.Contains( subgoal ) || _facts.Contains( subgoal ) ) )
				{
					ConditionDNode copy = ( ConditionDNode )dNode.Copy();
					_trueList.Subgoals.Add( copy.Subgoal );
					_trueList.Nodes.Add( copy );
				}
			}
			else if ( !result.FormsCycle && subgoal.NoVariables )
			{
				if ( !_trueList.Subgoals.Contains( subgoal ) )
					_falseSubgoals.Add( ( Condition )subgoal.Copy() );
			}

			result.FormsCycle = false;
		}

		protected Result EvaluateChild( ref DNode dNode, RPNNode rpnNode, bool recover )
		{
			Result result = null;

			bool createNewNode = ( dNode == null );

			if ( rpnNode is OperatorRPNNode )
			{
				OperatorRPNNode operRPNNode = rpnNode as OperatorRPNNode;

				if ( createNewNode )
					dNode = new OperatorDNode( operRPNNode.Operator );

				OperatorDNode operDNode = dNode as OperatorDNode;

				result = EvaluateOperator( operDNode, operRPNNode, recover );
			}
			else if ( rpnNode is ConditionRPNNode )
			{
				ConditionRPNNode condRPNNode = rpnNode as ConditionRPNNode;

				if ( createNewNode )
					dNode = new ConditionDNode( null );

				ConditionDNode condDNode = dNode as ConditionDNode;

				if ( recover )
				{
					_rulesStack.Peek().SetVariables( condRPNNode.Condition, condDNode.Subgoal );
					result = new Result();
					result.Reached = condDNode.Reached;
					result.More = condDNode.More;
				}
				else
				{
					condDNode.Subgoal = condRPNNode.Condition;

					if ( condDNode.More && !_falseSubgoals.Contains( condDNode.Subgoal ) )
					{
						result = EvaluateSubgoal( condDNode );
					}
					else
					{
						result = new Result();
						result.More = false;
					}
				}
			}

			return ( result );
		}

		private void CreateChildren( ref DNode dNode, RPNNode rpnNode, State fooState )
		{
			if ( rpnNode is OperatorRPNNode )
			{
				OperatorRPNNode operRPNNode = rpnNode as OperatorRPNNode;
				dNode = new OperatorDNode( operRPNNode.Operator );
				OperatorDNode operDNode = dNode as OperatorDNode;

				CreateChildren( ref operDNode.Child[ ChildIndex.First ], operRPNNode.Child[ ChildIndex.First ], fooState );
				if ( operRPNNode.Operator.Binary )
					CreateChildren( ref operDNode.Child[ ChildIndex.Second ], operRPNNode.Child[ ChildIndex.Second ], fooState );
			}
			else if ( rpnNode is ConditionRPNNode )
			{
				ConditionRPNNode conditionRPNNode = rpnNode as ConditionRPNNode;
				ConditionDNode conditionDNode = new ConditionDNode( conditionRPNNode.Condition );
				conditionDNode.State = fooState;
				dNode = conditionDNode;
			}
		}

		private Result EvaluateOperator( OperatorDNode dNode, OperatorRPNNode rpnNode, bool recover )
		{
			Result result;
			if ( dNode.Operator.Binary )
			{
				BinaryDelegates delegates = TuneBinaryDelegates( dNode.Operator );

				if ( !dNode.Reverse )
				{
					Rule usedRule = _rulesStack.Peek();
					int memorizeChangeID = usedRule.ChangeID;

					result = EvaluateBinary( dNode, rpnNode, recover, delegates );
					if ( !result.Reached )
					{
						usedRule.RevertVariables( memorizeChangeID );
						dNode.Reverse = true;

						OnUpdateLastRule();
					}
				}
				else
					result = new Result();

				if ( dNode.Reverse && !result.FormsCycle )
					result = EvaluateBinary( dNode, rpnNode, recover, delegates );
			}
			else
			{
				UnaryDelegates delegates = TuneUnaryDelegates( dNode.Operator );

				result = EvaluateUnary( dNode, rpnNode, recover, delegates );
			}

			dNode.New = false;

			return ( result );
		}


		#region Operators

		#region Operators delegates

		private class BinaryDelegates
		{
			public delegate bool OperatorDelegate( bool first, bool second );
			public delegate bool EvaluateSecondDelegate( bool first );
			public delegate void AlternativeDelegate( Result result, ref DNode dNode, RPNNode rpnNode );

			public OperatorDelegate Operator = null;
			public EvaluateSecondDelegate EvaluateSecond = null;
			public AlternativeDelegate Alternative = null;
		}

		private class UnaryDelegates
		{
			public delegate bool OperatorDelegate( bool result );
			public delegate void AdditionalDelegate( Result result, ref DNode dNode, RPNNode rpnNode );

			public OperatorDelegate Operator = null;
			public AdditionalDelegate Additional = null;
		}


		private BinaryDelegates TuneBinaryDelegates( Operator oper )
		{
			BinaryDelegates delegates = new BinaryDelegates();

			if ( oper == Operators.And )
			{
				delegates.EvaluateSecond = EvaluateSecond_And;
				delegates.Operator = And;
			}
			else if ( oper == Operators.Xor )
			{
				delegates.Operator = Xor;
			}
			else if ( oper == Operators.Or )
			{
				delegates.EvaluateSecond = EvaluateSecond_Or;
				delegates.Operator = Or;
				delegates.Alternative = Alternative_Or;
			}

			return ( delegates );
		}

		private UnaryDelegates TuneUnaryDelegates( Operator oper )
		{
			UnaryDelegates delegates = new UnaryDelegates();

			if ( oper == Operators.Not )
			{
				delegates.Operator = Not;
				delegates.Additional = Additional_Not;
			}

			return ( delegates );
		}

		#endregion



		#region Unary

		private Result EvaluateUnary( OperatorDNode dNode, OperatorRPNNode rpnNode, bool recover, UnaryDelegates delegates )
		{
			// (*) forget about subgoals-path.
			//int memStartIndex = _subgoalsStack.StartIndex;
			//_subgoalsStack.StartIndex = _subgoalsStack.Count;

			Result result = EvaluateChild( ref dNode.Child[ ChildIndex.First ], rpnNode.Child[ ChildIndex.First ], recover );

			// (*) now remember it.
			//_subgoalsStack.StartIndex = memStartIndex;


			if ( !result.FormsCycle )
				result.Reached = delegates.Operator( result.Reached );

			if ( delegates.Additional != null )
				delegates.Additional( result, ref dNode.Child[ ChildIndex.First ], rpnNode.Child[ ChildIndex.First ] );

			return ( result );
		}


		#region Not

		private bool Not( bool result )
		{
			return ( !result );
		}

		private void Additional_Not( Result result, ref DNode dNode, RPNNode rpnNode )
		{
			result.More = false;
			dNode.ClearChildren();

			if ( result.Reached )
				CreateChildren( ref dNode, rpnNode, State.Unreacheable );
		}

		#endregion

		#endregion


		#region Binary

		private Result EvaluateBinary( OperatorDNode dNode, OperatorRPNNode rpnNode, bool recover,
			BinaryDelegates delegates )
		{
			int firstIndex = ( dNode.Reverse ? ChildIndex.Second : ChildIndex.First );
			int secondIndex = ( dNode.Reverse ? ChildIndex.First : ChildIndex.Second );


			Result result = new Result();

			bool evaluateOnlySecond = false;
			for ( ; !result.Reached && result.More; )
			{
				if ( !dNode.SecondReached )
					dNode.RecoverFirst = false;

				Result first = null;
				if ( !evaluateOnlySecond )
				{
					bool recoverFirst = recover || dNode.RecoverFirst;
					first = EvaluateChild( ref dNode.Child[ firstIndex ], rpnNode.Child[ firstIndex ], recoverFirst );
				}
				else
				{
					first = new Result();
					first.More = false;
				}

				result.More = first.More;
				result.FormsCycle = first.FormsCycle;

				if ( !first.FormsCycle )
				{
					if ( delegates.EvaluateSecond == null || delegates.EvaluateSecond( first.Reached ) )
					{
						evaluateOnlySecond = !first.Reached && !first.More;

						bool recoverSecond = recover;
						Result second = EvaluateChild( ref dNode.Child[ secondIndex ], rpnNode.Child[ secondIndex ], recoverSecond );
						result.More |= second.More;
						result.FormsCycle |= second.FormsCycle;

						if ( !second.FormsCycle )
						{
							dNode.SecondReached = second.Reached;

							if ( second.Reached && !first.Reached && !recover )
							{
								dNode.Child[ firstIndex ] = null;

								first = EvaluateChild( ref dNode.Child[ firstIndex ], rpnNode.Child[ firstIndex ], false );
								result.More |= first.More;
								result.FormsCycle |= first.FormsCycle;
							}

							if ( !first.FormsCycle )
								result.Reached = delegates.Operator( first.Reached, second.Reached );
						}

						if ( !( result.Reached || evaluateOnlySecond && result.More ) )
							dNode.Child[ secondIndex ] = null;
					}
					else if ( delegates.Alternative != null )
						delegates.Alternative( result, ref dNode.Child[ secondIndex ], rpnNode.Child[ secondIndex ] );
				}
			}

			dNode.RecoverFirst = result.Reached;

			if ( !result.Reached )
				dNode.ClearChildren();

			return ( result );
		}


		#region And

		private bool And( bool first, bool second )
		{
			return ( first && second );
		}

		private bool EvaluateSecond_And( bool first )
		{
			return ( first );
		}

		#endregion

		#region Xor

		private bool Xor( bool first, bool second )
		{
			return ( first ^ second );
		}

		#endregion

		#region Or

		private bool Or( bool first, bool second )
		{
			return ( first || second );
		}

		private bool EvaluateSecond_Or( bool first )
		{
			return ( !first );
		}

		private void Alternative_Or( Result result, ref DNode dNode, RPNNode rpnNode )
		{
			result.Reached = true;
			CreateChildren( ref dNode, rpnNode, State.NotChecked );
		}

		#endregion

		#endregion

		#endregion



		#region Event handling

		#region Event Delegates

		public delegate void CompleteDelegate( Result result, ConditionDNode dTree );
		private event CompleteDelegate _eventOnComplete;

		public delegate void TraceDelegate();
		private event TraceDelegate _eventOnTrace;

		public delegate void BeginTraceDelelgate();
		private event BeginTraceDelelgate _eventOnBeginTrace;

		public delegate void RulesStackChangeDelegate( StackOfRules rulesStack );
		private event RulesStackChangeDelegate _eventOnRulesStackChange;

		public delegate void UpdateLastRuleDelegate( Rule rule );
		private event UpdateLastRuleDelegate _eventUpdateLastRule;


		public class FeedbackDelegates
		{
			public CompleteDelegate OnComplete;
			public TraceDelegate OnTrace;
			public BeginTraceDelelgate OnBeginTrace;
			public RulesStackChangeDelegate OnRulesStackChange;
			public UpdateLastRuleDelegate OnUpdateLastRule;
		}

		#endregion



		protected void OnComplete( Result result, ConditionDNode dTree )
		{
			if ( _eventOnComplete != null )
				_eventOnComplete( result, dTree );
		}

		protected void OnTrace()
		{
			if ( _eventOnTrace != null )
				_eventOnTrace();
		}

		protected void OnBeginTrace()
		{
			if ( _eventOnBeginTrace != null )
				_eventOnBeginTrace();
		}

		protected void OnUpdateLastRule()
		{
			if ( _eventUpdateLastRule != null && _rulesStack.Count > 0 )
				_eventUpdateLastRule( _rulesStack.Peek() );

			OnTrace();
		}

		protected void OnRulesStackChange()
		{
			if ( _eventOnRulesStackChange != null )
				_eventOnRulesStackChange( _rulesStack );

			OnTrace();
		}

		#endregion
	}
}
