using System;
using System.Collections.Generic;



namespace ExpertSystem.Deduction
{
	using Grammar;
	using Aux;
	using RPN;



	public class ReverseDeductor : Deductor
	{
		public ReverseDeductor( ListOfRules rules, ListOfFacts facts, FeedbackDelegates delegates )
			: base( rules, facts, delegates ) {}


		protected override Result Deduce( Condition goal, out ConditionDNode dTree )
		{
			dTree = new ConditionDNode( goal );
			return EvaluateSubgoal( dTree );
		}

		protected override Result AfterAllKnowledgesChecked( Result result, ConditionDNode dNode )
		{
			if ( dNode.State <= State.UsesRule && !result.Reached )
				result = TryDeducingFromRule( dNode );

			return ( result );
		}

		private Result TryDeducingFromRule( ConditionDNode dNode )
		{
			Condition subgoal = dNode.Subgoal;


			if ( dNode.Reached )
				dNode.ClearChildren();

			Result result = new Result();

			// (*) main subgoal.
			_subgoalsStack.Push( ( Condition )subgoal.Copy() );

			int ruleIndex = ( dNode.State == State.UsesRule ? dNode.Index + 1 : 0 );
			for ( ; ruleIndex < _rules.Count && !result.Reached; ruleIndex++ )
			{
				if ( subgoal.DeducibleFrom( _rules[ ruleIndex ] ) )
				{
					Rule usedRule = ( Rule )_rules[ ruleIndex ].Copy();
					usedRule.SetVariables( usedRule.Title, subgoal );

					// (**) temporary subgoals.
					int memorizeSubgoalsCount = _subgoalsStack.Count;

					// (***) current rule.
					_rulesStack.Push( usedRule );
					OnRulesStackChange();

					result = EvaluateChild( ref dNode.Child[ ChildIndex.First ], usedRule.Expression.RPNTree, false );

					// (***)
					_rulesStack.Pop();
					OnRulesStackChange();

					// (**)
					for ( ; _subgoalsStack.Count > memorizeSubgoalsCount; )
						_subgoalsStack.Pop();


					if ( !result.Reached )
					{
						result.FormsCycle = false;
						dNode.ClearChildren();
					}
					else if ( _rulesStack.Count > 0 )
					{
						Rule prevRule = _rulesStack.Peek();
						prevRule.SetVariables( subgoal, usedRule.Title );
						result.FormsCycle = ( prevRule.Title == subgoal );
						if ( result.FormsCycle )
						{
							result.Reached = false;
							result.More = false;
							dNode.ClearChildren();
						}
					}
				}
			}
			ruleIndex--;

			// (*)
			_subgoalsStack.Pop();


			if ( result.Reached )
			{
				dNode.State = State.UsesRule;
				dNode.Index = ruleIndex;
				OnUpdateLastRule();

				result.More |= ( ruleIndex < _rules.Count - 1 );
			}
			else
			{
				result.More = false;
			}

			Memorize( result, dNode );

			return ( result );
		}
	}
}
