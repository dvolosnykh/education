using System;
using System.Collections.Generic;



namespace ExpertSystem.Aux
{
	using Grammar;



	public class ChildIndex
	{
		public const int First = 0;
		public const int Second = 1;
		public const int Total = 2;
	}


	public class ListOf<T> : List<T>
		where T : Token
	{
		public new bool Contains( T item )
		{
			return ( this.IndexOf( item ) >= 0 );
		}

		public new int IndexOf( T searchItem )
		{
			int index = -1;
			for ( int i = 0; i < Count && index < 0; i++ )
				if ( base[ i ].Text == searchItem.Text ) index = i;

			return ( index );
		}
	}

	public class StackOf<T> : List<T>
		where T : Token
	{
		public void Push( T item )
		{
			if ( item != null )
				base.Add( item );
		}

		public T Pop()
		{
			T item = Peek();
			if ( item != null )
				base.Remove( item );
			return ( item );
		}

		public T Peek()
		{
			return ( Count > 0 ? base[ Count - 1 ] : null );
		}
	}


	public class ListOfConditions : ListOf<Condition> {}

	public class ListOfFacts : ListOfConditions
	{
		public ListOfConstants Constants = new ListOfConstants();


		public void Add( Fact fact )
		{
			base.Add( fact );
			if ( fact != null )
				Register( fact );
		}


		private void Register( Fact fact )
		{
			TermsList termsList = fact.TermsList;

			for ( int i = 0; i < termsList.Count; i++ )
			{
				Constant curConstant = termsList[ i ] as Constant;

				if ( !Constants.Contains( curConstant ) )
					Constants.Add( curConstant );
			}
		}


		public new Fact this[ int i ]
		{
			set
			{
				base[ i ] = value;
				if ( value != null )
					Register( value );
			}
			get { return ( base[ i ] as Fact ); }
		}
	}

	public class StackOfRules : StackOf<Rule> {}

	public class ListOfRules : ListOf<Rule> {}

	public class ListOfTerms : ListOf<Term> {}

	public class ListOfVariables : ListOf<Variable>
	{
		public void AddVariables( TermsList termsList )
		{
			for ( int j = 0; j < termsList.Count; j++ )
				if ( termsList[ j ] is Variable )
				{
					Variable curVariable = termsList[ j ] as Variable;

					int index = IndexOf( curVariable );

					if ( index < 0 )
						Add( curVariable );
					else
						termsList[ j ] = this[ index ];
				}
		}
	}

	public class ListOfConstants : ListOf<Constant> {}
}
