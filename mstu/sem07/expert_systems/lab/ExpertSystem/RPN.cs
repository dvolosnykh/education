using System;
using System.Collections.Generic;
using System.Diagnostics;



namespace ExpertSystem.RPN
{
	using Grammar;
	using Aux;



	#region Nodes

	public abstract class RPNNode
	{
		public bool SurroundWithBraces;
	}

	public class ConditionRPNNode : RPNNode
	{
		public Condition Condition;



		public ConditionRPNNode( Condition condition )
		{
			Condition = condition;
		}


		public override string ToString()
		{
			return ( Condition.Text );
		}
	}

	public class OperatorRPNNode : RPNNode
	{
		public Operator Operator;
		public RPNNode[] Child = new RPNNode[ ChildIndex.Total ];



		public OperatorRPNNode( Operator oper )
		{
			Operator = oper;
		}


		public override string ToString()
		{
			return ( Operator.Text );
		}
	}

	#endregion



	static class Operators
	{
		public static List<Operator> Logical = new List<Operator>();

		public static Operator Or = new Operator( Sign.Operator.Or, 1, true, true );
		public static Operator Xor = new Operator( Sign.Operator.Xor, 2, true, false );
		public static Operator And = new Operator( Sign.Operator.And, 3, true, true );
		public static Operator Not = new Operator( Sign.Operator.Not, 4, false, false );



		static Operators()
		{
			Logical.Add( Or );
			Logical.Add( Xor );
			Logical.Add( And );
			Logical.Add( Not );
		}
	}

	class OperatorStack : Stack<OperatorRPNNode> {}

	class RPNStack : Stack<RPNNode>
	{
		public RPNStack( string expr )
		{
			Build( expr );
		}


		public void Build( string expr )
		{
			Build( expr, false );
		}

		private void Build( string expr, bool surroundWithBraces )
		{
			OperatorStack operators = new OperatorStack();

			for ( ; expr.Length > 0; )
			{
				expr = expr.Trim();

				int openBracketPos = Expression.FindOpenBracket( expr );
				string strInBrackets = expr.Substring( openBracketPos );
				expr = expr.Remove( openBracketPos );

				int index;
				string strPredicate = Expression.ExtractPredicate( expr, out index );
				if ( strPredicate != string.Empty )
				{
					Condition condition = new Condition( strPredicate + strInBrackets );
					PushCondition( condition );
				}
				else
				{
					int subExprLength = strInBrackets.Length - Sign.CloseBracket.Length - Sign.OpenBracket.Length;
					string subExpr = strInBrackets.Substring( Sign.OpenBracket.Length, subExprLength );
					Build( subExpr, true );
				}

				expr = expr.Substring( 0, index );

				PushOperators( operators, ref expr );
			}

			PopAllOperators( operators );

			Peek().SurroundWithBraces = surroundWithBraces;
		}

		private void PushCondition( Condition condition )
		{
			Push( new ConditionRPNNode( condition ) );
		}

		private void PushOperators( OperatorStack operators, ref string expr )
		{
			for ( ; ; )
			{
				expr = expr.TrimEnd();
				int operatorIndex = Expression.OperatorIndex( expr );

			if ( operatorIndex < 0 ) break;

				PushOperator( operators, Operators.Logical[ operatorIndex ] );

				int newLength = expr.Length - Operators.Logical[ operatorIndex ].Text.Length;
				expr = expr.Remove( newLength );
			}
		}

		private void PushOperator( OperatorStack operators, Operator oper )
		{
			PopHigherPriorityOperators( operators, oper.Priority );
			operators.Push( new OperatorRPNNode( oper ) );
		}

		private void PopHigherPriorityOperators( OperatorStack operators, int priority )
		{
			for ( ; operators.Count > 0 && operators.Peek().Operator.Priority >= priority; )
				Push( operators.Pop() );
		}

		private void PopAllOperators( OperatorStack operators )
		{
			for ( ; operators.Count > 0; )
				Push( operators.Pop() );
		}
	}


	public static class RPNTreeBuilder
	{
		public static RPNNode Build( string expr )
		{
			return Walk( new RPNStack( expr ) );
		}

		private static RPNNode Walk( RPNStack stack )
		{
			RPNNode node = stack.Pop();

			if ( node is OperatorRPNNode )
			{
				OperatorRPNNode oper = node as OperatorRPNNode;

				oper.Child[ ChildIndex.First ] = Walk( stack );
				if ( oper.Operator.Binary )
					oper.Child[ ChildIndex.Second ] = Walk( stack );
			}

			return ( node );
		}
	}


	public static class Expression
	{
		public static string ExtractPredicate( string expr, out int index )
		{
			bool stop = false;

			int length;
			for ( length = expr.Length; !stop && length > 0; length-- )
			{
				string str = expr.Substring( 0, length );

				stop = str.EndsWith( Sign.OpenBracket );
				stop = !stop && CheckForOperator( str );
			}

			if ( stop )
				length++;

			expr = expr.Substring( length ).TrimStart();

			index = length;

			return ( expr );
		}

		private static bool CheckForOperator( string expr )
		{
			int operatorIndex = OperatorIndex( expr );
			return ( operatorIndex >= 0 );
		}

		public static int OperatorIndex( string expr )
		{
			bool found = false;

			int i;
			for ( i = 0; !found && i < Operators.Logical.Count; i++ )
				found = expr.EndsWith( Operators.Logical[ i ].Text );
			i--;

			return ( found ? i : -1 );
		}

		// unused
		private static int BracketsBalance( string expr )
		{
			int depth = 0;

			do
			{
				Advance( ref expr, ref depth );
			}
			while ( expr.Length > 0 );

			return ( depth );
		}

		public static int FindOpenBracket( string expr )
		{
			int depth = 0;

			do
			{
				Advance( ref expr, ref depth );
			}
			while ( expr.Length > 0 && depth > 0 );

			return ( expr.Length );
		}

		private static void Advance( ref string expr, ref int depth )
		{
			int length = expr.Length;

			if ( expr.EndsWith( Sign.OpenBracket ) )
			{
				depth--;
				length -= Sign.OpenBracket.Length;
			}
			else if ( expr.EndsWith( Sign.CloseBracket ) )
			{
				depth++;
				length -= Sign.CloseBracket.Length;
			}
			else
				length--;

			expr = expr.Remove( length );
		}
	}
}