using System;
using System.Collections.Generic;
using System.Diagnostics;



namespace ExpertSystem.Grammar
{
	using Aux;
	using RPN;



	#region Exceptions

	public abstract class GrammarException : Exception
	{
		public GrammarException( string message ) : base( message ) {}
	}


	public class UnexpectedDefinitionSign : GrammarException
	{
		public UnexpectedDefinitionSign( string rule )
			: base( "������ ����� ����������� \"" + Sign.Definition + "\" � �������:\n" + rule ) {}
	}

	public class NoDefinitionSign : GrammarException
	{
		public NoDefinitionSign( string rule )
			: base( "�� ������ ���� ����������� \"" + Sign.Definition + "\" � �������:\n" + rule ) {}
	}


	public class UnexpectedSplitTermsSign : GrammarException
	{
		public UnexpectedSplitTermsSign( string termsList )
			: base( "������ ����� ������������ \"" + Sign.SplitTerms + "\" � ������ ������:\n" + termsList ) {}
	}

	public class UnexpectedOperator : GrammarException
	{
		public UnexpectedOperator( string strOperator )
			: base( "���������� �������� \"" + strOperator + "\" ��� ������������ ��������" ) {}
	}

	public class UnknownOperator : GrammarException
	{
		public UnknownOperator( string strOperator )
			: base( "����������� ��������: " + strOperator ) {}
	}


	public class UnexpectedTermsListStart : GrammarException
	{
		public UnexpectedTermsListStart( string termsList )
			: base( "������ ������� ������ ������ ������ \"" + Sign.TermsListStart + "\":\n" + termsList ) {}
	}

	public class NoTermsListStart : GrammarException
	{
		public NoTermsListStart( string termsList )
			: base( "�� ������ ������� ������ ������ ���������� \"" + Sign.TermsListStart + "\":\n" + termsList ) {}
	}

	public class UnexpectedTermsListEnd : GrammarException
	{
		public UnexpectedTermsListEnd( string termsList )
			: base( "������ ������� ����� ������ ������ \"" + Sign.TermsListEnd + "\":\n" + termsList ) {}
	}

	public class NoTermsListEnd : GrammarException
	{
		public NoTermsListEnd( string termsList )
			: base( "�� ������ ������� ����� ������ ������ \"" + Sign.TermsListEnd + "\":\n" + termsList ) {}
	}

	public class UnexpectedTailAfterTermsListEnd : GrammarException
	{
		public UnexpectedTailAfterTermsListEnd( string termsList )
			: base( "������������ ������� ����� ������ ������:\n" + termsList ) {}
	}


	public class UnexpectedWhiteSpaceInPredicate : GrammarException
	{
		public UnexpectedWhiteSpaceInPredicate( string predicate )
			: base( "������������ ���������� ������� � ���������:\n" + predicate ) {}
	}

	public class UnexpectedWhiteSpaceInTerm : GrammarException
	{
		public UnexpectedWhiteSpaceInTerm( string term)
			: base( "������������ ���������� ������� � �����:\n" + term ) {}
	}


	public class EmptyConditionsList : GrammarException
	{
		public EmptyConditionsList()
			: base( "������ ������� ������ ��������� ���� �� ���� �������." ) {}
	}

	public class EmptyTermsList : GrammarException
	{
		public EmptyTermsList()
			: base( "������ ������ ������ ��������� ���� �� ���� ����." ) {}
	}


	public class EmptyPredicateName : GrammarException
	{
		public EmptyPredicateName()
			: base( "��� ��������� ������ ��������� ���� �� ���� ������." ) {}
	}

	public class EmptyTerm : GrammarException
	{
		public EmptyTerm()
			: base( "���� ������ ��������� ���� �� ���� ������." ) {}
	}

	public class UnbalancedBrackets : GrammarException
	{
		public UnbalancedBrackets( string expr, int depth )
			: base( "��������� �������� ������������������ ����������� ������: " + depth + " ��." ) {}
	}

	#endregion



	public static class Sign
    {
		public const string Definition		= ">";
		public const string SplitTerms		= ",";
		public const string TermsListStart	= "(";
		public const string TermsListEnd	= ")";
		public const string OpenBracket		= "(";
		public const string CloseBracket	= ")";
		public const string VariablePrefix	= "@";

		public static class Operator
		{
			public const string Not	= "!";
			public const string And	= "&";
			public const string Xor	= "^";
			public const string Or	= "|";
		}
	}
	


	public abstract class Token
	{
		public Token( string strProduction )
		{
			Text = strProduction;
		}


		protected static void DeleteUnnecessaryWhiteSpaces( ref string str )
		{
			str = str.Trim();

			for ( int i = str.Length - 1; ; i-- )
			{
				// find first space and skip it.
				for ( ; i >= 0 && str[ i ] != ' '; i-- );

			if ( i < 0 ) break;

				i--;

				// count the number of unnecessary spaces.
				int num = 0;
				for ( ; i >= 0 && str[ i ] == ' '; i--, num++ );

				// remove them if there are some.
				str.Remove( i + 1, num );
			}
		}


		public abstract string Text
		{
			set;
			get;
		}


		public Token Copy()
		{
			return ( Token )Activator.CreateInstance( GetType(), Text );
		}

		public override string ToString()
		{
			return ( Text );
		}


		#region Comparision

		public static bool operator == ( Token op1, Token op2 )
		{
			bool equal = false;

			bool op1null = ( object )op1 == null;
			bool op2null = ( object )op2 == null;

			if ( op1null && op2null )
				equal = true;
			else if ( op1null || op2null )
				equal = false;
			else if ( op1.GetType() == op2.GetType() )
				equal = ( op1.Text == op2.Text );

			return ( equal );
		}

		public static bool operator != ( Token op1, Token op2 )
		{
			return !( op1 == op2 );
		}

		#endregion
	}

	public class Rule : Token
	{
		public Condition Title;
		public Expression Expression;
		private ListOfVariables _variables = new ListOfVariables();
		public int ChangeID = 0;



        public Rule( string strRule ) : base( strRule ) {}


		public override string Text
		{
			set
			{
				DeleteUnnecessaryWhiteSpaces( ref value );


				string[] separator = { Sign.Definition };
				string[] parts = value.Split( separator, StringSplitOptions.None );

				if ( parts.Length > 2 )
					throw new UnexpectedDefinitionSign( value );
				else if ( parts.Length == 1 )
					throw new NoDefinitionSign( value );

				Title = new Condition( parts[ 0 ] );
				Expression = new Expression( parts[ 1 ] );

				RegisterVariables();
			}

			get
			{
				return ( Title.Text + " " + Sign.Definition + " " + Expression.Text );
			}
		}

		public void RegisterVariables()
		{
			_variables.Clear();

			RegisterExpressionVariables();
			RegisterTitleVariables();
		}

		private void RegisterTitleVariables()
		{
			_variables.AddVariables( Title.TermsList );
		}

		private void RegisterExpressionVariables()
		{
			ListOfVariables exprVariables = Expression.Variables;
			for ( int i = 0; i < exprVariables.Count; i++ )
				_variables.Add( exprVariables[ i ] );
		}


		public void SetVariables( Condition dest, Condition src )
		{
			ChangeID++;

			for ( int i = 0; i < dest.TermsList.Count; i++ )
				if ( !dest.TermsList[ i ].Defined && src.TermsList[ i ].Defined )
				{
					Variable variable = dest.TermsList[ i ] as Variable;
					variable.Value = src.TermsList[ i ].Value;
					variable.ChangeID = ChangeID;
				}
		}

		public void RevertVariables( int revertID )
		{
			for ( int i = 0; i < _variables.Count; i++ )
				if ( _variables[ i ].ChangeID > revertID )
					_variables[ i ].ChangeID = 0;

			ChangeID = revertID;
		}
	}

	public class Expression : Token
	{
		public RPNNode RPNTree;
		public ListOfVariables Variables = new ListOfVariables();


		public Expression( string strList ) : base( strList ) {}


		public override string Text
		{
			set
			{
				DeleteUnnecessaryWhiteSpaces( ref value );


				RPNTree = RPNTreeBuilder.Build( value );
				RegisterVariables( RPNTree );
			}

			get
			{
				string strList = "[ empty ]";

				if ( RPNTree != null )
					strList = OperandText( RPNTree );

				return ( strList );
			}
		}

		private string OperandText( RPNNode node )
		{
			string sublist = string.Empty;

			if ( node is OperatorRPNNode )
			{
				OperatorRPNNode operatorNode = node as OperatorRPNNode;
				int operatorIndex = Operators.Logical.IndexOf( operatorNode.Operator );
				string strOperator = Operators.Logical[ operatorIndex ].Text;

				if ( operatorNode.Operator.Binary )
				{
					string first = OperandText( operatorNode.Child[ ChildIndex.First ] );
					string second = OperandText( operatorNode.Child[ ChildIndex.Second ] );
					sublist = first + " " + strOperator + " " + second;
				}
				else
				{
					string first = OperandText( operatorNode.Child[ ChildIndex.First ] );
					sublist = strOperator + first;
				}
			}
			else if ( node is ConditionRPNNode )
			{
				ConditionRPNNode conditionNode = node as ConditionRPNNode;
				sublist = conditionNode.Condition.Text;
			}

			if ( node.SurroundWithBraces )
				sublist = Sign.OpenBracket + " " + sublist + " " + Sign.CloseBracket;

			return ( sublist );
		}


		private void RegisterVariables( RPNNode node )
		{
			if ( node is OperatorRPNNode )
			{
				OperatorRPNNode operatorNode = node as OperatorRPNNode;

				RegisterVariables( operatorNode.Child[ ChildIndex.First ] );
				if ( operatorNode.Operator.Binary )
					RegisterVariables( operatorNode.Child[ ChildIndex.Second ] );
			}
			else if ( node is ConditionRPNNode )
			{
				ConditionRPNNode conditionNode = node as ConditionRPNNode;
				Variables.AddVariables( conditionNode.Condition.TermsList );
			}
		}
	}

	public class Operator : Token
	{
		private string _text;
		public int Priority;
		public bool Binary;
		public bool Cycle;



		public Operator( string strOperator, int priority, bool binary, bool cycle )
			: base( strOperator )
		{
			Text = strOperator;
			Priority = priority;
			Binary = binary;
			Cycle = cycle;
		}


		public override string Text
		{
			set { _text = value; }
			get { return ( _text ); }
		}
	}

	public class Condition : Token
	{
		public Predicate Predicate;
		public TermsList TermsList;



		public Condition( string strCondition ) : base( strCondition ) {}


		public override string Text
		{
			set
			{
				DeleteUnnecessaryWhiteSpaces( ref value );


				string strPredicate = ExtractPredicate( value );
				Predicate = new Predicate( strPredicate );

				string strList = ExtractList( value );
				TermsList = new TermsList( strList );
			}

			get
			{
				return ( Predicate.Text + Sign.TermsListStart + " " + TermsList.Text + " " + Sign.TermsListEnd );
			}
		}

		protected string ExtractPredicate( string strDef )
		{
			string[] separator = { Sign.TermsListStart };
			string[] parts = strDef.Split( separator, StringSplitOptions.None );

			return ( parts[ 0 ] );
		}

		protected string ExtractList( string strDef )
		{
			string strList = strDef;

			int startIndex = strList.IndexOf( Sign.TermsListStart );
			if ( startIndex < 0 )
				throw new NoTermsListStart( strDef );

			int endIndex = strList.LastIndexOf( Sign.TermsListEnd );
			if ( endIndex < 0 )
				throw new NoTermsListEnd( strList );
			else if ( endIndex < strList.Length - 1 )
				throw new UnexpectedTailAfterTermsListEnd( strDef );

			startIndex++;
			endIndex--;
			strList = strList.Substring( startIndex, endIndex - startIndex + 1 );

			startIndex = strList.IndexOf( Sign.TermsListStart );
			if ( startIndex >= 0 )
				throw new UnexpectedTermsListStart( strDef );

			endIndex = strList.IndexOf( Sign.TermsListEnd );
			if ( endIndex >= 0 )
				throw new UnexpectedTermsListEnd( strDef );

			return ( strList );
		}
		

		//
		// goal's methods.
		//
		public bool EquivalentTo( Condition goal )
		{
			bool equivalent = SimpleSimiliarity( goal );

			for ( int i = 0; i < TermsList.Count && equivalent; i++ )
			{
				equivalent = ( TermsList[ i ].Defined == goal.TermsList[ i ].Defined );

				if ( equivalent && TermsList[ i ].Defined )
					equivalent = ( TermsList[ i ].Value == goal.TermsList[ i ].Value );
			}

			return ( equivalent );
		}

		public bool DeducibleFrom( Rule rule )
		{
			return rule.Title.ReplaceableBy( this );
		}

		public bool ReplaceableBy( Condition item )
		{
			bool similiar = SimpleSimiliarity( item );

			for ( int i = 0; i < TermsList.Count && similiar; i++ )
			{
				if ( TermsList[ i ].Defined )
				{
					similiar = ( item.TermsList[ i ].Defined && TermsList[ i ].Value == item.TermsList[ i ].Value );
				}
				else
				{
					// multiple references to a single variable have to be supplied with
					// either equal constants or with references to the same variable.
					for ( int j = i + 1; j < TermsList.Count && similiar; j++ )
						if ( !TermsList[ j ].Defined && TermsList[ j ] == TermsList[ i ] )
							similiar = ( item.TermsList[ j ] == item.TermsList[ i ] );
				}
			}

			return ( similiar );
		}

		private bool SimpleSimiliarity( Condition item )
		{
			return
			(
				Predicate == item.Predicate &&
				TermsList.Count == item.TermsList.Count
			);
		}

		public bool NoVariables
		{
			get
			{
				int i;
				for ( i = 0; i < TermsList.Count && TermsList[ i ].Defined; i++ );

				return ( i == TermsList.Count );
			}
		}
	}

	public class Fact : Condition
	{
		public Fact( string strFact ) : base( strFact ) {}


		public override string Text
		{
			set
			{
				DeleteUnnecessaryWhiteSpaces( ref value );

				
				string strPredicate = ExtractPredicate( value );
				Predicate = new Predicate( strPredicate );

				string strList = ExtractList( value );
				TermsList = new ConstantsList( strList );
			}

			get { return ( base.Text ); }
		}
	}

	public class Predicate : Token
	{
		protected string _name;



		public Predicate( string strPredicate ) : base( strPredicate ) {}


		public override string Text
		{
			set
			{
				DeleteUnnecessaryWhiteSpaces( ref value );


				if ( value == string.Empty )
					throw new EmptyPredicateName();

				int spaceIndex = value.IndexOf( ' ' );
				if ( spaceIndex >= 0 )
					throw new UnexpectedWhiteSpaceInPredicate( value );

				// [here] check for allowed characters.

				_name = value;
			}

			get { return ( _name ); }
		}
	}

	public class TermsList : Token
	{
		protected ListOfTerms _list = new ListOfTerms();



		public TermsList( string strList ) : base( strList ) {}


		public override string Text
		{
			set
			{
				DeleteUnnecessaryWhiteSpaces( ref value );


				string[] strTerms = ExtractTerms( value );

				// extract and save terms.
				_list.Clear();
				for ( int i = 0; i < strTerms.Length; i++ )
				{
					DeleteUnnecessaryWhiteSpaces( ref strTerms[ i ] );

					Term term;
					if ( strTerms[ i ].StartsWith( Sign.VariablePrefix ) )
						term = new Variable( strTerms[ i ].Substring( Sign.VariablePrefix.Length ) );
					else
						term = new Constant( strTerms[ i ] );

					_list.Add( term );
				}
			}

			get
			{
				string strList = "[ ����� ]";

				if ( _list.Count > 0 )
				{
					string[] strItems = new string[ _list.Count ];
					for ( int i = 0; i < _list.Count; i++ )
						strItems[ i ] = _list[ i ].Text;

					strList = string.Join( Sign.SplitTerms + " ", strItems );
				}

				return ( strList );
			}
		}


		protected string[] ExtractTerms( string strList )
		{
			// check if the list is empty.
			if ( strList == string.Empty )
				throw new EmptyTermsList();

			string[] separator = { Sign.SplitTerms };
			string[] strTerms = strList.Split( separator, StringSplitOptions.None );

			// check for blank fields.
			for ( int i = 0; i < strTerms.Length; i++ )
			{
				DeleteUnnecessaryWhiteSpaces( ref strTerms[ i ] );

				if ( strTerms[ i ] == string.Empty )
					throw new UnexpectedSplitTermsSign( strList );
			}

			return ( strTerms );
		}


		public Term this[ int i ]
		{
			get { return ( _list[ i ] ); }
			set { _list[ i ] = value; }
		}

		public int Count
		{
			get { return ( _list.Count ); }
		}
	}

	public class ConstantsList : TermsList
	{
		public ConstantsList( string strList ) : base( strList ) {}


		public override string Text
		{
			set
			{
				DeleteUnnecessaryWhiteSpaces( ref value );


				string[] strConstants = ExtractTerms( value );

				// TODO: check for variables!

				_list.Clear();
				for ( int i = 0; i < strConstants.Length; i++ )
					_list.Add( new Constant( strConstants[ i ] ) );
			}

			get { return ( base.Text ); }
		}
	}

	public abstract class Term : Token
	{
		public string Value;



		public Term( string strTerm )
			: base( strTerm ) {}


		protected void CheckTerm( ref string strTerm )
		{
			DeleteUnnecessaryWhiteSpaces( ref strTerm );

			if ( strTerm == string.Empty )
				throw new EmptyTerm();

			int spaceIndex = strTerm.IndexOf( ' ' );
			if ( spaceIndex >= 0 )
				throw new UnexpectedWhiteSpaceInTerm( strTerm );
		}

		public abstract bool Defined
		{
			get;
		}
	}

	public class Constant : Term
	{
		public Constant( string strConstant )
			: base( strConstant ) {}


		public override string Text
		{
			set
			{
				CheckTerm( ref value );

				Value = value;
			}

			get { return ( Value ); }
		}


		public override bool Defined
		{
			get { return ( true ); }
		}
	}

	public class Variable : Term
	{
		public string Name;
		public int ChangeID = 0;



		public Variable( string strVariable )
			: base( strVariable ) {}


		public override string Text
		{
			set
			{
				CheckTerm( ref value );

				// TODO: check for allowed characters.

				Name = value;
			}

			get { return ( Defined ? Value : Sign.VariablePrefix + Name ); }
		}

		public override bool Defined
		{
			get { return ( ChangeID > 0 ); }
		}
	}
}