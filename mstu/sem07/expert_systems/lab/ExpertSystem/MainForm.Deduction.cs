using System;
using System.Windows.Forms;
using System.Threading;



namespace ExpertSystem
{
	using Grammar;
	using Aux;
	using Deduction;



	partial class MainForm
	{
		private Deductor _deductor;
		


		#region On deduction complete event

		private void OnDeductionComplete( Result result, ConditionDNode dTree )
		{
			Invoke( new Deductor.CompleteDelegate( ShowResults ), result, dTree );
			_deductor = null;
		}

		private void ShowResults( Result result, ConditionDNode dTree )
		{
			treeView.Nodes.Clear();
			if ( result.Reached )
			{
				BuildTreeView( dTree );
			}
			else
			{
				MessageBox.Show( "���� " + textInputGoal.Text + " �������.", "���������.",
					MessageBoxButtons.OK, MessageBoxIcon.Exclamation );

				Activate();
			}
		}

		private void BuildTreeView( ConditionDNode dTree )
		{
			AddNodes( dTree, treeView.Nodes );

			checkExpandTree_CheckedChanged( checkExpandTree, EventArgs.Empty );
		}

		private void AddNodes( DNode node, TreeNodeCollection viewNodes )
		{
			if ( node != null )
			{
				TreeNode viewNode = new TreeNode();

				if ( node is OperatorDNode )
				{
					OperatorDNode operatorNode = node as OperatorDNode;

					viewNode.Text = operatorNode.Operator.Text;
				}
				else if ( node is ConditionDNode )
				{
					ConditionDNode conditionNode = node as ConditionDNode;

					const string tabulation = "         ";

					string nodeText = conditionNode.Subgoal.Text;
					nodeText += tabulation;
					switch ( conditionNode.State )
					{
					case State.UsesFact:
						nodeText += "[ ���� " + ( conditionNode.Index + 1 ) + " ]";
						break;

					case State.UsesRule:
						nodeText += "[ ������� " + ( conditionNode.Index + 1 ) + " ]";
						break;

					case State.NotChecked:
						nodeText += "[ �� ����������� ]";
						break;

					case State.Unreacheable:
						nodeText += "[ ����������� ]";
						break;
					}

					viewNode.Text = nodeText;
					viewNode.Tag = conditionNode;
				}

				viewNodes.Add( viewNode );

				for ( int i = 0; i < node.Child.Length; i++ )
					AddNodes( node.Child[ i ], viewNode.Nodes );
			}
		}

		#endregion


		#region Update rules events

		private bool _wasUpdated = false;


		private void OnRuleStackChanged( StackOfRules rulesStack )
		{
			Invoke( new ReverseDeductor.RulesStackChangeDelegate( UpdateRuleStack ), rulesStack );
		}

		private void UpdateRuleStack( StackOfRules rulesStack )
		{
			if ( checkUpdateRulesStack.Checked )
			{
				lock ( listRulesStack.Items )
				{
					if ( _wasUpdated )
					{
						bool add = ( rulesStack.Count > listRulesStack.Items.Count );

						if ( add )
						{
							string strLastRule = rulesStack.Peek().Text;
							listRulesStack.Items.Add( strLastRule );
						}
						else
						{
							int lastIndex = listRulesStack.Items.Count - 1;
							listRulesStack.Items.RemoveAt( lastIndex );
						}
					}
					else
					{
						listRulesStack.Items.Clear();
						for ( int i = 0; i < rulesStack.Count; i++ )
							listRulesStack.Items.Add( rulesStack[ i ].Text );
					}
				}
			}
			else
				_wasUpdated = false;
		}


		private void OnUpdateRule( Rule rule )
		{
			Invoke( new ReverseDeductor.UpdateLastRuleDelegate( UpdateLastRule ), rule );
		}

		private void UpdateLastRule( Rule rule )
		{
			if ( checkUpdateRulesStack.Checked )
			{
				lock ( listRulesStack.Items )
				{
					listRulesStack.Items[ listRulesStack.Items.Count - 1 ] = rule.Text;
				}
			}
			else
				_wasUpdated = false;
		}

		#endregion


		#region On Trace event

		private AutoResetEvent _pause = new AutoResetEvent( false );


		private void OnTrace()
		{
			if ( checkTrace.Checked )
			{
				_pause.Reset();
				WaitHandle.WaitAny( new WaitHandle[] { _pause } );
			}
		}

		private void OnBeginTrace()
		{
			Invoke( new MethodInvoker( SetCheckTrace ) );
		}

		private void SetCheckTrace()
		{
			checkTrace.Checked = true;
		}

		#endregion
	}
}
