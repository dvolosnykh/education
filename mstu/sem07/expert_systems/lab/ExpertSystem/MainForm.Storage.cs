using System;
using System.Data.SqlClient;
using System.Windows.Forms;



namespace ExpertSystem
{
	using Grammar;
	using Storing;


	partial class MainForm
	{
		private delegate void AddTokenDelegate( string strToken, bool replace, int index );

		private Storage _storage;



		private void LoadTokens( ListBox.ObjectCollection items, Table table, AddTokenDelegate AddToken )
		{
			string[] strTokens = table.Load();

			items.Clear();
			foreach ( string strToken in strTokens )
				AddToken( strToken, false, -1 );
		}

		private void SaveTokens( Table table, ListBox.ObjectCollection items )
		{
			string[] strTokens = new string[ items.Count ];
			items.CopyTo( strTokens, 0 );
			table.Save( strTokens );
		}
	}
}
