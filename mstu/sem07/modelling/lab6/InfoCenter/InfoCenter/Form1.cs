﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


using Modelling;


namespace BaumanMetro
{
	struct DoublePair
	{
		public double Average;
		public double Dispersion;
	}

	struct IntPair
	{
		public int Average;
		public int Dispersion;
	}


	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}


		private Model _model = new Model();
		private const double _timeUnit = 0.01;
		private DoublePair _clientsArrive;
		private const int _operatorsCount = 3;
		private DoublePair[] _operatorServe = new DoublePair [ _operatorsCount ];
		private const int _computersCount = 2;
		private DoublePair[] _computerSolve = new DoublePair [ _computersCount ];
		private int _clientCount;
		private double _finishTime;


		private void buttonStart_Click( object sender, EventArgs e )
		{
			if ( ReadValues() )
			{
				//
				// entities.
				//
				List<Device> operators = new List<Device>( _operatorsCount );
				for ( int i = 0; i < operators.Capacity; i++ )
				{
					Device oper = new Device();
					operators.Add( oper );
					_model.Register( oper );
				}

				List<Device> computers = new List<Device>( _computersCount );
				for ( int i = 0; i < computers.Capacity; i++ )
				{
					Device computer = new Device();
					computers.Add( computer );
					_model.Register( computer );
				}

				//
				// blocks.
				//
				_model.Register( new Generate( _clientsArrive.Average, _clientsArrive.Dispersion ) );			// client enters info-center.
				_model.Register( new Seize( operators[ 0 ], 5 ) );												// client tries to capture 1st operator.
				_model.Register( new Advance( _operatorServe[ 0 ].Average, _operatorServe[ 0 ].Dispersion ) );	// 1st operator serves the client.
				_model.Register( new Release( operators[ 0 ] ) );												// 1st operator has been released.
				_model.Register( new Transfer( 13 ) );															// the request is sent to 1st computer.
				_model.Register( new Seize( operators[ 1 ], 9 ) );												// client tries to capture 2nd operator.
				_model.Register( new Advance( _operatorServe[ 1 ].Average, _operatorServe[ 1 ].Dispersion ) );	// 2nd operator serves the client.
				_model.Register( new Release( operators[ 1 ] ) );												// 2nd operator has been released.
				_model.Register( new Transfer( 13 ) );															// the request is sent to 1st computer.
				_model.Register( new Seize( operators[ 2 ], 21 ) );												// client tries to capture 3rd operator.
				_model.Register( new Advance( _operatorServe[ 2 ].Average, _operatorServe[ 2 ].Dispersion ) );	// 3rd operator serves the client.
				_model.Register( new Release( operators[ 2 ] ) );												// 3rd operator has been released.
				_model.Register( new Transfer( 17 ) );															// the request is sent to 2nd computer.
				_model.Register( new Seize( computers[ 0 ] ) );													// 1st computer starts performing a task.
				_model.Register( new Advance( _computerSolve[ 0 ].Average, _computerSolve[ 0 ].Dispersion ) );	// 1st computer in process.
				_model.Register( new Release( computers[ 0 ] ) );												// 1st computer has finished performin the task.
				_model.Register( new Transfer( 20 ) );															// 
				_model.Register( new Seize( computers[ 1 ] ) );													// 2nd computer starts performing a task.
				_model.Register( new Advance( _computerSolve[ 1 ].Average, _computerSolve[ 1 ].Dispersion ) );	// 2nd computer in process.
				_model.Register( new Release( computers[ 1 ] ) );												// 2nd computer has finished performing the task.
				Terminate performed = new Terminate();
				_model.Register( performed );																	// request leaves the model.
				Terminate unperformed = new Terminate();
				_model.Register( unperformed );																	// request leaves the model unperformed.
				
				if ( checkTransactCount.Checked )
					_model.EnableTransactCounter( _clientCount );
				else
					_model.DisableTransactCounter();

				if ( checkFinishTime.Checked )
					_model.EnableFinishTime( _finishTime );
				else
					_model.DisableFinishTime();

				_model.Start();

				_model.Destroy();

				MessageBox.Show( "Завершено.", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk );

				//
				// output stats.
				//
				textTotalTime.Text = _model.FinishTime.ToString( "N3" );
				double denialProbability = unperformed.TransactCount / ( double )( unperformed.TransactCount + performed.TransactCount );
				textDenialProbability.Text = denialProbability.ToString( "N3" );

				textComputer1QueueMaxLen.Text = computers[ 0 ].Queue.MaxLength.ToString();
				textComputer1LoadCoeff.Text = computers[ 0 ].LoadCoeff( _model.FinishTime ).ToString( "N3" );
				textComputer1AverageWait.Text = computers[ 0 ].AverageWait.ToString( "N3" );
				textComputer2QueueMaxLen.Text = computers[ 1 ].Queue.MaxLength.ToString();
				textComputer2LoadCoeff.Text = computers[ 1 ].LoadCoeff( _model.FinishTime ).ToString( "N3" );
				textComputer2AverageWait.Text = computers[ 1 ].AverageWait.ToString( "N3" );

				textOperator1LoadCoeff.Text = operators[ 0 ].LoadCoeff( _model.FinishTime ).ToString( "N3" );
				textOperator2LoadCoeff.Text = operators[ 1 ].LoadCoeff( _model.FinishTime ).ToString( "N3" );
				textOperator3LoadCoeff.Text = operators[ 2 ].LoadCoeff( _model.FinishTime ).ToString( "N3" );
			}
		}

		private bool ReadValues()
		{
			bool goodFormat = true;
			bool goodConditions = true;

			try
			{
				_clientsArrive.Average = Convert.ToDouble( textClientArriveAverage.Text );
				_clientsArrive.Dispersion = Convert.ToDouble( textClientArriveDispersion.Text );
				goodConditions = TestDispersion( _clientsArrive.Average, _clientsArrive.Dispersion );

				_operatorServe[ 0 ].Average = Convert.ToDouble( textOperator1ServeAverage.Text );
				_operatorServe[ 0 ].Dispersion = Convert.ToDouble( textOperator1ServeDispersion.Text );
				goodConditions = TestDispersion( _operatorServe[ 0 ].Average, _operatorServe[ 0 ].Dispersion );

				_operatorServe[ 1 ].Average = Convert.ToDouble( textOperator2ServeAverage.Text );
				_operatorServe[ 1 ].Dispersion = Convert.ToDouble( textOperator2ServeDispersion.Text );
				goodConditions = TestDispersion( _operatorServe[ 1 ].Average, _operatorServe[ 1 ].Dispersion );

				_operatorServe[ 2 ].Average = Convert.ToDouble( textOperator3ServeAverage.Text );
				_operatorServe[ 2 ].Dispersion = Convert.ToDouble( textOperator3ServeDispersion.Text );
				goodConditions = TestDispersion( _operatorServe[ 2 ].Average, _operatorServe[ 2 ].Dispersion );

				_computerSolve[ 0 ].Average = Convert.ToDouble( textComputer1Solve.Text );
				_computerSolve[ 0 ].Dispersion = 0.0;

				_computerSolve[ 1 ].Average = Convert.ToDouble( textComputer2Solve.Text );
				_computerSolve[ 1 ].Dispersion = 0.0;

				_finishTime = Convert.ToDouble( textFinishTime.Text );
				goodConditions = ( !checkFinishTime.Checked || 0.0 < _finishTime );

				_clientCount = Convert.ToInt32( textTransactCount.Text );
				goodConditions = ( !checkTransactCount.Checked || 0 <= _clientCount ); 
			}
			catch ( FormatException )
			{
				goodFormat = false;

				MessageBox.Show( "Один из параметров введён не корректно.", "Ошибка",
					MessageBoxButtons.OK, MessageBoxIcon.Error );
			}

			if ( goodFormat && !goodConditions )
			{
				MessageBox.Show( "Отклонение любой величины должно быть меньше среднего значения.", "Ошибка",
					MessageBoxButtons.OK, MessageBoxIcon.Error );
			}

			if ( goodFormat && goodConditions )
			{
				goodConditions = checkFinishTime.Checked || checkTransactCount.Checked;
				if ( !goodConditions )
				{
					MessageBox.Show( "Должно быть выбрано хотя бы одно условие завершнеия.", "Внимание",
						MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
				}
			}

			return ( goodFormat && goodConditions );
		}
		
		private bool TestDispersion( double average, double dispersion )
		{
			return ( 0.0 <= dispersion && dispersion <= average );
		}
	}
}