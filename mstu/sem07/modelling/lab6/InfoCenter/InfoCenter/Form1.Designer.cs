﻿namespace BaumanMetro
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupSetup = new System.Windows.Forms.GroupBox();
			this.groupClientsArrive = new System.Windows.Forms.GroupBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.textClientArriveDispersion = new System.Windows.Forms.TextBox();
			this.textClientArriveAverage = new System.Windows.Forms.TextBox();
			this.groupComputersSetup = new System.Windows.Forms.GroupBox();
			this.label23 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.textComputer2Solve = new System.Windows.Forms.TextBox();
			this.label24 = new System.Windows.Forms.Label();
			this.textComputer1Solve = new System.Windows.Forms.TextBox();
			this.groupOperatorsSetup = new System.Windows.Forms.GroupBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.textOperator3ServeDispersion = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.textOperator2ServeDispersion = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.textOperator3ServeAverage = new System.Windows.Forms.TextBox();
			this.textOperator1ServeDispersion = new System.Windows.Forms.TextBox();
			this.textOperator2ServeAverage = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textOperator1ServeAverage = new System.Windows.Forms.TextBox();
			this.groupStats = new System.Windows.Forms.GroupBox();
			this.label31 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.groupOperatorsStats = new System.Windows.Forms.GroupBox();
			this.label30 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.textOperator3LoadCoeff = new System.Windows.Forms.TextBox();
			this.textOperator2LoadCoeff = new System.Windows.Forms.TextBox();
			this.textOperator1LoadCoeff = new System.Windows.Forms.TextBox();
			this.groupСomputersStats = new System.Windows.Forms.GroupBox();
			this.label14 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.textComputer2AverageWait = new System.Windows.Forms.TextBox();
			this.label32 = new System.Windows.Forms.Label();
			this.label28 = new System.Windows.Forms.Label();
			this.textComputer1AverageWait = new System.Windows.Forms.TextBox();
			this.textComputer2LoadCoeff = new System.Windows.Forms.TextBox();
			this.textComputer1LoadCoeff = new System.Windows.Forms.TextBox();
			this.label27 = new System.Windows.Forms.Label();
			this.textComputer2QueueMaxLen = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.textComputer1QueueMaxLen = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.textDenialProbability = new System.Windows.Forms.TextBox();
			this.textTotalTime = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.buttonStart = new System.Windows.Forms.Button();
			this.textFinishTime = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.textTransactCount = new System.Windows.Forms.TextBox();
			this.checkFinishTime = new System.Windows.Forms.CheckBox();
			this.checkTransactCount = new System.Windows.Forms.CheckBox();
			this.groupStopConditions = new System.Windows.Forms.GroupBox();
			this.groupSetup.SuspendLayout();
			this.groupClientsArrive.SuspendLayout();
			this.groupComputersSetup.SuspendLayout();
			this.groupOperatorsSetup.SuspendLayout();
			this.groupStats.SuspendLayout();
			this.groupOperatorsStats.SuspendLayout();
			this.groupСomputersStats.SuspendLayout();
			this.groupStopConditions.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupSetup
			// 
			this.groupSetup.Controls.Add( this.groupClientsArrive );
			this.groupSetup.Controls.Add( this.groupComputersSetup );
			this.groupSetup.Controls.Add( this.groupOperatorsSetup );
			this.groupSetup.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupSetup.ForeColor = System.Drawing.Color.Red;
			this.groupSetup.Location = new System.Drawing.Point( 12, 301 );
			this.groupSetup.Name = "groupSetup";
			this.groupSetup.Size = new System.Drawing.Size( 528, 161 );
			this.groupSetup.TabIndex = 1;
			this.groupSetup.TabStop = false;
			this.groupSetup.Text = "Настройка :";
			// 
			// groupClientsArrive
			// 
			this.groupClientsArrive.Controls.Add( this.label9 );
			this.groupClientsArrive.Controls.Add( this.label21 );
			this.groupClientsArrive.Controls.Add( this.label22 );
			this.groupClientsArrive.Controls.Add( this.textClientArriveDispersion );
			this.groupClientsArrive.Controls.Add( this.textClientArriveAverage );
			this.groupClientsArrive.Location = new System.Drawing.Point( 11, 19 );
			this.groupClientsArrive.Name = "groupClientsArrive";
			this.groupClientsArrive.Size = new System.Drawing.Size( 261, 49 );
			this.groupClientsArrive.TabIndex = 3;
			this.groupClientsArrive.TabStop = false;
			this.groupClientsArrive.Text = "Прибытие клиентов :";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label9.Location = new System.Drawing.Point( 155, 22 );
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size( 24, 13 );
			this.label9.TabIndex = 10;
			this.label9.Text = "+ | -";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label21.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label21.Location = new System.Drawing.Point( 227, 22 );
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size( 30, 13 );
			this.label21.TabIndex = 9;
			this.label21.Text = "мин.";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label22.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label22.Location = new System.Drawing.Point( 9, 22 );
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size( 98, 13 );
			this.label22.TabIndex = 8;
			this.label22.Text = "Время прибытия :";
			// 
			// textClientArriveDispersion
			// 
			this.textClientArriveDispersion.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textClientArriveDispersion.Location = new System.Drawing.Point( 185, 19 );
			this.textClientArriveDispersion.Name = "textClientArriveDispersion";
			this.textClientArriveDispersion.Size = new System.Drawing.Size( 36, 20 );
			this.textClientArriveDispersion.TabIndex = 7;
			this.textClientArriveDispersion.Text = "2";
			this.textClientArriveDispersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textClientArriveAverage
			// 
			this.textClientArriveAverage.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textClientArriveAverage.Location = new System.Drawing.Point( 113, 19 );
			this.textClientArriveAverage.Name = "textClientArriveAverage";
			this.textClientArriveAverage.Size = new System.Drawing.Size( 36, 20 );
			this.textClientArriveAverage.TabIndex = 6;
			this.textClientArriveAverage.Text = "10";
			this.textClientArriveAverage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// groupComputersSetup
			// 
			this.groupComputersSetup.Controls.Add( this.label23 );
			this.groupComputersSetup.Controls.Add( this.label12 );
			this.groupComputersSetup.Controls.Add( this.label7 );
			this.groupComputersSetup.Controls.Add( this.textComputer2Solve );
			this.groupComputersSetup.Controls.Add( this.label24 );
			this.groupComputersSetup.Controls.Add( this.textComputer1Solve );
			this.groupComputersSetup.Location = new System.Drawing.Point( 11, 74 );
			this.groupComputersSetup.Name = "groupComputersSetup";
			this.groupComputersSetup.Size = new System.Drawing.Size( 261, 76 );
			this.groupComputersSetup.TabIndex = 0;
			this.groupComputersSetup.TabStop = false;
			this.groupComputersSetup.Text = "Обработка компьютерами :";
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label23.Location = new System.Drawing.Point( 9, 48 );
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size( 80, 13 );
			this.label23.TabIndex = 1;
			this.label23.Text = "Компьютер 2 :";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label12.Location = new System.Drawing.Point( 138, 48 );
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size( 30, 13 );
			this.label12.TabIndex = 9;
			this.label12.Text = "мин.";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label7.Location = new System.Drawing.Point( 138, 22 );
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size( 30, 13 );
			this.label7.TabIndex = 9;
			this.label7.Text = "мин.";
			// 
			// textComputer2Solve
			// 
			this.textComputer2Solve.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textComputer2Solve.Location = new System.Drawing.Point( 95, 45 );
			this.textComputer2Solve.Name = "textComputer2Solve";
			this.textComputer2Solve.Size = new System.Drawing.Size( 36, 20 );
			this.textComputer2Solve.TabIndex = 0;
			this.textComputer2Solve.Text = "30";
			this.textComputer2Solve.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label24.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label24.Location = new System.Drawing.Point( 9, 22 );
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size( 80, 13 );
			this.label24.TabIndex = 1;
			this.label24.Text = "Компьютер 1 :";
			// 
			// textComputer1Solve
			// 
			this.textComputer1Solve.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textComputer1Solve.Location = new System.Drawing.Point( 95, 19 );
			this.textComputer1Solve.Name = "textComputer1Solve";
			this.textComputer1Solve.Size = new System.Drawing.Size( 36, 20 );
			this.textComputer1Solve.TabIndex = 0;
			this.textComputer1Solve.Text = "15";
			this.textComputer1Solve.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// groupOperatorsSetup
			// 
			this.groupOperatorsSetup.Controls.Add( this.label11 );
			this.groupOperatorsSetup.Controls.Add( this.label6 );
			this.groupOperatorsSetup.Controls.Add( this.label2 );
			this.groupOperatorsSetup.Controls.Add( this.label10 );
			this.groupOperatorsSetup.Controls.Add( this.label5 );
			this.groupOperatorsSetup.Controls.Add( this.label3 );
			this.groupOperatorsSetup.Controls.Add( this.textOperator3ServeDispersion );
			this.groupOperatorsSetup.Controls.Add( this.label8 );
			this.groupOperatorsSetup.Controls.Add( this.textOperator2ServeDispersion );
			this.groupOperatorsSetup.Controls.Add( this.label4 );
			this.groupOperatorsSetup.Controls.Add( this.textOperator3ServeAverage );
			this.groupOperatorsSetup.Controls.Add( this.textOperator1ServeDispersion );
			this.groupOperatorsSetup.Controls.Add( this.textOperator2ServeAverage );
			this.groupOperatorsSetup.Controls.Add( this.label1 );
			this.groupOperatorsSetup.Controls.Add( this.textOperator1ServeAverage );
			this.groupOperatorsSetup.Location = new System.Drawing.Point( 278, 19 );
			this.groupOperatorsSetup.Name = "groupOperatorsSetup";
			this.groupOperatorsSetup.Size = new System.Drawing.Size( 239, 101 );
			this.groupOperatorsSetup.TabIndex = 0;
			this.groupOperatorsSetup.TabStop = false;
			this.groupOperatorsSetup.Text = "Обработка операторами :";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label11.Location = new System.Drawing.Point( 128, 74 );
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size( 24, 13 );
			this.label11.TabIndex = 1;
			this.label11.Text = "+ | -";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label6.Location = new System.Drawing.Point( 128, 48 );
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size( 24, 13 );
			this.label6.TabIndex = 1;
			this.label6.Text = "+ | -";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label2.Location = new System.Drawing.Point( 128, 22 );
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size( 24, 13 );
			this.label2.TabIndex = 1;
			this.label2.Text = "+ | -";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label10.Location = new System.Drawing.Point( 200, 74 );
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size( 30, 13 );
			this.label10.TabIndex = 9;
			this.label10.Text = "мин.";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label5.Location = new System.Drawing.Point( 200, 48 );
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size( 30, 13 );
			this.label5.TabIndex = 9;
			this.label5.Text = "мин.";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label3.Location = new System.Drawing.Point( 200, 22 );
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size( 30, 13 );
			this.label3.TabIndex = 9;
			this.label3.Text = "мин.";
			// 
			// textOperator3ServeDispersion
			// 
			this.textOperator3ServeDispersion.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textOperator3ServeDispersion.Location = new System.Drawing.Point( 158, 71 );
			this.textOperator3ServeDispersion.Name = "textOperator3ServeDispersion";
			this.textOperator3ServeDispersion.Size = new System.Drawing.Size( 36, 20 );
			this.textOperator3ServeDispersion.TabIndex = 1;
			this.textOperator3ServeDispersion.Text = "20";
			this.textOperator3ServeDispersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label8.Location = new System.Drawing.Point( 9, 74 );
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size( 71, 13 );
			this.label8.TabIndex = 1;
			this.label8.Text = "Оператор 3 :";
			// 
			// textOperator2ServeDispersion
			// 
			this.textOperator2ServeDispersion.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textOperator2ServeDispersion.Location = new System.Drawing.Point( 158, 45 );
			this.textOperator2ServeDispersion.Name = "textOperator2ServeDispersion";
			this.textOperator2ServeDispersion.Size = new System.Drawing.Size( 36, 20 );
			this.textOperator2ServeDispersion.TabIndex = 1;
			this.textOperator2ServeDispersion.Text = "10";
			this.textOperator2ServeDispersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label4.Location = new System.Drawing.Point( 9, 48 );
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size( 71, 13 );
			this.label4.TabIndex = 1;
			this.label4.Text = "Оператор 2 :";
			// 
			// textOperator3ServeAverage
			// 
			this.textOperator3ServeAverage.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textOperator3ServeAverage.Location = new System.Drawing.Point( 86, 71 );
			this.textOperator3ServeAverage.Name = "textOperator3ServeAverage";
			this.textOperator3ServeAverage.Size = new System.Drawing.Size( 36, 20 );
			this.textOperator3ServeAverage.TabIndex = 0;
			this.textOperator3ServeAverage.Text = "40";
			this.textOperator3ServeAverage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textOperator1ServeDispersion
			// 
			this.textOperator1ServeDispersion.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textOperator1ServeDispersion.Location = new System.Drawing.Point( 158, 19 );
			this.textOperator1ServeDispersion.Name = "textOperator1ServeDispersion";
			this.textOperator1ServeDispersion.Size = new System.Drawing.Size( 36, 20 );
			this.textOperator1ServeDispersion.TabIndex = 1;
			this.textOperator1ServeDispersion.Text = "5";
			this.textOperator1ServeDispersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textOperator2ServeAverage
			// 
			this.textOperator2ServeAverage.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textOperator2ServeAverage.Location = new System.Drawing.Point( 86, 45 );
			this.textOperator2ServeAverage.Name = "textOperator2ServeAverage";
			this.textOperator2ServeAverage.Size = new System.Drawing.Size( 36, 20 );
			this.textOperator2ServeAverage.TabIndex = 0;
			this.textOperator2ServeAverage.Text = "40";
			this.textOperator2ServeAverage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label1.Location = new System.Drawing.Point( 9, 22 );
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size( 71, 13 );
			this.label1.TabIndex = 1;
			this.label1.Text = "Оператор 1 :";
			// 
			// textOperator1ServeAverage
			// 
			this.textOperator1ServeAverage.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textOperator1ServeAverage.Location = new System.Drawing.Point( 86, 19 );
			this.textOperator1ServeAverage.Name = "textOperator1ServeAverage";
			this.textOperator1ServeAverage.Size = new System.Drawing.Size( 36, 20 );
			this.textOperator1ServeAverage.TabIndex = 0;
			this.textOperator1ServeAverage.Text = "20";
			this.textOperator1ServeAverage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// groupStats
			// 
			this.groupStats.Controls.Add( this.label31 );
			this.groupStats.Controls.Add( this.label13 );
			this.groupStats.Controls.Add( this.groupOperatorsStats );
			this.groupStats.Controls.Add( this.groupСomputersStats );
			this.groupStats.Controls.Add( this.textDenialProbability );
			this.groupStats.Controls.Add( this.textTotalTime );
			this.groupStats.Controls.Add( this.label20 );
			this.groupStats.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupStats.ForeColor = System.Drawing.Color.Red;
			this.groupStats.Location = new System.Drawing.Point( 12, 12 );
			this.groupStats.Name = "groupStats";
			this.groupStats.Size = new System.Drawing.Size( 528, 283 );
			this.groupStats.TabIndex = 2;
			this.groupStats.TabStop = false;
			this.groupStats.Text = "Статистика :";
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label31.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label31.Location = new System.Drawing.Point( 8, 51 );
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size( 116, 13 );
			this.label31.TabIndex = 1;
			this.label31.Text = "Вероятность отказа :";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label13.Location = new System.Drawing.Point( 8, 25 );
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size( 129, 13 );
			this.label13.TabIndex = 1;
			this.label13.Text = "Время моделирования :";
			// 
			// groupOperatorsStats
			// 
			this.groupOperatorsStats.Controls.Add( this.label30 );
			this.groupOperatorsStats.Controls.Add( this.label29 );
			this.groupOperatorsStats.Controls.Add( this.label19 );
			this.groupOperatorsStats.Controls.Add( this.label25 );
			this.groupOperatorsStats.Controls.Add( this.textOperator3LoadCoeff );
			this.groupOperatorsStats.Controls.Add( this.textOperator2LoadCoeff );
			this.groupOperatorsStats.Controls.Add( this.textOperator1LoadCoeff );
			this.groupOperatorsStats.Location = new System.Drawing.Point( 11, 203 );
			this.groupOperatorsStats.Name = "groupOperatorsStats";
			this.groupOperatorsStats.Size = new System.Drawing.Size( 506, 67 );
			this.groupOperatorsStats.TabIndex = 0;
			this.groupOperatorsStats.TabStop = false;
			this.groupOperatorsStats.Text = "Операторы :";
			// 
			// label30
			// 
			this.label30.AutoSize = true;
			this.label30.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label30.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label30.Location = new System.Drawing.Point( 322, 16 );
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size( 45, 13 );
			this.label30.TabIndex = 10;
			this.label30.Text = "Опер. 3";
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label29.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label29.Location = new System.Drawing.Point( 255, 16 );
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size( 45, 13 );
			this.label29.TabIndex = 10;
			this.label29.Text = "Опер. 2";
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label19.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label19.Location = new System.Drawing.Point( 188, 16 );
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size( 45, 13 );
			this.label19.TabIndex = 10;
			this.label19.Text = "Опер. 1";
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label25.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label25.Location = new System.Drawing.Point( 6, 40 );
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size( 132, 13 );
			this.label25.TabIndex = 1;
			this.label25.Text = "Коэффициент загрузки :";
			// 
			// textOperator3LoadCoeff
			// 
			this.textOperator3LoadCoeff.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textOperator3LoadCoeff.Location = new System.Drawing.Point( 317, 37 );
			this.textOperator3LoadCoeff.Name = "textOperator3LoadCoeff";
			this.textOperator3LoadCoeff.Size = new System.Drawing.Size( 58, 20 );
			this.textOperator3LoadCoeff.TabIndex = 0;
			this.textOperator3LoadCoeff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textOperator2LoadCoeff
			// 
			this.textOperator2LoadCoeff.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textOperator2LoadCoeff.Location = new System.Drawing.Point( 250, 37 );
			this.textOperator2LoadCoeff.Name = "textOperator2LoadCoeff";
			this.textOperator2LoadCoeff.Size = new System.Drawing.Size( 58, 20 );
			this.textOperator2LoadCoeff.TabIndex = 0;
			this.textOperator2LoadCoeff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textOperator1LoadCoeff
			// 
			this.textOperator1LoadCoeff.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textOperator1LoadCoeff.Location = new System.Drawing.Point( 183, 37 );
			this.textOperator1LoadCoeff.Name = "textOperator1LoadCoeff";
			this.textOperator1LoadCoeff.Size = new System.Drawing.Size( 58, 20 );
			this.textOperator1LoadCoeff.TabIndex = 0;
			this.textOperator1LoadCoeff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// groupСomputersStats
			// 
			this.groupСomputersStats.Controls.Add( this.label14 );
			this.groupСomputersStats.Controls.Add( this.label18 );
			this.groupСomputersStats.Controls.Add( this.textComputer2AverageWait );
			this.groupСomputersStats.Controls.Add( this.label32 );
			this.groupСomputersStats.Controls.Add( this.label28 );
			this.groupСomputersStats.Controls.Add( this.textComputer1AverageWait );
			this.groupСomputersStats.Controls.Add( this.textComputer2LoadCoeff );
			this.groupСomputersStats.Controls.Add( this.textComputer1LoadCoeff );
			this.groupСomputersStats.Controls.Add( this.label27 );
			this.groupСomputersStats.Controls.Add( this.textComputer2QueueMaxLen );
			this.groupСomputersStats.Controls.Add( this.label17 );
			this.groupСomputersStats.Controls.Add( this.textComputer1QueueMaxLen );
			this.groupСomputersStats.Controls.Add( this.label15 );
			this.groupСomputersStats.Location = new System.Drawing.Point( 11, 79 );
			this.groupСomputersStats.Name = "groupСomputersStats";
			this.groupСomputersStats.Size = new System.Drawing.Size( 506, 118 );
			this.groupСomputersStats.TabIndex = 0;
			this.groupСomputersStats.TabStop = false;
			this.groupСomputersStats.Text = "Компьютеры :";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label14.Location = new System.Drawing.Point( 255, 17 );
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size( 46, 13 );
			this.label14.TabIndex = 11;
			this.label14.Text = "Комп. 2";
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label18.Location = new System.Drawing.Point( 188, 17 );
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size( 46, 13 );
			this.label18.TabIndex = 10;
			this.label18.Text = "Комп. 1";
			// 
			// textComputer2AverageWait
			// 
			this.textComputer2AverageWait.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textComputer2AverageWait.Location = new System.Drawing.Point( 250, 88 );
			this.textComputer2AverageWait.Name = "textComputer2AverageWait";
			this.textComputer2AverageWait.Size = new System.Drawing.Size( 58, 20 );
			this.textComputer2AverageWait.TabIndex = 0;
			this.textComputer2AverageWait.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label32
			// 
			this.label32.AutoSize = true;
			this.label32.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label32.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label32.Location = new System.Drawing.Point( 6, 91 );
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size( 144, 13 );
			this.label32.TabIndex = 1;
			this.label32.Text = "Среднее время ожидания :";
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label28.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label28.Location = new System.Drawing.Point( 314, 39 );
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size( 85, 13 );
			this.label28.TabIndex = 9;
			this.label28.Text = "задание(ия, ий)";
			// 
			// textComputer1AverageWait
			// 
			this.textComputer1AverageWait.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textComputer1AverageWait.Location = new System.Drawing.Point( 183, 88 );
			this.textComputer1AverageWait.Name = "textComputer1AverageWait";
			this.textComputer1AverageWait.Size = new System.Drawing.Size( 58, 20 );
			this.textComputer1AverageWait.TabIndex = 0;
			this.textComputer1AverageWait.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textComputer2LoadCoeff
			// 
			this.textComputer2LoadCoeff.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textComputer2LoadCoeff.Location = new System.Drawing.Point( 250, 62 );
			this.textComputer2LoadCoeff.Name = "textComputer2LoadCoeff";
			this.textComputer2LoadCoeff.Size = new System.Drawing.Size( 58, 20 );
			this.textComputer2LoadCoeff.TabIndex = 0;
			this.textComputer2LoadCoeff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textComputer1LoadCoeff
			// 
			this.textComputer1LoadCoeff.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textComputer1LoadCoeff.Location = new System.Drawing.Point( 183, 62 );
			this.textComputer1LoadCoeff.Name = "textComputer1LoadCoeff";
			this.textComputer1LoadCoeff.Size = new System.Drawing.Size( 58, 20 );
			this.textComputer1LoadCoeff.TabIndex = 0;
			this.textComputer1LoadCoeff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label27.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label27.Location = new System.Drawing.Point( 6, 65 );
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size( 132, 13 );
			this.label27.TabIndex = 1;
			this.label27.Text = "Коэффициент загрузки :";
			// 
			// textComputer2QueueMaxLen
			// 
			this.textComputer2QueueMaxLen.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textComputer2QueueMaxLen.Location = new System.Drawing.Point( 250, 36 );
			this.textComputer2QueueMaxLen.Name = "textComputer2QueueMaxLen";
			this.textComputer2QueueMaxLen.Size = new System.Drawing.Size( 58, 20 );
			this.textComputer2QueueMaxLen.TabIndex = 0;
			this.textComputer2QueueMaxLen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label17.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label17.Location = new System.Drawing.Point( 314, 91 );
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size( 30, 13 );
			this.label17.TabIndex = 9;
			this.label17.Text = "мин.";
			// 
			// textComputer1QueueMaxLen
			// 
			this.textComputer1QueueMaxLen.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textComputer1QueueMaxLen.Location = new System.Drawing.Point( 183, 36 );
			this.textComputer1QueueMaxLen.Name = "textComputer1QueueMaxLen";
			this.textComputer1QueueMaxLen.Size = new System.Drawing.Size( 58, 20 );
			this.textComputer1QueueMaxLen.TabIndex = 0;
			this.textComputer1QueueMaxLen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label15.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label15.Location = new System.Drawing.Point( 6, 39 );
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size( 167, 13 );
			this.label15.TabIndex = 1;
			this.label15.Text = "Максимальная длина очереди :";
			// 
			// textDenialProbability
			// 
			this.textDenialProbability.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textDenialProbability.Location = new System.Drawing.Point( 143, 48 );
			this.textDenialProbability.Name = "textDenialProbability";
			this.textDenialProbability.Size = new System.Drawing.Size( 74, 20 );
			this.textDenialProbability.TabIndex = 0;
			this.textDenialProbability.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textTotalTime
			// 
			this.textTotalTime.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textTotalTime.Location = new System.Drawing.Point( 143, 22 );
			this.textTotalTime.Name = "textTotalTime";
			this.textTotalTime.Size = new System.Drawing.Size( 74, 20 );
			this.textTotalTime.TabIndex = 0;
			this.textTotalTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label20.Location = new System.Drawing.Point( 223, 25 );
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size( 30, 13 );
			this.label20.TabIndex = 9;
			this.label20.Text = "мин.";
			// 
			// buttonStart
			// 
			this.buttonStart.Location = new System.Drawing.Point( 433, 519 );
			this.buttonStart.Name = "buttonStart";
			this.buttonStart.Size = new System.Drawing.Size( 107, 23 );
			this.buttonStart.TabIndex = 0;
			this.buttonStart.Text = "Запуск модели";
			this.buttonStart.UseVisualStyleBackColor = true;
			this.buttonStart.Click += new System.EventHandler( this.buttonStart_Click );
			// 
			// textFinishTime
			// 
			this.textFinishTime.Location = new System.Drawing.Point( 236, 19 );
			this.textFinishTime.Name = "textFinishTime";
			this.textFinishTime.Size = new System.Drawing.Size( 72, 20 );
			this.textFinishTime.TabIndex = 3;
			this.textFinishTime.Text = "100000";
			this.textFinishTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label16.Location = new System.Drawing.Point( 314, 22 );
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size( 30, 13 );
			this.label16.TabIndex = 9;
			this.label16.Text = "мин.";
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label26.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label26.Location = new System.Drawing.Point( 314, 48 );
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size( 27, 13 );
			this.label26.TabIndex = 9;
			this.label26.Text = "чел.";
			// 
			// textTransactCount
			// 
			this.textTransactCount.Location = new System.Drawing.Point( 236, 45 );
			this.textTransactCount.Name = "textTransactCount";
			this.textTransactCount.Size = new System.Drawing.Size( 72, 20 );
			this.textTransactCount.TabIndex = 3;
			this.textTransactCount.Text = "5000";
			this.textTransactCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// checkFinishTime
			// 
			this.checkFinishTime.AutoSize = true;
			this.checkFinishTime.Location = new System.Drawing.Point( 11, 21 );
			this.checkFinishTime.Name = "checkFinishTime";
			this.checkFinishTime.Size = new System.Drawing.Size( 219, 17 );
			this.checkFinishTime.TabIndex = 10;
			this.checkFinishTime.Text = "Продолжительность моделирования :";
			this.checkFinishTime.UseVisualStyleBackColor = true;
			// 
			// checkTransactCount
			// 
			this.checkTransactCount.AutoSize = true;
			this.checkTransactCount.Checked = true;
			this.checkTransactCount.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkTransactCount.Location = new System.Drawing.Point( 11, 47 );
			this.checkTransactCount.Name = "checkTransactCount";
			this.checkTransactCount.Size = new System.Drawing.Size( 141, 17 );
			this.checkTransactCount.TabIndex = 10;
			this.checkTransactCount.Text = "Количество клиентов :";
			this.checkTransactCount.UseVisualStyleBackColor = true;
			// 
			// groupStopConditions
			// 
			this.groupStopConditions.Controls.Add( this.textFinishTime );
			this.groupStopConditions.Controls.Add( this.checkTransactCount );
			this.groupStopConditions.Controls.Add( this.label16 );
			this.groupStopConditions.Controls.Add( this.textTransactCount );
			this.groupStopConditions.Controls.Add( this.checkFinishTime );
			this.groupStopConditions.Controls.Add( this.label26 );
			this.groupStopConditions.Location = new System.Drawing.Point( 12, 468 );
			this.groupStopConditions.Name = "groupStopConditions";
			this.groupStopConditions.Size = new System.Drawing.Size( 350, 74 );
			this.groupStopConditions.TabIndex = 11;
			this.groupStopConditions.TabStop = false;
			this.groupStopConditions.Text = "Условия завершения :";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 555, 554 );
			this.Controls.Add( this.groupStopConditions );
			this.Controls.Add( this.buttonStart );
			this.Controls.Add( this.groupStats );
			this.Controls.Add( this.groupSetup );
			this.Name = "Form1";
			this.Text = "Модель информационного центра";
			this.groupSetup.ResumeLayout( false );
			this.groupClientsArrive.ResumeLayout( false );
			this.groupClientsArrive.PerformLayout();
			this.groupComputersSetup.ResumeLayout( false );
			this.groupComputersSetup.PerformLayout();
			this.groupOperatorsSetup.ResumeLayout( false );
			this.groupOperatorsSetup.PerformLayout();
			this.groupStats.ResumeLayout( false );
			this.groupStats.PerformLayout();
			this.groupOperatorsStats.ResumeLayout( false );
			this.groupOperatorsStats.PerformLayout();
			this.groupСomputersStats.ResumeLayout( false );
			this.groupСomputersStats.PerformLayout();
			this.groupStopConditions.ResumeLayout( false );
			this.groupStopConditions.PerformLayout();
			this.ResumeLayout( false );

		}

		#endregion

		private System.Windows.Forms.GroupBox groupSetup;
		private System.Windows.Forms.TextBox textOperator1ServeAverage;
		private System.Windows.Forms.GroupBox groupOperatorsSetup;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textOperator1ServeDispersion;
		private System.Windows.Forms.GroupBox groupClientsArrive;
		private System.Windows.Forms.TextBox textClientArriveDispersion;
		private System.Windows.Forms.TextBox textClientArriveAverage;
		private System.Windows.Forms.GroupBox groupStats;
		private System.Windows.Forms.Button buttonStart;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.GroupBox groupСomputersStats;
		private System.Windows.Forms.TextBox textFinishTime;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.TextBox textTransactCount;
		private System.Windows.Forms.CheckBox checkFinishTime;
		private System.Windows.Forms.CheckBox checkTransactCount;
		private System.Windows.Forms.GroupBox groupStopConditions;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox textComputer1QueueMaxLen;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.TextBox textComputer1LoadCoeff;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox textTotalTime;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.TextBox textComputer1AverageWait;
		private System.Windows.Forms.GroupBox groupOperatorsStats;
		private System.Windows.Forms.TextBox textOperator1LoadCoeff;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textOperator3ServeDispersion;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox textOperator2ServeDispersion;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textOperator3ServeAverage;
		private System.Windows.Forms.TextBox textOperator2ServeAverage;
		private System.Windows.Forms.GroupBox groupComputersSetup;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.TextBox textComputer2Solve;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.TextBox textComputer1Solve;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textComputer2AverageWait;
		private System.Windows.Forms.TextBox textComputer2LoadCoeff;
		private System.Windows.Forms.TextBox textComputer2QueueMaxLen;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox textOperator3LoadCoeff;
		private System.Windows.Forms.TextBox textOperator2LoadCoeff;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.TextBox textDenialProbability;
	}
}

