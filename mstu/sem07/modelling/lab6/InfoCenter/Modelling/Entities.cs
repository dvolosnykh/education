using System;
using System.Collections.Generic;
using System.Diagnostics;



namespace Modelling
{
	public class TransactQueue : Queue<Transact>
	{
		public int MaxLength = 0;

		public new void Enqueue( Transact item )
		{
			base.Enqueue( item );
			if ( Count > MaxLength )
				MaxLength = Count;
		}

		public new void Clear()
		{
			MaxLength = 0;
			base.Clear();
		}

		public bool Empty
		{
			get { return ( Count == 0 ); }
		}
	}


	public abstract class Entity
	{
		public int Index;


		public virtual void Reset() {}
	}

	public class EntityList : List<Entity> {}


	public abstract class Container : Entity
	{
		public TransactQueue Queue = new TransactQueue();
		protected ValuesList _wait = new ValuesList();
		protected int _counter;
		protected double _lastChange;


		public void BlockUp( Transact transact )
		{
			transact.State = State.Blocked;
			Queue.Enqueue( transact );
		}


		public override void Reset()
		{
			Queue.Clear();
			_wait.Clear();
			_counter = 0;
			_lastChange = 0.0;
		}


		protected abstract void RegisterChanges( double curTime );
		public abstract double LoadCoeff( double totalTime );


		public double AverageWait
		{
			get { return ( _wait.TotalSum / _counter ); }
		}
	}

	public class Storage : Container
	{
		private TransactList _stored;
		private double[] _work;


		public Storage( int capacity )
		{
			_stored = new TransactList( capacity );
			_work = new double [ capacity ];

			Reset();
		}


		public void Enter( Transact transact )
		{
			_counter++;

			RegisterChanges( transact.DispatchTime );
			_stored.Add( transact );

			transact.BlockIndex++;
		}

		public Transact Leave( Transact transact )
		{
			Transact activated = null;

			if ( _stored.Contains( transact ) )
			{
				RegisterChanges( transact.DispatchTime );
				_stored.Remove( transact );

				if ( !Queue.Empty )
				{
					activated = Queue.Dequeue();
					activated.State = State.Active;
					_wait.Add( transact.DispatchTime - activated.DispatchTime );
					activated.DispatchTime = transact.DispatchTime;

					Enter( activated );
				}
			}

			return ( activated );
		}

		protected override void RegisterChanges( double curTime )
		{
			if ( !_stored.Empty )
				_work[ _stored.Count - 1 ] += curTime - _lastChange;
				
			_lastChange = curTime;
		}

		public override double LoadCoeff( double totalTime )
		{
			double load = 0.0;
			for ( int i = 0; i < _work.Length; i++ )
				load += _work[ i ] * ( i + 1 );

			load /= _work.Length * totalTime;

			return ( load );
		}

		public override void Reset()
		{
			base.Reset();
			_stored.Clear();

			for ( int i = 0; i < _work.Length; i++ )
				_work[ i ] = 0.0;
		}

		public bool Full
		{
			get { return ( _stored.Full ); }
		}

		public bool Empty
		{
			get { return ( _stored.Empty ); }
		}
	}
	
	public class Device : Container
	{
		private Transact _owner;
		private double _work;


		public Device()
		{
			Reset();
		}


		public void Seize( Transact transact )
		{
			_counter++;

			RegisterChanges( transact.DispatchTime );
			_owner = transact;

			transact.BlockIndex++;
		}

		public Transact Release( Transact transact )
		{
			Transact activated = null;

			if ( transact == _owner )
			{
				RegisterChanges( transact.DispatchTime );
				_owner = null;

				if ( !Queue.Empty )
				{
					activated = Queue.Dequeue();
					activated.State = State.Active;
					_wait.Add( transact.DispatchTime - activated.DispatchTime );
					activated.DispatchTime = transact.DispatchTime;

					Seize( activated );
				}
			}

			return ( activated );
		}

		protected override void RegisterChanges( double curTime )
		{
			if ( _owner != null )
				_work += curTime - _lastChange;

			_lastChange = curTime;
		}

		public override double LoadCoeff( double totalTime )
		{
			return ( _work / totalTime );
		}

		public override void Reset()
		{
			base.Reset();
			_owner = null;
			_work = 0.0;
		}

		public bool Free
		{
			get { return ( _owner == null ); }
		}
	}
}
