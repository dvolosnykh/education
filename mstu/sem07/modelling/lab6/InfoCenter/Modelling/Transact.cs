using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling
{
	public enum State
	{
		Delayed,
		Active,
		Blocked,
		Terminated
	}


	public struct TransactID
	{
		public int GeneratorIndex;
		public int Transact;
		public int Split;


		public TransactID( int generatorIndex, int id )
			: this( generatorIndex, id, 0 ) {}

		public TransactID( int generatorIndex, int id, int splitID )
		{
			GeneratorIndex = generatorIndex;
			Transact = id;
			Split = splitID;
		}


		public override string ToString()
		{
			return ( GeneratorIndex + "." + Transact + "." + Split );
		}
	}


	public class Transact
	{
		public bool New;
		public TransactID ID;
		public int BlockIndex;
		public double DispatchTime;
		public State State;


		public Transact( double dispatchTime, TransactID id )
		{
			DispatchTime = dispatchTime;
			ID = id;

			New = ( ID.Split == 0 );
			if ( New )
			{
				BlockIndex = ID.GeneratorIndex + 1;
				State = State.Delayed;
			}
		}


		public int GeneratedBy
		{
			get { return ( ID.GeneratorIndex ); }
		}

		public override string ToString()
		{
			return ( ID.ToString() + " : " + DispatchTime + " : " + BlockIndex );
		}
	}



	class TransactComparer : IComparer<Transact>
	{
		public int Compare( Transact tr1, Transact tr2 )
		{
			return Math.Sign( tr1.DispatchTime - tr2.DispatchTime );
		}
	}

	public class TransactList : List<Transact>
	{
		private TransactComparer _tc = new TransactComparer();


		public TransactList()
			: base() {}

		public TransactList( int capacity )
			: base( capacity ) {}


		public void SortInsert( Transact item )
		{
			int result = BinarySearch( item, _tc );
			int index = ( result >= 0 ? result : ~result );
			Insert( index, item );
		}


		public bool Full
		{
			get { return ( Count == Capacity ); }
		}

		public bool Empty
		{
			get { return ( Count == 0 ); }
		}
	}
}
