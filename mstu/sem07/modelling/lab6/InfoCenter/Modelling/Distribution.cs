using System;
using System.Collections.Generic;
using System.Text;

namespace Modelling
{
	public abstract class Distribution
	{
		protected AbstractRandom _rand;

		public double MinValue;
		public double MaxValue;

		public Distribution( double minValue, double maxValue )
		{
			MinValue = minValue;
			MaxValue = maxValue;
		}

		public double NextDouble()
		{
			return _rand.NextDouble( MinValue, MaxValue );
		}

		public int NextInt()
		{
			return _rand.Next( ( int )MinValue, ( int )MaxValue + 1 );
		}
	}

	public class Poisson : Distribution
	{
		public Poisson( double lambda, double minValue, double maxValue )
			: base( minValue, maxValue )
		{
			_rand = new RandomPoisson( lambda );
		}
	}

	public class Regular : Distribution
	{
		public Regular( double minValue, double maxValue )
			: base( minValue, maxValue )
		{
			_rand = new RandomRegular();
		}
	}
}
