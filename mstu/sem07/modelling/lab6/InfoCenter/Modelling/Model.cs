using System;
using System.Collections.Generic;
using System.Diagnostics;



namespace Modelling
{
	public class Model
	{
		private EntityList _entities = new EntityList();
		private BlockList _blocks = new BlockList();
		private TransactList _currentList = new TransactList();
		private TransactList _futureList = new TransactList();
		private Manager _manager = new Manager();


		public Model()
		{
			_manager.CurrentList = _currentList;
			_manager.FutureList = _futureList;
			_manager.Blocks = _blocks;
		}


		public void Start()
		{
			PreReset();

			for ( _manager.ModelTime = 0.0; !_manager.Finished; )
				_manager.Dispatch();

			PostReset();
		}

		public void Register( Block block )
		{
			block.CurrentList = _currentList;
			block.FutureList = _futureList;
			block.Index = _blocks.Count;
			_blocks.Add( block );

			if ( block is Generate )
				block.Execute();
		}

		public void Register( Entity entity )
		{
			entity.Index = _entities.Count;
			_entities.Add( entity );
		}

		private void PreReset()
		{
			foreach( Block block in _blocks )
				block.Reset();

			foreach ( Entity entity in _entities )
				entity.Reset();
		}

		private void PostReset()
		{
			_currentList.Clear();
			_futureList.Clear();
		}


		public void EnableFinishTime( double finishTime )
		{
			_manager.FinishTime = finishTime;
		}

		public void DisableFinishTime()
		{
			_manager.FinishTime = -1;
		}

		public void EnableTransactCounter( int transactCount )
		{
			_manager.TransactCounterEnabled = true;
			_manager.TransactCount = transactCount;
		}

		public void DisableTransactCounter()
		{
			_manager.TransactCounterEnabled = false;
		}

		public void Destroy()
		{
			_blocks.Clear();
			_entities.Clear();
		}

		public double FinishTime
		{
			get { return ( _manager.ModelTime ); }
		}
	}
}
