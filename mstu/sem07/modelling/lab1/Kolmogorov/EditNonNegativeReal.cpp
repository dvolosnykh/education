// EditNonNegativeReal.cpp : implementation file
//

#include "stdafx.h"
#include "Kolmogorov.h"
#include "EditNonNegativeReal.h"


// CEditNonNegativeReal

IMPLEMENT_DYNAMIC(CEditNonNegativeReal, CEdit)

CEditNonNegativeReal::CEditNonNegativeReal()
{
}

CEditNonNegativeReal::~CEditNonNegativeReal()
{
}


BEGIN_MESSAGE_MAP(CEditNonNegativeReal, CEdit)
	ON_WM_KILLFOCUS()
END_MESSAGE_MAP()



double CEditNonNegativeReal::GetValue() const
{
	CString strVal;
	GetWindowText( strVal );
	return _tcstod( strVal, NULL );
}


// CEditNonNegativeReal message handlers

void CEditNonNegativeReal::OnKillFocus( CWnd * pNewWnd )
{
	CEdit::OnKillFocus( pNewWnd );

	if ( pNewWnd != GetParent() )
	{
		CString strVal;
		GetWindowText( strVal );
		LPTSTR pEnd;
		const double value = _tcstod( strVal, &pEnd );

		if ( value < 0.0 || _tcscmp( pEnd, _T( "" ) ) != 0 )
		{
			const CString text = _T( "��������� ������ ������ ���� ��������������� ������������ ������." );
			MessageBox( text, _T( "������" ), MB_OK | MB_ICONERROR );

			SetFocus();
			SetSel( 0, GetWindowTextLengthW() );
		}
	}
}