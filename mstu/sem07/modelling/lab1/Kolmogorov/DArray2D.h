#ifndef DARRAY2D_H
#define DARRAY2D_H


#include "DArray.h"


//==================== INTERFACE =====================

template< class T >
class CTDArray2D : protected CTDArray< T >
{
protected:
	LONG _height;
	LONG _width;

public:
	CTDArray2D() { Init(); };
	CTDArray2D( const LONG new_height, const LONG new_width, const bool make_null )
		{ Init(); Alloc( new_height, new_width, make_null ); };
	virtual ~CTDArray2D() { Free(); };

	unsigned GetByteSize() const { return CTDArray< T >::GetByteSize(); };
	LONG GetSize() const { return CTDArray< T >::GetSize(); };

	const T * const GetPtr() const { return CTDArray< T >::GetPtr(); };
	void SetPtr( T * const new_ptr ) { CTDArray< T >::SetPtr( new_ptr ); };

	LONG GetHeight() const { return ( _height ); };
	LONG GetWidth() const { return ( _width ); };

	bool IsEmpty() const { return CTDArray< T >::IsEmpty(); };
	void Init() { CTDArray< T >::Init(); _width = _height = 0; };
	void Reset( const T value ) { CTDArray< T >::Reset( value ); };
	void Reset( const LONG dest_h, const LONG dest_w, const LONG count_h, const LONG count_w, const T value );
	void Alloc( const LONG new_height, const LONG new_width, const bool make_null );
	void Copy( const CTDArray2D< T > & src );
	virtual void Free() { CTDArray< T >::Free(); };

	T * operator [] ( const LONG index ) { return ( _ptr + index * _width ); };
	const T * operator [] ( const LONG index ) const { return ( _ptr + index * _width ); };
};

//==================== IMPLEMENTATION ====================

template< class T >
void CTDArray2D< T >::Reset( const LONG dest_h, const LONG dest_w, const LONG count_h, const LONG count_w, const T value )
{
	for ( register LONG i = 0, index = dest_h * _width + dest_w;
		i < count_h; i++, index += _width - count_w )
	{
		for ( register LONG j = 0; j < count_w; j++, index++ )
            ptr[ index ] = value;
	}
}

template< class T >
void CTDArray2D< T >::Alloc( const LONG new_height, const LONG new_width, const bool make_null )
{
	CTDArray< T >::Alloc( new_height * new_width, make_null );
	_height = new_height, _width = new_width;
}

template< class T >
void CTDArray2D< T >::Copy( const CTDArray2D< T > & src )
{
	if ( src.GetSize() != 0 )
		Alloc( src.GetHeight(), src.GetWidth(), false );

	memcpy( ptr, src.ptr, size * sizeof( T ) );
}

#endif