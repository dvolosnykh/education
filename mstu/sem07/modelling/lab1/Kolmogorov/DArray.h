#ifndef DARRAY_H
#define DARRAY_H

//==================== INTERFACE =====================

template< class T >
class CTDArray
{
protected:
	T * _ptr;
	LONG _size;

public:
	CTDArray() { Init(); };
	CTDArray( const LONG new_size, const bool make_null ) { Init(); Alloc( new_size, make_null ); };
	virtual ~CTDArray() { Free(); };

	unsigned GetByteSize() const { return ( _size * sizeof( T ) ); };
	LONG GetSize() const { return ( _size ); };

	const T * const GetPtr() const { return ( _ptr ); };
	void SetPtr( T * const new_ptr ) { _ptr = new_ptr; };

	bool IsEmpty() const { return ( _ptr == NULL ); };
	void Init() { _ptr = NULL, _size = 0; };
	void Reset( const T value );
	void Reset( const LONG from, const LONG to, const T value );
	void Alloc( const LONG new_size, const bool make_null );
	void Copy( const CTDArray< T > & src );
	virtual void Free();

	T & operator [] ( const LONG index ) { return ( _ptr[ index ] ); };
	const T & operator [] ( const LONG index ) const { return ( _ptr[ index ] ); };
};

//==================== IMPLEMENTATION ====================

template< class T >
void CTDArray< T >::Reset( const T value )
{
	for ( register LONG i = 0; i < _size; i++ )
		_ptr[ i ] = value;
}

template< class T >
void CTDArray< T >::Reset( const LONG from, const LONG to, const T value )
{
	for ( register LONG i = from; i <= to; i++ )
		_ptr[ i ] = value;
}

template< class T >
void CTDArray< T >::Alloc( const LONG new_size, const bool make_null )
{
	if ( _size != new_size )
	{
		Free();
		_ptr = new T [ _size = new_size ];
	}

	if ( make_null )
		memset( _ptr, 0, _size * sizeof( T ) );
}

template< class T >
void CTDArray< T >::Copy( const CTDArray< T > & src )
{
	if ( src.GetSize() != 0 )
		Alloc( src.GetSize(), false );

	memcpy( _ptr, src._ptr, _size * sizeof( T ) );
}

template< class T >
void CTDArray< T >::Free()
{
	if ( !IsEmpty() )
	{
		delete [] _ptr;
		Init();
	}
}

#endif