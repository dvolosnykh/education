// KolmogorovDlg.h : header file
//

#pragma once

#include "EditNonNegativeReal.h"
#include "DArray.h"
#include "DArray2D.h"


#define MAX_STATES_NUM                  10
#define IDC_EDIT_INTENSITY_MIN          1100
#define IDC_EDIT_INTENSITY_MAX			IDC_EDIT_INTENSITY_MIN + MAX_STATES_NUM * MAX_STATES_NUM - 1
#define IDC_EDIT_PROBABILITY_MIN		IDC_EDIT_INTENSITY_MAX + 1
#define IDC_EDIT_PROBABILITY_MAX		IDC_EDIT_PROBABILITY + MAX_STATES_NUM - 1




// CKolmogorovDlg dialog
class CKolmogorovDlg : public CDialog
{
// Construction
public:
	CKolmogorovDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_KOLMOGOROV_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	int _statesNum;
	CTDArray2D< CEditNonNegativeReal > _edtIntensity;
	CTDArray< CEdit > _edtProbability;
	CTDArray< CStatic > _stcTopIndex;
	CTDArray< CStatic > _stcBottomIndex;
	CTDArray< CStatic > _stcLeftIndex;
	CTDArray< CStatic > _stcRightIndex;


// Implementation
protected:
	HICON m_hIcon;

	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnEnChangeEditStatesNum();
	afx_msg void OnMenuAbout();
	afx_msg void OnBnClickedButtonCalc();

	DECLARE_MESSAGE_MAP()
private:
	bool IsConnected();
	bool CheckConnectedness( CTDArray< bool > & link, const int cur );
};
