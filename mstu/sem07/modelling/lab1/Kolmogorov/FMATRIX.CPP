#include "stdafx.h"

#include "fmatrix.h"



double ** new_matrix( const unsigned m, const unsigned n )
{
	double ** A = new double * [ m ];
	for ( register unsigned i = 0; i < m; i++ )
		A[ i ] = new double [ n ];

	return ( A );
}

void delete_matrix( double ** A, const unsigned m )
{
	for ( register unsigned i = 0; i < m; i++ )
		delete A[ i ];

	delete A;
}

void matrixcpy( double * const * const dest, const double * const * const src,
		const unsigned m, const unsigned n )
{
	for ( register unsigned i = 0; i < m; i++ )
		for ( register unsigned j = 0; j < n; j++ )
			dest[ i ][ j ] = src[ i ][ j ];
}

void columncpy( double * const * const dest, const double * const * const src,
		const unsigned j, const unsigned n )
{
	for ( register unsigned i = 0; i < n; i++ )
			dest[ i ][ j ] = src[ i ][ n ];
}