#include "stdafx.h"

#include "kramer.h"
#include "det.h"
#include "fmatrix.h"



bool kramer( const double * const * const A, double * const X, const unsigned n )
{
	double detA = det( A, n );

	const bool solvable = ( detA != 0 );

	if ( solvable ) kramer_evaluate( X, A, n, detA );

	return ( solvable );
}

void kramer_evaluate( double * const X, const double * const * const A, const unsigned n,
		const double detA )
{
	double ** tempX = new_matrix( n, n );

	for ( register unsigned j = 0; j < n; j++ )
	{
		matrixcpy( tempX, A, n, n );
		columncpy( tempX, A, j, n );

		X[ j ] = det( tempX, n ) / detA;
	}

	delete_matrix( tempX, n );
}