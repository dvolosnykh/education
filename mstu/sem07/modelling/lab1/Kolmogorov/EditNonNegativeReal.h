#pragma once


// CEditNonNegativeReal

class CEditNonNegativeReal : public CEdit
{
	DECLARE_DYNAMIC(CEditNonNegativeReal)

public:
	CEditNonNegativeReal();
	virtual ~CEditNonNegativeReal();

	double GetValue() const;

protected:
	virtual afx_msg void OnKillFocus( CWnd * pNewWnd );

	DECLARE_MESSAGE_MAP()
};


