// KolmogorovDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Kolmogorov.h"
#include "KolmogorovDlg.h"
#include "fmatrix.h"
#include "kramer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()



// CKolmogorovDlg dialog

CKolmogorovDlg::CKolmogorovDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CKolmogorovDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	_statesNum = -1;
}

void CKolmogorovDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CKolmogorovDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(IDC_EDIT_STATES_NUM, &CKolmogorovDlg::OnEnChangeEditStatesNum)
	ON_COMMAND(ID_MENU_ABOUT, &CKolmogorovDlg::OnMenuAbout)
	ON_BN_CLICKED(IDC_BUTTON_CALC, &CKolmogorovDlg::OnBnClickedButtonCalc)
END_MESSAGE_MAP()


// CKolmogorovDlg message handlers

BOOL CKolmogorovDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CSpinButtonCtrl * pSpinStatesNum = ( CSpinButtonCtrl * )GetDlgItem( IDC_SPIN_STATES_NUM );
	pSpinStatesNum->SetRange( 2, MAX_STATES_NUM );
	_statesNum = MAX_STATES_NUM / 2;
	pSpinStatesNum->SetPos( _statesNum );

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CKolmogorovDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		OnMenuAbout();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CKolmogorovDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CKolmogorovDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CKolmogorovDlg::OnEnChangeEditStatesNum()
{
	BOOL ok = _statesNum >= 0;

	int newStatesNum = -1;

	if ( ok )
	{
		BOOL translated;
		newStatesNum = GetDlgItemInt( IDC_EDIT_STATES_NUM, &translated, FALSE );

		CSpinButtonCtrl * pSpinStatesNum = ( CSpinButtonCtrl * )GetDlgItem( IDC_SPIN_STATES_NUM );
		int lower, upper;
		pSpinStatesNum->GetRange( lower, upper );

		ok = translated && ( lower <= newStatesNum && newStatesNum <= upper );
		if ( !ok )
		{
			CString text;
			text.Format( _T( "����� ��������� ������� ������ �������������� " )
				_T( "����������� ������ n: %i <= n <= %i." ), lower, upper );
			MessageBox( text, _T( "������" ), MB_OK | MB_ICONERROR );
		}
	}

	if ( ok )
	{
		// free old stuff.
		_edtIntensity.Free();
		_edtProbability.Free();
		_stcTopIndex.Free();
		_stcBottomIndex.Free();
		_stcLeftIndex.Free();
		_stcRightIndex.Free();

		// allocate new stuff.
		_statesNum = newStatesNum;

		// get styles and properties of the sample edit-control.
		CEdit * const pSampleEdit = ( CEdit * )GetDlgItem( IDC_EDIT_STATES_NUM );
		const DWORD sampleStyle = pSampleEdit->GetStyle();
		const DWORD sampleExStyle = pSampleEdit->GetExStyle();
//		HTHEME hTheme = ::GetWindowTheme( pSampleEdit->m_hWnd );
		CFont * const pSampleFont = pSampleEdit->GetFont();
		const DWORD sampleMargins = pSampleEdit->GetMargins();

		CRect sampleRect;
		pSampleEdit->GetWindowRect( &sampleRect );
		const CSize size( sampleRect.Width(), sampleRect.Height() );
		const CSize offset( size.cx - 1, size.cy - 1 );

		CStatic * const pLowerStatic = ( CStatic * )GetDlgItem( IDC_STATIC_LOWER );
		CRect lowerStaticRect;
		pLowerStatic->GetWindowRect( &lowerStaticRect );
		ScreenToClient( &lowerStaticRect );

		// create static labels for indexation.
		{
			_stcTopIndex.Alloc( _statesNum, false );
			_stcBottomIndex.Alloc( _statesNum, false );
			_stcLeftIndex.Alloc( _statesNum, false );
			_stcRightIndex.Alloc( _statesNum, false );

			const CPoint origin( lowerStaticRect.left, lowerStaticRect.bottom );

			const DWORD staticStyle = WS_VISIBLE | WS_CHILD | SS_CENTERIMAGE | SS_CENTER;

			const LONG bottomRowOffset = ( _statesNum + 1 ) * offset.cy;
			const LONG rightRowOffset = ( _statesNum + 1 ) * offset.cx;
			LONG horzOriginOffset = offset.cx;
			LONG vertOriginOffset = offset.cy;

			for ( register int i = 0; i < _statesNum; i++ )
			{
				CString strIndex;
				strIndex.Format( _T( "%i" ), i + 1 );

				CStatic & top = _stcTopIndex[ i ];
				top.Create( strIndex, staticStyle,
					CRect( origin + CSize( horzOriginOffset, 0 ), size ), this );
				top.SetFont( pSampleFont, FALSE );

				CStatic & bottom = _stcBottomIndex[ i ];
				bottom.Create( strIndex, staticStyle,
					CRect( origin + CSize( horzOriginOffset, bottomRowOffset ), size ), this );
				bottom.SetFont( pSampleFont, FALSE );

				horzOriginOffset += size.cx;

				CStatic & left = _stcLeftIndex[ i ];
				left.Create( strIndex, staticStyle,
					CRect( origin + CSize( 0, vertOriginOffset ), size ), this );
				left.SetFont( pSampleFont, FALSE );

				CStatic & right = _stcRightIndex[ i ];
				right.Create( strIndex, staticStyle,
					CRect( origin + CSize( rightRowOffset, vertOriginOffset ), size ), this );
				right.SetFont( pSampleFont, FALSE );

				vertOriginOffset += size.cy;
			}
		}

		// create edits for the elements of the matrix.
		{
			_edtIntensity.Alloc( _statesNum, _statesNum, false );

			CPoint origin = CPoint( -1, lowerStaticRect.bottom + offset.cy );
			UINT uID = IDC_EDIT_INTENSITY_MIN;

			for ( register int i = 0; i < _statesNum; i++ )
			{
				origin.x = lowerStaticRect.left + offset.cx;

				for ( register int j = 0; j < _statesNum; j++ )
				{
					DWORD curStyle = sampleStyle & ~ES_NUMBER | WS_BORDER;
					if ( i != j )
						curStyle &= ~ES_READONLY;
					else
						curStyle &= ~WS_TABSTOP;

					DWORD curExStyle = sampleExStyle & ~WS_EX_CLIENTEDGE;

					CEdit & curEdit = _edtIntensity[ i ][ j ];
					curEdit.CreateEx( curExStyle, _T( "EDIT" ), _T( "" ), curStyle,
						CRect( origin, size ), this, uID++ );

					curEdit.SetFont( pSampleFont, FALSE );
					curEdit.SetMargins( LOWORD( sampleMargins ), HIWORD( sampleMargins ) );

					origin.x += offset.cx;
				}

				origin.y += offset.cy;
			}
		}

		// move static label for probabilities.
		{
			CStatic * pStaticProb = ( CStatic * )GetDlgItem( IDC_STATIC_PROBABILITIES );
			CPoint topleft( lowerStaticRect.left, lowerStaticRect.bottom + ( _statesNum + 2 ) * offset.cy );
			pStaticProb->MoveWindow( CRect( topleft, size ), TRUE );
		}

		// create edits for the probabilities.
		{
			_edtProbability.Alloc( _statesNum, false );

			CPoint topleft( lowerStaticRect.left + offset.cx, lowerStaticRect.bottom + ( _statesNum + 2 ) * offset.cy );
			UINT uID = IDC_EDIT_PROBABILITY_MIN;

			for ( register int i = 0; i < _statesNum; i++ )
			{
				CEdit & prob = _edtProbability[ i ];
				prob.Create( sampleStyle & ~ES_NUMBER & ~WS_TABSTOP | WS_BORDER,
					CRect( topleft, size ), this, uID++ );
				prob.SetFont( pSampleFont, FALSE );
				prob.SetMargins( LOWORD( sampleMargins ), HIWORD( sampleMargins ) );

				topleft.x += offset.cx;
			}
		}

		// set focus to the first element so that all text is highlighted.
//		CEdit & first = _edtIntensity[ 0 ][ 1 ];
//		first.SetFocus();
//		first.SetSel( 0, first.GetWindowTextLengthW(), TRUE );

		// adjust dialog window size.
		CRect dlgWndRect;
		GetWindowRect( &dlgWndRect );
		CRect dlgClientRect;
		GetClientRect( &dlgClientRect );
		ClientToScreen( &dlgClientRect );

		const int borderWidth = ( dlgWndRect.Width() - dlgClientRect.Width() ) / 2;
		const int headerHeight = dlgWndRect.Height() - dlgClientRect.Height() - borderWidth;
		const int marginWidth = lowerStaticRect.left + borderWidth;

		const LONG newWndWidth = 2 * marginWidth + ( _statesNum + 2 ) * offset.cx;
		const LONG newWndHeight = headerHeight + lowerStaticRect.bottom +
			( _statesNum + 5 ) * offset.cy + marginWidth;
		MoveWindow( dlgWndRect.left, dlgWndRect.top, newWndWidth, newWndHeight, TRUE );

		// move "calculate" button.
		CButton * pBtnCalc = ( CButton * )GetDlgItem( IDC_BUTTON_CALC );
		CRect btnCalcRect;
		pBtnCalc->GetWindowRect( &btnCalcRect );
		ScreenToClient( btnCalcRect );

		const int x = newWndWidth - btnCalcRect.Width() - marginWidth - borderWidth;
		const int y = newWndHeight - btnCalcRect.Height() - marginWidth - headerHeight;
		pBtnCalc->MoveWindow( x, y, btnCalcRect.Width(), btnCalcRect.Height(), TRUE );
	}
}

void CKolmogorovDlg::OnMenuAbout()
{
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();
}

void CKolmogorovDlg::OnBnClickedButtonCalc()
{
	bool solved = false;

	double * X = new double [ _statesNum ];

	if ( IsConnected() )
	{
		double ** A = new_matrix( _statesNum, _statesNum + 1 );

		// define matrix.
		for ( register int i = 0; i < _statesNum - 1; i++ )
		{
			A[ i ][ i ] = 0.0;
			for ( register int j = 0; j < _statesNum; j++ )
				A[ i ][ i ] -= _edtIntensity[ i ][ j ].GetValue();

			for ( register int j = 0; j < _statesNum; j++ )
				if ( i != j )
					A[ i ][ j ] = _edtIntensity[ j ][ i ].GetValue();

			A[ i ][ _statesNum ] = 0.0;
		}

		for ( register int j = 0; j <= _statesNum; j++ )
			A[ _statesNum - 1 ][ j ] = 1.0;

		// solve by Kramer's method.
		solved = kramer( A, X, _statesNum );

		if ( !solved )
		{
			MessageBox( _T( "������� ��������� ����������� �� ����� ���� ������," )
				_T( " �.�. ������� ������������ ����� 0." ), _T( "������" ),
				MB_OK | MB_ICONERROR );
		}

		delete_matrix( A, _statesNum );
	}
	else
	{
		MessageBox( _T( "���� ���������������� ������� ������ ���� ������ ������� ������,\n" )
			_T( "�.�. ����� ��� ��������� ������� ������ ���� �����������������." ),
			_T( "��������" ), MB_OK | MB_ICONINFORMATION );
	}


	if ( solved )
	{
		for ( register int i = 0; i < _statesNum; i++ )
		{
			CString strVal;
			strVal.Format( _T( "%0.2lf" ), X[ i ] );
			_edtProbability[ i ].SetWindowTextW( strVal );
		}
	}
	else
	{
		for ( register int i = 0; i < _statesNum; i++ )
			_edtProbability[ i ].SetWindowTextW( _T( "---" ) );
	}

	delete X;
}

bool CKolmogorovDlg::IsConnected()
{
	CTDArray< bool > link( _statesNum, false );

	bool ok = true;
	for ( register int i = 0; ok &&  i < _statesNum; i++ )
	{
		link.Reset( false );
		link[ i ] = true;
		ok = ok && CheckConnectedness( link, i );
	}

	return ( ok );
}

bool CKolmogorovDlg::CheckConnectedness( CTDArray< bool > & link, const int cur )
{
	bool thereAreNew = false;
	CTDArray< bool > newers( _statesNum, true );

	for ( register int i = 0; i < _statesNum; i++ )
		if ( !link[ i ] )
		{
			newers[ i ] = link[ i ] = ( _edtIntensity[ cur ][ i ].GetValue() != 0.0 );
			if ( newers[ i ] )
				thereAreNew = true;
		}

	bool connected = false;

	if ( thereAreNew )
	{
		register int i;
		for ( i = 0; link[ i ] && i < _statesNum; i++ );

		connected = ( i == _statesNum );

		for ( register int i = 0; !connected && i < _statesNum; i++ )
			if ( newers[ i ] )
				connected = CheckConnectedness( link, i );
	}

	return ( connected );
}