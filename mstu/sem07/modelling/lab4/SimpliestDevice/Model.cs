using System;
using System.Collections.Generic;
using System.Diagnostics;

using Modelling;
using Modelling.Blocks;


namespace SimpliestDevice
{
	class Model
	{
		private Timer _timer = new Timer();

		public Manager Manager;
		public StatsCollector StatsCollector;
		public Generator Generator;
		public Queue Queue;
		public ServiceDevice ServiceDevice;


		public void Simulate()
		{
			for ( ; !Manager.Finished; )
				Manager.Work();
		}

		public void Init()
		{
			Manager.Timer = _timer;
			Manager.Generator = Generator;
			Manager.Queue = Queue;
			Manager.ServiceDevice = ServiceDevice;
			Manager.StatsCollector = StatsCollector;
			Manager.Init();
		}

		public void Deinit()
		{
			Manager.Deinit();
		}
	}
}
