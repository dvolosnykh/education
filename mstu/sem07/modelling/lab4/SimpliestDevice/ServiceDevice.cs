using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling.Blocks
{
	using Distributions;

	public class ServiceDevice : Block
	{
		public Distribution Distribution;

		private Request _servedRequest;

		// stats.
		private TimeList _servedIntervals = new TimeList();


		public Time Serve( Request request )
		{
			_servedRequest = request;

			float interval = Distribution.Next();
			return ( interval );
		}

		public override UnitStats GetStats()
		{
			ServiceDeviceStats stats = new ServiceDeviceStats();

			Time totalServeTime = _servedIntervals.TotalSum;
			stats.AverageServeTime = _servedIntervals.Average;
			stats.IdleTime = Timer.Now - totalServeTime;
			stats.LoadCoeff = totalServeTime / Timer.Now;

			return ( stats );
		}

		public override void Init()
		{
			_servedRequest = null;

			// stats.
			_servedIntervals.Clear();
		}

		public override void Deinit()
		{
			// stats.
			_servedIntervals.Clear();
		}


		public Request ServedRequest
		{
			get
			{
				_servedRequest.Served = Timer.Now;
				_servedIntervals.Add( _servedRequest.HasBeenServed );

				Request request = _servedRequest;
				_servedRequest = null;
				return ( request );
			}
		}

		public bool Free
		{
			get { return ( _servedRequest == null ); }
		}
	}


	public class ServiceDeviceStats : UnitStats
	{
		public Time AverageServeTime;
		public Time IdleTime;
		public float LoadCoeff;
	}
}
