using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling
{
	public class Timer
	{
		public Time Now;


		public void Start()
		{
			Now = 0;
		}

		public void Advance( Time progress )
		{
			Now += progress;
		}
	}
}
