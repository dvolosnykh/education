using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling
{
	public class Request
	{
		public Time EnteredQueue;
		public Time LeftQueue;
		public Time Served;


		public Time HasWaited
		{
			get { return ( LeftQueue - EnteredQueue ); }
		}

		public Time HasBeenServed
		{
			get { return ( Served - LeftQueue ); }
		}
	}
}
