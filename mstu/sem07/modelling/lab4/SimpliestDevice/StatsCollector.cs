using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling.Blocks
{
	public class StatsCollector : Block
	{
		public Time CollectStatsInterval;
		public Generator Generator;
		public Queue Queue;
		public ServiceDevice ServiceDevice;


		public override UnitStats GetStats()
		{
			throw new Exception( "The method should not be invoked." );
		}

		public override void Init()
		{
		}

		public override void Deinit()
		{
		}
	}
}
