namespace SimpliestDevice
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.buttonStart = new System.Windows.Forms.Button();
			this.textCollectStatsInterval = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.textFinishTime = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.spinQueueCapacity = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.textServDevB = new System.Windows.Forms.TextBox();
			this.textServDevA = new System.Windows.Forms.TextBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.textGenB = new System.Windows.Forms.TextBox();
			this.textGenLambda = new System.Windows.Forms.TextBox();
			this.textGenA = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.textQueueOptimalLen = new System.Windows.Forms.TextBox();
			this.textQueueOverflowCount = new System.Windows.Forms.TextBox();
			this.textQueueEmptinessCount = new System.Windows.Forms.TextBox();
			this.textQueueMaxLen = new System.Windows.Forms.TextBox();
			this.textQueueAvWaitTime = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.textLoadCoeff = new System.Windows.Forms.TextBox();
			this.textServDevIdleTime = new System.Windows.Forms.TextBox();
			this.textServDevAvServeTime = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.textGenAvRequestIncome = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox4.SuspendLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.spinQueueCapacity ) ).BeginInit();
			this.groupBox5.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.groupBox8.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add( this.buttonStart );
			this.groupBox1.Controls.Add( this.textCollectStatsInterval );
			this.groupBox1.Controls.Add( this.label16 );
			this.groupBox1.Controls.Add( this.textFinishTime );
			this.groupBox1.Controls.Add( this.label14 );
			this.groupBox1.Controls.Add( this.groupBox4 );
			this.groupBox1.Controls.Add( this.groupBox5 );
			this.groupBox1.Controls.Add( this.groupBox3 );
			this.groupBox1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupBox1.Location = new System.Drawing.Point( 12, 263 );
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size( 694, 182 );
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "��������� ���������� :";
			// 
			// buttonStart
			// 
			this.buttonStart.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.buttonStart.Location = new System.Drawing.Point( 596, 153 );
			this.buttonStart.Name = "buttonStart";
			this.buttonStart.Size = new System.Drawing.Size( 90, 23 );
			this.buttonStart.TabIndex = 8;
			this.buttonStart.Text = "���������";
			this.buttonStart.UseVisualStyleBackColor = true;
			this.buttonStart.Click += new System.EventHandler( this.buttonStart_Click );
			// 
			// textCollectStatsInterval
			// 
			this.textCollectStatsInterval.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textCollectStatsInterval.Location = new System.Drawing.Point( 197, 126 );
			this.textCollectStatsInterval.Name = "textCollectStatsInterval";
			this.textCollectStatsInterval.Size = new System.Drawing.Size( 75, 20 );
			this.textCollectStatsInterval.TabIndex = 7;
			this.textCollectStatsInterval.Text = "10,0";
			this.textCollectStatsInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label16.Location = new System.Drawing.Point( 6, 129 );
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size( 155, 13 );
			this.label16.TabIndex = 2;
			this.label16.Text = "�������� ����� ���������� :";
			// 
			// textFinishTime
			// 
			this.textFinishTime.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textFinishTime.Location = new System.Drawing.Point( 197, 152 );
			this.textFinishTime.Name = "textFinishTime";
			this.textFinishTime.Size = new System.Drawing.Size( 75, 20 );
			this.textFinishTime.TabIndex = 7;
			this.textFinishTime.Text = "100,0";
			this.textFinishTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label14.Location = new System.Drawing.Point( 6, 155 );
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size( 185, 13 );
			this.label14.TabIndex = 2;
			this.label14.Text = "����� ��������� ������������� :";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add( this.spinQueueCapacity );
			this.groupBox4.Controls.Add( this.label2 );
			this.groupBox4.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupBox4.Location = new System.Drawing.Point( 243, 19 );
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size( 193, 100 );
			this.groupBox4.TabIndex = 1;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "�������� ������ :";
			// 
			// spinQueueCapacity
			// 
			this.spinQueueCapacity.Location = new System.Drawing.Point( 99, 44 );
			this.spinQueueCapacity.Maximum = new decimal( new int[] {
            1000000,
            0,
            0,
            0} );
			this.spinQueueCapacity.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
			this.spinQueueCapacity.Name = "spinQueueCapacity";
			this.spinQueueCapacity.Size = new System.Drawing.Size( 58, 20 );
			this.spinQueueCapacity.TabIndex = 3;
			this.spinQueueCapacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.spinQueueCapacity.Value = new decimal( new int[] {
            10,
            0,
            0,
            0} );
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point( 33, 46 );
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size( 60, 13 );
			this.label2.TabIndex = 3;
			this.label2.Text = "������� =";
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add( this.label6 );
			this.groupBox5.Controls.Add( this.label7 );
			this.groupBox5.Controls.Add( this.textServDevB );
			this.groupBox5.Controls.Add( this.textServDevA );
			this.groupBox5.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupBox5.Location = new System.Drawing.Point( 442, 19 );
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size( 247, 100 );
			this.groupBox5.TabIndex = 0;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "������������� ������� :";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point( 36, 47 );
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size( 48, 13 );
			this.label6.TabIndex = 3;
			this.label6.Text = "[ A, B ] =";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point( 7, 20 );
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size( 237, 13 );
			this.label7.TabIndex = 2;
			this.label7.Text = "������������ ����������� ������������� :";
			// 
			// textServDevB
			// 
			this.textServDevB.Location = new System.Drawing.Point( 154, 44 );
			this.textServDevB.Name = "textServDevB";
			this.textServDevB.Size = new System.Drawing.Size( 58, 20 );
			this.textServDevB.TabIndex = 5;
			this.textServDevB.Text = "11";
			this.textServDevB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textServDevA
			// 
			this.textServDevA.Location = new System.Drawing.Point( 90, 44 );
			this.textServDevA.Name = "textServDevA";
			this.textServDevA.Size = new System.Drawing.Size( 58, 20 );
			this.textServDevA.TabIndex = 4;
			this.textServDevA.Text = "5";
			this.textServDevA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add( this.label4 );
			this.groupBox3.Controls.Add( this.label3 );
			this.groupBox3.Controls.Add( this.label1 );
			this.groupBox3.Controls.Add( this.textGenB );
			this.groupBox3.Controls.Add( this.textGenLambda );
			this.groupBox3.Controls.Add( this.textGenA );
			this.groupBox3.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupBox3.Location = new System.Drawing.Point( 6, 19 );
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size( 231, 100 );
			this.groupBox3.TabIndex = 0;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "��������� :";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point( 19, 73 );
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size( 56, 13 );
			this.label4.TabIndex = 3;
			this.label4.Text = "������ =";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point( 27, 47 );
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size( 48, 13 );
			this.label3.TabIndex = 3;
			this.label3.Text = "[ A, B ] =";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point( 7, 20 );
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size( 218, 13 );
			this.label1.TabIndex = 2;
			this.label1.Text = "������������ ������������� �������� :";
			// 
			// textGenB
			// 
			this.textGenB.Location = new System.Drawing.Point( 145, 44 );
			this.textGenB.Name = "textGenB";
			this.textGenB.Size = new System.Drawing.Size( 58, 20 );
			this.textGenB.TabIndex = 1;
			this.textGenB.Text = "7,0";
			this.textGenB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textGenLambda
			// 
			this.textGenLambda.Location = new System.Drawing.Point( 81, 70 );
			this.textGenLambda.Name = "textGenLambda";
			this.textGenLambda.Size = new System.Drawing.Size( 58, 20 );
			this.textGenLambda.TabIndex = 2;
			this.textGenLambda.Text = "2,34";
			this.textGenLambda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textGenA
			// 
			this.textGenA.Location = new System.Drawing.Point( 81, 44 );
			this.textGenA.Name = "textGenA";
			this.textGenA.Size = new System.Drawing.Size( 58, 20 );
			this.textGenA.TabIndex = 0;
			this.textGenA.Text = "3,0";
			this.textGenA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add( this.groupBox7 );
			this.groupBox2.Controls.Add( this.groupBox8 );
			this.groupBox2.Controls.Add( this.groupBox6 );
			this.groupBox2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupBox2.Location = new System.Drawing.Point( 12, 12 );
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size( 694, 236 );
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "���������� ������������� :";
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add( this.label5 );
			this.groupBox7.Controls.Add( this.label12 );
			this.groupBox7.Controls.Add( this.label11 );
			this.groupBox7.Controls.Add( this.label10 );
			this.groupBox7.Controls.Add( this.textQueueOptimalLen );
			this.groupBox7.Controls.Add( this.textQueueOverflowCount );
			this.groupBox7.Controls.Add( this.textQueueEmptinessCount );
			this.groupBox7.Controls.Add( this.textQueueMaxLen );
			this.groupBox7.Controls.Add( this.textQueueAvWaitTime );
			this.groupBox7.Controls.Add( this.label9 );
			this.groupBox7.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupBox7.Location = new System.Drawing.Point( 243, 19 );
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size( 193, 211 );
			this.groupBox7.TabIndex = 0;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "�������� ������ :";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label5.Location = new System.Drawing.Point( 6, 177 );
			this.label5.MaximumSize = new System.Drawing.Size( 104, 0 );
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size( 87, 26 );
			this.label5.TabIndex = 2;
			this.label5.Text = "����������� ����� ������� :";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point( 6, 131 );
			this.label12.MaximumSize = new System.Drawing.Size( 104, 0 );
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size( 104, 26 );
			this.label12.TabIndex = 2;
			this.label12.Text = "����� ������� ��� ������������ :";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point( 6, 95 );
			this.label11.MaximumSize = new System.Drawing.Size( 100, 0 );
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size( 100, 26 );
			this.label11.TabIndex = 2;
			this.label11.Text = "����� ������� ��� ����������� :";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point( 6, 59 );
			this.label10.MaximumSize = new System.Drawing.Size( 115, 0 );
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size( 87, 26 );
			this.label10.TabIndex = 2;
			this.label10.Text = "������������ ����� ������� :";
			// 
			// textQueueOptimalLen
			// 
			this.textQueueOptimalLen.BackColor = System.Drawing.SystemColors.Window;
			this.textQueueOptimalLen.Location = new System.Drawing.Point( 128, 182 );
			this.textQueueOptimalLen.Name = "textQueueOptimalLen";
			this.textQueueOptimalLen.ReadOnly = true;
			this.textQueueOptimalLen.Size = new System.Drawing.Size( 58, 20 );
			this.textQueueOptimalLen.TabIndex = 1;
			this.textQueueOptimalLen.TabStop = false;
			this.textQueueOptimalLen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textQueueOverflowCount
			// 
			this.textQueueOverflowCount.BackColor = System.Drawing.SystemColors.Window;
			this.textQueueOverflowCount.Location = new System.Drawing.Point( 128, 136 );
			this.textQueueOverflowCount.Name = "textQueueOverflowCount";
			this.textQueueOverflowCount.ReadOnly = true;
			this.textQueueOverflowCount.Size = new System.Drawing.Size( 58, 20 );
			this.textQueueOverflowCount.TabIndex = 1;
			this.textQueueOverflowCount.TabStop = false;
			this.textQueueOverflowCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textQueueEmptinessCount
			// 
			this.textQueueEmptinessCount.BackColor = System.Drawing.SystemColors.Window;
			this.textQueueEmptinessCount.Location = new System.Drawing.Point( 128, 100 );
			this.textQueueEmptinessCount.Name = "textQueueEmptinessCount";
			this.textQueueEmptinessCount.ReadOnly = true;
			this.textQueueEmptinessCount.Size = new System.Drawing.Size( 58, 20 );
			this.textQueueEmptinessCount.TabIndex = 1;
			this.textQueueEmptinessCount.TabStop = false;
			this.textQueueEmptinessCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textQueueMaxLen
			// 
			this.textQueueMaxLen.BackColor = System.Drawing.SystemColors.Window;
			this.textQueueMaxLen.Location = new System.Drawing.Point( 128, 64 );
			this.textQueueMaxLen.Name = "textQueueMaxLen";
			this.textQueueMaxLen.ReadOnly = true;
			this.textQueueMaxLen.Size = new System.Drawing.Size( 58, 20 );
			this.textQueueMaxLen.TabIndex = 1;
			this.textQueueMaxLen.TabStop = false;
			this.textQueueMaxLen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textQueueAvWaitTime
			// 
			this.textQueueAvWaitTime.BackColor = System.Drawing.SystemColors.Window;
			this.textQueueAvWaitTime.Location = new System.Drawing.Point( 128, 28 );
			this.textQueueAvWaitTime.Name = "textQueueAvWaitTime";
			this.textQueueAvWaitTime.ReadOnly = true;
			this.textQueueAvWaitTime.Size = new System.Drawing.Size( 58, 20 );
			this.textQueueAvWaitTime.TabIndex = 1;
			this.textQueueAvWaitTime.TabStop = false;
			this.textQueueAvWaitTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point( 6, 23 );
			this.label9.MaximumSize = new System.Drawing.Size( 130, 0 );
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size( 116, 26 );
			this.label9.TabIndex = 0;
			this.label9.Text = "������� ����� �������� � ������� :";
			// 
			// groupBox8
			// 
			this.groupBox8.Controls.Add( this.textLoadCoeff );
			this.groupBox8.Controls.Add( this.textServDevIdleTime );
			this.groupBox8.Controls.Add( this.textServDevAvServeTime );
			this.groupBox8.Controls.Add( this.label17 );
			this.groupBox8.Controls.Add( this.label15 );
			this.groupBox8.Controls.Add( this.label13 );
			this.groupBox8.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupBox8.Location = new System.Drawing.Point( 442, 19 );
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size( 247, 211 );
			this.groupBox8.TabIndex = 0;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "������������� ������� :";
			// 
			// textLoadCoeff
			// 
			this.textLoadCoeff.BackColor = System.Drawing.SystemColors.Window;
			this.textLoadCoeff.Location = new System.Drawing.Point( 166, 100 );
			this.textLoadCoeff.Name = "textLoadCoeff";
			this.textLoadCoeff.ReadOnly = true;
			this.textLoadCoeff.Size = new System.Drawing.Size( 58, 20 );
			this.textLoadCoeff.TabIndex = 1;
			this.textLoadCoeff.TabStop = false;
			this.textLoadCoeff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textServDevIdleTime
			// 
			this.textServDevIdleTime.BackColor = System.Drawing.SystemColors.Window;
			this.textServDevIdleTime.Location = new System.Drawing.Point( 166, 64 );
			this.textServDevIdleTime.Name = "textServDevIdleTime";
			this.textServDevIdleTime.ReadOnly = true;
			this.textServDevIdleTime.Size = new System.Drawing.Size( 58, 20 );
			this.textServDevIdleTime.TabIndex = 1;
			this.textServDevIdleTime.TabStop = false;
			this.textServDevIdleTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textServDevAvServeTime
			// 
			this.textServDevAvServeTime.BackColor = System.Drawing.SystemColors.Window;
			this.textServDevAvServeTime.Location = new System.Drawing.Point( 166, 29 );
			this.textServDevAvServeTime.Name = "textServDevAvServeTime";
			this.textServDevAvServeTime.ReadOnly = true;
			this.textServDevAvServeTime.Size = new System.Drawing.Size( 58, 20 );
			this.textServDevAvServeTime.TabIndex = 1;
			this.textServDevAvServeTime.TabStop = false;
			this.textServDevAvServeTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point( 22, 103 );
			this.label17.MaximumSize = new System.Drawing.Size( 140, 0 );
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size( 132, 13 );
			this.label17.TabIndex = 0;
			this.label17.Text = "����������� �������� :";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point( 23, 59 );
			this.label15.MaximumSize = new System.Drawing.Size( 140, 0 );
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size( 84, 26 );
			this.label15.TabIndex = 0;
			this.label15.Text = "����� ����� ������������ :";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point( 23, 23 );
			this.label13.MaximumSize = new System.Drawing.Size( 140, 0 );
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size( 137, 26 );
			this.label13.TabIndex = 0;
			this.label13.Text = "������� ������������ ������������ �������� :";
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add( this.textGenAvRequestIncome );
			this.groupBox6.Controls.Add( this.label8 );
			this.groupBox6.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupBox6.Location = new System.Drawing.Point( 6, 19 );
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size( 231, 211 );
			this.groupBox6.TabIndex = 0;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "��������� :";
			// 
			// textGenAvRequestIncome
			// 
			this.textGenAvRequestIncome.BackColor = System.Drawing.SystemColors.Window;
			this.textGenAvRequestIncome.Location = new System.Drawing.Point( 150, 28 );
			this.textGenAvRequestIncome.Name = "textGenAvRequestIncome";
			this.textGenAvRequestIncome.ReadOnly = true;
			this.textGenAvRequestIncome.Size = new System.Drawing.Size( 58, 20 );
			this.textGenAvRequestIncome.TabIndex = 1;
			this.textGenAvRequestIncome.TabStop = false;
			this.textGenAvRequestIncome.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point( 16, 23 );
			this.label8.MaximumSize = new System.Drawing.Size( 130, 0 );
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size( 128, 26 );
			this.label8.TabIndex = 0;
			this.label8.Text = "������� �������� ����������� �������� :";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 732, 459 );
			this.Controls.Add( this.groupBox2 );
			this.Controls.Add( this.groupBox1 );
			this.Name = "MainForm";
			this.Text = "���������� ���";
			this.groupBox1.ResumeLayout( false );
			this.groupBox1.PerformLayout();
			this.groupBox4.ResumeLayout( false );
			this.groupBox4.PerformLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.spinQueueCapacity ) ).EndInit();
			this.groupBox5.ResumeLayout( false );
			this.groupBox5.PerformLayout();
			this.groupBox3.ResumeLayout( false );
			this.groupBox3.PerformLayout();
			this.groupBox2.ResumeLayout( false );
			this.groupBox7.ResumeLayout( false );
			this.groupBox7.PerformLayout();
			this.groupBox8.ResumeLayout( false );
			this.groupBox8.PerformLayout();
			this.groupBox6.ResumeLayout( false );
			this.groupBox6.PerformLayout();
			this.ResumeLayout( false );

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textGenA;
		private System.Windows.Forms.TextBox textGenB;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.NumericUpDown spinQueueCapacity;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textGenLambda;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textServDevB;
		private System.Windows.Forms.TextBox textServDevA;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Button buttonStart;
		private System.Windows.Forms.TextBox textFinishTime;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox textCollectStatsInterval;
		private System.Windows.Forms.Label label16;
		public System.Windows.Forms.TextBox textGenAvRequestIncome;
		public System.Windows.Forms.TextBox textQueueAvWaitTime;
		public System.Windows.Forms.TextBox textQueueOverflowCount;
		public System.Windows.Forms.TextBox textQueueEmptinessCount;
		public System.Windows.Forms.TextBox textQueueMaxLen;
		public System.Windows.Forms.TextBox textServDevAvServeTime;
		public System.Windows.Forms.TextBox textServDevIdleTime;
		private System.Windows.Forms.Label label5;
		public System.Windows.Forms.TextBox textQueueOptimalLen;
		public System.Windows.Forms.TextBox textLoadCoeff;
		private System.Windows.Forms.Label label17;
	}
}

