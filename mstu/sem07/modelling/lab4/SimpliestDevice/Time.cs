using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling
{
	public class Time
	{
		public float Value;


		public Time( float time )
		{
			Value = time;
		}


		public static implicit operator float ( Time time )
		{
			return ( time.Value );
		}

		public static implicit operator Time ( float time )
		{
			return new Time( time );
		}


		public static Time Undefined
		{
			get { return ( -1 ); }
		}

		public bool Defined
		{
			get { return ( Value >= 0 ); }
		}

		public override string ToString()
		{
			return Value.ToString( "N3" );
		}
	}


	public class TimeList : List< Time >
	{
		public int MinIndex
		{
			get
			{
				int min = 0;
				for ( int i = 1; i < this.Count; i++ )
					if ( this[ i ].Defined && this[ i ] < this[ min ] )
						min = i;

				return ( min );
			}
		}

		public Time Average
		{
			get { return ( TotalSum / Count ); }
		}

		public Time TotalSum
		{
			get
			{
				Time sum = 0;
				for ( int i = 0; i < Count; i++ )
					sum += this[ i ];

				return ( sum );
			}
		}
	}
}
