using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling.Blocks
{
	using Distributions;

	public class Generator : Block
	{
		public Distribution Distribution;

		// stats.
		private TimeList _intervals = new TimeList();


		public Time Generate()
		{
			float interval = Distribution.Next();
			_intervals.Add( interval );

			return ( interval );
		}

		public override void Init()
		{
			// stats.
			_intervals.Clear();
		}

		public override void Deinit()
		{
			// stats.
			_intervals.Clear();
		}

		public override UnitStats GetStats()
		{
			GeneratorStats stats = new GeneratorStats();

			stats.AverageRequestIncome = _intervals.Average;

			return ( stats );
		}
	}


	public class GeneratorStats : UnitStats
	{
		public Time AverageRequestIncome;
	}
}
