using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling.Blocks
{
	public class Queue : Block
	{
		private List< Request > _memory = new List< Request >();

		// stats.
		private TimeList _waitIntervals = new TimeList();
		private int _maxLen;
		private int _emptinessCount;
		private int _overflowCount;


		public Queue( int capacity )
		{
			this.Capacity = capacity;
		}


		public Request Get()
		{
			Request wanted = null;

			if ( _memory.Count > 0 )
			{
				wanted = _memory[ 0 ];
				_memory.RemoveAt( 0 );

				wanted.LeftQueue = Timer.Now;
				_waitIntervals.Add( wanted.HasWaited );
			}
			else
			{
				_emptinessCount++;
			}

			return ( wanted );
		}

		public void Put( Request newbie )
		{
			if ( _memory.Count < _memory.Capacity )
			{
				newbie.EnteredQueue = Timer.Now;

				_memory.Add( newbie );

				if ( _memory.Count > _maxLen )
					_maxLen = _memory.Count;
			}
			else
			{
				_overflowCount++;
			}
		}


		public override UnitStats GetStats()
		{
			QueueStats stats = new QueueStats();

			stats.AverageWaitTime = _waitIntervals.Average;
			stats.MaxLen = _maxLen;
			stats.EmptinessCount = _emptinessCount;
			stats.OverflowCount = _overflowCount;

			return ( stats );
		}

		public override void Init()
		{
			_memory.Clear();

			// stats.
			_waitIntervals.Clear();
			_maxLen = 0;
			_emptinessCount = 0;
			_overflowCount = 0;
		}

		public override void Deinit()
		{
			_memory.Clear();

			// stats.
			_waitIntervals.Clear();
		}


		public int Capacity
		{
			get { return ( _memory.Capacity ); }
			set { _memory.Capacity = value; }
		}

		public bool Empty
		{
			get { return ( _memory.Count == 0 ); }
		}
	}


	public class QueueStats : UnitStats
	{
		public Time AverageWaitTime;
		public int MaxLen;
		public int EmptinessCount;
		public int OverflowCount;
	}
}
