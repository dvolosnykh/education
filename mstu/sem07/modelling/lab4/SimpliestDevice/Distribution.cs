using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling.Distributions
{
	public abstract class Distribution
	{
		public float A;
		public float B;


		private static long _seed = DateTime.Now.Ticks;
		protected Random _rand;


		public Distribution( float a, float b )
		{
			try
			{
				_rand = new Random( ( int )_seed );
				_seed *= 2;
			}
			catch ( OverflowException )
			{
				_seed = DateTime.Now.Ticks;
				_rand = new Random( ( int )_seed );
			}

			A = a;
			B = b;
		}

		public abstract float Next();
	}


	public class Poisson : Distribution
	{
		private const float _p = 0.001F;

		private float _lambda;

		private int _minK;
		private int _maxK;


		public Poisson( float lambda, float a, float b ) : base( a, b )
		{
			Lambda = lambda;
		}

		public override float Next()
		{
			float next = GenerateNext();

			// check bounds.
			if ( next > _maxK ) next = _maxK;
			else if ( next < _minK ) next = _minK;

			return Scale( next );
		}

		private int GenerateNext()
		{
			int n = ( int )Math.Round( _lambda / _p );

			int s = 0;
			for ( ; s == 0; )
			{
				for ( int i = 0; i < n; i++ )
				{
					float r = ( float )_rand.NextDouble();
					if ( r < _p ) s++;
				}
			}

			return ( s );
		}

		private float Scale( float s )
		{
			float k = ( B - A ) / ( _maxK - _minK );
			return ( A + k * ( s - _minK ) );
		}


		private float MaxPk()
		{
			float Pk = ( float )Math.Exp( -Lambda );

			float prevPk = Pk;

			int i;
			for ( i = 1; ; i++ )
			{
				Pk *= Lambda / i;

			if ( Pk < prevPk ) break;

				prevPk = Pk;
			}

			return ( prevPk );
		}

		private void BoundaryValues()
		{
			float maxP = MaxPk();
			float limitP = maxP / 1000.0F;

			float Pk = ( float )Math.Exp( -Lambda );
			int i;
			for ( i = 0; Pk < limitP; )
				Pk *= Lambda / ++i;

			_minK = i;


			for ( ; Pk >= limitP; )
				Pk *= Lambda / ++i;

			_maxK = i;
		}

		public float Lambda
		{
			set
			{
				_lambda = value;
				BoundaryValues();
			}
			get { return ( _lambda ); }
		}
	}

	public class Regular : Distribution
	{
		public Regular( float a, float b ) : base( a, b ) { }


		public override float Next()
		{
			float s = ( float )_rand.NextDouble();

			return Scale( s );
		}

		private float Scale( float s )
		{
			return ( A + s * ( B - A ) );
		}
	}
}