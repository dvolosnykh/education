using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling.Blocks
{
	public enum Index : int
	{
		Generator,
		ServiceDevice,
		StatsCollector,
		FinishTime,
		Max
	}

	public class Manager : Block
	{
		public Generator Generator;
		public Queue Queue;
		public ServiceDevice ServiceDevice;
		public StatsCollector StatsCollector;

		private TimeList _eventsList = new TimeList();

		public Manager()
		{
			for ( Index i = 0; i < Index.Max; i++ )
				_eventsList.Add( Time.Undefined );
		}

		public void Work()
		{
			Index index = ( Index )_eventsList.MinIndex;
			Timer.Now = _eventsList[ ( int )index ];

			switch ( index )
			{
			case Index.Generator:
				{
					Queue.Put( new Request() );

					if ( ServiceDevice.Free )
					{
						Request request = Queue.Get();
						_eventsList[ ( int )Index.ServiceDevice ] = Timer.Now + ServiceDevice.Serve( request );
					}

					_eventsList[ ( int )Index.Generator ] = Timer.Now + Generator.Generate();
				}
				break;

			case Index.ServiceDevice:
				{
					Debug.Assert( !ServiceDevice.Free );

					Request servedRequest = ServiceDevice.ServedRequest;

					Request request = Queue.Get();
					if ( request != null )
					{
						_eventsList[ ( int )Index.ServiceDevice ] = Timer.Now + ServiceDevice.Serve( request );
					}
					else
					{
						_eventsList[ ( int )Index.ServiceDevice ] = Time.Undefined;
					}
				}
				break;

			case Index.StatsCollector:
				{
					_eventsList[ ( int )Index.StatsCollector ] += StatsCollector.CollectStatsInterval;
				}
				break;

			case Index.FinishTime:
				{
				}
				break;
			}
		}

		public override void Init()
		{
			Generator.Timer = Timer;
			Generator.Init();

			Queue.Timer = Timer;
			Queue.Init();

			ServiceDevice.Timer = Timer;
			ServiceDevice.Init();

			StatsCollector.Timer = Timer;
			StatsCollector.Generator = Generator;
			StatsCollector.Queue = Queue;
			StatsCollector.ServiceDevice = ServiceDevice;
			StatsCollector.Init();

			_eventsList[ ( int )Index.Generator ] = Generator.Generate();
			_eventsList[ ( int )Index.StatsCollector ] = StatsCollector.CollectStatsInterval;

			Timer.Start();
		}

		public override void Deinit()
		{
			Generator.Deinit();
			Queue.Deinit();
			ServiceDevice.Deinit();
			StatsCollector.Deinit();
		}

		public override UnitStats GetStats()
		{
			throw new Exception( "The method should not be invoked." );
		}

		public Time FinishTime
		{
			set { _eventsList[ ( int )Index.FinishTime ] = value; }
			get { return ( _eventsList[ ( int )Index.FinishTime ] ); }
		}

		public bool Finished
		{
			get { return ( Timer.Now == _eventsList[ ( int )Index.FinishTime ] ); }
		}
	}
}
