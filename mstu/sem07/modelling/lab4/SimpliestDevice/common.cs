using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling.Blocks
{
	public abstract class Block
	{
		public Timer Timer;

		public abstract UnitStats GetStats();
		public abstract void Init();
		public abstract void Deinit();
	}

	public abstract class UnitStats { }

	public class UnitList : List< Block > { }
	public class UnitStatsList : List< UnitStats > { }
}
