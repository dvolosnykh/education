using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Modelling;
using Modelling.Blocks;
using Modelling.Distributions;


namespace SimpliestDevice
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();

			buttonStart_Click( buttonStart, EventArgs.Empty );
		}


		private Model _model;

		private void buttonStart_Click( object sender, EventArgs e )
		{
			if ( CheckInputData() )
			{
				_model = CreateModel();
				
				_model.Init();
				_model.Simulate();
				OutputStats( _model );
				_model.Deinit();

				CalcQueueOptimalLen();
			}
		}

		public void CalcQueueOptimalLen()
		{
			Time minGenTime = _model.Generator.Distribution.A;
			Time maxServeTime = _model.ServiceDevice.Distribution.B;
			Time modelTime = _model.Manager.FinishTime;

			float extraRequestsNum = maxServeTime / minGenTime - 1.0F;
			float times = ( modelTime - minGenTime ) / maxServeTime;

			int len = ( int )Math.Floor( extraRequestsNum * times ) + 1;

			textQueueOptimalLen.Text = len.ToString();
		}

		private Generator CreateGenerator()
		{
			float a = Convert.ToSingle( textGenA.Text );
			float b = Convert.ToSingle( textGenB.Text );
			float lambda = Convert.ToSingle( textGenLambda.Text );

			Generator g = new Generator();
			g.Distribution = new Poisson( lambda, a, b );

			return ( g );
		}

		private Queue CreateQueue()
		{
			int capacity = ( int )spinQueueCapacity.Value;
			Queue q = new Queue( capacity );

			return ( q );
		}

		private ServiceDevice CreateServiceDevice()
		{
			float a = Convert.ToSingle( textServDevA.Text );
			float b = Convert.ToSingle( textServDevB.Text );

			ServiceDevice sd = new ServiceDevice();
			sd.Distribution = new Regular( a, b );

			return ( sd );
		}

		private StatsCollector CreateStatsCollector()
		{
			float collectStatsInterval = Convert.ToSingle( textCollectStatsInterval.Text );

			StatsCollector sc = new StatsCollector();
			sc.CollectStatsInterval = collectStatsInterval;

			return ( sc );
		}

		private Manager CreateManager()
		{
			float finishTime = Convert.ToSingle( textFinishTime.Text );

			Manager m = new Manager();
			m.FinishTime = finishTime;

			return ( m );
		}

		private Model CreateModel()
		{
			Model model = new Model();
			model.Generator = CreateGenerator();
			model.Queue = CreateQueue();
			model.ServiceDevice = CreateServiceDevice();
			model.StatsCollector = CreateStatsCollector();
			model.Manager = CreateManager();			

			return ( model );
		}

		private void OutputGeneratorStats( Generator g )
		{
			GeneratorStats stats = ( GeneratorStats )g.GetStats();
			textGenAvRequestIncome.Text = stats.AverageRequestIncome.ToString();
		}

		private void OutputQueueStats( Queue q )
		{
			QueueStats stats = ( QueueStats )q.GetStats();
			textQueueAvWaitTime.Text = stats.AverageWaitTime.ToString();
			textQueueMaxLen.Text = stats.MaxLen.ToString();
			textQueueEmptinessCount.Text = stats.EmptinessCount.ToString();
			textQueueOverflowCount.Text = stats.OverflowCount.ToString();
		}

		private void OutputServiceDeviceStats( ServiceDevice sd )
		{
			ServiceDeviceStats stats = ( ServiceDeviceStats )sd.GetStats();
			textServDevAvServeTime.Text = stats.AverageServeTime.ToString();
			textServDevIdleTime.Text = stats.IdleTime.ToString();
			textLoadCoeff.Text = stats.LoadCoeff.ToString( "N3" );
		}

		private void OutputStats( Model model )
		{
			StatsCollector sc = model.StatsCollector;
			OutputGeneratorStats( sc.Generator );
			OutputQueueStats( sc.Queue );
			OutputServiceDeviceStats( sc.ServiceDevice );
		}

		private bool CheckInputData()
		{
			bool ok;
			string text = string.Empty;

			// generator parameters.
			try
			{
				float a = Convert.ToSingle( textGenA.Text );
				float b = Convert.ToSingle( textGenB.Text );
				float lambda = Convert.ToSingle( textGenLambda.Text );

				ok = ( a > 0 ) && ( b >= a ) && ( lambda > 0 );
			}
			catch ( FormatException )
			{
				ok = false;
			}

			if ( !ok )
			{
				text += "��������� ���������� A, B � ������ ������ ���� �������������� ������������� �������,\n" +
					"\t������ � >= B.";
			}


			// service device parameters.
			try
			{
				float a = Convert.ToSingle( textServDevA.Text );
				float b = Convert.ToSingle( textServDevB.Text );

				ok = ( a > 0 ) && ( b >= a );
			}
			catch ( FormatException )
			{
				ok = false;
			}

			if ( !ok )
			{
				if ( text != string.Empty )
					text += "\n";

				text += "��������� �������������� �������� A � B ������ ���� �������������� ������������� �������,\n" +
					"\t������ A >= B.";
			}


			// finish time.
			try
			{
				float finishTime = Convert.ToSingle( textFinishTime.Text );
				float collectStatsInterval = Convert.ToSingle( textCollectStatsInterval.Text );

				ok = ( collectStatsInterval > 0 ) && ( finishTime >= collectStatsInterval );
			}
			catch ( FormatException )
			{
				ok = false;
			}

			if ( !ok )
			{
				if ( text != string.Empty )
					text += "\n";

				text += "����� ��������� ������������� � �������� ����� ���������� ������ ���� �������������� ������������� �������,\n" +
					"\t������ �������� ����� ���������� �� ������ ��������� ����� ��������� �������������.";
			}

			// print error message if needed.
			ok = ( text == string.Empty );
			if ( !ok )
				MessageBox.Show( text, "������.", MessageBoxButtons.OK, MessageBoxIcon.Error );

			return ( ok );
		}
	}
}