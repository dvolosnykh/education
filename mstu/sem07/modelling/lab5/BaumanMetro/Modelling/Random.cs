using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace Modelling
{
	public abstract class AbstractRandom : Random
	{
		public AbstractRandom()
		{
		}

		public double NextDouble( double minValue, double maxValue )
		{
			return ( minValue + NextDouble() * ( maxValue - minValue ) );
		}
	}

	public class RandomRegular : AbstractRandom
	{
	}

	public class RandomPoisson : AbstractRandom
	{
		private const double _p = 0.001F;

		public int MinK;
		public int MaxK;
		private double _lambda;
		private RandomRegular _rand = new RandomRegular();


		public RandomPoisson( double lambda )
		{
			_lambda = lambda;
			BoundaryValues();
		}

		public override int Next()
		{
			int n = ( int )Math.Round( _lambda / _p );

			int s;
			for ( s = 0; s == 0; )
				for ( int i = 0; i < n; i++ )
				{
					double r = _rand.NextDouble();
					if ( r < _p )
						s++;
				}

			// check bounds.
			if ( s > MaxK )
				s = MaxK;
			else if ( s < MinK )
				s = MinK;

			return ( s );
		}

		public override int Next( int maxValue )
		{
 			return Next( 0, maxValue );
		}

		public override int Next( int minValue, int maxValue )
		{
			int next = minValue + ( int )( NextDouble() * ( maxValue - minValue ) );
			if ( next == maxValue )
				next--;

			return ( next );
		}

		public override double NextDouble()
		{
			return ( ( Next() - MinK ) / ( double )( MaxK - MinK ) );
		}

		private double MaxPk()
		{
			double Pk = Math.Exp( -_lambda );

			double prevPk = Pk;

			int i;
			for ( i = 1; ; i++ )
			{
				Pk *= _lambda / i;

			if ( Pk < prevPk ) break;

				prevPk = Pk;
			}

			return ( prevPk );
		}

		private void BoundaryValues()
		{
			double limitP = MaxPk() * _p;

			double Pk = Math.Exp( -_lambda );
			int i;
			for ( i = 0; Pk < limitP; )
				Pk *= _lambda / ++i;

			MinK = i;


			for ( ; Pk >= limitP; )
				Pk *= _lambda / ++i;

			MaxK = i;
		}
	}
}