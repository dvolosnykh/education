using System;
using System.Collections.Generic;
using System.Diagnostics;



namespace Modelling
{
	public class ValuesList : List<double>
	{
		public double Average
		{
			get { return ( TotalSum / Count ); }
		}

		public double TotalSum
		{
			get
			{
				double sum = 0.0f;
				for ( int i = 0; i < Count; i++ )
					sum += this[ i ];

				return ( sum );
			}
		}
	}
}
