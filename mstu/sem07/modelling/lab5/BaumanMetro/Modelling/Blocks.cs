using System;
using System.Collections.Generic;
using System.Diagnostics;



namespace Modelling
{
	public abstract class Block
	{
		public int Index;
		public bool On = true;
		public TransactList CurrentList;
		public TransactList FutureList;
		public int TransactCount;


		public void Execute()
		{
			Execute( null );
		}

		public void Execute( Transact transact )
		{
			if ( On )
			{
				TransactCount++;
				Work( transact );
			}
			else
				Skip( transact );
		}

		protected abstract void Work( Transact transact );

		private void Skip( Transact transact )
		{
			transact.BlockIndex++;
		}

		public virtual void Reset()
		{
			TransactCount = 0;
		}
	}

	public class BlockList : List<Block> {}


	public class RandomizePart
	{
		public Distribution Distribution;
		public ValuesList Values = new ValuesList();

		public RandomizePart( bool limits, double param1, double param2 )
		{
			double minValue = 0.0;
			double maxValue = 0.0;

			if ( !limits )
			{
				double average = param1;
				double dispersion = param2;
				minValue = average - dispersion;
				maxValue = average + dispersion;
			}
			else
			{
				minValue = param1;
				maxValue = param2;
			}

			Distribution = new Regular( minValue, maxValue );
		}


		public void Reset()
		{
			Values.Clear();
		}
	}

	public class TransferPart
	{
		public int Block;


		public TransferPart( int block )
		{
			Block = block;
		}


		public bool Valid
		{
			get { return ( Block >= 0 ); }
		}
	}


	public class Generate : Block
	{
		public RandomizePart RandomizePart;
		private int _maxCount;
		private int _count;
		private int _newTransactID;


		public Generate( double average )
			: this( average, 0.0 ) {}

		public Generate( double average, double dispersion )
			: this( average, dispersion, -1 ) {}

		public Generate( double average, double dispersion, int maxCount )
		{
			RandomizePart = new RandomizePart( false, average, dispersion );
			_maxCount = maxCount;

			Reset();
		}


		protected override void Work( Transact transact )
		{
			if ( Infinite || _count < _maxCount )
			{
				_count++;
				double interval = RandomizePart.Distribution.NextDouble();
				RandomizePart.Values.Add( interval );

				if ( transact != null )
					interval += transact.DispatchTime;

				TransactID id = new TransactID( Index, _newTransactID++ );
				Transact newbie = new Transact( interval, id );
				FutureList.SortInsert( newbie );
			}
		}


		private bool Infinite
		{
			get { return ( _maxCount < 0 ); }
		}


		public override void Reset()
		{
			base.Reset();
 			RandomizePart.Reset();
			_count = 0;
			_newTransactID = 0;
		}
	}

	public class Advance : Block
	{
		public RandomizePart RandomizePart;


		public Advance( double average )
			: this( average, 0.0 ) {}

		public Advance( double average, double dispersion )
		{
			RandomizePart = new RandomizePart( false, average, dispersion );

			Reset();
		}


		protected override void Work( Transact transact )
		{
			double advanceTime = RandomizePart.Distribution.NextDouble();
			RandomizePart.Values.Add( advanceTime );

			if ( advanceTime > 0 )
			{
				transact.DispatchTime += advanceTime;
				transact.State = State.Delayed;
				FutureList.SortInsert( transact );
			}

			transact.BlockIndex++;
		}


		public override void Reset()
		{
			base.Reset();
 			RandomizePart.Reset();
		}
	}

	public class Split : Block
	{
		public RandomizePart RandomizePart;
		public TransferPart TransferPart;
		private int _splitID;


		public Split( int average )
			: this( average, 0 ) {}

		public Split( int average, int dispersion )
			: this( average, dispersion, -1 ) {}

		public Split( int average, int dispersion, int transferBlock )
		{
			RandomizePart = new RandomizePart( false, average, dispersion );
			TransferPart = new TransferPart( transferBlock );

			Reset();
		}


		protected override void Work( Transact transact )
		{
			int number = RandomizePart.Distribution.NextInt();
			RandomizePart.Values.Add( number );

			for ( int i = 1; i <= number; i++ )
			{
				TransactID id = new TransactID( transact.ID.GeneratorIndex, transact.ID.Transact, ++_splitID );
				Transact copy = new Transact( transact.DispatchTime, id );
				copy.BlockIndex = ( TransferPart.Valid ? TransferPart.Block : Index + 1 );

				copy.State = State.Active;
				CurrentList.Add( copy );
			}

			transact.BlockIndex++;
		}


		public override void Reset()
		{
			base.Reset();
			RandomizePart.Reset();
			_splitID = 0;
		}
	}

	public class Transfer : Block
	{
		public RandomizePart RandomizePart;
		public TransferPart TransferPart;
		
		public double Probability;


		public Transfer( int transferBlock )
			: this( transferBlock, 1.0 ) {}

		public Transfer( int transferBlock, double probability )
		{
			RandomizePart = new RandomizePart( true, 0.0, 1.0 );
			TransferPart = new TransferPart( transferBlock );
			Probability = probability;

			Reset();
		}


		protected override void Work( Transact transact )
		{
			double trial = RandomizePart.Distribution.NextDouble();
			RandomizePart.Values.Add( trial );

			if ( trial <= Probability )
				transact.BlockIndex = TransferPart.Block;
			else
				transact.BlockIndex++;
		}


		public override void Reset()
		{
			base.Reset();
			RandomizePart.Reset();
		}
	}

	public class Enter : Block
	{
		public TransferPart TransferPart;

		public Storage Storage;


		public Enter( Storage storage ) : this( storage, -1 ) {}

		public Enter( Storage storage, int transferBlock )
		{
			TransferPart = new TransferPart( transferBlock );
			Storage = storage;

			Reset();
		}


		protected override void Work( Transact transact )
		{
			if ( !Storage.Full )
				Storage.Enter( transact );
			else if ( TransferPart.Valid )
				transact.BlockIndex = TransferPart.Block;
			else
				Storage.BlockUp( transact );
		}
	}

	public class Leave : Block
	{
		public Storage Storage;


		public Leave( Storage storage )
		{
			Storage = storage;

			Reset();
		}


		protected override void Work( Transact transact )
		{
			Transact activated = Storage.Leave( transact );

			if ( activated != null )
				CurrentList.Add( activated );

			transact.BlockIndex++;
		}
	}
	
	public class Seize : Block
	{
		public TransferPart TransferPart;

		public Device Device;


		public Seize( Device device ) : this( device, -1 ) {}

		public Seize( Device device, int transferBlock )
		{
			TransferPart = new TransferPart( transferBlock );
			Device = device;

			Reset();
		}


		protected override void Work( Transact transact )
		{
			if ( Device.Free )
				Device.Seize( transact );
			else if ( TransferPart.Valid )
				transact.BlockIndex = TransferPart.Block;
			else
				Device.BlockUp( transact );
		}
	}

	public class Release : Block
	{
		public Device Device;


		public Release( Device device )
		{
			Device = device;

			Reset();
		}


		protected override void Work( Transact transact )
		{
			Transact activated = Device.Release( transact );
				
			if ( activated != null )
				CurrentList.Add( activated );

			transact.BlockIndex++;
		}
	}
	
	public class Terminate : Block
	{
		public int DecreaseCount;


		public Terminate()
			: this( 1 )
		{
		}

		public Terminate( int decreaseCount )
		{
			DecreaseCount = decreaseCount;

			Reset();
		}


		protected override void Work( Transact transact )
		{
			transact.State = State.Terminated;
		}
	}
}