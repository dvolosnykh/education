using System;
using System.Collections.Generic;
using System.Diagnostics;



namespace Modelling
{
	public class Manager
	{
		public TransactList CurrentList;
		public TransactList FutureList;
		public BlockList Blocks;
		public bool TransactCounterEnabled = false;
		public int TransactCount;
		public double ModelTime;
		public double FinishTime = -1;


		public void Dispatch()
		{
			BuildCurrentList();

			ModelTime = CurrentList[ 0 ].DispatchTime;

			for ( ; CurrentList.Count > 0; )
			{
				Transact transact = CurrentList[ 0 ];
				CurrentList.Remove( transact );

				// new transaction. make source generator create new one.
				if ( transact.New )
				{
					Blocks[ transact.GeneratedBy ].Execute( transact );
					transact.New = false;
				}

				// forward transaction.
				for ( ; transact.State == State.Active; )
					Blocks[ transact.BlockIndex ].Execute( transact );

				// decrease global transact counter.
				if ( transact.State == State.Terminated )
					if ( TransactCounterEnabled )
					{
						Terminate terminator = Blocks[ transact.BlockIndex ] as Terminate;
						TransactCount -= terminator.DecreaseCount;
					}
			}
		}

		private void BuildCurrentList()
		{
			double earliest = FutureList[ 0 ].DispatchTime;

			for ( int i = 0;
				i < FutureList.Count && FutureList[ i ].DispatchTime == earliest;
				i++ )
			{
				Transact transact = FutureList[ i ];
				CurrentList.Add( transact );
				FutureList.Remove( transact );
				transact.State = State.Active;
			}
		}

		public bool Finished
		{
			get
			{
				return
				(
					FinishedByTransactCounter ||
					FinishedByTimeLimit ||
					FutureList.Empty
				);
			}
		}

		public bool FinishedByTransactCounter
		{
			get { return ( TransactCounterEnabled && TransactCount <= 0 ); }
		}

		public bool FinishedByTimeLimit
		{
			get { return ( FinishTime > 0.0 && ModelTime >= FinishTime ); }
		}
	}
}
