﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


using Modelling;


namespace BaumanMetro
{
	struct DoublePair
	{
		public double Average;
		public double Dispersion;
	}

	struct IntPair
	{
		public int Average;
		public int Dispersion;
	}


	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}


		private Model _model = new Model();
		private DoublePair _trainFromCenterArrive;
		private IntPair _passengersFromCenter;
		private DoublePair _trainToCenterArrive;
		private IntPair _passengersToCenter;
		private DoublePair _goToEscalator;
		private int _escalatorsUpCount;
		private double _escalatorTime;
		private int _stepsCount;
		private double _passDoorTime;
		private int _exitDoorsCount;
		private double _finishTime;
		private int _passengersCount;


		private void buttonStart_Click( object sender, EventArgs e )
		{
			if ( ReadValues() )
			{
				//
				// entities.
				//
				Storage escalatorUp = new Storage( _escalatorsUpCount * 2 );
				_model.Register( escalatorUp );

				List<Storage> exit = new List<Storage>( _exitDoorsCount );
				for ( int i = 0; i < exit.Capacity; i++ )
				{
					Storage door = new Storage( 2 );
					exit.Add( door );
					_model.Register( door );
				}

				//
				// blocks.
				//
				double oneStepTime = _escalatorTime / _stepsCount;
				
				_model.Register( new Generate( _trainFromCenterArrive.Average, _trainFromCenterArrive.Dispersion ) );	// train-from-center arrives.
				_model.Register( new Split( _passengersFromCenter.Average, _passengersFromCenter.Dispersion ) );		// passengers exit train.
				_model.Register( new Transfer( 5 ) );																	// move transact to train exit.
				_model.Register( new Generate( _trainToCenterArrive.Average, _trainToCenterArrive.Dispersion ) );		// train-to-center arrives.
				_model.Register( new Split( _passengersFromCenter.Average, _passengersFromCenter.Dispersion ) );		// passengers exit train.				
				_model.Register( new Advance( _goToEscalator.Average, _goToEscalator.Dispersion ) );					// passengers go to escalator.
				_model.Register( new Enter( escalatorUp ) );															// passenger tries to enter escalator, capturing 1st step.
				_model.Register( new Advance( oneStepTime ) );															// passenger is carried until 1st step becomes 2nd.
				_model.Register( new Leave( escalatorUp ) );															// passenger's step has become 2nd.
				_model.Register( new Advance( _escalatorTime - oneStepTime ) );											// passenger is carried by escalator till the end.
				for ( int i = 1, index = 10; i <= _exitDoorsCount; i++ )
				{
					if ( _exitDoorsCount > i )
					{
						double probability = ( _exitDoorsCount - i ) / ( double )_exitDoorsCount;
						_model.Register( new Transfer( index += 5, probability ) );										// passenger may choose another exit door.
					}
					_model.Register( new Enter( exit[ i - 1 ] ) );														// passenger enters this exit door.
					_model.Register( new Advance( _passDoorTime ) );													// it takes some time for him to pass by the door.
					_model.Register( new Leave( exit[ i - 1 ] ) );														// passenger has passed through the door.
					_model.Register( new Terminate() );																	// passenger is not of our care anymore.
				}
				
				if ( checkTransactCount.Checked )
					_model.EnableTransactCounter( _passengersCount );
				else
					_model.DisableTransactCounter();

				if ( checkFinishTime.Checked )
					_model.EnableFinishTime( _finishTime );
				else
					_model.DisableFinishTime();

				_model.Start();

				_model.Destroy();

				MessageBox.Show( "Завершено.", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk );

				//
				// output stats.
				//
				textTotalTime.Text = _model.FinishTime.ToString( "N3" );

				textEscalatorQueueMaxLen.Text = escalatorUp.Queue.MaxLength.ToString();
				textEscalatorLoadCoeff.Text = escalatorUp.LoadCoeff( _model.FinishTime ).ToString( "N3" );
				textEscalatorAverageWait.Text = escalatorUp.AverageWait.ToString( "N3" );

				int doorsMaxLength = 0;
				double doorsLoadCoeff = 0.0;
				double doorsAverageWait = 0.0;
				for ( int i = 0; i < exit.Capacity; i++ )
				{
					doorsMaxLength += exit[ i ].Queue.MaxLength;
					doorsLoadCoeff += exit[ i ].LoadCoeff( _model.FinishTime );
					doorsAverageWait += exit[ i ].AverageWait;
				}
				doorsMaxLength /= exit.Capacity;
				doorsLoadCoeff /= exit.Capacity;
				doorsAverageWait /= exit.Capacity;

				textDoorsQueueMaxLen.Text = doorsMaxLength.ToString();
				textDoorsLoadCoeff.Text = doorsLoadCoeff.ToString( "N3" );
				textDoorsAverageWait.Text = doorsAverageWait.ToString( "N3" );
			}
		}

		private bool ReadValues()
		{
			bool goodFormat = true;
			bool goodConditions = true;

			try
			{
				_trainFromCenterArrive.Average = Convert.ToDouble( textTrainFromCenterAverage.Text );
				_trainFromCenterArrive.Dispersion = Convert.ToDouble( textTrainFromCenterDispersion.Text );
				goodConditions = TestDispersion( _trainFromCenterArrive.Average, _trainFromCenterArrive.Dispersion );

				_passengersFromCenter.Average = Convert.ToInt32( textPassengersFromCenterAverage.Text );
				_passengersFromCenter.Dispersion = Convert.ToInt32( textPassengersFromCenterDispersion.Text );
				goodConditions = TestDispersion( _passengersFromCenter.Average, _passengersFromCenter.Dispersion );

				_trainToCenterArrive.Average = Convert.ToDouble( textTrainToCenterAverage.Text );
				_trainToCenterArrive.Dispersion = Convert.ToDouble( textTrainToCenterDispersion.Text );
				goodConditions = TestDispersion( _trainToCenterArrive.Average, _trainToCenterArrive.Dispersion );

				_passengersToCenter.Average = Convert.ToInt32( textPassengersToCenterAverage.Text );
				_passengersToCenter.Dispersion = Convert.ToInt32( textPassengersToCenterDispersion.Text );
				goodConditions = TestDispersion( _passengersToCenter.Average, _passengersToCenter.Dispersion );

				_goToEscalator.Average = Convert.ToDouble( textGoToEscalatorAverage.Text );
				_goToEscalator.Dispersion = Convert.ToDouble( textGoToEscalatorDispersion.Text );
				goodConditions = TestDispersion( _goToEscalator.Average, _goToEscalator.Dispersion );

				_escalatorsUpCount = ( int )numericEscalatorsCount.Value;
				_escalatorTime = Convert.ToDouble( textEscalatorTime.Text );
				goodConditions = ( 0.0 <= _escalatorTime );
				_stepsCount = ( int )numericStepsCount.Value;
				
				_exitDoorsCount = ( int )numericDoorsCount.Value;
				_passDoorTime = Convert.ToDouble( textPassDoorTime.Text );
				goodConditions = ( 0.0 <= _passDoorTime );

				_finishTime = Convert.ToDouble( textFinishTime.Text );
				goodConditions = ( !checkFinishTime.Checked || 0.0 < _finishTime );

				_passengersCount = Convert.ToInt32( textPassengersCount.Text );
				goodConditions = ( !checkTransactCount.Checked || 0 <= _passengersCount ); 
			}
			catch ( FormatException )
			{
				goodFormat = false;

				MessageBox.Show( "Один из параметров введён не корректно.", "Ошибка",
					MessageBoxButtons.OK, MessageBoxIcon.Error );
			}

			if ( goodFormat && !goodConditions )
			{
				MessageBox.Show( "Отклонение любой величины должно быть меньше среднего значения.", "Ошибка",
					MessageBoxButtons.OK, MessageBoxIcon.Error );
			}

			if ( goodFormat && goodConditions )
			{
				goodConditions = checkFinishTime.Checked || checkTransactCount.Checked;
				if ( !goodConditions )
				{
					MessageBox.Show( "Должно быть выбрано хотя бы одно условие завершнеия.", "Внимание",
						MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
				}
			}

			return ( goodFormat && goodConditions );
		}

		private bool TestDispersion( double average, double dispersion )
		{
			return ( 0.0 <= dispersion && dispersion <= average );
		}
	}
}