﻿namespace BaumanMetro
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupSetup = new System.Windows.Forms.GroupBox();
			this.groupDoors = new System.Windows.Forms.GroupBox();
			this.numericDoorsCount = new System.Windows.Forms.NumericUpDown();
			this.label8 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.textPassDoorTime = new System.Windows.Forms.TextBox();
			this.label28 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.groupEscalators = new System.Windows.Forms.GroupBox();
			this.numericStepsCount = new System.Windows.Forms.NumericUpDown();
			this.numericEscalatorsCount = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.textEscalatorTime = new System.Windows.Forms.TextBox();
			this.label29 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.groupTrainToCenter = new System.Windows.Forms.GroupBox();
			this.label24 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.textPassengersToCenterDispersion = new System.Windows.Forms.TextBox();
			this.label23 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.textTrainToCenterDispersion = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.textTrainToCenterAverage = new System.Windows.Forms.TextBox();
			this.textPassengersToCenterAverage = new System.Windows.Forms.TextBox();
			this.groupGoToEscalator = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.textGoToEscalatorDispersion = new System.Windows.Forms.TextBox();
			this.textGoToEscalatorAverage = new System.Windows.Forms.TextBox();
			this.groupTrainFromCenter = new System.Windows.Forms.GroupBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textPassengersFromCenterDispersion = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.textTrainFromCenterDispersion = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.textPassengersFromCenterAverage = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textTrainFromCenterAverage = new System.Windows.Forms.TextBox();
			this.groupStats = new System.Windows.Forms.GroupBox();
			this.label32 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label34 = new System.Windows.Forms.Label();
			this.textDoorsAverageWait = new System.Windows.Forms.TextBox();
			this.textDoorsLoadCoeff = new System.Windows.Forms.TextBox();
			this.textDoorsQueueMaxLen = new System.Windows.Forms.TextBox();
			this.label35 = new System.Windows.Forms.Label();
			this.groupEscalatorsStats = new System.Windows.Forms.GroupBox();
			this.label33 = new System.Windows.Forms.Label();
			this.textEscalatorAverageWait = new System.Windows.Forms.TextBox();
			this.textEscalatorLoadCoeff = new System.Windows.Forms.TextBox();
			this.textEscalatorQueueMaxLen = new System.Windows.Forms.TextBox();
			this.label25 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.textTotalTime = new System.Windows.Forms.TextBox();
			this.buttonStart = new System.Windows.Forms.Button();
			this.textFinishTime = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.textPassengersCount = new System.Windows.Forms.TextBox();
			this.checkFinishTime = new System.Windows.Forms.CheckBox();
			this.checkTransactCount = new System.Windows.Forms.CheckBox();
			this.groupStopConditions = new System.Windows.Forms.GroupBox();
			this.label26 = new System.Windows.Forms.Label();
			this.groupSetup.SuspendLayout();
			this.groupDoors.SuspendLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.numericDoorsCount ) ).BeginInit();
			this.groupEscalators.SuspendLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.numericStepsCount ) ).BeginInit();
			( ( System.ComponentModel.ISupportInitialize )( this.numericEscalatorsCount ) ).BeginInit();
			this.groupTrainToCenter.SuspendLayout();
			this.groupGoToEscalator.SuspendLayout();
			this.groupTrainFromCenter.SuspendLayout();
			this.groupStats.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupEscalatorsStats.SuspendLayout();
			this.groupStopConditions.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupSetup
			// 
			this.groupSetup.Controls.Add( this.groupDoors );
			this.groupSetup.Controls.Add( this.groupEscalators );
			this.groupSetup.Controls.Add( this.groupTrainToCenter );
			this.groupSetup.Controls.Add( this.groupGoToEscalator );
			this.groupSetup.Controls.Add( this.groupTrainFromCenter );
			this.groupSetup.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupSetup.ForeColor = System.Drawing.Color.Red;
			this.groupSetup.Location = new System.Drawing.Point( 12, 191 );
			this.groupSetup.Name = "groupSetup";
			this.groupSetup.Size = new System.Drawing.Size( 556, 349 );
			this.groupSetup.TabIndex = 1;
			this.groupSetup.TabStop = false;
			this.groupSetup.Text = "Настройка :";
			// 
			// groupDoors
			// 
			this.groupDoors.Controls.Add( this.numericDoorsCount );
			this.groupDoors.Controls.Add( this.label8 );
			this.groupDoors.Controls.Add( this.label20 );
			this.groupDoors.Controls.Add( this.textPassDoorTime );
			this.groupDoors.Controls.Add( this.label28 );
			this.groupDoors.Controls.Add( this.label19 );
			this.groupDoors.Location = new System.Drawing.Point( 157, 12 );
			this.groupDoors.Name = "groupDoors";
			this.groupDoors.Size = new System.Drawing.Size( 243, 77 );
			this.groupDoors.TabIndex = 5;
			this.groupDoors.TabStop = false;
			this.groupDoors.Text = "Двери :";
			// 
			// numericDoorsCount
			// 
			this.numericDoorsCount.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.numericDoorsCount.Location = new System.Drawing.Point( 133, 20 );
			this.numericDoorsCount.Maximum = new decimal( new int[] {
            3,
            0,
            0,
            0} );
			this.numericDoorsCount.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
			this.numericDoorsCount.Name = "numericDoorsCount";
			this.numericDoorsCount.Size = new System.Drawing.Size( 47, 20 );
			this.numericDoorsCount.TabIndex = 5;
			this.numericDoorsCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.numericDoorsCount.Value = new decimal( new int[] {
            3,
            0,
            0,
            0} );
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label8.Location = new System.Drawing.Point( 9, 22 );
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size( 118, 13 );
			this.label8.TabIndex = 4;
			this.label8.Text = "Количество выходов :";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label20.Location = new System.Drawing.Point( 9, 52 );
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size( 115, 13 );
			this.label20.TabIndex = 4;
			this.label20.Text = "Проход через дверь :";
			// 
			// textPassDoorTime
			// 
			this.textPassDoorTime.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textPassDoorTime.Location = new System.Drawing.Point( 133, 49 );
			this.textPassDoorTime.Name = "textPassDoorTime";
			this.textPassDoorTime.Size = new System.Drawing.Size( 47, 20 );
			this.textPassDoorTime.TabIndex = 0;
			this.textPassDoorTime.Text = "2";
			this.textPassDoorTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label28.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label28.Location = new System.Drawing.Point( 186, 22 );
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size( 23, 13 );
			this.label28.TabIndex = 1;
			this.label28.Text = "шт.";
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label19.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label19.Location = new System.Drawing.Point( 186, 52 );
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size( 28, 13 );
			this.label19.TabIndex = 1;
			this.label19.Text = "сек.";
			// 
			// groupEscalators
			// 
			this.groupEscalators.Controls.Add( this.numericStepsCount );
			this.groupEscalators.Controls.Add( this.numericEscalatorsCount );
			this.groupEscalators.Controls.Add( this.label7 );
			this.groupEscalators.Controls.Add( this.label30 );
			this.groupEscalators.Controls.Add( this.label17 );
			this.groupEscalators.Controls.Add( this.textEscalatorTime );
			this.groupEscalators.Controls.Add( this.label29 );
			this.groupEscalators.Controls.Add( this.label14 );
			this.groupEscalators.Controls.Add( this.label18 );
			this.groupEscalators.Location = new System.Drawing.Point( 157, 95 );
			this.groupEscalators.Name = "groupEscalators";
			this.groupEscalators.Size = new System.Drawing.Size( 243, 102 );
			this.groupEscalators.TabIndex = 4;
			this.groupEscalators.TabStop = false;
			this.groupEscalators.Text = "Эскалаторы :";
			// 
			// numericStepsCount
			// 
			this.numericStepsCount.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.numericStepsCount.Location = new System.Drawing.Point( 155, 76 );
			this.numericStepsCount.Maximum = new decimal( new int[] {
            200,
            0,
            0,
            0} );
			this.numericStepsCount.Minimum = new decimal( new int[] {
            50,
            0,
            0,
            0} );
			this.numericStepsCount.Name = "numericStepsCount";
			this.numericStepsCount.Size = new System.Drawing.Size( 47, 20 );
			this.numericStepsCount.TabIndex = 5;
			this.numericStepsCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.numericStepsCount.Value = new decimal( new int[] {
            100,
            0,
            0,
            0} );
			// 
			// numericEscalatorsCount
			// 
			this.numericEscalatorsCount.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.numericEscalatorsCount.Location = new System.Drawing.Point( 155, 23 );
			this.numericEscalatorsCount.Maximum = new decimal( new int[] {
            2,
            0,
            0,
            0} );
			this.numericEscalatorsCount.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
			this.numericEscalatorsCount.Name = "numericEscalatorsCount";
			this.numericEscalatorsCount.Size = new System.Drawing.Size( 46, 20 );
			this.numericEscalatorsCount.TabIndex = 5;
			this.numericEscalatorsCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.numericEscalatorsCount.Value = new decimal( new int[] {
            2,
            0,
            0,
            0} );
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label7.Location = new System.Drawing.Point( 9, 25 );
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size( 140, 13 );
			this.label7.TabIndex = 4;
			this.label7.Text = "Количество эскалаторов :";
			// 
			// label30
			// 
			this.label30.AutoSize = true;
			this.label30.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label30.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label30.Location = new System.Drawing.Point( 8, 78 );
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size( 121, 13 );
			this.label30.TabIndex = 4;
			this.label30.Text = "Количество ступенек :";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label17.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label17.Location = new System.Drawing.Point( 9, 52 );
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size( 131, 13 );
			this.label17.TabIndex = 4;
			this.label17.Text = "Подъём на эскалаторе :";
			// 
			// textEscalatorTime
			// 
			this.textEscalatorTime.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textEscalatorTime.Location = new System.Drawing.Point( 155, 49 );
			this.textEscalatorTime.Name = "textEscalatorTime";
			this.textEscalatorTime.Size = new System.Drawing.Size( 47, 20 );
			this.textEscalatorTime.TabIndex = 0;
			this.textEscalatorTime.Text = "120";
			this.textEscalatorTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label29.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label29.Location = new System.Drawing.Point( 208, 78 );
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size( 23, 13 );
			this.label29.TabIndex = 1;
			this.label29.Text = "шт.";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label14.Location = new System.Drawing.Point( 207, 25 );
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size( 23, 13 );
			this.label14.TabIndex = 1;
			this.label14.Text = "шт.";
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label18.Location = new System.Drawing.Point( 208, 52 );
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size( 28, 13 );
			this.label18.TabIndex = 1;
			this.label18.Text = "сек.";
			// 
			// groupTrainToCenter
			// 
			this.groupTrainToCenter.Controls.Add( this.label24 );
			this.groupTrainToCenter.Controls.Add( this.label9 );
			this.groupTrainToCenter.Controls.Add( this.label21 );
			this.groupTrainToCenter.Controls.Add( this.textPassengersToCenterDispersion );
			this.groupTrainToCenter.Controls.Add( this.label23 );
			this.groupTrainToCenter.Controls.Add( this.label22 );
			this.groupTrainToCenter.Controls.Add( this.textTrainToCenterDispersion );
			this.groupTrainToCenter.Controls.Add( this.label12 );
			this.groupTrainToCenter.Controls.Add( this.textTrainToCenterAverage );
			this.groupTrainToCenter.Controls.Add( this.textPassengersToCenterAverage );
			this.groupTrainToCenter.Location = new System.Drawing.Point( 14, 258 );
			this.groupTrainToCenter.Name = "groupTrainToCenter";
			this.groupTrainToCenter.Size = new System.Drawing.Size( 261, 76 );
			this.groupTrainToCenter.TabIndex = 3;
			this.groupTrainToCenter.TabStop = false;
			this.groupTrainToCenter.Text = "Поезд к центру :";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label24.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label24.Location = new System.Drawing.Point( 155, 48 );
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size( 24, 13 );
			this.label24.TabIndex = 1;
			this.label24.Text = "+ | -";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label9.Location = new System.Drawing.Point( 155, 22 );
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size( 24, 13 );
			this.label9.TabIndex = 10;
			this.label9.Text = "+ | -";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label21.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label21.Location = new System.Drawing.Point( 227, 22 );
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size( 28, 13 );
			this.label21.TabIndex = 9;
			this.label21.Text = "сек.";
			// 
			// textPassengersToCenterDispersion
			// 
			this.textPassengersToCenterDispersion.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textPassengersToCenterDispersion.Location = new System.Drawing.Point( 185, 45 );
			this.textPassengersToCenterDispersion.Name = "textPassengersToCenterDispersion";
			this.textPassengersToCenterDispersion.Size = new System.Drawing.Size( 36, 20 );
			this.textPassengersToCenterDispersion.TabIndex = 1;
			this.textPassengersToCenterDispersion.Text = "5";
			this.textPassengersToCenterDispersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label23.Location = new System.Drawing.Point( 227, 48 );
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size( 27, 13 );
			this.label23.TabIndex = 1;
			this.label23.Text = "чел.";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label22.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label22.Location = new System.Drawing.Point( 9, 22 );
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size( 98, 13 );
			this.label22.TabIndex = 8;
			this.label22.Text = "Время прибытия :";
			// 
			// textTrainToCenterDispersion
			// 
			this.textTrainToCenterDispersion.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textTrainToCenterDispersion.Location = new System.Drawing.Point( 185, 19 );
			this.textTrainToCenterDispersion.Name = "textTrainToCenterDispersion";
			this.textTrainToCenterDispersion.Size = new System.Drawing.Size( 36, 20 );
			this.textTrainToCenterDispersion.TabIndex = 7;
			this.textTrainToCenterDispersion.Text = "15";
			this.textTrainToCenterDispersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label12.Location = new System.Drawing.Point( 9, 48 );
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size( 77, 13 );
			this.label12.TabIndex = 1;
			this.label12.Text = "Пассажиров :";
			// 
			// textTrainToCenterAverage
			// 
			this.textTrainToCenterAverage.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textTrainToCenterAverage.Location = new System.Drawing.Point( 113, 19 );
			this.textTrainToCenterAverage.Name = "textTrainToCenterAverage";
			this.textTrainToCenterAverage.Size = new System.Drawing.Size( 36, 20 );
			this.textTrainToCenterAverage.TabIndex = 6;
			this.textTrainToCenterAverage.Text = "90";
			this.textTrainToCenterAverage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textPassengersToCenterAverage
			// 
			this.textPassengersToCenterAverage.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textPassengersToCenterAverage.Location = new System.Drawing.Point( 113, 45 );
			this.textPassengersToCenterAverage.Name = "textPassengersToCenterAverage";
			this.textPassengersToCenterAverage.Size = new System.Drawing.Size( 36, 20 );
			this.textPassengersToCenterAverage.TabIndex = 0;
			this.textPassengersToCenterAverage.Text = "49";
			this.textPassengersToCenterAverage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// groupGoToEscalator
			// 
			this.groupGoToEscalator.Controls.Add( this.label5 );
			this.groupGoToEscalator.Controls.Add( this.label6 );
			this.groupGoToEscalator.Controls.Add( this.textGoToEscalatorDispersion );
			this.groupGoToEscalator.Controls.Add( this.textGoToEscalatorAverage );
			this.groupGoToEscalator.Location = new System.Drawing.Point( 157, 203 );
			this.groupGoToEscalator.Name = "groupGoToEscalator";
			this.groupGoToEscalator.Size = new System.Drawing.Size( 243, 49 );
			this.groupGoToEscalator.TabIndex = 1;
			this.groupGoToEscalator.TabStop = false;
			this.groupGoToEscalator.Text = "Перемещение по платформе :";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label5.Location = new System.Drawing.Point( 131, 24 );
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size( 24, 13 );
			this.label5.TabIndex = 6;
			this.label5.Text = "+ | -";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label6.Location = new System.Drawing.Point( 203, 24 );
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size( 28, 13 );
			this.label6.TabIndex = 5;
			this.label6.Text = "сек.";
			// 
			// textGoToEscalatorDispersion
			// 
			this.textGoToEscalatorDispersion.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textGoToEscalatorDispersion.Location = new System.Drawing.Point( 163, 21 );
			this.textGoToEscalatorDispersion.Name = "textGoToEscalatorDispersion";
			this.textGoToEscalatorDispersion.Size = new System.Drawing.Size( 36, 20 );
			this.textGoToEscalatorDispersion.TabIndex = 3;
			this.textGoToEscalatorDispersion.Text = "5";
			this.textGoToEscalatorDispersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textGoToEscalatorAverage
			// 
			this.textGoToEscalatorAverage.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textGoToEscalatorAverage.Location = new System.Drawing.Point( 89, 21 );
			this.textGoToEscalatorAverage.Name = "textGoToEscalatorAverage";
			this.textGoToEscalatorAverage.Size = new System.Drawing.Size( 36, 20 );
			this.textGoToEscalatorAverage.TabIndex = 2;
			this.textGoToEscalatorAverage.Text = "34";
			this.textGoToEscalatorAverage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// groupTrainFromCenter
			// 
			this.groupTrainFromCenter.Controls.Add( this.label11 );
			this.groupTrainFromCenter.Controls.Add( this.label2 );
			this.groupTrainFromCenter.Controls.Add( this.textPassengersFromCenterDispersion );
			this.groupTrainFromCenter.Controls.Add( this.label4 );
			this.groupTrainFromCenter.Controls.Add( this.textTrainFromCenterDispersion );
			this.groupTrainFromCenter.Controls.Add( this.label3 );
			this.groupTrainFromCenter.Controls.Add( this.label10 );
			this.groupTrainFromCenter.Controls.Add( this.textPassengersFromCenterAverage );
			this.groupTrainFromCenter.Controls.Add( this.label1 );
			this.groupTrainFromCenter.Controls.Add( this.textTrainFromCenterAverage );
			this.groupTrainFromCenter.Location = new System.Drawing.Point( 281, 258 );
			this.groupTrainFromCenter.Name = "groupTrainFromCenter";
			this.groupTrainFromCenter.Size = new System.Drawing.Size( 261, 76 );
			this.groupTrainFromCenter.TabIndex = 0;
			this.groupTrainFromCenter.TabStop = false;
			this.groupTrainFromCenter.Text = "Поезд из центра :";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label11.Location = new System.Drawing.Point( 155, 48 );
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size( 24, 13 );
			this.label11.TabIndex = 1;
			this.label11.Text = "+ | -";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label2.Location = new System.Drawing.Point( 155, 22 );
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size( 24, 13 );
			this.label2.TabIndex = 1;
			this.label2.Text = "+ | -";
			// 
			// textPassengersFromCenterDispersion
			// 
			this.textPassengersFromCenterDispersion.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textPassengersFromCenterDispersion.Location = new System.Drawing.Point( 185, 45 );
			this.textPassengersFromCenterDispersion.Name = "textPassengersFromCenterDispersion";
			this.textPassengersFromCenterDispersion.Size = new System.Drawing.Size( 36, 20 );
			this.textPassengersFromCenterDispersion.TabIndex = 1;
			this.textPassengersFromCenterDispersion.Text = "6";
			this.textPassengersFromCenterDispersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label4.Location = new System.Drawing.Point( 227, 48 );
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size( 27, 13 );
			this.label4.TabIndex = 1;
			this.label4.Text = "чел.";
			// 
			// textTrainFromCenterDispersion
			// 
			this.textTrainFromCenterDispersion.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textTrainFromCenterDispersion.Location = new System.Drawing.Point( 185, 19 );
			this.textTrainFromCenterDispersion.Name = "textTrainFromCenterDispersion";
			this.textTrainFromCenterDispersion.Size = new System.Drawing.Size( 36, 20 );
			this.textTrainFromCenterDispersion.TabIndex = 1;
			this.textTrainFromCenterDispersion.Text = "11";
			this.textTrainFromCenterDispersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label3.Location = new System.Drawing.Point( 9, 48 );
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size( 77, 13 );
			this.label3.TabIndex = 1;
			this.label3.Text = "Пассажиров :";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label10.Location = new System.Drawing.Point( 227, 22 );
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size( 28, 13 );
			this.label10.TabIndex = 1;
			this.label10.Text = "сек.";
			// 
			// textPassengersFromCenterAverage
			// 
			this.textPassengersFromCenterAverage.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textPassengersFromCenterAverage.Location = new System.Drawing.Point( 113, 45 );
			this.textPassengersFromCenterAverage.Name = "textPassengersFromCenterAverage";
			this.textPassengersFromCenterAverage.Size = new System.Drawing.Size( 36, 20 );
			this.textPassengersFromCenterAverage.TabIndex = 0;
			this.textPassengersFromCenterAverage.Text = "51";
			this.textPassengersFromCenterAverage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label1.Location = new System.Drawing.Point( 9, 22 );
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size( 98, 13 );
			this.label1.TabIndex = 1;
			this.label1.Text = "Время прибытия :";
			// 
			// textTrainFromCenterAverage
			// 
			this.textTrainFromCenterAverage.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textTrainFromCenterAverage.Location = new System.Drawing.Point( 113, 19 );
			this.textTrainFromCenterAverage.Name = "textTrainFromCenterAverage";
			this.textTrainFromCenterAverage.Size = new System.Drawing.Size( 36, 20 );
			this.textTrainFromCenterAverage.TabIndex = 0;
			this.textTrainFromCenterAverage.Text = "90";
			this.textTrainFromCenterAverage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// groupStats
			// 
			this.groupStats.Controls.Add( this.label32 );
			this.groupStats.Controls.Add( this.label13 );
			this.groupStats.Controls.Add( this.label27 );
			this.groupStats.Controls.Add( this.groupBox1 );
			this.groupStats.Controls.Add( this.groupEscalatorsStats );
			this.groupStats.Controls.Add( this.label15 );
			this.groupStats.Controls.Add( this.label31 );
			this.groupStats.Controls.Add( this.textTotalTime );
			this.groupStats.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.groupStats.ForeColor = System.Drawing.Color.Red;
			this.groupStats.Location = new System.Drawing.Point( 12, 12 );
			this.groupStats.Name = "groupStats";
			this.groupStats.Size = new System.Drawing.Size( 556, 173 );
			this.groupStats.TabIndex = 2;
			this.groupStats.TabStop = false;
			this.groupStats.Text = "Статистика :";
			// 
			// label32
			// 
			this.label32.AutoSize = true;
			this.label32.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label32.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label32.Location = new System.Drawing.Point( 12, 131 );
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size( 144, 13 );
			this.label32.TabIndex = 1;
			this.label32.Text = "Среднее время ожидания :";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label13.Location = new System.Drawing.Point( 12, 25 );
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size( 129, 13 );
			this.label13.TabIndex = 1;
			this.label13.Text = "Время моделирования :";
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label27.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label27.Location = new System.Drawing.Point( 12, 105 );
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size( 132, 13 );
			this.label27.TabIndex = 1;
			this.label27.Text = "Коэффициент загрузки :";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add( this.label34 );
			this.groupBox1.Controls.Add( this.textDoorsAverageWait );
			this.groupBox1.Controls.Add( this.textDoorsLoadCoeff );
			this.groupBox1.Controls.Add( this.textDoorsQueueMaxLen );
			this.groupBox1.Controls.Add( this.label35 );
			this.groupBox1.Location = new System.Drawing.Point( 313, 53 );
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size( 112, 107 );
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Двери :";
			// 
			// label34
			// 
			this.label34.AutoSize = true;
			this.label34.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label34.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label34.Location = new System.Drawing.Point( 76, 78 );
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size( 28, 13 );
			this.label34.TabIndex = 9;
			this.label34.Text = "сек.";
			// 
			// textDoorsAverageWait
			// 
			this.textDoorsAverageWait.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textDoorsAverageWait.Location = new System.Drawing.Point( 13, 75 );
			this.textDoorsAverageWait.Name = "textDoorsAverageWait";
			this.textDoorsAverageWait.Size = new System.Drawing.Size( 58, 20 );
			this.textDoorsAverageWait.TabIndex = 0;
			this.textDoorsAverageWait.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textDoorsLoadCoeff
			// 
			this.textDoorsLoadCoeff.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textDoorsLoadCoeff.Location = new System.Drawing.Point( 13, 49 );
			this.textDoorsLoadCoeff.Name = "textDoorsLoadCoeff";
			this.textDoorsLoadCoeff.Size = new System.Drawing.Size( 58, 20 );
			this.textDoorsLoadCoeff.TabIndex = 0;
			this.textDoorsLoadCoeff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textDoorsQueueMaxLen
			// 
			this.textDoorsQueueMaxLen.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textDoorsQueueMaxLen.Location = new System.Drawing.Point( 13, 23 );
			this.textDoorsQueueMaxLen.Name = "textDoorsQueueMaxLen";
			this.textDoorsQueueMaxLen.Size = new System.Drawing.Size( 58, 20 );
			this.textDoorsQueueMaxLen.TabIndex = 0;
			this.textDoorsQueueMaxLen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label35
			// 
			this.label35.AutoSize = true;
			this.label35.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label35.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label35.Location = new System.Drawing.Point( 77, 26 );
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size( 27, 13 );
			this.label35.TabIndex = 1;
			this.label35.Text = "чел.";
			// 
			// groupEscalatorsStats
			// 
			this.groupEscalatorsStats.Controls.Add( this.label33 );
			this.groupEscalatorsStats.Controls.Add( this.textEscalatorAverageWait );
			this.groupEscalatorsStats.Controls.Add( this.textEscalatorLoadCoeff );
			this.groupEscalatorsStats.Controls.Add( this.textEscalatorQueueMaxLen );
			this.groupEscalatorsStats.Controls.Add( this.label25 );
			this.groupEscalatorsStats.Location = new System.Drawing.Point( 185, 53 );
			this.groupEscalatorsStats.Name = "groupEscalatorsStats";
			this.groupEscalatorsStats.Size = new System.Drawing.Size( 112, 107 );
			this.groupEscalatorsStats.TabIndex = 0;
			this.groupEscalatorsStats.TabStop = false;
			this.groupEscalatorsStats.Text = "Эскалаторы :";
			// 
			// label33
			// 
			this.label33.AutoSize = true;
			this.label33.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label33.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label33.Location = new System.Drawing.Point( 76, 78 );
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size( 28, 13 );
			this.label33.TabIndex = 9;
			this.label33.Text = "сек.";
			// 
			// textEscalatorAverageWait
			// 
			this.textEscalatorAverageWait.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textEscalatorAverageWait.Location = new System.Drawing.Point( 13, 75 );
			this.textEscalatorAverageWait.Name = "textEscalatorAverageWait";
			this.textEscalatorAverageWait.Size = new System.Drawing.Size( 58, 20 );
			this.textEscalatorAverageWait.TabIndex = 0;
			this.textEscalatorAverageWait.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textEscalatorLoadCoeff
			// 
			this.textEscalatorLoadCoeff.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textEscalatorLoadCoeff.Location = new System.Drawing.Point( 13, 49 );
			this.textEscalatorLoadCoeff.Name = "textEscalatorLoadCoeff";
			this.textEscalatorLoadCoeff.Size = new System.Drawing.Size( 58, 20 );
			this.textEscalatorLoadCoeff.TabIndex = 0;
			this.textEscalatorLoadCoeff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textEscalatorQueueMaxLen
			// 
			this.textEscalatorQueueMaxLen.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textEscalatorQueueMaxLen.Location = new System.Drawing.Point( 13, 23 );
			this.textEscalatorQueueMaxLen.Name = "textEscalatorQueueMaxLen";
			this.textEscalatorQueueMaxLen.Size = new System.Drawing.Size( 58, 20 );
			this.textEscalatorQueueMaxLen.TabIndex = 0;
			this.textEscalatorQueueMaxLen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label25.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label25.Location = new System.Drawing.Point( 77, 26 );
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size( 27, 13 );
			this.label25.TabIndex = 1;
			this.label25.Text = "чел.";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label15.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label15.Location = new System.Drawing.Point( 12, 79 );
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size( 167, 13 );
			this.label15.TabIndex = 1;
			this.label15.Text = "Максимальная длина очереди :";
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label31.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label31.Location = new System.Drawing.Point( 227, 25 );
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size( 28, 13 );
			this.label31.TabIndex = 9;
			this.label31.Text = "сек.";
			// 
			// textTotalTime
			// 
			this.textTotalTime.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.textTotalTime.Location = new System.Drawing.Point( 147, 22 );
			this.textTotalTime.Name = "textTotalTime";
			this.textTotalTime.Size = new System.Drawing.Size( 74, 20 );
			this.textTotalTime.TabIndex = 0;
			this.textTotalTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// buttonStart
			// 
			this.buttonStart.Location = new System.Drawing.Point( 461, 597 );
			this.buttonStart.Name = "buttonStart";
			this.buttonStart.Size = new System.Drawing.Size( 107, 23 );
			this.buttonStart.TabIndex = 0;
			this.buttonStart.Text = "Запуск модели";
			this.buttonStart.UseVisualStyleBackColor = true;
			this.buttonStart.Click += new System.EventHandler( this.buttonStart_Click );
			// 
			// textFinishTime
			// 
			this.textFinishTime.Location = new System.Drawing.Point( 236, 19 );
			this.textFinishTime.Name = "textFinishTime";
			this.textFinishTime.Size = new System.Drawing.Size( 72, 20 );
			this.textFinishTime.TabIndex = 3;
			this.textFinishTime.Text = "100000";
			this.textFinishTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label16.Location = new System.Drawing.Point( 314, 22 );
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size( 28, 13 );
			this.label16.TabIndex = 9;
			this.label16.Text = "сек.";
			// 
			// textPassengersCount
			// 
			this.textPassengersCount.Location = new System.Drawing.Point( 236, 45 );
			this.textPassengersCount.Name = "textPassengersCount";
			this.textPassengersCount.Size = new System.Drawing.Size( 72, 20 );
			this.textPassengersCount.TabIndex = 3;
			this.textPassengersCount.Text = "5000";
			this.textPassengersCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// checkFinishTime
			// 
			this.checkFinishTime.AutoSize = true;
			this.checkFinishTime.Location = new System.Drawing.Point( 11, 21 );
			this.checkFinishTime.Name = "checkFinishTime";
			this.checkFinishTime.Size = new System.Drawing.Size( 219, 17 );
			this.checkFinishTime.TabIndex = 10;
			this.checkFinishTime.Text = "Продолжительность моделирования :";
			this.checkFinishTime.UseVisualStyleBackColor = true;
			// 
			// checkTransactCount
			// 
			this.checkTransactCount.AutoSize = true;
			this.checkTransactCount.Checked = true;
			this.checkTransactCount.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkTransactCount.Location = new System.Drawing.Point( 11, 47 );
			this.checkTransactCount.Name = "checkTransactCount";
			this.checkTransactCount.Size = new System.Drawing.Size( 153, 17 );
			this.checkTransactCount.TabIndex = 10;
			this.checkTransactCount.Text = "Количество пассажиров:";
			this.checkTransactCount.UseVisualStyleBackColor = true;
			// 
			// groupStopConditions
			// 
			this.groupStopConditions.Controls.Add( this.textFinishTime );
			this.groupStopConditions.Controls.Add( this.checkTransactCount );
			this.groupStopConditions.Controls.Add( this.label16 );
			this.groupStopConditions.Controls.Add( this.label26 );
			this.groupStopConditions.Controls.Add( this.textPassengersCount );
			this.groupStopConditions.Controls.Add( this.checkFinishTime );
			this.groupStopConditions.Location = new System.Drawing.Point( 12, 546 );
			this.groupStopConditions.Name = "groupStopConditions";
			this.groupStopConditions.Size = new System.Drawing.Size( 350, 74 );
			this.groupStopConditions.TabIndex = 11;
			this.groupStopConditions.TabStop = false;
			this.groupStopConditions.Text = "Условия завершения :";
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label26.ForeColor = System.Drawing.SystemColors.ControlText;
			this.label26.Location = new System.Drawing.Point( 314, 48 );
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size( 27, 13 );
			this.label26.TabIndex = 1;
			this.label26.Text = "чел.";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 582, 633 );
			this.Controls.Add( this.groupStopConditions );
			this.Controls.Add( this.buttonStart );
			this.Controls.Add( this.groupStats );
			this.Controls.Add( this.groupSetup );
			this.Name = "Form1";
			this.Text = "Модель м.Бауманская";
			this.groupSetup.ResumeLayout( false );
			this.groupDoors.ResumeLayout( false );
			this.groupDoors.PerformLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.numericDoorsCount ) ).EndInit();
			this.groupEscalators.ResumeLayout( false );
			this.groupEscalators.PerformLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.numericStepsCount ) ).EndInit();
			( ( System.ComponentModel.ISupportInitialize )( this.numericEscalatorsCount ) ).EndInit();
			this.groupTrainToCenter.ResumeLayout( false );
			this.groupTrainToCenter.PerformLayout();
			this.groupGoToEscalator.ResumeLayout( false );
			this.groupGoToEscalator.PerformLayout();
			this.groupTrainFromCenter.ResumeLayout( false );
			this.groupTrainFromCenter.PerformLayout();
			this.groupStats.ResumeLayout( false );
			this.groupStats.PerformLayout();
			this.groupBox1.ResumeLayout( false );
			this.groupBox1.PerformLayout();
			this.groupEscalatorsStats.ResumeLayout( false );
			this.groupEscalatorsStats.PerformLayout();
			this.groupStopConditions.ResumeLayout( false );
			this.groupStopConditions.PerformLayout();
			this.ResumeLayout( false );

		}

		#endregion

		private System.Windows.Forms.GroupBox groupSetup;
		private System.Windows.Forms.TextBox textTrainFromCenterAverage;
		private System.Windows.Forms.GroupBox groupTrainFromCenter;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textTrainFromCenterDispersion;
		private System.Windows.Forms.GroupBox groupTrainToCenter;
		private System.Windows.Forms.TextBox textTrainToCenterDispersion;
		private System.Windows.Forms.TextBox textTrainToCenterAverage;
		private System.Windows.Forms.GroupBox groupGoToEscalator;
		private System.Windows.Forms.TextBox textGoToEscalatorDispersion;
		private System.Windows.Forms.TextBox textGoToEscalatorAverage;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.GroupBox groupEscalators;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox textEscalatorTime;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.GroupBox groupDoors;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox textPassDoorTime;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.GroupBox groupStats;
		private System.Windows.Forms.Button buttonStart;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TextBox textPassengersToCenterDispersion;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox textPassengersToCenterAverage;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox textPassengersFromCenterDispersion;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textPassengersFromCenterAverage;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.GroupBox groupEscalatorsStats;
		private System.Windows.Forms.TextBox textFinishTime;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox textPassengersCount;
		private System.Windows.Forms.CheckBox checkFinishTime;
		private System.Windows.Forms.CheckBox checkTransactCount;
		private System.Windows.Forms.GroupBox groupStopConditions;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox textEscalatorQueueMaxLen;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.TextBox textEscalatorLoadCoeff;
		private System.Windows.Forms.NumericUpDown numericEscalatorsCount;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.NumericUpDown numericDoorsCount;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox textTotalTime;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.NumericUpDown numericStepsCount;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.TextBox textEscalatorAverageWait;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.TextBox textDoorsAverageWait;
		private System.Windows.Forms.TextBox textDoorsLoadCoeff;
		private System.Windows.Forms.TextBox textDoorsQueueMaxLen;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label26;
	}
}

