using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Diagnostics;
using System.Collections;

using ExploreDistributions.Distributions;


namespace ExploreDistributions.GraphDrawer
{
	public class PointsList : ArrayList
	{
		public new PointF this[ int i ]
		{
			get { return ( ( PointF )base[ i ] ); }
			set { base[ i ] = value; }
		}
	}

	class LabelsList : SortedList< float, PointF >
	{
		public new void Add( float key, PointF item )
		{
			if ( base.ContainsKey( key ) )
			{
				if ( base[ key ] == PointF.Empty || base[ key ].Y < item.Y )
				{
					base.Remove( key );
					base.Add( key, item );
				}
			}
			else
			{
				base.Add( key, item );
			}
		}
	}


	// drawing _options.
	public struct Options
	{
		// current scale
		public float sx;
		public float sy;

		// graph line.
		public Pen GraphPen;
		public Pen StrokePen;

		// arrows of the axises.
		public float ArrowCapLength;
		public float ArrowCapWidth;
		public Pen AxisPen;
		public Brush ArrowBrush;

		// labels.
		public float LabelStorkeLen;
		public Pen LabelPen;
		public Font LabelsFont;
		public Font HeaderFont;

		// define dimensional values.
		public int MarginLeft;
		public int MarginRight;
		public int MarginTop;
		public int MarginBottom;
	}

	class Drawer
	{
		public Options Options;

		// drawing area.
		private Graphics _g;
		private Rectangle _viewRect;


		public Drawer( Graphics g, Rectangle clipRect )
		{
			// save context.
			_g = g;

			// tune drawing _options.
			const int margin = 30;
			Options.MarginBottom = margin;
			Options.MarginLeft = ( int )( 1.5 * margin );
			Options.MarginRight = margin;
			Options.MarginTop = margin;

			Options.GraphPen = new Pen( Color.Blue, 3.0F );
			Options.StrokePen = new Pen( Color.DarkGray );
			Options.StrokePen.DashStyle = DashStyle.Dash;

			Options.ArrowCapLength = 6.0F;
			Options.ArrowCapWidth = 4.0F;
			Options.ArrowBrush = Brushes.DarkGray;
			Options.AxisPen = new Pen( Color.Black );
			PointF[] arrowCapDots = 
			{
				new PointF( - Options.ArrowCapWidth / 2, - Options.ArrowCapLength / 2 ),
				new PointF( 0.0F, Options.ArrowCapLength / 2 ),
				new PointF( + Options.ArrowCapWidth / 2, - Options.ArrowCapLength / 2 )
			};
			GraphicsPath gp = new GraphicsPath();
			gp.AddPolygon( arrowCapDots );
			Options.AxisPen.CustomEndCap = new CustomLineCap( gp, null );
			gp.Dispose();

			Options.LabelStorkeLen = 6.0F;
			Options.LabelPen = new Pen( Options.AxisPen.Color );
			Options.LabelsFont = new Font( FontFamily.GenericMonospace, 8 );
			Options.HeaderFont = new Font( FontFamily.GenericMonospace, 10 );

			// deflate drawing area.
			_viewRect = clipRect;
			_viewRect.Inflate( new Size( - Options.MarginLeft, - Options.MarginTop ) );
			_viewRect.Location += new Size( Options.MarginLeft - Options.MarginRight,
				Options.MarginTop - Options.MarginBottom );

			InitialTransform();
		}

		~Drawer()
		{
			_viewRect = Rectangle.Empty;
			_g = null;


			if ( Options.GraphPen != null ) Options.GraphPen.Dispose();
			if ( Options.StrokePen != null ) Options.StrokePen.Dispose();

			if ( Options.LabelPen != null ) Options.LabelPen.Dispose();

			if ( Options.AxisPen != null ) Options.AxisPen.Dispose();
			if ( Options.ArrowBrush != null ) Options.ArrowBrush.Dispose();

			if ( Options.LabelsFont != null ) Options.LabelsFont.Dispose();
			if ( Options.HeaderFont != null ) Options.HeaderFont.Dispose();
		}


		private void InitialTransform()
		{
			// flip y-axis, and move the origin.
			_g.ResetTransform();
			_g.TranslateTransform( _viewRect.Left, _viewRect.Bottom );
		}

		private void DrawAxes()
		{
			_g.ScaleTransform( 1, -1 );
			
			// draw y-axis.
			_g.DrawLine( Options.AxisPen, new PointF( 0, 0 ), new PointF( 0, _viewRect.Height ) );

			// draw x-axis.
			_g.DrawLine( Options.AxisPen, new PointF( 0, 0 ), new PointF( _viewRect.Width, 0 ) );

			_g.ScaleTransform( 1, -1 );
		}

		private void SetScale( RectangleF bounds )
		{
			Options.sx = ( _viewRect.Width - 2 * Options.ArrowCapLength ) / bounds.Width;
			Options.sy = ( _viewRect.Height - 2 * Options.ArrowCapLength ) / bounds.Height;
		}


		private PointF Transform( PointF original, RectangleF bounds )
		{
			float x = ( original.X - bounds.X ) * Options.sx;
			float y = -( original.Y - bounds.Y ) * Options.sy;
			return new PointF( x, y );
		}



		private RectangleF LabelRectForX( string strLabel, float x )
		{
			SizeF labelSize = _g.MeasureString( strLabel, Options.LabelsFont );
			PointF labelPos = new PointF( x - labelSize.Width / 2, Options.LabelStorkeLen );
			return new RectangleF( labelPos, labelSize );
		}

		private float DrawLowLimitLabelAlongX( float x, PointF original, RectangleF bounds )
		{
			PointF point = Transform( original, bounds );

			string strLabel = x.ToString( "N2" );
			RectangleF labelRect = LabelRectForX( strLabel, point.X );

			PointF pointProjOX = new PointF( point.X, 0.0F );
			_g.DrawLine( Options.StrokePen, pointProjOX, point );

			_g.DrawString( strLabel, Options.LabelsFont, Brushes.Black, labelRect.Location );
			_g.DrawLine( Options.LabelPen, pointProjOX, new PointF( point.X, Options.LabelStorkeLen ) );

			return ( labelRect.X + labelRect.Width );
		}

		private float DrawHighLimitLabelAlongX( float x, PointF original, RectangleF bounds )
		{
			PointF point = Transform( original, bounds );

			string strLabel = x.ToString( "N2" );
			RectangleF labelRect = LabelRectForX( strLabel, point.X );

			PointF pointProjOX = new PointF( point.X, 0.0F );
			_g.DrawLine( Options.StrokePen, pointProjOX, point );

			_g.DrawString( strLabel, Options.LabelsFont, Brushes.Black, labelRect.Location );
			_g.DrawLine( Options.LabelPen, pointProjOX, new PointF( point.X, Options.LabelStorkeLen ) );

			return ( labelRect.X );
		}

		private void DrawSingleLabelAlongX( float x, PointF original, RectangleF bounds, ref float lowLimitX, float highLimitX )
		{
			PointF point = Transform( original, bounds );

			string strLabel = x.ToString( "N2" );
			RectangleF labelRect = LabelRectForX( strLabel, point.X );

			PointF pointProjOX = new PointF( point.X, 0.0F );
			_g.DrawLine( Options.StrokePen, pointProjOX, point );

			if ( labelRect.X > lowLimitX && labelRect.X + labelRect.Width < highLimitX )
			{
				_g.DrawString( strLabel, Options.LabelsFont, Brushes.Black, labelRect.Location );
				lowLimitX = labelRect.X + labelRect.Width;

				_g.DrawLine( Options.LabelPen, pointProjOX, new PointF( point.X, Options.LabelStorkeLen ) );
			}
		}

		private void DrawLabelsAlongX( PointsList points, RectangleF bounds, string strHdr )
		{
			// header of the axis.
			SizeF size = _g.MeasureString( strHdr, Options.HeaderFont );
			PointF pos = new PointF( _viewRect.Width + 2 * Options.ArrowCapLength, - size.Height / 2 );
			_g.DrawString( strHdr, Options.HeaderFont, Brushes.Black, pos );

			// labels along the axis.
			LabelsList labels = new LabelsList();
			labels.Add( bounds.X, PointF.Empty );
			foreach ( PointF point in points )
				labels.Add( point.X, point );

			float lowX = labels.Keys[ 0 ];
			float lowLimitX = DrawLowLimitLabelAlongX( lowX, labels[ lowX ], bounds );

			float highX = labels.Keys[ labels.Keys.Count - 1 ];
			float highLimitX = DrawHighLimitLabelAlongX( highX, labels[ highX ], bounds );

			for ( int i = 1; i < labels.Keys.Count - 1; i++ )
			{
				float x = labels.Keys[ i ];
				DrawSingleLabelAlongX( x, labels[ x ], bounds, ref lowLimitX, highLimitX );
			}
		}



		private RectangleF LabelRectForY( string strLabel, float y )
		{
			SizeF labelSize = _g.MeasureString( strLabel, Options.LabelsFont );
			PointF labelPos = new PointF( -( Options.LabelStorkeLen + labelSize.Width ), y - labelSize.Height / 2 );
			return new RectangleF( labelPos, labelSize );
		}

		private float DrawLowLimitLabelAlongY( float y, PointF original, RectangleF bounds )
		{
			PointF point = Transform( original, bounds );

			string strLabel = y.ToString( "N2" );
			RectangleF labelRect = LabelRectForY( strLabel, point.Y );

			_g.DrawString( strLabel, Options.LabelsFont, Brushes.Black, labelRect.Location );

			PointF pointProjOY = new PointF( 0.0F, point.Y );
			_g.DrawLine( Options.LabelPen, pointProjOY, new PointF( -Options.LabelStorkeLen, point.Y ) );

			if ( original != PointF.Empty )
				_g.DrawLine( Options.StrokePen, pointProjOY, point );

			return ( labelRect.Y );
		}

		private float DrawHighLimitLabelAlongY( float y, PointF original, RectangleF bounds )
		{
			PointF point = Transform( original, bounds );

			string strLabel = y.ToString( "N2" );
			RectangleF labelRect = LabelRectForY( strLabel, point.Y );

			_g.DrawString( strLabel, Options.LabelsFont, Brushes.Black, labelRect.Location );

			PointF pointProjOY = new PointF( 0.0F, point.Y );
			_g.DrawLine( Options.LabelPen, pointProjOY, new PointF( -Options.LabelStorkeLen, point.Y ) );

			if ( original != PointF.Empty )
				_g.DrawLine( Options.StrokePen, pointProjOY, point );

			return ( labelRect.Y + labelRect.Height );
		}

		private void DrawSingleLabelAlongY( float y, PointF original, RectangleF bounds, ref float lowLimitY, float highLimitY )
		{
			PointF point = Transform( original, bounds );

			string strLabel = y.ToString( "N2" );
			RectangleF labelRect = LabelRectForY( strLabel, point.Y );

			if ( - labelRect.Y + lowLimitY > labelRect.Height &&
				labelRect.Y > highLimitY )
			{
				_g.DrawString( strLabel, Options.LabelsFont, Brushes.Black, labelRect.Location );
				lowLimitY = labelRect.Y;

				PointF pointProjOY = new PointF( 0.0F, point.Y );
				_g.DrawLine( Options.LabelPen, pointProjOY, new PointF( -Options.LabelStorkeLen, point.Y ) );

				if ( original != PointF.Empty )
					_g.DrawLine( Options.StrokePen, pointProjOY, point );
			}
		}

		private void DrawLabelsAlongY( PointsList points, RectangleF bounds, string strHdr )
		{
			// header of the axis.
			SizeF size = _g.MeasureString( strHdr, Options.HeaderFont );
			PointF pos = new PointF( - size.Width / 2, -(_viewRect.Height + size.Height + 2 * Options.ArrowCapLength ) );
			_g.DrawString( strHdr, Options.HeaderFont, Brushes.Black, pos );

			// labels along the axis.
			LabelsList labels = new LabelsList();
			labels.Add( bounds.Y, PointF.Empty );
			foreach ( PointF point in points )
				labels.Add( point.Y, point );

			float lowY = labels.Keys[ 0 ];
			float lowLimitY = DrawLowLimitLabelAlongY( lowY, labels[ lowY ], bounds );

			float highY = labels.Keys[ labels.Keys.Count - 1 ];
			float highLimitY = DrawHighLimitLabelAlongY( highY, labels[ highY ], bounds );

			for ( int i = 1; i < labels.Keys.Count - 1; i++ )
			{
				float y = labels.Keys[ i ];
				DrawSingleLabelAlongY( y, labels[ y ], bounds, ref lowLimitY, highLimitY );
			}
		}

		private void DrawLabels( PointsList points, RectangleF bounds, string strFuncHdr )
		{
			char[] sep = { '(', ')' };
			string[] headers = strFuncHdr.Split( sep, StringSplitOptions.RemoveEmptyEntries );

			DrawLabelsAlongX( points, bounds, headers[ 1 ] );
			DrawLabelsAlongY( points, bounds, strFuncHdr );
		}

		private void DrawLines( PointsList points, RectangleF bounds )
		{
			if ( points.Count > 1 )
			{
				for ( int i = 0; i < points.Count; i++ )
					points[ i ] = Transform( points[ i ], bounds );

				for ( int i = 1; i < points.Count; i++ )
					if ( points[ i ].X != points[ i - 1 ].X )
						_g.DrawLine( Options.GraphPen, points[ i - 1 ], points[ i ] );
			}
		}

		public void DrawGraph( IGraph func, bool graphOnly, string strFuncHdr )
		{
			RectangleF bounds = func.Bounds;

			PointsList points = func.GetPoints();

			SetScale( bounds );

			if ( !graphOnly )
			{
				DrawLabels( points, bounds, strFuncHdr );
				DrawAxes();
			}

			DrawLines( points, bounds );
		}

		public void DrawGraph( IGraph func, bool graphOnly )
		{
			DrawGraph( func, graphOnly, "" );
		}
	}
}