namespace ExploreDistributions
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( Form1 ) );
			this.tabControlDistributions = new System.Windows.Forms.TabControl();
			this.tabPagePoisson = new System.Windows.Forms.TabPage();
			this.buttonPoissonRedraw = new System.Windows.Forms.Button();
			this.groupPoissonEmpStats = new System.Windows.Forms.GroupBox();
			this.textPoissonEmpDX = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.textPoissonEmpMX = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.groupPoissonTheorStats = new System.Windows.Forms.GroupBox();
			this.textPoissonTheorDX = new System.Windows.Forms.TextBox();
			this.textPoissonTheorMX = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupPoissonParam = new System.Windows.Forms.GroupBox();
			this.label17 = new System.Windows.Forms.Label();
			this.numericPoissonExpCount = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.textLambda = new System.Windows.Forms.TextBox();
			this.picturePoissonDistrFunc = new System.Windows.Forms.PictureBox();
			this.picturePoissonPoly = new System.Windows.Forms.PictureBox();
			this.tabPageRegular = new System.Windows.Forms.TabPage();
			this.groupRegularEmpStats = new System.Windows.Forms.GroupBox();
			this.textRegularEmpDX = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.textRegularEmpMX = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.groupRegularTheorStats = new System.Windows.Forms.GroupBox();
			this.textRegularTheorDX = new System.Windows.Forms.TextBox();
			this.textRegularTheorMX = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.buttonRegularRedraw = new System.Windows.Forms.Button();
			this.groupRegularParam = new System.Windows.Forms.GroupBox();
			this.label16 = new System.Windows.Forms.Label();
			this.numericRegularExpCount = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this.textB = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.textA = new System.Windows.Forms.TextBox();
			this.pictureRegularDistrFunc = new System.Windows.Forms.PictureBox();
			this.pictureRegularDens = new System.Windows.Forms.PictureBox();
			this.tabControlDistributions.SuspendLayout();
			this.tabPagePoisson.SuspendLayout();
			this.groupPoissonEmpStats.SuspendLayout();
			this.groupPoissonTheorStats.SuspendLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureBox4 ) ).BeginInit();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureBox1 ) ).BeginInit();
			this.groupPoissonParam.SuspendLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.numericPoissonExpCount ) ).BeginInit();
			( ( System.ComponentModel.ISupportInitialize )( this.picturePoissonDistrFunc ) ).BeginInit();
			( ( System.ComponentModel.ISupportInitialize )( this.picturePoissonPoly ) ).BeginInit();
			this.tabPageRegular.SuspendLayout();
			this.groupRegularEmpStats.SuspendLayout();
			this.groupRegularTheorStats.SuspendLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureBox3 ) ).BeginInit();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureBox2 ) ).BeginInit();
			this.groupRegularParam.SuspendLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.numericRegularExpCount ) ).BeginInit();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureRegularDistrFunc ) ).BeginInit();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureRegularDens ) ).BeginInit();
			this.SuspendLayout();
			// 
			// tabControlDistributions
			// 
			this.tabControlDistributions.Controls.Add( this.tabPagePoisson );
			this.tabControlDistributions.Controls.Add( this.tabPageRegular );
			this.tabControlDistributions.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.tabControlDistributions.Location = new System.Drawing.Point( 12, 12 );
			this.tabControlDistributions.Name = "tabControlDistributions";
			this.tabControlDistributions.SelectedIndex = 0;
			this.tabControlDistributions.Size = new System.Drawing.Size( 789, 648 );
			this.tabControlDistributions.TabIndex = 0;
			// 
			// tabPagePoisson
			// 
			this.tabPagePoisson.Controls.Add( this.buttonPoissonRedraw );
			this.tabPagePoisson.Controls.Add( this.groupPoissonEmpStats );
			this.tabPagePoisson.Controls.Add( this.groupPoissonTheorStats );
			this.tabPagePoisson.Controls.Add( this.pictureBox4 );
			this.tabPagePoisson.Controls.Add( this.pictureBox1 );
			this.tabPagePoisson.Controls.Add( this.label2 );
			this.tabPagePoisson.Controls.Add( this.label1 );
			this.tabPagePoisson.Controls.Add( this.groupPoissonParam );
			this.tabPagePoisson.Controls.Add( this.picturePoissonDistrFunc );
			this.tabPagePoisson.Controls.Add( this.picturePoissonPoly );
			this.tabPagePoisson.Location = new System.Drawing.Point( 4, 22 );
			this.tabPagePoisson.Name = "tabPagePoisson";
			this.tabPagePoisson.Padding = new System.Windows.Forms.Padding( 3 );
			this.tabPagePoisson.Size = new System.Drawing.Size( 781, 622 );
			this.tabPagePoisson.TabIndex = 0;
			this.tabPagePoisson.Text = "�������";
			this.tabPagePoisson.UseVisualStyleBackColor = true;
			// 
			// buttonPoissonRedraw
			// 
			this.buttonPoissonRedraw.Location = new System.Drawing.Point( 662, 593 );
			this.buttonPoissonRedraw.Name = "buttonPoissonRedraw";
			this.buttonPoissonRedraw.Size = new System.Drawing.Size( 112, 23 );
			this.buttonPoissonRedraw.TabIndex = 3;
			this.buttonPoissonRedraw.Text = "������������";
			this.buttonPoissonRedraw.UseVisualStyleBackColor = true;
			this.buttonPoissonRedraw.Click += new System.EventHandler( this.buttonPoissonRedraw_Click );
			// 
			// groupPoissonEmpStats
			// 
			this.groupPoissonEmpStats.Controls.Add( this.textPoissonEmpDX );
			this.groupPoissonEmpStats.Controls.Add( this.label9 );
			this.groupPoissonEmpStats.Controls.Add( this.textPoissonEmpMX );
			this.groupPoissonEmpStats.Controls.Add( this.label8 );
			this.groupPoissonEmpStats.ForeColor = System.Drawing.Color.Red;
			this.groupPoissonEmpStats.Location = new System.Drawing.Point( 227, 491 );
			this.groupPoissonEmpStats.Name = "groupPoissonEmpStats";
			this.groupPoissonEmpStats.Size = new System.Drawing.Size( 215, 96 );
			this.groupPoissonEmpStats.TabIndex = 5;
			this.groupPoissonEmpStats.TabStop = false;
			this.groupPoissonEmpStats.Text = "�������������� �������������� (������������)";
			// 
			// textPoissonEmpDX
			// 
			this.textPoissonEmpDX.BackColor = System.Drawing.SystemColors.Window;
			this.textPoissonEmpDX.Location = new System.Drawing.Point( 90, 64 );
			this.textPoissonEmpDX.Name = "textPoissonEmpDX";
			this.textPoissonEmpDX.ReadOnly = true;
			this.textPoissonEmpDX.Size = new System.Drawing.Size( 63, 20 );
			this.textPoissonEmpDX.TabIndex = 0;
			this.textPoissonEmpDX.TabStop = false;
			this.textPoissonEmpDX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.ForeColor = System.Drawing.SystemColors.WindowText;
			this.label9.Location = new System.Drawing.Point( 48, 67 );
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size( 35, 13 );
			this.label9.TabIndex = 1;
			this.label9.Text = "DX =";
			// 
			// textPoissonEmpMX
			// 
			this.textPoissonEmpMX.BackColor = System.Drawing.SystemColors.Window;
			this.textPoissonEmpMX.Location = new System.Drawing.Point( 90, 38 );
			this.textPoissonEmpMX.Name = "textPoissonEmpMX";
			this.textPoissonEmpMX.ReadOnly = true;
			this.textPoissonEmpMX.Size = new System.Drawing.Size( 63, 20 );
			this.textPoissonEmpMX.TabIndex = 0;
			this.textPoissonEmpMX.TabStop = false;
			this.textPoissonEmpMX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.ForeColor = System.Drawing.SystemColors.WindowText;
			this.label8.Location = new System.Drawing.Point( 48, 41 );
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size( 36, 13 );
			this.label8.TabIndex = 1;
			this.label8.Text = "MX =";
			// 
			// groupPoissonTheorStats
			// 
			this.groupPoissonTheorStats.Controls.Add( this.textPoissonTheorDX );
			this.groupPoissonTheorStats.Controls.Add( this.textPoissonTheorMX );
			this.groupPoissonTheorStats.Controls.Add( this.label11 );
			this.groupPoissonTheorStats.Controls.Add( this.label10 );
			this.groupPoissonTheorStats.Location = new System.Drawing.Point( 6, 491 );
			this.groupPoissonTheorStats.Name = "groupPoissonTheorStats";
			this.groupPoissonTheorStats.Size = new System.Drawing.Size( 215, 96 );
			this.groupPoissonTheorStats.TabIndex = 5;
			this.groupPoissonTheorStats.TabStop = false;
			this.groupPoissonTheorStats.Text = "�������������� �������������� (�������������)";
			// 
			// textPoissonTheorDX
			// 
			this.textPoissonTheorDX.BackColor = System.Drawing.SystemColors.Window;
			this.textPoissonTheorDX.Location = new System.Drawing.Point( 88, 64 );
			this.textPoissonTheorDX.Name = "textPoissonTheorDX";
			this.textPoissonTheorDX.ReadOnly = true;
			this.textPoissonTheorDX.Size = new System.Drawing.Size( 63, 20 );
			this.textPoissonTheorDX.TabIndex = 0;
			this.textPoissonTheorDX.TabStop = false;
			this.textPoissonTheorDX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textPoissonTheorMX
			// 
			this.textPoissonTheorMX.BackColor = System.Drawing.SystemColors.Window;
			this.textPoissonTheorMX.Location = new System.Drawing.Point( 88, 38 );
			this.textPoissonTheorMX.Name = "textPoissonTheorMX";
			this.textPoissonTheorMX.ReadOnly = true;
			this.textPoissonTheorMX.Size = new System.Drawing.Size( 63, 20 );
			this.textPoissonTheorMX.TabIndex = 0;
			this.textPoissonTheorMX.TabStop = false;
			this.textPoissonTheorMX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.ForeColor = System.Drawing.SystemColors.WindowText;
			this.label11.Location = new System.Drawing.Point( 46, 67 );
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size( 35, 13 );
			this.label11.TabIndex = 1;
			this.label11.Text = "DX =";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.ForeColor = System.Drawing.SystemColors.WindowText;
			this.label10.Location = new System.Drawing.Point( 46, 41 );
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size( 36, 13 );
			this.label10.TabIndex = 1;
			this.label10.Text = "MX =";
			// 
			// pictureBox4
			// 
			this.pictureBox4.Image = ( ( System.Drawing.Image )( resources.GetObject( "pictureBox4.Image" ) ) );
			this.pictureBox4.Location = new System.Drawing.Point( 450, 261 );
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size( 324, 204 );
			this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.pictureBox4.TabIndex = 4;
			this.pictureBox4.TabStop = false;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ( ( System.Drawing.Image )( resources.GetObject( "pictureBox1.Image" ) ) );
			this.pictureBox1.Location = new System.Drawing.Point( 450, 28 );
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size( 324, 204 );
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.pictureBox1.TabIndex = 4;
			this.pictureBox1.TabStop = false;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label2.Location = new System.Drawing.Point( 447, 245 );
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size( 163, 13 );
			this.label2.TabIndex = 1;
			this.label2.Text = "������� ������������� :";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label1.Location = new System.Drawing.Point( 447, 12 );
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size( 201, 13 );
			this.label1.TabIndex = 1;
			this.label1.Text = "������������� ������������� :";
			// 
			// groupPoissonParam
			// 
			this.groupPoissonParam.Controls.Add( this.label17 );
			this.groupPoissonParam.Controls.Add( this.numericPoissonExpCount );
			this.groupPoissonParam.Controls.Add( this.label5 );
			this.groupPoissonParam.Controls.Add( this.textLambda );
			this.groupPoissonParam.ForeColor = System.Drawing.SystemColors.ControlText;
			this.groupPoissonParam.Location = new System.Drawing.Point( 450, 491 );
			this.groupPoissonParam.Name = "groupPoissonParam";
			this.groupPoissonParam.Size = new System.Drawing.Size( 324, 96 );
			this.groupPoissonParam.TabIndex = 3;
			this.groupPoissonParam.TabStop = false;
			this.groupPoissonParam.Text = "��������� :";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point( 48, 62 );
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size( 152, 13 );
			this.label17.TabIndex = 11;
			this.label17.Text = "���������� ��������� :";
			// 
			// numericPoissonExpCount
			// 
			this.numericPoissonExpCount.Location = new System.Drawing.Point( 206, 60 );
			this.numericPoissonExpCount.Maximum = new decimal( new int[] {
            1000,
            0,
            0,
            0} );
			this.numericPoissonExpCount.Minimum = new decimal( new int[] {
            10,
            0,
            0,
            0} );
			this.numericPoissonExpCount.Name = "numericPoissonExpCount";
			this.numericPoissonExpCount.Size = new System.Drawing.Size( 63, 20 );
			this.numericPoissonExpCount.TabIndex = 2;
			this.numericPoissonExpCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.numericPoissonExpCount.Value = new decimal( new int[] {
            20,
            0,
            0,
            0} );
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point( 78, 22 );
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size( 64, 13 );
			this.label5.TabIndex = 1;
			this.label5.Text = "������ =";
			// 
			// textLambda
			// 
			this.textLambda.Location = new System.Drawing.Point( 148, 19 );
			this.textLambda.Name = "textLambda";
			this.textLambda.Size = new System.Drawing.Size( 63, 20 );
			this.textLambda.TabIndex = 1;
			this.textLambda.Text = "3,5";
			this.textLambda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// picturePoissonDistrFunc
			// 
			this.picturePoissonDistrFunc.Location = new System.Drawing.Point( 6, 261 );
			this.picturePoissonDistrFunc.Name = "picturePoissonDistrFunc";
			this.picturePoissonDistrFunc.Size = new System.Drawing.Size( 436, 204 );
			this.picturePoissonDistrFunc.TabIndex = 0;
			this.picturePoissonDistrFunc.TabStop = false;
			this.picturePoissonDistrFunc.Paint += new System.Windows.Forms.PaintEventHandler( this.picturePoissonDistrFunc_Paint );
			// 
			// picturePoissonPoly
			// 
			this.picturePoissonPoly.Location = new System.Drawing.Point( 6, 28 );
			this.picturePoissonPoly.Name = "picturePoissonPoly";
			this.picturePoissonPoly.Size = new System.Drawing.Size( 436, 204 );
			this.picturePoissonPoly.TabIndex = 0;
			this.picturePoissonPoly.TabStop = false;
			this.picturePoissonPoly.Paint += new System.Windows.Forms.PaintEventHandler( this.picturePoissonPoly_Paint );
			// 
			// tabPageRegular
			// 
			this.tabPageRegular.Controls.Add( this.groupRegularEmpStats );
			this.tabPageRegular.Controls.Add( this.groupRegularTheorStats );
			this.tabPageRegular.Controls.Add( this.pictureBox3 );
			this.tabPageRegular.Controls.Add( this.pictureBox2 );
			this.tabPageRegular.Controls.Add( this.label4 );
			this.tabPageRegular.Controls.Add( this.label3 );
			this.tabPageRegular.Controls.Add( this.buttonRegularRedraw );
			this.tabPageRegular.Controls.Add( this.groupRegularParam );
			this.tabPageRegular.Controls.Add( this.pictureRegularDistrFunc );
			this.tabPageRegular.Controls.Add( this.pictureRegularDens );
			this.tabPageRegular.Location = new System.Drawing.Point( 4, 22 );
			this.tabPageRegular.Name = "tabPageRegular";
			this.tabPageRegular.Padding = new System.Windows.Forms.Padding( 3 );
			this.tabPageRegular.Size = new System.Drawing.Size( 781, 622 );
			this.tabPageRegular.TabIndex = 1;
			this.tabPageRegular.Text = "�����������";
			this.tabPageRegular.UseVisualStyleBackColor = true;
			// 
			// groupRegularEmpStats
			// 
			this.groupRegularEmpStats.Controls.Add( this.textRegularEmpDX );
			this.groupRegularEmpStats.Controls.Add( this.label12 );
			this.groupRegularEmpStats.Controls.Add( this.textRegularEmpMX );
			this.groupRegularEmpStats.Controls.Add( this.label13 );
			this.groupRegularEmpStats.ForeColor = System.Drawing.Color.Red;
			this.groupRegularEmpStats.Location = new System.Drawing.Point( 227, 491 );
			this.groupRegularEmpStats.Name = "groupRegularEmpStats";
			this.groupRegularEmpStats.Size = new System.Drawing.Size( 215, 96 );
			this.groupRegularEmpStats.TabIndex = 7;
			this.groupRegularEmpStats.TabStop = false;
			this.groupRegularEmpStats.Text = "�������������� �������������� (������������)";
			// 
			// textRegularEmpDX
			// 
			this.textRegularEmpDX.BackColor = System.Drawing.SystemColors.Window;
			this.textRegularEmpDX.Location = new System.Drawing.Point( 90, 64 );
			this.textRegularEmpDX.Name = "textRegularEmpDX";
			this.textRegularEmpDX.ReadOnly = true;
			this.textRegularEmpDX.Size = new System.Drawing.Size( 63, 20 );
			this.textRegularEmpDX.TabIndex = 0;
			this.textRegularEmpDX.TabStop = false;
			this.textRegularEmpDX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.ForeColor = System.Drawing.SystemColors.WindowText;
			this.label12.Location = new System.Drawing.Point( 48, 67 );
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size( 35, 13 );
			this.label12.TabIndex = 1;
			this.label12.Text = "DX =";
			// 
			// textRegularEmpMX
			// 
			this.textRegularEmpMX.BackColor = System.Drawing.SystemColors.Window;
			this.textRegularEmpMX.Location = new System.Drawing.Point( 90, 38 );
			this.textRegularEmpMX.Name = "textRegularEmpMX";
			this.textRegularEmpMX.ReadOnly = true;
			this.textRegularEmpMX.Size = new System.Drawing.Size( 63, 20 );
			this.textRegularEmpMX.TabIndex = 0;
			this.textRegularEmpMX.TabStop = false;
			this.textRegularEmpMX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.ForeColor = System.Drawing.SystemColors.WindowText;
			this.label13.Location = new System.Drawing.Point( 48, 41 );
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size( 36, 13 );
			this.label13.TabIndex = 1;
			this.label13.Text = "MX =";
			// 
			// groupRegularTheorStats
			// 
			this.groupRegularTheorStats.Controls.Add( this.textRegularTheorDX );
			this.groupRegularTheorStats.Controls.Add( this.textRegularTheorMX );
			this.groupRegularTheorStats.Controls.Add( this.label14 );
			this.groupRegularTheorStats.Controls.Add( this.label15 );
			this.groupRegularTheorStats.Location = new System.Drawing.Point( 6, 491 );
			this.groupRegularTheorStats.Name = "groupRegularTheorStats";
			this.groupRegularTheorStats.Size = new System.Drawing.Size( 215, 96 );
			this.groupRegularTheorStats.TabIndex = 6;
			this.groupRegularTheorStats.TabStop = false;
			this.groupRegularTheorStats.Text = "�������������� �������������� (�������������)";
			// 
			// textRegularTheorDX
			// 
			this.textRegularTheorDX.BackColor = System.Drawing.SystemColors.Window;
			this.textRegularTheorDX.Location = new System.Drawing.Point( 88, 64 );
			this.textRegularTheorDX.Name = "textRegularTheorDX";
			this.textRegularTheorDX.ReadOnly = true;
			this.textRegularTheorDX.Size = new System.Drawing.Size( 63, 20 );
			this.textRegularTheorDX.TabIndex = 0;
			this.textRegularTheorDX.TabStop = false;
			this.textRegularTheorDX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textRegularTheorMX
			// 
			this.textRegularTheorMX.BackColor = System.Drawing.SystemColors.Window;
			this.textRegularTheorMX.Location = new System.Drawing.Point( 88, 38 );
			this.textRegularTheorMX.Name = "textRegularTheorMX";
			this.textRegularTheorMX.ReadOnly = true;
			this.textRegularTheorMX.Size = new System.Drawing.Size( 63, 20 );
			this.textRegularTheorMX.TabIndex = 0;
			this.textRegularTheorMX.TabStop = false;
			this.textRegularTheorMX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.ForeColor = System.Drawing.SystemColors.WindowText;
			this.label14.Location = new System.Drawing.Point( 46, 67 );
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size( 35, 13 );
			this.label14.TabIndex = 1;
			this.label14.Text = "DX =";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.ForeColor = System.Drawing.SystemColors.WindowText;
			this.label15.Location = new System.Drawing.Point( 46, 41 );
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size( 36, 13 );
			this.label15.TabIndex = 1;
			this.label15.Text = "MX =";
			// 
			// pictureBox3
			// 
			this.pictureBox3.Image = ( ( System.Drawing.Image )( resources.GetObject( "pictureBox3.Image" ) ) );
			this.pictureBox3.Location = new System.Drawing.Point( 450, 261 );
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size( 324, 204 );
			this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox3.TabIndex = 4;
			this.pictureBox3.TabStop = false;
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = ( ( System.Drawing.Image )( resources.GetObject( "pictureBox2.Image" ) ) );
			this.pictureBox2.Location = new System.Drawing.Point( 450, 28 );
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size( 324, 204 );
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox2.TabIndex = 4;
			this.pictureBox2.TabStop = false;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label4.Location = new System.Drawing.Point( 447, 245 );
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size( 163, 13 );
			this.label4.TabIndex = 3;
			this.label4.Text = "������� ������������� :";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.label3.Location = new System.Drawing.Point( 447, 12 );
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size( 157, 13 );
			this.label3.TabIndex = 3;
			this.label3.Text = "��������� ����������� :";
			// 
			// buttonRegularRedraw
			// 
			this.buttonRegularRedraw.Location = new System.Drawing.Point( 662, 593 );
			this.buttonRegularRedraw.Name = "buttonRegularRedraw";
			this.buttonRegularRedraw.Size = new System.Drawing.Size( 112, 23 );
			this.buttonRegularRedraw.TabIndex = 7;
			this.buttonRegularRedraw.Text = "������������";
			this.buttonRegularRedraw.UseVisualStyleBackColor = true;
			this.buttonRegularRedraw.Click += new System.EventHandler( this.buttonRegularRedraw_Click );
			// 
			// groupRegularParam
			// 
			this.groupRegularParam.Controls.Add( this.label16 );
			this.groupRegularParam.Controls.Add( this.numericRegularExpCount );
			this.groupRegularParam.Controls.Add( this.label7 );
			this.groupRegularParam.Controls.Add( this.textB );
			this.groupRegularParam.Controls.Add( this.label6 );
			this.groupRegularParam.Controls.Add( this.textA );
			this.groupRegularParam.ForeColor = System.Drawing.SystemColors.ControlText;
			this.groupRegularParam.Location = new System.Drawing.Point( 450, 491 );
			this.groupRegularParam.Name = "groupRegularParam";
			this.groupRegularParam.Size = new System.Drawing.Size( 324, 96 );
			this.groupRegularParam.TabIndex = 2;
			this.groupRegularParam.TabStop = false;
			this.groupRegularParam.Text = "���������";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point( 48, 62 );
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size( 152, 13 );
			this.label16.TabIndex = 9;
			this.label16.Text = "���������� ��������� :";
			// 
			// numericRegularExpCount
			// 
			this.numericRegularExpCount.Location = new System.Drawing.Point( 206, 60 );
			this.numericRegularExpCount.Maximum = new decimal( new int[] {
            100000,
            0,
            0,
            0} );
			this.numericRegularExpCount.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
			this.numericRegularExpCount.Name = "numericRegularExpCount";
			this.numericRegularExpCount.Size = new System.Drawing.Size( 63, 20 );
			this.numericRegularExpCount.TabIndex = 6;
			this.numericRegularExpCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.numericRegularExpCount.Value = new decimal( new int[] {
            20,
            0,
            0,
            0} );
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point( 175, 25 );
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size( 25, 13 );
			this.label7.TabIndex = 7;
			this.label7.Text = "b =";
			// 
			// textB
			// 
			this.textB.Location = new System.Drawing.Point( 206, 22 );
			this.textB.Name = "textB";
			this.textB.Size = new System.Drawing.Size( 63, 20 );
			this.textB.TabIndex = 5;
			this.textB.Text = "3,5";
			this.textB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point( 53, 25 );
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size( 25, 13 );
			this.label6.TabIndex = 5;
			this.label6.Text = "a =";
			// 
			// textA
			// 
			this.textA.Location = new System.Drawing.Point( 84, 22 );
			this.textA.Name = "textA";
			this.textA.Size = new System.Drawing.Size( 63, 20 );
			this.textA.TabIndex = 4;
			this.textA.Text = "1,0";
			this.textA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// pictureRegularDistrFunc
			// 
			this.pictureRegularDistrFunc.Location = new System.Drawing.Point( 6, 261 );
			this.pictureRegularDistrFunc.Name = "pictureRegularDistrFunc";
			this.pictureRegularDistrFunc.Size = new System.Drawing.Size( 436, 204 );
			this.pictureRegularDistrFunc.TabIndex = 1;
			this.pictureRegularDistrFunc.TabStop = false;
			this.pictureRegularDistrFunc.Paint += new System.Windows.Forms.PaintEventHandler( this.pictureRegularDistrFunc_Paint );
			// 
			// pictureRegularDens
			// 
			this.pictureRegularDens.Location = new System.Drawing.Point( 6, 28 );
			this.pictureRegularDens.Name = "pictureRegularDens";
			this.pictureRegularDens.Size = new System.Drawing.Size( 436, 204 );
			this.pictureRegularDens.TabIndex = 1;
			this.pictureRegularDens.TabStop = false;
			this.pictureRegularDens.Paint += new System.Windows.Forms.PaintEventHandler( this.pictureRegularDens_Paint );
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 812, 674 );
			this.Controls.Add( this.tabControlDistributions );
			this.Name = "Form1";
			this.Text = "�������������";
			this.tabControlDistributions.ResumeLayout( false );
			this.tabPagePoisson.ResumeLayout( false );
			this.tabPagePoisson.PerformLayout();
			this.groupPoissonEmpStats.ResumeLayout( false );
			this.groupPoissonEmpStats.PerformLayout();
			this.groupPoissonTheorStats.ResumeLayout( false );
			this.groupPoissonTheorStats.PerformLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureBox4 ) ).EndInit();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureBox1 ) ).EndInit();
			this.groupPoissonParam.ResumeLayout( false );
			this.groupPoissonParam.PerformLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.numericPoissonExpCount ) ).EndInit();
			( ( System.ComponentModel.ISupportInitialize )( this.picturePoissonDistrFunc ) ).EndInit();
			( ( System.ComponentModel.ISupportInitialize )( this.picturePoissonPoly ) ).EndInit();
			this.tabPageRegular.ResumeLayout( false );
			this.tabPageRegular.PerformLayout();
			this.groupRegularEmpStats.ResumeLayout( false );
			this.groupRegularEmpStats.PerformLayout();
			this.groupRegularTheorStats.ResumeLayout( false );
			this.groupRegularTheorStats.PerformLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureBox3 ) ).EndInit();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureBox2 ) ).EndInit();
			this.groupRegularParam.ResumeLayout( false );
			this.groupRegularParam.PerformLayout();
			( ( System.ComponentModel.ISupportInitialize )( this.numericRegularExpCount ) ).EndInit();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureRegularDistrFunc ) ).EndInit();
			( ( System.ComponentModel.ISupportInitialize )( this.pictureRegularDens ) ).EndInit();
			this.ResumeLayout( false );

		}

		#endregion

		private System.Windows.Forms.TabControl tabControlDistributions;
		private System.Windows.Forms.TabPage tabPagePoisson;
		private System.Windows.Forms.TabPage tabPageRegular;
		private System.Windows.Forms.PictureBox picturePoissonPoly;
		private System.Windows.Forms.PictureBox pictureRegularDens;
		private System.Windows.Forms.PictureBox picturePoissonDistrFunc;
		private System.Windows.Forms.PictureBox pictureRegularDistrFunc;
		private System.Windows.Forms.GroupBox groupPoissonParam;
		private System.Windows.Forms.GroupBox groupRegularParam;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textLambda;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.PictureBox pictureBox4;
		private System.Windows.Forms.Button buttonPoissonRedraw;
		private System.Windows.Forms.Button buttonRegularRedraw;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textB;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textA;
		private System.Windows.Forms.GroupBox groupPoissonTheorStats;
		private System.Windows.Forms.GroupBox groupPoissonEmpStats;
		private System.Windows.Forms.TextBox textPoissonEmpMX;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox textPoissonEmpDX;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox textPoissonTheorDX;
		private System.Windows.Forms.TextBox textPoissonTheorMX;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.GroupBox groupRegularEmpStats;
		private System.Windows.Forms.TextBox textRegularEmpDX;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox textRegularEmpMX;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.GroupBox groupRegularTheorStats;
		private System.Windows.Forms.TextBox textRegularTheorDX;
		private System.Windows.Forms.TextBox textRegularTheorMX;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.NumericUpDown numericRegularExpCount;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.NumericUpDown numericPoissonExpCount;
	}
}

