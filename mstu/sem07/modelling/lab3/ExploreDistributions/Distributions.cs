using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using ExploreDistributions.GraphDrawer;



namespace ExploreDistributions.Distributions
{
	public class Experiments : List< float > { }


	public interface IGraph
	{
		PointsList GetPoints();


		RectangleF Bounds
		{
			get;
		}
	}

	public interface IDistribution
	{
		float MX
		{
			get;
		}

		float DX
		{
			get;
		}
	}



	public abstract class Poisson
	{
		public float Lambda;

		protected int _minK;
		protected int _maxK;
		protected float _maxP;


		public Poisson( float lambda )
		{
			Lambda = lambda;
			BoundaryValues();
		}


		private float MaxPk()
		{
			float Pk = ( float )Math.Exp( -Lambda );

			float prevPk = Pk;

			int i;
			for ( i = 1; ; i++ )
			{
				Pk *= Lambda / i;

			if ( Pk < prevPk ) break;

				prevPk = Pk;
			}

			return ( prevPk );
		}

		private void BoundaryValues()
		{
			_maxP = MaxPk();
			float limitP = _maxP / 1000.0F;

			float Pk = ( float )Math.Exp( -Lambda );
			int i;
			for ( i = 0; Pk < limitP; )
				Pk *= Lambda / ++i;

			_minK = i;


			for ( ; Pk >= limitP; )
				Pk *= Lambda / ++i;

			_maxK = i;
		}
	}

	public class PoissonTheor : Poisson, IDistribution
	{
		public PoissonTheor( float lambda ) : base( lambda ) { }


		public float MX
		{
			get { return ( Lambda ); }
		}

		public float DX
		{
			get { return ( Lambda ); }
		}
	}
	
	public class PoissonTheorDistrFunc : Poisson, IGraph
	{
		public PoissonTheorDistrFunc( float lambda ) : base( lambda ) {}


		public PointsList GetPoints()
		{
			PointsList points = new PointsList();

			float Pk = ( float )Math.Exp( -Lambda );
			float Fk = Pk;
			if ( 0 >= _minK )
				points.Add( new PointF( 0, Fk ) );

			for ( int i = 1; i <= _maxK; i++ )
			{
				if ( i >= _minK )
					points.Add( new PointF( ( float )i, Fk ) );

				Pk *= Lambda / i;
				Fk += Pk;

				if ( i >= _minK )
					points.Add( new PointF( ( float )i, Fk ) );
			}

			return ( points );
		}

		public RectangleF Bounds
		{
			get
			{
				float width = ( float )( _maxK - _minK );
				float height = 1.0F;

				PointF point = new PointF( ( float )_minK, 0.0F );
				SizeF size = new SizeF( width, 1.2F * height );
				return new RectangleF( point, size );
			}
		}
	}
	
	public class PoissonTheorPolygon : Poisson, IGraph
	{
		public PoissonTheorPolygon( float lambda ) : base( lambda ) {}


		public PointsList GetPoints()
		{
			PointsList points = new PointsList();

			float Pk = ( float )Math.Exp( -Lambda );
			if ( 0 >= _minK )
				points.Add( new PointF( 0, Pk ) );

			for ( int i = 1; i <= _maxK; i++ )
			{
				Pk *= Lambda / i;

				if ( i >= _minK )
					points.Add( new PointF( ( float )i, Pk ) );
			}

			return ( points );
		}

		public RectangleF Bounds
		{
			get
			{
				float width = ( float )( _maxK - _minK );
				float height = _maxP;

				PointF point = new PointF( ( float )_minK, 0.0F );
				SizeF size = new SizeF( width, 1.2F * height );
				return new RectangleF( point, size );
			}
		}
	}
	

	public class PoissonExperiments : Experiments
	{
		private float _p = 0.001F;


		public PoissonExperiments( float lambda, int count )
		{
			base.Capacity = count;
			Generate( lambda );
		}


		public void Generate( float lambda )
		{
			Random random = new Random();

			for ( int i = 0; i < base.Capacity; i++ )
			{
				int nextValue = GenerateNext( random, lambda );
				base.Add( ( float )nextValue );
			}

			base.Sort();
		}

		public int GenerateNext( Random random, float lambda )
		{
			int n = ( int )Math.Round( lambda / _p );

			int s = 0;
			for ( ; s == 0; )
			{
				for ( int i = 0; i < n; i++ )
				{
					float r = ( float )random.NextDouble();
					if ( r < _p ) s++;
				}
			}

			return ( s );
		}
	}

	public class PoissonEmp : Poisson, IDistribution
	{
		public PoissonExperiments X;


		public PoissonEmp( float lambda, int count ) : base( lambda )
		{
			X = new PoissonExperiments( lambda, count );
		}


		public float MX
		{
			get
			{
				float sum = 0.0F;

				for ( int i = 0; i < X.Count; i++ )
					sum += X[ i ];

				return ( sum / X.Count );
			}
		}

		public float DX
		{
			get
			{
				float mx = this.MX;

				float sum = 0.0F;
				for ( int i = 0; i < X.Count; i++ )
					sum += ( float )Math.Pow( X[ i ] - mx, 2 );

				return ( sum / X.Count );
			}
		}
	}

	public class PoissonEmpDistrFunc : Poisson, IGraph
	{
		protected PoissonEmp _empirical;


		public PoissonEmpDistrFunc( PoissonEmp empirical ) : base( empirical.Lambda )
		{
			_empirical = empirical;
		}


		public PointsList GetPoints()
		{
			PoissonExperiments x = _empirical.X;

			RectangleF drawBounds = this.Bounds;

			PointsList points = new PointsList();

			points.Add( drawBounds.Location );

			for ( int i = 0; i < x.Count; i++ )
			{
				// skip consecutive equal values.
				int k = i;
				for ( ; i + 1 < x.Count && x[ i + 1 ] == x[ k ]; i++ )
					;

				float y = ( i + 1 ) / ( float )x.Count;
				points.Add( new PointF( x[ i ], points[ points.Count - 1 ].Y ) );
				points.Add( new PointF( x[ i ], y ) );
			}

			points.Add( new PointF( drawBounds.X + drawBounds.Width, 1.0F ) );

			return ( points );
		}

		public RectangleF Bounds
		{
			get
			{
				float width = ( float )( _maxK - _minK );
				float height = 1.0F;

				PointF point = new PointF( ( float )_minK, 0.0F );
				SizeF size = new SizeF( width, 1.2F * height );
				return new RectangleF( point, size );
			}
		}
	}



	public abstract class Regular
	{
		public float A;
		public float B;


		public Regular( float a, float b )
		{
			A = a;
			B = b;
		}
	}

	public class RegularTheor : Regular, IDistribution
	{
		public RegularTheor( float a, float b ) : base( a, b ) {}


		public float MX
		{
			get { return ( A + B ) / 2.0F; }
		}

		public float DX
		{
			get { return ( float )Math.Pow( B - A, 2 ) / 12.0F; }
		}
	}

	public class RegularTheorDistrFunc : Regular, IGraph
	{
		public RegularTheorDistrFunc( float a, float b ) : base( a, b ) {}


		public PointsList GetPoints()
		{
			RectangleF drawBounds = this.Bounds;

			PointsList points = new PointsList();

			points.Add( drawBounds.Location );
			points.Add( new PointF( A, 0.0F ) );
			points.Add( new PointF( B, 1.0F ) );
			points.Add( new PointF( drawBounds.X + drawBounds.Width, 1.0F ) );

			return ( points );
		}

		public RectangleF Bounds
		{
			get
			{
				float width = B - A;
				float height = 1.0F;

				PointF point = new PointF( A - width / 2, 0.0F );
				SizeF size = new SizeF( 2 * width, 1.2F * height );
				return new RectangleF( point, size );
			}
		}
	}

	public class RegularTheorDensFunc : Regular, IGraph
	{
		public RegularTheorDensFunc( float a, float b ) : base( a, b ) {}


		public PointsList GetPoints()
		{
			RectangleF drawBounds = this.Bounds;

			float height = 1.0F / ( B - A );

			PointsList points = new PointsList();

			points.Add( drawBounds.Location );
			points.Add( new PointF( A, 0.0F ) );
			points.Add( new PointF( A, height ) );
			points.Add( new PointF( B, height ) );
			points.Add( new PointF( B, 0.0F ) );
			points.Add( new PointF( drawBounds.X + drawBounds.Width, 0.0F ) );

			return ( points );
		}

		public RectangleF Bounds
		{
			get
			{
				float width = B - A;
				float height = 1.0F / ( B - A );

				PointF point = new PointF( A - width / 2, 0.0F );
				SizeF size = new SizeF( 2 * width, 1.2F * height );
				return new RectangleF( point, size );
			}
		}
	}


	public class RegularExperiments : Experiments
	{
		public RegularExperiments( float a, float b, int count )
		{
			base.Capacity = count;
			Generate( a, b );
		}


		public void Generate( float a, float b )
		{
			Random random = new Random();

			for ( int i = 0; i < base.Capacity; i++ )
			{
				float nextValue = a + ( b - a ) * ( float )random.NextDouble();
				base.Add( nextValue );
			}

			base.Sort();
		}
	}

	public class RegularEmp : Regular, IDistribution
	{
		public RegularExperiments X;


		public RegularEmp( float a, float b, int count ) : base( a, b )
		{
			X = new RegularExperiments( a, b, count );
		}


		public float MX
		{
			get
			{
				float sum = 0.0F;

				for ( int i = 0; i < X.Count; i++ )
					sum += X[ i ];

				return ( sum / X.Count );
			}
		}

		public float DX
		{
			get
			{
				float mx = this.MX;

				float sum = 0.0F;
				for ( int i = 0; i < X.Count; i++ )
					sum += ( float )Math.Pow( X[ i ] - mx, 2 );

				return ( sum / X.Count );
			}
		}
	}
	
	public class RegularEmpDistrFunc : Regular, IGraph
	{
		protected RegularEmp _empirical;


		public RegularEmpDistrFunc( RegularEmp empirical ) : base( empirical.A, empirical.B )
		{
			_empirical = empirical;
		}


		public PointsList GetPoints()
		{
			RegularExperiments x = _empirical.X;

			RectangleF drawBounds = this.Bounds;

			PointsList points = new PointsList();

			points.Add( drawBounds.Location );

			for ( int i = 0; i < x.Count; i++ )
			{
				// skip consecutive equal values.
				int k = i;
				for ( ; i + 1 < x.Count && x[ i + 1 ] == x[ k ]; i++ );

				float y = ( i + 1 ) / ( float )x.Count;
				points.Add( new PointF( x[ i ], points[ points.Count - 1 ].Y ) );
				points.Add( new PointF( x[ i ], y ) );
			}

			points.Add( new PointF( drawBounds.X + drawBounds.Width, 1.0F ) );

			return ( points );
		}

		public RectangleF Bounds
		{
			get
			{
				float width = B - A;
				float height = 1.0F;

				PointF point = new PointF( A - width / 2, 0.0F );
				SizeF size = new SizeF( 2 * width, 1.2F * height );
				return new RectangleF( point, size );
			}
		}
	}
}