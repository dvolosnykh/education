using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ExploreDistributions.GraphDrawer;
using ExploreDistributions.Distributions;


namespace ExploreDistributions
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			buttonPoissonRedraw_Click( buttonPoissonRedraw, EventArgs.Empty );
			buttonRegularRedraw_Click( buttonRegularRedraw, EventArgs.Empty );
		}


		private float _lambda;
		private PoissonEmp _poissonEmp;

		private void GetLambda()
		{
			bool ok = true;

			try
			{
				float lambda = Convert.ToSingle( textLambda.Text );
				ok = ( 0.0F < lambda && lambda <= 100.0F );
				_lambda = lambda;
			}
			catch
			{
				ok = false;
			}

			if ( !ok )
			{
				MessageBox.Show( "�������� � ������ ���� ������������ ������������� ������, �� ����� 100.",
						"������", MessageBoxButtons.OK, MessageBoxIcon.Stop );
			}
		}

		private void picturePoissonPoly_Paint( object sender, PaintEventArgs e )
		{
			Drawer gd = new Drawer( e.Graphics, e.ClipRectangle );
			PoissonTheorPolygon graphTheor = new PoissonTheorPolygon( _lambda );
			gd.DrawGraph( graphTheor, false, "P(k)" );
		}

		private void picturePoissonDistrFunc_Paint( object sender, PaintEventArgs e )
		{
			Drawer gd = new Drawer( e.Graphics, e.ClipRectangle );
			PoissonTheorDistrFunc graphTheor = new PoissonTheorDistrFunc( _lambda );
			gd.DrawGraph( graphTheor, false, "F(k)" );

			gd.Options.GraphPen = new Pen( Color.Red, 3.0F );
			PoissonEmpDistrFunc graphEmp = new PoissonEmpDistrFunc( _poissonEmp );
			gd.DrawGraph( graphEmp, true );
		}

		private void buttonPoissonRedraw_Click( object sender, EventArgs e )
		{
			GetLambda();

			PoissonTheor poisson = new PoissonTheor( _lambda );
			textPoissonTheorMX.Text = poisson.MX.ToString( "N2" );
			textPoissonTheorDX.Text = poisson.DX.ToString( "N2" );


			int count = ( int )numericPoissonExpCount.Value;
			_poissonEmp = new PoissonEmp( _lambda, count );
			textPoissonEmpMX.Text = _poissonEmp.MX.ToString( "N2" );
			textPoissonEmpDX.Text = _poissonEmp.DX.ToString( "N2" );

			picturePoissonPoly.Refresh();
			picturePoissonDistrFunc.Refresh();
		}



		private float _a;
		private float _b;
		private RegularEmp _regularEmp;

		private void GetAB()
		{
			bool ok = true;

			try
			{
				float a = Convert.ToSingle( textA.Text );
				float b = Convert.ToSingle( textB.Text );
				ok = ( a < b );
				_a = a;
				_b = b;
			}
			catch
			{
				ok = false;
			}

			if ( !ok )
			{
				MessageBox.Show( "��������� a � b ������ ���� ������������� �������, ������ a < b.",
					"������", MessageBoxButtons.OK, MessageBoxIcon.Stop );
			}
		}

		private void pictureRegularDens_Paint( object sender, PaintEventArgs e )
		{
			Drawer gd = new Drawer( e.Graphics, e.ClipRectangle );
			RegularTheorDensFunc graphTheor = new RegularTheorDensFunc( _a, _b );
			gd.DrawGraph( graphTheor, false, "f(x)" );
/*
			gd.Options.GraphPen = new Pen ( Color.Red, 3.0F );
			RegularEmpPoly graphEmp = new RegularEmpPoly( _regularEmp );
			gd.DrawGraph( graphEmp, true );
*/		}

		private void pictureRegularDistrFunc_Paint( object sender, PaintEventArgs e )
		{
			Drawer gd = new Drawer( e.Graphics, e.ClipRectangle );
			RegularTheorDistrFunc graphTheor = new RegularTheorDistrFunc( _a, _b );
			gd.DrawGraph( graphTheor, false, "F(x)" );

			gd.Options.GraphPen = new Pen( Color.Red, 3.0F );
			RegularEmpDistrFunc graphEmp = new RegularEmpDistrFunc( _regularEmp );
			gd.DrawGraph( graphEmp, true );
		}

		private void buttonRegularRedraw_Click( object sender, EventArgs e )
		{
			GetAB();

			RegularTheor regularTheor = new RegularTheor( _a, _b );
			textRegularTheorMX.Text = regularTheor.MX.ToString( "N2" );
			textRegularTheorDX.Text = regularTheor.DX.ToString( "N2" );

			int count = ( int )numericRegularExpCount.Value;
			_regularEmp = new RegularEmp( _a, _b, count );
			textRegularEmpMX.Text = _regularEmp.MX.ToString( "N2" );
			textRegularEmpDX.Text = _regularEmp.DX.ToString( "N2" );

			pictureRegularDens.Refresh();
			pictureRegularDistrFunc.Refresh();
		}
	}
}