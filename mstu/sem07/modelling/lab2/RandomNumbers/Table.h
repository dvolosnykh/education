#pragma once



class CTable
{
protected:
	FILE * _f;
public:
	CString Filename;

public:
	CTable( CString filename );
	virtual ~CTable();

	bool Init();
	void Deinit();

	bool Loaded() { return ( _f != NULL ); };

	virtual int Integer( const int digitsNum ) = NULL;
};