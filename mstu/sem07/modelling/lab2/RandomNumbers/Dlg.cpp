#include "stdafx.h"

#include "App.h"
#include "Dlg.h"
#include "Normal.h"
#include "Regular.h"
#include "Criteria.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CDlg dialog




CDlg::CDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_ALGO_GENERATE, &CDlg::GenerateByAlgoMethod)
	ON_BN_CLICKED(IDC_BUTTON_TABLE_GENERATE, &CDlg::GenerateByTableMethod)
END_MESSAGE_MAP()


// CDlg message handlers

void CDlg::InitListCtrl( CListCtrl * pListCtrl, const int count )
{
	CRect rect;
	pListCtrl->GetClientRect( &rect );
	const int width = ( rect.Width() - 15 ) / MAX_DIGITS_NUM;

	for ( int i = 1; i <= MAX_DIGITS_NUM; i++ )
	{
		CString text;
		text.Format( _T( "%i �." ), i );
		pListCtrl->InsertColumn( i, text, LVCFMT_RIGHT, width );
	}

	for ( int j = 0; j < count; j++ )
		pListCtrl->InsertItem( j, NULL );
}

BOOL CDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	InitListCtrl( ( CListCtrl * )GetDlgItem( IDC_LIST_TABLE_SEQ ), MAX_NUMBERS_COUNT );
	InitListCtrl( ( CListCtrl * )GetDlgItem( IDC_LIST_ALGO_SEQ ), MAX_NUMBERS_COUNT );
	InitListCtrl( ( CListCtrl * )GetDlgItem( IDC_LIST_TABLE_CRITERIA ), 1 );
	InitListCtrl( ( CListCtrl * )GetDlgItem( IDC_LIST_ALGO_CRITERIA ), 1 );

	GenerateByTableMethod();
	GenerateByAlgoMethod();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CDlg::GenerateByTableMethod()
{
	if ( NormalLaw.Table.Loaded() )
	{
		CListCtrl & listValues = *( CListCtrl * )GetDlgItem( IDC_LIST_TABLE_SEQ );
		CListCtrl & listCriteria = *( CListCtrl * )GetDlgItem( IDC_LIST_TABLE_CRITERIA );

		for ( int digitsNum = 1, minValue = 0, maxLimit = 10;
			digitsNum <= MAX_DIGITS_NUM;
			digitsNum++, minValue = maxLimit, maxLimit = minValue * 10 )
		{
			int * value = new int [ MAX_NUMBERS_COUNT ];

			// generate sequence.
			for ( register int i = 0; i < MAX_NUMBERS_COUNT; i++ )
			{
				int & curValue = value[ i ];

				curValue = NormalLaw.Table.Integer( digitsNum );

				CString strValue;
				strValue.Format( _T( "%i" ), curValue );
				listValues.SetItemText( i, digitsNum - 1, strValue );
			}

			// evaluate criteria.
			const int curCriteria = Criteria( value, MAX_NUMBERS_COUNT, minValue, maxLimit, NormalLaw.Distribution );
			delete value;

			CString strCriteria;
			strCriteria.Format( _T( "%i" ), curCriteria );
			listCriteria.SetItemText( 0, digitsNum - 1, strCriteria );
		}
	}
	else
	{
		CString text;
		text.Format( _T( "���� \"%s\" �� ������ ��� �������� ������." ), NormalLaw.Table.Filename );
		MessageBox( text, _T( "������." ), MB_OK | MB_ICONERROR );
	}
}

void CDlg::GenerateByAlgoMethod()
{
	CListCtrl & listValues = *( CListCtrl * )GetDlgItem( IDC_LIST_ALGO_SEQ );
	CListCtrl & listCriteria = *( CListCtrl * )GetDlgItem( IDC_LIST_ALGO_CRITERIA );

	for ( int digitsNum = 1, minValue = 0, maxLimit = 10;
		digitsNum <= MAX_DIGITS_NUM;
		digitsNum++, minValue = maxLimit, maxLimit = minValue * 10 )
	{
		int * value = new int [ MAX_NUMBERS_COUNT ];

		// generate sequence.
		for ( register int i = 0; i < MAX_NUMBERS_COUNT; i++ )
		{
			int & curValue = value[ i ];

			curValue = NormalLaw.Generator.Integer( minValue, maxLimit );

			CString strValue;
			strValue.Format( _T( "%i" ), curValue );
			listValues.SetItemText( i, digitsNum - 1, strValue );
		}

		// evaluate criteria.
		const int curCriteria = Criteria( value, MAX_NUMBERS_COUNT, minValue, maxLimit, NormalLaw.Distribution );
		delete value;

		CString strCriteria;
		strCriteria.Format( _T( "%i" ), curCriteria );
		listCriteria.SetItemText( 0, digitsNum - 1, strCriteria );
	}
}