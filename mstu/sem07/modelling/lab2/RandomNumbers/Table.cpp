#include "stdafx.h"

#include "Table.h"



CTable::CTable( CString filename )
{
	Filename = filename;
	Init();
}

CTable::~CTable()
{
	Deinit();
}

// open file with table.
bool CTable::Init()
{
	_f = _tfopen( Filename, _T( "r" ) );

	bool ok = ( _f != NULL );

	if ( ok )
	{
		fseek( _f, 0, SEEK_END );
		const long len = ftell( _f );
		fseek( _f, 0, SEEK_SET );

		ok = len > 0;
	}

	return ( ok );
}

// close file with table.
void CTable::Deinit()
{
	if ( Loaded() )
		fclose( _f );
}