#pragma once



class CGenerator
{
public:
	CGenerator();

	void Init();

	virtual int Integer( const int minValue, const int maxLimit ) = NULL;
};