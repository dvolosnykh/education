#pragma once


typedef double ( * DistrFunc )( double x, double a, double b );


int Criteria( const int * value, const int n, const int a, const int b, DistrFunc Distr );