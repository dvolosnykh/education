#pragma once

#include "Table.h"
#include "Generator.h"



class CRegularTable : public CTable
{
public:
	CRegularTable() : CTable( _T( "_regular.txt" ) ) {};

	int Integer( const int digitsNum );

private:
	int ReadDigits( const int digitsNum );
	int NextDigit();
	char NextChar();
};



class CRegularGenerator : public CGenerator
{
public:
	CRegularGenerator() : CGenerator() {};

	int Integer( const int minValue, const int maxLimit );

	static double R( const double a, const double b );
};



class CRegularLaw
{
public:
	CRegularTable Table;
	CRegularGenerator Generator;

	static double Distribution( double x, double a, double b );
};

extern CRegularLaw RegularLaw;