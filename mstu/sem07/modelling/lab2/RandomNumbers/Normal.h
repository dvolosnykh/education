#pragma once


#include "Table.h"
#include "Generator.h"



class CNormalTable : public CTable
{
public:
	CNormalTable() : CTable( _T( "_normal.txt" ) ) {};

	int Integer( const int digitsNum );
};



class CNormalGenerator : public CGenerator
{
public:
	CNormalGenerator() : CGenerator() {};

	int Integer( const int minValue, const int maxLimit );

	static double N( const double m, const double sigma );
};


class CNormalLaw
{
public:
	CNormalTable Table;
	CNormalGenerator Generator;

	static double Distribution( double x, double a, double b );
};

extern CNormalLaw NormalLaw;