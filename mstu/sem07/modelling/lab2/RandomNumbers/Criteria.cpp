#include "stdafx.h"

#include <math.h>
#include <float.h>

#include "Criteria.h"



int SortIncreasing( const void * pArg1, const void * pArg2 )
{
	const int & arg1 = *( const int * )pArg1;
	const int & arg2 = *( const int * )pArg2;

	return ( arg1 - arg2 );
}

// Kolmogorov-Smirnov criteria
int Criteria( const int * value, const int n, const int a, const int b, DistrFunc Distr )
{
	int * X = new int [ n ];
	memcpy( X, value, n * sizeof( int ) );

	// sort in an increasing order.
	qsort( X, n, sizeof( int ), SortIncreasing );

	// find maximum deviations.
	double maxDiffPlus = -DBL_MAX;
	double maxDiffMinus = -DBL_MAX;
	for ( register int j = 1; j <= n; j++ )
	{
		const double F = Distr( X[ j - 1 ], a, b );

		const double Gmax = j / double( n );
		if ( Gmax - F > maxDiffPlus ) maxDiffPlus = Gmax - F;

		const double Gmin = ( j - 1 ) / double( n );
		if ( F - Gmin > maxDiffMinus ) maxDiffMinus = F - Gmin;
	}

	delete X;

	// find statistics.
	const double KnPlus = maxDiffPlus;
	const double KnMinus = maxDiffMinus;
	const double Kn = max( KnPlus, KnMinus );

	// return final quality-mark.
	return ( int )( 100 * Kn );

}