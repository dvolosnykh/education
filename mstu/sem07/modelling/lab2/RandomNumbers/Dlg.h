#pragma once



#define MAX_DIGITS_NUM		3
#define MAX_NUMBERS_COUNT	10



// CDlg dialog
class CDlg : public CDialog
{
// Construction
public:
	CDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_RANDOMNUMBERS_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void GenerateByTableMethod();
	afx_msg void GenerateByAlgoMethod();

private:
	void InitListCtrl( CListCtrl * pListCtrl, const int count );

protected:
	DECLARE_MESSAGE_MAP()
};
