#include "stdafx.h"

#include "Regular.h"


CRegularLaw RegularLaw;



// random integer with a specified number of digits.
int CRegularTable::Integer( const int digitsNum )
{
	int value = ReadDigits( digitsNum );

	if ( digitsNum > 1 )
	{
		int minValue = 1;
		for ( register int i = 1; i < digitsNum; i++ )
			minValue *= 10;

		value = ( 2 * ( 10 - 1 ) * value + 10 ) / ( 2 * 10 ) + minValue;
	}

	return ( value );
}

// read specified number of digits.
int CRegularTable::ReadDigits( const int digitsNum )
{
	ASSERT( digitsNum > 0 );

	int value = 0;

	for ( int i = 1; ; i++ )
	{
		value += NextDigit();

	if ( i == digitsNum ) break;

		value *= 10;
	}

	return ( value );
}

// read digit.
int CRegularTable::NextDigit()
{
	char c;

	for ( ; !isdigit( c = NextChar() ); )
	{
		ASSERT( isspace( c ) );
	}

	return ( int )( c - TCHAR( '0' ) );
}

// read character.
char CRegularTable::NextChar()
{
	char c;

	if ( fread( &c, sizeof( c ), 1, _f ) < 1 )
	{
		fseek( _f, 0, SEEK_SET );
		fread( &c, sizeof( c ), 1, _f );
	}

	return ( c );
}



int CRegularGenerator::Integer( const int minValue, const int maxLimit )
{
	double x = R( minValue, maxLimit );
	return ( int )( x + 0.5 );
}

// random R[ a, b ].
double CRegularGenerator::R( const double a, const double b )
{
	const double x = rand() / double( RAND_MAX );
	return ( a + x * ( b - a ) );
}


// regular distribution law.
double CRegularLaw::Distribution( double x, double a, double b )
{
	return ( x - a ) / ( b - a );
}