#include <limits.h>

#include "Camera.h"
#include "../Mathematics.h"


void Camera::SetChannels( const size_t channelsCount, const size_t colorsCount )
{
	__info.Frame.ChannelsCount = channelsCount;
	__info.Frame.ColorsCount = ( colorsCount == 0 ? channelsCount : colorsCount );
}

void Camera::SetFPS( const size_t fps )
{
	__info.FPS = fps;
#define ONE_SECOND	1000	// milliseconds
	__info.FrameInterval = CEIL_DIV_UINT( ONE_SECOND, fps );
#undef ONE_SECOND
}

size_t Camera::GetFrameSize() const
{
	const size_t pixelsCount = __info.Frame.Width * __info.Frame.Height;
	const size_t channelSize = CEIL_DIV_UINT( __info.Frame.BitsPerChannel, CHAR_BIT );
	const size_t pixelSize = __info.Frame.ChannelsCount * channelSize;
	return ( pixelsCount * pixelSize );
}
