#include "CameraManager.h"
#include "../Exceptions.h"


Camera * CameraManager::_pCamera = NULL;
Event * CameraManager::_pNewFrameReady = NULL;
Event * CameraManager::_pFrameProcessed = NULL;
Event * CameraManager::_pFrameResized = NULL;
Event * CameraManager::_pCalibrationNeeded = NULL;
MultiBuffer * CameraManager::_pFrame = NULL;
MultiBuffer * CameraManager::__pResults = NULL;


CameraManager::CameraManager( Camera * const pCamera, MultiBuffer * const pFrame, Event * const pNewFrameReady, Event * const pFrameProcessed, Event * const pFrameResized, Event * const pCalibrationNeeded )
	: SyncJob()
{
	_pCamera = pCamera;
	_pNewFrameReady = pNewFrameReady;
	_pFrameProcessed = pFrameProcessed;
	_pFrameResized = pFrameResized;
	_pCalibrationNeeded = pCalibrationNeeded;	_pCalibrationNeeded->Set();
	_pFrame = pFrame;
}

void CameraManager::SaveResults( const bool success, const double yaw, const double pitch, const double roll )
{
	void * const pResults = __pResults->Lock( 0 );
	{
		bool * const pSuccess = static_cast< bool * const >( pResults );
		*pSuccess = success;

		if ( success )
		{
			double * const pAttitude = reinterpret_cast< double * const >( pSuccess + 1 );
			pAttitude[ 0 ] = yaw;
			pAttitude[ 1 ] = pitch;
			pAttitude[ 2 ] = roll;
		}
	}
	__pResults->Unlock( 0 );
}

void CameraManager::LoadResults( bool & success, double & yaw, double & pitch, double & roll )
{
	void * const pResults = __pResults->Lock( 0 );
	{
		bool * const pSuccess = static_cast< bool * const >( pResults );
		success = *pSuccess;

		if ( success )
		{
			double * const pAttitude = reinterpret_cast< double * const >( pSuccess + 1 );
			yaw = pAttitude[ 0 ];
			pitch = pAttitude[ 1 ];
			roll = pAttitude[ 2 ];
		}
	}
	__pResults->Unlock( 0 );
}

void CameraManager::_SaveFrame()
{
	void * const pFrameBuffer = _pFrame->Lock( 0 );
	{
		_pCamera->GetFrame( pFrameBuffer );
	}
	_pFrame->Unlock( 0 );
}

void CameraManager::_ChangeResolution( const size_t width, const size_t height )
{
	_pCamera->SetResolution( width, height );

	_pFrame->SetSize( _pCamera->GetFrameSize() );
	_pFrameResized->Set();
}

bool CameraManager::_Initialize()
{
	__pResults = new MultiBuffer( 1, sizeof( bool ) + 3 * sizeof( double ) );
	if ( __pResults == NULL )
		throw MemoryException( "new MultiBuffer" );

	return ( _pCamera->Initialize() );
}

void CameraManager::_Deinitialize()
{
	_pCamera->Deinitialize();
	delete __pResults;
}
