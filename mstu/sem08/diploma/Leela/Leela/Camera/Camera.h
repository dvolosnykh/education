#pragma once

#include <stddef.h>

#include "../FrameInfo.h"


class Camera
{
public:
	struct Info
	{
		FrameInfo Frame;
		size_t FPS;
		time_t FrameInterval;
		double Focus;
	};

private:
	Info __info;

protected:
	Camera() {}
	virtual ~Camera() {}

public:
	virtual void SetResolution( const size_t frameWidth, const size_t frameHeight ) { __info.Frame.Width = frameWidth, __info.Frame.Height = frameHeight; }
	virtual size_t GetFrameWidth() const { return ( __info.Frame.Width ); }
	virtual size_t GetFrameHeight() const { return ( __info.Frame.Height ); }

	virtual void SetChannels( const size_t channelsCount, const size_t colorsCount = 0 );
	virtual size_t GetChannelsCount() const { return ( __info.Frame.ChannelsCount ); }
	virtual size_t GetChannelsUsed() const { return ( __info.Frame.ColorsCount ); }

	virtual void SetBitsPerChannel( const size_t bitsPerChannel ) { __info.Frame.BitsPerChannel = bitsPerChannel; }
	virtual size_t GetBitsPerChannel() const { return ( __info.Frame.BitsPerChannel ); }

	virtual void SetFPS( const size_t fps );
	virtual size_t GetFPS() const { return ( __info.FPS ); }
	time_t GetFrameInterval() const { return ( __info.FrameInterval ); }

	virtual void SetFocus( const double focus ) { __info.Focus = focus; }
	virtual double GetFocus() const { return ( __info.Focus ); }

	size_t GetFrameSize() const;
	virtual void GetFrame( void * const pFrame ) const = 0;

	Info GetInfo() const { return ( __info ); }

	virtual bool Initialize() = 0;
	virtual void Deinitialize() = 0;
};
