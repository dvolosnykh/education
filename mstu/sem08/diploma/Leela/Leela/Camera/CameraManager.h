#pragma once

#include "Camera.h"
#include "Intercommunication.h"



class CameraManager : public SyncJob
{
protected:
	static Camera * _pCamera;
	static Event * _pNewFrameReady;
	static Event * _pFrameProcessed;
	static Event * _pFrameResized;
	static Event * _pCalibrationNeeded;
	static MultiBuffer * _pFrame;

private:
	static MultiBuffer *__pResults;

public:
	CameraManager( Camera * const pCamera, MultiBuffer * const pFrame, Event * const pNewFrameReady, Event * const pFrameProcessed, Event * const pFrameResized, Event * const pCalibrationNeeded );
	virtual ~CameraManager() {}

	Camera::Info GetCameraInfo() const { return ( _pCamera->GetInfo() ); }

	size_t GetFrameSize() const { return ( _pFrame->GetSize() ); }
	unsigned char * LockFrame() const { return static_cast< unsigned char * >( _pFrame->Lock( 1 ) ); }
	void UnlockFrame() const { _pFrame->Unlock( 1 ); }
	
	static void SaveResults( const bool success, const double yaw, const double pitch, const double roll );
	static void LoadResults( bool & success, double & yaw, double & pitch, double & roll );

protected:
	static void _SaveFrame();
	static void _ChangeResolution( const size_t width, const size_t height );

	virtual bool _Initialize();
	virtual void _Deinitialize();
};
