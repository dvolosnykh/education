#pragma once

#include "../RealCamera.h"


class VideoscanCamera : public RealCamera
{
public:
	VideoscanCamera()
		: RealCamera() {}
	virtual ~VideoscanCamera() {}

public:
	virtual void GetShapshot( void * const pSnapshot ) const {}

	virtual bool Initialize() {}
	virtual void Deinitialize() {}
};
