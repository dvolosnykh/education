#include "VirtualCamera.h"


VirtualCamera::VirtualCamera( const Info & info )
	: Camera()
{
	SetResolution( info.Frame.Width, info.Frame.Height );
	SetChannels( info.Frame.ChannelsCount, info.Frame.ColorsCount );
	SetBitsPerChannel( info.Frame.BitsPerChannel );
	SetFPS( info.FPS );
	SetFocus( info.Focus );
}
