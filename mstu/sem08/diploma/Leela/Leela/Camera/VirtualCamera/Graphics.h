#pragma once


class Graphics
{
public:
	Graphics() {}
	virtual ~Graphics() {}

public:
	virtual void BeginDrawing() = 0;
	virtual void EndDrawing() = 0;
	virtual void SetViewport( const size_t width, const size_t height ) = 0;

	virtual bool Initialize() = 0;
	virtual void Deinitialize() = 0;
};
