#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

#include "../Camera.h"
#include "../../Mathematics/Vertex.h"


class VirtualCamera : public Camera
{
public:
	enum
	{
		DEFAULT_WIDTH = 1000,
		DEFAULT_HEIGHT = 700,
		DEFAULT_FPS = 50
	};

public:
	double Angle, Distance;
	Vertex3D Target, Up;

public:
	VirtualCamera() {}
	VirtualCamera( const Info & info );
	virtual ~VirtualCamera() {}

public:
	virtual bool Initialize() { return ( true ); }
	virtual void Deinitialize() {}

	virtual void Locate() const = 0;
	virtual void SetupSight( const size_t width, const size_t height ) const = 0;

protected:
	inline static double _DegreeToRadian( const double degree )
	{
		static const double ratio = M_PI / 180.0;
		return ( degree * ratio );
	}

	inline static double _RadianToDegree( const double radian )
	{
		static const double ratio = 180.0 / M_PI;
		return ( radian * ratio );
	}
};
