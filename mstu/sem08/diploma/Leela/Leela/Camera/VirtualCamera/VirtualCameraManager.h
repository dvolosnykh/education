#pragma once

#include "../CameraManager.h"
#include "VirtualCamera.h"
#include "Scene.h"
#include "Graphics.h"

#define ANGLE		1.0
#define FAST_ANGLE	( 10 * ANGLE )


class VirtualCameraManager : public CameraManager
{
protected:
	static Scene * _pScene;
	static Graphics * _pGraphics;

public:
	VirtualCameraManager( VirtualCamera * const pCamera, MultiBuffer * const pFrame, Scene * const pScene, Event * const pNewFrameReady, Event * const pFrameProcessed, Event * const pFrameResized, Event * const pCalibrationNeeded, Graphics * const pGraphics );
	virtual ~VirtualCameraManager();

protected:
	static void _DrawFrame();

	virtual bool _Initialize();
	virtual void _Deinitialize();
};
