#pragma once

#include "../VirtualCamera.h"


class OpenGLCamera : public VirtualCamera
{
private:
	enum
	{
		__BITS_PER_CHANNEL = 8,
		__CHANNELS_COUNT = 4,
		__CHANNELS_USED = 3,
		__FOVY = 45,
		__Z_NEAR = 1,
		__Z_FAR = 200,
	};

public:
	OpenGLCamera( const size_t frameWidth, const size_t frameHeight, const size_t fps );
	virtual ~OpenGLCamera() {}

public:
	virtual void Locate() const;
	virtual void SetupSight( const size_t width, const size_t height ) const;

	virtual void GetFrame( void * const pFrame ) const;

private:
	static double __CalculateFOVY( const size_t frameHeight, const double focus );
	static double __CalculateFocus( const size_t frameHeight, const double fovy );
};
