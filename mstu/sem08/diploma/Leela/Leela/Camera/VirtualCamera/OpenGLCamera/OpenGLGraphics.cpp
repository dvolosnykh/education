#include "OpenGLGraphics.h"

#include <GL/glut.h>


void OpenGLGraphics::BeginDrawing()
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
}

void OpenGLGraphics::EndDrawing()
{
}

void OpenGLGraphics::SetViewport( const size_t width, const size_t height )
{
	glViewport( 0, 0, width, height );
}

bool OpenGLGraphics::Initialize()
{
	glPushAttrib( GL_ALL_ATTRIB_BITS );

	__InitializeParameters();
	__InitializeLighting();

	return ( true );
}

void OpenGLGraphics::Deinitialize()
{
	glPopAttrib();
}

void OpenGLGraphics::__InitializeLighting()
{
	glEnable( GL_LIGHTING );
	GLfloat ambientLight[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	glLightModelfv( GL_LIGHT_MODEL_AMBIENT, ambientLight );
	glLightModelf( GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE );

	glEnable( GL_LIGHT0 );
	GLfloat lightColor[] = { 0.9f, 0.9f, 0.9f, 1.0f };
	GLfloat lightPos[] = { 50.0f, 100.0f, 0.0f, 1.0f};
	glLightfv( GL_LIGHT0, GL_AMBIENT, lightColor );
	glLightfv( GL_LIGHT0, GL_DIFFUSE, lightColor );
	glLightfv( GL_LIGHT0, GL_SPECULAR, lightColor );
	glLightfv( GL_LIGHT0, GL_POSITION, lightPos );

	glShadeModel( GL_SMOOTH );
}

void OpenGLGraphics::__InitializeParameters()
{
	glEnable( GL_DEPTH_TEST );

	glEnable( GL_CULL_FACE );
	glCullFace( GL_BACK );
}
