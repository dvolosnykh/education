#pragma once

#include "../Graphics.h"


class OpenGLGraphics : public Graphics
{
public:
	OpenGLGraphics()
		: Graphics() {}
	virtual ~OpenGLGraphics() {}

public:
	virtual void BeginDrawing();
	virtual void EndDrawing();
	virtual void SetViewport( const size_t width, const size_t height );

	virtual bool Initialize();
	virtual void Deinitialize();

protected:
	void __InitializeLighting();
	void __InitializeParameters();
};
