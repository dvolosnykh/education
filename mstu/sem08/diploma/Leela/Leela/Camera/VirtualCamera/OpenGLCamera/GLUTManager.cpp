#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#include "GLUTManager.h"

#include <GL/glut.h>
//#include <GL/freeglut.h>

#define GAP	1


int GLUTManager::__argc = 0;
char ** GLUTManager::__argv = NULL;
GLint GLUTManager::__windowMain;
GLint GLUTManager::__windowCamera;
GLint GLUTManager::__windowSpectator;
bool GLUTManager::__retained = true;
GLvoid * GLUTManager::__pFont = GLUT_BITMAP_TIMES_ROMAN_10;


#define PITCH_MAX	30.0
#define PITCH_MIN	-20.0
#define YAW_MAX		45.0
#define YAW_MIN		-45.0
#define ROLL_MAX	0.0
#define ROLL_MIN	-0.0


GLUTManager::GLUTManager( OpenGLCamera * const pCamera, MultiBuffer * const pFrame, Scene * const pScene, Event * const pNewFrameReady, Event * const pFrameProcessed, Event * const pFrameResized, Event * const pCalibrationNeeded, int argc, char ** argv )
	: VirtualCameraManager( pCamera, pFrame, pScene, pNewFrameReady, pFrameProcessed, pFrameResized, pCalibrationNeeded, new OpenGLGraphics() )
{
	__argc = argc;
	__argv = argv;
	__windowMain = 0;
	__windowCamera = 0;
	__windowSpectator = 0;
}

void GLUTManager::_Main()
{
	glutMainLoop();
}

bool GLUTManager::_Initialize()
{
	bool ok = __InitializeWindows();
	if ( ok )
	{
		glutSetWindow( __windowCamera );
		ok = VirtualCameraManager::_Initialize();
	}

	return ( ok );
}

void GLUTManager::_Deinitialize()
{
	VirtualCameraManager::_Deinitialize();
	__DeinitializeWindows();
}

void GLUTManager::__IdleHandler()
{
#ifdef _DEBUG
	printf( "idle\n" );
#endif

	if ( _pFrameProcessed->TryWait() )
		glutPostWindowRedisplay( __windowSpectator );
}

void GLUTManager::__TimerHandler( int value )
{
#ifdef _DEBUG
	printf( "timer\n" );
#endif

	if ( __retained )
	{
		const int window = glutGetWindow();
		{
			glutSetWindow( __windowCamera );
			_SaveFrame();
		}
		glutSetWindow( window );

		_pNewFrameReady->Set();
	}

	glutTimerFunc( _pCamera->GetFrameInterval(), __TimerHandler, 0 );
}

void GLUTManager::__KeyboardHandler( const int specialKey, const unsigned char key, const bool down, const int x, const int y )
{
#define ESC		27
#define SPACE	' '

#ifdef _DEBUG
	printf( "keyboard: %u\n", glutGetWindow() );
#endif

	const int modifiers = glutGetModifiers();

	bool redrawCameraWindow = false;

	if ( down )
	{
		if ( specialKey > 0 )
		{
			switch ( specialKey )
			{
				case GLUT_KEY_UP:
				case GLUT_KEY_DOWN:
				case GLUT_KEY_LEFT:
				case GLUT_KEY_RIGHT:
				case GLUT_KEY_PAGE_UP:
				case GLUT_KEY_PAGE_DOWN:
					{
						double dPhi = ( modifiers & GLUT_ACTIVE_SHIFT ? FAST_ANGLE : ANGLE );
						Object * const pPilot = _pScene->FindSubobject( "pilot" );
						Object * const pHead = pPilot->FindSubobject( "head" );
						Object * const pHelmet = pHead->FindSubobject( "helmet" );
						Object * const pRotated = ( modifiers & GLUT_ACTIVE_ALT ? pPilot : pHead );

						switch ( specialKey )
						{
							case GLUT_KEY_UP:
								if ( dPhi > PITCH_MAX - pRotated->GetPitch() )
									dPhi = PITCH_MAX - pRotated->GetPitch();

								pRotated->IncPitch( dPhi );
								break;

							case GLUT_KEY_DOWN:
								if ( dPhi > pRotated->GetPitch() - PITCH_MIN )
									dPhi = pRotated->GetPitch() - PITCH_MIN;

								pRotated->DecPitch( dPhi );
								break;

							case GLUT_KEY_LEFT:
								if ( dPhi > YAW_MAX - pRotated->GetYaw() )
									dPhi = YAW_MAX - pRotated->GetYaw();

								pRotated->IncYaw( dPhi );
								break;

							case GLUT_KEY_RIGHT:
								if ( dPhi > pRotated->GetYaw() - YAW_MIN )
									dPhi = pRotated->GetYaw() - YAW_MIN;
								
								pRotated->DecYaw( dPhi );
								break;

							case GLUT_KEY_PAGE_DOWN:
								if ( dPhi > ROLL_MAX - pRotated->GetRoll() )
									dPhi = ROLL_MAX - pRotated->GetRoll();

								pRotated->IncRoll( dPhi );
								break;

							case GLUT_KEY_PAGE_UP:
								if ( dPhi > pRotated->GetRoll() - ROLL_MIN )
									dPhi = pRotated->GetRoll() - ROLL_MIN;
								
								pRotated->DecRoll( dPhi );
								break;
						}

						redrawCameraWindow = true;
					}
					break;

				case GLUT_KEY_HOME:
					{
						Object * const pPilot = _pScene->FindSubobject( "pilot" );
						Object * const pHead = pPilot->FindSubobject( "head" );

						pPilot->SetPitch( 0.0 );
						pPilot->SetYaw( 0.0 );
						pPilot->SetRoll( 0.0 );

						pHead->SetPitch( 0.0 );
						pHead->SetYaw( 0.0 );
						pHead->SetRoll( 0.0 );

						redrawCameraWindow = true;
					}
					break;

				case GLUT_KEY_F1:
				case GLUT_KEY_F2:
				case GLUT_KEY_F3:
				case GLUT_KEY_F4:
					// TODO: switch visualization mode.
					redrawCameraWindow = true;
					break;

				case GLUT_KEY_F9:
					__ReshapeWindows( _pCamera->GetFrameWidth(), _pCamera->GetFrameHeight() );
					redrawCameraWindow = true;
					break;
			}
		}
		else
		{
			switch ( key )
			{
				case ' ':
					_pCalibrationNeeded->Set();
					break;

				case ESC:
					//glutLeaveMainLoop();
					exit( 0 );	// FIX: clean up
					break;
			}
		}
	}
	else
	{
		// key up.
	}

	if ( redrawCameraWindow )
		glutPostWindowRedisplay( __windowCamera );
}

void GLUTManager::__KeyboardDownHandler( unsigned char key, int x, int y )
{
	__KeyboardHandler( NULL, key, true, x, y );
}

void GLUTManager::__KeyboardUpHandler( unsigned char key, int x, int y )
{
	__KeyboardHandler( NULL, key, false, x, y );
}

void GLUTManager::__SpecialKeyboardDownHandler( int specialKey, int x, int y )
{
	__KeyboardHandler( specialKey, NULL, true, x, y );
}

void GLUTManager::__SpecialKeyboardUpHandler( int specialKey, int x, int y )
{
	__KeyboardHandler( specialKey, NULL, false, x, y );
}

void GLUTManager::__MainWindowStatusHandler( int state )
{
#ifdef _DEBUG
	printf( "main.windowstatus\n" );
#endif

	__retained = ( state != GLUT_HIDDEN );
	glutIdleFunc( __retained ? __IdleHandler : NULL );
}

void GLUTManager::__MainDisplayHandler()
{
#ifdef _DEBUG
	printf( "main.display" );
#endif

	glClearColor( 0.4f, 0.0f, 0.0f, 0.0f );
    glClear( GL_COLOR_BUFFER_BIT );

	glutSwapBuffers();
}

void GLUTManager::__MainReshapeHandler( int width, int height )
{
#ifdef _DEBUG
	printf( "main.reshape\n" );
#endif

	width = ( width - GAP ) / 2;

	const int window = glutGetWindow();
	{
		glutSetWindow( __windowCamera );
		glutPositionWindow( 0, 0 );
		glutReshapeWindow( width, height );

		glutSetWindow( __windowSpectator );
		glutPositionWindow( width + GAP, 0 );
		glutReshapeWindow( width, height );
	}
	glutSetWindow( window );

	_ChangeResolution( width, height );
}

void GLUTManager::__CameraDisplayHandler()
{
#if 1 //def _DEBUG
	printf( "camera.display\n" );
#endif

	_DrawFrame();

	_SaveFrame();
	_pFrame->SwapPair( 1, 0 );
	_pNewFrameReady->Set();

	__CameraReportAttitude();

	glutSwapBuffers();
}

void GLUTManager::__CameraReportAttitude()
{
	const Object * pHead = _pScene->FindSubobject( std::string( "head" ) );

	glPushAttrib( GL_CURRENT_BIT );
	{
		__SetTextFont( "helvetica", 18 );
		__SetTextColor( 255, 255, 0 );
		__DrawText( 0, 40, "Yaw:    %6.2lf", pHead->GetYaw() );
		__DrawText( 0, 20, "Pitch:  %6.2lf", pHead->GetPitch() );
		__DrawText( 0,  0, "Roll:   %6.2lf", pHead->GetRoll() );
	}
	glPopAttrib();
}

void GLUTManager::__CameraReshapeHandler( int width, int height )
{
#ifdef _DEBUG
	printf( "camera.reshape\n" );
#endif

	_pGraphics->SetViewport( width, height );
}

void GLUTManager::__CameraMouseHandler( int button, int state, int x, int y )
{
#ifdef _DEBUG
	printf( "camera.mouse\n" );
#endif

	const int modifiers = glutGetModifiers();

	bool redrawCameraWindow = false;
	// TODO: scene rotation on mouse move.

/*
	const Object * pHead = _pScene->FindSubobject( std::string( "head" ) );
	__ReportAttitude( pHead->GetAttitude() );
	redrawCameraWindow = true;
*/
	if ( redrawCameraWindow )
		glutPostWindowRedisplay( __windowCamera );
}

void GLUTManager::__SpectatorDisplayHandler()
{
#if 1 //def _DEBUG
	printf( "spectator.display\n" );
#endif
	if ( _pFrameProcessed->TryWait() )
	{
		void * const pFrameBuffer = _pFrame->Lock( 1 );
		{
			glDrawPixels( _pCamera->GetFrameWidth(), _pCamera->GetFrameHeight(), GL_RGBA, GL_UNSIGNED_BYTE, pFrameBuffer );
			__SpectatorReportAttitude();
		}
		_pFrame->Unlock( 1 );

		glutSwapBuffers();

		_pFrameProcessed->Reset();
	}
}

void GLUTManager::__SpectatorReportAttitude()
{
	const Object * pHead = _pScene->FindSubobject( std::string( "head" ) );

	glPushAttrib( GL_CURRENT_BIT );
	{
		__SetTextFont( "helvetica", 18 );
		__SetTextColor( 255, 255, 0 );

		bool success;
		double yaw, pitch, roll;
		LoadResults( success, yaw, pitch, roll );

		if ( success )
		{
			static double yawMaxDiff = 0.0, pitchMaxDiff = 0.0, rollMaxDiff = 0.0;

			double yawDiff = abs( pHead->GetYaw() - yaw );
			double pitchDiff = abs( pHead->GetPitch() - pitch );
			double rollDiff = abs( pHead->GetRoll() - roll );
			if ( yawDiff > yawMaxDiff )
				yawMaxDiff = yawDiff;
			if ( pitchDiff > pitchMaxDiff )
				pitchMaxDiff = pitchDiff;
			if ( rollDiff > rollMaxDiff )
				rollMaxDiff = rollDiff;

			__DrawText( 0, 120, "Yaw:    %6.2lf", pHead->GetYaw() );
			__DrawText( 0, 100, "Pitch:  %6.2lf", pHead->GetPitch() );
			__DrawText( 0, 80, "Roll:   %6.2lf", pHead->GetRoll() );

			__DrawText( 0, 40, "Yaw:    %6.2lf (inaccuracy: %6.2lf, max: %6.2lf)", yaw, yawDiff, yawMaxDiff );
			__DrawText( 0, 20, "Pitch:  %6.2lf (inaccuracy: %6.2lf, max: %6.2lf)", pitch, pitchDiff, pitchMaxDiff );
			__DrawText( 0,  0, "Roll:   %6.2lf (inaccuracy: %6.2lf, max: %6.2lf)", roll, rollDiff, rollMaxDiff );
		}
		else
			__DrawText( 0, 0, "Undetermined." );
	}
	glPopAttrib();
}

void GLUTManager::__ReshapeWindows( const size_t width, const size_t height )
{
	const int window = glutGetWindow();
	{
		glutSetWindow( __windowMain );
		glutReshapeWindow( width + GAP + width, height );
	}
	glutSetWindow( window );

	_ChangeResolution( width, height );
}

bool GLUTManager::__InitializeWindows()
{
	const size_t width = _pCamera->GetFrameWidth();
	const size_t height = _pCamera->GetFrameHeight();

	glutInit( &__argc, __argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	glutInitWindowPosition( 0, 0 );
	glutInitWindowSize( width + GAP + width, height );

	__windowMain = glutCreateWindow( "Leela" );
	bool ok = ( __windowMain != 0 );

	if ( ok )
	{
		__windowCamera = glutCreateSubWindow( __windowMain, 0, 0, width, height );
		ok = ( __windowCamera != 0 );
	}

	if ( ok )
	{
		__windowSpectator = glutCreateSubWindow( __windowMain, width + GAP, 0, width, height );
		ok = ( __windowSpectator != 0 );
	}

	if ( ok )
	{
		__BindHandlers();
		__ReshapeWindows( width, height );
		//_ChangeResolution( width, height );
	}

	return ( ok );
}

void GLUTManager::__DeinitializeWindows()
{
	glutDestroyWindow( __windowSpectator );
	__windowSpectator = 0;

	glutDestroyWindow( __windowCamera );
	__windowCamera = 0;

	glutDestroyWindow( __windowMain );
	__windowMain = 0;
}

void GLUTManager::__BindHandlers()
{
	//glutTimerFunc( _pCamera->GetFrameInterval(), __TimerHandler, 0 );

	const int window = glutGetWindow();
	{
		glutSetWindow( __windowMain );
		{
			glutWindowStatusFunc( __MainWindowStatusHandler );
			glutDisplayFunc( __MainDisplayHandler );
			glutReshapeFunc( __MainReshapeHandler );

			glutKeyboardFunc( __KeyboardDownHandler );
			glutKeyboardUpFunc( __KeyboardUpHandler );
			glutSpecialFunc( __SpecialKeyboardDownHandler );
			glutSpecialUpFunc( __SpecialKeyboardUpHandler );
		}

		glutSetWindow( __windowCamera );
		{
			glutDisplayFunc( __CameraDisplayHandler );
			glutReshapeFunc( __CameraReshapeHandler );

			glutMouseFunc( __CameraMouseHandler );
			glutKeyboardFunc( __KeyboardDownHandler );
			glutKeyboardUpFunc( __KeyboardUpHandler );
			glutSpecialFunc( __SpecialKeyboardDownHandler );
			glutSpecialUpFunc( __SpecialKeyboardUpHandler );
		}

		glutSetWindow( __windowSpectator );
		{
			glutDisplayFunc( __SpectatorDisplayHandler );

			glutKeyboardFunc( __KeyboardDownHandler );
			glutKeyboardUpFunc( __KeyboardUpHandler );
			glutSpecialFunc( __SpecialKeyboardDownHandler );
			glutSpecialUpFunc( __SpecialKeyboardUpHandler );
		}
	}
	glutSetWindow( window );
}

void GLUTManager::__SetTextColor( const size_t red, const size_t green, const size_t blue )
{
	glColor3ub( red, green, blue );
}

void GLUTManager::__SetTextFont( char * const name, const size_t size )
{
    __pFont = GLUT_BITMAP_HELVETICA_10;
    if ( strcmp( name, "helvetica" ) == 0 )
	{
        switch ( size )
		{
		case 10:	__pFont = GLUT_BITMAP_HELVETICA_10;	break;
		case 12:	__pFont = GLUT_BITMAP_HELVETICA_12;	break;
		case 18:	__pFont = GLUT_BITMAP_HELVETICA_18;	break;
		}
    }
	else if ( strcmp( name, "times roman" ) == 0 )
	{
		switch ( size )
		{
		case 10:	__pFont = GLUT_BITMAP_TIMES_ROMAN_10;	break;
		case 24:	__pFont = GLUT_BITMAP_TIMES_ROMAN_24;	break;
		}
    }
	else if ( strcmp( name, "8x13" ) == 0 )
	{
        __pFont = GLUT_BITMAP_8_BY_13;
    }
	else if ( strcmp( name, "9x15" ) == 0 )
	{
        __pFont = GLUT_BITMAP_9_BY_15;
    }
}

void GLUTManager::__DrawText( const int x, const int y, char * format, ... )
{
    static char buffer[ 1024 ];
    
	va_list args;
    va_start( args, format );
    vsprintf( buffer, format, args );
    va_end( args );

	GLint mode;
	glGetIntegerv( GL_MATRIX_MODE, &mode );
	{
		glMatrixMode( GL_PROJECTION );
		glPushMatrix();
		glLoadIdentity();
		gluOrtho2D( 0, _pCamera->GetFrameWidth(), 0, _pCamera->GetFrameHeight() );
		{
			glMatrixMode( GL_MODELVIEW );
			glPushMatrix();
			glLoadIdentity();
			{
				glPushAttrib( GL_LIGHTING_BIT );
				{
					glDisable( GL_LIGHTING );

					glRasterPos2i( x, y );
					for ( register const char * s = buffer; *s != '\0'; s++ )
						glutBitmapCharacter( __pFont, *s );
				}
				glPopAttrib();
			}
			glPopMatrix();
		}
		glMatrixMode( GL_PROJECTION );
		glPopMatrix();
	}
	glMatrixMode( mode );
}
 