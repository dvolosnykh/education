#pragma once

#include "../VirtualCameraManager.h"
#include "OpenGLCamera.h"
#include "OpenGLGraphics.h"


class GLUTManager : public VirtualCameraManager
{
private:
	static int __argc;
	static char ** __argv;
	static GLint __windowMain;
	static GLint __windowCamera;
	static GLint __windowSpectator;
	static bool __retained;
	static GLvoid * __pFont;

public:
	GLUTManager( OpenGLCamera * const pCamera, MultiBuffer * const pFrame, Scene * const pScene, Event * const pNewFrameReady, Event * const pFrameProcessed, Event * const pFrameResized, Event * pCalibrationNeeded, int argc, char ** argv );
	virtual ~GLUTManager() {}

protected:
	virtual void _Main();
	virtual bool _Initialize();
	virtual void _Deinitialize();

private:
	static void __IdleHandler();
	static void __TimerHandler( int value );
	static void __KeyboardHandler( const int specialKey, const unsigned char key, const bool down, const int x, const int y );
	static void __KeyboardDownHandler( unsigned char key, int x, int y );
	static void __KeyboardUpHandler( unsigned char key, int x, int y );
	static void __SpecialKeyboardDownHandler( int specialKey, int x, int y );
	static void __SpecialKeyboardUpHandler( int specialKey, int x, int y );

	static void __MainWindowStatusHandler( int state );
	static void __MainDisplayHandler();
	static void __MainReshapeHandler( int width, int height );

	static void __CameraDisplayHandler();
	static void __CameraReportAttitude();
	static void __CameraReshapeHandler( int width, int height );
	static void __CameraMouseHandler( int button, int state, int x, int y );

	static void __SpectatorDisplayHandler();
	static void __SpectatorReportAttitude();

	static void __ReshapeWindows( const size_t width, const size_t height );

	static bool __InitializeWindows();
	static void __DeinitializeWindows();
	static void __BindHandlers();

	static void __SetTextColor( const size_t reg, const size_t green, const size_t blue );
	static void __SetTextFont( char * const name, const size_t size );
	static void __DrawText( const int x, const int y, char * format, ... );
};
