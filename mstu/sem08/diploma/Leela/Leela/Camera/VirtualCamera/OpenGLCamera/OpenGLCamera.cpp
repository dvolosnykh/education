#include <math.h>

#include "OpenGLCamera.h"

#include <GL/glut.h>


OpenGLCamera::OpenGLCamera( const size_t frameWidth, const size_t frameHeight, const size_t fps )
{
	SetResolution( frameWidth, frameHeight );
	SetChannels( __CHANNELS_COUNT, __CHANNELS_USED );
	SetBitsPerChannel( __BITS_PER_CHANNEL );
	SetFPS( fps );
	SetFocus( __CalculateFocus( frameHeight, __FOVY ) );
}

void OpenGLCamera::Locate() const
{
	GLint mode;
	glGetIntegerv( GL_MATRIX_MODE, &mode );
	if ( mode != GL_MODELVIEW )
		glMatrixMode( GL_MODELVIEW );

	glLoadIdentity();
	const GLdouble radian = _DegreeToRadian( Angle );
	const Vertex3D position( Target.X - Distance * cos( radian ), Target.Y, Target.Z - Distance * sin( radian ) );
	gluLookAt( position.X, position.Y, position.Z, Target.X, Target.Y, Target.Z, Up.X, Up.Y, Up.Z );

	if ( mode != GL_MODELVIEW )
		glMatrixMode( mode );
}

void OpenGLCamera::SetupSight( const size_t width, const size_t height ) const
{
	GLint mode;
	glGetIntegerv( GL_MATRIX_MODE, &mode );
	if ( mode != GL_PROJECTION )
		glMatrixMode( GL_PROJECTION );
	
	glLoadIdentity();
	const GLdouble aspect = width / ( GLdouble )height;
	const GLdouble fovy = __CalculateFOVY( height, GetFocus() );
	gluPerspective( fovy, aspect, __Z_NEAR, __Z_FAR );

	if ( mode != GL_PROJECTION )
		glMatrixMode( mode );
}

void OpenGLCamera::GetFrame( void * const pFrame ) const
{
	glReadPixels( 0, 0, GetFrameWidth(), GetFrameHeight(), GL_RGBA, GL_UNSIGNED_BYTE, pFrame );
}

double OpenGLCamera::__CalculateFOVY( const size_t frameHeight, const double focus )
{
	return _RadianToDegree( 2 * atan( frameHeight / ( 2 * focus ) ) );
}

double OpenGLCamera::__CalculateFocus( const size_t frameHeight, const double fovy )
{
	return ( frameHeight / ( 2 * tan( _DegreeToRadian( fovy ) / 2 ) ) );
}
