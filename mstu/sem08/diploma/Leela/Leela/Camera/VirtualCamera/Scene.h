#pragma once

#include "Object.h"


#define HELMET_RADIUS	( 10.0 )
#define BODY_HEIGHT		( 4 * HELMET_RADIUS )
#define BODY_RADIUS		( 1.0 )


class Body : public Cylinder
{
public:
	Body( const std::string & name, const double radius, const double height )
		: Cylinder( name, radius, height, 4 ) {}
};

class Helmet : public ComplexObject
{
public:
	double Radius;

public:
	Helmet( const std::string & name, const double radius, const Vertex3D & origin )
		: ComplexObject( name, origin )
		, Radius( radius )
	{ __Construct(); }

private:
	void __Construct();
};

class Diode : public Sphere
{
public:
	Diode( const std::string & name, const Vertex3D & origin )
		: Sphere( name, 0.15, 10, origin )
	{ this->IsLighted = false; }
};

class Head : public ComplexObject
{
public:
	Head( const std::string & name, const Vertex3D & origin )
		: ComplexObject( name, origin )
	{ __Construct(); }

private:
	void __Construct();
};

class Pilot : public ComplexObject
{
public:
	Pilot( const std::string & name )
		: ComplexObject( name )
	{ __Construct(); }

private:
	void __Construct();
};

class Cockpit : public ComplexObject
{
public:
	Cockpit( const std::string & name )
		: ComplexObject( name )
	{ __Construct(); }

private:
	void __Construct();
};

class Scene : public ComplexObject
{
public:
	Scene()
		: ComplexObject( std::string( "scene" ) )
	{ __Construct(); }

private:
	void __Construct();
};
