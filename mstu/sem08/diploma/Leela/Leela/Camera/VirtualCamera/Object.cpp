#define _USE_MATH_DEFINES
#include <math.h>

#include "Object.h"


Object::Object( const std::string & name, const Vertex3D & origin )
	: Name( name )
	, Origin( origin )
{
	__attitude.Yaw = __attitude.Pitch = __attitude.Roll = 0.0;
}

void Object::Draw() const
{
	_BeginDraw();
	_DrawObject();
	_EndDraw();
}

Object * Object::SubobjectLocation( Vertex3D * const pLocation, const std::string & name ) const
{
	Object * pObject = ( Name == name ? ( Object * const )this : NULL );
	if ( pObject != NULL && pLocation != NULL )
		*pLocation = pObject->Origin;

	return ( pObject );
}

void Object::_BeginDraw() const
{
	glGetIntegerv( GL_MATRIX_MODE, &__matrixMode );
	if ( __matrixMode != GL_MODELVIEW )
		glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	// TODO: rotate object's coordinate-system!!
	glTranslated( Origin.X, Origin.Y, Origin.Z );
	glRotated( __attitude.Yaw, 0.0, 0.0, 1.0 );
	glRotated( __attitude.Pitch, 0.0, 1.0, 0.0 );
	glRotated( __attitude.Roll, 1.0, 0.0, 0.0 );
}

void Object::_EndDraw() const
{
	glPopMatrix();
	if ( __matrixMode != GL_MODELVIEW )
		glMatrixMode( __matrixMode );
}

Vertex3D Object::RadialCoords( const Vertex3D & center, const double radius, double longtitude, double latitude )
{
	const double rad_per_deg = M_PI / 180.0;
	latitude *= rad_per_deg;
	longtitude *= rad_per_deg;

	Vertex3D origin = center;
	const double rHorz = radius * cos( latitude );
	origin.X += rHorz * sin( longtitude );
	origin.Y += -rHorz * cos( longtitude );
	origin.Z += radius * sin( latitude );

	return ( origin );
}

void Object::__SetAngle( double * const pAngle, const double new_angle )
{
	*pAngle = fmod( new_angle + 180.0, 360.0 ) - 180.0;
}

void Object::__IncAngle( double * const pAngle, const double dAngle )
{
	if ( ( *pAngle += dAngle ) > 180.0 ) *pAngle -= 360;
}

void Object::__DecAngle( double * const pAngle, const double dAngle )
{
	if ( ( *pAngle -= dAngle ) <= -180.0 ) *pAngle += 360;
}

void SimpleObject::_BeginDraw () const
{
	Object::_BeginDraw();

	if ( !IsLighted )
	{
		glPushAttrib( GL_LIGHTING_BIT );
		glDisable( GL_LIGHTING );
	}
}

void SimpleObject::_EndDraw() const
{
	if ( !IsLighted )
		glPopAttrib();

	Object::_EndDraw();
}

void SimpleObject::__Initialize()
{
	_q = gluNewQuadric();

	gluQuadricDrawStyle( _q, GLU_FILL );
	gluQuadricNormals( _q, GLU_SMOOTH );
	gluQuadricOrientation( _q, GLU_OUTSIDE );
}

void SimpleObject::__Deinitialize()
{
	if ( _q != NULL )
		gluDeleteQuadric( _q );
}

void ComplexObject::RemoveAll()
{
	for ( __Dictionary::const_iterator p = __subobjects.begin(); p != __subobjects.end(); p++ )
		delete p->second;

	__subobjects.clear();
}

Object * ComplexObject::SubobjectLocation( Vertex3D * const pLocation, const std::string & name ) const
{
	Object * pObject = Object::SubobjectLocation( pLocation, name );

	if ( pObject == NULL )
	{
		for ( __Dictionary::const_iterator p = __subobjects.begin(); pObject == NULL && p != __subobjects.end(); p++ )
			pObject = p->second->SubobjectLocation( pLocation, name );

		if ( pObject != NULL && pLocation != NULL )
		{
			*pLocation += Origin; // TODO: take 'yaw, pitch and roll' into consideration.
		}
	}

	return ( pObject );
}

void ComplexObject::_DrawObject() const
{
	for ( __Dictionary::const_iterator p = __subobjects.begin(); p != __subobjects.end(); p++ )
		p->second->Draw();
}
