#include "VirtualCameraManager.h"
#include "../../Exceptions.h"


Scene * VirtualCameraManager::_pScene = NULL;
Graphics * VirtualCameraManager::_pGraphics = NULL;


VirtualCameraManager::VirtualCameraManager( VirtualCamera * const pCamera, MultiBuffer * const pFrame, Scene * const pScene, Event * const pNewFrameReady, Event * const pFrameProcessed, Event * const pFrameResized, Event * const pCalibrationNeeded, Graphics * const pGraphics )
	: CameraManager( pCamera, pFrame, pNewFrameReady, pFrameProcessed, pFrameResized, pCalibrationNeeded )
{
	_pScene = pScene;
	_pGraphics = pGraphics;
	if ( _pGraphics == NULL )
		throw MemoryException( typeid( _pGraphics ).name() );
}

VirtualCameraManager::~VirtualCameraManager()
{
	delete _pGraphics;
}

void VirtualCameraManager::_DrawFrame()
{
	VirtualCamera * const pCamera = static_cast< VirtualCamera * >( _pCamera );
	_pGraphics->BeginDrawing();
	pCamera->Locate();
	_pScene->Draw();
	_pGraphics->EndDrawing();
}
#include <math.h>
#include <stdio.h>
#define DISTANCE( p1, p2 ) sqrt( pow( p1.X - p2.X, 2 ) + pow( p1.Y - p2.Y, 2 ) + pow( p1.Z - p2.Z, 2 ) )
bool VirtualCameraManager::_Initialize()
{
	bool ok = CameraManager::_Initialize();
	if ( ok )
		ok = _pGraphics->Initialize();

	if ( ok )
	{
		_pScene->SetPitch( 0.0 );
		_pScene->SetYaw( 0.0 );
		_pScene->SetRoll( 180.0 );

		VirtualCamera * const pCamera = static_cast< VirtualCamera * >( _pCamera );
		pCamera->Angle = 0.0;
		pCamera->Distance = 35.0;
		pCamera->Target = Vertex3D( 0.0, 0.0, -( BODY_HEIGHT + HELMET_RADIUS ) );	// TODO: see _pScene->SubobjectLocation( &pCamera->Target, "head" );
		pCamera->Up.Set( 0.0, 0.0, -1.0 );

		pCamera->SetupSight( _pCamera->GetFrameWidth(), _pCamera->GetFrameHeight() );

#if 0	// FIX: delete this block
		Object * const rd = _pScene->FindSubobject( "right_diode" );
		Object * const ld = _pScene->FindSubobject( "left_diode" );
		Object * const ud = _pScene->FindSubobject( "upper_diode" );


		printf( "left-to-right:\t%f\n", DISTANCE( ld->Origin, rd->Origin ) );
		printf( "upper-to-left:\t%f\n", DISTANCE( ud->Origin, ld->Origin ) );
		printf( "upper-to-right:\t%f\n", DISTANCE( ud->Origin, rd->Origin ) );
		int i;
		cin >> i;
#endif
	}

	return ( ok );
}

void VirtualCameraManager::_Deinitialize()
{
	_pGraphics->Deinitialize();
	CameraManager::_Deinitialize();
}
