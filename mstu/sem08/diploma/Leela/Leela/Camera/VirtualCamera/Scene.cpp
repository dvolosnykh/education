#include "Scene.h"
#include "../../Exceptions.h"


void Helmet::__Construct()
{
	Sphere * const pSphere = new Sphere( Name, Radius, 30 );
	if ( pSphere == NULL )
		throw MemoryException( typeid( pSphere ).name() );
	this->Add( pSphere );

	Diode * const pOriginDiode = new Diode( std::string( "origin_diode" ), Object::RadialCoords( Vertex3D::Default, Radius, -90.0, -20.0 ) );
	if ( pOriginDiode == NULL )
		throw MemoryException( typeid( pOriginDiode ).name() );
	this->Add( pOriginDiode );

	Diode * const pUpperRightDiode = new Diode( std::string( "upper_right_diode" ), Object::RadialCoords( Vertex3D::Default, Radius, -70.0, 20.0 ) );
	if ( pUpperRightDiode == NULL )
		throw MemoryException( typeid( pUpperRightDiode ).name() );
	this->Add( pUpperRightDiode );

	Diode * const pLowerRightDiode = new Diode( std::string( "lower_right_diode" ), Object::RadialCoords( Vertex3D::Default, Radius, -70.0, -20.0 ) );
	if ( pLowerRightDiode == NULL )
		throw MemoryException( typeid( pLowerRightDiode ).name() );
	this->Add( pLowerRightDiode );

	Diode * const pUpperLeftDiode = new Diode( std::string( "upper_left_diode" ), Object::RadialCoords( Vertex3D::Default, Radius, -110.0, 20.0 ) );
	if ( pUpperLeftDiode == NULL )
		throw MemoryException( typeid( pUpperLeftDiode ).name() );
	this->Add( pUpperLeftDiode );

	Diode * const pLowerLeftDiode = new Diode( std::string( "lower_left_diode" ), Object::RadialCoords( Vertex3D::Default, Radius, -110.0, -20.0 ) );
	if ( pLowerLeftDiode == NULL )
		throw MemoryException( typeid( pLowerLeftDiode ).name() );
	this->Add( pLowerLeftDiode );
}

void Head::__Construct()
{
	Helmet * const pHelmet = new Helmet( std::string( "helmet" ), HELMET_RADIUS, Vertex3D( 0.0, 0.0, HELMET_RADIUS ) );
	if ( pHelmet == NULL )
		throw MemoryException( typeid( pHelmet ).name() );
	this->Add( pHelmet );
}

void Pilot::__Construct()
{
	Body * const pBody = new Body( std::string( "body" ), BODY_RADIUS, BODY_HEIGHT );
	if ( pBody == NULL )
		throw MemoryException( typeid( pBody ).name() );
	this->Add( pBody );

	Head * const pHead = new Head( std::string( "head" ), Vertex3D( 0.0, 0.0, pBody->Height ) );
	if ( pHead == NULL )
		throw MemoryException( typeid( pHead ).name() );
	this->Add( pHead );
}

void Cockpit::__Construct()
{
	Pilot * const pPilot = new Pilot( std::string( "pilot" ) );
	if ( pPilot == NULL )
		throw MemoryException( typeid( pPilot ).name() );
	this->Add( pPilot );
}

void Scene::__Construct()
{
	Cockpit * const pCockpit = new Cockpit( std::string( "cockpit" ) );
	if ( pCockpit == NULL )
		throw MemoryException( typeid( pCockpit ).name() );
	this->Add( pCockpit );
}
