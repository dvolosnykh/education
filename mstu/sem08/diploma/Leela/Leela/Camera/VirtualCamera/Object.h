#pragma once

#include <map>
#include <string>

#include <GL/glut.h>

#include "../../Mathematics/Vertex.h"


class Object
{
public:
	struct Attitude
	{
		double Yaw, Pitch, Roll;
	};

public:
	const std::string Name;
	Vertex3D Origin;

private:
	mutable GLint __matrixMode;
	Attitude __attitude;

public:
	Object( const std::string & name, const Vertex3D & origin );
	virtual ~Object() {}

public:
	void Draw() const;
	Object * FindSubobject( const std::string & name ) const { return SubobjectLocation( NULL, name ); }
	virtual Object * SubobjectLocation( Vertex3D * const pLocation, const std::string & name ) const;
	const Attitude GetAttitude() const { return ( __attitude ); }
	
	double GetPitch() const { return ( __attitude.Pitch ); }
	void SetPitch( const double pitch ) { __SetAngle( &__attitude.Pitch, pitch ); }
	void IncPitch( const double dPitch ) { __IncAngle( &__attitude.Pitch, dPitch ); }
	void DecPitch( const double dPitch ) { __DecAngle( &__attitude.Pitch, dPitch ); }
	
	double GetYaw() const { return ( __attitude.Yaw ); }
	void SetYaw( const double yaw ) { __SetAngle( &__attitude.Yaw, yaw ); }
	void IncYaw( const double dYaw ) { __IncAngle( &__attitude.Yaw, dYaw ); }
	void DecYaw( const double dYaw ) { __DecAngle( &__attitude.Yaw, dYaw ); }
	
	double GetRoll() const { return ( __attitude.Roll ); }
	void SetRoll( const double roll ) { __SetAngle( &__attitude.Roll, roll ); }
	void IncRoll( const double dRoll ) { __IncAngle( &__attitude.Roll, dRoll ); }
	void DecRoll( const double dRoll ) { __DecAngle( &__attitude.Roll, dRoll ); }

	static Vertex3D RadialCoords( const Vertex3D & center, const double radius,
		double longtitude, double latitude );

protected:
	virtual void _BeginDraw() const;
	virtual void _EndDraw() const;
	virtual void _DrawObject() const = 0;
	
private:
	static void __SetAngle( double * const pAngle, const double new_angle );
	static void __IncAngle( double * const pAngle, const double dAngle );
	static void __DecAngle( double * const pAngle, const double dAngle );
};

class SimpleObject : public Object
{
public:
	bool IsLighted;

protected:
	GLUquadric * _q;

public:
	SimpleObject( const std::string & name, const Vertex3D & origin )
		: Object( name, origin )
		, IsLighted( true )	{ __Initialize(); }
	virtual ~SimpleObject() { __Deinitialize(); }

protected:
	void _BeginDraw() const;
	void _EndDraw() const;

private:
	void __Initialize();
	void __Deinitialize();
};

class ComplexObject : public SimpleObject
{
private:
	typedef std::map< std::string, Object * > __Dictionary;
	__Dictionary __subobjects;

public:
	ComplexObject( const std::string & name, const Vertex3D & origin = Vertex3D::Default )
		: SimpleObject( name, origin ) {}
	virtual ~ComplexObject() { RemoveAll(); }

public:
	void Add( Object * pObj ) { __subobjects.insert( make_pair( pObj->Name, pObj ) ); }
	void RemoveAll();
	virtual Object * SubobjectLocation( Vertex3D * const pLocation, const std::string & name ) const;

protected:
	void _DrawObject() const;
};

class Sphere : public SimpleObject
{
public:
	double Radius;

private:
	const GLint __details;

public:
	Sphere( const std::string & name, const double radius, const GLint details, const Vertex3D & center = Vertex3D::Default )
		: SimpleObject( name, center )
		, Radius( radius )
		, __details( details ) {}

protected:
	void _DrawObject() const { gluSphere( _q, Radius, __details, __details ); }
};

class Cylinder : public SimpleObject
{
public:
	double Radius;
	double Height;

private:
	const GLint __details;

public:
	Cylinder( const std::string & name, const double radius, const double height, const GLint details, const Vertex3D & baseCenter = Vertex3D::Default )
		: SimpleObject( name, baseCenter )
		, Radius( radius )
		, Height( height )
		, __details( details ) {}
		
protected:
	void _DrawObject() const { gluCylinder( _q, Radius, Radius, Height, __details, 1 ); }
};
