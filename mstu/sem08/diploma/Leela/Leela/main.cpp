#include <stdio.h>

#include "ApplicationManager.h"
#include "Exceptions.h"


int main( int argc, char * argv[] )
{
	try
	{
		ApplicationManager applicationManager( argc, argv );
		applicationManager.Execute();
	}
	catch ( Intercommunication::Exception & ex )
	{
		printf( "%s\n", ex.Text );
	}
	catch ( GeneralException & ex )
	{
		printf( "%s\n", ex.Text );
	}
	
	return ( EXIT_SUCCESS );
}
