#include <exception>
#include <string>


class GeneralException : public std::exception
{
public:
	std::string Text;

public:
	GeneralException( const std::string text )
		: Text( text ) {}
	virtual ~GeneralException() throw() {}
};

class MemoryException : public GeneralException
{
public:
	std::string Text;

public:
	MemoryException( const std::string comment )
		: GeneralException( "Failed allocating memory. " + comment ) {}
	virtual ~MemoryException() throw() {}
};
