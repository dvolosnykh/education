#pragma once

#include "Intercommunication.h"


class ApplicationManager : public SyncJob
{
private:
	int __argc;
	char ** __argv;
	Event __newFrameReady;
	Event __frameProcessed;
	Event __frameResized;
	Event __calibrationNeeded;

public:
	ApplicationManager( int argc, char ** argv )
		: __argc( argc )
		, __argv( argv ) {}
	virtual ~ApplicationManager() {}

protected:
	virtual void _Main();
	virtual bool _Initialize();
	virtual void _Deinitialize();
};
