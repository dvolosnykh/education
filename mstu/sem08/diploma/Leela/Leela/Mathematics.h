#pragma once

// assuming:
// n - non-negative integer.
// d - natural number.

#define FLOOR_DIV_UINT( n, d )	( ( n ) / ( d ) )

// ranges of correctly performed calculations for n and d are twice thinner the ranges of their corresponding types.
#define ROUND_DIV_UINT( n, d )	FLOOR_DIV_UINT( ( ( n ) << 1 ) + ( d ), ( d ) << 1 )


#define CEIL_DIV_UINT( n, d )	( ( ( n ) - 1 ) / ( d ) + 1 )