#include "ApplicationManager.h"
#include "Camera/VirtualCamera/OpenGLCamera/GLUTManager.h"
#include "Spectator/Spectator.h"


void ApplicationManager::_Main()
{
	OpenGLCamera camera( VirtualCamera::DEFAULT_WIDTH, VirtualCamera::DEFAULT_HEIGHT, VirtualCamera::DEFAULT_FPS );
	Scene scene;
	MultiBuffer frame( 2, camera.GetFrameSize() );
	GLUTManager cameraManager( &camera, &frame, &scene, &__newFrameReady, &__frameProcessed, &__frameResized, &__calibrationNeeded, __argc, __argv );

	Spectator spectator( &cameraManager, &__newFrameReady, &__frameProcessed, &__frameResized, &__calibrationNeeded );

	spectator.ExecuteAsynchronous();
	cameraManager.Execute();
	spectator.RequestTermination();
	spectator.WaitForTermination();

	frame.Reset();
}

bool ApplicationManager::_Initialize()
{
	__newFrameReady = Event( "NewFrameReadyEvent" );
	__newFrameReady.Create();
	__frameProcessed = Event( "FrameProcessedEvent" );
	__frameProcessed.Create();
	__frameResized = Event( "FrameResizedEvent" );
	__frameResized.Create();
	__calibrationNeeded = Event( "CalibrationNeededEvent" );
	__calibrationNeeded.Create();
	return ( true );
}

void ApplicationManager::_Deinitialize()
{
	__calibrationNeeded.Close();
	__frameResized.Close();
	__frameProcessed.Close();
	__newFrameReady.Close();
}