#pragma once

#include "../Mathematics/Vertex.h"
#include "DiscoverPoints.h"
#include "OrderPoints.h"
#include "SolvePnP.h"


#define POINTS_COUNT	5

class FindTransformation
{
public:
	bool Success;
	Vertex3D Origin;
	Attitude Attitude;

private:
	DiscoverPoints __discoverPoints;
	OrderPoints __orderPoints;
	SolvePnP __solvePnP;

	Vertex2D __pointsInitial [ POINTS_COUNT ];

public:
	FindTransformation() {}
	virtual ~FindTransformation() {}

public:
	bool operator() ( void * const frame, const size_t width, const size_t height, const double focus, const bool updateInitalPosition );
};
