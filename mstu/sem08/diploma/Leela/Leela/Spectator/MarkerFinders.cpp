#include "MarkerFinders.h"
#include "../Mathematics.h"


void MarkerFinder::_UpdateData( const int y, const int xLeft, const int xRight )
{
	if ( y < Marker.TopLeft.Y )
		Marker.TopLeft.Y = y;
	else if ( y > Marker.BottomRight.Y )
		Marker.BottomRight.Y = y;

	if ( xLeft < Marker.TopLeft.X )
		Marker.TopLeft.X = xLeft;

	if ( xRight > Marker.BottomRight.X )
		Marker.BottomRight.X = xRight;
}

void MarkerFinder::_CalculateCenter()
{
	Center.X = ( Marker.TopLeft.X + Marker.Width() * 0.5 ) - ( _width * 0.5 - 0.5 );
	Center.Y = ( Marker.TopLeft.Y + Marker.Height() * 0.5 ) - ( _height * 0.5 - 0.5 );
}


bool MarkerFinder_RGBA_8_8_8_8::_Matches( const uint32_t & pixel ) const
{
	//const uint8_t * const channel = reinterpret_cast< const uint8_t * >( &pixel );
	uint8_t * const channel = ( uint8_t * )( &pixel );
	uint8_t & R = channel[ __RED ], & G = channel[ __GREEN ], & B = channel[ __BLUE ];
	const uint8_t grey = static_cast< uint8_t>( 0.299 * R + 0.587 * G + 0.114 * B );	// See Rec. ITU-R BT.601-4.

	R = G = B = ( grey >= __minBrightness ? 255 : 0 );

	return ( grey >= __minBrightness );
}

void MarkerFinder_RGBA_8_8_8_8::_Visit( uint32_t & pixel )
{
	pixel = __VISITED;
}

bool MarkerFinder_RGBA_8_8_8_8::_Visited( const uint32_t & pixel ) const
{
	return ( pixel == __VISITED );
}


bool MarkerFinder_Grey_16::_Matches( const uint16_t & pixel ) const
{
	return ( pixel >= __minBrightness );
}

void MarkerFinder_Grey_16::_Visit( uint16_t & pixel )
{
	pixel = __VISITED;
}

bool MarkerFinder_Grey_16::_Visited( const uint16_t & pixel ) const
{
	return ( pixel == __VISITED );
}