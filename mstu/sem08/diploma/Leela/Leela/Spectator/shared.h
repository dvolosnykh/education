#ifndef SHARED_H_99823534
#define SHARED_H_99823534

//////////////////////////////
// Error codes
#include "error_codes.h"


//////////////////////////////
// machine zero
#include <limits>
const double eps = std::numeric_limits<double>::epsilon();

//////////////////////////////
// on screen

struct pixel
{
	double x;
	double y;
}; 

//////////////////////////////
// 3D

struct point3d
{
	double x;
	double y;
	double z;
};

#define MODEL(x, y) {x, y, 0}

union vector3
{
	double v[3];

	point3d p;
};

struct matrix33
{
	double e[9];
};

#define VECTOR3(POINT3D) {POINT3D.x, POINT3D.y, POINT3D.z}

//////////////////////////////
// transform & rotation

struct matrix44
{
	double e[16];
};

struct vector4
{
	double v[4];
};

struct aiming_beam
{
	double x_offset;
	double y_offset;
	double z_offset;

	double cos_roll;
	double sin_roll;
	
	double cos_pitch;
	double sin_pitch;
	
	double cos_yaw;
	double sin_yaw;
};
	
//////////////////////////////
// operators

// 4x4 * 4x4 = 4x4
matrix44 operator * (const matrix44& left, const matrix44& right);

// 4x4 * 4x1 = 4x1
vector4 operator * (const matrix44& left, const vector4& right);

// 1x3/3x1 - 1x3/3x1 = 1x3/3x1
vector4 operator - (const vector4& left, const vector4& right);

// 3x3 * 3x3 = 3x3
matrix33 operator * (const matrix33& left, const matrix33& right);

// 3x3 * 3x1 = 3x1
vector3 operator * (const matrix33& left, const vector3& right);

// 1x3 * 3x3 = 1x3
vector3 operator * (const vector3& left, const matrix33& right);

// 1x3/3x1 * scalar = 1x3/3x1
vector3 operator * (const vector3& left, const double scalar);

vector3 operator * (const double scalar, const vector3& right);

// 1x3 * 3x1 = scalar
double operator * (const vector3& left, const vector3& right);

// 1x3/3x1 - 1x3/3x1 = 1x3/3x1
vector3 operator - (const vector3& left, const vector3& right);

// 1x3/3x1 == 1x3/3x1
bool operator == (const vector3& left, const vector3& right);

// 1x4/4x1 == 1x4/4x1
bool operator == (const vector4& left, const vector4& right);

// 3x3 == 3x3
bool operator == (const matrix33& left, const matrix33& right);

// 4x4 == 4x4
bool operator == (const matrix44& left, const matrix44& right);


//////////////////////////////
// common operations

double square_distance(const point3d& point1, const point3d& point2);

vector3 cross_product(const vector3& left, const vector3& right);

vector3 get_unit_vector(double x, double y, double z);

void normalize(vector3* vector);

bool is_confluent(const vector3& vector);

double vector_abs(const vector3& vector);

#define DET2(e1, e2, e3, e4) (e1*e4 - e2*e3)

bool invert(const matrix33& matrix, matrix33* inverted); //(mult/div = 39/1)

//////////////////////////////
// debug_output

void print_matrix(const char* str, const matrix44& matrix);

void print_matrix(const char* str, const matrix33& matrix);

#endif //SHARED_H_99823534