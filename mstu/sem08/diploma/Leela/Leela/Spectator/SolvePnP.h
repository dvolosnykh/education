#pragma once


#include "../Mathematics/Vertex.h"
#include "shared.h"


struct Attitude
{
	double SinYaw, CosYaw;
	double SinPitch, CosPitch;
	double SinRoll, CosRoll;
};


class SolvePnP
{
public:
	Vertex3D Origin;
	Attitude Attitude;


public:
	SolvePnP() {}
	virtual ~SolvePnP() {}

public:
	size_t operator() ( const Vertex2D pointsInitial[], const Vertex2D pointsActual[], const size_t count, const double focus );


private:
	void __output_beam(const char* str, const aiming_beam& beam, int yaw, int pitch, int roll);
};
