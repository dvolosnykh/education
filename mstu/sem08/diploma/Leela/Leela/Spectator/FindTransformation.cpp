#include "FindTransformation.h"


#include <stdio.h>
#include <string.h>


bool FindTransformation::operator() ( void * const frame, const size_t width, const size_t height, const double focus, const bool updateInitialPosition )
{
	Vertex2D points[ POINTS_COUNT ];
	const size_t pointsFound = __discoverPoints( points, POINTS_COUNT, frame, width, height );

	bool ok = ( pointsFound >= POINTS_COUNT );
	if ( ok )
	{
#if 0
		for ( register size_t i = 0; i < pointsCount; i++ )
			printf( "Marker %u:\t( %.2f, %.2f )\n", i, points[ i ].X, points[ i ].Y );
#endif
		__orderPoints( points, POINTS_COUNT );
#if 0
		for ( register size_t i = 0; i < pointsCount; i++ )
			printf( "Marker %u:\t( %.2f, %.2f )\n", i, points[ i ].X, points[ i ].Y );
#endif
		std::swap( points[ 3 ], points[ 4 ] );
		std::swap( points[ 2 ], points[ 3 ] );

		if ( updateInitialPosition )
			memcpy( __pointsInitial, points, sizeof( Vertex2D ) * POINTS_COUNT  );

		const size_t solutionsNumber = __solvePnP( __pointsInitial + 1, points + 1, POINTS_COUNT - 1, focus );
		ok = ( solutionsNumber == 1 );
	}
	else
		printf( "Less than %u points found!", POINTS_COUNT );

	if ( ok )
	{
		Origin = __solvePnP.Origin;
		Attitude = __solvePnP.Attitude;
	}

	return ( ok );
}
