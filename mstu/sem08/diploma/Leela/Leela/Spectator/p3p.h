#ifndef P3P_H_23874690
#define P3P_H_23874690

#include "shared.h"

bool invert_jacobian33(const double factors[3]     , 
					   const double dot_products[3], 
					   double* inverted)
{
	bool result = false;

	double J11 = 2*(factors[0] - factors[1]*dot_products[0]);
	double J22 = 2*(factors[1] - factors[2]*dot_products[1]);
	double J33 = 2*(factors[2] - factors[0]*dot_products[2]);

	double J12 = 2*(factors[1] - factors[0]*dot_products[0]);
	double J23 = 2*(factors[2] - factors[1]*dot_products[1]);
	double J31 = 2*(factors[0] - factors[2]*dot_products[2]);

	double determinant = J11*J22*J33 + J12*J23*J31;

	if (fabs(determinant) > eps)
	{
		double inv_det = 1.0f / determinant;
		
		inverted[0] =  J22*J33*inv_det;
		inverted[1] = -J12*J33*inv_det;
		inverted[2] =  J12*J23*inv_det;
		inverted[3] =  J23*J31*inv_det; 
		inverted[4] =  J11*J33*inv_det;
		inverted[5] = -J11*J23*inv_det;
		inverted[6] = -J22*J31*inv_det;
		inverted[7] =  J12*J31*inv_det;
		inverted[8] =  J11*J22*inv_det;

		result = true;
	}

	return result;
}

void estimate(const vector3& factors         , 
			  const vector3& dot_products    , 
			  const vector3& square_distances, 
			  vector3* deltas)
{
	double factor1_sqr = factors.v[0]*factors.v[0];
	double factor2_sqr = factors.v[1]*factors.v[1];
	double factor3_sqr = factors.v[2]*factors.v[2];

	deltas->v[0] = factor1_sqr - 2*factors.v[0]*factors.v[1]*dot_products.v[0] + factor2_sqr - square_distances.v[0];
	deltas->v[1] = factor2_sqr - 2*factors.v[1]*factors.v[2]*dot_products.v[1] + factor3_sqr - square_distances.v[1];
	deltas->v[2] = factor3_sqr - 2*factors.v[2]*factors.v[0]*dot_products.v[2] + factor1_sqr - square_distances.v[2];
}

int p3p(const pixel*    pixels      , 
		const point3d*  model_points, 
		const double     focus       , 
		const double     tolerance   ,
		point3d*        world_points,
		const double*    factor_approx = 0,
		const unsigned  iter_limit    = -1)
{
	int error_code = PNP_NO_ERROR;
	
	// initialization
	vector3 square_distances = {0};
	square_distances.v[0] = square_distance(model_points[0], model_points[1]);   
	square_distances.v[1] = square_distance(model_points[1], model_points[2]);
	square_distances.v[2] = square_distance(model_points[2], model_points[0]);

	vector3 unit_vector1  = get_unit_vector(pixels[0].x, pixels[0].y, -focus);
	vector3 unit_vector2  = get_unit_vector(pixels[1].x, pixels[1].y, -focus);
	vector3 unit_vector3  = get_unit_vector(pixels[2].x, pixels[2].y, -focus);

	vector3 dot_products  = {(unit_vector1 * unit_vector2),
							 (unit_vector2 * unit_vector3),
							 (unit_vector3 * unit_vector1)};
	vector3 factors = {0};
	if (factor_approx)
		memcpy(factors.v, factor_approx, sizeof(factors));
		
	//iteration
	unsigned iteration = 0;
	vector3  deltas    = {0};
	matrix33 inverted_jacobian = {0};
	
	estimate(factors, dot_products, square_distances, &deltas);

	while (((fabs(deltas.v[0]) > tolerance)  ||
			(fabs(deltas.v[1]) > tolerance)  ||
			(fabs(deltas.v[2]) > tolerance)) &&
		    (iter_limit        > iteration++)&&
			(error_code == PNP_NO_ERROR))
	{
		if (invert_jacobian33(factors.v, dot_products.v, inverted_jacobian.e))
		{
			estimate(factors, dot_products, square_distances, &deltas);
			factors = factors - inverted_jacobian * deltas;	
		}
		else
		{
			error_code = ZERO_DETERMINANT;
		}
	}

	//output
	world_points[0] = (unit_vector1 * factors.v[0]).p;
	world_points[1] = (unit_vector2 * factors.v[1]).p;
	world_points[2] = (unit_vector3 * factors.v[2]).p;

	return error_code;
}

#endif //P3P_H_23874690