#pragma once

#include <limits>
#include <stack>
#include <utility>
#include "../pstdint.h"
#include "../Mathematics/Vertex.h"


// ATTENTION: for the case when two adjacent lines to the seed-point are already __BRIGHTEST the filling-algorithm may not work!
// It happens when there's no opportunity to determine if the pixel was visited (i.e. when the whole range of values is used to represent colors).

class Coordinates
{
public:
	int X, Y;

public:
	Coordinates() {}
	Coordinates( const int x, const int y )
		: X( x ), Y( y ) {}
};


struct Square
{
	Coordinates TopLeft, BottomRight;

	size_t Width() const { return static_cast< size_t >( BottomRight.X - TopLeft.X ); }
	size_t Height() const { return static_cast< size_t >( BottomRight.Y - TopLeft.Y ); }

	size_t Area() const { return ( Width() * Height() ); }
	bool IsInside( const Coordinates & coord ) const { return ( TopLeft.X <= coord.X && coord.X <= BottomRight.X && TopLeft.Y <= coord.Y && coord.Y <= BottomRight.Y ); }
};


class MarkerFinder
{
public:
	Vertex2D Center;
	Square Marker;

protected:
	const size_t _width, _height;

public:
	MarkerFinder( const size_t width, const size_t height )
		: _width( width ), _height( height ) {}
	virtual ~MarkerFinder() {}

public:
	virtual void FillAt( const Coordinates & seed ) = 0;
	virtual bool MatchesAt( const Coordinates & coord ) const = 0;

protected:
	void _UpdateData( const int y, const int xLeft, const int xRight );
	void _CalculateCenter();
};

// typenames 'pixel_t' and 'channel_t' - are one of the standard unsigned integer types (in terms of ISO/IEC: 14882).
// The following relation should be satisfied: sizeof( pixel_t ) % sizeof( channel_t ) == 0.
template< typename pixel_t, typename channel_t >
class MarkerFinderT : public MarkerFinder
{
private:
	typedef std::pair< pixel_t *, pixel_t * > __pointers;
	typedef std::pair< size_t, __pointers > __data;

private:
	pixel_t * const __frame;
	std::stack< __data > __stack;

	const size_t __channelsCount;	// left for possible future use.

public:
	MarkerFinderT( pixel_t * const frame, const size_t width, const size_t height )
		: MarkerFinder( width, height )
		, __frame( frame )
		, __channelsCount( sizeof( pixel_t ) / sizeof( channel_t ) ) {}

public:
	void FillAt( const Coordinates & seed );
	bool MatchesAt( const Coordinates & coord ) const { return _Matches( __frame[ coord.X + coord.Y * _width ] ); }

protected:
	virtual bool _Matches( const pixel_t & pixel ) const = 0;
	virtual void _Visit( pixel_t & pixel ) = 0;
	virtual bool _Visited( const pixel_t & pixel ) const = 0;

private:
	pixel_t * __FindLeft( pixel_t * const line, pixel_t * const p );
	pixel_t * __FindRight( pixel_t * const line, pixel_t * const p );
	void __CheckLine( pixel_t * const line, pixel_t * const left, pixel_t * const right, const size_t y );
};

template< typename pixel_t, typename channel_t >
void MarkerFinderT< pixel_t, channel_t >::FillAt( const Coordinates & seed )
{
	Marker.TopLeft = Marker.BottomRight = seed;

	pixel_t * line = __frame + seed.Y * _width;	// pointer to the beginning of the scanning line.
	pixel_t * p = line + seed.X;				// pointer to the beginning of the pixel.

	__stack.push( __data( seed.Y, __pointers( p, line ) ) );
	for ( ; !__stack.empty(); )
	{
		const __data current = __stack.top();
		__stack.pop();
		
		const size_t & y = current.first;
		p = current.second.first, line = current.second.second; 

		_Visit( *p );

		pixel_t * const left = __FindLeft( line, p );
		pixel_t * const right = __FindRight( line, p );

		_UpdateData( static_cast< int >( y ), left - line, right - line);

		if ( y + 1 < _height )
			__CheckLine( line + _width, left + _width, right + _width, y + 1 );
		if ( y >= 1 )	// read as 'y - 1 >= 0': due to 'y' beeing unsigned causes overflow.
			__CheckLine( line - _width, left - _width, right - _width, y - 1 );
	}

	_CalculateCenter();
}

template< typename pixel_t, typename channel_t >
pixel_t * MarkerFinderT< pixel_t, channel_t >::__FindLeft( pixel_t * const line, pixel_t * const p )
{
	register pixel_t * left = p - 1;
	for ( ; line <= left && _Matches( *left ); left-- )
		_Visit( *left );

	return ( left );
}

template< typename pixel_t, typename channel_t >
pixel_t * MarkerFinderT< pixel_t, channel_t >::__FindRight( pixel_t * const line, pixel_t * const p )
{
	register pixel_t * right = p + 1;
	for ( ; right < line + _width && _Matches( *right ); right++ )
		_Visit( *right );

	return ( right );
}

template< typename pixel_t, typename channel_t >
void MarkerFinderT< pixel_t, channel_t >::__CheckLine( pixel_t * const line, pixel_t * const left, pixel_t * const right, const size_t y )
{
	for ( register pixel_t * p = left; p <= right; )
	{
		// scan to the right while pixels are not-visited-and-matching.
		for ( ; p <= right && !_Visited( *p ) && _Matches( *p ); p++ );

		// if there's at least one not-visited-and-matching pixel then 'p' differs from 'left'.
		if ( p > left )
		{
			// push the last not-visited-and-matching pixel (previous to the one pointed by 'p').
			__stack.push( __data( y, __pointers( p - 1, line ) ) );
		}

		// scan farther while pixels are visited-or-not-matching.
		for ( p++; p <= right && ( _Visited( *p ) || !_Matches( *p ) ); p++ );
	}
}


class MarkerFinder_RGBA_8_8_8_8 : public MarkerFinderT< uint32_t, uint8_t >
{
private:
	enum { __RED = 0, __GREEN = 1, __BLUE = 2, __ALPHA = 3 };
	enum { __VISITED = 0x00FFFFFF };

private:
	const uint8_t __minBrightness;

public:
	MarkerFinder_RGBA_8_8_8_8( uint32_t * const frame, const size_t width, const size_t height, const double minBrightness )
		: MarkerFinderT< uint32_t, uint8_t >( frame, width, height )
		, __minBrightness( static_cast< uint8_t >( minBrightness * UCHAR_MAX ) ) {}

protected:
	virtual bool _Matches( const uint32_t & pixel ) const;
	virtual void _Visit( uint32_t & pixel );
	virtual bool _Visited( const uint32_t & pixel ) const;
};


class MarkerFinder_Grey_16 : public MarkerFinderT< uint16_t, uint16_t >
{
private:
	enum { __VISITED = 0xFFFF };

private:
	const uint16_t __minBrightness;

public:
	MarkerFinder_Grey_16( uint16_t * const frame, const size_t width, const size_t height, const double minBrightness )
		: MarkerFinderT< uint16_t, uint16_t >( frame, width, height )
		, __minBrightness( static_cast< uint16_t >( minBrightness * USHRT_MAX ) ) {}

protected:
	virtual bool _Matches( const uint16_t & pixel ) const;
	virtual void _Visit( uint16_t & pixel );
	virtual bool _Visited( const uint16_t & pixel ) const;
};
