#define _USE_MATH_DEFINES
#include <math.h>

#include "SolvePnP.h"
#include "p4p.h"
#include "p3p.h"
#include "transformation.h"
#include "shared.h"


size_t SolvePnP::operator() ( const Vertex2D pointsInitial[], const Vertex2D pointsActual[], const size_t count, const double focus )
{
	pixel * pixels_initial = ( pixel * )( pointsInitial );
	pixel * pixels_actual = ( pixel * )( pointsActual );

	for ( register size_t i = 0; i < count; i++ )
			printf( "Marker %u:\t( %.2f, %.2f )\n", i, pointsActual[ i ].X, pointsActual[ i ].Y );

	const double side = 2370;
	point3d model_points[4] = 
		{MODEL(0   ,    0),
		 MODEL(side,    0),
		 MODEL(   0, side),
		 MODEL(side, side)};

	matrix44 matrixTR = {0};
	aiming_beam beam  = {0};

	point3d world_points_initial[3] = {0};
	point3d world_points_actual[3]  = {0};

	double approx_initial[4] = {0};
	double approx_actual[4]  = {0};

	//

   	p4p_analytic(pixels_initial, model_points, focus, world_points_initial);
	p4p_analytic(pixels_actual, model_points,  focus, world_points_actual);
	
	/*/

	p4p_analytic(pixels_initial, model_points, focus, approx_initial);
	p4p_analytic(pixels_actual, model_points, focus, approx_actual);
	p3p(pixels_initial, model_points, focus, 1.0f, world_points_initial, approx_initial, 1000);
	p3p(pixels_actual , model_points, focus, 1.0f, world_points_actual , approx_actual , 1000);

	/**/
	
	get_transform(world_points_initial, world_points_actual, &matrixTR);
	obtain_angles_and_offset(&beam, matrixTR);

	__output_beam("p4p_analytic", beam, 0, 0, 0);

	return ( 1 );
}

void SolvePnP::__output_beam(const char* str, const aiming_beam& beam, int yaw, int pitch, int roll)
{
	printf("%s\r\n", str);
	
	printf("x_offset = %f\r\n", beam.x_offset);
	printf("y_offset = %f\r\n", beam.y_offset);
	printf("z_offset = %f\r\n", beam.z_offset);

	Attitude.SinYaw = beam.sin_yaw,		Attitude.CosYaw = beam.cos_yaw;
	Attitude.SinPitch = beam.sin_pitch,	Attitude.CosPitch = beam.cos_pitch;
	Attitude.SinRoll = beam.sin_roll,	Attitude.CosRoll = beam.cos_roll;

	printf("yaw   = %5.2f\t%5.2f\n", asin(beam.sin_yaw  )/M_PI*180, acos(beam.cos_yaw  )/M_PI*180);
	printf("pitch = %5.2f\t%5.2f\n", asin(beam.sin_pitch)/M_PI*180, acos(beam.cos_pitch)/M_PI*180);
	printf("roll  = %5.2f\t%5.2f\n", asin(beam.sin_roll )/M_PI*180, acos(beam.cos_roll )/M_PI*180);
}
