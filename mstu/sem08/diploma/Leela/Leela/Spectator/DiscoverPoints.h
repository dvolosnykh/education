#pragma once

#include <utility>
#include <vector>

#include "MarkerFinders.h"


class DiscoverPoints
{
private:
	typedef std::pair< Vertex2D, Square > __PointData;
	typedef std::vector< __PointData > __PointsList;

private:
	size_t __gridStepHorz, __gridStepVert;
	double __minBrightness;

public:
	DiscoverPoints()
		: __gridStepHorz( 1 ), __gridStepVert( 1 )
		, __minBrightness( 1.0 ) {}

public:
	size_t operator()( Vertex2D * const markers, const size_t pointsExpected, void * const frame, const size_t width, const size_t height );

private:
	static bool __InOneOfTheFounded( const __PointsList & found, const Coordinates & coord );
	static bool __ChooseMarkers( Vertex2D * const markers, const size_t expectedCount, const __PointsList & found );
	void __UpdateGridSteps( const long minWidth, const long minHeight, const size_t markersFound, const size_t pointsExpected );
	void __UpdateMinBrightness();
};
