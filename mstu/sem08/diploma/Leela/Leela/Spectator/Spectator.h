#pragma once

#include "../Camera/CameraManager.h"
#include "../FrameInfo.h"
#include "Intercommunication.h"
#include "FindTransformation.h"


class Spectator : public TerminableAsyncJob
{
private:
	CameraManager * const __pCameraManager;
	Camera::Info __cameraInfo;
	Event * const __pNewFrameReady;
	Event * const __pFrameProcessed;
	Event * const __pFrameResized;
	Event * const __pCalibrationNeeded;
	FindTransformation __findTransformation;

public:
	Spectator( CameraManager * const pCameraManager, Event * const pNewFrameReady, Event * const pFrameProcessed, Event * const pFrameResized, Event * const pCalibrationNeeded )
		: __pCameraManager( pCameraManager )
		, __pNewFrameReady( pNewFrameReady )
		, __pFrameProcessed( pFrameProcessed )
		, __pFrameResized( pFrameResized )
		, __pCalibrationNeeded( pCalibrationNeeded ) {}
	virtual ~Spectator() {}

protected:
	virtual void _Main();
	virtual bool _Initialize();
	virtual void _Deinitialize();

private:
	void __ReportAttitude( const bool success );
};
