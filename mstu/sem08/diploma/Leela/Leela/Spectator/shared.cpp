#include "shared.h"

#include <stdio.h>


//////////////////////////////
// operators

// 4x4 * 4x4 = 4x4
matrix44 operator * (const matrix44& left, const matrix44& right)
{
	matrix44 mult_matrix = {left.e[0]*right.e[0] + left.e[1]*right.e[4] + left.e[2]*right.e[8 ] + left.e[3]*right.e[12],
							left.e[0]*right.e[1] + left.e[1]*right.e[5] + left.e[2]*right.e[9 ] + left.e[3]*right.e[13],
							left.e[0]*right.e[2] + left.e[1]*right.e[6] + left.e[2]*right.e[10] + left.e[3]*right.e[14],
							left.e[0]*right.e[3] + left.e[1]*right.e[7] + left.e[2]*right.e[11] + left.e[3]*right.e[15],

							left.e[4]*right.e[0] + left.e[5]*right.e[4] + left.e[6]*right.e[8 ] + left.e[7]*right.e[12],
							left.e[4]*right.e[1] + left.e[5]*right.e[5] + left.e[6]*right.e[9 ] + left.e[7]*right.e[13],
							left.e[4]*right.e[2] + left.e[5]*right.e[6] + left.e[6]*right.e[10] + left.e[7]*right.e[14],
							left.e[4]*right.e[3] + left.e[5]*right.e[7] + left.e[6]*right.e[11] + left.e[7]*right.e[15],

							left.e[8]*right.e[0] + left.e[9]*right.e[4] + left.e[10]*right.e[8 ] + left.e[11]*right.e[12],
							left.e[8]*right.e[1] + left.e[9]*right.e[5] + left.e[10]*right.e[9 ] + left.e[11]*right.e[13],
							left.e[8]*right.e[2] + left.e[9]*right.e[6] + left.e[10]*right.e[10] + left.e[11]*right.e[14],
							left.e[8]*right.e[3] + left.e[9]*right.e[7] + left.e[10]*right.e[11] + left.e[11]*right.e[15],

							left.e[12]*right.e[0] + left.e[13]*right.e[4] + left.e[14]*right.e[8 ] + left.e[15]*right.e[12],
							left.e[12]*right.e[1] + left.e[13]*right.e[5] + left.e[14]*right.e[9 ] + left.e[15]*right.e[13],
							left.e[12]*right.e[2] + left.e[13]*right.e[6] + left.e[14]*right.e[10] + left.e[15]*right.e[14],
							left.e[12]*right.e[3] + left.e[13]*right.e[7] + left.e[14]*right.e[11] + left.e[15]*right.e[15]};
	return mult_matrix;
}

// 4x4 * 4x1 = 4x1
vector4 operator * (const matrix44& left, const vector4& right)
{
	vector4 mult_vector = {left.e[0 ]*right.v[0] + left.e[1 ]*right.v[1] + left.e[2 ]*right.v[2] + left.e[3 ]*right.v[3],
						   left.e[4 ]*right.v[0] + left.e[5 ]*right.v[1] + left.e[6 ]*right.v[2] + left.e[7 ]*right.v[3],
						   left.e[8 ]*right.v[0] + left.e[9 ]*right.v[1] + left.e[10]*right.v[2] + left.e[11]*right.v[3],
						   left.e[12]*right.v[0] + left.e[13]*right.v[1] + left.e[14]*right.v[2] + left.e[15]*right.v[3]};
	return mult_vector;
}

// 1x3/3x1 - 1x3/3x1 = 1x3/3x1
vector4 operator - (const vector4& left, const vector4& right)
{
	vector4 subtract_vector = {left.v[0] - right.v[0],
						       left.v[1] - right.v[1],
						       left.v[2] - right.v[2],
							   left.v[3] - right.v[3]};
	return subtract_vector;
}

// 3x3 * 3x3 = 3x3
matrix33 operator * (const matrix33& left, const matrix33& right)
{
	matrix33 mult_matrix = {left.e[0]*right.e[0] + left.e[1]*right.e[3] + left.e[2]*right.e[6],
							left.e[0]*right.e[1] + left.e[1]*right.e[4] + left.e[2]*right.e[7],
							left.e[0]*right.e[2] + left.e[1]*right.e[5] + left.e[2]*right.e[8],

							left.e[3]*right.e[0] + left.e[4]*right.e[3] + left.e[5]*right.e[6],
							left.e[3]*right.e[1] + left.e[4]*right.e[4] + left.e[5]*right.e[7],
							left.e[3]*right.e[2] + left.e[4]*right.e[5] + left.e[5]*right.e[8],

							left.e[6]*right.e[0] + left.e[7]*right.e[3] + left.e[8]*right.e[6],
							left.e[6]*right.e[1] + left.e[7]*right.e[4] + left.e[8]*right.e[7],
							left.e[6]*right.e[2] + left.e[7]*right.e[5] + left.e[8]*right.e[8]};
	return mult_matrix;
}

// 3x3 * 3x1 = 3x1
vector3 operator * (const matrix33& left, const vector3& right)
{
	vector3 mult_vector = {left.e[0]*right.v[0] + left.e[1]*right.v[1] + left.e[2]*right.v[2],
						   left.e[3]*right.v[0] + left.e[4]*right.v[1] + left.e[5]*right.v[2],
						   left.e[6]*right.v[0] + left.e[7]*right.v[1] + left.e[8]*right.v[2]};
	return mult_vector;
}

// 1x3 * 3x3 = 1x3
vector3 operator * (const vector3& left, const matrix33& right)
{
	vector3 mult_vector = {left.v[0]*right.e[0] + left.v[1]*right.e[3] + left.v[2]*right.e[6],
						   left.v[0]*right.e[1] + left.v[1]*right.e[4] + left.v[2]*right.e[7],
						   left.v[0]*right.e[2] + left.v[1]*right.e[5] + left.v[2]*right.e[8]};
	return mult_vector;
}

// 1x3/3x1 * scalar = 1x3/3x1
vector3 operator * (const vector3& left, const double scalar)
{
	vector3 mult_vector = {left.v[0]*scalar, left.v[1]*scalar, left.v[2]*scalar};

	return mult_vector;
}

vector3 operator * (const double scalar, const vector3& right)
{
	vector3 mult_vector = {right.v[0]*scalar + right.v[1]*scalar + right.v[2]*scalar};
	
	return mult_vector;
}

// 1x3 * 3x1 = scalar
double operator * (const vector3& left, const vector3& right)
{
	return left.v[0]*right.v[0] + left.v[1]*right.v[1] + left.v[2]*right.v[2];
}

// 1x3/3x1 - 1x3/3x1 = 1x3/3x1
vector3 operator - (const vector3& left, const vector3& right)
{
	vector3 subtract_vector = {left.v[0] - right.v[0],
						       left.v[1] - right.v[1],
						       left.v[2] - right.v[2]};
	return subtract_vector;
}

// 1x3/3x1 == 1x3/3x1
bool operator == (const vector3& left, const vector3& right)
{
	return (fabs(left.v[0] - right.v[0]) < eps) && 
		   (fabs(left.v[1] - right.v[1]) < eps) && 
		   (fabs(left.v[2] - right.v[2]) < eps);
}

// 1x4/4x1 == 1x4/4x1
bool operator == (const vector4& left, const vector4& right)
{
	return (fabs(left.v[0] - right.v[0]) < eps) && 
		   (fabs(left.v[1] - right.v[1]) < eps) && 
		   (fabs(left.v[2] - right.v[2]) < eps) && 
		   (fabs(left.v[3] - right.v[3]) < eps);
}

// 3x3 == 3x3
bool operator == (const matrix33& left, const matrix33& right)
{
	unsigned zeroes = 0;
	
	for (unsigned i = 0; i < 9; ++i)
		if (fabs(left.e[i] - right.e[i]) < eps) ++zeroes;

	return (zeroes == 9);
}

// 4x4 == 4x4
bool operator == (const matrix44& left, const matrix44& right)
{
	unsigned zeroes = 0;
	
	for (unsigned i = 0; i < 16; ++i)
		if (fabs(left.e[i] - right.e[i]) < eps) ++zeroes;

	return (zeroes == 16);
}


//////////////////////////////
// common operations

double square_distance(const point3d& point1, const point3d& point2)
{
	return (point1.x - point2.x)*(point1.x - point2.x) +  
		   (point1.y - point2.y)*(point1.y - point2.y) +
		   (point1.z - point2.z)*(point1.z - point2.z);
}

vector3 cross_product(const vector3& left, const vector3& right)
{
	vector3 result = {left.v[1]*right.v[2] - left.v[2]*right.v[1], 
				      left.v[2]*right.v[0] - left.v[0]*right.v[2],
					  left.v[0]*right.v[1] - left.v[1]*right.v[0]};
	return result;
}

vector3 get_unit_vector(double x, double y, double z)
{
	double length = x*x + y*y + z*z;
	vector3 unit = {0};

	if (length > eps)
	{
		double inv_length = 1.0f / sqrt(length);
		unit.v[0] = x*inv_length;
		unit.v[1] = y*inv_length; 
		unit.v[2] = z*inv_length;
	}

	return unit;
}

void normalize(vector3* vector)
{
	*vector = get_unit_vector(vector->v[0], vector->v[1], vector->v[2]);
}

bool is_confluent(const vector3& vector)
{
	return (fabs(vector.v[0]) < eps) && (fabs(vector.v[1]) < eps) && (fabs(vector.v[2]) < eps);
}

double vector_abs(const vector3& vector)
{
	return sqrt(vector * vector);
}

#define DET2(e1, e2, e3, e4) (e1*e4 - e2*e3)

bool invert(const matrix33& matrix, matrix33* inverted) //(mult/div = 39/1)
{
	bool can_invert = false;

	double determinant = matrix.e[0]*matrix.e[4]*matrix.e[8] + 
				         matrix.e[1]*matrix.e[5]*matrix.e[6] +
						 matrix.e[2]*matrix.e[3]*matrix.e[7] -

						 matrix.e[2]*matrix.e[4]*matrix.e[6] -
						 matrix.e[0]*matrix.e[5]*matrix.e[7] -
						 matrix.e[1]*matrix.e[3]*matrix.e[8];
	
	if (fabs(determinant) > eps)
	{
		double inv_det = 1.0f / determinant;

		inverted->e[0] =  DET2(matrix.e[4], matrix.e[5], matrix.e[7], matrix.e[8]) * inv_det;
		inverted->e[3] = -DET2(matrix.e[3], matrix.e[5], matrix.e[6], matrix.e[8]) * inv_det;
		inverted->e[6] =  DET2(matrix.e[3], matrix.e[4], matrix.e[6], matrix.e[7]) * inv_det;

		inverted->e[1] = -DET2(matrix.e[1], matrix.e[2], matrix.e[7], matrix.e[8]) * inv_det;
		inverted->e[4] =  DET2(matrix.e[0], matrix.e[2], matrix.e[6], matrix.e[8]) * inv_det;
		inverted->e[7] = -DET2(matrix.e[0], matrix.e[1], matrix.e[6], matrix.e[7]) * inv_det;

		inverted->e[2] =  DET2(matrix.e[1], matrix.e[2], matrix.e[4], matrix.e[5]) * inv_det;
		inverted->e[5] = -DET2(matrix.e[0], matrix.e[2], matrix.e[3], matrix.e[5]) * inv_det;
		inverted->e[8] =  DET2(matrix.e[0], matrix.e[1], matrix.e[3], matrix.e[4]) * inv_det;

		can_invert = true;
	}

	return can_invert; 
}

//////////////////////////////
// debug_output

void print_matrix(const char* str, const matrix44& matrix)
{
	printf("%s\r\n", str);
	for (unsigned i = 0; i < 4; ++i)
	{
		for (unsigned j = 0; j < 4; ++j)
		{
			printf("%10.2f", matrix.e[i*4 + j]);
		}
		printf("\r\n");
	}

	printf("\r\n");
}

void print_matrix(const char* str, const matrix33& matrix)
{
	printf("%s\r\n", str);
	for (unsigned i = 0; i < 3; ++i)
	{
		for (unsigned j = 0; j < 3; ++j)
		{
			printf("%10.2f", matrix.e[i*3 + j]);
		}
		printf("\r\n");
	}

	printf("\r\n");
}
