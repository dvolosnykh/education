#include <limits>
#include <math.h>
#include <stdio.h>
#include <time.h>

#include "OrderPoints.h"
#include "../Exceptions.h"

// yaw: 100, pitch: 163, roll: 0  -- bad order (good origin though)
void OrderPoints::operator() ( Vertex2D * points, const size_t count )
{
	__points = points, __count = count;

	const size_t originIndex = __FindOriginIndex();
	const Vertex2D center = __FindCenter();

	size_t * const order = new size_t[ __count ];
	if ( order == NULL )
		throw MemoryException( "new size_t" );

	__FindClockwiseSelection( order, originIndex, __points[ originIndex ] - center );

	printf( "order:\t" );
	for ( register size_t i = 0; i < __count; i++ )
		printf( "%u ", order[ i ] );
	printf( "\n" );

	Vertex2D * p = new Vertex2D[ __count ];
	if ( p == NULL )
		throw MemoryException( "new Vertex2D" );

	for ( register size_t i = 0; i < __count; i++ )
		p[ i ] = __points[ order[ i ] ];

	memcpy( points, p, sizeof( Vertex2D ) * __count );

	delete p;
	delete order;
}

size_t OrderPoints::__FindOriginIndex() const
{
	size_t originIndex = 0;
	double minCosine2 = std::numeric_limits< double >::max();

	for ( register size_t i = 0; i < __count; i++ )
		for ( register size_t j = 0; j < __count; j++ )
			for ( register size_t k = 0; k < __count; k++ )
			{
				const Vertex2D v1 = __points[ j ] - __points[ i ];
				const Vertex2D v2 = __points[ k ] - __points[ i ];
				const double cosine2 = ( v1.X * v2.X + v1.Y * v2.Y ) / sqrt( v1.Length2() * v2.Length2() );

				if ( cosine2 < minCosine2 )
				{
					minCosine2 = cosine2;
					originIndex = i;
				}
			}

	return ( originIndex );
}

Vertex2D OrderPoints::__FindCenter() const
{
	double minX = std::numeric_limits< double >::max();
	double minY = std::numeric_limits< double >::max();
	double maxX = -std::numeric_limits< double >::max();
	double maxY = -std::numeric_limits< double >::max();

	for ( register size_t i = 0; i < __count; i++ )
	{
		if ( __points[ i ].X < minX )
			minX = __points[ i ].X;
		else if ( __points[ i ].X > maxX )
			maxX = __points[ i ].X;

		if ( __points[ i ].Y < minY )
			minY = __points[ i ].Y;
		else if ( __points[ i ].Y > maxY )
			maxY = __points[ i ].Y;
	}

	return Vertex2D( ( minX + maxX ) * 0.5, ( minY + maxY ) * 0.5 );
}

void OrderPoints::__FindClockwiseSelection( size_t order[], const size_t originIndex, Vertex2D direction ) const
{
	bool * visited = new bool[ __count ];
	if ( visited == NULL )
		throw MemoryException( "new bool" );

	std::fill( visited, visited + __count, false );

	size_t orderIndex = 0;
	for ( size_t current = originIndex; ; )
	{
		visited[ current ] = true;
		order[ orderIndex++ ] = current;

		int next = -1;
		double maxCosine2 = -std::numeric_limits< double >::max();
		for ( register size_t i = 0; i < __count; i++ )
			if ( !visited[ i ] )
			{
				const Vertex2D tested = __points[ i ] - __points[ current ];
				const double sineMeanValue = direction.X * tested.Y - tested.X * direction.Y;

				if ( sineMeanValue <= 0.0 )
				{
					const double cosine2 = ( direction.X * tested.X + direction.Y * tested.Y ) / sqrt( direction.Length2() * tested.Length2() );
					if ( cosine2 > maxCosine2 )
					{
						maxCosine2 = cosine2;
						next = i;
					}
				}
			}

	if ( next < 0 ) break;

		direction = __points[ next ] - __points[ current ];
		current = next;
	}

	delete visited;
}