#ifndef P4P_H_56874690
#define P4P_H_56874690

#include "shared.h"

bool invert_jacobian44(const double factors[4]     , 
					   const double dot_products[4], 
					   double* inverted)
{
	bool result = false;

	double J11 = 2*(factors[0] - factors[1]*dot_products[0]);
	double J22 = 2*(factors[1] - factors[2]*dot_products[1]);
	double J33 = 2*(factors[2] - factors[3]*dot_products[2]);
	double J44 = 2*(factors[3] - factors[0]*dot_products[3]);

	double J12 = 2*(factors[1] - factors[0]*dot_products[0]);
	double J23 = 2*(factors[2] - factors[1]*dot_products[1]);
	double J34 = 2*(factors[3] - factors[2]*dot_products[2]);
	double J41 = 2*(factors[0] - factors[3]*dot_products[3]);
	
	double determinant = J11*J22*J33*J44 - J12*J23*J34*J41;

	if (fabs(determinant) > eps)
	{
		double inv_det = 1.0f / determinant;
		
		inverted[0 ] =  J22*J33*J44*inv_det;
		inverted[1 ] = -J12*J33*J44*inv_det;
		inverted[2 ] =  J12*J23*J44*inv_det;
		inverted[3 ] = -J12*J23*J34*inv_det;

		inverted[4 ] = -J23*J34*J41*inv_det;
		inverted[5 ] =  J11*J33*J44*inv_det;
		inverted[6 ] = -J11*J23*J44*inv_det;
		inverted[7 ] =  J11*J23*J34*inv_det;

		inverted[8 ] =  J22*J34*J41*inv_det;
		inverted[9 ] = -J12*J34*J41*inv_det;
		inverted[10] =  J11*J22*J44*inv_det;
		inverted[11] = -J11*J22*J34*inv_det;

		inverted[12] = -J22*J33*J41*inv_det; 
		inverted[13] =  J12*J33*J41*inv_det;
		inverted[14] = -J12*J23*J41*inv_det;
		inverted[15] =  J11*J22*J33*inv_det;

		result = true;
	}

	return result;
}

void estimate(const vector4& factors         , 
			  const vector4& dot_products    , 
			  const vector4& square_distances, 
			  vector4* deltas)
{
	double factor0_sqr = factors.v[0]*factors.v[0];
	double factor1_sqr = factors.v[1]*factors.v[1];
	double factor2_sqr = factors.v[2]*factors.v[2];
	double factor3_sqr = factors.v[3]*factors.v[3];

	deltas->v[0] = factor0_sqr - 2*factors.v[0]*factors.v[1]*dot_products.v[0] + factor1_sqr - square_distances.v[0];
	deltas->v[1] = factor1_sqr - 2*factors.v[1]*factors.v[2]*dot_products.v[1] + factor2_sqr - square_distances.v[1];
	deltas->v[2] = factor2_sqr - 2*factors.v[2]*factors.v[3]*dot_products.v[2] + factor3_sqr - square_distances.v[2];
	deltas->v[3] = factor3_sqr - 2*factors.v[3]*factors.v[0]*dot_products.v[3] + factor0_sqr - square_distances.v[3];
}

int p4p_analytic(const pixel*   pixels      , 
				 const point3d* model_points, 
				 const double    focus       , 
				 point3d* world_points)
				// double*         approx)
{	
	int error_code = PNP_NO_ERROR;

	// initialization
	vector3 unit_vector0 = {pixels[0].x, pixels[0].y, -focus},
			unit_vector1 = {pixels[1].x, pixels[1].y, -focus},
			unit_vector2 = {pixels[2].x, pixels[2].y, -focus},
			unit_vector3 = {pixels[3].x, pixels[3].y, -focus};

	/*/

	vector3 unit_vector0 = get_unit_vector(pixels[0].x, pixels[0].y, -focus),
			unit_vector1 = get_unit_vector(pixels[1].x, pixels[1].y, -focus),
			unit_vector2 = get_unit_vector(pixels[2].x, pixels[2].y, -focus),
			unit_vector3 = get_unit_vector(pixels[3].x, pixels[3].y, -focus);
	/**/

	vector3 model_p3 = VECTOR3(model_points[3]);

	// analytical solution
	matrix33 unit_vectors = { unit_vector0.v[0], unit_vector0.v[1], unit_vector0.v[2],
					          unit_vector1.v[0], unit_vector1.v[1], unit_vector1.v[2],
					          unit_vector2.v[0], unit_vector2.v[1], unit_vector2.v[2]};

	matrix33 inv_unit_vectors = {0};
		
	if (invert(unit_vectors, &inv_unit_vectors))
	{
		vector3 factors = unit_vector3 * inv_unit_vectors;
		factors.v[0]    = -factors.v[0];
		
		double factor_3 = vector_abs(model_p3)/vector_abs(unit_vector3 - factors.v[0]*unit_vector0);
		factors         = factors * factor_3;

		/*/output
		
		approx[0] = factors.v[0];
		approx[1] = factors.v[1];
		approx[2] = factors.v[2];

		/*/
		
		world_points[0] = (unit_vector0 * factors.v[0]).p;
		world_points[1] = (unit_vector1 * factors.v[1]).p;
		world_points[2] = (unit_vector2 * factors.v[2]).p;
		/**/
	}
	else
	{
		error_code = ZERO_DETERMINANT;	
	}

	return error_code;
}

int p4p_iterative(const pixel*    pixels      , 
				  const point3d*  model_points, 
				  const double     focus       , 
				  const double     tolerance   ,
				  point3d*        world_points,
				  const double*    factor_approx = 0,
				  const unsigned  iter_limit    = -1)
{	
	int error_code = PNP_NO_ERROR;

	// initialization
	vector3 unit_vector0 = get_unit_vector(pixels[0].x, pixels[0].y, -focus),
			unit_vector1 = get_unit_vector(pixels[1].x, pixels[1].y, -focus),
			unit_vector2 = get_unit_vector(pixels[2].x, pixels[2].y, -focus),
			unit_vector3 = get_unit_vector(pixels[3].x, pixels[3].y, -focus);

	vector4 square_distances = {0};
	square_distances.v[0] = square_distance(model_points[0], model_points[1]);   
	square_distances.v[1] = square_distance(model_points[1], model_points[2]);
	square_distances.v[2] = square_distance(model_points[2], model_points[3]);
	square_distances.v[3] = square_distance(model_points[3], model_points[0]);

	vector4 dot_products  = {(unit_vector0 * unit_vector1),
							 (unit_vector1 * unit_vector2),
							 (unit_vector2 * unit_vector3),
							 (unit_vector3 * unit_vector0)};
	
	vector4 factors = {1, 1, 1, 1};
	if (factor_approx)
		memcpy(factors.v, factor_approx, sizeof(factors));
		
	//iteration
	unsigned iteration = 0;
	vector4  deltas    = {0};
	matrix44 inverted_jacobian = {0};
	
	estimate(factors, dot_products, square_distances, &deltas);

	while (((fabs(deltas.v[0]) > tolerance)  ||
			(fabs(deltas.v[1]) > tolerance)  ||
			(fabs(deltas.v[2]) > tolerance)  ||
			(fabs(deltas.v[3]) > tolerance)) &&
		    (iter_limit        > iteration++)&&
			(error_code == PNP_NO_ERROR))
	{
		if (invert_jacobian44(factors.v, dot_products.v, inverted_jacobian.e))
		{
			estimate(factors, dot_products, square_distances, &deltas);
			factors = factors - inverted_jacobian * deltas;	
		}
		else
		{
			error_code = ZERO_DETERMINANT;
		}
	}

	//output
	world_points[0] = (unit_vector0 * factors.v[0]).p;
	world_points[1] = (unit_vector1 * factors.v[1]).p;
	world_points[2] = (unit_vector2 * factors.v[2]).p;

	return error_code;
}

#endif  //P4P_H_56874690
