#include <stdio.h>

#include "Spectator.h"
#include "../Exceptions.h"
#include "../Mathematics.h"


void Spectator::_Main()
{
	for ( ; !_TerminationRequested(); )
	{
		if ( __pFrameResized->TryWait() )
		{
			__cameraInfo = __pCameraManager->GetCameraInfo();
			__pFrameResized->Reset();
		}

		if ( __pNewFrameReady->TimedWait( __cameraInfo.FrameInterval ) )
		{
			bool updateInitialPosition = __pCalibrationNeeded->TryWait();
			if ( updateInitialPosition )
				__pCalibrationNeeded->Reset();

			bool success = false;
			unsigned char * frame = __pCameraManager->LockFrame();
			{
				success = __findTransformation( frame, __cameraInfo.Frame.Width, __cameraInfo.Frame.Height, __cameraInfo.Focus, updateInitialPosition );
			}
			__pCameraManager->UnlockFrame();

			__ReportAttitude( success );

			__pFrameProcessed->Set();
			__pNewFrameReady->Reset();
		}
	}
}

bool Spectator::_Initialize()
{
	__pFrameResized->Wait();
	__pCalibrationNeeded->Wait();

	return ( true );
}

void Spectator::_Deinitialize()
{
}

#define _USE_MATH_DEFINES
#include <math.h>

void Spectator::__ReportAttitude( const bool success )
{
	const double & sinYaw = __findTransformation.Attitude.SinYaw;
	const double & sinPitch = __findTransformation.Attitude.SinPitch;
	const double & sinRoll = __findTransformation.Attitude.SinRoll;
	__pCameraManager->SaveResults( success, asin( sinYaw )/M_PI*180, asin( sinPitch )/M_PI*180, asin( -sinRoll )/M_PI*180 );
}
