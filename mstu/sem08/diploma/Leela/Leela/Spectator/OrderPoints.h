#pragma once


#include "../Mathematics/Vertex.h"


class OrderPoints
{
private:
	const Vertex2D * __points;
	size_t __count;

public:
	OrderPoints() {}
	virtual ~OrderPoints() {}

public:
	void operator() ( Vertex2D * points, const size_t count );

private:
	size_t __FindOriginIndex() const;
	Vertex2D __FindCenter() const;
	void __FindClockwiseSelection( size_t order[], const size_t originIndex, Vertex2D direction ) const;
};
