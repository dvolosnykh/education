#ifndef TRANSFORMATION_H_12094565
#define TRANSFORMATION_H_12094565

#include "shared.h"
#include <algorithm>
	
matrix44 invert44TR(const matrix44& inversive)
{	
	matrix44 inverted = inversive;

	std::swap(inverted.e[1], inverted.e[4]);
	std::swap(inverted.e[2], inverted.e[8]);
	std::swap(inverted.e[6], inverted.e[9]);

	/*/
	inverted.e[3 ] = -inverted.e[3 ];
    inverted.e[7 ] = -inverted.e[7 ];
    inverted.e[11] = -inverted.e[11]; 

	/*/

	double e3  = inverted.e[0]*inverted.e[3] + inverted.e[1]*inverted.e[7] + inverted.e[2 ]*inverted.e[11];
    double e7  = inverted.e[4]*inverted.e[3] + inverted.e[5]*inverted.e[7] + inverted.e[6 ]*inverted.e[11];
    double e11 = inverted.e[8]*inverted.e[3] + inverted.e[9]*inverted.e[7] + inverted.e[10]*inverted.e[11];
	inverted.e[3 ] = -e3;
    inverted.e[7 ] = -e7;
    inverted.e[11] = -e11; 

	/**/

	return inverted;
}

void transfer_to_origin(point3d* points, matrix44* T)
{
	double x_offset = -points[0].x;
	double y_offset = -points[0].y;
	double z_offset = -points[0].z;

	points[0].x = 0;
	points[0].y = 0;
	points[0].z = 0;
	
	points[1].x += x_offset;
	points[1].y += y_offset;
	points[1].z += z_offset;

	points[2].x += x_offset;
	points[2].y += y_offset;
	points[2].z += z_offset;

	T->e[0 ] = 1;
	T->e[1 ] = 0;
	T->e[2 ] = 0;
	T->e[3 ] = x_offset;

	T->e[4 ] = 0;
	T->e[5 ] = 1;
	T->e[6 ] = 0;
	T->e[7 ] = y_offset;
	
	T->e[8 ] = 0;
	T->e[9 ] = 0;
	T->e[10] = 1;
	T->e[11] = z_offset;
	
	T->e[12] = 0;
	T->e[13] = 0;
	T->e[14] = 0;
	T->e[15] = 1;
}

void superpose_with_axis_X(point3d* points, matrix44* R)
{ 
	vector3 vectors[3] = {{0}, VECTOR3(points[1]), VECTOR3(points[2])};
	
	if (!is_confluent(vectors[1]))
	{
		vector3 x_ort = {1, 0, 0};
		vector3 pivot_pin = cross_product(vectors[1], x_ort);
		normalize(&pivot_pin);

		double cos_angle     = (vectors[1] * x_ort) / vector_abs(vectors[1]); // x_ort * x_ort == 1;  
		double sin_angle     = sqrt(1 - cos_angle*cos_angle);
		double inv_cos_angle = 1 - cos_angle;

		matrix33 rot_matrix = {	inv_cos_angle*pivot_pin.v[0]*pivot_pin.v[0] + cos_angle,
								inv_cos_angle*pivot_pin.v[0]*pivot_pin.v[1] - sin_angle*pivot_pin.v[2],
								inv_cos_angle*pivot_pin.v[0]*pivot_pin.v[2] + sin_angle*pivot_pin.v[1],

								inv_cos_angle*pivot_pin.v[0]*pivot_pin.v[1] + sin_angle*pivot_pin.v[2], 
								inv_cos_angle*pivot_pin.v[1]*pivot_pin.v[1] + cos_angle,
								inv_cos_angle*pivot_pin.v[1]*pivot_pin.v[2] - sin_angle*pivot_pin.v[0],
								
								inv_cos_angle*pivot_pin.v[0]*pivot_pin.v[2] - sin_angle*pivot_pin.v[1], 
								inv_cos_angle*pivot_pin.v[1]*pivot_pin.v[2] + sin_angle*pivot_pin.v[0],
								inv_cos_angle*pivot_pin.v[2]*pivot_pin.v[2] + cos_angle};

		R->e[0 ] = rot_matrix.e[0];
		R->e[1 ] = rot_matrix.e[1];
		R->e[2 ] = rot_matrix.e[2];
		R->e[3 ] = 0;

		R->e[4 ] = rot_matrix.e[3]; 
		R->e[5 ] = rot_matrix.e[4];
		R->e[6 ] = rot_matrix.e[5];
		R->e[7 ] = 0;

		R->e[8 ] = rot_matrix.e[6];
		R->e[9 ] = rot_matrix.e[7];
		R->e[10] = rot_matrix.e[8];
		R->e[11] = 0;

		R->e[12] = 0;
		R->e[13] = 0;
		R->e[14] = 0;
		R->e[15] = 1;

		vectors[1] = rot_matrix * vectors[1];
		vectors[2] = rot_matrix * vectors[2]; 

		points[1] = vectors[1].p;
		points[2] = vectors[2].p;
	}
}

void superpose_with_plane_XY(point3d* points, matrix44* R)
{
	vector3 ort_Y            = {0, 1, 0};
	vector3 v_triangle_plane = {0, points[2].y, points[2].z};

	if (!is_confluent(v_triangle_plane))
	{
		double cos_angle = (ort_Y * v_triangle_plane) / sqrt(v_triangle_plane * v_triangle_plane); // ort_Y * ort_Y == 1;   
		double sin_angle = sqrt(1 - cos_angle*cos_angle);

		if (points[2].z < 0)
			sin_angle = -sin_angle;

		matrix33 rot_matrix = {	1,		   0,          0,
								0, cos_angle, -sin_angle,
								0, sin_angle,  cos_angle};

		R->e[0 ] = rot_matrix.e[0];
		R->e[1 ] = rot_matrix.e[1];
		R->e[2 ] = rot_matrix.e[2];
		R->e[3 ] = 0;

		R->e[4 ] = rot_matrix.e[3]; 
		R->e[5 ] = rot_matrix.e[4];
		R->e[6 ] = rot_matrix.e[5];
		R->e[7 ] = 0;

		R->e[8 ] = rot_matrix.e[6];
		R->e[9 ] = rot_matrix.e[7];
		R->e[10] = rot_matrix.e[8];
		R->e[11] = 0;

		R->e[12] = 0;
		R->e[13] = 0;
		R->e[14] = 0;
		R->e[15] = 1;	

		vector3 v_top = VECTOR3(points[2]);

		v_top     = v_top * rot_matrix;
		points[2] = v_top.p;
	}
}

void get_transform(const point3d* initial_points, const point3d* actual_points, matrix44* transformation_matrix)
{
	point3d points_M[3] = {initial_points[0], initial_points[1], initial_points[2]};
	point3d points_W[3] = {actual_points [0], actual_points [1], actual_points [2]};

	matrix44 T_WW_1 = {0};
	matrix44 T_WW_2 = {0};

	transfer_to_origin(points_M, &T_WW_1);
	transfer_to_origin(points_W, &T_WW_2);

	matrix44 R_WW_1 = {0};
	matrix44 R_WW_2 = {0};

	superpose_with_axis_X(points_M, &R_WW_1);
	superpose_with_axis_X(points_W, &R_WW_2);

	matrix44 R_WW_3 = {0};
	matrix44 R_WW_4 = {0};

	superpose_with_plane_XY(points_M, &R_WW_3);
	superpose_with_plane_XY(points_W, &R_WW_4);

	matrix44 inv_T_WW_2 = invert44TR(T_WW_2);
	matrix44 inv_R_WW_2 = invert44TR(R_WW_2);
	matrix44 inv_R_WW_4 = invert44TR(R_WW_4);

	*transformation_matrix = ((((inv_T_WW_2 * inv_R_WW_2) * inv_R_WW_4 * R_WW_3) * R_WW_1) * T_WW_1);

	/*/
	print_matrix("T_WW_1", T_WW_1);
	print_matrix("T_WW_2", T_WW_2);
	print_matrix("R_WW_1", R_WW_1);
	print_matrix("R_WW_2", R_WW_2);
	print_matrix("R_WW_3", R_WW_3);
	print_matrix("R_WW_4", R_WW_4);
	print_matrix("inv_R_WW_4", inv_R_WW_4);
	print_matrix("inv_R_WW_4 * R_WW_3", inv_R_WW_4 * R_WW_3);
	print_matrix("result", *transformation_matrix);
	/**/
}

void obtain_angles_and_offset(aiming_beam* beam, const matrix44& transformation_matrix)
{
	beam->x_offset = transformation_matrix.e[3 ];
	beam->y_offset = transformation_matrix.e[7 ];
	beam->z_offset = transformation_matrix.e[11];

	beam->sin_pitch = -transformation_matrix.e[6];
	beam->cos_pitch = sqrt(1 - beam->sin_pitch*beam->sin_pitch);

	double inv_cos_pitch = 1.0f / beam->cos_pitch; // abs(beam->cos_pitch) always > 0

	beam->sin_yaw  = transformation_matrix.e[2] * inv_cos_pitch;
	beam->cos_yaw  = transformation_matrix.e[10] * inv_cos_pitch;

	beam->sin_roll = transformation_matrix.e[4] * inv_cos_pitch;
	beam->cos_roll = transformation_matrix.e[5] * inv_cos_pitch;
}

#endif //TRANSFORMATION_H_12094565