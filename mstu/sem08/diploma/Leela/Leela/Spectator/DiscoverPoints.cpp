#include "DiscoverPoints.h"


#define AREA_TOLERANCE	4

#include <algorithm>
#include <functional>
#include <time.h>
#include <stdio.h>
#include <string.h>


size_t DiscoverPoints::operator()( Vertex2D * const markers, const size_t pointsExpected, void * const frame, const size_t width, const size_t height )
{
	printf( "Grid steps:\t%u\t%u\n", __gridStepHorz, __gridStepVert );

	long minWidth = -1, minHeight = -1;

	MarkerFinder * pMarkerFinder = NULL;
	switch ( 1 )
	{
	case 1:	pMarkerFinder = new MarkerFinder_RGBA_8_8_8_8( static_cast< uint32_t * >( frame ), width, height, __minBrightness );	break;
	case 2: pMarkerFinder = new MarkerFinder_Grey_16( static_cast< uint16_t * >( frame ), width, height, __minBrightness );		break;
	}

	__PointsList found;
	found.reserve( pointsExpected );
	
//	const size_t size = width * height;
//	uint32_t * const temp = new uint32_t[ size ];
//	memcpy( temp, frame, size * sizeof( uint32_t ) );

//	clock_t elapsed = 0;
//	for ( volatile size_t i = 0; i < 1000; i++ )
//	{
//		memcpy( frame, temp, size * sizeof( uint32_t ) );

//		const clock_t start = clock();

		Coordinates coord;
		for ( coord.Y = __gridStepVert >> 1; static_cast< size_t >( coord.Y ) < height; coord.Y += __gridStepVert )
			for ( coord.X = __gridStepHorz >> 1; static_cast< size_t >( coord.X ) < width; coord.X += __gridStepHorz )
				if ( pMarkerFinder->MatchesAt( coord ) && !__InOneOfTheFounded( found, coord ) )
				{
					pMarkerFinder->FillAt( coord );
					if ( pMarkerFinder->Marker.Area() >= AREA_TOLERANCE )
						found.push_back( __PointData( pMarkerFinder->Center, pMarkerFinder->Marker ) );

					const size_t blobWidth = pMarkerFinder->Marker.Width();
					if ( minWidth < 0 || blobWidth < static_cast< size_t >( minWidth ) )
						minWidth = blobWidth;

					const size_t blobHeight = pMarkerFinder->Marker.Height();
					if ( minHeight < 0 || blobHeight < static_cast< size_t >( minHeight ) )
						minHeight = blobHeight;
				}

//		elapsed += clock() - start;
//	}

//	delete temp;

//	printf( "elapsed time: %u\n", elapsed );

	delete pMarkerFinder;

	__UpdateGridSteps( minWidth, minHeight, found.size(), pointsExpected );
	__UpdateMinBrightness();

	__ChooseMarkers( markers, pointsExpected, found );

	return ( found.size() );
}

bool DiscoverPoints::__InOneOfTheFounded( const __PointsList & found, const Coordinates & coord )
{
	class Contains
	{
	private:
		const Coordinates & __coord;

	public:
		Contains( const Coordinates & coord )
			: __coord( coord ) {}

	public:
		bool operator()( __PointData markerData )
		{
			return ( markerData.second.IsInside( __coord ) );
		}
	} contains( coord );

	return ( find_if( found.begin(), found.end(), contains ) != found.end() );
}

bool DiscoverPoints::__ChooseMarkers( Vertex2D * const markers, const size_t expectedCount, const __PointsList & found )
{
	printf( "Markers found: %u\n", found.size() );

	const bool ok = ( found.size() >= expectedCount );
	if ( ok )
	{
		for ( register size_t i = 0; i < expectedCount; i++ )
			markers[ i ] = found[ i ].first;
	}

	return ( ok );
}

void DiscoverPoints::__UpdateGridSteps( const long minWidth, const long minHeight, const size_t markersFound, const size_t pointsExpected )
{
	if ( markersFound < pointsExpected )
	{
		__gridStepHorz = 1, __gridStepVert = 1;
	}
	else
	{
		if ( minWidth >= 0 )
		{
			__gridStepHorz = static_cast< size_t >( minWidth ) >> 1;
			if ( __gridStepHorz == 0 )
				__gridStepHorz = 1;
		}

		if ( minHeight >= 0 )
		{
			__gridStepVert = static_cast< size_t >( minHeight ) >> 1;
			if ( __gridStepVert == 0 )
				__gridStepVert = 1;
		}
	}
}

void DiscoverPoints::__UpdateMinBrightness()
{
}
