#include "Vertex.h"

#include <math.h>


Vertex2D Vertex2D::operator + ( const Vertex2D & v ) const
{
	return Vertex2D( X + v.X, Y + v.Y );
}

Vertex2D Vertex2D::operator - ( const Vertex2D & v ) const
{
	return Vertex2D( X - v.X, Y - v.Y );
}

Vertex2D & Vertex2D::operator += ( const Vertex2D & v )
{
	X += v.X, Y += v.Y;
	return ( *this );
}

Vertex2D & Vertex2D::operator -= ( const Vertex2D & v )
{
	X -= v.X, Y -= v.Y;
	return ( *this );
}

double Vertex2D::Length() const
{
	return sqrt( Length2() );
}

double Vertex2D::Length2() const
{
	return ( X * X + Y * Y );
}



const Vertex3D Vertex3D::Default = Vertex3D( 0.0, 0.0, 0.0 );


Vertex3D Vertex3D::operator + ( const Vertex3D & v ) const
{
	return Vertex3D( X + v.X, Y + v.Y, Z + v.Z );
}

Vertex3D Vertex3D::operator - ( const Vertex3D & v ) const
{
	return Vertex3D( X - v.X, Y - v.Y, Z - v.Z );
}

Vertex3D & Vertex3D::operator += ( const Vertex3D & v )
{
	X += v.X, Y += v.Y, Z += v.Z;
	return ( *this );
}

Vertex3D & Vertex3D::operator -= ( const Vertex3D & v )
{
	X -= v.X, Y -= v.Y, Z -= v.Z;
	return ( *this );
}

double Vertex3D::Length() const
{
	return sqrt( Length2() );
}

double Vertex3D::Length2() const
{
	return ( Vertex2D::Length2() + Z * Z );
}
