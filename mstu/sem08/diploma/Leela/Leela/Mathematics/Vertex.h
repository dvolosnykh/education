#pragma once


class Vertex2D
{
public:
	double X, Y;

public:
	Vertex2D() {}
	Vertex2D( const double x, const double y ) { Set( x, y ); }

public:
	Vertex2D operator + ( const Vertex2D & v ) const;
	Vertex2D operator - ( const Vertex2D & v ) const;

	Vertex2D & operator += ( const Vertex2D & v );
	Vertex2D & operator -= ( const Vertex2D & v );

	void Set( const double x, const double y ) { X = x, Y = y; }

	double Length() const;
	double Length2() const;
};


class Vertex3D : public Vertex2D
{
public:
	static const Vertex3D Default;

public:
	double Z;
	
public:
	Vertex3D() : Vertex2D() {}
	Vertex3D( double x, double y, double z ) { Set( x, y, z ); }

public:
	Vertex3D operator + ( const Vertex3D & v ) const;
	Vertex3D operator - ( const Vertex3D & v ) const;

	Vertex3D & operator += ( const Vertex3D & v );
	Vertex3D & operator -= ( const Vertex3D & v );

	void Set( const double x, const double y, const double z ) { Vertex2D::Set( x, y ); Z = z; }

	double Length() const;
	double Length2() const;
};

