#pragma once


struct FrameInfo
{
	size_t Width, Height;
	size_t ChannelsCount, ColorsCount;
	size_t BitsPerChannel;
};