#include "Mutex.h"
#include "Exception.h"


namespace Intercommunication
{
	class MultiBuffer
	{
	private:
		size_t __count;				// number of buffers.
		size_t __size;				// size of a particular buffer.
		unsigned char ** __pMemory;	// buffers.
		mutable Mutex ** __pMutex;	// lock for each one of the buffers.

	public:
		MultiBuffer()
			: __count( 0 )
			, __size( 0 )
			, __pMemory( NULL )
			, __pMutex( NULL ) {}
		MultiBuffer( const size_t count )
			: __count( 0 )
			, __size( 0 )
			, __pMemory( NULL )
			, __pMutex( NULL ) { SetCount( count ); }
		MultiBuffer( const size_t count, const size_t size )
			: __count( 0 )
			, __size( 0 )
			, __pMemory( NULL )
			, __pMutex( NULL ) { Set( count, size ); }
		virtual ~MultiBuffer() { Reset(); }

	public:
		void * Lock( const size_t index ) const;
		void Unlock( const size_t index ) const;

		void SwapPair( const size_t index1, const size_t index2 );
		void SwapCycledLeft();
		void SwapCycledRight();

		void Set( const size_t count, const size_t size );
		void SetCount( const size_t count );
		void SetSize( const size_t size );
		size_t GetSize() const { return ( __size ); }

		void Reset() { Set( 0, 0 ); }

	private:
		void __WaitAll() const;
		void __ReleaseAll() const;

		void __ChangeCount( const size_t count );
		void __SetCount( const size_t count );
		void __ResetCount();

		void __ChangeSize( const size_t size );
		void __SetSize( const size_t size );
		void __ResetSize();

	public:
		class Exception : public Intercommunication::Exception
		{
		public:
			Exception( const std::string text )
				: Intercommunication::Exception( text ) {}
		};

		class AllocateException : public Exception
		{
		public:
			AllocateException( const std::string funcName )
				: Exception( _BuildMessage( "Failed allocating multi buffer.", funcName ) ) {}
		};

		class ZeroSizeException : public Exception
		{
		public:
			ZeroSizeException()
				: Exception( "Allocation of zero-sized multibuffer is prohibitted." ) {}
		};

		class ZeroCountException : public Exception
		{
		public:
			ZeroCountException()
				: Exception( "Allocation of zero-count multibuffer is prohibitted." ) {}
		};
	};
}
