#pragma once

#if defined( __linux__ )
#	include <pthread.h>
#elif defined( _WIN32 )
#	include <windows.h>
#endif

#include "SystemResource.h"
#include "Exception.h"


namespace Intercommunication
{
	class Event : public SystemResource
	{
	private:
#if defined( __linux__ )
		pthread__mutex_t __mutex;
		pthread_cond_t __condition;
		bool __raised;
#elif defined( _WIN32 )
		HANDLE __hEvent;
#endif

	public:
		Event()
			: SystemResource()
#if defined( __linux__ )
			, __raised( false )
#elif defined( _WIN32 )
			, __hEvent( NULL )
#endif
		{}
		Event( const std::string name )
			: SystemResource( name )
#if defined( __linux__ )
			, __raised( false )
#elif defined( _WIN32 )
			, __hEvent( NULL )
#endif
		{}
		Event( const Event & orig )
			: SystemResource( orig )
#if defined( __linux__ )
			, __mutex( orig.__mutex )
			, __condition( orig.__condition )
			, __raised( orig.__raised )
#elif defined( _WIN32 )
			, __hEvent( orig.__hEvent )
#endif
		{}
		virtual ~Event() {}

		virtual void Create();
		virtual void Open();
		virtual void Close();

		void Set();
		void Reset();
		void Wait() const;
		bool TimedWait( const time_t milliseconds ) const;
		bool TryWait() const;

	public:
		class Exception : public Intercommunication::Exception
		{
		public:
			Exception( const std::string text )
				: Intercommunication::Exception( text ) {}
		};

		class CreateException : public Exception
		{
		public:
			CreateException( const std::string funcName )
				: Exception( _BuildMessage( "Failed creating event.", funcName ) ) {}
		};

		class OpenException : public Exception
		{
		public:
			OpenException( const std::string funcName )
				: Exception( _BuildMessage( "Failed opening event.", funcName ) ) {}
		};

		class CloseException : public Exception
		{
		public:
			CloseException( const std::string funcName )
				: Exception( _BuildMessage( "Failed closing event.", funcName ) ) {}
		};

		class SetException : public Exception
		{
		public:
			SetException( const std::string funcName )
				: Exception( _BuildMessage( "Failed setting event.", funcName ) ) {}
		};

		class ResetException : public Exception
		{
		public:
			ResetException( const std::string funcName )
				: Exception( _BuildMessage( "Failed reseting event.", funcName ) ) {}
		};

		class WaitException : public Exception
		{
		public:
			WaitException( const std::string funcName )
				: Exception( _BuildMessage( "Failed waiting for event.", funcName ) ) {}
		};
	};
}
