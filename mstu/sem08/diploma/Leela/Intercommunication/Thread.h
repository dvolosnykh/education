#pragma once

#if defined( __linux__ )
#	include <pthread.h>
#elif defined( _WIN32 )
#	include <windows.h>
#endif


#include "Exception.h"


namespace Intercommunication
{
#if defined( __linux__ )
#	define THREAD_OBJECT		pthread_t
#	define API
#	define EXIT_VALUE_TYPE		void *
#	define EXIT_VALUE_DEFAULT	NULL
#elif defined( _WIN32 )
#	define THREAD_OBJECT		HANDLE
#	define API					WINAPI
#	define EXIT_VALUE_TYPE		DWORD
#	define EXIT_VALUE_DEFAULT	EXIT_SUCCESS
#endif


	class Thread
	{
	public:
		typedef void ( * Procedure )( void * const pArgument );

	private:
		THREAD_OBJECT __thread;
		Procedure __procedure;
		void * __pArgument;

	public:
		Thread()
			: __procedure( NULL )
			, __pArgument( NULL ) {}
		Thread( Procedure procedure, void * pArgument )
			: __procedure( procedure )
			, __pArgument( pArgument ) {}
		Thread( Thread & orig )
			: __thread( orig.__thread )
			, __procedure( orig.__procedure )
			, __pArgument( orig.__pArgument ) {}
		virtual ~Thread() {}

	public:
		void Create();
		void Join();

	private:
		static EXIT_VALUE_TYPE API __Entry( void * pArg );

	public:
		class Exception : public Intercommunication::Exception
		{
		public:
			Exception( const std::string text )
				: Intercommunication::Exception( text ) {}
		};

		class CreateException : public Exception
		{
		public:
			CreateException( const std::string funcName )
				: Exception( _BuildMessage( "Failed creating thread.", funcName ) ) {}
		};

		class JoinException : public Exception
		{
		public:
			JoinException( const std::string funcName )
				: Exception( _BuildMessage( "Failed joining thread.", funcName ) ) {}
		};
	};
}
