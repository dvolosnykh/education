#include "MultiBuffer.h"


namespace Intercommunication
{
	void * MultiBuffer::Lock( const size_t index ) const
	{
		__pMutex[ index ]->Wait();
		return ( __pMemory[ index ] );
	}

	void MultiBuffer::Unlock( const size_t index ) const
	{
		__pMutex[ index ]->Release();
	}

	void MultiBuffer::SwapPair( const size_t index1, const size_t index2 )
	{
		__pMutex[ index1 ]->Wait();
		__pMutex[ index2 ]->Wait();
		{
			unsigned char * temp = __pMemory[ index1 ];
			__pMemory[ index1 ] = __pMemory[ index2 ];
			__pMemory[ index2 ] = temp;
		}
		__pMutex[ index2 ]->Release();
		__pMutex[ index1 ]->Release();
	}

	void MultiBuffer::SwapCycledLeft()
	{
		__WaitAll();
		{
			unsigned char * temp = __pMemory[ 0 ];
			for ( register size_t i = 0; i < __count - 1; i++ )
				__pMemory[ i ] = __pMemory[ i + 1 ];
			__pMemory[ __count - 1 ] = temp;
		}
		__ReleaseAll();
	}

	void MultiBuffer::SwapCycledRight()
	{
		__WaitAll();
		{
			unsigned char * temp = __pMemory[ __count - 1 ];
			for ( register size_t i = __count; i > 1; i-- )
				__pMemory[ i - 1 ] = __pMemory[ i - 2 ];
			__pMemory[ 0 ] = temp;
		}
		__ReleaseAll();
	}

	// assumes that noone even tries to lock on any buffer.
	void MultiBuffer::Set( const size_t count, const size_t size )	// TODO: modern in order to allocate extra and free unneeded pMemory'ies instead of reallocating all the stuff.
	{
		if ( __size > 0 )	// then __count > 0 also.
			__ResetSize();

		if ( count != __count )
			__ChangeCount( count );

		if ( __count > 0 )
		{
			if ( size > 0 )
				__SetSize( size );
		}
	}

	// assumes than noone even tries to lock on any buffer.
	void MultiBuffer::SetCount( const size_t count )
	{
		if ( count == 0 )
			throw ZeroCountException();

		Set( count, __size );
	}

	void MultiBuffer::SetSize( const size_t size )
	{
		if ( size == 0 )
			throw ZeroSizeException();

		if ( size != __size )
		{
			__WaitAll();
			{
				__ChangeSize( size );
			}
			__ReleaseAll();
		}
	}

	void MultiBuffer::__WaitAll() const
	{
		for ( register size_t i = 0; i < __count; i++ )
			__pMutex[ i ]->Wait();
	}

	void MultiBuffer::__ReleaseAll() const
	{
		for ( register size_t i = __count; i > 0; i-- )
			__pMutex[ i - 1 ]->Release();
	}

	// assumes that size was reset to zero, and count changes indeed.
	void MultiBuffer::__ChangeCount( const size_t count )
	{
		if ( __count > 0 )
			__ResetCount();

		if ( count > 0 )
			__SetCount( count );
	}

	// assumes that size was reset to zero, and count was reset to zero.
	void MultiBuffer::__SetCount( const size_t count )
	{
		if ( count == 0 )
			throw ZeroCountException();

		__pMemory = new unsigned char *[ count ];
		if ( __pMemory == NULL )
			throw AllocateException( "new unsigned char *" );

		__pMutex = new Mutex *[ count ];
		if ( __pMutex == NULL )
			throw AllocateException( "new Mutex *" );

		for ( register size_t i = 0; i < count; i++ )
		{
			__pMemory[ i ] = NULL;

			__pMutex[ i ] = new Mutex;
			if ( __pMutex[ i ] == NULL )
				throw AllocateException( "new Mutex" );
			__pMutex[ i ]->Create();
		}

		__count = count;
	}

	// assumes that size was reset to zero, and count was set to non-zero.
	void MultiBuffer::__ResetCount()
	{
		delete __pMemory;
		__pMemory = NULL;

		for ( register size_t i = 0; i < __count; i++ )
		{
			__pMutex[ i ]->Close();
			delete __pMutex[ i ];
		}
		delete __pMutex;
		__pMutex = NULL;

		__count = 0;
	}

	// assumes that count was set to non-zero, and size changes indeed.
	void MultiBuffer::__ChangeSize( const size_t size )
	{
		if ( __size > 0 )
			__ResetSize();

		if ( size > 0 )
			__SetSize( size );
	}

	// assumes that count was set to non-zero, and size was reset to zero.
	void MultiBuffer::__SetSize( const size_t size )
	{
		if ( size == 0 )
			throw ZeroSizeException();

		for ( register size_t i = 0; i < __count; i++ )
		{
			__pMemory[ i ] = new unsigned char[ size ];
			if ( __pMemory[ i ] == NULL )
				throw AllocateException( "new unsigned char" );
		}

		__size = size;
	}

	// assumes that count was set to non-zero, and size was set to non-zero.
	void MultiBuffer::__ResetSize()
	{
		for ( register size_t i = 0; i < __count; i++ )
		{
			delete __pMemory[ i ];
			__pMemory[ i ] = NULL;
		}

		__size = 0;
	}
}
