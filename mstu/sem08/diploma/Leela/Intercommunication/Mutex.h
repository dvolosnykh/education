#pragma once

#if defined( __linux__ )
#	include <semaphore.h>
#elif defined( _WIN32 )
#	include <windows.h>
#endif

#include "SystemResource.h"
#include "Exception.h"


namespace Intercommunication
{
#if defined( __linux__ )
#	define MUTEX_OBJECT	sem_t *
#elif defined( _WIN32 )
#	define MUTEX_OBJECT	HANDLE
#endif

	class Mutex : SystemResource
	{
	private:
		MUTEX_OBJECT __mutex;

	public:
		Mutex()
			: SystemResource()
			, __mutex( NULL ) {}
		Mutex( const std::string name )
			: SystemResource( name )
			, __mutex( NULL ) {}
		Mutex( const Mutex & orig )
			: SystemResource( orig )
			, __mutex( orig.__mutex ) {}
		virtual ~Mutex() {}

		void Create();
		void Open();
		void Close();

		void Wait();
		void Release();

	public:
		class Exception : public Intercommunication::Exception
		{
		public:
			Exception( const std::string text )
				: Intercommunication::Exception( text ) {}
		};

		class CreateException : public Exception
		{
		public:
			CreateException( const std::string funcName )
				: Exception( _BuildMessage( "Failed creating mutex.", funcName ) ) {}
		};

		class OpenException : public Exception
		{
		public:
			OpenException( const std::string funcName )
				: Exception( _BuildMessage( "Failed opening mutex.", funcName ) ) {}
		};

		class CloseException : public Exception
		{
		public:
			CloseException( const std::string funcName )
				: Exception( _BuildMessage( "Failed closing mutex.", funcName ) ) {}
		};

		class WaitException : public Exception
		{
		public:
			WaitException( const std::string funcName )
				: Exception( _BuildMessage( "Failed waiting for mutex.", funcName ) ) {}
		};

		class ReleaseException : public Exception
		{
		public:
			ReleaseException( const std::string funcName )
				: Exception( _BuildMessage( "Failed releasing mutex.", funcName ) ) {}
		};
	};
}
