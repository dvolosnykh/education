#pragma once

#include "Thread.h"
#include "Event.h"


namespace Intercommunication
{
	class Job
	{
	protected:
		Job() {}
		virtual ~Job() {}

	protected:
		void _Execute();
		virtual void _Main() = 0;
		virtual bool _Initialize() { return ( true ); }
		virtual void _Deinitialize() {}
	};

	class SyncJob : public Job
	{
	protected:
		SyncJob() : Job() {}
		virtual ~SyncJob() {}

	public:
		void Execute() { _Execute(); }
	};

	class AsyncJob : public Job
	{
	private:
		Thread __thread;

	protected:
		AsyncJob() : Job() { __thread = Thread( __Procedure, this ); }
		virtual ~AsyncJob() {}

	public:
		void ExecuteSynchronous() { _Execute(); }
		void ExecuteAsynchronous() { __thread.Create(); }
		void WaitForTermination() { __thread.Join(); }

	private:
		static void __Procedure( void * const pArgument ) { static_cast< AsyncJob * const >( pArgument )->_Execute(); }
	};

	class TerminableAsyncJob : public AsyncJob
	{
	private:
		Event __terminationRequested;

	protected:
		TerminableAsyncJob() : AsyncJob() { __terminationRequested.Create(); }
		virtual ~TerminableAsyncJob() { __terminationRequested.Close(); }

	public:
		void RequestTermination() { __terminationRequested.Set(); }

	protected:
		bool _TerminationRequested() { return __terminationRequested.TryWait(); }
	};
}
