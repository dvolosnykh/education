#include "Jobs.h"


namespace Intercommunication
{
	void Job::_Execute()
	{
		if ( _Initialize() )
		{
			_Main();
			_Deinitialize();
		}
	}
}
