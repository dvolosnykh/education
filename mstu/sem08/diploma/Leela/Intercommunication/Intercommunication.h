#pragma once

#include "MultiBuffer.h"
#include "Event.h"
#include "Exception.h"
#include "Jobs.h"
#include "Lockable.h"
#include "Mutex.h"
#include "SharedMemory.h"
#include "Thread.h"

using namespace Intercommunication;	// TODO: rewrite all to be inter-thread communication.