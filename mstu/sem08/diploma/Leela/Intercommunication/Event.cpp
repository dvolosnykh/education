#include "Event.h"


namespace Intercommunication
{
#if defined( __linux__ )

	Event::Create()
	{
		int result = pthread__mutex_init( &__mutex, NULL );
		if ( result != 0 )
			throw CreateException( "pthread__mutex_init" );

		result = pthread_cond_init( &__condition, NULL );
		if ( result != 0 )
			throw CreateException( "pthread_cond_init" );

		__raised = false;
	}

	Event::Open()
	{
		if ( Name.empty() )
			throw Exception( "Unnamed objects can't be opened" );

		throw Exception( "Event::Open (linux) not implemented." );
	}

	Event::Close()
	{
		int result = pthread__mutex_destroy( &__mutex );
		if ( result != 0 )
			throw CloseException( "pthread__mutex_destroy" );

		result = pthread_cond_destroy( &__condition );
		if ( result != 0 )
			throw CloseException( "pthread_cond_destroy" );
	}

	void Event::Set()
	{
		int result = pthread__mutex_lock( &__mutex );
		if ( result != 0 )
			throw SetException( "pthread__mutex_lock" );

		__raised = true;

		result = pthread_cond_broadcast( &__condition );
		if ( result != 0 )
			throw SetException( "pthread_cond_broadcast" );
		
		result = pthread__mutex_unlock( &__mutex );
		if ( result != 0 )
			throw SetException( "pthread__mutex_unlock" );
	}

	void Event::Reset()
	{
		int result = pthread__mutex_lock( &__mutex );
		if ( result != 0 )
			throw ResetException( "pthread__mutex_lock" );

		__raised = false;
		
		result = pthread__mutex_unlock( &__mutex );
		if ( result != 0 )
			throw ResetException( "pthread__mutex_unlock" );
	}

	void Event::Wait() const
	{
		TimedWait( -1 );	// is "-1"  stands for infinite waiting?
	}

	bool Event::TimedWait() const
	{
		int result = pthread__mutex_lock( &__mutex );
		if ( result != 0 )
			throw WaitException( "pthread__mutex_lock" );

		bool raised = __raised;
		if( !__raised )
		{
			result = pthread_cond_wait( &__condition, &__mutex );	// timed wait ?
			if ( result != 0 )
				throw WaitException( "pthread_cond_wait" );
		}

		result = pthread__mutex_unlock( &__mutex );
		if ( result != 0 )
			throw WaitException( "pthread__mutex_unlock" );
	}

#elif defined( _WIN32 )

	void Event::Create()
	{
		if ( __hEvent == NULL )
		{
			__hEvent = CreateEvent( NULL, TRUE, FALSE, !Name.empty() ? Name.c_str() : NULL );
			if ( __hEvent == NULL )
				throw CreateException( "CreateEvent" );
		}
	}

	void Event::Open()
	{
		if ( __hEvent == NULL )
		{
			if ( Name.empty() )
				throw Exception( "Unnamed objects can't be opened" );

			__hEvent = OpenEvent( EVENT_ALL_ACCESS, FALSE, Name.c_str() );
			if ( __hEvent == NULL )
				throw OpenException( "OpenEvent" );
		}
	}

	void Event::Close()
	{
		if ( __hEvent != NULL )
		{
			const BOOL result = CloseHandle( __hEvent );
			if ( !result )
				throw CloseException( "CloseHandle" );

			__hEvent = NULL;
		}
	}

	void Event::Set()
	{
		if ( __hEvent == NULL )
			throw Exception( "Event was not created." );

		const BOOL result = SetEvent( __hEvent );
		if ( !result )
			throw SetException( "SetEvent" );
	}

	void Event::Reset()
	{
		if ( __hEvent == NULL )
			throw Exception( "Event was not created." );

		const BOOL result = ResetEvent( __hEvent );
		if ( !result )
			throw ResetException( "ResetEvent" );
	}

	void Event::Wait() const
	{
		TimedWait( INFINITE );
	}

	bool Event::TimedWait( const time_t milliseconds ) const
	{
		if ( __hEvent == NULL )
			throw Exception( "Event was not created." );

		const DWORD result = WaitForSingleObject( __hEvent, static_cast< DWORD >( milliseconds ) );
		if ( result == WAIT_FAILED )
			throw WaitException( "WaitForSingleObject" );

		return ( result == WAIT_OBJECT_0 );
	}

#endif

	bool Event::TryWait() const
	{
		return TimedWait( 0 );
	}
}
