#pragma once

#if defined( __linux__ )
#elif defined( _WIN32 )
#	include <windows.h>
#endif

#include "Lockable.h"
#include "Exception.h"


namespace Intercommunication
{
#if defined( __linux__ )
#	define FILE_OBJECT	int
#	define FILE_OBJECT_INVALID	-1
#elif defined( _WIN32 )
#	define FILE_OBJECT	HANDLE
#	define FILE_OBJECT_INVALID	NULL
#endif


	class SharedMemory : public SystemResource, public Lockable
	{
	public:
		void * Address;

	protected:
		FILE_OBJECT _file;
		size_t _size;

	public:
		SharedMemory()
			: SystemResource(), Lockable()
			, Address( NULL )
			, _file( FILE_OBJECT_INVALID )
			, _size( 0 ) {}
		SharedMemory( const std::string name )
			: SystemResource( name ), Lockable( name )
			, Address( NULL )
			, _file( FILE_OBJECT_INVALID )
			, _size( 0 ) {}
		SharedMemory( SharedMemory & orig )
			: SystemResource( orig ), Lockable( orig )
			, Address( orig.Address )
			, _file( orig._file )
			, _size( orig._size ) {}
		virtual ~SharedMemory() { Close(); }

		virtual void Create();
		virtual void Open();
		virtual void Close();
		bool IsClosed() const { return ( _file == FILE_OBJECT_INVALID && Address == NULL ); }
		void SetSize( const size_t size );				// if SharedMemory was Open'ed then the behaviour is undefined. (WIN32: size is ignored. you get the handle of existing FileMapping)
		size_t GetSize() const { return ( _size ); }	// if SharedMemory was Open'ed then the return-value is undefined.

	public:
		class Exception : public Intercommunication::Exception
		{
		public:
			Exception( const std::string text )
				: Intercommunication::Exception( text ) {}
		};

		class CreateException : public Exception
		{
		public:
			CreateException( const std::string funcName )
				: Exception( _BuildMessage( "Failed creating shared memory.", funcName ) ) {}
		};

		class OpenException : public Exception
		{
		public:
			OpenException( const std::string funcName )
				: Exception( _BuildMessage( "Failed opening shared memory.", funcName ) ) {}
		};

		class CloseException : public Exception
		{
		public:
			CloseException( const std::string funcName )
				: Exception( _BuildMessage( "Failed closing shared memory.", funcName ) ) {}
		};
	};
}
