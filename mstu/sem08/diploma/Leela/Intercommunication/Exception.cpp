#include "Exception.h"


#if defined( __linux__ )
#	include <cstd::string>
#	include <cerrno>
#elif defined( _WIN32 )
#	include <windows.h>
#endif


namespace Intercommunication
{
	std::string Exception::_BuildMessage( const std::string message, const std::string funcName )
	{
		 return ( message + " " + funcName + ": " + _ErrorString() );
	}

	std::string Exception::_ErrorString()
	{
		std::string errorString;

#if defined( __linux__ )

		errorString = std::string( strerror( errno ) );

#elif defined( _WIN32 )

		/*	char * text;

		FormatMessage(
			FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
			NULL,
			GetLastError(),
			GetUserDefaultLangID(),
			( LPSTR )&text,
			0,
			NULL );

		errorString = std::string( text );
		*/

		char text[ MAX_PATH ];
		_itoa_s( GetLastError(), text, sizeof( text ), 10 );
		errorString = std::string( text );

#endif

		return ( errorString );
	}
}
