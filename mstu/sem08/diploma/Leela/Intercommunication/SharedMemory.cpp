#if defined( __linux__ )
#	include <fcntl.h>
#	include <unistd.h>
#	include <sys/mman.h>
#elif defined( _WIN32 )
#endif

#include "SharedMemory.h"


namespace Intercommunication
{
	void SharedMemory::Create()
	{
		Lockable::Create();

		if ( _file == FILE_OBJECT_INVALID )
		{
#if defined( __linux__ )

			_file = shm_open( Name.c_str(), O_CREAT | O_RDWR, S_IREAD | S_IWRITE );
			if ( _file == FILE_OBJECT_INVALID )
				throw CreateException( "shm_open" );

			int result = ftruncate( _file, _size );
			if ( result != 0 )
				throw CreateException( "ftruncate" );

#elif defined( _WIN32 )

			_file = CreateFileMapping(
				INVALID_HANDLE_VALUE,	// use paging file
				NULL,					// default security
				PAGE_READWRITE,			// read/write access
				0, _size,				// buffer size
				Name.c_str() );			// name of mapping object
			if ( _file == FILE_OBJECT_INVALID )
				throw CreateException( "CreateFileMapping" );

#endif
		}

		if ( Address == NULL )
		{
#if defined( __linux__ )

			Address = mmap( NULL, _size,  PROT_WRITE, MAP_SHARED, _file, 0 );
			if ( Address == MAP_FAILED )
			{
				Address = NULL;
				throw CreateException( "mmap" );
			}

#elif defined( _WIN32 )

			Address = MapViewOfFile(
				_file,				// handle to map object
				FILE_MAP_WRITE,		// write access
				0, 0,
				_size );
			if ( Address == NULL )
				throw CreateException( "MapViewOfFile" );

#endif
		}
	}

	void SharedMemory::Open()
	{
		Lockable::Open();

		if ( _file == FILE_OBJECT_INVALID )
		{
#if defined( __linux__ )

			_file = shm_open( Name.c_str(), O_RDWR, S_IREAD | S_IWRITE );
			if ( _file == FILE_OBJECT_INVALID )
				throw OpenException( "shm_open" );
			// _size = lseek( _file, SEEK_END, 0 );

#elif defined( _WIN32 )

			_file = OpenFileMapping(
				FILE_MAP_READ,		// read access
				FALSE,				// do not inherit handle
				Name.c_str() );		// name of mapping object
			if ( _file == FILE_OBJECT_INVALID )
				throw OpenException( "OpenFileMapping" );
			// _size = GetFileSize( _file, NULL );	TODO: implement shared memory basing on temporary file, not paging file.

#endif
		}

		if ( Address == NULL )
		{
#if defined( __linux__ )

			Address = mmap( NULL, _size,  PROT_READ, MAP_SHARED, _file, 0 );
			if ( Address == MAP_FAILED )
			{
				Address = NULL;
				throw OpenException( "mmap" );
			}

#elif defined( _WIN32 )

			Address = MapViewOfFile(
				_file,				// handle to map object
				FILE_MAP_READ,		// read access
				0, 0,				// till the end. TODO: Should specify exact _size?
				0 );
			if ( Address == NULL )
				throw OpenException( "MapViewOfFile" );

#endif
		}
	}

	void SharedMemory::Close()
	{
		if ( Address != NULL )
		{
#if defined( __linux__ )

			const int result = munmap( Address, _size );
			if ( result != 0 )
				throw CloseException( "munmap" );

#elif defined( _WIN32 )

			const BOOL result = UnmapViewOfFile( Address );
			if ( !result )
				throw CloseException( "UnmapViewOfFile" );
#endif

			Address = NULL;
		}

		if ( _file != FILE_OBJECT_INVALID )
		{
#if defined( __linux__ )

			int result = close( _file );
			if ( result != 0 )
				throw CloseException( "close" );

			result = shm_unlink( Name.c_str() );
			if ( result != 0 )
				throw CloseException( "shm_unlink" );

#elif defined( _WIN32 )

			const BOOL result = CloseHandle( _file );
			if ( !result )
				throw CloseException( "CloseHandle" );

#endif

			_file = FILE_OBJECT_INVALID;
		}

		Lockable::Close();
	}

	void SharedMemory::SetSize( const size_t size )
	{
		if ( !IsClosed() )
			Close();

		_size = size;
		Create();
	}
}
