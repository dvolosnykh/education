#pragma once

#include <string>


namespace Intercommunication
{
	class SystemResource
	{
	public:
		std::string Name;

	protected:
		SystemResource()
			: Name( "" ) {}
		SystemResource( const std::string name )
			: Name( name ) {}
		SystemResource( const SystemResource & orig ) { Name = orig.Name; }
		virtual ~SystemResource() {}

	public:
		virtual void Create() = 0;
		virtual void Open() = 0;
		virtual void Close() = 0;
	};
}
