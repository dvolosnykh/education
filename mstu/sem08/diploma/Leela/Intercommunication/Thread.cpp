#include "Thread.h"


namespace Intercommunication
{
	void Thread::Create()
	{
#if defined( __linux__ )

		const int result = pthread_create( &__thread, NULL, __Entry, this );
		if ( result != 0 )
			throw CreateException( "pthread_create" );

#elif defined( _WIN32 )

		__thread = CreateThread( NULL, 0, __Entry, this, 0, NULL );
		if ( __thread == NULL )
			throw CreateException( "CreateThread" );

#endif
	}

	void Thread::Join()
	{
#if defined( __linux__ )

		const int result = pthread_join( __thread, NULL );
		if ( result != 0 )
			throw JoinException( "pthread_join" );

#elif defined( _WIN32 )

		DWORD result = WaitForSingleObject( __thread, INFINITE );
		if ( result == WAIT_FAILED )
			throw JoinException( "WaitForSingleObject" );

		{
			BOOL result = CloseHandle( __thread );
			if ( !result )
				throw JoinException( "CloseHandle" );
		}

#endif
	}

	EXIT_VALUE_TYPE API Thread::__Entry( void * pArg )
	{
		Thread * const pThread = static_cast< Thread * >( pArg );
		pThread->__procedure( pThread->__pArgument );

		return ( EXIT_VALUE_DEFAULT );
	}
}
