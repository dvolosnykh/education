#if defined( __linux__ )
#	include <fcntl.h>
#	include <unistd.h>
#elif defined( _WIN32 )
#endif

#include "Mutex.h"


namespace Intercommunication
{
	void Mutex::Create()
	{
		if ( __mutex == NULL )
		{
#if defined( __linux__ )

			__mutex = sem_open( Name.c_str(), O_CREAT, S_IREAD | S_IWRITE, 1 );
			if ( __mutex == SEM_FAILED )
			{
				__mutex = NULL;
				throw CreateException( "sem_open" ) );
			}

#elif defined( _WIN32 )

			__mutex = CreateMutex( NULL, FALSE, Name.c_str() );
			if ( __mutex == NULL )
				throw CreateException( "CreateMutex" );

#endif
		}
	}

	void Mutex::Open()
	{
		if ( __mutex == NULL )
		{
			if ( Name.empty() )
				throw Exception( "Unnamed objects can't be opened" );
#if defined( __linux__ )

			__mutex = sem_open( Name.c_str(), NULL, S_IREAD | S_IWRITE, 1 );
			if ( __mutex == SEM_FAILED )
			{
				__mutex = NULL;
				throw OpenException( "sem_open" );
			}

#elif defined( _WIN32 )

			__mutex = OpenMutex( MUTEX_ALL_ACCESS, FALSE, Name.c_str() );
			if ( __mutex == NULL )
				throw OpenException( "OpenMutex" );

#endif
		}
	}

	void Mutex::Close()
	{
		if ( __mutex != NULL )
		{
#if defined( __linux__ )

			int result = sem_close( __mutex );
			if ( result != 0 )
				throw CloseException( "sem_close" );

			result = sem_unlink( Name.c_str() );
			if ( result != 0 )
				throw CloseException( "sem_unlink" );

#elif defined( _WIN32 )

			const BOOL result = CloseHandle( __mutex );
			if ( !result  )
				throw CloseException( "CloseHandle" );

#endif
			__mutex = NULL;
		}
	}

	void Mutex::Wait()
	{
		if ( __mutex == NULL )
			throw Exception( "Mutex was not created." );

#if defined( __linux__ )

		const int result = sem_wait( __mutex );
		if ( result != 0 )
			throw WaitException( "sem_wait" );

#elif defined( _WIN32 )

		const DWORD result = WaitForSingleObject( __mutex, INFINITE );
		if ( result != WAIT_OBJECT_0 )
			throw WaitException( "WaitForSingleObject" );

#endif
	}

	void Mutex::Release()
	{
		if ( __mutex == NULL )
			throw Exception( "Mutex was not created." );

#if defined( __linux__ )

		const int result = sem_post( __mutex );
		if ( result != 0 )
			throw ReleaseException( "sem_post" );

#elif defined( _WIN32 )

		const BOOL result = ReleaseMutex( __mutex );
		if ( !result )
			throw ReleaseException( "ReleaseMutex" );

#endif
	}
}
