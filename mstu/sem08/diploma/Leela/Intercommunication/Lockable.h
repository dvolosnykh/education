#pragma once

#include "Mutex.h"


namespace Intercommunication
{
#define LOCK_SUFFIX		std::string( "_Lock" )


	class Lockable
	{
	private:
		Mutex __mutex;

	protected:
		Lockable() { Create(); }
		Lockable( const std::string & resourceName )	// FIX: needed for shared memory which isn't needed.
			: __mutex( Mutex( __LockName( resourceName ) ) ) { Create(); }
		Lockable( const Lockable & orig )
			: __mutex( orig.__mutex ) {}
		virtual ~Lockable() { Close(); }

	public:
		void Lock() { __mutex.Wait(); }
		void Unlock() { __mutex.Release(); }

		void Create() { __mutex.Create(); }
		void Open() { __mutex.Open(); }
		void Close() { __mutex.Close(); }

	private:
		static std::string __LockName( const std::string & resourceName ) { return ( !resourceName.empty() ? resourceName + LOCK_SUFFIX : "" ); }
	};
}
