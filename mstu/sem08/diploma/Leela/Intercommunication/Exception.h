#pragma once

#include <exception>
#include <string>


namespace Intercommunication
{
	class Exception : public std::exception
	{
	public:
		const std::string Text;

	public:
		Exception( const std::string text )
			: Text( text ) {}
		virtual ~Exception() throw() {}

	protected:
		static std::string _BuildMessage( const std::string message, const std::string funcName );

	private:
		static std::string _ErrorString();
	};
}
