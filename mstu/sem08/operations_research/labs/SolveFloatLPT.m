function [ f, X ] = SolveFloatLPT( lpt, debugLevel )
	[ f, X ] = Simplex( lpt, debugLevel );
end

function [ f, X ] = Simplex( lpt, debugLevel )
	if ( debugLevel >= 1 )
		disp( sprintf( '===== Enter Simplex =====\n' ) );
	end
	
	lpt = ToPositiveB( lpt, debugLevel );
	N = size( lpt.A, 2 );
	[ lpt, indexNotLTEQ ] = GeneralToCanonical( lpt, debugLevel );
	[ f, Y ] = EvaluateTable( lpt, N, indexNotLTEQ, debugLevel );
	
	if ( ~isempty( Y ) )
		X = Y( 1 : N );
	else
		X = [];
	end
	
	if ( debugLevel >= 1 )
		disp( sprintf( 'f = %s\t\tX = [ %s ]''\n', num2str( f ), num2str( X' ) ) );
		disp( sprintf( '===== Exit Simplex =====\n' ) );
	end
end

function lpt = ToPositiveB( lpt, debugLevel )
	negMask = ( lpt.b < 0 );
	if ( any( negMask ) )
		lpt.A( negMask, : ) = -lpt.A( negMask, : );
		lpt.b( negMask ) = -lpt.b( negMask );
		lpt.type( negMask ) = 4 - lpt.type( negMask );

		if ( debugLevel >= 2 )
			disp( 'Managing all restrictions so that vector b is positive:' );
			PrintLPT( lpt );
		end
	end
end

function [ lpt, indexNotLTEQ ] = GeneralToCanonical( lpt, debugLevel )
	global TYPE
	
	if ( ~lpt.max )
		lpt.c = -lpt.c;
		lpt.max = ~lpt.max;
	end

	indexLTEQ = find( lpt.type == TYPE.LTEQ );
	numLTEQ = numel( indexLTEQ );
	indexEQ = find( lpt.type == TYPE.EQ );
	numEQ = numel( indexEQ );
	indexGTEQ = find( lpt.type == TYPE.GTEQ );
	numGTEQ = numel( indexGTEQ );
	
	lpt.type( : ) = TYPE.EQ;

	addNum = numLTEQ + numEQ + 2 * numGTEQ;
	M = zeros( size( lpt.A, 1 ), addNum );

	if ( numLTEQ > 0 )
		range = ( 1 : numLTEQ );
		M( indexLTEQ, range ) = eye( numLTEQ );
	end

	if ( numGTEQ > 0 )
		range = numLTEQ + ( 1 : numGTEQ );
		M( indexGTEQ, range ) = -eye( numGTEQ );
	end

	indexNotLTEQ = [];
	if ( numEQ + numGTEQ > 0 )
		range = numLTEQ + numGTEQ + ( 1 : numEQ + numGTEQ );
		indexNotLTEQ = [indexEQ; indexGTEQ ];
		M( indexNotLTEQ, range ) = eye( numEQ + numGTEQ );
	end

	lpt.A = [ lpt.A, M ];
	lpt.c = [ lpt.c, zeros( 1, addNum ) ];
	
	if ( debugLevel >= 2 )
		disp( 'Leading to canonical form:' );
		PrintLPT( lpt );
	end
end

function [ f, Y ] = EvaluateTable( lpt, numInitVars, indexNotLTEQ, debugLevel )
	global EPS

	basisIndex = BasisVariables;
	helpVarsIndex = basisIndex( indexNotLTEQ );

	[ phi, phiDiffs ] = DefinePhi;

	f = 0;
	fDiffs = lpt.c;
	func = [ phi; f ];
	diffs = [ phiDiffs; fDiffs ];

	hasSolution = true( 1 );
	basisValue = lpt.b;
	for times = 1 : size( func, 1 )
		while ( hasSolution && any( diffs( 1, : ) > EPS ) )
			leadIn = MaxPosDiffIndex( diffs( 1, : ) );
			rowMin = MinPosRatioIndex( basisValue,  lpt.A( :, leadIn ) );

			hasSolution = ~isempty( rowMin );
			PrintTable( hasSolution );

			if ( hasSolution )
				basisIndex( rowMin ) = leadIn;

				basisValue( rowMin ) = basisValue( rowMin ) / lpt.A( rowMin, leadIn );
				lpt.A( rowMin, : ) = lpt.A( rowMin, : ) / lpt.A( rowMin, leadIn );

				for i = 1 : size( lpt.A, 1 )
					if ( i ~= rowMin )
						basisValue( i ) = basisValue( i ) - lpt.A( i, leadIn ) * basisValue( rowMin );
						lpt.A( i, : ) = lpt.A( i, : ) - lpt.A( i, leadIn ) * lpt.A( rowMin, : );
					end
				end

				for i = 1 : size( func, 1 )
					func( i ) = func( i ) + diffs( i, leadIn ) * basisValue( rowMin );
					diffs( i, : ) = diffs( i, : ) - diffs( i, leadIn ) * lpt.A( rowMin, : );
				end
			end
		end

		if ( hasSolution )
			PrintTable( false( 1 ) );

			if ( times < size( func, 1 )  )
				mask = true( 1, size( diffs, 2 ) );
				mask( helpVarsIndex ) = false( 1 );
				hasSolution = all( abs( [ func( 1 ), diffs( 1, mask ) ] ) < EPS );
				
				if ( hasSolution )
					func( 1 ) = [];
					diffs( 1, : ) = [];

					lpt.A( :, helpVarsIndex ) = [];
					diffs( :, helpVarsIndex ) = [];
				end
			end
		end
	end

	if ( hasSolution )
		f = func( 1 );
		Y = OptimalSolution;
	else
		f = [];
		Y = [];
	end

	function basisIndex = BasisVariables
		M = lpt.A( :, numInitVars + 1 : size( lpt.A, 2 ) );
		[ r, c ] = find( M == 1 );
		c( r ) = c;
		basisIndex = numInitVars + c;
	end

	function [ phi, phiDiffs ] = DefinePhi
		if ( ~isempty( indexNotLTEQ ) )
			phi = -sum( lpt.b( indexNotLTEQ ) );
			phiDiffs = sum( lpt.A( indexNotLTEQ, : ), 1 );
			phiDiffs( helpVarsIndex ) = 0;
		else
			phi = [];
			phiDiffs = [];
		end
	end

	function index =  MaxPosDiffIndex( diffs )
		mask = ( diffs( 1, : ) > EPS );
		
		if ( ~isempty( mask ) )
			[ value, index ] = max( diffs( 1, mask ) );
			index = CorrectIndex( index, mask );
		else
			index = [];
		end
	end

	function index = MinPosRatioIndex( v1, v2 )
		mask = ( v2 > EPS );
		if ( ~isempty( mask ) )
			[ value, index ] = min( v1( mask ) ./ v2( mask ) );
			index = CorrectIndex( index, mask );
		else
			index = [];
		end
	end

	function index = CorrectIndex( index, mask )
		if ( ~isempty( index ) )
			if ( issparse( mask ) )
				maskedIndices = find( mask );
			else
				maskedIndices = find( mask, index );
			end
			index = maskedIndices( index );
		end
	end

	function Y = OptimalSolution
		Y = zeros( size( lpt.A, 2 ) + numel( helpVarsIndex ), 1 );
		Y( basisIndex ) = basisValue;
	end

	function PrintTable( printLeader )
		if ( debugLevel >= 2 )
			str = 'Simplex-table';
			if ( ~printLeader )
				str = [ str, ':' ];
			elseif ( hasSolution )
				str = [ str, sprintf( ' with leader = ( %u, %u ):', rowMin, leadIn ) ];
			else
				str = [ str, ' with no leader:' ];
			end
			disp( str );
			disp( num2str( [ basisValue, lpt.A; -func, diffs ] ) );
			display( basisIndex );
			disp( sprintf( ' ' ) );
			pause;
		end
	end
end