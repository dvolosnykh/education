function lab2( lptst, debugLevel )
	variant4 = true( 1 );

	if ( isempty( lptst ) )
		if ( variant4 )
			lptst.C = [ inf  11   3   9  10; ...
				         9 inf  12   8   2; ...
					     3   8 inf   9  11; ...
					     9   2  10 inf  10; ...
				        10   8  11   4 inf ];
			lptst.max = false( 1 );
		else
			lptst.C = [ inf  11   3; ...
				         9 inf  12; ...
					     3   8 inf ];
			lptst.max = false( 1 );
		end
	end

	[ f, X ] = SolveLPTST( lptst, debugLevel );
end