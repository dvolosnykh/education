function PrintLPDT( C, logic )
	global NONE SIZ PRIME

	WIDTH = 6;

	if ( ~isempty( logic ) )
		str = SelectStr( false( 1 ) );

		for j = 1 : size( C, 2 )
			str = [ str, SelectStr( logic.IsColSelected( j ) ) ];
		end
		disp( str );
	end

	for i = 1 : size( C, 1 )
		str = SelectStr( ~isempty( logic ) && logic.IsRowSelected( i ) );

		for j = 1 : size( C, 2 )
			prevMarked = ( ~isempty( logic ) && j > 1 && logic.X( i, j - 1 ) ~= NONE );
			str = [ str, ValueStr, MarkStr ];
		end

		disp( str );
	end

	disp( sprintf( ' ' ) );
	pause;

	function str = SelectStr( isSelected )
		MARK_SELECTED = '+';

		if ( isSelected )
			selectStr = MARK_SELECTED;
		else
			selectStr = ' ';
		end
		
		str = sprintf( '%*s', WIDTH, selectStr );
	end
	
	function str = ValueStr
		str = sprintf( '%*u', WIDTH - prevMarked, C( i, j ) );
	end
	
	function str = MarkStr
		MARK_SIZ = '*';
		MARK_PRIME = '''';
	
		str = '';
		if ( ~isempty( logic ) && logic.X( i, j ) ~= NONE )
			switch ( logic.X( i, j ) )
				case SIZ
					str = MARK_SIZ;
				case PRIME
					str = MARK_PRIME;
			end
		end
	end
end