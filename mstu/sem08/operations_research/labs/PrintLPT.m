function PrintLPT( lpt )
	str = num2str( [ lpt.c; lpt.A ] );

	disp( sprintf( '\tCriterion:' ) );
	disp( sprintf( '\t\t%s   -> %s\n', str( 1, : ), ExtrStr( lpt.max ) ) );
	str( 1, : ) = [];

	disp( sprintf( '\tRestrictions:' ) );
	bStr = num2str( lpt.b );
	for i = 1 : size( lpt.A, 1 )
		disp( sprintf( '\t\t%s   %s %s', str( i, : ), ...
			CompareStr( lpt.type( i ) ), bStr( i, : ) ) );
	end
	disp( ' ' );
	
	pause;
end

function str = ExtrStr( max )
	if ( max )
		str = 'max';
	else
		str = 'min';
	end
end

function str = CompareStr( type )
	global TYPE
	
	str = '';
	switch ( type )
		case TYPE.LTEQ
			str = '<=';
		case TYPE.EQ
			str = '==';
		case TYPE.GTEQ
			str = '>=';
	end
end