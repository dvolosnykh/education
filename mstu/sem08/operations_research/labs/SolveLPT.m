% LPT - Linear Programmin Task

function [ f, X ] = SolveLPT( lpt, debugLevel )
	EnumTypes;
	Defines;

	Correction;
	
	if ( debugLevel >= 1 )
		disp( sprintf( 'Initial LPT:\n' ) );
		PrintLPT( lpt );
	end

	if ( any( lpt.mustBeInt ) )
		if ( debugLevel >= 2 )
			disp( 'Some or all of the variables are required to be integer.' );
			disp( sprintf( '\tSolving integer LPT.\n' ) );
		end
		
		[ f, X ] = SolveIntegerLPT( lpt, debugLevel - 2 );
	else
		if ( debugLevel >= 2 )
			disp( 'None of the variables are required to be integer.' );
			disp( sprintf( '\tSolving restriction-relaxed LPT.\n' ) );
		end
		
		[ f, X ] = SolveFloatLPT( lpt, debugLevel - 2 );
	end
	
	if ( debugLevel >= 1 )
		if ( ~isempty( X ) )
			disp( sprintf( 'Optimal solution:' ) );
			disp( sprintf( '\tf = %s\tX'' = [ %s ]', num2str( f ), num2str( X' ) ) );
		else
			disp( sprintf( 'No solution.' ) );
		end
	end
	
	function EnumTypes
		global TYPE
		TYPE.LTEQ = -1;   % <=
		TYPE.EQ = 0;     % ==
		TYPE.GTEQ = +1;   % >=
	end

	function Defines
		global EPS
		EPS = 1e-10;
	end
	
	function Correction
		global TYPE
		
		assert( isvector( lpt.c ), '''c'' must be a vector.' );
		if ( size( lpt.c, 1 ) > 1 ), lpt.c = lpt.c'; end;   % make it row-vector.

		assert( isscalar( lpt.max ), ...
			'''max'' must be a logical scalar.' );
		lpt.max = logical( lpt.max );

		assert( ndims( lpt.A ) == 2, '''A'' must be a two-dimensional matrix.' );

		assert( isvector( lpt.b ), '''b'' must be a vector.' );
		if ( size( lpt.b, 2 ) > 1 ), lpt.b = lpt.b'; end;   % make it column-vector.

		assert( isvector( lpt.type ), '''type'' must be a vector.' );
		if ( size( lpt.type, 2 ) > 1 ), lpt.type = lpt.type'; end;  % make it column-vector.
		lpt.type( lpt.type < 0 ) = TYPE.LTEQ;
		lpt.type( lpt.type == 0 ) = TYPE.EQ;
		lpt.type( lpt.type > 0 ) = TYPE.GTEQ;
		
		assert( isvector( lpt.mustBeInt ), '''mustBeInt'' must be a logical vector.' );
		lpt.mustBeInt = logical( lpt.mustBeInt );
		if ( size( lpt.mustBeInt, 2 ) > 1 ), lpt.mustBeInt = lpt.mustBeInt'; end;  % make it column-vector.

		assert( length( lpt.b ) == length( lpt.type ), ...
			'Vectors ''b'' and ''type'' must be of equal length.' );

		assert( size( lpt.A, 2 ) == length( lpt.c ) && ...
			length( lpt.c ) == length( lpt.mustBeInt ), ...
			'The length of vector ''c'' and the number of columns in ''A'' must be equal.' );

		assert( isscalar( debugLevel ), '''debugLevel'' must be a scalar.' );
	end
end