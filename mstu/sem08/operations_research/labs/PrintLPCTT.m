function PrintLPCTT( lpctt, X )
	strSplit = '|';
	spaceWidth = 4;
	
	if ( isempty( X ) )
		X.Values = sparse( zeros( size( lpctt.C ) ) );
		X.Mask = sparse( false( size( lpctt.C ) ) );
	end;
	
	strColX = num2str( reshape( X.Values, [], 1 ) );
	strColX( ~X.Mask, : ) = repmat( ' ', numel( find( ~X.Mask ) ), size( strColX, 2 ) );
	strColC = num2str( reshape( lpctt.C, [], 1 ) );
	strColSplit = repmat( strSplit, size( strColX, 1 ), 1 );
	
	strColXC = [ strColX, strColSplit, strColC ];
	strColD = num2str( lpctt.D' );
	strColS = num2str( lpctt.S );

	widthXC = size( strColXC, 2 );
	widthD = size( strColD, 2 );
	if ( widthD < widthXC )
		padding = repmat( ' ', size( strColD, 1 ), widthXC - widthD );
		strColD = [ padding, strColD ];
	elseif( widthXC < widthD )
		padding = repmat( ' ', size( strColXC, 1 ), widthD - widthXC );
		strColXC = [ padding, strColXC ];
	end

	strColSpace = repmat( ' ', size( strColXC, 1 ), spaceWidth );
	strColXC = [ strColSpace, strColXC ];
	
	strColSpace = repmat( ' ', size( strColD, 1 ), spaceWidth );
	strColD = [ strColSpace, strColD ];

	strColSpace = repmat( ' ', size( strColS, 1 ), spaceWidth );
	strColS = [ strColSpace, strColS ];

	ns = numel( lpctt.S );
	nd = numel( lpctt.D );
	
	for j = 1 : ns
		strXC = reshape( strColXC( j : ns : ns * nd, : )', 1, [] );
		strOffset = repmat( ' ', 1, size( strColS, 1 ) );
		disp( [ strXC, strOffset, strColS( j, : ) ] );
	end
	disp( sprintf( '\n%s\n', reshape( strColD', 1, [] ) ) );
	pause;
end