% LPCTT - Linear Programming Classic Transport Task

function [ f, X ] = SolveLPCTT( lpctt, debugLevel )
	Correction;

	if ( debugLevel >= 1 )
		disp( sprintf( 'Initial transport table:\n' ) );
		PrintLPCTT( lpctt, [] );
	end

	[ X, lpctt ] = FindBasicSolution( lpctt, debugLevel );
	[ f, X ] = Potential( lpctt, X, debugLevel );

	if ( debugLevel >= 1 )
		disp( sprintf( 'Optimal solution:' ) );
		disp( sprintf( 'f = %s', num2str( f ) ) );
		disp( sprintf( 'X =' ) );
		disp( full( X ) );
	end

	function Correction
		assert( isvector( lpctt.S ), '''S'' must be a vector.' );
		if ( size( lpctt.S, 2 ) > 1 ), lpctt.S = lpctt.S'; end;   % make it column-vector.
		
		assert( isvector( lpctt.D ), '''D'' must be a vector.' );
		if ( size( lpctt.D, 1 ) > 1 ), lpctt.D = lpctt.D'; end;   % make it row-vector.
		
		assert( ndims( lpctt.C ) == 2, '''C'' must be a two-dimensional matrix.' );
	end
end

function [ f, X ] = Potential( lpctt, X, debugLevel )
	while ( 1 )
		f = sum( lpctt.C( X.Mask ) .* X.Values( X.Mask ) );

		if ( debugLevel >= 1 )
			disp( sprintf( 'Current solution:\n' ) );
			disp( sprintf( 'f = %s\n', num2str( f ) ) );
			PrintLPCTT( lpctt, X );
		end

		leadedIn = FindLeadedIn;

	if ( isempty( leadedIn ) ), break, end;

		cycle = FindCycle;
		leadedOut = FindLeadedOut;
		FormNewBasis;
	end

	if ( debugLevel >= 1 )
		disp( sprintf( 'Solution cannot be improved.\n' ) );
		pause;
	end

	X = X.Values;

	function [ leadedIn ] = FindLeadedIn
		EPS = 1e-4;
		
		[ U, V ] = FindUV;
		diff = FormDiff( lpctt.C, U, V );
		[ minDiff, leadedIn.Row, leadedIn.Col ] = MatrixMin( diff );

		if ( minDiff >= -EPS ), leadedIn = []; end;
		
		if ( debugLevel >= 1 )
			if ( ~isempty( leadedIn ) )
				disp( sprintf( 'New basis variable: ( %u, %u )\n', ...
					leadedIn.Row, leadedIn.Col ) );
			else
				disp( sprintf( 'No free variable can be leaded into the basis.\n' ) );
			end
			pause;
		end

		function [ U, V ] = FindUV
			[ r, c ] = find( X.Mask );
			A = sparse( numel( r ), sum( size( X.Mask ) ) );
			B = zeros( numel( r ), 1 );
			for i = 1 : numel( r )
				A( i, [ r( i ), size( X.Mask, 1 ) + c( i ) ] ) = 1;
				B( i ) = lpctt.C( r( i ), c( i ) );
			end

			UV = A \ B;

			U = UV( 1 : numel( lpctt.S ) );
			V = UV( numel( lpctt.S ) + 1 : end )';
			
			if ( debugLevel >= 2 )
				disp( sprintf( 'SLAE:\n' ) );
				disp( full( [ A, B ] ) );
				
				display( U );
				display( V );
				pause;
			end
		end
		
		function diff = FormDiff( diff, U, V )
			for i = 1 : size( lpctt.C, 1 )
				diff( i, : ) = diff( i, : ) - V;
			end
			for j = 1 : size( lpctt.C, 2 )
				diff( :, j ) = diff( :, j ) - U;
			end
			
			if ( debugLevel >= 2 )
				disp( sprintf( 'Tested differencies:\n' ) );
				disp( diff );
			end
		end
		
		function [ value, r, c ] = MatrixMin( M )
			[ value, r ] = min( M );
			[ value, c ] = min( value );
			r = r( c );
		end
	end

	function [ cycle ] = FindCycle
		stack = EmptyStack;
		stack = Push( stack, leadedIn.Row );

		prev = leadedIn;
		cycle = struct( 'Row', {}, 'Col', {} );

		horizontal = true( 1 );
		found = false( 1 );
		while ( 1 )
			[ indices, stack ] = Pop( stack );

			if ( ~isempty( indices ) )
				stack = Push( stack, indices( 1 : end - 1 ) );

				cycle( end + 1 ) = prev;

				if ( horizontal )
					cycle( end ).Row = indices( end );
				else
					cycle( end ).Col = indices( end );
				end

		if ( found ), break, end;

				if ( horizontal )
					searchField = X.Mask( cycle( end ).Row, : );
					pivot = cycle( end ).Col;
				else
					searchField = X.Mask( :, cycle( end ).Col );
					pivot = cycle( end ).Row;
				end

				indices = find( searchField );
				indices = indices( indices ~= pivot );

				found = horizontal && ismember( leadedIn.Col, indices );
				if ( found )
					indices = leadedIn.Col;
				end
				stack = Push( stack, indices );
			else
				cycle( end ) = [];
			end

			horizontal = ~horizontal;
			prev = cycle( end );
		end

		if ( debugLevel >= 1 )
			if ( ~isempty( cycle ) )
				disp( sprintf( 'Cycle:\n' ) );
				for i = 1 : length( cycle )
					disp( sprintf( '\t( %u, %u )', cycle( i ).Row, cycle( i ).Col ) );
				end
				disp( sprintf( '\n' ) );
			else
				disp( sprintf( 'Cycle: does not exist.\n' ) );
			end
		end

		function s = EmptyStack
			s.Length = 0;
			s.Items = struct( [] );
		end

		function s = Push( s, v )
			s.Length = s.Length + 1;
			s.Items( s.Length ).Value = v;
		end

		function [ v, s ] = Pop( s )
			v = s.Items( end ).Value;
			s.Items( end ) = [];
			s.Length = s.Length - 1;
		end
	end
	
	function [ leadedOut ] = FindLeadedOut
		minIndex = 2;
		for i = 4 : 2 : length( cycle )
			curValue = X.Values( cycle( i ).Row, cycle( i ).Col );
			minValue = X.Values( cycle( minIndex ).Row, cycle( minIndex ).Col );
			if ( curValue < minValue )
				minIndex = i;
			end
		end
		
		leadedOut = cycle( minIndex );
		
		if ( debugLevel >= 1 )
			disp( sprintf( 'Leaded out basis variable: ( %u, %u )\n', ...
				leadedOut.Row, leadedOut.Col ) );
			pause;
		end
	end
	
	function FormNewBasis
		w = X.Values( leadedOut.Row, leadedOut.Col );
		
		offset = +1;
		for i = 1 : length( cycle )
			r = cycle( i ).Row;
			c = cycle( i ).Col;
			X.Values( r, c ) = X.Values( r, c ) + offset * w;
			
			offset = -offset;
		end
		
		X.Mask( leadedIn.Row, leadedIn.Col ) = true( 1 );
		X.Mask( leadedOut.Row, leadedOut.Col ) = false( 1 );
	end
end

function [ X, lpctt ] = FindBasicSolution( lpctt, debugLevel )
	if ( debugLevel >= 2 )
		disp( sprintf( 'Evaluating conforming basic solution...\n' ) );
	end
	
	[ X, lpctt ] = NorthEastCorner( lpctt, debugLevel );
	
	if ( debugLevel >= 2 )
		disp( sprintf( 'Conforming basic solution:\n' ) );
		disp( sprintf( 'X =' ) );
		disp( full( X.Values ) );
		pause;
	end
end

function [ X, lpctt ] = NorthEastCorner( lpctt, debugLevel )
	X.Values = sparse( zeros( size( lpctt.C ) ) );
	X.Mask = sparse( false( size( lpctt.C ) ) );

	north = 1;
	east = 1;
	while ( north <= length( lpctt.S ) && east <= length( lpctt.D ) )
		if ( lpctt.S( north ) < lpctt.D( east ) )
			X.Values( north, east ) = lpctt.S( north );
			X.Mask( north, east ) = true( 1 );
			lpctt.D( east ) = lpctt.D( east ) - lpctt.S( north );
			north = north + 1;
		else
			X.Values( north, east ) = lpctt.D( east );
			X.Mask( north, east ) = true( 1 );
			lpctt.S( north ) = lpctt.S( north ) - lpctt.D( east );
			east = east + 1;
		end

		if ( debugLevel >= 2 )
			disp( sprintf( 'Current transport table:\n' ) );
			PrintLPCTT( lpctt, X );
		end
	end
end