function [ f, X ] = SolveIntegerLPT( lpt, debugLevel )
	[ f, X ] = BranchesAndBounds( lpt, debugLevel );
end

function [ f0, X0 ] = BranchesAndBounds( lpt, debugLevel )
	if ( debugLevel >= 1 )
		disp( sprintf( '===== Enter BranchesAndBounds =====\n' ) );
	end
	
	queue = EmptyQueue;
	queue = Queue( queue, lpt );

	f0 = -inf;
	X0 = [];
	while ( queue.Length > 0 )
		curTask = Stage1;
		[ f, X ] = Stage2;
		
		if ( ~isempty( X ) && IsBetterSolution )
			indexFrac = Stage3;

			if ( ~isempty( indexFrac ) )
				Stage4;
			end
		end
	end
	
	if ( debugLevel >= 1 )
		if ( debugLevel >= 2 )
			disp( sprintf( 'No tasks in the task-queue.\n' ) );
		end
		
		disp( sprintf( 'Optimal solution:' ) );
		if ( ~isempty( X0 ) )
			disp( sprintf( '\tf0 = %s\t\tX0 = [ %s ]''\n', num2str( f0 ), num2str( X0' ) ) );
		else
			disp( sprintf( '\tNo solution.\n' ) );
		end
		
		disp( sprintf( '===== Exit BranchesAndBounds =====\n' ) );
	end
	
	function curTask = Stage1
		[ curTask, queue ] = Dequeue( queue );
		
		if ( debugLevel >= 2 )
			disp( sprintf( 'Current task:\n' ) );
			PrintLPT( curTask );
		end
	end
	
	function [ f, X ] = Stage2
		[ f, X ] = SolveFloatLPT( curTask, debugLevel - 2 );
	
		if ( debugLevel >= 2 )
			if ( ~isempty( X ) )
				disp( sprintf( '\tCurrent solution:' ) );
				disp( sprintf( '\t\tf = %s\t\tX = [ %s ]''\n', ...
					num2str( f ), num2str( X' ) ) );

				if ( ~IsBetterSolution )
					disp( sprintf( '\t\tRejected due to reason: f is not better than f0.\n' ) );

					if ( isfinite( f0 ) )
						disp( sprintf( '\tPrefered solution:' ) );
						disp( sprintf( '\t\tf0 = %s\t\tX0 = [ %s ]''\n', ...
							num2str( f0 ), num2str( X0' ) ) );
					end

					pause;
				end
			else
				disp( sprintf( 'No solution.\n' ) );
				pause;
			end
		end
	end
	
	function test = IsBetterSolution
		test = ( curTask.max && f > f0 || ~curTask.max && f < f0 );
	end
	
	function indexFrac = Stage3
		indexFrac = find( ~IsInt( X ) & lpt.mustBeInt, 1 );

		if ( isempty( indexFrac ) )
			f0 = f;
			X0 = round( X );
		end
		
		if ( debugLevel >= 2 )
			if ( isempty( indexFrac ) )
				disp( sprintf( '\t\tAccepted.\n' ) );
			else
				disp( sprintf( '\t\tRejected due to reason: variable %u must be an integer.\n', indexFrac ) );
				
				if ( isfinite( f0 ) )
					disp( sprintf( '\tPrefered solution:' ) );
					disp( sprintf( '\t\tf0 = %s\t\tX0 = [ %s ]''\n', ...
						num2str( f0 ), num2str( X0' ) ) );
				end
			end
			
			pause;
		end
		
		function test = IsInt( a )
			global EPS
			test = ( abs( a - round( a ) ) < EPS );
		end
	end
	
	function Stage4
		global TYPE
		
		newRow = length( curTask.b ) + 1;

		curTask.A( newRow, indexFrac ) = 1;
		curTask.b( newRow ) = floor( X( indexFrac ) );
		curTask.type( newRow ) = TYPE.LTEQ;
		queue = Queue( queue, curTask );
		
		if ( debugLevel >= 2 )
			disp( 'Split into 2 tasks:' );
			disp( sprintf( 'Task 1:\n' ) );
			PrintLPT( curTask );
		end

		curTask.b( newRow ) = curTask.b( newRow ) + 1;
		curTask.type( newRow ) = TYPE.GTEQ;
		queue = Queue( queue, curTask );
		
		if ( debugLevel >= 2 )
			disp( sprintf( 'Task 2:\n' ) );
			PrintLPT( curTask );
		end
	end
	
	function q = EmptyQueue
		q.Length = 0;
		q.Items = struct( [] );
	end

	function q = Queue( q, v )
		q.Length = q.Length + 1;
		q.Items( q.Length ).Value = v;
	end

	function [ v, q ] = Dequeue( q )
		v = q.Items( 1 ).Value;
		q.Items( 1 ) = [];
		q.Length = q.Length - 1;
	end
end