% LPDT - Linear Programming Distribution Task

function [ f, X ] = SolveLPDT( lpdt, debugLevel )
	Correction;

	EnumMarkation;
	
	if ( debugLevel >= 1 )
		disp( sprintf( 'Initial cost-matrix:\n' ) );
		PrintLPDT( lpdt.C, [] );
	end
	
	[ f, X ] = Hungarian( lpdt, debugLevel );
	
	if ( debugLevel >= 1 )
		disp( sprintf( 'Optimal solution:' ) );
		disp( sprintf( 'f = %s', num2str( f ) ) );
		disp( sprintf( 'X =\n' ) );
		disp( full( X ) );
	end
	
	function Correction
		assert( ndims( lpdt.C ) == 2 && size( lpdt.C, 1 ) == size( lpdt.C, 2 ), ...
			'''C'' must be a two-dimensional square-matrix' );

		assert( isscalar( lpdt.max ), ...
			'''max'' must be a logical scalar.' );
		lpdt.max = logical( lpdt.max );

		assert( isscalar( debugLevel ), '''debugLevel'' must be a scalar.' );
	end
end

function [ f, X ] = Hungarian( lpdt, debugLevel )
	global NONE SIZ PRIME

	if ( debugLevel >= 1 )
		disp( sprintf( '===== Enter Hungarian =====\n' ) );
	end

	C = lpdt.C;
	N = size( lpdt.C, 1 );
	if ( lpdt.max ), Rearrange, end;
	logic = Prepare;
	Iteration;

	X = logic.X;
	f = sum( C( X ) );

	if ( debugLevel >= 1 )
		disp( sprintf( '===== Exit Hungarian =====\n' ) );
	end
	
	function Rearrange
		m = max( lpdt.C, [], 2 );
		for j = 1 : N
			lpdt.C( :, j ) = m - lpdt.C( :, j );
		end

		if ( debugLevel >= 2 )
			disp( 'REARRANGE (maximization problem):' );
			PrintLPDT( lpdt.C, [] );
		end
	end

	function logic = Prepare
		PrepareStep1;
		PrepareStep2;
		logic = PrepareStep3;

		function PrepareStep1
			l = min( lpdt.C );
			for i = 1 : N
				lpdt.C( i, : ) = lpdt.C( i, : ) - l;
			end

			if ( debugLevel >= 2 )
				disp( 'PREPARE. STEP 1:' );
				PrintLPDT( lpdt.C, [] );
			end
		end

		function PrepareStep2
			d = min( lpdt.C, [], 2 );
			for j = 1 : N
				lpdt.C( :, j ) = lpdt.C( :, j ) - d;
			end

			if ( debugLevel >= 2 )
				disp( 'PREPARE. STEP 2:' );
				PrintLPDT( lpdt.C, [] );
			end
		end

		function logic = PrepareStep3
			logic.X = sparse( N, N );
			logic.IsRowSelected = false( 1, N );
			logic.IsColSelected = false( 1, N );

			for j = 1 : N
				for i = find( lpdt.C( :, j ) == 0 )'
					if ( all( ( logic.X( i, : ) ~= SIZ ) ) )
						logic.X( i, j ) = SIZ;
						break;
					end
				end
			end
		end
	end

	function Iteration
		iterNum = 0;
		while ( 1 )
			if ( debugLevel >= 1 )
				iterNum = iterNum + 1;
				disp( sprintf( 'ITERATION %u:', iterNum ) );
				PrintLPDT( lpdt.C, logic );
			end

			k = IterationStep1;

		if k == N, break, end;

			IterationStep3;

			while ( 1 )
				while ( 1 )
					[ newZero ] = IterationStep4;

				% there is at least one unhighlighted zero
				if ( ~isempty( newZero ) ), break, end;

					IterationStep9;
				end

				noZeroFromSIZ = IterationStep5;

			if noZeroFromSIZ, break, end;

				IterationStep6;
			end

			IterationStep7_8;
		end

		IterationStep2;

		function k = IterationStep1
			k = numel( find( logic.X == SIZ ) );

			if ( debugLevel >= 2 )
				disp( sprintf( 'ITERATION %u. STEP 1:', iterNum ) );
				if ( k < N )
					compareStr = '<';
				else
					compareStr = '=';
				end
				disp( sprintf( 'k = %u %s %u = n\n', k, compareStr, N ) );
				pause;
			end
		end

		function IterationStep2
			% in general this function is idle.

			assert( all( all( logic.X ~= PRIME ) ) );
			logic.X = logical( logic.X );

			if ( debugLevel >= 2 )
				disp( sprintf( 'ITERATION %u. STEP 2:', iterNum ) );
				disp( 'Final equivalent cost-matrix:' );
				PrintLPDT( lpdt.C, logic );
			end
		end

		function IterationStep3
			logic.RowSIZ = full( any( logic.X' == SIZ ) );
			logic.ColSIZ = full( any( logic.X == SIZ ) );
			
			logic.IsColSelected = logic.ColSIZ;

			if ( debugLevel >= 2 )
				disp( sprintf( 'ITERATION %u. STEP 3:', iterNum ) );
				disp( 'Highlighting columns containing 0*:' );
				PrintLPDT( lpdt.C, logic );
			end
		end

		function [ newZero ] = IterationStep4
			[ newZero.Row, newZero.Col ] = ...
				find( lpdt.C( ~logic.IsRowSelected, ~logic.IsColSelected ) == 0, 1 );
			
			if ( isempty( newZero.Row ) ), newZero = []; end;
			
			if ( ~isempty( newZero ) )
				newZero.Row = CorrectIndex( newZero.Row, ~logic.IsRowSelected );
				newZero.Col = CorrectIndex( newZero.Col, ~logic.IsColSelected );
			end

			if ( debugLevel >= 2 )
				disp( sprintf( 'ITERATION %u. STEP 4:', iterNum ) );
				if ( ~isempty( newZero ) )
					disp( sprintf( 'C( %u, %u ) = 0 - is unhighlighted.\n', ...
						newZero.Row, newZero.Col ) );
				else
					disp( sprintf( 'There is no unhighlighted 0.\n' ) );
				end
				pause;
			end

			function index = CorrectIndex( index, mask )
				if ( ~isempty( index ) )
					maskedIndices = find( mask, index );
					index = maskedIndices( index );
				end
			end
		end

		function noZeroFromSIZ = IterationStep5
			noZeroFromSIZ = ~logic.RowSIZ( newZero.Row );

			if ( debugLevel >= 2 )
				disp( sprintf( 'ITERATION %u. STEP 5:', iterNum ) );
				if ( ~noZeroFromSIZ )
					disp( sprintf( 'C( %u, %u ) = 0* - is in one row with the new 0''.\n', ...
						newZero.Row, find( logic.X( newZero.Row, : ) == SIZ ) ) );
				else
					disp( sprintf( 'There is no 0* in one row with the new 0''.\n' ) );
				end
				pause;
			end
		end

		function IterationStep6
			logic.X( newZero.Row, newZero.Col ) = PRIME;

			logic.IsRowSelected( newZero.Row ) = 1;
			logic.IsColSelected( logic.X( newZero.Row, : ) == SIZ ) = 0;

			if ( debugLevel >= 2 )
				disp( sprintf( 'ITERATION %u. STEP 6:', iterNum ) );
				disp( sprintf( 'Setting one more C( %u, %u ) = 0'':', ...
					newZero.Row, newZero.Col ) );
				PrintLPDT( lpdt.C, logic );
			end
		end

		function IterationStep7_8
			logic.X( newZero.Row, newZero.Col ) = PRIME;

			if ( debugLevel >= 2 )
				disp( sprintf( 'ITERATION %u. STEPS 7 & 8:', iterNum ) );
				PrintLPDT( lpdt.C, logic );
				disp( sprintf( 'Builded L-chain:' ) );
			end

			prime.Row = newZero.Row;
			prime.Col = newZero.Col;
			while ( 1 )
				row = find( logic.X( :, prime.Col ) == SIZ );
				logic.X( prime.Row, prime.Col ) = SIZ;

				if ( debugLevel >= 2 )
					disp( sprintf( 'C( %u, %u )', prime.Row, prime.Col ) );
				end

			if ( isempty( row ) ), break, end;

				logic.X( row, prime.Col ) = NONE;

				if ( debugLevel >= 2 )
					disp( sprintf( 'C( %u, %u )', row, prime.Col ) );
				end

				prime.Col = find( logic.X( row, : ) == PRIME );
				prime.Row = row;
			end

			if ( debugLevel >= 2 )
				pause;
				disp( ' ' );
				disp( 'Changing 0* to 0 and 0'' to 0* inside an L-chain:' );
				PrintLPDT( lpdt.C, logic );
			end

			logic.X( logic.X == PRIME ) = NONE;

			if ( debugLevel >= 2 )
				disp( 'Changing 0'' to 0 outside of L-chain:' );
				PrintLPDT( lpdt.C, logic );
			end
			
			logic.IsRowSelected( : ) = false( 1 );
			logic.IsColSelected( : ) = false( 1 );
		end

		function IterationStep9
			h = min( min( lpdt.C( ~logic.IsRowSelected, ~logic.IsColSelected ) ) );
			lpdt.C( ~logic.IsRowSelected, ~logic.IsColSelected ) = ...
				lpdt.C( ~logic.IsRowSelected, ~logic.IsColSelected ) - h;
			lpdt.C( logic.IsRowSelected, logic.IsColSelected ) = ...
				lpdt.C( logic.IsRowSelected, logic.IsColSelected ) + h;

			if ( debugLevel >= 2 )
				disp( sprintf( 'ITERATION %u. STEP 9:', iterNum ) );
				disp( 'Obtaining new equivalent cost-matrix containing at least one unhighlighted 0:' );
				disp( sprintf( 'h = %u\n', h ) );
				PrintLPDT( lpdt.C, logic );
			end
		end
	end
end
	
function EnumMarkation
	global NONE SIZ PRIME
	NONE = 0;   % unmarked number
	SIZ = 1;    % 0* - System of Independent Zeros
	PRIME = 2;  % 0'
end