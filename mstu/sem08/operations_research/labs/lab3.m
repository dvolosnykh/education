function lab3( lpctt, debugLevel )
	variant4 = true( 1 );
	distribution = true( 1 );
	
	if ( isempty( lpctt ) )
		if ( variant4 )
			if ( ~distribution )
				lpctt.S = [ 100 150 50 ];
				lpctt.D = [ 75 80 60 85 ];
				lpctt.C = [ 6  7  3  5; ...
				        	1  2  5  6; ...
					        8 10 20  1 ];
			else
				lpctt.S = [ 1 1 1 1 1 ];
				lpctt.D = [ 1 1 1 1 1 ];
				lpctt.C = [  3  5  2  4  8; ...
                            10 10  4  3  6; ...
                             5  6  9  8  3; ...
                             6  2  5  8  4; ...
                             5  4  8  9  3 ];
			end
		else
			lpctt.S = [ 1 1 1 1 1 ];
			lpctt.D = [ 1 1 1 1 1 ];
			lpctt.C = [  3  5  2  4  0; ...
                        10  0  4  3  6; ...
                         5  6  0  8  3; ...
                         0  2  5  8  4; ...
                         5  4  8  0  3 ];
		end
	end
	
	[ f, X ] = SolveLPCTT( lpctt, debugLevel );
end