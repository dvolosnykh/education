% LPTST - Linear Programming Travelling Salesman Task

function [ f, X ] = SolveLPTST( lptst, debugLevel )
	Correction;

	if ( debugLevel >= 1 )
		disp( sprintf( 'Inital cost-matrix:\n' ) );
		PrintLPDT( lptst.C, [] );
	end

	[ f, X ] = BranchesAndBounds( lptst, debugLevel );

	if ( debugLevel >= 1 )
		disp( sprintf( 'Optimal solution:' ) );
		disp( sprintf( '\tf = %s', num2str( f ) ) );
		disp( sprintf( '\tX =' ) );
		disp( full( X ) );
	end

	function Correction
		assert( ndims( lptst.C ) == 2 && size( lptst.C, 1 ) == size( lptst.C, 2 ), ...
			'''C'' must be a two-dimensional square-matrix' );
		
		assert( isscalar( lptst.max ), ...
			'''max'' must be a logical scalar.' );
		lpdt.max = logical( lptst.max );

		assert( isscalar( debugLevel ), '''debugLevel'' must be a scalar.' );
	end
end

function [ f0, X0 ] = BranchesAndBounds( lptst, debugLevel )
	if ( debugLevel >= 1 )
		disp( sprintf( '===== Enter BranchesAndBounds =====\n' ) );
	end

	stack = EmptyStack;
	stack = Push( stack, lptst );

	if ( lptst.max )
		f0 = -inf;
	else
		f0 = inf;
	end

	X0 = [];
	while ( stack.Length > 0 )
		curTask = Stage1;
		[ f, X ] = Stage2;

		if ( ~isempty( X ) && IsBetterSolution( f ) )
			cycle = Stage3;

			if ( length( cycle ) < size( curTask.C, 1 ) )
				Stage4;
			end
		end
	end

	if ( debugLevel >= 1 )
		if ( debugLevel >= 2 )
			disp( sprintf( 'No tasks in the task-stack.\n' ) );
		end

		disp( sprintf( '===== Exit BranchesAndBounds =====\n' ) );
	end

	function curTask = Stage1
		[ curTask, stack ] = Pop( stack );
		
		if ( debugLevel >= 2 )
			disp( sprintf( 'Current task:\n' ) );
			PrintLPDT( curTask.C, [] );
		end
	end

	function [ f, X ] = Stage2
		[ f, X ] = SolveLPDT( curTask, debugLevel - 2 );

		if ( debugLevel >= 2 )
			if ( ~isempty( X ) )
				disp( sprintf( 'Current solution:' ) );
				disp( sprintf( 'f = %s', num2str( f ) ) );
				disp( sprintf( 'X =' ) );
				disp( full( X ) );

				if ( ~IsBetterSolution( f ) )
					disp( sprintf( '\t\tRejected due to reason: f is not better than f0.\n' ) );

					if ( isfinite( f0 ) )
						disp( sprintf( 'Prefered solution:' ) );
						disp( sprintf( 'f0 = %s', num2str( f0 ) ) );
						disp( sprintf( 'X0 =' ) );
						disp( full( X0 ) );
					end

					pause;
				end
			else
				disp( sprintf( 'No solution.\n' ) );
				pause;
			end
		end
	end

	function test = IsBetterSolution( f )
		test = ( curTask.max && f > f0 || ~curTask.max && f < f0 );
	end

	function cycle = Stage3
		cycle = ShortestCycle;

		if ( length( cycle ) == size( curTask.C, 1 ) )
			f0 = f;
			X0 = X;
		end

		if ( debugLevel >= 2 )
			if ( length( cycle ) == size( curTask.C, 1 ) )
				disp( sprintf( '\t\tAccepted.\n' ) );
			else
				disp( sprintf( '\t\tRejected due to reason: the way found is not a Hamiltonian cycle.\n' ) );

				if ( isfinite( f0 ) )
					disp( sprintf( 'Prefered solution:' ) );
					disp( sprintf( 'f0 = %s', num2str( f0 ) ) );
					disp( sprintf( 'X0 =' ) );
					disp( full( X0 ) );
				end
			end

			pause;
		end

		function cycle = ShortestCycle
			towns = 1 : size( curTask.C, 1 );

			cycle = [];
			while ( ~isempty( towns ) )
				curCycle = [];
				
				i = towns( 1 );
				while ( ~ismember( i, curCycle ) )
					curCycle = [ curCycle, i ];
					i = find( X( i, : ) );
				end
				
				towns = setdiff( towns, curCycle );
				
				if ( isempty( cycle ) || length( curCycle ) < length( cycle ) )
					cycle = curCycle;
				end
			end
			
			if ( debugLevel >= 2 )
				display( cycle );
			end
		end
	end
	
	function Stage4
		if ( debugLevel >= 2 )
			disp( sprintf( 'Split into %u tasks:', length( cycle ) ) );
		end

		n = length( cycle );
		cycle = [ cycle, cycle( 1 ) ];
		for i = 1 : n
			newTask = curTask;
			newTask.C( cycle( i ), cycle( i + 1 ) ) = inf;
			stack = Push( stack, newTask );
			
			if ( debugLevel >= 2 )
				disp( sprintf( 'Task %u:\n', i ) );
				PrintLPDT( newTask.C, [] );
			end
		end
	end
	
	function s = EmptyStack
		s.Length = 0;
		s.Items = struct( [] );
	end

	function s = Push( s, v )
		s.Length = s.Length + 1;
		s.Items( s.Length ).Value = v;
	end

	function [ v, s ] = Pop( s )
		v = s.Items( end ).Value;
		s.Items( end ) = [];
		s.Length = s.Length - 1;
	end
end

function [ f, X ] = Direct( C, debugLevel )
	N = size( C, 1 );

	[ f, X ] = SolveLPT( FormLPT, debugLevel - 2 );
	X = reshape( X( 1 : N ^ 2 ), N, [] );

	function lpt = FormLPT
		exclude = 0;

		noFirstTownRestriction = 1;
		noDiagonalRestriction = 0;
		
		lpt = [];
		Form_c;
		Form_max;
		Form_A;
		Form_b;
		Form_type;
		Form_mustBeInt;
		
%		Aux;

		function Form_c
			lpt.c = [ reshape( C, 1, [] ), zeros( 1, N ) ];
			
			if ( exclude )
				lpt.c( N ^ 2 + 1 ) = [];
			end
		end
		
		function Form_max
			lpt.max = false( 1 );
		end
		
		function Form_A
			XI = repmat( speye( N ), 1, N );
			XJ = reshape( XI', N, [] );
			ZU = sparse( 2 * N, N );
			oneness = [ [ XJ; XI ], ZU ];

			XIJ = N * speye( N ^ 2 );
			U = ( XI - XJ )';
			hilbertCycle = [ XIJ, U ];

			lpt.A = [ oneness; hilbertCycle ];
			
			if ( exclude )
				indices = [];
				if ( noFirstTownRestriction )
					indices = unique( [ indices, [ 1, N + 1 ] ] );
					indices = unique( [ indices, 2 * N + [ 1 : N, N + 1 : N : N ^ 2 ] ] );
				end
				if ( noDiagonalRestriction )
					indices = unique( [ indices, 2 * N + ( 1 : N + 1 : N ^ 2 ) ] );
				end
				lpt.A( indices, : ) = [];
				
				lpt.A( :, N ^ 2 + 1 ) = [];
			end
		end
		
		function Form_b
			lpt.b = ones( 2 * N + N ^ 2, 1 );
			lpt.b( 2 * N + ( 1 : N ^ 2 ) ) = N - 1;
			
			if ( exclude )
				indices = [];
				if ( noFirstTownRestriction )
					indices = unique( [ indices, [ 1, N + 1 ] ] );
					indices = unique( [ indices, 2 * N + [ 1 : N, N + 1 : N : N ^ 2 ] ] );
				end
				if ( noDiagonalRestriction )
					indices = unique( [ indices, 2 * N + ( 1 : N + 1 : N ^ 2 ) ] );
				end
				lpt.b( indices, : ) = [];
			end
		end
		
		function Form_type
			lpt.type = zeros( 2 * N + N ^ 2, 1 );
			lpt.type( 2 * N + ( 1 : N ^ 2 ) ) = -1;
			
			if ( exclude )
				indices = [];
				if ( noFirstTownRestriction )
					indices = unique( [ indices, [ 1, N + 1 ] ] );
					indices = unique( [ indices, 2 * N + [ 1 : N, N + 1 : N : N ^ 2 ] ] );
				end
				if ( noDiagonalRestriction )
					indices = unique( [ indices, 2 * N + ( 1 : N + 1 : N ^ 2 ) ] );
				end
				lpt.type( indices, : ) = [];
			end
		end
		
		function Form_mustBeInt
			lpt.mustBeInt = true( N ^ 2 + N, 1 );
			
			if ( exclude )
				lpt.mustBeInt( N ^ 2 + 1 ) = [];
			end
		end
%{
		function Aux
			indices = 2 * N + 1 : size( lpt.A, 1 ) + 1 : N ^ 2 * size( lpt.A, 1 );
			diagonal = lpt.A( indices );
			diagonal( 1 : N + 1 : N ^ 2 ) = 1;
			lpt.A( indices ) = diagonal;
			
			lpt.b( 2 * N + ( 1 : N + 1 : N ^ 2 ) ) = 0;
			
			lpt.type( 2 * N + ( 1 : N + 1 : N ^ 2 ) ) = 0;
		end
%}
	end
end