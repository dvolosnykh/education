#ifndef FRUSTRUM_H
#define FRUSTRUM_H


#include "Matrix.h"
#include "Plane.h"
#include "Bound.h"


enum { FULL_OUTSIDE, PARTIAL_INSIDE, FULL_INSIDE };


class CFrustum
{
private:
	enum
	{
		PLANE_RIGHT,
		PLANE_LEFT,
		PLANE_BOTTOM,
		PLANE_TOP,
		PLANE_FAR,
		PLANE_NEAR,
		PLANE_NUM
	};

public:
	float m_Aspect;
	float m_FOV;
	float m_TanY;
	float m_TanX;

	POINT min;			// upper-left corner.
	LONG height, width;	// screen dimensions.

	CPlane m_Frustum[ PLANE_NUM ];

public:
	float m_Near;
	float m_Far;

public:
	CFrustum() {};
	virtual ~CFrustum() {};

	unsigned BoundLocation( const CBound * const bound ) const;

protected:
	void UpdateFrustum( const CMatrix & m );
	void Set( const float FOV, const float aspect, const float Near, const float Far );

private:
	unsigned SphereLocation( const CVertex3D * const center, const float radius ) const;
	unsigned BoxLocation( const CVertex3D * const min, const CVertex3D * const max ) const;
};

#endif