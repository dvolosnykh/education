#include "stdafx.h"

#include "Bound.h"
#include "ray.h"


enum { TEST_NEGATIVE, TEST_POSITIVE, TEST_UNKNOWN };


CBound::func_t CBound::test[ PLANE_NUM ] =
{
	&CBound::test1, &CBound::test2,
	&CBound::test3, &CBound::test4,
	&CBound::test5, &CBound::test6
};


bool CBound::CheckIntersection( const CRay * const ray ) const
{
	VertexArray v( 2, false );
	v[ 0 ] = ray->m_start;
	v[ 1 ] = ray->m_end;

	return Test( v );
}

bool CBound::CheckCollision( const CTriangle * const tri ) const
{
	const register unsigned * const index = tri->m_index;
	VertexArray v( 3, false );

	for ( register unsigned i = 0; i < 3; i++ )
		v[ i ] = CGL::Vertex[ index[ i ] ];

	return Test( v );
}

int CBound::SimpleTest( const VertexArray & v ) const
{
	return
	(
		!AllPointsOutside( v ) ? TEST_POSITIVE :
		!PointsByDifferentSides( v ) ? TEST_NEGATIVE :
		TEST_UNKNOWN
	);
}

bool CBound::Test( const VertexArray & v ) const
{
	int intersects = SimpleTest( v );

	if ( intersects == TEST_UNKNOWN )
	{
		LONG i;
		for ( i = 0; i < v.GetSize() - 1 && !IntersectsBound( CRay( v[ i ], v[ i + 1 ] ) ); i++ );

		intersects = i < v.GetSize() - 1 ? TEST_POSITIVE : TEST_NEGATIVE;
	}

	return ( intersects == TEST_POSITIVE ? true : false );
}


bool CBound::IntersectsFace( const CVertex3D * const bound, const unsigned i, const CRay * const ray ) const
{
	return
	(
		ray->IntersectsBoundingPlane( bound, i ) &&
		(
			i == X ? PointInside_NoX( ray->IntersectionWithBoundingPlane( bound, i ) ) :
			i == Y ? PointInside_NoY( ray->IntersectionWithBoundingPlane( bound, i ) ) :
			PointInside_NoZ( ray->IntersectionWithBoundingPlane( bound, i ) )
		)
	);
}

bool CBound::IntersectsBound( const CRay ray ) const
{
	return
	(
		IntersectsFace( &m_Min, Z, &ray ) ||
		IntersectsFace( &m_Max, Z, &ray ) ||
		IntersectsFace( &m_Min, X, &ray ) ||
		IntersectsFace( &m_Max, X, &ray ) ||
		IntersectsFace( &m_Min, Y, &ray ) ||
		IntersectsFace( &m_Max, Y, &ray )
	);

}

// true - when there's no side for which all points are on the outer side.
bool CBound::PointsByDifferentSides( const VertexArray & v ) const
{

	unsigned i;
	for ( i = 0;
		i < PLANE_NUM && !AllOnOuterSide( v, i );
		i++ );

	return ( i < PLANE_NUM );
}

bool CBound::AllOnOuterSide( const VertexArray & v, const unsigned index ) const
{
	LONG i;
	for ( i = 0;
		i < v.GetSize() && !( this->*test[ index ] )( &v[ i ] );
		i++ );

	return ( i < v.GetSize() );
}

bool CBound::AllPointsOutside( const VertexArray & v ) const
{
	LONG i;
	for ( i = 0;
		i < v.GetSize() && !PointInside( &v[ i ] );
		i++ );

	return ( i == v.GetSize() );
}

bool CBound::PointInside( const CVertex3D * const point ) const
{
	unsigned i;
	for ( i = 0;
		i < PLANE_NUM && ( this->*test[ i ] )( point );
		i++ );

	return ( i == PLANE_NUM );
}

bool CBound::PointInside_NoX( const CVertex3D point ) const
{
	return
	(
		( this->*test[ PLANE_BOTTOM ] )( &point ) &&
		( this->*test[ PLANE_TOP    ] )( &point ) &&
		( this->*test[ PLANE_FAR    ] )( &point ) &&
		( this->*test[ PLANE_NEAR   ] )( &point )
	);
}

bool CBound::PointInside_NoY( const CVertex3D point ) const
{
	return
	(
		( this->*test[ PLANE_RIGHT ] )( &point ) &&
		( this->*test[ PLANE_LEFT  ] )( &point ) &&
		( this->*test[ PLANE_FAR   ] )( &point ) &&
		( this->*test[ PLANE_NEAR  ] )( &point )
	);
}

bool CBound::PointInside_NoZ( const CVertex3D point ) const
{
	return
	(
		( this->*test[ PLANE_RIGHT  ] )( &point ) &&
		( this->*test[ PLANE_LEFT   ] )( &point ) &&
		( this->*test[ PLANE_BOTTOM ] )( &point ) &&
		( this->*test[ PLANE_TOP    ] )( &point )
	);
}

void CBound::Calculate( const CTriangleList & Triangles )
{
	m_Min = m_Max = CGL::Vertex[ Triangles[ 0 ]->m_index[ 0 ] ];

	// Bounding volume is calculated so that every triangle fits into it
	for ( LONG i = 0; i < Triangles.GetSize(); i++ )
		for ( LONG j = 0; j < 3; j++ )
		{
			const CVertex3D & v = CGL::Vertex[ Triangles[ i ]->m_index[ j ] ];

			if ( v.x < m_Min.x )
				m_Min.x = v.x;
			else if ( v.x > m_Max.x )
				m_Max.x = v.x;

			if ( v.y < m_Min.y )
				m_Min.y = v.y;
			else if ( v.y > m_Max.y )
				m_Max.y = v.y;

			if ( v.z < m_Min.z )
				m_Min.z = v.z;
			else if ( v.z > m_Max.z )
				m_Max.z = v.z;
		}

	Set( m_Min, m_Max );
}

void CBound::CalcBoundSphere()
{
	MakeCubic();

	m_Center = ( m_Min + m_Max ) * 0.5f;
	m_Radius = 0.86602540378443864676372317075294f * ( m_Max.x - m_Min.x );	// sqrt( 3 ) / 2;
}

void CBound::MakeCubic()
{
	CVector3D d = m_Max - m_Min;

	// Make all coordinates of the vector equal to the largest.
	if ( d.y > d.x )
		d.x = d.y;
	else
		d.y = d.x;

	if ( d.z > d.x )
		d.x = d.y = d.z;
	else
		d.z = d.x;

	m_Max = m_Min + d;
}

void CBound::Set( const CVertex3D & Min, const CVertex3D & Max )
{
	m_Min = Min, m_Max = Max;

	CalcBoundSphere();
}

void CBound::Draw( const float intensity ) const
{
	// Prepare for drawing.
	RGBQUAD color;
	color.rgbRed = UCHAR_MAX;
	color.rgbGreen = color.rgbBlue = ( BYTE )( UCHAR_MAX * ( 1.0f - intensity ) );

//	HPEN hPen = CreatePen( PS_SOLID, 1, *( COLORREF * )&color );
//	HGDIOBJ hOldObj = CGL::SelectObject( hPen );

	// Drawing.
/*
	CGL::MoveTo( m_Vertex[ 0 ] );
	CGL::LineTo( m_Vertex[ 1 ] );
	CGL::LineTo( m_Vertex[ 2 ] );
	CGL::LineTo( m_Vertex[ 3 ] );
	CGL::LineTo( m_Vertex[ 0 ] );
	
	CGL::MoveTo( m_Vertex[ 4 ] );
	CGL::LineTo( m_Vertex[ 5 ] );
	CGL::LineTo( m_Vertex[ 6 ] );
	CGL::LineTo( m_Vertex[ 7 ] );
	CGL::LineTo( m_Vertex[ 4 ] );

	for ( unsigned i = 0; i < 3; i++ )
	{
		CGL::MoveTo( m_Vertex[ i ] );
		CGL::LineTo( m_Vertex[ i + 4 ] );
	}
*/
	// Finish drawing.
//	CGL::SelectObject( hOldObj );
//	DeleteObject( hPen );
}