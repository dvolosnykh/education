#include "stdafx.h"

#include "Triangle.h"
#include "ray.h"


const CTriangle * pWanted = NULL;


void CTriangle::Set( const unsigned i0, const unsigned i1, const unsigned i2 )
{
	const CVertex3D & p0 = CGL::Vertex[ m_index[ 0 ] = i0 ];
	const CVertex3D & p1 = CGL::Vertex[ m_index[ 1 ] = i1 ];
	const CVertex3D & p2 = CGL::Vertex[ m_index[ 2 ] = i2 ];

	CPlane::Set( p0, p1, p2 );
	CalcAntiPlanes( p0, p1, p2 );
}

void CTriangle::CalcAntiPlanes( const CVertex3D & v0, const CVertex3D & v1, const CVertex3D & v2 )
{
	m_AntiPlane[ 0 ].Set( v0, v1, v1 - m_Normal );
	m_AntiPlane[ 1 ].Set( v1, v2, v2 - m_Normal );
	m_AntiPlane[ 2 ].Set( v2, v0, v0 - m_Normal );
}

bool CTriangle::InsideAntiPrism( const CVertex3D point ) const
{
	return
	(
		m_AntiPlane[ 0 ].SignDistance( &point ) >= -SMALL_VALUE &&
		m_AntiPlane[ 1 ].SignDistance( &point ) >= -SMALL_VALUE &&
		m_AntiPlane[ 2 ].SignDistance( &point ) >= -SMALL_VALUE
	);
}

bool CTriangle::CheckIntersection( const CRay * const ray ) const
{
	return
	(
		ray->IntersectsPlane( this ) &&
		InsideAntiPrism( ray->IntersectionWithPlane( this ) )
	);
}


float CTriangle::GetSmoothHeight( const float at_x, const float at_z ) const
{
	const CVector3D & n = m_Normal;
	return -( n.x * at_x + n.z * at_z + m_D ) / n.y;
}

bool CTriangle::IsRendered() const
{
	return ( m_FrameIndex == CGL::FrameIndex );
}

void CTriangle::Render()
{
	CGL::VisTriList.Add( this );
	m_FrameIndex = CGL::FrameIndex;
}

void CTriangle::DrawHorzLine( const CObserver * const pObserver, const float x_main, float x_side, const unsigned scan_line, float z, const float dz, CVertex2D t, const CVector2D dt ) const
{
	const LONG & Width = pObserver->width;

	if ( x_main <= x_side )
	{
		LONG xm = ( LONG )ceilf( x_main );
		LONG xs = ( LONG )floorf( x_side );

		// is there something to draw?
		if ( 0 <= xs && xm < Width )
		{
			if ( xs >= Width ) xs = Width - 1;

			register unsigned index = scan_line + xm;
			for ( register LONG cur_x = xm; cur_x <= xs; cur_x++, index++, z += dz, t += dt )
				if ( cur_x >= 0 && CGL::NeedSavePoint( pObserver, z, index ) )
					CGL::SavePoint( z, index, t );
		}
	}
	else
	{
		LONG xm = ( LONG )floorf( x_main );
		LONG xs = ( LONG )ceilf( x_side );

		// is there something to draw?
		if ( 0 <= xm && xs < Width )
		{
			if ( xs < 0 ) xs = 0;

			register unsigned index = scan_line + xm;
			for ( register LONG cur_x = xm; cur_x >= xs; cur_x--, index--, z -= dz, t -= dt )
				if ( cur_x < Width && CGL::NeedSavePoint( pObserver, z, index ) )
					CGL::SavePoint( z, index, t );
		}
	}
}

void CTriangle::Draw( const CObserver * const pObserver ) const
{
	const LONG & Height = pObserver->height;
	const LONG & Width = pObserver->width;

	unsigned i0 = m_index[ 0 ];
	unsigned i1 = m_index[ 1 ];
	unsigned i2 = m_index[ 2 ];

	const POINT3D & u0 = CGL::Screen[ i0 ];
	const POINT3D & u1 = CGL::Screen[ i1 ];
	const POINT3D & u2 = CGL::Screen[ i2 ];

	SortIndicesByY( i0, i1, i2 );

	const POINT3D & v0 = CGL::Screen[ i0 ];
	const POINT3D & v1 = CGL::Screen[ i1 ];
	const POINT3D & v2 = CGL::Screen[ i2 ];

	const CVertex2D & t0 = CGL::TexCoord[ i0 ];
	const CVertex2D & t1 = CGL::TexCoord[ i1 ];
	const CVertex2D & t2 = CGL::TexCoord[ i2 ];

	LONG miny = v0.y;
	LONG midy = v1.y;
	LONG maxy = v2.y;

	bool visible = miny != midy || midy != maxy;

	if ( visible )
	{
		visible = ( 0 <= maxy && miny < Height );

		if ( visible )
		{
			LONG minx, maxx;
			MinMaxX( minx, maxx );

			visible = ( 0 <= maxx && minx < Width );

			if ( visible )
			{
				float minz, maxz;
				MinMaxZ( minz, maxz );
				visible = CGL::pActive->m_Near <= minz && maxz <= CGL::pActive->m_Far;

				if ( visible )
					visible = u0.x * ( u1.y - u2.y ) + u1.x * ( u2.y - u0.y ) + u2.x * ( u0.y - u1.y ) > 0;
			}
		}
	}

	if ( visible )
	{
		// precalculations.
		const LONG dy20 = v2.y - v0.y;
		const float dy20_inv = 1.0f / dy20;
		const float dx20 = ( v2.x - v0.x ) * dy20_inv;
		const float dz20 = ( v2.z - v0.z ) * dy20_inv;
		const float dtx20 = ( t2.x - t0.x ) * dy20_inv;
		const float dty20 = ( t2.y - t0.y ) * dy20_inv;

		// initialize variables.
		float x_main = ( float )v0.x;
		float z = v0.z;
		CVertex2D t = t0;

		unsigned scan_line = miny > 0 ? miny * CGL::C.GetWidth(): 0;

		// draw triangle.
		if ( miny == midy )
		{
			float x_side = ( float )v1.x;

			// calc dz and dt_tx.
			const float len_inv = 1.0f / ( v1.x - v0.x );
			const CVector2D dt_dx( ( t1.x - t0.x ) * len_inv, ( t1.y - t0.y ) * len_inv );
			const float dz = ( v1.z - v0.z ) * len_inv;

			const float dx21 = ( v2.x - v1.x ) / ( float )( v2.y - v1.y );

			if ( maxy >= Height ) maxy = Height - 1;

			for ( register LONG cur_y = miny; cur_y <= maxy; cur_y++ )
			{
				if ( cur_y >= 0 )
				{
					DrawHorzLine( pObserver, x_main, x_side, scan_line, z, dz, t, dt_dx );
					scan_line += CGL::C.GetWidth();
				}

				x_side += dx21;
				x_main += dx20, z += dz20, t += CVector2D( dtx20, dty20 );
			}
		}
		else if ( midy == maxy )
		{
			float x_side = ( float )v0.x;

			// calc dz and dt_tx.
			const float len_inv = 1.0f / ( v1.x - v2.x );
			const CVector2D dt_dx( ( t1.x - t2.x ) * len_inv, ( t1.y - t2.y ) * len_inv );
			const float dz = ( v1.z - v2.z ) * len_inv;

			const float dx10 = ( v1.x - v0.x ) / ( float )( v1.y - v0.y );

			if ( maxy >= Height ) maxy = Height - 1;

			for ( register LONG cur_y = miny; cur_y <= maxy; cur_y++ )
			{
				if ( cur_y >= 0 )
				{
					DrawHorzLine( pObserver, x_main, x_side, scan_line, z, dz, t, dt_dx );
					scan_line += CGL::C.GetWidth();
				}

				x_side += dx10;
				x_main += dx20, z += dz20, t += CVector2D( dtx20, dty20 );
			}
		}
		else
		{
			float x_side = ( float )v0.x;

			// calc dz and dt_tx.
			const LONG dy10 = v1.y - v0.y;
			const LONG start_x = v0.x + LONG( dy10 * dx20 );
			const float start_u = t0.x + dy10 * dtx20;
			const float start_v = t0.y + dy10 * dty20;
			const float start_z = v0.z + dy10 * dz20;
			const float len_inv = 1.0f / ( v1.x - start_x );
			const CVector2D dt_dx( ( t1.x - start_u ) * len_inv, ( t1.y - start_v ) * len_inv );
			const float dz = ( v1.z - start_z ) * len_inv;

			const float dx10 = ( v1.x - v0.x ) / ( float )dy10;
			const float dx21 = ( v2.x - v1.x ) / ( float )( v2.y - v1.y );

			if ( maxy >= Height ) maxy = Height - 1;

			for ( register LONG cur_y = miny; cur_y <= maxy; cur_y++ )
			{
				if ( cur_y >= 0 )
				{
					DrawHorzLine( pObserver, x_main, x_side, scan_line, z, dz, t, dt_dx );
					scan_line += CGL::C.GetWidth();
				}

				x_side += cur_y < midy ? dx10 : dx21;
				x_main += dx20, z += dz20, t += CVector2D( dtx20, dty20 );
			}
		}
	}
}

void CTriangle::MinMaxX( LONG & minx, LONG & maxx ) const
{
	const POINT3D & v0 = CGL::Screen[ m_index[ 0 ] ];
	const POINT3D & v1 = CGL::Screen[ m_index[ 1 ] ];
	const POINT3D & v2 = CGL::Screen[ m_index[ 2 ] ];

	maxx = minx = v0.x;
	if ( v1.x < minx ) minx = v1.x;
	else if ( v1.x > maxx ) maxx = v1.x;

	if ( v2.x < minx ) minx = v2.x;
	else if ( v2.x > maxx ) maxx = v2.x;
}

void CTriangle::MinMaxZ( float & minz, float & maxz ) const
{
	const POINT3D & v0 = CGL::Screen[ m_index[ 0 ] ];
	const POINT3D & v1 = CGL::Screen[ m_index[ 1 ] ];
	const POINT3D & v2 = CGL::Screen[ m_index[ 2 ] ];

	maxz = minz = v0.z;
	if ( v1.z < minz ) minz = v1.z;
	else if ( v1.z > maxz ) maxz = v1.z;

	if ( v2.z < minz ) minz = v2.z;
	else if ( v2.z > maxz ) maxz = v2.z;
}

void CTriangle::SortIndicesByY( unsigned & i0, unsigned & i1, unsigned & i2 ) const
{
	static unsigned temp;

	if ( CGL::Screen[ i0 ].y > CGL::Screen[ i1 ].y )
		temp = i0, i0 = i1, i1 = temp;

	if ( CGL::Screen[ i1 ].y > CGL::Screen[ i2 ].y )
		temp = i1, i1 = i2, i2 = temp;

	if ( CGL::Screen[ i0 ].y > CGL::Screen[ i1 ].y )
		temp = i0, i0 = i1, i1 = temp;
}


void CVisibleTriList::DrawAll( const CObserver * const pObserver ) const
{
	for ( register unsigned i = 0; i < m_counter; i++ )
		ptr[ i ]->Draw( pObserver );
}