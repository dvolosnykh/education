#pragma once


class CLine
{
public:
	enum { ONE, TWO };
	CVertex2D dot[ 2 ];

	CVector2D m_Normal;
	float m_C;

public:
	CLine() {};
	CLine( const CVertex2D & p1, const CVertex2D & p2 ) { Set( p1, p2 ); };
	~CLine() {};

	void Set( const CVertex2D & p1, const CVertex2D & p2 ) { dot[ ONE ] = p1, dot[ TWO ] = p2; };
	bool IsValid() const { return ( Length() > 0 ); };
	float Length() const;
	bool IsVertical() const { return ( m_Normal.x == 0 ); };
	bool IsHorizontal() const { return ( m_Normal.y == 0 ); };
	void FindCoeffs();
	float SignDistance( const CVertex2D & point ) const;
};

