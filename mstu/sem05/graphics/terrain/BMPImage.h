#ifndef BMPIMAGE_H
#define BMPIMAGE_H


#include "Image.h"
#include "BMPHeader.h"


#pragma warning( push )
#pragma warning( disable : 4250 )


class CBMPImage : public CImage, public CBMPHeader
{
public:
	CBMPImage() {};
	virtual ~CBMPImage() {};

	LONG GetHeight() const { return CBMPHeader::GetHeight(); };
	LONG GetWidth() const { return CBMPHeader::GetWidth(); };
};

#pragma warning( pop )

#endif