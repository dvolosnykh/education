#include "stdafx.h"

#include "Image.h"


bool CImage::LoadImage( FILE * const pFile )
{
	bool isOK = LoadHeader( pFile ) && IsSupported();

	if ( isOK )
	{
		if ( !IsEmpty() )	// Release old stuff.
			Free();

		// Set file-pointer to color-array.
		fseek( pFile, GetOffBits(), SEEK_SET );

		isOK = LoadData( pFile );
	}

	return ( isOK );
}

bool CImage::LoadImage( LPCTSTR szFileName )
{
	FILE * pFile = _tfopen( szFileName, "r+bt" );

	bool isOK = pFile != NULL;
	if ( isOK )
	{
		isOK = LoadImage( pFile );
		fclose( pFile );
	}

  return ( isOK );
}

bool CImage::LoadData( FILE * const pFile, const bool flip /* = false */ )
{
	// Allocate space for the image.
	const DWORD linesize = ( GetBitDepth() >> 3 ) * GetWidth();
	Alloc( GetHeight() * linesize, false );

	if ( !IsEmpty() )
	{
		bool read_OK;

		if ( flip )
		{
			// Read the image in.
			unsigned left = GetByteSize();
			for ( ;	left > 0
				&&	fread( ptr + left - linesize, linesize, 1, pFile ) == 1;
				left -= linesize );

			read_OK = left == 0;
		}
		else
			read_OK = fread( ptr, GetByteSize(), 1, pFile ) == 1;

		if ( !read_OK )
			Free();
	}

 	return ( !IsEmpty() );
}