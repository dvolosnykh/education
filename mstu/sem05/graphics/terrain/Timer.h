#ifndef TIMER_H
#define TIMER_H


#define ONE_SECOND	1000


class CTimer 
{
private:
	bool valid;
	DWORD m_StartTime;
	DWORD m_ElapsedTime;
	DWORD m_StopTime;

	DWORD m_LastNote;
	DWORD m_LastInterval;

	bool paused;
	DWORD m_PauseTime;
	DWORD m_PauseInterval;

	unsigned m_FrameCount;

public:
	CTimer( const bool start = false ) : valid( false ) { if ( start ) Start(); };
	~CTimer() {};

	DWORD GetStartTime() const { return ( m_StartTime ); };
	DWORD GetElapsedTime() const { return ( m_ElapsedTime ); };
	DWORD GetLastNote() const { return ( m_LastNote ); };
	DWORD GetLastInterval() const { return ( m_LastInterval ); };
	unsigned GetFrameCount() const { return ( m_FrameCount ); };

	DWORD Start();
	DWORD Stop();
	DWORD MakeNote();
	void Pause();
	void Resume();

protected:
	DWORD GetTick() const { return GetTickCount(); };
};

#endif