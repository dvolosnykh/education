#ifndef MATHFUNC_H
#define MATHFUNC_H


typedef CTDArray< BYTE > CBytesArray;


float linear( const float a, const float b, const float t );
float bilinear( const float v00, const float v10, const float v01,
	const float v11, const float kx, const float ky );
void distort( BYTE & old, const unsigned distortion );
float degree_to_rad( const float degrees );
float rad_to_degree( const float rads );
float ScaleToElapsedTime( const float value, const DWORD dwTimeElapsed );
long round( const float x );


bool IsSet( const BYTE & byte, const unsigned index );
bool IsSet( const CBytesArray & field, const unsigned index );

void Set( BYTE & byte, const unsigned bit );
void Set( CBytesArray & field, const unsigned index );

bool IsClear( const BYTE & byte, const unsigned bit );
bool IsClear( const CBytesArray & field, const unsigned index );

void Clear( BYTE & byte, const unsigned bit );
void Clear( CBytesArray & field, const unsigned index );

void Invert( BYTE & byte, const unsigned bit );
void Invert( CBytesArray & field, const unsigned index );

#endif