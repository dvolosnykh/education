#include "stdafx.h"

#include "ProgressWindow.h"


#ifdef DEBUG_INFO
static LONG minz, maxz;
static bool first;
#endif


BITMAPINFO	CGL::m_bmp;
HDC			CGL::m_hDC = NULL;
int			CGL::m_OldTextMode;
COLORREF	CGL::m_OldTextColor;

LONG	CGL::m_BarHeight = BAR_HEIGHT;
engine_param_t	CGL::m_params;

IndexArray		CGL::Frame;
VertexArray		CGL::Vertex;
ScreenArray		CGL::Screen;
VectorArray		CGL::Normal;
LightArray		CGL::Light;
TextureArray	CGL::TexCoord;
CVisibleTriList	CGL::VisTriList;

int				CGL::window_mode;
int				CGL::draw_mode;
DepthBuffer		CGL::Z;
ColorBuffer		CGL::C;

CHeightmap		CGL::Heightmap;
CLandscape		CGL::Landscape;
CHeavenlyBody			CGL::HeavenlyBody;
CCrosshair		CGL::Crosshair;
CObserver		CGL::Observer[ OBSERVERS_NUM ];
CObserver *		CGL::pActive;
CObserver *		CGL::pMain;

unsigned		CGL::FrameIndex = 1;


bool CHeightmap::Load( LPCTSTR pszFileName )
{
	CTexture hmap_tex;
	bool isOK = hmap_tex.LoadTexture( pszFileName );

	if ( isOK )
	{
		Alloc( hmap_tex.GetHeight(), hmap_tex.GetWidth(), false );

		for ( LONG i = 0, index = 0; i < height; i++ )
			for ( LONG j = 0; j < width; j++, index++ )
				ptr[ index ] = hmap_tex[ index ].rgbRed;
	}

	return ( isOK );
}


void CGL::Draw( const unsigned fps )
{
	switch ( draw_mode )
	{
	case DRAW_SCENES:	DrawScenes( fps );	break;
	case DRAW_TEXTURE:	DrawTexture();		break;
	}
}

unsigned CGL::TransformWorld( const CObserver * const pObserver )
{
#ifdef DEBUG_INFO
	first = true;
#endif

	const LONG width_2 = pObserver->width >> 1;
	const LONG height_2 = pObserver->height >> 1;

	unsigned vertices = 0;
	for ( register unsigned i = 0; i < VisTriList.GetTriNum(); i++ )
		for ( register unsigned j = 0; j < 3; j++ )
		{
			const register unsigned index = VisTriList[ i ]->m_index[ j ];

			if ( Frame[ index ] != FrameIndex )
			{
				TransformVertex( Screen[ index ], Vertex[ index ], pObserver, height_2, width_2 );
				Frame[ index ] = FrameIndex;
				vertices++;
			}
		}

	return ( vertices );
}

void CGL::TransformVertex( POINT3D & screen, const CVertex3D & vertex, const CObserver * const pObserver, const LONG height_2, const LONG width_2 )
{
	CVertex3D point = pObserver->view * vertex;
	const float w = 1.0f / ( screen.z = point.z );
	point.x *= w, point.y *= w;

#ifndef USE_OPENGL_PROJECTION
	point.x *= pObserver->proj[ 0 ][ 0 ];
	point.y *= pObserver->proj[ 1 ][ 1 ];
#endif

	screen.x = LONG( ( 1.0f + point.x ) * width_2 );
	screen.y = LONG( ( 1.0f - point.y ) * height_2 );

#ifdef DEBUG_INFO
	if ( g_param.ShowStats )
	{
		if ( first )
			minz = maxz = ( LONG )screen.z, first = false;
		else if ( screen.z < minz )
			minz = ( LONG )screen.z;
		else if ( screen.z > maxz )
			maxz = ( LONG )screen.z;
	}
#endif
}

bool CGL::NeedSavePoint( const CObserver * const pObserver, const float z, const unsigned index )
{
	return ( z < CGL::Z[ index ] && pObserver->m_Near <= z && z <= pObserver->m_Far );
}

void CGL::SavePoint( const float z, const unsigned index, CVertex2D t )
{
	const CTexture & tex =
		g_param.Shadows || g_param.SimpleLighting ?
		Landscape.m_LightedTexture :
		Landscape.m_Texture;

	long u = long( t.x );
	long v = long( t.y );

	if ( u < 0 ) u = 0;
	else if ( u >= tex.GetHeight() ) u = tex.GetWidth() - 1;

	if ( v < 0 ) v = 0;
	else if ( v >= tex.GetHeight() ) v = tex.GetHeight() - 1;

	Z[ index ] = z;
	C[ index ] = tex[ v * tex.GetWidth() + u ];
}

void CGL::InitHeavenlyBody()
{
	const CVertex3D center = Landscape.GetCenter();
	const float width = Landscape.GetWidth();
	const float height = Landscape.GetHeight();
	const float length = Landscape.GetLength();
	const float average = ( width + length ) * 0.5f;

	HeavenlyBody.m_dir.x = float( g_param.light_dir.end.x - g_param.light_dir.start.x );
	HeavenlyBody.m_dir.z = float( g_param.light_dir.end.y - g_param.light_dir.start.y );
	const float len_xz = ( float )hypot( HeavenlyBody.m_dir.x, HeavenlyBody.m_dir.z );
	HeavenlyBody.m_dir.y = -len_xz * tanf( degree_to_rad( g_param.light_angle ) );
	HeavenlyBody.m_dir.Normalize();
	HeavenlyBody.m_dir *= 40.0f * average;

	HeavenlyBody.m_pos = center - HeavenlyBody.m_dir;
}

void CGL::DisplaceHeavenlyBody( const float angle_x, const float angle_y )
{
	if ( angle_x != 0.0f )
	{
		POINT & start = g_param.light_dir.start;
		POINT & end = g_param.light_dir.end;

		const float cosine = cosf( degree_to_rad( angle_x ) );
		const float sine = sinf( degree_to_rad( angle_x ) );
		start.x -= end.x, start.y -= end.y;
		const POINT new_start =
		{
			LONG( start.x * cosine + start.y * sine + end.x ),
			LONG( -start.x * sine + start.y * cosine + end.y )
		};

		start = new_start;
	}

	g_param.light_angle += angle_y;

	if ( angle_x != 0.0f || angle_y != 0.0f )
	{
		InitHeavenlyBody();
		ProcessLighting( false );
		ApplyLightingToTexture( false );

		switch ( draw_mode )
		{
		case DRAW_SCENES:	pActive->updated = true;							break;
		case DRAW_TEXTURE:	if ( CTexMode::shadows ) CTexMode::updated = true;	break;
		};
	}
}

bool CGL::InitEngine()
{
	bool isOK = true;
	CProgressWindow::Create( g_param.szLogoBitmap );

	// *** Display ***
	Z.Alloc( g_param.ResHeight, g_param.ResWidth, false );
	C.Alloc( g_param.ResHeight, g_param.ResWidth, false );
	pMain = pActive = &Observer[ 0 ];
	SetWindowMode( WND_FULL );
	SetDrawMode( DRAW_SCENES );

	if ( !( isOK = Crosshair.LoadTexture( g_param.szCrosshairTexture ) ) )		// *** Cursor ***
	{
		MessageBox( CApp::m_hWnd, TEXT( "�� ��������� �������� ��� �������." ), 
			TEXT( "��������������" ), MB_ICONERROR);
	}
	else if ( !( isOK = HeavenlyBody.LoadTextures( g_param.szHeavenlyBodySearchPath ) ) )		// *** HeavenlyBody ***
	{
		MessageBox( CApp::m_hWnd, TEXT( "�� ��������� �������� ��� ������." ), 
			TEXT( "������" ), MB_ICONERROR);
	}
	else if ( !( isOK = Landscape.LoadTerrain( m_params ) ) )	// *** Landscape ***
	{
		MessageBox( CApp::m_hWnd, TEXT( "�������� �� ������." ), 
			TEXT( "������" ), MB_ICONERROR);
	}

	if ( isOK )
	{
		InitObservers();
		CTexMode::Init();
	}

	CProgressWindow::Destroy();
	return ( isOK );
}

void CGL::ProcessLighting( const bool show_progress /* = true */ )
{
	if ( show_progress )
		CProgressWindow::SetTask( TEXT( "���������� ��������� ������ (%i%%)..." ) );

	static const RGBQUAD sunny_day = { 255, 255, 255, UCHAR_MAX };
	static const RGBQUAD sunset = { 100, 100, 255, UCHAR_MAX };
	static const RGBQUAD dead_night = { 200, 92, 92, UCHAR_MAX };
	static RGBQUAD light_color;

	switch ( g_param.DayTime )
	{
	case IDC_SUNNY_DAY:		light_color = sunny_day;	break;
	case IDC_SUNSET:		light_color = sunset;		break;
	case IDC_MOON_NIGHT:	light_color = dead_night;	break;
	}

	const CVector3D light_dir = HeavenlyBody.m_dir.GetNormalized();

	for ( register LONG i = 0; i < Vertex.GetSize(); i++ )
	{
		Light[ i ] = CalcVertexLighting( i, light_color, light_dir );

		if ( show_progress )
			CProgressWindow::SetProgress( 100 * ( i + 1 ) / Vertex.GetSize() );
	}
}

/*
void CGL::ApplyLightingToTextureRect( CTexture & dest_tex, const CTexture & src_tex, const RECT rect )
{
	const CTexture & src_tex = Landscape.m_Texture;
	CTexture & dest_tex = Landscape.m_LightedTexture;


}
*/
void CGL::ApplyLightingToTexture( const bool show_progress /* = true */ )
{
	if ( show_progress )
		CProgressWindow::SetTask( TEXT( "���������� ��������� � �������� ��������� (%i%%)..." ) );

	const CTexture & src_tex = Landscape.m_Texture;
	CTexture & dest_tex = Landscape.m_LightedTexture;
	dest_tex.Alloc( src_tex.GetHeight(), src_tex.GetWidth(), false );
/*
	for ( register LONG i = 0; i < Light.GetHeight() - 1; i++ )
	{
		for ( register LONG j = 0; j < Light.GetWidth() - 1; j++ )
		{
		}

		if ( show_porgress )
			CProgressWindow::SetProgress( 100 * ( i + 1 ) / .GetHeight() );
	}
*/

	const register float step_h = ( Light.GetHeight() - 1 ) / ( float )( src_tex.GetHeight() - 1 );
	const register float step_w = ( Light.GetWidth() - 1 ) / ( float )( src_tex.GetWidth() - 1 );

	{
	register LONG i, j;
	register unsigned index = 0;
	float h, w;
	for ( i = 0, h = 0.0f; i < src_tex.GetHeight(); i++, h += step_h )
	{
		LONG h0 = ( LONG )floorf( h );
		LONG h1 = ( LONG )ceilf( h );

		if ( h0 >= Light.GetHeight() ) h1 = h0 = Light.GetHeight() - 1;
		else if ( h1 >= Light.GetHeight() ) h1 = Light.GetHeight() - 1;

		const register float kh = h - h0;

		for ( j = 0, w = 0.0f; j < src_tex.GetWidth(); j++, w += step_w, index++ )
		{
			LONG w0 = ( LONG )floorf( w );
			LONG w1 = ( LONG )ceilf( w );

			if ( w0 >= Light.GetWidth() ) w1 = w0 = Light.GetWidth() - 1;
			else if ( w1 >= Light.GetWidth() ) w1 = Light.GetWidth() - 1;

			const register float kw = w - w0;

			const RGBQUAD & light00 = Light[ h0 * Light.GetWidth() + w0 ];
			const RGBQUAD & light01 = Light[ h0 * Light.GetWidth() + w1 ];
			const RGBQUAD & light10 = Light[ h1 * Light.GetWidth() + w0 ];
			const RGBQUAD & light11 = Light[ h1 * Light.GetWidth() + w1 ];

			static RGBQUAD light;
			register float value1, value2;
			value1 = linear( light00.rgbRed, light10.rgbRed, kh );
			value2 = linear( light01.rgbRed, light11.rgbRed, kh );
			light.rgbRed = ( BYTE )linear( value1, value2, kw );
			value1 = linear( light00.rgbGreen, light10.rgbGreen, kh );
			value2 = linear( light01.rgbGreen, light11.rgbGreen, kh );
			light.rgbGreen = ( BYTE )linear( value1, value2, kw );
			value1 = linear( light00.rgbBlue, light10.rgbBlue, kh );
			value2 = linear( light01.rgbBlue, light11.rgbBlue, kh );
			light.rgbBlue = ( BYTE )linear( value1, value2, kw );
			light.rgbReserved = UCHAR_MAX;

            dest_tex[ index ].rgbRed   = Lighting( src_tex[ index ].rgbRed,   light.rgbRed );
			dest_tex[ index ].rgbGreen = Lighting( src_tex[ index ].rgbGreen, light.rgbGreen );
			dest_tex[ index ].rgbBlue  = Lighting( src_tex[ index ].rgbBlue,  light.rgbBlue );
			dest_tex[ index ].rgbReserved = UCHAR_MAX;
		}

		if ( show_progress )
			CProgressWindow::SetProgress( 100 * ( i + 1 ) / src_tex.GetHeight() );
	}
	}
}

RGBQUAD CGL::CalcVertexLighting( const unsigned index, const RGBQUAD & light, const CVector3D & light_dir )
{
	WORD red = g_param.Ambient, green = g_param.Ambient, blue = g_param.Ambient;

	// apply simple lighting.
	if ( g_param.SimpleLighting )
	{
		const float cosine = Normal[ index ] * light_dir;

		if ( cosine > 0.0f )
		{
			if ( cosine < 1.0f )
			{
				red += WORD( cosine * light.rgbRed );
				green += WORD( cosine * light.rgbGreen );
				blue += WORD( cosine * light.rgbBlue );
			}
			else
			{
				red += light.rgbRed;
				green += light.rgbGreen;
				blue += light.rgbBlue;
			}
		}
	}
	else
	{
		red += light.rgbRed;
		green += light.rgbGreen;
		blue += light.rgbBlue;
	}

	// cast shodows.
	if ( g_param.Shadows )
	{
		CVertex3D vertex = Vertex[ index ];
		vertex.y += g_param.GridStep * 0.1f * SMALL_VALUE;

		if ( Landscape.CheckIntersection( CRay( vertex, vertex - HeavenlyBody.m_dir ) ) )
			red >>= 1, green >>= 1, blue >>= 1;
	}

	if ( red > UCHAR_MAX ) red = UCHAR_MAX;
	if ( green > UCHAR_MAX ) green = UCHAR_MAX;
	if ( blue > UCHAR_MAX ) blue = UCHAR_MAX;

	const RGBQUAD color = { BYTE( blue ), BYTE( green ), BYTE( red ), UCHAR_MAX };
	return ( color );
}

static bool NeedChangeResolution( const DWORD res_width, const DWORD res_height, const DWORD bits )
{
	DEVMODE dm;
	EnumDisplaySettings( NULL, ENUM_CURRENT_SETTINGS, &dm );

	return ( dm.dmPelsWidth != res_width || dm.dmPelsHeight != res_height || dm.dmBitsPerPel != bits );
}

static bool ChangeResolution( const DWORD res_width, const DWORD res_height, const DWORD bits )
{
	DEVMODE dm;
	dm.dmSize = sizeof( DEVMODE );
	dm.dmPelsWidth = res_width;
	dm.dmPelsHeight = res_height;
	dm.dmBitsPerPel = bits;
	dm.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

	return ChangeDisplaySettings( &dm, CDS_FULLSCREEN ) == DISP_CHANGE_SUCCESSFUL;
}

bool CGL::InitDisplay( const DWORD res_width, const DWORD res_height, const bool fullscreen )
{
	static const DWORD bits = sizeof( RGBQUAD ) << 3;
	static LPCTSTR text = NULL;

	memset( &m_bmp, 0, sizeof( BITMAPINFO ) );
	m_bmp.bmiHeader.biSize = sizeof( BITMAPINFOHEADER );
	m_bmp.bmiHeader.biWidth = res_width;
	m_bmp.bmiHeader.biHeight = -( LONG )res_height;
	m_bmp.bmiHeader.biPlanes = 1;
	m_bmp.bmiHeader.biBitCount = bits;

	bool isOK = true;
	if ( fullscreen && NeedChangeResolution( res_width, res_height, bits ) )
	{
		isOK = ChangeResolution( res_width, res_height, bits );
		if ( !isOK )
			text = TEXT( "������ ��� ��������� ���������� �������." );
	}

	if ( isOK )
	{
		m_hDC = GetDC( CApp::m_hWnd );
		isOK = m_hDC != NULL;
	}

	if ( isOK )
	{
		m_OldTextMode = SetBkMode( m_hDC, TRANSPARENT );
		m_OldTextColor = SetTextColor( m_hDC, RGB( UCHAR_MAX, UCHAR_MAX, UCHAR_MAX ) );
	}
	else
	{
		text = m_hDC == NULL ? TEXT( "�� ������� �������� �������." ) : text;
		MessageBox( CApp::m_hWnd, text, TEXT( "������" ), MB_OK | MB_ICONERROR );
	}

	return ( isOK );
}

void CGL::DeinitDisplay()
{
	SetTextColor( m_hDC, m_OldTextColor );
	SetBkMode( m_hDC, m_OldTextMode );

	ChangeDisplaySettings( NULL, 0 );
	ReleaseDC( CApp::m_hWnd, m_hDC );	m_hDC = NULL;
}

void CGL::RefreshDisplay()
{
	SetWindowMode( window_mode );
	SetDrawMode( draw_mode );
}

void CGL::ClearDisplay( const CObserver * const pObserver /* = NULL */ )
{
	// max. - 239
	static const RGBQUAD default_color = { 0, 0, 0, UCHAR_MAX };
	static const RGBQUAD sunny_day = { 239, 207, 183, UCHAR_MAX };
//	static const RGBQUAD sunset = { 239, 207, 224, UCHAR_MAX };
	static const RGBQUAD sunset = { 190, 190, 255, UCHAR_MAX };
	static const RGBQUAD dead_night = { 16, 16, 16, UCHAR_MAX };
	static const RGBQUAD active_color = { 16, 16, 16, UCHAR_MAX };
	static RGBQUAD main_color;

	switch ( g_param.DayTime )
	{
	case IDC_SUNNY_DAY:		main_color = sunny_day;		break;
	case IDC_SUNSET:		main_color = sunset;		break;
	case IDC_MOON_NIGHT:	main_color = dead_night;	break;
	}

	if ( pObserver != NULL )
	{
		RGBQUAD color = pObserver == pMain ? main_color : default_color;

		switch ( window_mode )
		{
		case WND_FULL:
			C.Reset( color );
			Z.Reset( FLT_MAX );
			break;

		case WND_SPLIT_4:
		case WND_SPLIT_1_3:
			if ( pObserver == pActive && !g_param.DrawCrosshair )
			{
				color.rgbRed += active_color.rgbRed;
				color.rgbGreen += active_color.rgbGreen;
				color.rgbBlue += active_color.rgbBlue;
			}

			C.Reset( 0, 0, pObserver->height, pObserver->width, color );
			Z.Reset( 0, 0, pObserver->height, pObserver->width, FLT_MAX );

			break;
		}
	}
	else
	{
		C.Reset( default_color );
	}
}

void CGL::UpdateDisplay( const CObserver * const pObserver /* = NULL */ )
{
	if ( pObserver != NULL )
		UpdateDC( pObserver->min.x, pObserver->min.y, pObserver->width, pObserver->height );
	else
		UpdateDC( 0, 0, C.GetWidth(), C.GetHeight() );

	FrameIndex++;
}

void CGL::ToggleWindowMode( const int index /* = WND_MODE_NEXT */ )
{
	const int new_mode = index >= 0 ? index : ( window_mode + 1 ) % WND_MODES_NUM;
	if ( new_mode != window_mode ) SetWindowMode( new_mode );
}

void CGL::ToggleDrawMode( const int index /* = DRAW_MODE_NEXT */ )
{
	int new_mode = index >= 0 ? index : ( draw_mode + 1 ) % DRAW_MODES_NUM;

	if ( new_mode != draw_mode )
		SetDrawMode( new_mode );
}

void CGL::SetWindowMode( const int new_mode )
{
	LONG Height = C.GetHeight();
	if ( g_param.ShowStats ) Height -= m_BarHeight;
	const LONG Width = C.GetWidth();

	switch ( window_mode = new_mode )
	{
	case WND_FULL:
		for ( unsigned i = 0; i < OBSERVERS_NUM; i++ )
		{
			Observer[ i ].min.x = 0,		Observer[ i ].min.y = 0;
			Observer[ i ].width = Width,	Observer[ i ].height = Height;

			const float aspect = Observer[ i ].width / ( float )Observer[ i ].height;
			Observer[ i ].Perspective( g_param.FOV, aspect, g_param.NearPlane, g_param.FarPlane );
		}

		pActive->updated = true;

		break;

	case WND_SPLIT_4:
		{
		const LONG height_2 = Height >> 1;
		const LONG width_2 = Width >> 1;

		Observer[ 0 ].min.x = 0,		Observer[ 0 ].min.y = 0;
		Observer[ 1 ].min.x = width_2,	Observer[ 1 ].min.y = 0;
		Observer[ 2 ].min.x = 0,		Observer[ 2 ].min.y = height_2;
		Observer[ 3 ].min.x = width_2,	Observer[ 3 ].min.y = height_2;

		Observer[ 0 ].width = width_2,			Observer[ 0 ].height = height_2;
		Observer[ 1 ].width = Width - width_2,	Observer[ 1 ].height = height_2;
		Observer[ 2 ].width = width_2,			Observer[ 2 ].height = Height - height_2;
		Observer[ 3 ].width = Width - width_2,	Observer[ 3 ].height = Height - height_2;

		for ( unsigned i = 0; i < OBSERVERS_NUM; i++ )
		{
			const float aspect = Observer[ i ].width / ( float )Observer[ i ].height;
			Observer[ i ].Perspective( g_param.FOV, aspect, g_param.NearPlane, g_param.FarPlane );
		}

		AllObserversUpdated();
		}
		break;

	case WND_SPLIT_1_3:
		{
		const LONG width_4 = Width >> 2;
		const LONG height_3 = Height / 3;

		Observer[ 0 ].min.x = 0,				Observer[ 0 ].min.y = 0;
		Observer[ 1 ].min.x = Width - width_4,	Observer[ 1 ].min.y = 0;
		Observer[ 2 ].min.x = Width - width_4,	Observer[ 2 ].min.y = height_3;
		Observer[ 3 ].min.x = Width - width_4,	Observer[ 3 ].min.y = height_3 << 1;

		Observer[ 0 ].width = Width - width_4,	Observer[ 0 ].height = Height;
		Observer[ 1 ].width = width_4,			Observer[ 1 ].height = height_3;
		Observer[ 2 ].width = width_4,			Observer[ 2 ].height = height_3;
		Observer[ 3 ].width = width_4,			Observer[ 3 ].height = Height - ( height_3 << 1 );

		for ( unsigned i = 0; i < OBSERVERS_NUM; i++ )
		{
			const float aspect = Observer[ i ].width / ( float )Observer[ i ].height;
			Observer[ i ].Perspective( g_param.FOV, aspect, g_param.NearPlane, g_param.FarPlane );
		}

		AllObserversUpdated();
		}
		break;
	}
}

void CGL::SetDrawMode( const int new_mode )
{
	switch ( draw_mode = new_mode )
	{
	case DRAW_SCENES:	AllObserversUpdated();		break;
	case DRAW_TEXTURE:	CTexMode::updated = true;	break;
	}
}

BYTE CGL::Lighting( const BYTE & color, const BYTE & light )
{
	return ( color * light >> 8 );
}


void CGL::UpdateDC( const LONG dest_x, const LONG dest_y, const LONG width, const LONG height )
{
	const LONG StatusBar = draw_mode == DRAW_SCENES && g_param.ShowStats ? m_BarHeight : 0;

	SetDIBitsToDevice( m_hDC, dest_x, StatusBar + dest_y,
			( DWORD )width, ( DWORD )height, 0, 0, 0, ( UINT )height,
			C.GetPtr(), &m_bmp, DIB_PAL_COLORS );
}

void CGL::ShowText( int pos_x, int pos_y, LPCTSTR text )
{
	TextOut( m_hDC, pos_x, pos_y, text, ( int )_tcslen( text ) );
}