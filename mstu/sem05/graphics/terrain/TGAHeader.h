#ifndef TGAHEADER_H
#define TGAHEADER_H


#include "Header.h"


#define CMT_ABSENT			0
#define CMT_PRESENT			1

#define IT_ABSENT			0
#define IT_CLRTABLE			1
#define IT_TRUECLR			2
#define IT_MONOCHR			3
#define IT_CLRTABLE_COMP	9
#define IT_TRUECLR_COMP		10
#define IT_MONOCHR_COMP		11


struct TGAFILEHEADER
{
	BYTE IDLength;							// ������ ���� ID-�����������:	�� 0 �� 255 (������ ������������������ ����).
	BYTE ColorMapType;						// ��� ���� �������:			0 - ���, 1 - ����, 2 - 255 - ����, �� � ����������� �������.
	BYTE ImageType;							// ��� ���� �����������:		0 = ����������� ���, 1-3 = ��� ������, 9-11 = ������.
	BYTE lColorMapStart, hColorMapStart;	// ������ �������:				�������� ������� �������� � ������� ������.
	BYTE lColorMapLength, hColorMapLength;	// ����� �������:				���������� ��������� �������� �������.
	BYTE ColorMapDepth;						// ������� ��������� �������:	���������� ����� � �������� �������.
	BYTE lXoffset, hXoffset;				// �������� �� �����������:		���������� �������� ����������� �� X.
	BYTE lYoffset, hYoffset;				// �������� �� ���������:		���������� �������� ����������� �� Y.
	BYTE lWidth, hWidth;					// ������:						������ ����������� � ��������.
	BYTE lHeight, hHeight;					// ������:						������ ����������� � ��������.
	BYTE BitDepth;							// ������ �������:				���������� ��� � �������� - 8, 16, 24 ��� 32.
	BYTE ImageDesc;							// ���������� �����������:		���� 0-3=, ���� 4-5 ����������.
};


class CTGAHeader : virtual public CHeader
{
private:
	TGAFILEHEADER hdr;

public:
	CTGAHeader() {};
	~CTGAHeader() {};

	bool LoadHeader( FILE * const pFile );
	bool LoadHeader( LPCTSTR szFileName );
	bool IsSupported() const;

	BYTE GetIDLength() const { return ( hdr.IDLength ); };
	BYTE GetColorMapType() const { return ( hdr.ColorMapType ); };
	BYTE GetImageType() const { return ( hdr.ImageType ); };
	WORD GetColorMapStart() const { return MAKEWORD( hdr.lColorMapStart, hdr.hColorMapStart ); };
	WORD GetColorMapLength() const { return MAKEWORD( hdr.lColorMapLength, hdr.hColorMapLength ); };
	BYTE GetColorMapDepth() const { return ( hdr.ColorMapDepth ); };
	WORD GetXoffset() const { return MAKEWORD( hdr.lXoffset, hdr.hXoffset ); };
	WORD GetYoffset() const { return MAKEWORD( hdr.lYoffset, hdr.hYoffset ); };
	LONG GetWidth() const { return MAKEWORD( hdr.lWidth, hdr.hWidth ); };
	LONG GetHeight() const { return MAKEWORD( hdr.lHeight, hdr.hHeight ); };
	WORD GetBitDepth() const { return ( hdr.BitDepth ); };
	BYTE GetImageDesc() const { return ( hdr.ImageDesc ); };
	DWORD GetOffBits() const { return ( sizeof( hdr ) + hdr.IDLength ); };

	void SetIDLength( const BYTE value ) { hdr.IDLength = value; };
	void SetColorMapType( const BYTE value ) { hdr.ColorMapType = value; };
	void SetImageType( const BYTE value ) { hdr.ImageType = value; };
	void SetColorMapStart( const WORD value ) { hdr.lColorMapStart = LOBYTE( value ), hdr.hColorMapStart = HIBYTE( value ); };
	void SetColorMapLength( const WORD value ) { hdr.lColorMapLength = LOBYTE( value ), hdr.hColorMapLength = HIBYTE( value ); };
	void SetColorMapDepth( const BYTE value ) { hdr.ColorMapDepth = value; };
	void SetXoffset( const WORD value ) { hdr.lXoffset = LOBYTE( value ), hdr.hXoffset = HIBYTE( value ); };
	void SetYoffset( const WORD value ) { hdr.lYoffset = LOBYTE( value ), hdr.hXoffset = HIBYTE( value ); };
	void SetWidth( const WORD value ) { hdr.lWidth = LOBYTE( value ), hdr.hWidth = HIBYTE( value ); };
	void SetHeight( const WORD value ) { hdr.lHeight = LOBYTE( value ), hdr.hHeight = HIBYTE( value ); };
	void SetBitDepth( const BYTE value ) { hdr.BitDepth = value; };
	void SetImageDesc( const BYTE value ) { hdr.ImageDesc = value; };
};

#endif