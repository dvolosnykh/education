#ifndef TGAIMAGE_H
#define TGAIMAGE_H


#include "Image.h"
#include "TGAHeader.h"


#pragma warning( push )
#pragma warning( disable : 4250 )


class CTGAImage : public CImage, public CTGAHeader
{
public:
	CTGAImage() {};
	virtual ~CTGAImage() {};

	LONG GetHeight() const { return CTGAHeader::GetHeight(); };
	LONG GetWidth() const { return CTGAHeader::GetWidth(); };
};


#pragma warning( pop )

#endif