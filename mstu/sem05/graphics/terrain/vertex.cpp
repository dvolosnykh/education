#include "stdafx.h"

//==================== 1D-vertex ====================

CVertex::CVertex( const CVector & v )
{
	Set( v.x );
}

CVertex::CVertex( const CVertex2D & init )
{
	Set( init.x );
}

inline CVector CVertex::operator - ( const CVertex v ) const
{
	return CVector( x - v.x );
}

inline float CVertex::operator * ( const CVector v ) const
{
	return v.ScalarMult( *this );
}

//==================== 2D-vertex ====================

CVertex2D::CVertex2D( const CVector2D & v )
{
	Set( v.x, v.y );
}

CVertex2D::CVertex2D( const CVertex3D & init )
{
	Set( init.x, init.y );
}

inline float CVertex2D::Distance( const CVertex2D v ) const
{
	const float dx = x - v.x;
	const float dy = y - v.y;

	return sqrtf( dx * dx + dy * dy );
}

inline CVector2D CVertex2D::operator - ( const CVertex2D v ) const
{
	return CVector2D( x - v.x, y - v.y );
}

inline float CVertex2D::operator * ( const CVector2D v ) const
{
	return v.ScalarMult( *this );
}

//==================== 3D-vertex ====================

CVertex3D::CVertex3D( const CVector3D & v )
{
	Set( v.x, v.y, v.z );
}

float CVertex3D::Distance( const CVertex3D v ) const
{
	const float dx = x - v.x;
	const float dy = y - v.y;
	const float dz = z - v.z;

	return sqrtf( dx * dx + dy * dy + dz * dz );
}

CVector3D CVertex3D::operator - ( const CVertex3D v ) const
{
	return CVertex3D( x - v.x, y - v.y, z - v.z );
}

inline float CVertex3D::operator * ( const CVector3D v ) const
{
	return v.ScalarMult( *this );
}

//==================== 4D-vertex ====================

CVertex4D::CVertex4D( const CVertex3D & v )
{
	Set( v.x, v.y, v.z, 1.0f );
}

CVertex3D CVertex4D::Unify()
{
	w = 1.0f / w;
	x *= w, y *= w, z *= w,	w = 1.0f;
	return CVertex3D( x, y, z ); 
}