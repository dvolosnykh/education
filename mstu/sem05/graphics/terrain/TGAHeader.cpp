#include "stdafx.h"

#include "TGAHeader.h"


bool CTGAHeader::LoadHeader( FILE * const pFile )
{
	fseek( pFile, 0, SEEK_SET );
	return ( fread( &hdr, sizeof( hdr ), 1, pFile ) == 1 );
}

bool CTGAHeader::LoadHeader( LPCTSTR szFileName )
{
	FILE * pFile = _tfopen( szFileName, "r+bt" );

	bool isOK = pFile != NULL;
	if ( isOK )
	{
		isOK = LoadHeader( pFile );
		fclose( pFile );
	}

	return ( isOK );
}

bool CTGAHeader::IsSupported() const
{
	return
	(
		CHeader::IsSupported() &&
		GetColorMapType() == CMT_ABSENT	&&
		(
			GetImageType() == IT_TRUECLR ||
			GetImageType() == IT_MONOCHR
		)
	);
}