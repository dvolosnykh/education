#include "stdafx.h"

#include "Texture.h"
#include "TGAImage.h"
#include "BMPImage.h"
#include "Camera.h"

static bool IsBlack( const RGBQUAD color )
{
	return ( color.rgbBlue == 0 && color.rgbGreen == 0 && color.rgbRed == 0 );
}

static BYTE BlendComponent( const BYTE & src, const BYTE & dest )
{
	return ( src + ( ( UCHAR_MAX - src ) * dest >> 8 ) );
}

static RGBQUAD Blend( const RGBQUAD & dest, const RGBQUAD & src )
{
	RGBQUAD result;
	result.rgbBlue = BlendComponent( dest.rgbBlue, src.rgbBlue );
	result.rgbGreen = BlendComponent( dest.rgbGreen, src.rgbGreen );
	result.rgbRed = BlendComponent( dest.rgbRed, src.rgbRed );
	result.rgbReserved = BlendComponent( dest.rgbReserved, src.rgbReserved );

	return ( result );
}

bool CTexture::Draw( const CObserver * const pObserver, LONG screen_x, LONG screen_y, const unsigned origin_type, const bool transparent /* = false */ ) const
{
	const bool draw = !IsEmpty();

	if ( draw )
	{
		switch ( origin_type )
		{
		case ORIGIN_TOPLEFT:	break;
		case ORIGIN_CENTER:	screen_x -= width >> 1, screen_y -= height >> 1;	break;
		}

		const LONG & screen_width = pObserver->width;
		const LONG & screen_height = pObserver->height;

		RECT visible = { 0, 0, width - 1, height - 1 };

		if ( 0 <= screen_x + visible.right && screen_x < screen_width &&
			0 <= screen_y + visible.bottom && screen_y < screen_height )
		{
			if ( screen_x < 0 ) visible.left = -screen_x, screen_x = 0;
			if ( screen_y < 0 ) visible.top = -screen_y, screen_y = 0;
			if ( screen_x + visible.right >= screen_width ) visible.right = screen_width - screen_x - 1;
			if ( screen_y + visible.bottom >= screen_height ) visible.bottom = screen_height - screen_y - 1;
		
			if ( transparent )
			{
				register LONG screen_line = screen_y * CGL::C.GetWidth();
				register LONG tex_line = visible.top * width;

				for ( register LONG tex_i = visible.top, screen_i = screen_y;
					tex_i <= visible.bottom;
					tex_i++, screen_i++, tex_line += width, screen_line += CGL::C.GetWidth() )
				{
					for ( register LONG tex_j = visible.left, screen_j = screen_x;
						tex_j <= visible.right;
						tex_j++, screen_j++ )
					{
						RGBQUAD & color = CGL::C[ screen_line + screen_j ];
						color = Blend( color, ptr[ tex_line + tex_j ] );
					}
				}
			}
			else
			{
				register LONG screen_left = screen_y * CGL::C.GetWidth() + screen_x;
				register LONG tex_left = visible.top * width + visible.left;
				const LONG tex_width = ( visible.right - visible.left + 1 ) * sizeof( RGBQUAD );

				for ( register LONG tex_i = visible.top, screen_i = screen_y;
					tex_i <= visible.bottom;
					tex_i++, screen_i++, screen_left += CGL::C.GetWidth(), tex_left += width )
				{
					memcpy( CGL::C.GetPtr() + screen_left, ptr + tex_left, tex_width );
				}
			}
		}
	}

	return ( draw );
}

void CTexture::Resize( const LONG new_height, const LONG new_width )
{
	if ( new_height != height || new_width != width )
	{
		CTexture old;	old.Copy( *this );
		Alloc( new_height, new_width, false );

		const float step_h = ( old.GetHeight() - 1 ) / ( float )( height - 1 );
		const float step_w = ( old.GetWidth() - 1 ) / ( float )( width - 1 );

		register LONG i, j;
		register unsigned index = 0;
		float h, w;
		for ( i = 0, h = 0.0f; i < height; i++, h += step_h )
		{
			LONG h0 = ( LONG )floorf( h );
			LONG h1 = ( LONG )ceilf( h );

			if ( h0 >= height ) h1 = h0 = height - 1;
			else if ( h1 >= height ) h1 = height - 1;

			const register float kh = h - h0;

			for ( j = 0, w = 0.0f; j < width; j++, w += step_w, index++ )
			{
				LONG w0 = ( LONG )floorf( w );
				LONG w1 = ( LONG )ceilf( w );

				if ( w0 >= width ) w1 = w0 = width - 1;
				else if ( w1 >= width ) w1 = width - 1;

				const register float kw = w - w0;

				const RGBQUAD & tex00 = old[ h0 * old.GetWidth() + w0 ];
				const RGBQUAD & tex01 = old[ h0 * old.GetWidth() + w1 ];
				const RGBQUAD & tex10 = old[ h1 * old.GetWidth() + w0 ];
				const RGBQUAD & tex11 = old[ h1 * old.GetWidth() + w1 ];

				register float value1, value2;
				value1 = linear( tex00.rgbRed, tex10.rgbRed, kh );
				value2 = linear( tex01.rgbRed, tex11.rgbRed, kh );
				ptr[ index ].rgbRed = ( BYTE )linear( value1, value2, kw );
				value1 = linear( tex00.rgbGreen, tex10.rgbGreen, kh );
				value2 = linear( tex01.rgbGreen, tex11.rgbGreen, kh );
				ptr[ index ].rgbGreen = ( BYTE )linear( value1, value2, kw );
				value1 = linear( tex00.rgbBlue, tex10.rgbBlue, kh );
				value2 = linear( tex01.rgbBlue, tex11.rgbBlue, kh );
				ptr[ index ].rgbBlue = ( BYTE )linear( value1, value2, kw );
				ptr[ index ].rgbReserved = UCHAR_MAX;
			}
		}
	}
}

bool CTexture::PutOnto( const CTexture & base, const bool fit_base_dims /* = true */ )
{
	const bool same_size = height == base.GetHeight() && width == base.GetWidth();

	if ( same_size )
	{
		for ( LONG i = 0; i < size; i++ )
			ptr[ i ] = Blend( base[ i ], ptr[ i ] );
	}
	else if ( fit_base_dims )
	{
		Resize( base.GetHeight(), base.GetWidth() );

		for ( LONG i = 0; i < size; i++ )
			ptr[ i ] = Blend( base[ i ], ptr[ i ] );
	}
	else
	{
		CTexture temp;	temp.Copy( base );
		temp.Resize( height, width );

		for ( LONG i = 0; i < size; i++ )
			ptr[ i ] = Blend( temp[ i ], ptr[ i ] );
	}

	return ( same_size );
}

bool CTexture::LoadTexture( LPCTSTR pszFileName, const bool transparent /* = false */ )
{
	static TCHAR szBuffer[ _MAX_PATH ];
	_tcscpy( szBuffer, pszFileName );
	_tcslwr( szBuffer );

	const size_t len = _tcslen( szBuffer );

	bool result = false;
	if ( _tcscmp( szBuffer + len - 3, TEXT( "bmp" ) ) == 0 )			// .BMP file ?
		result = LoadBMPTexture( szBuffer, transparent );
	else if ( _tcscmp( szBuffer + len - 3, TEXT( "tga" ) ) == 0 )	// .TGA file ?
		result = LoadTGATexture( szBuffer, transparent );

	return ( result );
}

bool CTexture::LoadTGATexture( LPCTSTR pszFileName, const bool transparent /* = false */ )
{
	CTGAImage tga;
	return LoadImage( &tga, pszFileName, transparent );
}

bool CTexture::LoadBMPTexture( LPCTSTR pszFileName, const bool transparent /* = false */ )
{
	CBMPImage bmp;
	return LoadImage( &bmp, pszFileName, transparent );
}

bool CTexture::LoadImage( CImage * const pImg, LPCTSTR pszFileName, const bool transparent /* = false */ )
{
	bool isOK = pImg->LoadImage( pszFileName );

	if ( isOK )
	{
		Alloc( pImg->GetHeight(), pImg->GetWidth(), false );

		const unsigned bytedepth = pImg->GetBitDepth() >> 3;

		for ( LONG i = 0, index = 0; i < height; i++ )
			for ( LONG j = 0; j < width; j++, index++ )
			{
				const unsigned offset = index * bytedepth;
				ptr[ index ].rgbBlue = ( *pImg )[ offset ];
				ptr[ index ].rgbGreen = ( *pImg )[ offset + 1 ];
				ptr[ index ].rgbRed = ( *pImg )[ offset + 2 ];
				ptr[ index ].rgbReserved = transparent && IsBlack( ptr[ index ] ) ? 0 : UCHAR_MAX;
			}
	}

	return ( isOK );
}