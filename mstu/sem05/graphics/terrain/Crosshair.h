#ifndef CROSSHAIR_H
#define CROSSHAIR_H


#include "Texture.h"
#include "Observer.h"


class CCrosshair
{
private:
	CTexture m_Texture;

public:
	CCrosshair() {};
	~CCrosshair() {};

	bool LoadTexture( LPCTSTR pszFilename );
	void Draw( const CObserver * const pObserver );

	void Destroy();
};

#endif