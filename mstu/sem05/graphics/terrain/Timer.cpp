#include "stdafx.h"

#include "Timer.h"


DWORD CTimer::Start()
{
	valid = true;
	m_StartTime = GetTick();
	m_ElapsedTime = 0;
	m_StopTime = 0;

	m_LastNote = m_StartTime;
	m_LastInterval = 0;
	
	paused = false;
	m_PauseTime = 0;
	m_PauseInterval = 0;

	m_FrameCount = 0;

	return ( m_StartTime );
}

DWORD CTimer::Stop()
{
	MakeNote();
	m_StopTime = GetTick();
	valid = false;

	return ( m_ElapsedTime );
}

DWORD CTimer::MakeNote()
{
	DWORD interval = 0;

	if ( valid && !paused )
	{
		const DWORD CurrentNote = GetTick();
		m_LastInterval = CurrentNote - m_LastNote;
		if ( m_LastNote < m_PauseTime )
			m_LastInterval -= m_PauseInterval;
		m_ElapsedTime += m_LastInterval;

		m_LastNote = CurrentNote;

		DWORD CurFrameCount = ONE_SECOND;
		if ( m_LastInterval > 0 )
			CurFrameCount /= m_LastInterval;
		m_FrameCount = CurFrameCount >> 1;

		interval = m_LastInterval;
	}

	return ( interval );
}

void CTimer::Pause()
{
	assert( valid && !paused );

	if ( valid && !paused )
	{
		m_PauseTime = GetTick();
		paused = true;
	}
}

void CTimer::Resume()
{
	assert( valid && paused );

	if ( valid && paused )
	{
		m_PauseInterval += GetTick() - m_PauseTime;
		paused = false;
	}
}