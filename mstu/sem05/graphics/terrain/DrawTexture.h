#ifndef DRAW_TEXTURE_H
#define DRAW_TEXTURE_H


enum { ZOOM_IN, ZOOM_OUT };


class CTexMode
{
private:
	static POINT limit;
	static LONG offset;

public:
	static POINT top;
	static float zoom;
	static bool updated;
	static bool shadows;

public:
	static void Init();

	static void ScrollUp();
	static void ScrollDown();
	static void ScrollLeft();
	static void ScrollRight();

	static void ScrollTop();
	static void ScrollBottom();
	static void ScrollLeftmost();
	static void ScrollRightmost();

	static void ZoomIn() { Zoom( ZOOM_IN ); };
	static void ZoomOut() { Zoom( ZOOM_OUT ); };

	static void IncOffset();
	static void DecOffset();

private:
	static void RefreshLimits();
	static void VerifyCoords();
	static void Zoom( const unsigned type );
};

#endif