#ifndef VERTVECT_H
#define VERTVECT_H

class CVector;
class CVector2D;
class CVector3D;

class CVertex;
class CVertex2D;
class CVertex3D;
class CVertex4D;

#endif




/*
class CVector;

class CPoint
{
public:
	float_t x, y, z;

	CPoint() {};
	CPoint( const float_t init_x, const float_t init_y, const float_t init_z ) { Set( init_x, init_y, init_z ); };
	CPoint( const CPoint & init ) { Set( init ); };
	~CPoint() {};

	void Set( const float_t new_x, const float_t new_y, const float_t new_z ) { x = new_x, y = new_y, z = new_z; };
	void Set( const CPoint & p ) { Set( p.x, p.y, p.z ); };

	float_t Distance( const CPoint & p ) const
	{
		const float_t dx = x - p.x;
		const float_t dy = y - p.y;
		const float_t dz = z - p.z;

		return sqrt( dx * dx + dy * dy + dz * dz );
	}

	int operator == ( const CPoint & p ) const { return ( x == p.x && y == p.y && z == p.z ); };
	int operator != ( const CPoint & p ) const { return !( *this == p ); };
	CPoint & operator += ( const CPoint & p ) { x += p.x, y += p.y, z += p.z; return ( *this ); };
	CPoint & operator -= ( const CPoint & p ) { x -= p.x, y -= p.y, z -= p.z; return ( *this ); };
	CPoint operator + ( const CPoint & p ) const { return CPoint( x + p.x, y + p.y, z + p.z ); };
	CVector operator - ( const CPoint & p ) const;
};

class CVector
{
public:
	float_t x, y, z;

public:
	CVector( const float_t init_x, const float init_y, const float_t init_z )
	{
		x = init_x, y = init_y, z = init_z;
	}
};


CVector CPoint::operator - ( const CPoint & p ) const { return CVector( x - p.x, y - p.y, z - p.z ); }
CPoint CPoint::operator + ( const CVector & v ) const { return CPoint( x + v.x, y + v.y, z + v.z ); }
CPoint CPoint::operator - ( const CVector & v ) const { return CPoint( x - v.x, y - v.y, z - v.z ); }
CPoint & CPoint::operator += ( const CVector & v ) { x += v.x, y += v.y, z += v.z; return ( *this ); }
CPoint & operator -= ( const CPoint & p ) { x -= p.x, y -= p.y, z -= p.z; return ( *this ); }
CPoint operator + ( const CPoint & p ) const { return CPoint( x + p.x, y + p.y, z + p.z ); }

*/