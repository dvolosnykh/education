#include "stdafx.h"

#include "DrawTexture.h"


POINT	CTexMode::limit;
LONG	CTexMode::offset;
POINT	CTexMode::top;
float	CTexMode::zoom;
bool	CTexMode::updated;
bool	CTexMode::shadows;



void CTexMode::Init()
{
	top.x = 0, top.y = 0;
	zoom = 1.0f;
	offset = 15;
	RefreshLimits();
	updated = true;
}

void CTexMode::ScrollUp()
{
	top.y -= offset;
	VerifyCoords();
	updated = true;
}

void CTexMode::ScrollDown()
{
	top.y += offset;
	VerifyCoords();
	updated = true;
}

void CTexMode::ScrollLeft()
{
	top.x -= offset;
	VerifyCoords();
	updated = true;
}

void CTexMode::ScrollRight()
{
	top.x += offset;
	VerifyCoords();
	updated = true;
}

void CTexMode::ScrollTop()
{
	top.y = 0;
	updated = true;
}

void CTexMode::ScrollBottom()
{
	top.y = limit.y;
	updated = true;
}

void CTexMode::ScrollLeftmost()
{
	top.x = 0;
	updated = true;
}

void CTexMode::ScrollRightmost()
{
	top.x = limit.x;
	updated = true;
}

void CTexMode::Zoom( const unsigned type )
{
	const float zoom_old = zoom;

	switch ( type )
	{
	case ZOOM_IN:
		if ( zoom >= 1.0f ) zoom += 1.0f;
		else zoom /= 1.0f - zoom;

		break;

	case ZOOM_OUT:
		if ( zoom > 1.0f ) zoom -= 1.0f;
		else zoom /= 1.0f + zoom;

		break;
	}

	const float t = ( zoom - zoom_old ) / ( zoom * zoom_old );
	top.x += LONG( ( CGL::C.GetWidth() >> 1 ) * t );
	top.y += LONG( ( CGL::C.GetHeight() >> 1 ) * t );

	RefreshLimits();
	VerifyCoords();
	updated = true;
}

void CTexMode::IncOffset()
{
	offset += 5;
}

void CTexMode::DecOffset()
{
	offset -= 5;
}

void CTexMode::RefreshLimits()
{
	const CTexture & tex = CGL::Landscape.m_LightedTexture;
	limit.x = tex.GetWidth() - LONG( CGL::C.GetWidth() / zoom );
	limit.y = tex.GetHeight() - LONG( CGL::C.GetHeight() / zoom );
	if ( limit.x < 0 ) limit.x = 0;
	if ( limit.y < 0 ) limit.y = 0;
}

void CTexMode::VerifyCoords()
{
	if ( top.x < 0 ) top.x = 0;
	else if ( top.x > limit.x ) top.x = limit.x;

	if ( top.y < 0 ) top.y = 0;
	else if ( top.y > limit.y ) top.y = limit.y;
}



void CGL::DrawTexture()
{
	if ( CTexMode::updated )
	{
		ClearDisplay();

		const CTexture & tex = CTexMode::shadows ? Landscape.m_LightedTexture : Landscape.m_Texture;

		const LONG & height = tex.GetHeight();
		const LONG & width = tex.GetWidth();

		const float step = 1.0f / CTexMode::zoom;

		float fheight = C.GetHeight() - height * CTexMode::zoom;
		if ( fheight < 0.0f ) fheight = 0.0f;
		register LONG i = LONG( fheight ) >> 1;
		float y = ( float )CTexMode::top.y;

		for ( ; i < ( LONG )C.GetHeight() && y < height; i++, y += step )
		{
			float fwidth = C.GetWidth() - width * CTexMode::zoom;
			if ( fwidth < 0.0f ) fwidth = 0.0f;
			register LONG j = LONG( fwidth ) >> 1;
			float x = ( float )CTexMode::top.x;

			for ( ; j < ( LONG )C.GetWidth() && x < width; j++, x += step )
				C[ i * C.GetWidth() + j ] = tex[ long( y ) * width + long( x ) ];
		}

		UpdateDisplay();
		CTexMode::updated = false;
	}
}