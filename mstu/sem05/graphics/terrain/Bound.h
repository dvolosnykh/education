#ifndef BOUND_H
#define BOUND_H


#include "GL.h"
#include "Triangle.h"
#include "ray.h"


class CBound
{
private:
	enum
	{
		PLANE_RIGHT,	PLANE_LEFT,
		PLANE_BOTTOM,	PLANE_TOP,
		PLANE_FAR,		PLANE_NEAR,
		PLANE_NUM
	};

public:
	CVertex3D m_Min;
	CVertex3D m_Max;

	CVertex3D m_Center;
	float m_Radius;

public:
	CBound() {};
	~CBound() {};

	void Set( const CVertex3D & Min, const CVertex3D & Max );
	float GetSideLength() const { return ( m_Max.x - m_Min.x ); };

	bool CheckIntersection( const CRay * const ray ) const;
	bool CheckCollision( const CTriangle * const tri ) const;
	void Calculate( const CTriangleList & Triangles );

	void Draw( const float intensity ) const;

private:
	int SimpleTest( const VertexArray & v ) const;
	bool Test( const VertexArray & v ) const;
	bool IntersectsFace( const CVertex3D * const bound, const unsigned i, const CRay * const ray ) const;
	bool IntersectsBound( const CRay ray ) const;

	bool PointsByDifferentSides( const VertexArray & v ) const;
	bool AllOnOuterSide( const VertexArray & v, const unsigned index ) const;

	bool PointInside( const CVertex3D * const point ) const;
	bool PointInside_NoX( const CVertex3D point ) const;
	bool PointInside_NoY( const CVertex3D point ) const;
	bool PointInside_NoZ( const CVertex3D point ) const;
	bool AllPointsOutside( const VertexArray & v ) const;

	void CalcBoundSphere();

private:
	void MakeCubic();

	typedef BOOL ( CBound::* func_t )( const CVertex3D * const point ) const;
	static func_t test[ PLANE_NUM ];

	BOOL test1( const CVertex3D * const point ) const { return ( m_Min.x <= point->x ); };
	BOOL test2( const CVertex3D * const point ) const { return ( point->x <= m_Max.x ); };
	BOOL test3( const CVertex3D * const point ) const { return ( m_Min.y <= point->y ); };
	BOOL test4( const CVertex3D * const point ) const { return ( point->y <= m_Max.y ); };
	BOOL test5( const CVertex3D * const point ) const { return ( m_Min.z <= point->z ); };
	BOOL test6( const CVertex3D * const point ) const { return ( point->z <= m_Max.z ); };
};

#endif