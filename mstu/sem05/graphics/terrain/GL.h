#ifndef GL_H
#define GL_H


#include "Landscape.h"
#include "Observer.h"
#include "Triangle.h"
#include "HeavenlyBody.h"
#include "Crosshair.h"

#include "DrawScenes.h"
#include "DrawTexture.h"


#undef DEBUG_INFO
//#define DEBUG_INFO

#undef USE_OPENGL_PROJECTION
#define USE_OPENGL_PROJECTION


#define OBSERVERS_NUM	4
#define BAR_HEIGHT		16
#define MAX_TEXT_LEN	256


struct POINT3D : public POINT
{
	float z;
};

typedef CTDArray< POINT3D >		ScreenArray;
typedef CTDArray< CVector3D >	VectorArray;
typedef CTDArray< RGBQUAD >		LightArray;
typedef CTDArray< CVertex2D >	TextureArray;
typedef CTDArray< unsigned >	IndexArray;

enum { WND_MODE_NEXT = -1, WND_FULL, WND_SPLIT_4, WND_SPLIT_1_3, WND_MODES_NUM };
enum { DRAW_MODE_NEXT = -1, DRAW_SCENES, DRAW_TEXTURE, DRAW_MODES_NUM };

typedef CTDArray< float >		DepthBuffer;
typedef CTDArray< RGBQUAD >		ColorBuffer;


class CHeightmap : public CBytesArray
{
public:
	CHeightmap() {};
	virtual ~CHeightmap() {};

	bool Load( LPCTSTR pszFileName );
};



class CGL
{
private:
	static BITMAPINFO	m_bmp;
 	static HDC			m_hDC;
	static int			m_OldTextMode;
	static COLORREF		m_OldTextColor;

	static LONG	m_BarHeight;

public:
	static IndexArray		Frame;
	static VertexArray		Vertex;
	static ScreenArray		Screen;
	static VectorArray		Normal;
	static LightArray		Light;
	static TextureArray		TexCoord;

	static CVisibleTriList	VisTriList;

	static engine_param_t	m_params;
	static int			window_mode;
	static int			draw_mode;
	static DepthBuffer	Z;
	static ColorBuffer	C;

	static CHeightmap	Heightmap;
	static CLandscape	Landscape;
	static CHeavenlyBody			HeavenlyBody;
	static CCrosshair	Crosshair;
	static CObserver	Observer[ OBSERVERS_NUM ];
	static CObserver *	pActive;
	static CObserver *	pMain;

	static unsigned		FrameIndex;

public:
	static void Draw( const unsigned fps );
	static bool NeedSavePoint( const CObserver * const pObserver, const float z, const unsigned index );
	static void SavePoint( const float z, const unsigned index, CVertex2D t );

	static void InitObserversPos();
	static void InitHeavenlyBody();
	static bool InitEngine();

	static void ProcessLighting( const bool show_progress = true );
//	static void ApplyLightingToTextureRect( CTexture & dest_tex, const CTexture & src_tex, const RECT rect );
	static void ApplyLightingToTexture( const bool show_progress = true );

	static void DisplaceHeavenlyBody( const float angle_x, const float angle_y );

	static bool InitDisplay( const DWORD res_width, const DWORD res_height, const bool fullscreen );
	static void DeinitDisplay();
	static void RefreshDisplay();

	static void UpdateObservers( const DWORD elapsed );
	static void AllObserversUpdated();

	static void ToggleWindowMode( const int index = WND_MODE_NEXT );
	static void ToggleDrawMode( const int index = DRAW_MODE_NEXT );

	static void SetWindowMode( const int new_mode );
	static void SetDrawMode( const int new_mode );

	static void TransformVertex( POINT3D & screen, const CVertex3D & vertex, const CObserver * const pObserver, const LONG height_2, const LONG width_2 );

	static BYTE Lighting( const BYTE & color, const BYTE & light );

private:
	static void InitObservers();

	static RGBQUAD CalcVertexLighting( const unsigned index, const RGBQUAD & light, const CVector3D & light_dir );

	static void DrawTexture();
	static void DrawScenes( const unsigned fps );
	static void RenderScene( const CObserver * const pObserver, stats_t * const pstats = NULL );
	static void DrawScene( CObserver * const pObserver, stats_t * const pstats = NULL );
	static void RemoveArtefacts( const CObserver * const pObserver );

	static unsigned TransformWorld( const CObserver * const pObserver );

	static void ClearDisplay( const CObserver * const pObserver = NULL );
	static void UpdateDisplay( const CObserver * const pObserver = NULL );

	static void UpdateDC( const LONG dest_x, const LONG dest_y, const LONG width, const LONG height );

	static void ShowStats( const stats_t * const pstats );
	static void ShowFPS( const unsigned fps );
	static void ShowValue( int pos_x, int pos_y, LPCTSTR format, const unsigned value );


	static void ShowText( int pos_x, int pos_y, LPCTSTR text );
};

#endif