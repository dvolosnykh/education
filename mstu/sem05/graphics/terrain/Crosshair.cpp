#include "stdafx.h"

#include "Crosshair.h"


void CCrosshair::Draw( const CObserver * const pObserver )
{
	m_Texture.Draw( pObserver, pObserver->width >> 1, pObserver->height >> 1, ORIGIN_CENTER, true );
}

bool CCrosshair::LoadTexture( LPCTSTR pszFilename )
{
	return m_Texture.LoadTexture( pszFilename, true );
}

void CCrosshair::Destroy()
{
	m_Texture.Free();
}