#ifndef RAY_H
#define RAY_H


#include "plane.h"


class CRay
{
public:
	CVector3D m_dir;
	CVertex3D m_start;
	CVertex3D m_end;

public:
	CRay() {};
	CRay( const CVertex3D & init_start, const CVertex3D & init_end ) { Set( init_start, init_end ); };
	~CRay() {};

	void Set( const CVertex3D & new_start, const CVertex3D & new_end ) { m_dir = ( m_end = new_end ) - ( m_start = new_start ); };

	bool IntersectsPlane( const CPlane * const plane ) const;
	CVertex3D IntersectionWithPlane( const CPlane * const plane ) const;

	bool IntersectsBoundingPlane( const CVertex3D * const bound, const unsigned i ) const;
	CVertex3D IntersectionWithBoundingPlane( const CVertex3D * const bound, const unsigned i ) const;
};

#endif