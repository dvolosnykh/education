#include "stdafx.h"


#include "HeavenlyBody.h"
#include "ProgressWindow.h"
#include "ray.h"


void CHeavenlyBody::Draw( const CObserver * const pObserver ) const
{
	if ( IsBackground() || !CGL::Landscape.CheckIntersection( CRay( m_pos, pObserver->m_pos ) ) )
	{
		POINT3D screen;
		CGL::TransformVertex( screen, m_pos, pObserver, pObserver->height >> 1, pObserver->width >> 1 );

		if ( screen.z > 0.0f )
			m_Texture.Draw( pObserver, screen.x, screen.y, ORIGIN_CENTER, true ); // !IsBackground()
	}
}

bool CHeavenlyBody::LoadTextures( LPCTSTR pszSearchPath )
{
	static bool tex_loaded = false;

	if ( !tex_loaded || CGL::m_params.light_changed )
	{
		CProgressWindow::SetTask( TEXT( "�������� ������� ��� ��������� ������� (%i%%)..." ) );

		static TCHAR szTexturePath[ _MAX_PATH ];
		_tcscpy( szTexturePath, pszSearchPath );
		const size_t len = _tcslen( szTexturePath );

		switch ( g_param.DayTime )
		{
		case IDC_SUNNY_DAY:
		case IDC_SUNSET:
			{
			// Load sun flare.
			_tcscpy( szTexturePath + len, TEXT( "sunflare.tga" ) );
			tex_loaded = m_Texture.LoadTexture( szTexturePath, true );
			CProgressWindow::SetProgress( 50 );

			// Load sun base.
			_tcscpy( szTexturePath + len, TEXT( "sunbase.tga" ) );
			CTexture SunBase;
			tex_loaded = tex_loaded && SunBase.LoadTexture( szTexturePath, true );

			if ( tex_loaded )		// Merge textures with transparency.
				m_Texture.PutOnto( SunBase, false );

			background = false;
			}
			break;

		case IDC_MOON_NIGHT:
			// Load sun flare.
			_tcscpy( szTexturePath + len, TEXT( "moon.tga" ) );
			tex_loaded = m_Texture.LoadTexture( szTexturePath, false );

			background = true;

			break;
		}

		CProgressWindow::SetProgress( 100 );
	}

	return ( tex_loaded );
}

void CHeavenlyBody::Destroy()
{
	m_Texture.Free();
}