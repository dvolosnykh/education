#ifndef MATRIX_H
#define MATRIX_H


#include "vertex.h"

#define DIM		4


class CMatrix
{
private:
	float ptr[ DIM ][ DIM ];

public:
	CMatrix() { Reset(); };
	CMatrix( const float value ) { *this = value; };
	~CMatrix() {};

	void Reset() { memset( ptr, 0, sizeof( ptr ) ); };

	float ** GetPtr() { return ( float ** )ptr; };
	float * const * const GetPtr() const { return ( float ** )ptr; };

	void Identity() { *this = 1.0f; };
	CMatrix & RotateX( const float angle );
	CMatrix & RotateY( const float angle );
	CMatrix & RotateZ( const float angle );
	CMatrix & Offset( const CVector3D offset );
	CMatrix GetOffset( const CVector3D offset );

	CVertex4D TransformDot( const CVertex4D p ) const;

	CMatrix & Mult( const CMatrix & m ) { return ( *this = GetMult( m ) ); };
	CMatrix GetMult( const CMatrix & m ) const;

	void Print() const;

	float * operator [] ( const unsigned index ) { return ( ptr[ index ] ); };
	const float * const operator [] ( const unsigned index ) const { return ( ptr[ index ] ); };

	CMatrix operator - ( CVector3D offset ) { return GetOffset( -offset ); };
	CMatrix operator + ( const CVector3D offset ) { return GetOffset( offset ); };
	CMatrix & operator -= ( CVector3D offset ) { return Offset( -offset ); };
	CMatrix & operator += ( const CVector3D offset ) { return Offset( offset ); };
	CMatrix & operator = ( const float unit );
	CVertex4D operator * ( const CVertex4D p ) const { return TransformDot( p ); };
	CMatrix operator * ( const CMatrix & m ) { return GetMult( m ); };
	CMatrix & operator *= ( const CMatrix & m ) { return Mult( m ); };

private:
	float MultRowByCol( const unsigned i, const CMatrix & m, const unsigned j ) const;
};

#endif