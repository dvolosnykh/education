#include "stdafx.h"

#include "DrawScenes.h"


void CGL::DrawScenes( const unsigned fps )
{
	static stats_t stats;

	RenderScene( pMain, &stats );

	switch ( window_mode )
	{
	case WND_FULL:
		DrawScene( pActive, &stats );
		break;

	case WND_SPLIT_4:
	case WND_SPLIT_1_3:
		for ( register unsigned i = 0; i < OBSERVERS_NUM; i++ )
		{
			CObserver * const pObserver = &Observer[ i ];
			DrawScene( pObserver, pObserver == pMain ? &stats : NULL );
		}
	}

	if ( g_param.ShowStats ) ShowFPS( fps );
}

void CGL::RenderScene( const CObserver * const pObserver, stats_t * const pstats /* = NULL */ )
{
	if ( pObserver->updated )
	{
		unsigned terrain = Landscape.Render( pObserver );
		unsigned water = 0;

		if ( pstats != NULL )
		{
			pstats->terrain = terrain;
			pstats->water = water;
		}

		switch ( window_mode )
		{
		case WND_FULL:
			pActive->updated = true;
			break;	// in fact unneeded.

		case WND_SPLIT_4:
		case WND_SPLIT_1_3:
			AllObserversUpdated();
			break;
		}
	}
}

void CGL::DrawScene( CObserver * const pObserver, stats_t * const pstats /* = NULL */ )
{
	if ( pObserver->updated )
	{
		ClearDisplay( pObserver );

		const unsigned vertices = TransformWorld( pObserver );
		if ( pstats != NULL )
			pstats->vertices = vertices;

		if ( g_param.DrawHeavenlyBody && pObserver == pMain && HeavenlyBody.IsBackground() )
			HeavenlyBody.Draw( pObserver );

		VisTriList.DrawAll( pObserver );

		if ( g_param.RemoveArtifacts )
			RemoveArtefacts( pObserver );

		if ( g_param.DrawHeavenlyBody && pObserver == pMain && !HeavenlyBody.IsBackground() )
			HeavenlyBody.Draw( pObserver );

		if ( g_param.DrawCrosshair && pObserver == pActive )
			Crosshair.Draw( pObserver );

		if ( pstats != NULL && g_param.ShowStats )
			ShowStats( pstats );

		UpdateDisplay( pObserver );
		pObserver->updated = false;
	}
}

void CGL::RemoveArtefacts( const CObserver * const pObserver )
{
#define BAD_PRECISION	1.0f

	const LONG & height = pObserver->height;
	const LONG & width = pObserver->width;
	const LONG z_width = Z.GetWidth();

	for ( register LONG i = 0, start_index = 0; i < height; i++, start_index += z_width)
	{
		register LONG start_j = 0, end_j = width - 1;
		register LONG index;

		// exclude trailing and leading "sky" pixels;
		for ( index = start_index + end_j; end_j >= 0 && Z[ index ] == FLT_MAX; end_j--, index-- );
		for ( index = start_index; start_j < end_j && Z[ index ] == FLT_MAX; start_j++, index++ );

		if ( end_j - start_j + 1 > 2 )
		{
			register LONG j = start_j;
			float prev_z = Z[ index ];

			for ( j++, index++; j <= end_j; )
			{
				float & cur_z = Z[ index ];

				if ( cur_z - prev_z <= BAD_PRECISION )
				{
					prev_z = cur_z;
					j++, index++;
				}
				else if ( j < end_j )
				{
					const float & next_z = Z[ index + 1 ];

					if ( cur_z - next_z > BAD_PRECISION )
					{
						bool bad = true;

/*						if ( cur_z == FLT_MAX )
							if ( i > 0 )
								bad = Z[ index - z_width ] < FLT_MAX;
							else if ( i < height - 1 )
								bad = Z[ index + z_width ] < FLT_MAX;
*/
						if ( bad )
						{
//							cur_z = ( prev_z + next_z ) * 0.5f;

							C[ index ].rgbRed = ( C[ index - 1 ].rgbRed + C[ index + 1 ].rgbRed ) >> 1;
							C[ index ].rgbGreen = ( C[ index - 1 ].rgbGreen + C[ index + 1 ].rgbGreen ) >> 1;
							C[ index ].rgbBlue = ( C[ index - 1 ].rgbBlue + C[ index + 1 ].rgbBlue ) >> 1;
						}

						prev_z = next_z;
						j += 2, index += 2;
					}
					else
					{
						prev_z = cur_z;
						j++, index++;
					}
				}
				else
				{
					bool bad = false;

					if ( cur_z < FLT_MAX )
						bad = true;
					else if ( i > 0 )
						bad = Z[ index - z_width ] < FLT_MAX;
					else if ( i < height - 1 )
						bad = Z[ index + z_width ] < FLT_MAX;

					if ( bad )
					{
						cur_z = prev_z;
						C[ index ] = C[ index - 1 ];
					}

					j++, index++;
				}
			}
		}
	}

#undef BAD_PRECISION
}

void CGL::InitObserversPos()
{
	const float width = Landscape.GetWidth();
	const float height = Landscape.GetHeight();
	const float length = Landscape.GetLength();
	const float average = ( width + length ) * 0.5f;
	const float top = 3.0f;

	const float offset = 0.1f;
	CVertex3D pos;

	// Main view
	pos.x = width * 0.5f;
	pos.y = top * height;
	pos.z = ( 1.0f + offset ) * length;
	Observer[ 0 ].SetPos( pos );

	// View from the top
	pos.x = width * 0.5f;
	float hx = width / ( 2.0f * Observer[ 1 ].m_TanX );
	float hy = length / ( 2.0f * Observer[ 1 ].m_TanY );
	hy = hy > hx ? hy : hx;
	pos.y = ( hy > 2.0f * height ? hy : 2.0f * height ) * 1.3f;
	pos.z = length * 0.5f;
	Observer[ 1 ].SetPos( pos );

	// View from the far right corner
	pos.x = ( 1.0f + offset ) * width;
	pos.y = top * height;
	pos.z = -offset * length;
	Observer[ 2 ].SetPos( pos );

	// View from the far left corner
	pos.x = -offset * width;
	pos.y = top * height;
	pos.z = -offset * length;
	Observer[ 3 ].SetPos( pos );
	
	CVertex3D center = Landscape.GetCenter();
	center.y = 0;
	for ( unsigned i = 0; i < OBSERVERS_NUM; i++ )
	{
		Observer[ i ].Reset();
		Observer[ i ].LookAt( CVertex3D( center.x, 0.0f, center.z ) );
		Observer[ i ].SetType( TYPE_AIRBORNE );
		Observer[ i ].updated = true;
	}
}

void CGL::InitObservers()
{
	const float width = Landscape.GetWidth();
	const float length = Landscape.GetLength();
	const float average = ( width + length ) * 0.5f;

	for ( unsigned i = 0; i < OBSERVERS_NUM; i++ )
	{
		Observer[ i ].SetType( TYPE_AIRBORNE );
		Observer[ i ].SetStartSpeed( average * 0.05f );
		Observer[ i ].m_size = average / 178.0f;
		Observer[ i ].m_height = average / 50.0f;
	}

	InitObserversPos();
}

void CGL::AllObserversUpdated()
{
	for ( register unsigned i = 0; i < OBSERVERS_NUM; i++ )
		Observer[ i ].updated = true;
}

void CGL::UpdateObservers( const DWORD elapsed )
{
	for ( register unsigned i = 0; i < OBSERVERS_NUM; i++ )
	{
		Observer[ i ].UpdatePos( elapsed );
		Observer[ i ].UpdateView();
	}
}


void CGL::ShowStats( const stats_t * const pstats )
{
	const RECT rect = { 0, 0, C.GetWidth() - 100, m_BarHeight };
	FillRect( m_hDC, &rect, ( HBRUSH )GetStockObject( BLACK_BRUSH ) );

	static int pos_x;

	ShowValue( rect.left, rect.top, TEXT( "%i ������." ), pstats->vertices );
	ShowValue( rect.left + 110, rect.top, TEXT( "%i �������������." ), pstats->terrain );
//	ShowValue( rect.left + 230, rect.top, TEXT( "%i ����." ), pstats->water );

#ifdef DEBUG_INFO
	ShowValue( rect.left + g_param.ResWidth - 100, rect.top + 30, TEXT( "%i minz" ), minz );
	ShowValue( rect.left + g_param.ResWidth - 100, rect.top + 50, TEXT( "%i maxz" ), maxz );
#endif

	pos_x = rect.left + g_param.ResWidth - 400;
	ShowText( pos_x - 100, rect.top, TEXT( "�����������: " ) );
	ShowValue( pos_x, rect.top, TEXT( "% 5i," ), unsigned( pActive->m_pos.x ) );
	ShowValue( pos_x += 50, rect.top, TEXT( "% 5i," ), unsigned( pActive->m_pos.y ) );
	ShowValue( pos_x += 50, rect.top, TEXT( "% 5i" ), unsigned( pActive->m_pos.z ) );
}

void CGL::ShowFPS( const unsigned fps )
{
	const RECT rect = { C.GetWidth() - 100, 0, C.GetWidth(), m_BarHeight };
	FillRect( m_hDC, &rect, ( HBRUSH )GetStockObject( BLACK_BRUSH ) );

	ShowValue( rect.left, rect.top, TEXT( "FPS: % 5i" ), fps );
}

void CGL::ShowValue( int pos_x, int pos_y, LPCTSTR format, const unsigned value )
{
	static TCHAR str[ MAX_TEXT_LEN ];

	_stprintf( str, format, value );
	str[ MAX_TEXT_LEN - 1 ] = TCHAR( '\0' );

	ShowText( pos_x, pos_y, str );
}