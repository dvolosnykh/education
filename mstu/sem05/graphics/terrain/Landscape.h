#ifndef LANDSCAPE_H
#define LANDSCAPE_H


#include "TGAImage.h"
#include "Triangle.h"
#include "Texture.h"
#include "ProgressWindow.h"
#include "OctreeNode.h"
#include "Frustum.h"
#include "SetupDialog.h"
#include "vectors.h"
#include "Param.h"


#define MAX_TEXTURES	10


class CLandscape
{
private:
	CVector3D m_dim;
	COctreeNode m_Octree;	// Mother node of the Octree
	CTriangleArray m_Triangles;

public:
	CTexture m_LightedTexture;
	CTexture m_Texture;
	CTexture m_Detail;

public:
	CLandscape() {};
	~CLandscape() {};

	float GetWidth() const { return ( m_dim.x ); };
	float GetLength() const { return ( m_dim.z ); };
	float GetHeight() const { return ( m_dim.y ); };
	CVertex3D GetCenter() const { return ( m_dim * 0.5f ); };

	float GetSmoothHeight( const float at_x, const float at_z ) const;
	bool InScope( const float at_x, const float at_z ) const;

	bool LoadTerrain( engine_param_t & engine );
	void Destroy();

	bool CheckIntersection( const CRay ray ) const { return m_Octree.CheckIntersection( &ray ); };

	unsigned Render( const CObserver * const pObserver );

private:
	unsigned GenerateTerrainTexture();
	void CalcVertices();
	void Triangulate();
	void BuildOctree();
	void CalcTexCoords();
};

#endif