#include "stdafx.h"

#include "BMPHeader.h"


CBMPHeader::CBMPHeader()
{
	hdr.file.bfType = BITMAP_TYPE;
	hdr.file.bfReserved1 = 0;
	hdr.file.bfReserved2 = 0;

	hdr.info.biSize = sizeof( BITMAPINFOHEADER );
	hdr.info.biPlanes = 1;
}

bool CBMPHeader::LoadHeader( FILE * const pFile )
{
	//fseek( pFile, 0, SEEK_SET );
	bool isOK = ( fread( &hdr.file, sizeof( hdr.file ), 1, pFile ) == 1 ) &&
		( fread( &hdr.info, sizeof( hdr.info ), 1, pFile ) == 1 );
	return ( isOK );
}

bool CBMPHeader::LoadHeader( LPCTSTR szFileName )
{
	FILE * pFile = _tfopen( szFileName, "r+bt" );

	bool isOK = pFile != NULL;
	if ( isOK )
	{
		isOK = LoadHeader( pFile );
		fclose( pFile );
	}

	return ( isOK );
}

bool CBMPHeader::IsSupported() const
{
	return
	(
		CHeader::IsSupported() &&
		GetCompression() == BI_RGB
	);
}