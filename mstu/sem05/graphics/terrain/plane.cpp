#include "stdafx.h"

#include "plane.h"


void CPlane::Set( const CVector3D & normal, const float d )
{
	m_Normal = normal, m_D = d;
	Normalize();
};

void CPlane::Set( const CVertex3D & p0, const CVertex3D & p1, const CVertex3D & p2 )
{
	CalcNormal( &p0, &p1, &p2 );
	CalcD( &p0 );
}

void CPlane::Normalize()
{
	assert( !m_Normal.IsNull() );

	const float k = 1.0f / m_Normal.Length();
	m_Normal *= k, m_D *= k;
}

void CPlane::CalcNormal( const CVertex3D * const p0, const CVertex3D * const p1, const CVertex3D * const p2 )
{
	m_Normal = ( *p2 - *p1 ) ^ ( *p0 - *p1 );
	m_Normal.Normalize();
}

bool CPlane::AllOnNormalSide( const CVertex3D * const v, const unsigned n ) const
{
	register unsigned i;
	for ( i = 0; i < n && SignDistance( v + i ) > 0.0f; i++ );

	return ( i == n );
}