#include "stdafx.h"

#include "ProgressWindow.h"


int __stdcall _tWinMain( HINSTANCE hInstance, HINSTANCE hPrev, LPTSTR lpCmdLine, int nCmdShow )
{
	int result = 0;

	if ( CApp::RegisterClass() && CProgressWindow::RegisterClass() )
		result = CApp::Main( hInstance, hPrev, lpCmdLine, nCmdShow );

	return ( result );
}