#ifndef PROGRESSWINDOW_H
#define PROGRESSWINDOW_H


#define WINDOW_SIZE_X	512
#define WINDOW_SIZE_Y	384

#define MAX_TASK_LENGTH	256


class CProgressWindow
{
private:
	static HWND m_hWnd;
	static HWND m_hStatic;
	static HBITMAP m_hBmp;
	static HDC m_hCompDC;
	static TCHAR m_pszCurrentTask[ MAX_TASK_LENGTH ];

public:
	static ATOM RegisterClass();

	static bool Create( LPCTSTR pszFileName );
	static void Destroy();

	static void SetTask( LPCTSTR pszTaskDescription );
	static void SetProgress( unsigned iProgress );

private:
	static LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );

	static void InitWindow();
	static void DeinitWindow();
	static bool InitInstance( LPCTSTR pszFileName );
	static bool LoadBackground( LPCTSTR pszFileName );
};

#endif