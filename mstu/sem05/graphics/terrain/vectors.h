#ifndef VECTORS_H
#define VECTORS_H


#include "vertvect.h"


/************************************************************************/
/* Rewirte using virtual methods... !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  */
/************************************************************************/


//==================== 1D-vector ====================

class CVector
{
public:
	float x;

	CVector() {};
	CVector( const float init_x ) { Set( init_x ); };
	CVector( const CVector & init ) { Set( init ); };
	CVector( const CVertex & init );
	CVector( const float * const array ) { LoadArray( array ); };
	~CVector() {};

	void LoadArray( const float * const array ) { Set( array[ 0 ] ); };

	void Set( const float new_x ) { x = new_x; };
	void Set( const CVector & v ) { Set( v.x ); };
	CVector & Inverse() { Set( -x ); return ( *this ); };
	CVector GetInversed() const { return CVector( -x ); };

	/*virtual*/ float Length() const { return fabsf( x ); };

	CVector & ScaleX( const float k ) { x *= k; return ( *this ); };
	/*virtual*/ CVector & Scale( const float k ) { return ScaleX( k ); };
	CVector GetScaled( const float k ) const { CVector v = *this; return v.Scale( k ); };
	CVector & Normalize();
	CVector GetNormalized() const { CVector v = *this; return v.Normalize(); };

	float ScalarMult( const CVector v ) const { return ( x * v.x ); };
	float Cosine( const CVector v ) const;
	float Sine( const CVector v ) const { return ( 0.0f ); };

	/*virtual*/ bool IsNull() const { return ( x == 0 ); };
	/*virtual*/ bool IsCollinear( const CVector v ) const { return ( true ); };
	/*virtual*/ bool IsOrtogonal( const CVector v ) const { return ( false ); };

	bool operator == ( const CVector v ) const { return ( x == v.x ); };
	bool operator != ( const CVector v ) const { return !( *this == v ); };
	CVector & operator += ( const CVector v ) { x += v.x; return ( *this ); };
	CVector & operator -= ( const CVector v ) { x -= v.x; return ( *this ); };
	CVector operator + ( const CVector v ) const { return CVector( x + v.x ); };
	CVector operator - ( const CVector v ) const { return CVector( x - v.x ); };
	bool operator < ( const CVector v ) const { return ( Length() < v.Length() ); };
	bool operator > ( const CVector v ) const { return ( Length() > v.Length() ); };
	bool operator >= ( const CVector v ) const { return !( *this < v ); };
	bool operator <= ( const CVector v ) const { return !( *this > v ); };
	float operator * ( const CVector v ) const { return ScalarMult( v ); };
	CVector operator * ( const float k ) const { return GetScaled( k ); };
	CVector & operator *= ( const float k ) { return Scale( k ); };
	CVector operator - () const { return GetInversed(); };
	
	float & operator [] ( const unsigned index ) { return *( &x + index ); };	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	const float & operator [] ( const unsigned index ) const { return *( &x + index ); };

	float * GetPtr() { return ( float * )&x; };	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	const float * const GetPtr() const { return ( float * )&x; };
};

//==================== 2D-vector ====================

class CVector2D : public CVector
{
public:
	float y;

	CVector2D() {};
	CVector2D( const float init_x, const float init_y ) { Set( init_x, init_y ); };
	CVector2D( const CVector2D & init ) { Set( init ); };
	CVector2D( const CVertex2D & init );
	CVector2D( const CVector & init ) { Set( init.x, 0.0f ); };
	CVector2D( const float * const array ) { LoadArray( array ); };
	~CVector2D() {};

	void LoadArray( const float * const array ) { Set( array[ 0 ], array[ 1 ] ); };

	void Set( const float new_x, const float new_y ) { x = new_x, y = new_y; };
	void Set( const CVector2D & v ) { Set( v.x, v.y ); };
	CVector2D & Inverse() { Set( -x, -y ); return ( *this ); };
	CVector2D GetInversed() const { return CVector2D( -x, -y ); };

	/*virtual*/ float Length() const { return sqrtf( Length2() ); };
	/*virtual*/ float Length2() const { return ScalarMult( *this ); };

	CVector2D & ScaleY( const float k ) { y *= k; return ( *this ); };
	/*virtual*/ CVector2D & Scale( const float k ) { CVector::Scale( k ); return ScaleY( k ); };
	CVector2D GetScaled( const float k ) const { CVector2D v = *this; return v.Scale( k ); };
	CVector2D & Normalize();
	CVector2D GetNormalized() const { CVector2D v = *this; return v.Normalize(); };

	float ScalarMult( const CVector2D v ) const { return ( x * v.x + y * v.y ); };
	float Cosine( const CVector2D v ) const;
	float Sine( const CVector2D v ) const;

	/*virtual*/ bool IsNull() const { return ( CVector::IsNull() && y == 0 ); };
	/*virtual*/ bool IsCollinear( const CVector2D v ) const;
	/*virtual*/ bool IsOrtogonal( const CVector2D v ) const { return ( *this * v == 0 ); };

	CVector2D & ProjX() { y = 0.0f; return ( *this ); };
	CVector2D & ProjY() { x = 0.0f; return ( *this ); };
	CVector2D GetProjX() const { CVector2D v = *this; return v.ProjX(); };
	CVector2D GetProjY() const { CVector2D v = *this; return v.ProjY(); };

	bool operator == ( const CVector2D v ) const { return ( x == v.x && y == v.y ); };
	bool operator != ( const CVector2D v ) const { return !( *this == v ); };
	CVector2D & operator += ( const CVector2D v ) { x += v.x, y += v.y; return ( *this ); };
	CVector2D & operator -= ( const CVector2D v ) { x -= v.x, y -= v.y; return ( *this ); };
	CVector2D operator + ( const CVector2D v ) const { return CVector2D( x + v.x, y + v.y ); };
	CVector2D operator - ( const CVector2D v ) const { return CVector2D( x - v.x, y - v.y ); };
	bool operator < ( const CVector2D v ) const { return ( Length2() < v.Length2() ); };
	bool operator > ( const CVector2D v ) const { return ( Length2() > v.Length2() ); };
	bool operator >= ( const CVector2D v ) const { return !( *this < v ); };
	bool operator <= ( const CVector2D v ) const { return !( *this > v ); };
	float operator * ( const CVector2D v ) const { return ScalarMult( v ); };
	CVector2D operator * ( const float k ) const { return GetScaled( k ); };
	CVector2D & operator *= ( const float k ) { return Scale( k ); };
	CVector2D operator - () const { return GetInversed(); };
};

//==================== 3D-vector ====================

class CVector3D : public CVector2D
{
public:
	float z;

	CVector3D() {};
	CVector3D( const float init_x, const float init_y, const float init_z ) { Set( init_x, init_y, init_z ); };
	CVector3D( const CVector3D & init ) { Set( init ); };
	CVector3D( const CVertex3D & init );
	CVector3D( const CVector2D & init ) { Set( init.x, init.y, 0.0f ); };
	CVector3D( const float * const array ) { LoadArray( array ); };
	~CVector3D() {};

	void LoadArray( const float * const array ) { Set( array[ 0 ], array[ 1 ], array[ 2 ] ); };

	void Set( const float new_x, const float new_y, const float new_z ) { x = new_x, y = new_y, z = new_z; };
	void Set( const CVector3D & v ) { Set( v.x, v.y, v.z ); };
	CVector3D & Inverse() { Set( -x, -y, -z ); return ( *this ); };
	CVector3D GetInversed() const { return CVector3D( -x, -y, -z ); };

	/*virtual*/ float Length() const { return sqrtf( Length2() ); };
	/*virtual*/ float Length2() const { return ScalarMult( *this ); };

	CVector3D & ScaleZ( const float k ) { z *= k; return ( *this ); };
	/*virtual*/ CVector3D & Scale( const float k ) { CVector2D::Scale( k ); return ScaleZ( k ); };
	CVector3D GetScaled( const float k ) const { CVector3D v = *this; return v.Scale( k ); };
	CVector3D & Normalize();
	CVector3D GetNormalized() const { CVector3D v = *this; return v.Normalize(); };

	CVector3D & VectorMult( const CVector3D v );
	CVector3D GetVectorMult( const CVector3D v ) const { CVector3D mult = *this; return mult.VectorMult( v ); };
	float ScalarMult( const CVector3D v ) const { return ( x * v.x + y * v.y + z * v.z ); };
	float Cosine( const CVector3D v ) const;
	float Sine( const CVector3D v ) const;

	/*virtual*/ bool IsNull() const { return ( CVector2D::IsNull() && z == 0 ); };
	/*virtual*/ bool IsCollinear( const CVector3D v ) const { return ( *this ^ v ).IsNull(); };
	/*virtual*/ bool IsOrtogonal( const CVector3D v ) const { return ( *this * v == 0 ); };

	CVector3D & ProjXY() { z = 0.0f; };
	CVector3D & ProjYZ() { x = 0.0f; };
	CVector3D & ProjXZ() { y = 0.0f; };
	CVector3D GetProjXY() const { CVector3D v = *this; return v.ProjXY(); };
	CVector3D GetProjYZ() const { CVector3D v = *this; return v.ProjYZ(); };
	CVector3D GetProjXZ() const { CVector3D v = *this; return v.ProjXZ(); };

	bool operator == ( const CVector3D v ) const { return ( x == v.x && y == v.y && z == v.z ); };
	bool operator != ( const CVector3D v ) const { return !( *this == v ); };
	CVector3D & operator += ( const CVector3D v ) { x += v.x, y += v.y, z += v.z; return ( *this ); };
	CVector3D & operator -= ( const CVector3D v ) { x -= v.x, y -= v.y, z -= v.z; return ( *this ); };
	CVector3D operator + ( const CVector3D v ) const { return CVector3D( x + v.x, y + v.y, z + v.z ); };
	CVector3D operator - ( const CVector3D v ) const { return CVector3D( x - v.x, y - v.y, z - v.z ); };
	bool operator < ( const CVector3D v ) const { return ( Length2() < v.Length2() ); };
	bool operator > ( const CVector3D v ) const { return ( Length2() > v.Length2() ); };
	bool operator >= ( const CVector3D v ) const { return !( *this < v ); };
	bool operator <= ( const CVector3D v ) const { return !( *this > v ); };
	float operator * ( const CVector3D v ) const { return ScalarMult( v ); };
	CVector3D operator * ( const float k ) const { return GetScaled( k ); };
	CVector3D & operator *= ( const float k ) { return Scale( k ); };
	CVector3D operator ^ ( const CVector3D v ) const { return GetVectorMult( v ); };
	CVector3D & operator ^= ( const CVector3D v ) { return VectorMult( v ); };
	CVector3D operator - () const { return GetInversed(); };
};

#endif