#ifndef IMAGE_H
#define IMAGE_H


#include "Header.h"


class CImage : virtual public CHeader, public CBytesArray
{
public:
	CImage() {};
	virtual ~CImage() {};

	bool LoadImage( FILE * const pFile );
	bool LoadImage( LPCTSTR szFileName );

	virtual LONG GetWidth() const = NULL;
	virtual LONG GetHeight() const = NULL;

private:
	bool LoadData( FILE * const pFile, const bool flip = false );
};

#endif
