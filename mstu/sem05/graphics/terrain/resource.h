//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Terrain.rc
//
#define IDDEFAULT                       3
#define IDD_SETUP                       101
#define IDI_APPICON                     103
#define IDC_SC640                       1003
#define IDC_SC800                       1004
#define IDC_SC1024                      1005
#define IDC_FULLSCREEN                  1009
#define IDC_HEIGHTMAP                   1010
#define IDC_HEIGHT_SCALING              1012
#define IDC_GRID_SIZE                   1013
#define IDC_TX64                        1014
#define IDC_TX128                       1015
#define IDC_TX256                       1016
#define IDC_TX512                       1017
#define IDC_TX1024                      1018
#define IDC_TX2048                      1019
#define IDC_HSR                         1030
#define IDC_MAX_TRI_IN_NODE             1031
#define IDC_MIN_NODE_SIZE               1032
#define IDC_SHADOWS                     1034
#define IDC_AMBIENT                     1035
#define IDC_DRAW_HBODY_FLARE              1037
#define IDREADME                        1040
#define IDC_FOV                         1041
#define IDC_NEAR                        1042
#define IDC_FAR                         1043
#define IDC_CROSSHAIR                   1044
#define IDC_STATISTICS                  1045
#define IDC_PREVIEW                     1048
#define IDC_RELOAD                      1049
#define IDC_LIGHT_ANGLE                 1051
#define IDC_SIMPLE_LIGHTING             1052
#define IDC_SUNNY_DAY                   1054
#define IDC_SUNSET                      1055
#define IDC_MOON_NIGHT                  1056
#define IDC_REMOVE_ARTEFACTS            1057
#define IDC_COLLISION                   1058

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1059
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
