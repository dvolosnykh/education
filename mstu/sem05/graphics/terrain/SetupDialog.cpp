#include "stdafx.h"

#include <shellapi.h>

#include "SetupDialog.h"


param_t		CSetupDialog::m_Param;
param_t		CSetupDialog::m_OldParam;
canvas_t	CSetupDialog::preview;
bool		CSetupDialog::map_loaded;
HWND		CSetupDialog::m_hDlg = NULL;
TCHAR		CSetupDialog::buffer[ _MAX_PATH ];

param_t & g_param = CSetupDialog::m_Param;


bool canvas_t::InRect( const LONG x, const LONG y ) const
{
	const POINT pos = { x - origin.x, y - origin.y };
	return ( 0 <= pos.x && pos.x < width && 0 <= pos.y && pos.y < height );
}


int APIENTRY CSetupDialog::Main()
{
	return ( int )DialogBox( CApp::m_hInstance, ( LPCTSTR )IDD_SETUP, NULL, DlgProc );
}

bool CSetupDialog::GetItemCheck( const unsigned item_id )
{
	return IsDlgButtonChecked( m_hDlg, item_id ) != BST_UNCHECKED;
}

void CSetupDialog::SetItemCheck( const unsigned item_id, const bool check /* = true */ )
{
	const unsigned bst = check ? BST_CHECKED : BST_UNCHECKED;
	SendDlgItemMessage( m_hDlg, item_id, BM_SETCHECK, bst, 0 );
}

BOOL CSetupDialog::CheckRadioButton( const unsigned first, const unsigned last, const unsigned desired )
{
	return ::CheckRadioButton( m_hDlg, first, last, desired );
}

void CSetupDialog::ClickButton( const unsigned item_id )
{
	SendDlgItemMessage( m_hDlg, item_id, BN_CLICKED, 0, 0 );
}

UINT CSetupDialog::GetItemText( const unsigned item_id, LPTSTR buffer, int count )
{
	return GetDlgItemText( m_hDlg, item_id, buffer, count );
}

BOOL CSetupDialog::SetItemText( const unsigned item_id, LPTSTR buffer )
{
	return SetDlgItemText( m_hDlg, item_id, buffer );
}

BOOL CSetupDialog::GetItemValue( const unsigned item_id, const unsigned type, void * pValue )
{
	BOOL success = FALSE;

	switch ( type )
	{
	case VAL_FLOAT:
		if ( success = ( BOOL )GetItemText( item_id, buffer, sizeof( buffer ) ) )
		{
			TCHAR * endptr;
			*( FLOAT * )pValue = ( FLOAT )_tcstod( buffer, &endptr );
			success = *endptr == TCHAR( '\0' );
		}
		break;

	case VAL_INT:
		*( INT * )pValue = ( INT )GetDlgItemInt( m_hDlg, item_id, &success, TRUE );
		break;

	case VAL_UINT:
		*( UINT * )pValue = GetDlgItemInt( m_hDlg, item_id, &success, FALSE );
		break;

	case VAL_BYTE:
		*( BYTE * )pValue = ( BYTE )GetDlgItemInt( m_hDlg, item_id, &success, FALSE );
		break;
	}

	return ( success );
}

BOOL CSetupDialog::SetItemValue( const unsigned item_id, const unsigned type, const void * pValue )
{
	BOOL success = FALSE;

	switch ( type )
	{
	case VAL_FLOAT:
        _stprintf( buffer, TEXT( "%.2f" ), *( const FLOAT * )pValue );
		success = SetItemText( item_id, buffer );
		break;

	case VAL_INT:
		success = SetDlgItemInt( m_hDlg, item_id, *( const UINT * )pValue, TRUE );
		break;

	case VAL_UINT:
		success = SetDlgItemInt( m_hDlg, item_id, *( const UINT * )pValue, FALSE );
		break;

	case VAL_BYTE:
		success = SetDlgItemInt( m_hDlg, item_id, *( const BYTE * )pValue, FALSE );
		break;
	}

	return ( success );
}

void CSetupDialog::InitSettings( bool defaults /* = true */ )
{
	// Paths to resources
	_tcscpy( m_Param.szTextureSearchPath, PATH_TEXTURE );
	_tcscpy( m_Param.szWaterTexture, PATH_WATER );
	_tcscpy( m_Param.szCrosshairTexture, PATH_CROSSHAIR );
	_tcscpy( m_Param.szHeavenlyBodySearchPath, PATH_HEAVENLY_BODY );
	_tcscpy( m_Param.szSkyBoxSearchPath, PATH_SKY );
	_tcscpy( m_Param.szLogoBitmap, PATH_LOGO );
	_tcscpy( m_Param.szHeightmapPath, PATH_HEIGHTMAP );

	// Heightmap
	SendMessage( m_hDlg, WM_COMMAND, MAKEWPARAM( IDC_RELOAD, BN_CLICKED ),
		( LPARAM )GetDlgItem( m_hDlg, IDC_RELOAD ) );

	if ( defaults )
		DefaultSettings( m_Param );
	else
		LoadSettings( m_Param );

	m_OldParam = m_Param;
}

void CSetupDialog::DefaultSettings( param_t & params )
{
	FLOAT fValue;	UINT uValue;	BYTE bValue;

	// Display
	SetItemCheck( IDC_FULLSCREEN, FULLSCREEN );
	CheckRadioButton( IDC_SC640, IDC_SC1024, RESOLUTION );

	// Texture
	CheckRadioButton( IDC_TX64, IDC_TX2048, TEX_SIZE );

	// Water
//	SendDlgItemMessage( m_hDlg, IDC_WD_MID, BM_SETCHECK, 1, 0 );
//	SendDlgItemMessage( m_hDlg, IDC_WS_MID, BM_SETCHECK, 1, 0 );
//	SendDlgItemMessage( m_hDlg, IDC_DRAW_WATER, BM_SETCHECK, 1, 0 );
//	SetDlgItemText( m_hDlg, IDC_WAVE_HEIGHT, "6.5" );

	// Octree
	uValue = MAX_TRI_IN_NODE;	SetItemValue( IDC_MAX_TRI_IN_NODE, VAL_UINT, &uValue );
	fValue = MIN_NODE_SIZE;		SetItemValue( IDC_MIN_NODE_SIZE, VAL_FLOAT, &fValue );

	fValue = GRID_STEP;
	SetItemValue( IDC_GRID_SIZE, VAL_FLOAT, &fValue );

	// Lighting
	SetItemCheck( IDC_SIMPLE_LIGHTING, SIMPLE_LIGHTING );
	SetItemCheck( IDC_SHADOWS, CAST_SHADOWS );
	fValue = LIGHT_ANGLE;	SetItemValue( IDC_LIGHT_ANGLE, VAL_FLOAT, &fValue );
	bValue = AMBIENT;		SetItemValue( IDC_AMBIENT, VAL_BYTE, &bValue );
	CheckRadioButton( IDC_SUNNY_DAY, IDC_MOON_NIGHT, DAY_TIME );

	// Frustum
	fValue = FOV_Y;			SetItemValue( IDC_FOV, VAL_FLOAT, &fValue );
	fValue = NEAR_PLANE;	SetItemValue( IDC_NEAR, VAL_FLOAT, &fValue );
	fValue = FAR_PLANE;		SetItemValue( IDC_FAR, VAL_FLOAT, &fValue );
	SetItemCheck( IDC_HSR, USE_HSR );

	// Misc
	SetItemCheck( IDC_DRAW_HBODY_FLARE, DRAW_HBODY );
	SetItemCheck( IDC_STATISTICS, SHOW_STATS );
	SetItemCheck( IDC_CROSSHAIR, DRAW_CROSSHAIR );
	SetItemCheck( IDC_REMOVE_ARTEFACTS, REMOVE_ARTEFACTS );
	SetItemCheck( IDC_COLLISION, CHECK_COLLISION );

	params.light_dir.start.x = preview.width - 1;
	params.light_dir.start.y = 0;
	params.light_dir.end.x = preview.width >> 1;
	params.light_dir.end.y = preview.height >> 1;

	PreviewMap();

	CGL::m_params.new_hmap = true;
	CGL::m_params.build_octree = true;
	CGL::m_params.tex_changed = true;
	CGL::m_params.light_changed = true;
}

bool CSetupDialog::SaveSettings( param_t & params )
{
	FLOAT fValue;	UINT uValue;

////////////////////////////////////////////////////////////////////////
// Display
////////////////////////////////////////////////////////////////////////

	params.Fullscreen = GetItemCheck( IDC_FULLSCREEN );
	if ( GetItemCheck( IDC_SC640 ) )		params.ResWidth =  640, params.ResHeight = 480;
	else if ( GetItemCheck( IDC_SC800 ) )	params.ResWidth =  800, params.ResHeight = 600;
	else if ( GetItemCheck( IDC_SC1024 ) )	params.ResWidth = 1024, params.ResHeight = 768;

////////////////////////////////////////////////////////////////////////
// Texture resolution
////////////////////////////////////////////////////////////////////////

	if ( GetItemCheck( IDC_TX64 ) )			params.TexSize =   64;
	else if ( GetItemCheck( IDC_TX128 ) )	params.TexSize =  128;
	else if ( GetItemCheck( IDC_TX256 ) )	params.TexSize =  256;
	else if ( GetItemCheck( IDC_TX512 ) )	params.TexSize =  512;
	else if ( GetItemCheck( IDC_TX1024 ) )	params.TexSize = 1024;
	else if ( GetItemCheck( IDC_TX2048 ) )	params.TexSize = 2048;

////////////////////////////////////////////////////////////////////////
// Water
////////////////////////////////////////////////////////////////////////
/*
	// Render ?
	params.RenderWater = SendDlgItemMessage( m_hDlg, IDC_DRAW_WATER, BM_GETCHECK, 0, 0 ) == BST_CHECKED;

	// Water detail
	if ( SendDlgItemMessage( m_hDlg, IDC_WD_LOW, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterTesselation = 50;
	else if ( SendDlgItemMessage( m_hDlg, IDC_WD_MID, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterTesselation = 100;
	else if ( SendDlgItemMessage( m_hDlg, IDC_WD_HIGH, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterTesselation = 150;

	// Water speed
	if ( SendDlgItemMessage( m_hDlg, IDC_WS_LOW, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterSpeed = 0.2f;
	else if ( SendDlgItemMessage( m_hDlg, IDC_WS_MID, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterSpeed = 0.4f;
	else if ( SendDlgItemMessage( m_hDlg, IDC_WS_HIGH, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterSpeed = 0.6f;

	// Wave height
	GetDlgItemText( m_hDlg, IDC_WAVE_HEIGHT, szBuffer, sizeof( szBuffer ) );
	fValue = ( float )_tstof( szBuffer );
	// Verify
	if ( fValue < 1.0f || fValue > 15.0f )
	{
		MessageBox( m_hDlg, TEXT( "Enter a value between 1 and 15 for the wave height!" ),
			TEXT( "Invalid Input" ), MB_ICONERROR );
		return false;
	}
	else
		params.WaveHeight = fValue;
*/
////////////////////////////////////////////////////////////////////////
// Octree
////////////////////////////////////////////////////////////////////////

	// Maximum number of triangles in a single node
	GetItemValue( IDC_MAX_TRI_IN_NODE, VAL_UINT, &uValue );
	if ( uValue <= 0 || uValue > 100 )
	{
		MessageBox( m_hDlg, TEXT( "������� ����� ����� �� ������� [ 0, 100 ] ��� ���� \"����. ����� ������������� � ����\"." ),
			TEXT( "������������ ����" ), MB_ICONERROR );
//		SendDlgItemMessage( m_hDlg, IDC_MAX_TRI_IN_NODE, WM_SETFOCUS, NULL, 0 );
		return false;
	}
	else
		params.MaxTriInOctNode = uValue;

	// Minimum node size.
	GetItemValue( IDC_MIN_NODE_SIZE, VAL_FLOAT, &fValue );
	if ( fValue < 5.0f || fValue > 500.0f )
	{
		MessageBox( m_hDlg, TEXT( "������� ����� �� ������� [ 5.0, 500.0 ] ��� ���� \"����������� ������ ����\"." ),
			TEXT( "������������ ����" ), MB_ICONERROR );
//		SendDlgItemMessage( m_hDlg, IDC_MIN_NODE_SIZE, WM_SETFOCUS, NULL, 0 );
		return false;
	}
	else
		params.MinOctNodeSize = fValue;

////////////////////////////////////////////////////////////////////////
// Heightmap
////////////////////////////////////////////////////////////////////////

	// Heightmap file
	if ( SendDlgItemMessage( m_hDlg, IDC_HEIGHTMAP, LB_GETCURSEL, 0, 0 ) == LB_ERR )
	{
		// No file selected
		MessageBox( m_hDlg, TEXT( "���������� ������� ����� �����." ),
			TEXT( "������" ), MB_ICONERROR );
		return false;
	}

	// Grid size
	GetItemValue( IDC_GRID_SIZE, VAL_FLOAT, &fValue );
	if ( fValue < 1.0f || fValue > 500.0f )
	{
		MessageBox( m_hDlg, TEXT( "������� ����� �� ������� [ 1.0, 500.0 ] ��� ���� \"��� �����\"." ),
			TEXT( "������������ ����" ), MB_ICONERROR );
//		SendDlgItemMessage( m_hDlg, IDC_GRID_SIZE, WM_SETFOCUS, NULL, 0 );
		return false;
	}
	else
		params.GridStep = fValue;

	////////////////////////////////////////////////////////////////////////
	// Lighting
	////////////////////////////////////////////////////////////////////////

	// Shadows?
	params.SimpleLighting = GetItemCheck( IDC_SIMPLE_LIGHTING );
	params.Shadows = GetItemCheck( IDC_SHADOWS );
	GetItemValue( IDC_LIGHT_ANGLE, VAL_FLOAT, &params.light_angle );
	if ( GetItemCheck( IDC_SUNNY_DAY ) )		params.DayTime = IDC_SUNNY_DAY;
	else if ( GetItemCheck( IDC_SUNSET ) )		params.DayTime = IDC_SUNSET;
	else if ( GetItemCheck( IDC_MOON_NIGHT ) )	params.DayTime = IDC_MOON_NIGHT;

	// Ambient brightness
	GetItemValue( IDC_AMBIENT, VAL_UINT, &uValue );
	if ( uValue > UCHAR_MAX )
	{
		MessageBox( m_hDlg, TEXT( "������� ����� ����� �� ������� [ 0, 255 ] ��� ���� \"��������� ���������\"." ),
			TEXT( "������������ ����" ), MB_ICONERROR );
//		SendDlgItemMessage( m_hDlg, IDC_AMBIENT, WM_SETFOCUS, NULL, 0 );
		return false;
	}
	else
		params.Ambient = ( BYTE )uValue;

	////////////////////////////////////////////////////////////////////////
	// Frustum
	////////////////////////////////////////////////////////////////////////

	// FOV
	GetItemValue( IDC_FOV, VAL_FLOAT, &fValue );
	if ( fValue < 10.0f || fValue > 170.0f )
	{
		MessageBox( m_hDlg, TEXT( "������� ����� �� ������� [ 10.0, 170.0 ] ��� ���� \"���� ������\"." ),
			TEXT( "������������ ����" ), MB_ICONERROR );
//		SendDlgItemMessage( m_hDlg, IDC_FOV, WM_SETFOCUS, NULL, 0 );
		return false;
	}
	else
		params.FOV = fValue;

	// Near clipping plane
	GetItemValue( IDC_NEAR, VAL_FLOAT, &fValue );
	if ( fValue < 0.5f || fValue > 50.0f )
	{
		MessageBox( m_hDlg, TEXT( "������� ����� �� ������� [ 0.5, 50.0 ] ��� ���� \"���. ���������\"." ),
			TEXT( "������������ ����" ), MB_ICONERROR );
//		SendDlgItemMessage( m_hDlg, IDC_NEAR, WM_SETFOCUS, NULL, 0 );
		return false;
	}
	else
		params.NearPlane = fValue;

	// Far clipping plane
	GetItemValue( IDC_FAR, VAL_FLOAT, &fValue );
	if ( fValue < 500.0f || fValue > 50000.0f )
	{
		MessageBox( m_hDlg, TEXT( "������� ����� �� ������� [ 500.0, 50000.0 ] ��� ���� \"����. ���������\"." ),
			TEXT( "������������ ����" ), MB_ICONERROR );
//		SendDlgItemMessage( m_hDlg, IDC_FAR, WM_SETFOCUS, NULL, 0 );
		return false;
	}
	else
		params.FarPlane = fValue;

	// Use HSR?
	params.UseHSR = GetItemCheck( IDC_HSR );

	// Misc.
	params.DrawHeavenlyBody = GetItemCheck( IDC_DRAW_HBODY_FLARE );
	params.DrawCrosshair = GetItemCheck( IDC_CROSSHAIR );
	params.ShowStats = GetItemCheck( IDC_STATISTICS );
	params.RemoveArtifacts = GetItemCheck( IDC_REMOVE_ARTEFACTS );
	params.CheckCollision = GetItemCheck( IDC_COLLISION );

	// All settings saved, we can close the dialog
	return true;
}

bool CSetupDialog::LoadSettings( const param_t & params )
{
////////////////////////////////////////////////////////////////////////
// Display
////////////////////////////////////////////////////////////////////////

	SetItemCheck( IDC_FULLSCREEN, params.Fullscreen );
	switch ( params.ResWidth )
	{
	case 640:	SetItemCheck( IDC_SC640 );	break;
	case 800:	SetItemCheck( IDC_SC800 );	break;
	case 1024:	SetItemCheck( IDC_SC1024 );	break;
	}

////////////////////////////////////////////////////////////////////////
// Texture resolution
////////////////////////////////////////////////////////////////////////

	switch ( params.TexSize )
	{
	case 64:	SetItemCheck( IDC_TX64 );	break;
	case 128:	SetItemCheck( IDC_TX128 );	break;
	case 256:	SetItemCheck( IDC_TX256 );	break;
	case 512:	SetItemCheck( IDC_TX512 );	break;
	case 1024:	SetItemCheck( IDC_TX1024 );	break;
	case 2048:	SetItemCheck( IDC_TX2048 );	break;
	}

////////////////////////////////////////////////////////////////////////
// Water
////////////////////////////////////////////////////////////////////////
/*
	// Render ?
	params.RenderWater = SendDlgItemMessage( m_hDlg, IDC_DRAW_WATER, BM_GETCHECK, 0, 0 ) == BST_CHECKED;

	// Water detail
	if ( SendDlgItemMessage( m_hDlg, IDC_WD_LOW, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterTesselation = 50;
	else if ( SendDlgItemMessage( m_hDlg, IDC_WD_MID, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterTesselation = 100;
	else if ( SendDlgItemMessage( m_hDlg, IDC_WD_HIGH, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterTesselation = 150;

	// Water speed
	if ( SendDlgItemMessage( m_hDlg, IDC_WS_LOW, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterSpeed = 0.2f;
	else if ( SendDlgItemMessage( m_hDlg, IDC_WS_MID, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterSpeed = 0.4f;
	else if ( SendDlgItemMessage( m_hDlg, IDC_WS_HIGH, BM_GETCHECK, 0, 0 ) == BST_CHECKED )
		params.WaterSpeed = 0.6f;

	// Wave height
	GetDlgItemText( m_hDlg, IDC_WAVE_HEIGHT, szBuffer, sizeof( szBuffer ) );
	fValue = ( float )_tstof( szBuffer );
	// Verify
	if ( fValue < 1.0f || fValue > 15.0f )
	{
		MessageBox( m_hDlg, TEXT( "Enter a value between 1 and 15 for the wave height!" ),
			TEXT( "Invalid Input" ), MB_ICONERROR );
		return false;
	}
	else
		params.WaveHeight = fValue;
*/
////////////////////////////////////////////////////////////////////////
// Octree
////////////////////////////////////////////////////////////////////////

	// Maximum number of triangles in a single node
	SetItemValue( IDC_MAX_TRI_IN_NODE, VAL_UINT, &params.MaxTriInOctNode );

	// Minimum node size.
	SetItemValue( IDC_MIN_NODE_SIZE, VAL_FLOAT, &params.MinOctNodeSize );

////////////////////////////////////////////////////////////////////////
// Heightmap
////////////////////////////////////////////////////////////////////////

	// Grid size
	SetItemValue( IDC_GRID_SIZE, VAL_FLOAT, &params.GridStep );

	////////////////////////////////////////////////////////////////////////
	// Lighting
	////////////////////////////////////////////////////////////////////////

	// Shadows?
	SetItemCheck( IDC_SIMPLE_LIGHTING, params.SimpleLighting );
	SetItemCheck( IDC_SHADOWS, params.Shadows );
	SetItemValue( IDC_LIGHT_ANGLE, VAL_FLOAT, &params.light_angle );
	switch ( params.DayTime )
	{
	case IDC_SUNNY_DAY:		SetItemCheck( IDC_SUNNY_DAY );	break;
	case IDC_SUNSET:		SetItemCheck( IDC_SUNSET );		break;
	case IDC_MOON_NIGHT:	SetItemCheck( IDC_MOON_NIGHT );	break;
	}

	// Ambient brightness
	SetItemValue( IDC_AMBIENT, VAL_BYTE, &params.Ambient );

	////////////////////////////////////////////////////////////////////////
	// Frustum
	////////////////////////////////////////////////////////////////////////

	// FOV
	SetItemValue( IDC_FOV, VAL_FLOAT, &params.FOV );

	// Near clipping plane
	SetItemValue( IDC_NEAR, VAL_FLOAT, &params.NearPlane );

	// Far clipping plane
	SetItemValue( IDC_FAR, VAL_FLOAT, &params.FarPlane );

	// Use HSR?
	SetItemCheck( IDC_HSR, params.UseHSR );

	// Misc.
	SetItemCheck( IDC_DRAW_HBODY_FLARE, params.DrawHeavenlyBody );
	SetItemCheck( IDC_CROSSHAIR, params.DrawCrosshair );
	SetItemCheck( IDC_STATISTICS, params.ShowStats );
	SetItemCheck( IDC_REMOVE_ARTEFACTS, params.RemoveArtifacts );
	SetItemCheck( IDC_COLLISION, params.CheckCollision );

	CGL::m_params.new_hmap = false;
	CGL::m_params.build_octree = false;
	CGL::m_params.tex_changed = false;
	CGL::m_params.light_changed = false;

	// All settings saved, we can close the dialog
	return true;
}

void CSetupDialog::LoadMapList()
{
	TCHAR szBuffer[ _MAX_PATH ];

	_tcscpy( szBuffer, m_Param.szHeightmapPath );
	size_t len = _tcslen( szBuffer );
	_tcscpy( szBuffer + len, TEXT( "*." ) );
	len += 2;
	SendDlgItemMessage( m_hDlg, IDC_HEIGHTMAP, LB_RESETCONTENT, 0, 0 );

	_tcscpy( szBuffer + len, TEXT( "tga" ) );
	SendDlgItemMessage( m_hDlg, IDC_HEIGHTMAP, LB_DIR, 0, ( LPARAM )szBuffer );

	_tcscpy( szBuffer + len, TEXT( "bmp" ) );
	SendDlgItemMessage( m_hDlg, IDC_HEIGHTMAP, LB_DIR, 0, ( LPARAM )szBuffer );
}

bool CSetupDialog::LoadMap()
{
	bool isOK = true;

	if ( !map_loaded )
	{
		LRESULT index;

		if ( SendDlgItemMessage( m_hDlg, IDC_HEIGHTMAP, LB_GETCOUNT, 0, 0 ) == 0 )
		{
			MessageBox( m_hDlg, TEXT( "� ������ ��� �� ����� ����� �����." ),
				TEXT( "������" ), MB_ICONERROR );
			isOK = false;
		}
		else if ( ( index = SendDlgItemMessage( m_hDlg, IDC_HEIGHTMAP, LB_GETCURSEL, 0, 0 ) ) == LB_ERR )
		{
			MessageBox( m_hDlg, TEXT( "����� ����� �� �������." ),
				TEXT( "������" ), MB_ICONERROR );
			isOK = false;
		}
		else
		{
			TCHAR szBuffer[ _MAX_PATH ];

			_tcscpy( szBuffer, m_Param.szHeightmapPath );
			SendDlgItemMessage( m_hDlg, IDC_HEIGHTMAP, LB_GETTEXT, index,
				( LPARAM )( szBuffer + _tcslen( szBuffer ) ) );

			if ( !( isOK = CGL::Heightmap.Load( szBuffer ) ) )
			{
				MessageBox( m_hDlg, TEXT( "�� ��������� �������� ����� �����." ), 
					TEXT( "������" ), MB_ICONERROR );
				isOK = false;
			}
			else
			{
				CGL::m_params.new_hmap = true;
				map_loaded = true;
			}
		}
	}

	return ( isOK );
}

void CSetupDialog::PreviewMap( const bool in_paint /* = false */ )
{
	HWND hPreview = GetDlgItem( m_hDlg, IDC_PREVIEW );

	PAINTSTRUCT PaintStruct;
	HDC hDC;
	RECT rect;
	if ( in_paint )
	{
		hDC = BeginPaint( hPreview, &PaintStruct );
		rect = PaintStruct.rcPaint;
	}
	else
	{
		hDC = GetDC( hPreview );
		GetClientRect( hPreview, &rect );
	}
	IntersectClipRect( hDC, rect.left, rect.top, rect.right, rect.bottom );

	// draw heightmap.
	const CHeightmap & hmap = CGL::Heightmap;
	const float step_x = hmap.GetWidth() / ( float )preview.width;
	const float step_y = hmap.GetHeight() / ( float )preview.height;
	LONG i, j;
	float x, y;
	for ( i = 0, y = 0.0f; i < preview.height; i++, y += step_y )
		for ( j = 0, x = 0.0f; j < preview.width; j++, x += step_x )
		{
			const BYTE & h = hmap[ long( y ) * hmap.GetWidth() + long( x ) ];
			SetPixel( hDC, j, i, RGB( h, h, h) );
		}

	// draw light-arrow.
	HPEN hPen = CreatePen( PS_SOLID, 0, RGB( UCHAR_MAX, 0, 0 ) );
	HGDIOBJ old_obj = SelectObject( hDC, hPen );

	const POINT & start = m_Param.light_dir.start;
	const POINT & end = m_Param.light_dir.end;
	const POINT d = { end.x - start.x, end.y - start.y };

	const float angle = atan2f( ( float )d.y, ( float )d.x );

	MoveToEx( hDC, start.x, start.y, NULL );
	LineTo( hDC, end.x, end.y );

#define RELATIVE_LEN	0.33f
	float length = sqrtf( float( d.x * d.x + d.y * d.y ) ) * RELATIVE_LEN;
#undef RELATIVE_LEN
#define MAX_LEN	13.0f
	if ( length > MAX_LEN ) length = MAX_LEN;
#undef MAX_LEN

	float phi;
	POINT p;

	phi = angle + ( float )M_PI / 9.0f;
	p.x = end.x - LONG( length * cosf( phi ) );
	p.y = end.y - LONG( length * sinf( phi ) );
	LineTo( hDC, p.x, p.y );

	phi = angle - ( float )M_PI / 9.0f;
	p.x = end.x - LONG( length * cosf( phi ) );
	p.y = end.y - LONG( length * sinf( phi ) );
	MoveToEx( hDC, end.x, end.y, NULL );
	LineTo( hDC, p.x, p.y );

	SelectObject( hDC, old_obj );
	DeleteObject( hPen );

	if ( in_paint )
		EndPaint( hPreview, &PaintStruct );
	else
		ReleaseDC( hPreview, hDC );
}

BOOL CALLBACK CSetupDialog::DlgProc( HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam )
{
	static POINT temp_start;

	static POINT & start = m_Param.light_dir.start;
	static POINT & end = m_Param.light_dir.end;

	BOOL result = FALSE;

	switch ( msg )
	{
	case WM_INITDIALOG:
		map_loaded = false;

		{
		HWND hPreview = GetDlgItem( hDlg, IDC_PREVIEW );
		RECT rect;
		GetClientRect( hPreview, &rect );

		preview.width = rect.right;
		preview.height = rect.bottom;
		preview.origin.x = rect.left;
		preview.origin.y = rect.top;
		MapWindowPoints( hPreview, hDlg, &preview.origin, 1 );

		bool defaults = m_hDlg == NULL;
		m_hDlg = hDlg;
		InitSettings( defaults );
		}

		result = TRUE;
		break;

	case WM_PAINT:
		if ( LoadMap() ) PreviewMap( true );
		break;

	case WM_LBUTTONDOWN:
		{
		POINT point = { GET_X_LPARAM( lParam ), GET_Y_LPARAM( lParam ) };
		if ( preview.InRect( point ) )
		{
			temp_start.x = point.x - preview.origin.x;
			temp_start.y = point.y - preview.origin.y;
		}
		}
		break;

	case WM_MOUSEMOVE:
		{
		POINT point = { GET_X_LPARAM( lParam ), GET_Y_LPARAM( lParam ) };
		if ( wParam & MK_LBUTTON && preview.InRect( point ) )
		{
			start.x = temp_start.x;
			start.y = temp_start.y;
			end.x = point.x - preview.origin.x;
			end.y = point.y - preview.origin.y;

			if ( LoadMap() ) PreviewMap();

			CGL::m_params.light_changed = true;
		}
		}
		break;

	case WM_LBUTTONUP:
		{
		POINT point = { GET_X_LPARAM( lParam ), GET_Y_LPARAM( lParam ) };
		if ( preview.InRect( point ) )
		{
			end.x = point.x - preview.origin.x;
			end.y = point.y - preview.origin.y;
		}
		}
		break;

	case WM_COMMAND:
		{
		const int wmID = LOWORD( wParam );
		const int wmEvent = HIWORD( wParam );

		switch ( wmEvent )
		{
		case EN_CHANGE:
			switch ( wmID )
			{
			case IDC_GRID_SIZE:
				{
				float fValue;
				GetItemValue( wmID, VAL_FLOAT, &fValue );
				CGL::m_params.new_hmap = fValue != m_OldParam.GridStep;
				}
				break;

			case IDC_MAX_TRI_IN_NODE:
				{
				UINT uValue;
				GetItemValue( wmID, VAL_UINT, &uValue );
				CGL::m_params.build_octree = uValue != m_OldParam.MaxTriInOctNode;
				}
				break;

			case IDC_MIN_NODE_SIZE:
				{
				float fValue;
				GetItemValue( wmID, VAL_FLOAT, &fValue );
				CGL::m_params.build_octree = fValue != m_OldParam.MinOctNodeSize;
				}
				break;

			case IDC_AMBIENT:
				{
				UINT uValue;
				GetItemValue( wmID, VAL_UINT, &uValue );
				CGL::m_params.light_changed = uValue != ( UINT )m_OldParam.Ambient;
				}
				break;

			case IDC_LIGHT_ANGLE:
				{
				float fValue;
				GetItemValue( wmID, VAL_FLOAT, &fValue );
				CGL::m_params.light_changed = fValue != m_OldParam.light_angle;
				}
				break;
			}
			break;

		case BN_CLICKED:
			switch ( wmID )
			{
			case IDC_TX64:		m_Param.TexSize = 64;
			case IDC_TX128:		m_Param.TexSize = 128;
			case IDC_TX256:		m_Param.TexSize = 256;
			case IDC_TX512:		m_Param.TexSize = 512;
			case IDC_TX1024:	m_Param.TexSize = 1024;
			case IDC_TX2048:	m_Param.TexSize = 2048;
				CGL::m_params.tex_changed = m_Param.TexSize != m_OldParam.TexSize;
				break;

			case IDC_SIMPLE_LIGHTING:
				CGL::m_params.light_changed = GetItemCheck( wmID ) == m_OldParam.SimpleLighting;
				break;

			case IDC_SHADOWS:
				CGL::m_params.light_changed = GetItemCheck( wmID ) == m_OldParam.Shadows;
				break;

			case IDC_SUNNY_DAY:		case IDC_SUNSET:	case IDC_MOON_NIGHT:
				CGL::m_params.light_changed = wmID != m_OldParam.DayTime;
				break;
			}
			break;
		}

		switch ( wmID )
		{
		case IDC_HEIGHTMAP:
			switch ( wmEvent )
			{
			case LBN_SELCHANGE:
				map_loaded = false;
				if ( LoadMap() ) PreviewMap();
				break;
			}
			break;

		case IDC_RELOAD:
			LoadMapList();
			SendDlgItemMessage( hDlg, IDC_HEIGHTMAP, LB_SETCURSEL, 0, 0 );
			SendMessage( hDlg, WM_COMMAND, MAKEWPARAM( IDC_HEIGHTMAP, LBN_SELCHANGE ),
				( LPARAM )GetDlgItem( m_hDlg, IDC_HEIGHTMAP ) );
			break;

		case IDOK:
			if ( SaveSettings( m_Param ) )
			{
				m_OldParam = m_Param;
				EndDialog( hDlg, ( WPARAM )TRUE );
				result = TRUE;
			}
			break;

		case IDCANCEL:
			EndDialog( hDlg, ( WPARAM )FALSE );
			break;

		case IDDEFAULT:
			{
			const int choice = MessageBox( hDlg,
				TEXT( "������������ ��������� �� ���������?" ),
				TEXT( "�������������" ), MB_ICONQUESTION | MB_YESNO );

			if ( choice == IDYES )
			{
				DefaultSettings( m_Param );
				m_OldParam = m_Param;
			}
			}
			result = TRUE;
			break;

		case IDREADME:
			if ( ( int )ShellExecute( hDlg, TEXT( "open" ), TEXT( "ReadMe.txt" ),
				TEXT( "notepad.exe" ), NULL, SW_SHOW ) <= 32 )
			{
				MessageBox( hDlg, TEXT( "���� � ����������� �� ������." ),
					TEXT( "������" ), MB_ICONERROR );
			}
			result = TRUE;
			break;
		}
		break;
		}
	}

	return ( result );
}