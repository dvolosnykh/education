#include "stdafx.h"


float linear( const float a, const float b, const float t )
{
	return ( t == 0.0f || a == b ? a : t == 1.0f ? b : a + ( b - a ) * t );
}

float bilinear( const float v00, const float v10, const float v01,
	const float v11, const float kx, const float ky )
{
	return linear( linear( v00, v10, kx ), linear( v01, v11, kx ), ky );
}

void distort( BYTE & old, const unsigned distortion )
{
	int height = old + ( rand() % ( distortion << 1 ) - distortion );

	if ( height < 0 )
		height = 0;
	else if ( height > UCHAR_MAX )
		height = UCHAR_MAX;

	old = BYTE( height );
}

float degree_to_rad( const float degrees )
{
	static const float k = ( float )( M_PI / 180.0f );
	return ( degrees * k );
}

float rad_to_degree( const float rads )
{
	static const float k = ( float )( 180.0f / M_PI );
	return ( rads * k );
}

float ScaleToElapsedTime( const float value, const DWORD dwTimeElapsed )
{
	return ( value * dwTimeElapsed / 1000.0f );
}

long round( const float x )
{
	return ( long )( x + 0.5f );
}


bool IsSet( const BYTE & byte, const unsigned bit )
{
	return ( ( byte & ( 1 << bit ) ) == 1 ? true : false );
}

bool IsSet( const CBytesArray & field, const unsigned index )
{
	const unsigned byte = index >> 3;
	const unsigned bit = index & 7;

	return IsSet( field[ byte ], bit );
}

bool IsClear( const BYTE & byte, const unsigned bit )
{
	return !IsSet( byte, bit );
}

bool IsClear( const CBytesArray & field, const unsigned index )
{
	return !IsSet( field, index );
}

void Set( BYTE & byte, const unsigned bit )
{
	byte |= 1 << bit;
}

void Set( CBytesArray & field, const unsigned index )
{
	const unsigned byte = index >> 3;
	const unsigned bit = index & 7;

	Set( field[ byte ], bit );
}

void Clear( BYTE & byte, const unsigned bit )
{
	byte &= ~( 1 << bit );
}

void Clear( CBytesArray & field, const unsigned index )
{
	const unsigned byte = index >> 3;
	const unsigned bit = index & 7;

	Clear( field[ byte ], bit );
}

void Invert( BYTE & byte, const unsigned bit )
{
	byte ^= 1 << bit;
}

void Invert( CBytesArray & field, const unsigned index )
{
	const unsigned byte = index >> 3;
	const unsigned bit = index & 7;

	Invert( field[ byte ], bit );
}