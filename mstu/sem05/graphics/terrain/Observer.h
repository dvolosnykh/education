#ifndef OBSERVER_H
#define OBSERVER_H


#include "Camera.h"
#include "Input.h"


enum { TYPE_NEXT = -1, TYPE_AIRBORNE, TYPE_WALKER, TYPES_NUM };


class CObserver : public CCamera
{
private:
	bool m_forward;
	bool m_backward;
	bool m_left;
	bool m_right;

	int m_type;
	CVector3D m_move_dir;
	float m_StartSpeed;
	float m_SlowDown;
	float m_Accel;
	float m_CurSpeed;

public:
	float m_size;
	float m_height;
	bool updated;
	CVertex3D m_pos;

public:
	CObserver() {};
	virtual ~CObserver() {};

	float GetStartSpeed() const { return ( m_StartSpeed ); };
	float GetSlowDown() const { return ( m_SlowDown ); };
	unsigned GetType() const { return ( m_type ); };

	void ToggleType( const int index = TYPE_NEXT );
	void SetType( const int new_type );

	void Reset();
	void LookAt( const CVertex3D target );

	void SetPos( const CVertex3D new_pos ) { m_pos = new_pos; };
	void SetPos( const float new_x, const float new_y, const float new_z ) { m_pos.Set( new_x, new_y, new_z ); };
	void SetStartSpeed( const float value ) { m_StartSpeed = value; m_SlowDown = m_StartSpeed * 1.5f, m_Accel = m_StartSpeed / 20.0f; };
	void SetSlowDown( const float value ) { m_SlowDown = value; };

	void Apply( const move_t * pmove );
	void UpdateView();
	void UpdatePos( const DWORD elapsed );

	void Accelerate() { m_StartSpeed += m_Accel; m_SlowDown = m_StartSpeed * 1.5f; };
	void Decelerate() { m_StartSpeed -= m_Accel; m_SlowDown = m_StartSpeed * 1.5f; };

private:
	void SetMotion( bool f, bool b, bool l, bool r );
	bool CanMove( CVertex3D test_pos, CVertex3D & dest_pos, const int index = -1 );
	void Move( float speed );
	void Stop();
};

#endif