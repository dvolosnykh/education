#ifndef APP_H
#define APP_H


#include "Timer.h"
#include "Input.h"


class CApp
{
public:
	static HINSTANCE	m_hInstance;
	static HWND			m_hWnd;
	static POINT		m_offs;

	static CTimer		m_Timer;
	static CInput		m_Input;

private:
	static bool			m_quit;

public:
	static ATOM RegisterClass();

	static int APIENTRY Main( HINSTANCE hInstance, HINSTANCE hPrev, LPTSTR lpCmdLine, int nCmdShow );
	static int Run();

private:
	static LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );

	static bool InitInstance();
	static bool InitWindow();
	static void DeinitWindow();
};

#endif