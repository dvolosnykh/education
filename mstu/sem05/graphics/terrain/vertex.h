#ifndef VERTEX_H
#define VERTEX_H


#include "vertvect.h"


#define PT_ABOVE	1
#define PT_BELONGS	0
#define PT_BELOW	-1

//==================== 1D-vertex ====================

class CVertex
{
public:
	float x;

	CVertex() {};
	CVertex( const float init_x ) { Set( init_x ); };
	CVertex( const CVertex & init ) { Set( init ); };
	CVertex( const CVertex2D & init );
	CVertex( const CVector & init );
	CVertex( const float * const array ) { LoadArray( array ); };
	/*virtual*/ ~CVertex() {};

	void LoadArray( const float * const array ) { Set( array[ 0 ] ); };

	void Set( const float new_x ) { x = new_x; };
	void Set( const CVertex & v ) { Set( v.x ); };

	CVertex & ScaleX( const float k ) { x *= k; return ( *this ); };
	/*virtual*/ CVertex & Scale( const float k ) { return ScaleX( k ); };
	CVertex GetScaled( const float k ) const { CVertex v = *this; return v.Scale( k ); };

	/*virtual*/ float Distance( const CVertex v ) const { return fabsf( x - v.x ); };

	bool operator == ( const CVertex v ) const { return ( x == v.x ); };
	bool operator != ( const CVertex v ) const { return !( *this == v ); };
	CVertex & operator += ( const CVertex v ) { x += v.x; return ( *this ); };
	CVertex & operator -= ( const CVertex v ) { x -= v.x; return ( *this ); };
	CVertex operator + ( const CVertex v ) const { return CVertex( x + v.x ); };
	CVector operator - ( const CVertex v ) const;
	float operator * ( const CVector v ) const;
	CVertex operator * ( const float k ) const { return GetScaled( k ); };
	CVertex & operator *= ( const float k ) { return Scale( k ); };

	float & operator [] ( const unsigned index ) { return *( &x + index ); };	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	const float & operator [] ( const unsigned index ) const { return *( &x + index ); };

	float * GetPtr() { return ( float * )&x; };	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	const float * const GetPtr() const { return ( float * )&x; };
};

//==================== 2D-vertex ====================

class CVertex2D : public CVertex
{
public:
	float y;

	CVertex2D() {};
	CVertex2D( const float init_x, const float init_y ) { Set( init_x, init_y ); };
	CVertex2D( const CVertex2D & init ) { Set( init ); };
	CVertex2D( const CVertex3D & init );
	CVertex2D( const CVector2D & init );
	CVertex2D( const float * const array ) { LoadArray( array ); };
	/*virtual*/ ~CVertex2D() {};

	void LoadArray( const float * const array ) { Set( array[ 0 ], array[ 1 ] ); };

	void Set( const float new_x, const float new_y ) { x = new_x, y = new_y; };
	void Set( const CVertex2D & v ) { Set( v.x, v.y ); };

	CVertex2D & ScaleY( const float k ) { y *= k; return ( *this ); };
	/*virtual*/ CVertex2D & Scale( const float k ) { CVertex::Scale( k ); return ScaleY( k ); };
	CVertex2D GetScaled( const float k ) const { CVertex2D v = *this; return v.Scale( k ); };

	/*virtual*/ float Distance( const CVertex2D v ) const;
	int VsLine( const CVertex2D v1, const CVertex2D v2 ) const;

	CVertex2D & ProjX() { y = 0; return ( *this ); };
	CVertex2D & ProjY() { x = 0; return ( *this ); };
	CVertex2D GetProjX() const { CVertex2D v = *this; return v.ProjX(); };
	CVertex2D GetProjY() const { CVertex2D v = *this; return v.ProjY(); };

	bool operator == ( const CVertex2D v ) const { return ( x == v.x && y == v.y ); };
	bool operator != ( const CVertex2D v ) const { return !( *this == v ); };
	CVertex2D & operator += ( const CVertex2D v ) { x += v.x, y += v.y; return ( *this ); };
	CVertex2D & operator -= ( const CVertex2D v ) { x -= v.x, y -= v.y; return ( *this ); };
	CVertex2D operator + ( const CVertex2D v ) const { return CVertex2D( x + v.x, y + v.y ); };
	CVector2D operator - ( const CVertex2D v ) const;
	float operator * ( const CVector2D v ) const;
	CVertex2D operator * ( const float k ) const { return GetScaled( k ); };
	CVertex2D & operator *= ( const float k ) { return Scale( k ); };
};

//==================== 3D-vertex ====================

class CVertex3D : public CVertex2D
{
public:
	float z;

	CVertex3D() {};
	CVertex3D( const float init_x, const float init_y, const float init_z ) { Set( init_x, init_y, init_z ); };
	CVertex3D( const CVertex3D & init ) { Set( init ); };
	CVertex3D( const CVector3D & init );
	CVertex3D( const float * const array ) { LoadArray( array ); };
	/*virtual*/ ~CVertex3D() {};

	void LoadArray( const float * const array ) { Set( array[ 0 ], array[ 1 ], array[ 2 ] ); };

	void Set( const float new_x, const float new_y, const float new_z ) { x = new_x, y = new_y, z = new_z; };
	void Set( const CVertex3D & v ) { Set( v.x, v.y, v.z ); };

	CVertex3D & ScaleZ( const float k ) { z *= k; return ( *this ); };
	/*virtual*/ CVertex3D & Scale( const float k ) { CVertex2D::Scale( k ); return ScaleZ( k ); };
	CVertex3D GetScaled( const float k ) const { CVertex3D v = *this; return v.Scale( k ); };

	/*virtual*/ float Distance( CVertex3D v ) const;

	CVertex3D & ProjXY() { z = 0; return ( *this ); };
	CVertex3D & ProjYZ() { x = 0; return ( *this ); };
	CVertex3D & ProjXZ() { y = 0; return ( *this ); };
	CVertex3D GetProjXY() const { CVertex3D v = *this; return v.ProjXY(); };
	CVertex3D GetProjYZ() const { CVertex3D v = *this; return v.ProjYZ(); };
	CVertex3D GetProjXZ() const { CVertex3D v = *this; return v.ProjXZ(); };

	bool operator == ( const CVertex3D v ) const { return ( x == v.x && y == v.y && z == v.z ); };
	bool operator != ( const CVertex3D v ) const { return !( *this == v ); };
	CVertex3D & operator += ( const CVertex3D v ) { x += v.x, y += v.y, z += v.z; return ( *this ); };
	CVertex3D & operator -= ( const CVertex3D v ) { x -= v.x, y -= v.y, z -= v.z; return ( *this ); };
	CVertex3D operator + ( const CVertex3D v ) const { return CVertex3D( x + v.x, y + v.y, z + v.z ); };
	CVector3D operator - ( const CVertex3D v ) const;
	float operator * ( const CVector3D v ) const;
	CVertex3D operator * ( const float k ) const { return GetScaled( k ); };
	CVertex3D & operator *= ( const float k ) { return Scale( k ); };
};

//==================== 4D-vertex ====================

class CVertex4D : public CVertex3D
{
public:
	float w;

	CVertex4D() {};
	CVertex4D( const float init_x, const float init_y, const float init_z, const float init_w ) { Set( init_x, init_y, init_z, init_w ); };
	CVertex4D( const CVertex4D & init ) { Set( init ); };
	CVertex4D( const CVertex3D & init );
	CVertex4D( const float * const array ) { LoadArray( array ); };
	/*virtual*/ ~CVertex4D() {};

	void LoadArray( const float * const array ) { Set( array[ 0 ], array[ 1 ], array[ 2 ], array[ 3 ] ); };

	void Set( const float new_x, const float new_y, const float new_z, const float new_w ) { x = new_x, y = new_y, z = new_z, w = new_w; };
	void Set( const CVertex4D & v ) { Set( v.x, v.y, v.z, v.w ); };

	CVertex3D Unify();

	CVertex4D & ScaleW( const float k ) { w *= k; return ( *this ); };
	/*virtual*/ CVertex4D & Scale( const float k ) { CVertex3D::Scale( k ); return ScaleW( k ); };
	CVertex4D GetScaled( const float k ) const { CVertex4D v = *this; return v.Scale( k ); };

	bool operator == ( const CVertex4D v ) const { return ( x == v.x && y == v.y && z == v.z && w == v.w ); };
	bool operator != ( const CVertex4D v ) const { return !( *this == v ); };
	CVertex4D operator * ( const float k ) const { return GetScaled( k ); };
	CVertex4D & operator *= ( const float k ) { return Scale( k ); };
};

#endif