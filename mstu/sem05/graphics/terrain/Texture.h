#ifndef TEXTURE_H
#define TEXTURE_H


#include "Image.h"
#include "Observer.h"


enum { ORIGIN_TOPLEFT, ORIGIN_CENTER };


class CTexture : public CTDArray< RGBQUAD >
{
public:
	CTexture() {};
	virtual ~CTexture() {};

	bool Draw( const CObserver * const pObserver, LONG dest_x, LONG dest_y, const unsigned origin_type, const bool transparent = false ) const;
	void Resize( const LONG new_height, const LONG new_width );
	bool PutOnto( const CTexture & base, const bool fit_base_dims = true );
	bool LoadTexture( LPCTSTR pszFileName, const bool transparent = false );

private:
	bool LoadTGATexture( LPCTSTR pszFileName, const bool transparent = false );
	bool LoadBMPTexture( LPCTSTR szFileName, const bool transparent = false );
	bool LoadImage( CImage * const pImage, LPCTSTR pszFileName, const bool transparent = false );
};

#endif