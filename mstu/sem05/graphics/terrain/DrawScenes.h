#ifndef DRAW_SCENES_H
#define DRAW_SCENES_H


// Statistics info
struct stats_t
{
	unsigned vertices;
	unsigned terrain;
	unsigned water;
	unsigned obs_id;
	CVertex3D obs_pos;
};

#endif