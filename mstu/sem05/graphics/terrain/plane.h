#ifndef PLANE_H
#define PLANE_H


class CPlane
{
public:
	CVector3D m_Normal;	// Kept as unit-vector.
	float m_D;

public:
	CPlane() {};
	CPlane( const CVector3D & normal, const float d ) { Set( normal, d ); };
	CPlane( const CVertex3D & p0, const CVertex3D & p1, const CVertex3D & p2 ) { Set( p0, p1, p2 ); };
	~CPlane() {};

	void Set( const CVector3D & normal, const float d );
	void Set( const CVertex3D & p0, const CVertex3D & p1, const CVertex3D & p2 );
	float Distance( const CVertex3D * const point ) const { return fabsf( SignDistance( point ) ); };
	float SignDistance( const CVertex3D * const point ) const { return ( ( m_Normal * ( *point ) ) + m_D ); };

	bool IsComplanar( const CPlane * const plane ) const { return m_Normal.IsCollinear( plane->m_Normal ); };

	bool AllOnNormalSide( const CVertex3D * const v, const unsigned n ) const;

	float & operator [] ( const unsigned i ) { return ( i == 0 ? m_Normal.x : i == 1 ? m_Normal.y : i == 2 ? m_Normal.z : m_D ); };

private:
	void Normalize();
	void CalcD( const CVertex3D * const point ) { m_D = -( m_Normal * ( *point ) ); };
	void CalcNormal( const CVertex3D * const p0, const CVertex3D * const p1, const CVertex3D * const p2 );
};

#endif