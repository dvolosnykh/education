#ifndef CAMERA_H
#define CAMERA_H


#include "Frustum.h"
#include "Quat.h"
#include "Matrix.h"


class CCamera : public CFrustum
{
public:
	CMatrix view;
	CMatrix proj;

protected:
	CVector3D m_view_dir;
	CQuat m_Quat;

public:
	CCamera() {};
	virtual ~CCamera() {};

	void Reset() { m_Quat.Reset(); };

	void RotateVert( const float degrees );
	void RotateHorz( const float degrees );

	void Perspective( const float FOV, const float aspect, const float Near, const float Far );
};

#endif
