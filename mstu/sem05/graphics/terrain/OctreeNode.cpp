#include "stdafx.h"

#include "OctreeNode.h"
#include "SetupDialog.h"


unsigned COctreeNode::FormList( const CTriangleList & Triangles )
{
	CTDArray< bool > collides( Triangles.GetSize(), true );

	// Identify the intersecting triangles and count them
	unsigned count = 0;
	for ( LONG i = 0; i < Triangles.GetSize(); i++ )
		if ( collides[ i ] = m_Bound.CheckCollision( Triangles[ i ] ) )
			count++;

	if ( count > 0 )
	{
		m_Triangles.Alloc( count, false );

		for ( LONG i = 0, cur = 0; i < Triangles.GetSize(); i++ )
			if ( collides[ i ] )
				m_Triangles[ cur++ ] = Triangles[ i ];
	}

	return ( count );
}

void COctreeNode::Build( const CTriangleList & Triangles, const COctreeNode * const pParent )
{
	static unsigned Total;
	static float progress;

	if ( pParent == NULL )	// root-node.
	{
		CProgressWindow::SetTask( TEXT( "�������� ��������� ����-������ (%i%%)..." ) );
		progress = 0.0f;
		m_Bound.Calculate( Triangles );
		Total = unsigned( Triangles.GetSize() * 2.5f );
	}

	// Attach triangles to this node by default.
	const unsigned included = FormList( Triangles );

	if ( included > g_param.MaxTriInOctNode &&
		m_Bound.GetSideLength() > g_param.MinOctNodeSize )
	{
		m_ChildNodes.Alloc( NODES_NUM, false );
		Subdivide();

		for ( unsigned i = 0; i < NODES_NUM; i++ )
			m_ChildNodes[ i ].Build( m_Triangles, this );

		// Triangles are passed to children -> release list.
		m_Triangles.Free();
	}
	else
	{
		progress += 100 * included / ( float )Total;
		CProgressWindow::SetProgress( ( unsigned )progress );
	}
}

void COctreeNode::Destroy()
{
	if ( !IsEmpty() )
		if ( !IsLeafNode() )
		{
			for ( register unsigned i = 0; i < NODES_NUM; i++ )
				m_ChildNodes[ i ].Destroy();

			m_ChildNodes.Free();
		}
		else
			m_Triangles.Free();
}

bool COctreeNode::CheckIntersection( const CRay * const ray ) const
{
	return
	(
		!IsEmpty() && m_Bound.CheckIntersection( ray ) &&
		( !IsLeafNode() ? CheckSubnodes( ray ) : CheckTriangles( ray ) )
	);
}

bool COctreeNode::CheckTriangles( const CRay * const ray ) const
{
	LONG i;
	for ( i = 0; i < m_Triangles.GetSize() &&
		!m_Triangles[ i ]->CheckIntersection( ray );
		i++ );

	return ( i < m_Triangles.GetSize() );
}

bool COctreeNode::CheckSubnodes( const CRay * const ray ) const
{
	LONG i;
	for ( i = 0; i < NODES_NUM &&
		!m_ChildNodes[ i ].CheckIntersection( ray );
		i++ );

	return ( i < NODES_NUM );
}

void COctreeNode::Render( const CObserver * const pObserver, const bool HSR /* = true */ ) const
{
	if ( !IsEmpty() )
	{
		const unsigned cull = !HSR ? FULL_INSIDE :
			pObserver->BoundLocation( &m_Bound );

		if ( cull != FULL_OUTSIDE )
			( !IsLeafNode() ? RenderSubnodes( pObserver, HSR && cull == PARTIAL_INSIDE ) : RenderTriangles() );
	}
}

void COctreeNode::RenderSubnodes( const CObserver * const pObserver, const bool HSR /* = true */ ) const
{
	for ( unsigned i = 0; i < NODES_NUM; i++ )
		m_ChildNodes[ i ].Render( pObserver, HSR );
}

void COctreeNode::RenderTriangles() const
{
	for ( LONG i = 0; i < m_Triangles.GetSize(); i++ )
		if ( !m_Triangles[ i ]->IsRendered() )
			m_Triangles[ i ]->Render();
}

void COctreeNode::Subdivide()
{
	const CVertex3D & min = m_Bound.m_Min;
	const CVertex3D & max = m_Bound.m_Max;
	const CVertex3D & center = m_Bound.m_Center;

	m_ChildNodes[ 0 ].m_Bound.Set( min, center );
	m_ChildNodes[ 1 ].m_Bound.Set
	(
		CVertex3D( min.x, min.y, center.z ),
		CVertex3D( center.x, center.y, max.z )
	);
	m_ChildNodes[ 2 ].m_Bound.Set
	(
		CVertex3D( center.x, min.y, center.z ),
		CVertex3D( max.x, center.y, max.z )
	);
	m_ChildNodes[ 3 ].m_Bound.Set
	(
		CVertex3D( center.x, min.y, min.z ),
		CVertex3D( max.x, center.y, center.z )
	);
	m_ChildNodes[ 4 ].m_Bound.Set
	(
		CVertex3D( min.x, center.y, min.z ),
		CVertex3D( center.x, max.y, center.z )
	);
	m_ChildNodes[ 5 ].m_Bound.Set
	(
		CVertex3D( min.x, center.y, center.z ),
		CVertex3D( center.x, max.y, max.z )
	);
	m_ChildNodes[ 6 ].m_Bound.Set( center, max );
	m_ChildNodes[ 7 ].m_Bound.Set
	(
		CVertex3D( center.x, center.y, min.z ),
		CVertex3D( max.x, max.y, center.z )
	);
}

void COctreeNode::DrawNodes( bool SkipEmpty /* = true */ ) const
{
	if ( !IsEmpty() || !SkipEmpty )
		if ( !IsLeafNode() )
		{
			for ( register unsigned i = 0; i < NODES_NUM; i++ )
				m_ChildNodes[ i ].DrawNodes( SkipEmpty );
		}
		else
		{
			// Color. A node that contains the maximum amount of faces is red
			m_Bound.Draw( m_Triangles.GetSize() / ( float )g_param.MaxTriInOctNode );
		}
}