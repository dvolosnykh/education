#include "stdafx.h"

#include "Header.h"


bool CHeader::IsSupported() const
{
	return ( GetBitDepth() == 24 );
}