#ifndef DARRAY_H
#define DARRAY_H

//==================== INTERFACE =====================

template< class T = BYTE >
class CTDArray
{
protected:
	T * ptr;
	LONG size;
	LONG height;
	LONG width;

public:
	CTDArray() { Init(); };
	CTDArray( const LONG new_size, const bool make_null ) { Init(); Alloc( new_size, make_null ); };
	CTDArray( const LONG new_height, const LONG new_width, const bool make_null ) { Init(); Alloc( new_height, new_width, make_null ); };
	virtual ~CTDArray() { Free(); };

	unsigned GetByteSize() const { return ( size * sizeof( T ) ); };
	LONG GetSize() const { return ( size ); };
	LONG GetHeight() const { return ( height ); };
	LONG GetWidth() const { return ( width ); };

	T * GetPtr() { return ( ptr ); };
	const T * const GetPtr() const { return ( ptr ); };
	void SetPtr( T * const new_ptr ) { ptr = new_ptr; };

	bool IsEmpty() const { return ( ptr == NULL ); };
	void Init() { ptr = NULL, size = height = width = 0; };
	void Reset( const T value );
	void Reset( const LONG from, const LONG to, const T value );
	void Reset( const LONG dest_h, const LONG dest_w, const LONG count_h, const LONG count_w, const T value );
	void Alloc( const LONG new_height, const LONG new_width, const bool make_null );
	void Alloc( const LONG new_size, const bool make_null );
	void Copy( const CTDArray< T > & src );
	void Free();

	T & operator [] ( const LONG index ) { return ( ptr[ index ] ); };
	const T & operator [] ( const LONG index ) const { return ( ptr[ index ] ); };
};

//==================== IMPLEMENTATION ====================

template< class T >
void CTDArray< T >::Reset( const T value )
{
	for ( register LONG i = 0; i < size; i++ )
		ptr[ i ] = value;
}

template< class T >
void CTDArray< T >::Reset( const LONG from, const LONG to, const T value )
{
	for ( register LONG i = from; i <= to; i++ )
		ptr[ i ] = value;
}

template< class T >
void CTDArray< T >::Reset( const LONG dest_h, const LONG dest_w, const LONG count_h, const LONG count_w, const T value )
{
	for ( register LONG i = 0, index = dest_h * width + dest_w;
		i < count_h; i++, index += width - count_w )
	{
		for ( register LONG j = 0; j < count_w; j++, index++ )
            ptr[ index ] = value;
	}
}

template< class T >
void CTDArray< T >::Alloc( const LONG new_size, const bool make_null )
{
	if ( size != new_size )
	{
		Free();
		ptr = new T [ size = new_size ];
	}

	if ( make_null )
		memset( ptr, 0, size * sizeof( T ) );
}

template< class T >
void CTDArray< T >::Alloc( const LONG new_height, const LONG new_width, const bool make_null )
{
	Alloc( new_height * new_width, make_null );
	height = new_height, width = new_width;
}

template< class T >
void CTDArray< T >::Copy( const CTDArray< T > & src )
{
	if ( src.GetSize() != 0 )
		if ( src.GetHeight() != 0 && src.GetWidth() != 0 )
			Alloc( src.GetHeight(), src.GetWidth(), false );
		else
			Alloc( src.GetSize(), false );

	memcpy( ptr, src.ptr, size * sizeof( T ) );
}

template< class T >
void CTDArray< T >::Free()
{
	if ( !IsEmpty() )
	{
		delete [] ptr;
		Init();
	}
}

#endif