#include "stdafx.h"

#include "Matrix.h"


CVertex4D CMatrix::TransformDot( const CVertex4D p ) const
{
	return CVertex4D
	(
		ptr[ 0 ][ 0 ] * p.x + ptr[ 0 ][ 1 ] * p.y + ptr[ 0 ][ 2 ] * p.z + ptr[ 0 ][ 3 ] * p.w,
		ptr[ 1 ][ 0 ] * p.x + ptr[ 1 ][ 1 ] * p.y + ptr[ 1 ][ 2 ] * p.z + ptr[ 1 ][ 3 ] * p.w,
		ptr[ 2 ][ 0 ] * p.x + ptr[ 2 ][ 1 ] * p.y + ptr[ 2 ][ 2 ] * p.z + ptr[ 2 ][ 3 ] * p.w,
		ptr[ 3 ][ 0 ] * p.x + ptr[ 3 ][ 1 ] * p.y + ptr[ 3 ][ 2 ] * p.z + ptr[ 3 ][ 3 ] * p.w
	);
}

CMatrix CMatrix::GetMult( const CMatrix & m ) const
{
	static CMatrix result;

	for ( register unsigned i = 0; i < DIM; i++ )
		for ( register unsigned j = 0; j < DIM; j++ )
			result[ i ][ j ] = MultRowByCol( i, m, j );

	return ( result );
}

float CMatrix::MultRowByCol( const unsigned i, const CMatrix & m, const unsigned j ) const
{
	static float result;
	result = 0.0f;

	for ( register unsigned k = 0; k < DIM; k++ )
		result += ptr[ i ][ k ] * m[ k ][ j ];

	return ( result );
}


CMatrix & CMatrix::operator = ( const float unit )
{
	Reset();
	for ( register unsigned i = 0; i < DIM; i++ )
		ptr[ i ][ i ] = unit;

	return ( *this );
}

CMatrix & CMatrix::RotateZ( const float angle )
{
	const float c = cosf( angle );
	const float s = sinf( angle );

	static CMatrix temp;
	temp = 1.0f;
	temp[ 0 ][ 0 ] = c;		temp[ 0 ][ 1 ] = -s;
	temp[ 1 ][ 0 ] = s;		temp[ 1 ][ 1 ] = c;

	return ( *this *= temp );
}

CMatrix & CMatrix::RotateY( const float angle )
{
	const float c = cosf( angle );
	const float s = sinf( angle );

	static CMatrix temp;
	temp = 1.0f;
	temp[ 0 ][ 0 ] = c;		temp[ 0 ][ 2 ] = -s;
	temp[ 2 ][ 0 ] = s;		temp[ 2 ][ 2 ] = c;

	return ( *this *= temp );
}

CMatrix & CMatrix::RotateX( const float angle )
{
	const float c = cosf( angle );
	const float s = sinf( angle );

	static CMatrix temp;
	temp = 1.0f;
	temp[ 1 ][ 1 ] = c;		temp[ 1 ][ 2 ] = -s;
	temp[ 2 ][ 1 ] = s;		temp[ 2 ][ 2 ] = c;

	return ( *this *= temp );
}

CMatrix & CMatrix::Offset( const CVector3D offset )
{
	static CMatrix temp;
	temp = 1.0f;
	temp[ 0 ][ 3 ] = offset.x;
	temp[ 1 ][ 3 ] = offset.y;
	temp[ 2 ][ 3 ] = offset.z;

	return ( *this *= temp );
}

CMatrix CMatrix::GetOffset( const CVector3D offset )
{
	CMatrix temp = *this;
	return temp.Offset( offset );
}

void CMatrix::Print() const
{
#ifdef _DEBUG
	for ( unsigned i = 0; i < DIM; i++ )
	{
		for ( unsigned j = 0; j < DIM; j++ )
			TRACE( TEXT( "%4.3f\t" ), ptr[ i ][ j ] );

		TRACE( TEXT( "\n" ) );
	};
	TRACE( TEXT( "\n" ) );
#endif
}