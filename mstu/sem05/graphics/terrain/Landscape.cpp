#include "stdafx.h"

#include "Landscape.h"
#include "ray.h"
#include "HeavenlyBody.h"


bool CLandscape::LoadTerrain( engine_param_t & engine )
{
	bool isOK = true;

	const LONG & height = CGL::Heightmap.GetHeight();
	const LONG & width = CGL::Heightmap.GetWidth();

	if ( engine.new_hmap )
	{
		CGL::Frame.Alloc( height, width, true );
		CGL::Vertex.Alloc( height, width, false );
		CGL::Screen.Alloc( height, width, false );
		CalcVertices();
	}

	if ( engine.new_hmap || engine.light_changed )
	{
		if ( g_param.SimpleLighting )
			CGL::Normal.Alloc( height, width, true );

		Triangulate();

		CGL::VisTriList.Alloc( m_Triangles.GetSize(), false );
	}

	CGL::InitHeavenlyBody();

	if ( engine.new_hmap || engine.build_octree )
	{
		m_Octree.Destroy();
		BuildOctree();
	}

	static unsigned tex_num;
	if ( engine.new_hmap || engine.tex_changed )
	{
		tex_num = GenerateTerrainTexture();
		isOK = tex_num > 0;

		CGL::TexCoord.Alloc( height, width, false );
		CalcTexCoords();
	}

	if ( isOK && ( engine.new_hmap || engine.tex_changed || engine.light_changed ) &&
		( g_param.Shadows || g_param.SimpleLighting ) )
	{
		CGL::Light.Alloc( height, width, false );
		CGL::ProcessLighting();
		CGL::ApplyLightingToTexture();
	}

	//if ( isOK ) isOK = m_Detail.LoadTexture( g_param.szDetailTexture );

	if ( isOK )
	{
		engine.tex_changed = false;
		engine.new_hmap = false;
		engine.build_octree = false;
		engine.light_changed = false;
	}

	return ( isOK );
}

static RGBQUAD rgb_linear( const RGBQUAD & low, const RGBQUAD & high, const float alpha )
{
	RGBQUAD color;
	color.rgbBlue	= ( BYTE )linear( low.rgbBlue,  high.rgbBlue,  alpha );
	color.rgbGreen	= ( BYTE )linear( low.rgbGreen, high.rgbGreen, alpha );
	color.rgbRed	= ( BYTE )linear( low.rgbRed,   high.rgbRed,   alpha );
	color.rgbReserved = UCHAR_MAX;

	return ( color );
}

unsigned CLandscape::GenerateTerrainTexture()
{
	const CHeightmap & hmap = CGL::Heightmap;

	// Load all source textures
	CProgressWindow::SetTask( TEXT( "�������� ��������� ������� ��� ��������� (%i%%)..." ) );

	static TCHAR szFileBuf[ _MAX_PATH ];
	_tcscpy( szFileBuf, g_param.szTextureSearchPath );
	size_t len = _tcslen( szFileBuf );
	_tcscat( szFileBuf + len, TEXT( "Texture" ) );
	len += 7;

	CTexture Level[ MAX_TEXTURES ];
	unsigned levels_num;
	bool quit = false;
	for ( levels_num = 0; !quit && ++levels_num < MAX_TEXTURES; )
	{
		_stprintf( szFileBuf + len, TEXT( "%i.bmp" ), levels_num );

		if ( !Level[ levels_num - 1 ].LoadTexture( szFileBuf ) )
			quit = true;

		if ( quit )
		{
			_stprintf( szFileBuf + len, TEXT( "%i.tga" ), levels_num );

			quit = !Level[ levels_num - 1 ].LoadTexture( szFileBuf );
		}
	}

	// We need at least two textures for our blending to work
	if ( --levels_num >= 2 )
	{
		srand( ( unsigned )time( NULL ) );

		const unsigned TexSizeX = g_param.TexSize;
		const unsigned TexSizeY = g_param.TexSize;

		CProgressWindow::SetTask( TEXT( "��������� �������� ��������� (%i%%)..." ) );
		float progress = 0.0f;
		const float dprogress = 100.0f / TexSizeY;

		m_Texture.Alloc( TexSizeY, TexSizeX, false );

		const float step_y = ( hmap.GetHeight() - 1 ) / ( float )( TexSizeY - 1 );
		const float step_x = ( hmap.GetWidth() - 1 ) / ( float )( TexSizeX - 1 );

		const float height_scale = levels_num / ( float )UCHAR_MAX;
		const unsigned tex_noise = 10;

		register unsigned i, j, index;
		float x, y;
		for ( i = 0, y = 0.0f, index = 0; i < TexSizeY; i++, y += step_y )
		{
			// precalculations for height-interpolating...
			LONG y0 = ( LONG )floorf( y );
			LONG y1 = ( LONG )ceilf( y );

			if ( y0 >= hmap.GetHeight() )
				y1 = y0 = hmap.GetHeight() - 1;
			else if ( y1 >= hmap.GetHeight() )
				y1 = hmap.GetHeight() - 1;

			const float ky = y - y0;

			for ( j = 0, x = 0.0f; j < TexSizeX; j++, x += step_x, index++ )
			{
				// precalculations for height-interpolating...
				LONG x0 = ( LONG )floorf( x );
				LONG x1 = ( LONG )ceilf( x );

				if ( x0 >= hmap.GetWidth() )
					x1 = x0 = hmap.GetWidth() - 1;
				else if ( x1 >= hmap.GetWidth() )
					x1 = hmap.GetWidth() - 1;

				const float kx = x - x0;

				// height-interpolating and applying noise.
				const float value1 = linear( hmap[ y0 * hmap.GetWidth() + x0 ], hmap[ y1 * hmap.GetWidth() + x0 ], ky );
				const float value2 = linear( hmap[ y0 * hmap.GetWidth() + x1 ], hmap[ y1 * hmap.GetWidth() + x1 ], ky );
				BYTE height = ( BYTE )linear( value1, value2, kx );
				distort( height, tex_noise );

				// processing multi-level textures...
				const float actual = height * height_scale;
				unsigned high_level = ( unsigned )ceilf( actual );
				unsigned low_level = ( unsigned )floorf( actual );

				if ( low_level >= levels_num )
					high_level = low_level = levels_num - 1;
				else if ( high_level >= levels_num )
					high_level = levels_num - 1;

				const CTexture & low_tex = Level[ low_level ];
				const CTexture & high_tex = Level[ high_level ];

				const unsigned low_index = ( ( i % low_tex.GetHeight() ) * low_tex.GetWidth() + j % low_tex.GetWidth() );
				const unsigned high_index = ( ( i % high_tex.GetHeight() ) * high_tex.GetWidth() + j % high_tex.GetWidth() );

				m_Texture[ index ] = rgb_linear( low_tex[ low_index ], high_tex[ high_index ], actual - low_level );
			}

			CProgressWindow::SetProgress( ( unsigned )( progress += dprogress ) );
		}
	}

	return ( levels_num );
}

void CLandscape::CalcVertices()
{
	const CHeightmap & hmap = CGL::Heightmap;

	CProgressWindow::SetTask( TEXT( "���������� ������ ��������� (%i%%)..." ) );

	const float & step = g_param.GridStep;

	m_dim.x = CGL::Vertex.GetWidth() * step;
	m_dim.z = CGL::Vertex.GetHeight() * step;
	m_dim.y = ( m_dim.x + m_dim.z ) * 0.1f;
	const float scale = m_dim.y / UCHAR_MAX;

	register LONG i, j;
	register unsigned index;
	float x, z;
	for ( i = 0, z = 0.0f, index = 0; i < hmap.GetHeight(); i++, z += step )
	{
		for ( j = 0, x = 0.0f; j < hmap.GetWidth(); j++, x += step, index++ )
			CGL::Vertex[ index ].Set( x, hmap[ index ] * scale, z );

		CProgressWindow::SetProgress( 100 * ( i + 1 ) / ( hmap.GetHeight() - 1 ) );
	}
}

void CLandscape::Triangulate()
{
	CProgressWindow::SetTask( TEXT( "������������ ��������� (%i%%)..." ) );

	const LONG & height = CGL::Heightmap.GetHeight();
	const LONG & width = CGL::Heightmap.GetWidth();

	m_Triangles.Alloc( ( height - 1 ) * ( width - 1 ) * 2, false );

	for ( LONG i = 0, cur_i = 0, index = 0; i < height - 1; i++ )
	{
		const LONG next_i = cur_i + width;

		for ( LONG j = 0; j < width - 1; )
		{
			const LONG j_1 = j + 1;

			m_Triangles[ index ].Set( cur_i + j, cur_i + j_1, next_i + j_1 );
			if ( g_param.SimpleLighting )
			{
				CGL::Normal[ cur_i + j ] += m_Triangles[ index ].m_Normal;
				CGL::Normal[ cur_i + j_1 ] += m_Triangles[ index ].m_Normal;
				CGL::Normal[ next_i + j_1 ] += m_Triangles[ index ].m_Normal;
			}
			index++;

			m_Triangles[ index ].Set( cur_i + j, next_i + j_1, next_i + j );
			if ( g_param.SimpleLighting )
			{
				CGL::Normal[ cur_i + j ] += m_Triangles[ index ].m_Normal;
				CGL::Normal[ next_i + j_1 ] += m_Triangles[ index ].m_Normal;
				CGL::Normal[ next_i + j ] += m_Triangles[ index ].m_Normal;
			}
			index++;

			j = j_1;
		}

		cur_i = next_i;

		CProgressWindow::SetProgress( 100 * ( i + 1 ) / ( height - 1 ) );
	}

	// make all normals to be a unit-vectors.
	for ( LONG i = 0; i < CGL::Normal.GetSize(); i ++ )
		CGL::Normal[ i ].Normalize();
}

void CLandscape::BuildOctree()
{
	CTriangleList TriangleList( m_Triangles.GetSize(), false );

	for ( LONG i = 0; i < m_Triangles.GetSize(); i++ )
		TriangleList[ i ] = &m_Triangles[ i ];

	m_Octree.Build( TriangleList );
}

void CLandscape::CalcTexCoords()
{
	CProgressWindow::SetTask( TEXT( "���������� ���������� ��������� (%i%%)..." ) );

	const CTexture & tex = m_Texture;
	const float step_h = ( tex.GetHeight() - 1 ) / ( float )( CGL::TexCoord.GetHeight() - 1 );
	const float step_w = ( tex.GetWidth() - 1 ) / ( float )( CGL::TexCoord.GetWidth() - 1 );

	for ( register LONG i = 0, index = 0; i < CGL::TexCoord.GetHeight(); i++ )
		for ( register LONG j = 0; j < CGL::TexCoord.GetWidth(); j++, index++ )
			CGL::TexCoord[ index ].Set( j * step_w, i * step_h );
}

bool CLandscape::InScope( const float at_x, const float at_z ) const
{
	return ( 0.0f <= at_x && at_x <= m_dim.x ) && ( 0.0f <= at_z && at_z <= m_dim.z );
}

float CLandscape::GetSmoothHeight( const float at_x, const float at_z ) const
{
	float height = FLT_MIN;

	const float w = 1.0f / g_param.GridStep;

	const float fj = at_x * w;
	LONG j = ( LONG )floorf( fj );
	LONG j_1 = ( LONG )ceilf( fj );
	const float fj_part = fj - j;

	if ( j < 0 ) j = 0;
	else if ( j >= CGL::Vertex.GetWidth() ) j = CGL::Vertex.GetWidth() - 1;

	if ( j_1 < 0 ) j_1 = 0;
	else if ( j_1 >= CGL::Vertex.GetWidth() ) j_1 = CGL::Vertex.GetWidth() - 1;

	const float fi = at_z * w;
	LONG i = ( LONG )floorf( fi );
	LONG i_1 = ( LONG )ceilf( fi );
	const float fi_part = fi - i;

	if ( i < 0 ) i = 0;
	else if ( i >= CGL::Vertex.GetWidth() ) i = CGL::Vertex.GetWidth() - 1;

	if ( i_1 < 0 ) i_1 = 0;
	else if ( i_1 >= CGL::Vertex.GetWidth() ) i_1 = CGL::Vertex.GetWidth() - 1;

	if ( i < i_1 && j < j_1 )
	{
		// interpolate in a rectangle.
		unsigned index = 2 * ( i * ( CGL::Vertex.GetWidth() - 1 ) + j );
		if ( fj_part < fi_part ) index++;

		pWanted = &m_Triangles[ index ];
		height = m_Triangles[ index ].GetSmoothHeight( at_x, at_z );
	}
	else if ( i < i_1 && j == j_1 )
	{
		// interpolate by vertical edge.
		const unsigned iw = i * CGL::Vertex.GetWidth();
		const float value1 = CGL::Vertex[ iw + j ].y;
		const float value2 = CGL::Vertex[ ( iw + CGL::Vertex.GetWidth() ) + j ].y;
		height = linear( value1, value2, fi_part );
	}
	else if ( i == i_1 && j < j_1 )
	{
		// interpolate by horizontal edge.
		const unsigned iw = i * CGL::Vertex.GetWidth();
		const float value1 = CGL::Vertex[ iw + j ].y;
		const float value2 = CGL::Vertex[ iw + j_1 ].y;
		height = linear( value1, value2, fj_part );
	}
	else if ( i == i_1 && j == j_1 )
	{
		// single point.
		height = CGL::Vertex[ i * CGL::Vertex.GetWidth() + j ].y;
	}

	return ( height );
}

unsigned CLandscape::Render( const CObserver * const pObserver )
{
	CGL::VisTriList.ResetCounter();
	m_Octree.Render( pObserver, g_param.UseHSR );
	return ( CGL::VisTriList.GetTriNum() );
}

void CLandscape::Destroy()
{
	m_Triangles.Free();
	CGL::VisTriList.Free();
	m_Octree.Destroy();
	m_Texture.Free();
	m_LightedTexture.Free();
	m_Detail.Free();
}