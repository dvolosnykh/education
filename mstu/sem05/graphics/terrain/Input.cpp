#include "stdafx.h"

#include "Input.h"
#include "Observer.h"
#include "HeavenlyBody.h"


void CMouse::TranslateMessage( UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch ( msg )
	{
	case WM_LBUTTONDOWN:	Press( MOUSE_LBUTTON );		break;
	case WM_LBUTTONUP:		Release( MOUSE_LBUTTON );	break;

	case WM_RBUTTONDOWN:	Press( MOUSE_RBUTTON );		break;
	case WM_RBUTTONUP:		Release( MOUSE_RBUTTON );	break;

	case WM_MBUTTONDOWN:	Press( MOUSE_MBUTTON );		break;
	case WM_MBUTTONUP:		Release( MOUSE_MBUTTON );	break;
	}
}

void CMouse::Process( move_t & move )
{
	POINT curPos;
	GetCursorPos( &curPos );

	const POINT center =
	{
		GetSystemMetrics( SM_CXSCREEN ) >> 1,
		GetSystemMetrics( SM_CYSCREEN ) >> 1
	};

	switch ( CGL::draw_mode )
	{
	case DRAW_SCENES:
		{
		move.rot_x -= ( curPos.x - center.x ) * m_speed;
		move.rot_y -= ( curPos.y - center.y ) * m_speed;

		if ( IsPressed( MOUSE_LBUTTON ) ) move.front = true;
		if ( IsPressed( MOUSE_RBUTTON ) ) move.back = true;
		}
		break;

	case DRAW_TEXTURE:
		break;
	}

	SetCursorPos( center.x, center.y );
}


void CKeyBoard::TranslateMessage( UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch ( msg )
	{
	case WM_KEYDOWN:
		Press( ( unsigned )wParam );

		switch ( CGL::draw_mode )
		{
		case DRAW_SCENES:	KeyDownWhenDrawScenes( wParam, lParam );	break;
		case DRAW_TEXTURE:	KeyDownWhenDrawTexture( wParam, lParam );	break;
		}
		break;

	case WM_KEYUP:
		Release( ( unsigned )wParam );
		break;
	}
}

void CKeyBoard::KeyDownWhenDrawScenes( WPARAM key, LPARAM data )
{
	switch ( key )
	{
	case KEY_EXIT:	SendMessage( CApp::m_hWnd, WM_CLOSE, 0, 0 );	break;

	case KEY_WND_MODE:	CGL::ToggleWindowMode();	break;
	case KEY_DRAW_MODE:	CGL::ToggleDrawMode();		break;
	case KEY_DEFAULTS:	CGL::InitObserversPos();	break;

	case KEY_OBS_0:	case KEY_OBS_1:	case KEY_OBS_2:	case KEY_OBS_3:
		// Allow the only one switch-key to be pressed.
		if ( IsPressed( KEY_OBS_0 ) + IsPressed( KEY_OBS_1 ) + IsPressed( KEY_OBS_2 ) + IsPressed( KEY_OBS_3 ) == 1 )
		{
			CObserver * pObserver = &CGL::Observer[ key - KEY_OBS_0 ];

			switch ( CGL::window_mode )
			{
			case WND_FULL:
				if ( IsPressed( KEY_SWITCH_MAIN_OBS ) )
				{
					if ( pObserver != CGL::pMain )
					{
						CGL::pActive->updated = true;
						CGL::pMain = pObserver;
					}
				}
				else
				{
					if ( pObserver != CGL::pActive )
					{
						pObserver->updated = true;
						CGL::pActive = pObserver;
					}
				}
				break;

			case WND_SPLIT_4:
			case WND_SPLIT_1_3:
				if ( IsPressed( KEY_SWITCH_MAIN_OBS ) )
				{
					if ( pObserver != CGL::pMain )
					{
						CGL::AllObserversUpdated();
						CGL::pMain = pObserver;
					}
				}
				else
				{
					if ( pObserver != CGL::pActive )
					{
						CGL::pActive->updated = true;
						pObserver->updated = true;
						CGL::pActive = pObserver;
					}
				}
				break;
			}
		}
		break;

	case KEY_ACCEL:		CGL::pActive->Accelerate();		break;
	case KEY_DECEL:		CGL::pActive->Decelerate();		break;

	case KEY_CHANGE_TYPE:	CGL::pActive->ToggleType();	break;
	}
}

void CKeyBoard::KeyDownWhenDrawTexture( WPARAM key, LPARAM data )
{
	switch ( key )
	{
	case KEY_EXIT:		CGL::ToggleDrawMode();		break;

	case KEY_CHANGE_TYPE:
		CTexMode::shadows = !CTexMode::shadows;
		CTexMode::updated = true;
		break;

	case KEY_DRAW_MODE:	CGL::ToggleDrawMode();		break;
	case KEY_DEFAULTS:	CTexMode::Init();			break;

	case KEY_TEX_OFF_INC:	CTexMode::IncOffset();	break;
	case KEY_TEX_OFF_DEC:	CTexMode::DecOffset();	break;

	case KEY_TEX_ZOOM_IN:	CTexMode::ZoomIn();		break;
	case KEY_TEX_ZOOM_OUT:	CTexMode::ZoomOut();	break;
	}
}

void CKeyBoard::Process( move_t & move )
{
	switch ( CGL::draw_mode )
	{
	case DRAW_SCENES:	ProcessWhenDrawScenes( move );	break;
	case DRAW_TEXTURE:	ProcessWhenDrawTexture();		break;
	}
}

void CKeyBoard::ProcessWhenDrawScenes( move_t & move )
{
	if ( IsPressed( HBODY_MOVE ) )
	{
		if ( g_param.Shadows || g_param.SimpleLighting )
		{
			float angle_x = 0.0f, angle_y = 0.0f;

			if ( IsPressed( HBODY_MOVE_UP ) )		angle_y += HBODY_MOVE_LONGITUDE;
			if ( IsPressed( HBODY_MOVE_DOWN ) )	angle_y -= HBODY_MOVE_LONGITUDE;
			if ( IsPressed( HBODY_MOVE_CCW ) )	angle_x += HBODY_MOVE_LATITUDE;
			if ( IsPressed( HBODY_MOVE_CW ) )		angle_x -= HBODY_MOVE_LATITUDE;

			CGL::DisplaceHeavenlyBody( angle_x, angle_y );
		}
	}
	else
	{
		if ( IsPressed( KEY_PITCH_UP ) )	move.rot_y += 1.0f;
		if ( IsPressed( KEY_PITCH_DOWN ) )	move.rot_y -= 1.0f;
		if ( IsPressed( KEY_YAW_LEFT ) )	move.rot_x -= 1.0f;
		if ( IsPressed( KEY_YAW_RIGHT ) )	move.rot_x += 1.0f;

		if ( IsPressed( KEY_MOVE_FRONT ) )		move.front = true;
		if ( IsPressed( KEY_MOVE_BACK ) )		move.back  = true;
		if ( IsPressed( KEY_STRAFE_LEFT ) )		move.left  = true;
		if ( IsPressed( KEY_STRAFE_RIGHT ) )	move.right = true;

		if ( IsPressed( KEY_ACCEL ) )	move.accel = true;
		if ( IsPressed( KEY_DECEL ) )	move.decel = true;
	}
}

void CKeyBoard::ProcessWhenDrawTexture()
{
	if ( CTexMode::shadows && IsPressed( HBODY_MOVE ) )
	{
		if ( g_param.Shadows || g_param.SimpleLighting )
		{
			float angle_x = 0.0f, angle_y = 0.0f;

			if ( IsPressed( HBODY_MOVE_UP ) )		angle_y += HBODY_MOVE_LONGITUDE;
			if ( IsPressed( HBODY_MOVE_DOWN ) )	angle_y -= HBODY_MOVE_LONGITUDE;
			if ( IsPressed( HBODY_MOVE_CCW ) )	angle_x += HBODY_MOVE_LATITUDE;
			if ( IsPressed( HBODY_MOVE_CW ) )		angle_x -= HBODY_MOVE_LATITUDE;

			CGL::DisplaceHeavenlyBody( angle_x, angle_y );
		}
	}
	else
	{
		if ( IsPressed( KEY_TEX_TOP ) )			CTexMode::ScrollTop();
		if ( IsPressed( KEY_TEX_BOTTOM ) )		CTexMode::ScrollBottom();
		if ( IsPressed( KEY_TEX_LEFTMOST ) )	CTexMode::ScrollLeftmost();
		if ( IsPressed( KEY_TEX_RIGHTMOST ) )	CTexMode::ScrollRightmost();

		if ( IsPressed( KEY_TEX_UP ) )		CTexMode::ScrollUp();
		if ( IsPressed( KEY_TEX_DOWN ) )	CTexMode::ScrollDown();
		if ( IsPressed( KEY_TEX_LEFT ) )	CTexMode::ScrollLeft();
		if ( IsPressed( KEY_TEX_RIGHT ) )	CTexMode::ScrollRight();
	}
}


void CInput::Clear()
{
	CKeyBoard::Clear();
	CMouse::Clear();
}

void CInput::TranslateMessage( UINT msg, WPARAM wParam, LPARAM lParam )
{
	if ( IS_MOUSE_MSG( msg ) )
		CMouse::TranslateMessage( msg, wParam, lParam );
	else if ( IS_KEY_MSG( msg ) )
		CKeyBoard::TranslateMessage( msg, wParam, lParam );
}

void CInput::Process()
{
	move_t move = { false, false, false, false, false, false, 0.0f, 0.0f };

	CMouse::Process( move );
	CKeyBoard::Process( move );

	if ( m_invert ) move.rot_y = -move.rot_y;

	CGL::pActive->Apply( &move );
}