#include "stdafx.h"

#include "ProgressWindow.h"


#define WINDOW_CLASS	TEXT( "ProgressWindowClass" )
#define WINDOW_TITLE	TEXT( "Loading World" )


HWND		CProgressWindow::m_hWnd = NULL;
HWND		CProgressWindow::m_hStatic = NULL;
HBITMAP		CProgressWindow::m_hBmp = NULL;
HDC			CProgressWindow::m_hCompDC = NULL;
TCHAR		CProgressWindow::m_pszCurrentTask[ MAX_TASK_LENGTH ];


ATOM CProgressWindow::RegisterClass()
{
	// Register the window class
	WNDCLASSEX wcex;
	wcex.cbSize			= sizeof( WNDCLASSEX );
	wcex.style			= CS_HREDRAW | CS_VREDRAW | CS_PARENTDC;
	wcex.lpfnWndProc	= CProgressWindow::WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= CApp::m_hInstance;
	wcex.hIcon			= NULL;
	wcex.hCursor		= NULL;
	wcex.hbrBackground	= NULL;
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= WINDOW_CLASS;
	wcex.hIconSm		= NULL;

	return RegisterClassEx( &wcex );
}

bool CProgressWindow::InitInstance( LPCTSTR pszFileName )
{
	const int screen_x = GetSystemMetrics( SM_CXSCREEN );
	const int screen_y = GetSystemMetrics( SM_CYSCREEN );

	m_hWnd = CreateWindowEx( WS_EX_APPWINDOW, WINDOW_CLASS,
		WINDOW_TITLE, WS_POPUP | WS_CLIPCHILDREN,
		( screen_x - WINDOW_SIZE_X ) >> 1, ( screen_y - WINDOW_SIZE_Y ) >> 1,
		WINDOW_SIZE_X, WINDOW_SIZE_Y, CApp::m_hWnd, NULL, CApp::m_hInstance, NULL );

	if ( m_hWnd == NULL )
		return ( false );

	m_hStatic = CreateWindow( TEXT( "STATIC" ), NULL,
		SS_CENTER | WS_VISIBLE | WS_CHILD, 10, WINDOW_SIZE_Y - 25, 325, 15,
		m_hWnd, NULL, CApp::m_hInstance, NULL );

	if ( m_hStatic != NULL )
	{
		HFONT hFont = CreateFont( -11 , 0, 0, 0, FW_NORMAL, 0, 0, 0, 0, 0, 0, 0, 0, "Tahoma" );
		SendMessage( m_hStatic, WM_SETFONT, ( WPARAM )hFont, MAKELPARAM( FALSE, 0 ) );
	}

	HDC hDC = GetDC( m_hWnd );
	m_hCompDC = CreateCompatibleDC( hDC );
	ReleaseDC( m_hWnd, hDC );
	hDC = NULL;
	if ( m_hCompDC != NULL )
		LoadBackground( pszFileName );

	return ( true );
}

bool CProgressWindow::Create( LPCTSTR pszFileName )
{
	const bool isOK = InitInstance( pszFileName );
	if ( isOK ) InitWindow();

	return ( isOK );
}

void CProgressWindow::Destroy()
{
	if ( m_hBmp != NULL )
	{
		SelectObject( m_hCompDC, NULL );
		DeleteObject( m_hBmp );
		m_hBmp = NULL;
	}
	DeleteDC( m_hCompDC );
	m_hCompDC = NULL;

	DeleteObject( ( HFONT )SendMessage( m_hStatic, WM_GETFONT, 0, 0 ) );
	DeinitWindow();
	DestroyWindow( m_hWnd );
	m_hWnd = NULL;
}

void CProgressWindow::InitWindow()
{
	SetWindowPos( m_hWnd, HWND_TOP, 0, 0, 0, 0,
		SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW );
}

void CProgressWindow::DeinitWindow()
{
	ShowWindow( m_hWnd, SW_HIDE );
}

void CProgressWindow::SetTask( LPCTSTR pszTaskDescription )
{
	_tcscpy( m_pszCurrentTask, pszTaskDescription );
	SetProgress( 0 );
}

void CProgressWindow::SetProgress( unsigned cur )
{
	if ( m_hStatic == NULL )
		return;

	static unsigned prev;

	if ( cur > 100 )
		cur = 100;

	if ( cur != prev )
	{
		// Change the %i token to the percent value
		TCHAR szBuffer[ MAX_TASK_LENGTH ];
		_stprintf( szBuffer, m_pszCurrentTask, cur );
		SetWindowText( m_hStatic, szBuffer );

		prev = cur;
	}
}

bool CProgressWindow::LoadBackground( LPCTSTR pszFileName )
{
	if ( m_hCompDC == NULL )
		return ( false );

	// Destroy the old bitmap
	if ( m_hBmp != NULL )
	{
		SelectObject( m_hCompDC, NULL );
		DeleteObject( m_hBmp);
	}

	// Load the new image
	m_hBmp = ( HBITMAP )LoadImage( CApp::m_hInstance, pszFileName, IMAGE_BITMAP,
		0, 0, LR_LOADFROMFILE | LR_DEFAULTSIZE );

	if ( m_hBmp == NULL )
		return ( false );

	SelectObject( m_hCompDC, m_hBmp );

	return ( true );
}

LRESULT CALLBACK CProgressWindow::WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	LRESULT result = 0;

	switch ( message )
	{
	case WM_ACTIVATE:
		// Since while this window is on the screen a huge amount of
		// calculations are done in a continuous way there's no
		// opportunity to switch to/from this window...
		// Message-loop should be provided.
		if ( !HIWORD( wParam ) )
		{
			if ( LOWORD( wParam ) == WA_INACTIVE )
			{
				ShowWindow( hWnd, SW_MINIMIZE );
			}
			else
			{
				InvalidateRect( hWnd, NULL, FALSE );
				UpdateWindow( hWnd );
				ShowWindow( hWnd, SW_RESTORE );
			}
		}
		break;

	case WM_PAINT:
		{
		PAINTSTRUCT ps;
		HDC hDC = BeginPaint( hWnd, &ps );

		if ( hDC == NULL )
			break;

		if ( m_hBmp != NULL && m_hCompDC != NULL )
		{
			BitBlt( hDC, 0, 0, WINDOW_SIZE_X, WINDOW_SIZE_Y, m_hCompDC, 0, 0, SRCCOPY );
		}
		else
		{
			// No image, just clear the background
			RECT rc;
			GetClientRect( hWnd, &rc );
			FillRect( hDC, &rc, ( HBRUSH )GetSysColorBrush( COLOR_MENU ) );
		}

		EndPaint( hWnd, &ps );
		}
		break;

	case WM_CTLCOLORSTATIC:
		{
		HDC hDC = ( HDC )wParam;
		SetTextColor( hDC, RGB( 255, 255, 255 ) );
		SetBkMode( hDC, TRANSPARENT );

		result = ( LRESULT )GetStockObject( BLACK_BRUSH );
		}
		break;

	default:
		result = DefWindowProc( hWnd, message, wParam, lParam );
		break;
	}

	return ( result );
};