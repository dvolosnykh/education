#ifndef TRIANGLE_H
#define TRIANGLE_H


#include "RayPlane.h"
#include "plane.h"
#include "Sight.h"


class CTriangle : public CPlane
{
public:
	unsigned m_index[ 3 ];
	unsigned m_FrameIndex;

private:
	CPlane m_AntiPlane[ 3 ];

public:
	CTriangle() : m_FrameIndex( 0 ) {};
	CTriangle( const unsigned i0, const unsigned i1, const unsigned i2 ) : m_FrameIndex( 0 ) { Set( i0, i1, i2 ); };
	~CTriangle() {};

	void Set( const unsigned i0, const unsigned i1, const unsigned i2 );
	bool CheckIntersection( const CRay * const ray ) const;
	float GetSmoothHeight( const float at_x, const float at_z ) const;

	bool IsRendered() const;
	void Render();

	void Draw( const CObserver * const pObserver ) const;

private:
	void SortIndicesByY( unsigned & i0, unsigned & i1, unsigned & i2 ) const;
	void MinMaxX( LONG & minx, LONG & maxx ) const;
	void MinMaxZ( float & minz, float & maxz ) const;
	void DrawHorzLine( const CObserver * const pObserver, float x_main, float x_side, const unsigned scan_line, float z, const float dz, CVertex2D t, const CVector2D dt ) const;
	void CalcAntiPlanes( const CVertex3D & v0, const CVertex3D & v1, const CVertex3D & v2 );
	bool InsideAntiPrism( const CVertex3D point ) const;
};


typedef CTDArray< CTriangle > CTriangleArray;
typedef CTDArray< CTriangle * > CTriangleList;

extern const CTriangle * pWanted;

class CVisibleTriList : public CTDArray< const CTriangle * >
{
private:
	unsigned m_counter;

public:
	CVisibleTriList() { ResetCounter(); };
	virtual ~CVisibleTriList() {};

	unsigned GetTriNum() { return ( m_counter ); };
	void ResetCounter() { m_counter = 0; };
	void Add( const CTriangle * const tri ) { ptr[ m_counter++ ] = tri; };
	void DrawAll( const CObserver * const pObserver ) const;
};

#endif