#ifndef OCTREENODE_H
#define OCTREENODE_H


#include "Bound.h"
#include "Triangle.h"
#include "ProgressWindow.h"
#include "Frustum.h"
#include "Observer.h"


#define NODES_NUM	8


class COctreeNode
{
private:
	CTDArray< COctreeNode > m_ChildNodes;
	CTriangleList m_Triangles;
	CBound	m_Bound;

public:
	COctreeNode() {};
	~COctreeNode() {};

	void Build( const CTriangleList & Triangles ) { Build( Triangles, NULL ); };
	void Destroy();

	bool CheckIntersection( const CRay * const ray ) const;
	void Render( const CObserver * const pObserver, const bool HSR = true ) const;

	bool IsEmpty() const { return ( IsLeafNode() && m_Triangles.GetSize() == 0 ); };
	void DrawNodes( bool SkipEmpty = true ) const;

private:
	void Build( const CTriangleList & Triangles, const COctreeNode * const pParent );

	bool CheckSubnodes( const CRay * const ray ) const;
	bool CheckTriangles( const CRay * const ray ) const;

	void RenderSubnodes( const CObserver * const pObserver, const bool HSR = true ) const;
	void RenderTriangles() const;

	unsigned FormList( const CTriangleList & Triangles );
	void Subdivide();
	bool IsLeafNode() const { return m_ChildNodes.IsEmpty(); };
};

#endif