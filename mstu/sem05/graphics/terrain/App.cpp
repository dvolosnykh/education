#include "stdafx.h"

#include "App.h"
#include "Timer.h"
#include "SetupDialog.h"


#define WINDOW_CLASS	TEXT( "TerrainClass" )
#define WINDOW_TITLE	TEXT( "Terrain" )



HWND		CApp::m_hWnd = NULL;
HINSTANCE	CApp::m_hInstance = NULL;
POINT		CApp::m_offs;

CTimer		CApp::m_Timer;
CInput		CApp::m_Input;
bool		CApp::m_quit;


ATOM CApp::RegisterClass()
{
	WNDCLASSEX wcex;
	wcex.cbSize			= sizeof( WNDCLASSEX );
	wcex.style			= CS_OWNDC;
	wcex.lpfnWndProc	= CApp::WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= m_hInstance;
	wcex.hIcon			= NULL;
	wcex.hCursor		= NULL;
	wcex.hbrBackground	= NULL;
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= WINDOW_CLASS;
	wcex.hIconSm		= NULL;

	return RegisterClassEx( &wcex );
}

bool CApp::InitInstance()
{
	m_offs.x = g_param.Fullscreen ? 0 : ( GetSystemMetrics( SM_CXSCREEN ) - g_param.ResWidth ) >> 1;
	m_offs.y = g_param.Fullscreen ? 0 : ( GetSystemMetrics( SM_CYSCREEN ) - g_param.ResHeight ) >> 1;

	m_hWnd = CreateWindowEx( WS_EX_APPWINDOW, WINDOW_CLASS, WINDOW_TITLE,
		WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, m_offs.x, m_offs.y,
		g_param.ResWidth, g_param.ResHeight, NULL, NULL, m_hInstance, NULL );
	if ( m_hWnd == NULL )
	{
		DWORD error = GetLastError();
		return ( false );
	}

	bool ok = CGL::InitDisplay( g_param.ResWidth, g_param.ResHeight, g_param.Fullscreen );
	if ( !ok )
		return ( false );

	return InitWindow();
}

bool CApp::InitWindow()
{
	bool isOK = false;
	if ( isOK = CGL::InitEngine() )
	{
		SetWindowPos( m_hWnd,
#ifdef _DEBUG
		HWND_TOP
#else
		HWND_TOPMOST
#endif
			, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW );
	}

	return ( isOK );
}

void CApp::DeinitWindow()
{
	ShowWindow( m_hWnd, SW_HIDE );
}

int APIENTRY CApp::Main( HINSTANCE hInstance, HINSTANCE hPrev, LPTSTR lpCmdLine, int nCmdShow )
{
	int result = 0;

	m_hInstance = hInstance;

	while ( CSetupDialog::Main() )
		result = CApp::Run();

	return ( result );
}

int CApp::Run()
{
	if ( !InitInstance() )
		return ( -1 );

	MSG msg;
	m_quit = false;
	for ( m_Input.Clear(), m_Timer.Start();
		!m_quit && GetMessage( &msg, m_hWnd, 0, 0 );
		m_Input.Process(), m_Timer.MakeNote() )
	{
		CGL::UpdateObservers( m_Timer.GetLastInterval() );
		CGL::Draw( m_Timer.GetFrameCount() );

		do
		{
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		while ( !m_quit && PeekMessage( &msg, m_hWnd, 0, 0, PM_REMOVE ) );
	}

	return ( 0 );
}

LRESULT CALLBACK CApp::WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	LRESULT result = 0;

	switch ( msg )
	{
	case WM_CLOSE:
		CGL::DeinitDisplay();
		DeinitWindow();
		DestroyWindow( hWnd );
		m_quit = true;
		break;

	case WM_ACTIVATE:
		if ( !HIWORD( wParam ) )
		{
			if ( LOWORD( wParam ) == WA_INACTIVE )
			{
				ShowWindow( hWnd, SW_MINIMIZE );
				ShowCursor( TRUE );
			}
			else
			{
				const POINT center =
				{
					GetSystemMetrics( SM_CXSCREEN ) >> 1,
					GetSystemMetrics( SM_CYSCREEN ) >> 1
				};
				SetCursorPos( center.x, center.y );

				ShowWindow( hWnd, SW_RESTORE );
				ShowCursor( FALSE );

				CGL::RefreshDisplay();
			}
		}

		break;

	default:
		if ( IS_INPUT_MSG( msg ) )
			m_Input.TranslateMessage( msg, wParam, lParam );
		else
			result = DefWindowProc( hWnd, msg, wParam, lParam );

		break;
	}

	return ( result );
}