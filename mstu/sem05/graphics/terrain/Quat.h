#ifndef QUAT_H
#define QUAT_H


#include "Matrix.h"


#define DEF_EPSILON		0.0001


class CQuat
{
public:
	float w;
	CVector3D v;

public:
	CQuat() {};
	CQuat( const float init_w, const float init_x, const float init_y, const float init_z ) { Set( init_w, init_x, init_y, init_z ); };

	void Reset();
	void Set( const CQuat & new_q ) { w = new_q.w, v = new_q.v; };
	void Set( float new_w, const float new_x, const float new_y, const float new_z );

	void Scale( const float k ) { w *= k, v *= k; };
	void Normalize() { Scale( 1.0f / sqrtf( w * w + v.Length2() ) ); };

	CQuat & Mult( const CQuat & quat );

	CMatrix GetMatrix();
	CVector3D GetDirection();
};

#endif