#include "stdafx.h"

#include "Observer.h"


void CObserver::ToggleType( const int index /* = TYPE_NEXT */ )
{
	const int type = index >= 0 ? index : ( m_type + 1 ) % TYPES_NUM;
	if ( type != m_type ) SetType( type );
		
}

void CObserver::SetType( const int new_type )
{
	switch ( new_type )
	{
	case TYPE_AIRBORNE:
		m_StartSpeed *= 2.0f;
		m_type = new_type;
		updated = true;
		break;

	case TYPE_WALKER:
		if ( CanMove( m_pos, m_pos, new_type ) )
		{
			m_StartSpeed *= 0.5f;
			m_type = new_type;
			updated = true;
		}
		break;
	}
}

void CObserver::Reset()
{
	CCamera::Reset();
	Stop();
}

void CObserver::LookAt( const CVertex3D target )
{
	const CVector3D dir = target - m_pos;

	if ( !dir.IsNull() )
	{
		CCamera::Reset();

		if ( dir.x != 0.0f || dir.z != 0.0f )
		{
			float rot_x = 0.0f;

			if ( dir.x > 0.0f )
				rot_x = -float( M_PI_2 ) - atanf( dir.z / dir.x );
			else if ( dir.x < 0.0f )
				rot_x = float( M_PI_2 ) - atanf( dir.z / dir.x );
			else if ( dir.z > 0.0f )
				rot_x = -float( M_PI );

			RotateHorz( rad_to_degree( rot_x ) );
		}

		if ( dir.y != 0.0f )
		{
			const float len_xz = CVector2D( dir.x, dir.z ).Length();

			const float rot_y =
				len_xz > 0.0f ? atanf( dir.y / len_xz ) :
				float( dir.y > 0.0f ? M_PI_2 : -M_PI_2 );

			RotateVert( rad_to_degree( rot_y ) );
		}
	}
}

void CObserver::Apply( const move_t * const pmove )
{
	if ( pmove->rot_x != 0.0f || pmove->rot_y != 0.0f )
	{
		RotateHorz( pmove->rot_x );
		RotateVert( pmove->rot_y );
		updated = true;
	}

	SetMotion( pmove->front, pmove->back, pmove->left, pmove->right );
}

void CObserver::UpdateView()
{
	if ( updated )
	{
		view = m_Quat.GetMatrix() - m_pos;
		m_view_dir = -CVector3D( view[ 2 ][ 0 ], view[ 2 ][ 1 ], view[ 2 ][ 2 ] );

		UpdateFrustum(
#ifdef USE_OPENGL_PROJECTION
			view =
#endif
			proj * view );
	}
}

void CObserver::UpdatePos( const DWORD elapsed )
{
	if ( m_CurSpeed > 0.0f )
	{
		Move( ScaleToElapsedTime( m_CurSpeed, elapsed ) );
		m_CurSpeed -= ScaleToElapsedTime( m_SlowDown, elapsed );
	}
	else
		Stop();
}

void CObserver::Move( float speed )
{
	float dir_angle = 0.0f;

	if ( m_forward )
	{
		if ( m_left )		dir_angle = 45.0f;
		else if ( m_right )	dir_angle = -45.0f;
	}
	else if ( m_backward )
	{
		if ( m_left )		dir_angle = -45.0f;
		else if ( m_right )	dir_angle = 45.0f;

		speed = -speed;
	}
	else if ( m_left )	dir_angle = 90.0f;
	else if ( m_right )	dir_angle = -90.0f;

    if ( dir_angle != 0.0f )
	{
		CQuat move = m_Quat;
		move.Mult( CQuat( dir_angle, 0.0f, 1.0f, 0.0f ) );
		m_move_dir = move.GetDirection();
	}
	else
		m_move_dir = m_view_dir;

	if ( m_type == TYPE_WALKER )
	{
		m_move_dir.y = 0.0f;
		m_move_dir.Normalize();
	}

	CVertex3D new_pos = m_pos + m_move_dir * speed;

	if ( CanMove( new_pos, new_pos ) )
	{
		m_pos = new_pos;
		updated = true;
	}
	else
		Stop();
}

bool CObserver::CanMove( CVertex3D new_pos, CVertex3D & dest_pos, const int index /* = -1 */ )
{
	bool go;

	const int type = index >= 0 ? index : m_type;

	const CVertex3D far_pos = new_pos + m_move_dir * m_size;

	switch ( type )
	{
	case TYPE_AIRBORNE:
		if ( g_param.CheckCollision )
		{
			go = !CGL::Landscape.CheckIntersection( CRay( m_pos, far_pos ) );

			if ( go )
			{
				go = !CGL::Landscape.InScope( new_pos.x, new_pos.z ) ||
					( new_pos.y >= CGL::Landscape.GetSmoothHeight( new_pos.x, new_pos.z ) + m_height );
			}
		}
		else
			go = true;

		break;

	case TYPE_WALKER:
		go = CGL::Landscape.InScope( new_pos.x, new_pos.z );

		if ( go )
		{
			new_pos.y = CGL::Landscape.GetSmoothHeight( new_pos.x, new_pos.z ) + m_height;
			dest_pos = new_pos;
		}

		break;
	}

	return ( go );
}

void CObserver::SetMotion( bool f, bool b, bool l, bool r )
{
	// Avoid opposite directions at the same time.
	if ( l && r ) r = l = false;
	if ( f && b ) f = b = false;

	if ( f || b || l || r )
	{
		m_forward = f, m_backward = b, m_left = l, m_right = r;
		m_CurSpeed = m_StartSpeed;
	}
}

void CObserver::Stop()
{
	m_forward = m_backward = m_left = m_right = false;
	m_CurSpeed = 0.0f;
}