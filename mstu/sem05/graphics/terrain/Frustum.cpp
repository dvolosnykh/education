#include "stdafx.h"

#include "Frustum.h"


void CFrustum::UpdateFrustum( const CMatrix & m )
{
	m_Frustum[ PLANE_RIGHT ].Set
	(
		CVector3D
		(
			m[ 3 ][ 0 ] - m[ 0 ][ 0 ],
			m[ 3 ][ 1 ] - m[ 0 ][ 1 ],
			m[ 3 ][ 2 ] - m[ 0 ][ 2 ]
		),
			m[ 3 ][ 3 ] - m[ 0 ][ 3 ]
	);
	m_Frustum[ PLANE_LEFT ].Set
	(
		CVector3D
		(
			m[ 3 ][ 0 ] + m[ 0 ][ 0 ],
			m[ 3 ][ 1 ] + m[ 0 ][ 1 ],
			m[ 3 ][ 2 ] + m[ 0 ][ 2 ]
		),
			m[ 3 ][ 3 ] + m[ 0 ][ 3 ]
	);
	m_Frustum[ PLANE_BOTTOM ].Set
	(
		CVector3D
		(
			m[ 3 ][ 0 ] - m[ 1 ][ 0 ],
			m[ 3 ][ 1 ] - m[ 1 ][ 1 ],
			m[ 3 ][ 2 ] - m[ 1 ][ 2 ]
		),
			m[ 3 ][ 3 ] - m[ 1 ][ 3 ]
	);
	m_Frustum[ PLANE_TOP ].Set
	(
		CVector3D
		(
			m[ 3 ][ 0 ] + m[ 1 ][ 0 ],
			m[ 3 ][ 1 ] + m[ 1 ][ 1 ],
			m[ 3 ][ 2 ] + m[ 1 ][ 2 ]
		),
			m[ 3 ][ 3 ] + m[ 1 ][ 3 ]
	);
	m_Frustum[ PLANE_FAR ].Set
	(
		CVector3D
		(
			m[ 3 ][ 0 ] - m[ 2 ][ 0 ],
			m[ 3 ][ 1 ] - m[ 2 ][ 1 ],
			m[ 3 ][ 2 ] - m[ 2 ][ 2 ]
		),
			m[ 3 ][ 3 ] - m[ 2 ][ 3 ]
	);
	m_Frustum[ PLANE_NEAR ].Set
	(
		CVector3D
		(
			m[ 3 ][ 0 ] + m[ 2 ][ 0 ],
			m[ 3 ][ 1 ] + m[ 2 ][ 1 ],
			m[ 3 ][ 2 ] + m[ 2 ][ 2 ]
		),
			m[ 3 ][ 3 ] + m[ 2 ][ 3 ]
	);
}

unsigned CFrustum::BoundLocation( const CBound * const bound ) const
{
	unsigned cull = SphereLocation( &bound->m_Center, bound->m_Radius );

	if ( cull == PARTIAL_INSIDE )
		cull = BoxLocation( &bound->m_Min, &bound->m_Max );

	return ( cull );
}

unsigned CFrustum::SphereLocation( const CVertex3D * const center, const float radius ) const
{
	register unsigned count = 0;
	register float sign_dist;
	register unsigned i;
	for ( i = 0; i < PLANE_NUM &&
		( sign_dist = m_Frustum[ i ].SignDistance( center ) ) > -( radius + SMALL_VALUE );
		i++ )
	{
		if ( sign_dist >= radius )
			count++;
	}

	return
	(
		i < PLANE_NUM ? FULL_OUTSIDE :
		count == PLANE_NUM ? FULL_INSIDE :
		PARTIAL_INSIDE
	);
}

unsigned CFrustum::BoxLocation( const CVertex3D * const min, const CVertex3D * const max ) const
{
	const CVertex3D point[ 8 ] =
	{
		*min,
		CVertex3D( min->x, min->y, max->z ),
		CVertex3D( min->x, max->y, min->z ),
		CVertex3D( min->x, max->y, max->z ),
		CVertex3D( max->x, min->y, min->z ),
		CVertex3D( max->x, min->y, max->z ),
		CVertex3D( max->x, max->y, min->z ),
		*max
	};

	register unsigned count = 0;
	register unsigned count1 = 1;
	register unsigned i;
	for ( i = 0; i < PLANE_NUM && count1 > 0; i++ )
	{
		count1 = 0;

		for ( register unsigned j = 0; j < 8; j++ )
			if ( m_Frustum[ i ].SignDistance( point + j ) > -SMALL_VALUE )
				count1++;

		if ( count1 == 8 ) count++;
	}

	return
	(
		i < PLANE_NUM ? FULL_OUTSIDE :
		count == PLANE_NUM ? FULL_INSIDE :
		PARTIAL_INSIDE
	);
}

void CFrustum::Set( const float FOV, const float aspect, const float Near, const float Far )
{
	m_Aspect = aspect;
	m_Near = Near, m_Far = Far;
	m_FOV = FOV;
	m_TanY = tanf( degree_to_rad( m_FOV * 0.5f ) );
	m_TanX = m_TanY * m_Aspect;
}