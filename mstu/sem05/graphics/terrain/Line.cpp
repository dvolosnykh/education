#include "stdafx.h"

#include "Line.h"


float CLine::Length() const
{
	const float dx = dot[ ONE ].x - dot[ TWO ].x;
	const float dy = dot[ ONE ].y - dot[ TWO ].y;
	return sqrtf( dx * dx + dy * dy );
}

void CLine::FindCoeffs()
{
	CVertex2D & dot1 = dot[ ONE ];
	CVertex2D & dot2 = dot[ TWO ];

	m_Normal.x = dot1.y - dot2.y;
	m_Normal.y = dot2.x - dot1.x;
	m_C = dot1.x * dot2.y - dot2.x * dot1.y;
}

float CLine::SignDistance( const CVertex2D & point ) const
{
	return ( m_Normal * point + m_C );
}