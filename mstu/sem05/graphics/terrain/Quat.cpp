#include "stdafx.h"

#include "Quat.h"


void CQuat::Reset()
{
	w = 1.0f;
	v.Set( 0.0f, 0.0f, 0.0f );
}

void CQuat::Set( float angle, const float new_x, const float new_y, const float new_z )
{
	if ( angle != 0.0f )
	{
		angle = degree_to_rad( angle * 0.5f );

		v.Set( new_x, new_y, new_z );
		v.Normalize();
		v *= sinf( angle );

		w = cosf( angle );
	}
	else
		Reset();	
}

CQuat & CQuat::Mult( const CQuat & quat )
{
	const float A = (   w + v.x ) * (   quat.w + quat.v.x );
	const float B = ( v.z - v.y ) * ( quat.v.y - quat.v.z );
	const float C = (   w - v.x ) * ( quat.v.y + quat.v.z );
	const float D = ( v.y + v.z ) * (   quat.w - quat.v.x );

	const float E = ( v.x + v.z ) * ( quat.v.x + quat.v.y );
	const float F = ( v.x - v.z ) * ( quat.v.x - quat.v.y );
	const float EmF = E - F;
	const float EpF = E + F;

	const float G = ( w + v.y ) * ( quat.w - quat.v.z );
	const float H = ( w - v.y ) * ( quat.w + quat.v.z );
	const float GmH = G - H;
	const float GpH = G + H;

	  w = B - ( EpF - GpH ) * 0.5f;
	v.x = A - ( EpF + GpH ) * 0.5f; 
	v.y = C + ( EmF + GmH ) * 0.5f;
	v.z = D + ( EmF - GmH ) * 0.5f;

	return ( *this );
}

CMatrix CQuat::GetMatrix()
{
	Normalize();

	const float x2 = v.x + v.x;
	const float y2 = v.y + v.y;
	const float z2 = v.z + v.z;
	const float xx = v.x * x2;
	const float xy = v.x * y2;
	const float xz = v.x * z2;
	const float yy = v.y * y2;
	const float yz = v.y * z2;
	const float zz = v.z * z2;
	const float wx =   w * x2;
	const float wy =   w * y2;
	const float wz =   w * z2;

	CMatrix m;
	m[ 0 ][ 0 ] = 1.0f - ( yy + zz );
	m[ 1 ][ 0 ] = xy - wz;
	m[ 2 ][ 0 ] = xz + wy;

	m[ 0 ][ 1 ] = xy + wz;
	m[ 1 ][ 1 ] = 1.0f - ( xx + zz );
	m[ 2 ][ 1 ] = yz - wx;

	m[ 0 ][ 2 ] = xz - wy;
	m[ 1 ][ 2 ] = yz + wx;
	m[ 2 ][ 2 ] = 1.0f - ( xx + yy );

	m[ 3 ][ 0 ] = m[ 3 ][ 1 ] = m[ 3 ][ 2 ] = 0.0f;
	m[ 0 ][ 3 ] = m[ 1 ][ 3 ] = m[ 2 ][ 3 ] = 0.0f;
	m[ 3 ][ 3 ] = 1.0f;

	return ( m );
}

CVector3D CQuat::GetDirection()
{
	Normalize();

	// 2nd line of matrix... negative
	return -CVector3D
	(
		2.0f * ( v.x * v.z + w * v.y ),
		2.0f * ( v.y * v.z - w * v.x ),
		1.0f - 2.0f * ( v.x * v.x + v.y * v.y )
	);
}