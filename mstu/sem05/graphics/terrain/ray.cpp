#include "stdafx.h"

#include "ray.h"


bool CRay::IntersectsPlane( const CPlane * const plane ) const
{
	const float ds = plane->SignDistance( &m_start );
	const float de = plane->SignDistance( &m_end );
	return ( ds >= 0.0f && de <= 0.0f || ds <= 0.0f && de >= 0.0f );
}

CVertex3D CRay::IntersectionWithPlane( const CPlane * const plane ) const
{
	const float dist = plane->SignDistance( &m_start );
	const float cosine = -plane->m_Normal.Cosine( m_dir );
	const float len = m_dir.Length();
	const float t = dist / ( cosine * len );

	return ( m_start + m_dir * t );
}


bool CRay::IntersectsBoundingPlane( const CVertex3D * const bound, const unsigned i ) const
{
	const float ds = m_start[ i ] - ( *bound )[ i ];
	const float de = m_end[ i ] - ( *bound )[ i ];
	return ( ds >= 0.0f && de <= 0.0f || ds <= 0.0f && de >= 0.0f );
}

CVertex3D CRay::IntersectionWithBoundingPlane( const CVertex3D * const bound, const unsigned i ) const
{
	const float from_start = ( *bound )[ i ] - m_start[ i ];
	const float total = m_dir[ i ];
	const float t = from_start / total;
	return ( m_start + m_dir * t );
}