#include "stdafx.h"

//==================== 1D-vector ====================

inline CVector::CVector( const CVertex & v )
{
	Set( v.x );
}

inline CVector & CVector::Normalize()
{
	if ( !IsNull() )
		Scale( 1.0f / Length() );

	return ( *this );
}

inline float CVector::Cosine( const CVector v ) const
{
	float result = *this * v;

	if ( result != 0.0f )
		result /= Length() * v.Length();

	return ( result );
}

//==================== 2D-vector ====================

CVector2D::CVector2D( const CVertex2D & v )
{
	Set( v.x, v.y );
}

inline CVector2D & CVector2D::Normalize()
{
	if ( !IsNull() )
		Scale( 1.0f / Length() );

	return ( *this );
}

inline float CVector2D::Cosine( const CVector2D v ) const
{
	float result = *this * v;

	if ( result != 0.0f )
		result /= sqrtf( Length2() * v.Length2() );

	return ( result );
}

inline float CVector2D::Sine( const CVector2D v ) const
{
	const CVector3D v1 = *this;

	return v1.Sine( v );
}

inline bool CVector2D::IsCollinear( const CVector2D v ) const
{
	const CVector3D v1 = *this;

	return ( ( v1 ^ v ) == 0 );
};

//==================== 3D-vector ====================

CVector3D::CVector3D( const CVertex3D & v )
{
	Set( v.x, v.y, v.z );
}

CVector3D & CVector3D::Normalize()
{
	if ( !IsNull() )
		Scale( 1.0f / Length() );

	return ( *this );
}

CVector3D & CVector3D::VectorMult( const CVector3D v )
{
	Set( y * v.z - v.y * z, v.x * z - x * v.z, x * v.y - v.x * y );
	return ( *this );
}

float CVector3D::Cosine( const CVector3D v ) const
{
	float result = *this * v;

	if ( result != 0.0f )
		result /= sqrtf( Length2() * v.Length2() );

	return ( result );
}

inline float CVector3D::Sine( const CVector3D v ) const
{
	float result = ( *this ^ v ).Length();

	if ( result != 0.0f )
		result /= sqrtf( Length2() * v.Length2() );

	return ( result );
}