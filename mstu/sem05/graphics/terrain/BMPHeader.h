#ifndef BMPHEADER_H
#define BMPHEADER_H


#include "Header.h"


#define BITMAP_TYPE		MAKEWORD( 'M', 'B' )


struct BMPFILEHEADER
{
	BITMAPFILEHEADER file;
	BITMAPINFOHEADER info;
};


class CBMPHeader : virtual public CHeader
{
private:
	BMPFILEHEADER hdr;

public:
	CBMPHeader();
	~CBMPHeader() {};

	bool LoadHeader( FILE * const pFile );
	bool LoadHeader( LPCTSTR szFileName );
	bool IsSupported() const;

	WORD GetType() const { return ( hdr.file.bfType ); };
	DWORD GetSize() const { return ( hdr.file.bfSize ); };
	WORD GetReserved1() const { return ( hdr.file.bfReserved1 ); };
	WORD GetReserved2() const { return ( hdr.file.bfReserved2 ); };
	DWORD GetOffBits() const { return ( hdr.file.bfOffBits ); };
	DWORD GetInfoSize() const { return ( hdr.info.biSize ); };
	LONG GetWidth() const { return ( hdr.info.biWidth ); };
	LONG GetHeight() const { return ( hdr.info.biHeight ); };
	WORD GetPlanes() const { return ( hdr.info.biPlanes ); };
	WORD GetBitDepth() const { return ( hdr.info.biBitCount ); };
	DWORD GetCompression() const { return ( hdr.info.biCompression ); };
	DWORD GetSizeImage() const { return ( hdr.info.biSizeImage ); };
	LONG GetXPelsPerMeter() const { return ( hdr.info.biXPelsPerMeter ); };
	LONG GetYPelsPerMeter() const { return ( hdr.info.biYPelsPerMeter ); };
	DWORD GetClrUsed() const { return ( hdr.info.biClrUsed ); };
	DWORD GetClrImportant() const { return ( hdr.info.biClrImportant ); };

	void SetType( const WORD value = BITMAP_TYPE ) { hdr.file.bfType = value; };
	void SetSize( const DWORD value ) { hdr.file.bfSize = value; };
	void SetReserved1( const WORD value = 0 ) { hdr.file.bfReserved1 = value; };
	void SetReserved2( const WORD value = 0 ) { hdr.file.bfReserved2 = value; };
	void SetOffBits( const DWORD value ) { hdr.file.bfOffBits = value; };
	void SetInfoSize( const DWORD value = sizeof( BITMAPINFOHEADER ) ) { hdr.info.biSize = value; };
	void SetWidth( const LONG value ) { hdr.info.biWidth = value; };
	void SetHeight( const LONG value ) { hdr.info.biHeight = value; };
	void SetPlanes( const WORD value = 1 ) { hdr.info.biPlanes = value; };
	void SetBitDepth( const WORD value ) { hdr.info.biBitCount = value; };
	void SetCompression( const DWORD value = BI_RGB ) { hdr.info.biCompression = value; };
	void SetSizeImage( const DWORD value ) { hdr.info.biSizeImage = value; };
	void SetXPelsPerMeter( const LONG value ) { hdr.info.biXPelsPerMeter = value; };
	void SetYPelsPerMeter( const LONG value ) { hdr.info.biYPelsPerMeter = value; };
	void SetClrUsed( const DWORD value ) { hdr.info.biClrUsed = value; };
	void SetClrImportant( const DWORD value ) { hdr.info.biClrImportant = value; };
};

#endif