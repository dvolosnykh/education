#ifndef PARAM_H
#define PARAM_H


struct arrow_t
{
	POINT start;
	POINT end;
};

// Used to avoid unnecessary calculations during secondary launches.
struct engine_param_t
{
	bool tex_changed;
	bool light_changed;
	bool new_hmap;
	bool build_octree;
};


// Parameters that are needed to init and load the engine before the rendering can begin.
struct param_t
{
	TCHAR szHeightmapPath[ _MAX_PATH ];		// Heightmap that defines out terrain
	TCHAR szTextureSearchPath[ _MAX_PATH ];	// Search path for the terrain textures
	TCHAR szWaterTexture[ _MAX_PATH ];		// Search path of the water cubemap
	TCHAR szDetailTexture[ _MAX_PATH ];		// Filename and path of the detail texture
	TCHAR szCrosshairTexture[ _MAX_PATH ];	// Filename and path of the crosshair texture
	TCHAR szHeavenlyBodySearchPath[ _MAX_PATH ];		// Search path of the hevaenly body texture
	TCHAR szSkyBoxSearchPath[ _MAX_PATH ];	// Search path of the skybox textures
	TCHAR szLogoBitmap[ _MAX_PATH ];			// Filename and path of the logo bitmap file

	float GridStep;							// Size of one field in the terrain grid
	unsigned TexSize;						// Size of the landscape texture
	unsigned DetailTextureTiling;			// Tiling of the detail texture
	
	bool Shadows;							// Can mountains cast shadows?
	bool SimpleLighting;					// Lighting is on?
	BYTE Ambient;							// Ambient lighting
	arrow_t light_dir;						// Direction of light.
	float light_angle;						// Angle of light-falling.
	unsigned DayTime;						// Day time.

	bool ShowStats;							// Toggle the FPS and polycount display
	bool CheckCollision;					// Apply collision-detection?
	bool DrawCrosshair;						// Draw crosshair?
	bool RemoveArtifacts;
	bool DrawHeavenlyBody;						// Should we draw the sun's flare?

	bool Fullscreen;						// Fullscreen or not?
	DWORD ResWidth;							// Screen width resolution
	DWORD ResHeight;						// Screen height resolution
	float FOV;								// Filed of view
	float NearPlane;						// Near clip plane
	float FarPlane;							// Far clip plane

	bool RenderWater;						// Render the water?
	float WaveHeight;						// Inital height of a new water wave
	float WaterSpeed;						// Movement speed of the water waves
	unsigned WaterTesselation;				// Detail level of the water

	bool DrawNodes;							// Should the Octree draw its bounding volumes?
	bool UseHSR;							// Enable or disable frustum culling
	unsigned MaxTriInOctNode;				// Maximum number of triangles in a single Octree node
	float MinOctNodeSize;					// Smallest allowed Octree 
};

#endif