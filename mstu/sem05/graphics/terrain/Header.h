#ifndef HEADER_H
#define HEADER_H


class CHeader
{
public:
	CHeader() {};
	virtual ~CHeader() {};

	virtual bool LoadHeader( FILE * const pFile ) = NULL;
	virtual bool LoadHeader( LPCTSTR szFileName ) = NULL;
	virtual bool IsSupported() const;

	virtual WORD GetBitDepth() const = NULL;
	virtual DWORD GetOffBits() const = NULL;
};

#endif