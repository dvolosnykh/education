#ifndef HEAVENLY_BODY_H
#define HEAVENLY_BODY_H


#include "Texture.h"
#include "Observer.h"


#define HBODY_MOVE_LONGITUDE	5.0f
#define HBODY_MOVE_LATITUDE	10.0f


class CHeavenlyBody 
{
public:
	CVertex3D m_pos;
	CVector3D m_dir;

private:
	CTexture m_Texture;
	bool background;

public:
	CHeavenlyBody() {};
	~CHeavenlyBody() {};

	bool LoadTextures( LPCTSTR pszSearchPath );
	void Draw( const CObserver * const pObserver ) const;
	bool IsBackground() const { return ( background ); };

	void Destroy();
};

#endif