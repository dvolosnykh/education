#ifndef SETUPDIALOG_H
#define SETUPDIALOG_H


#include "Param.h"


#define PATH_TEXTURE		TEXT( "Data/Texture/" )
#define PATH_WATER			TEXT( "Data/Water/" )
#define PATH_CROSSHAIR		TEXT( "Data/Crosshair/crosshair.tga" )
#define PATH_HEAVENLY_BODY	TEXT( "Data/HeavenlyBody/" )
#define PATH_SKY			TEXT( "Data/Sky/" )
#define PATH_LOGO			TEXT( "Data/Logo/Logo.bmp" )
#define PATH_HEIGHTMAP		TEXT( "Data/Heightmap/" )
#define GRID_STEP			10.0f
#define TEX_SIZE			IDC_TX1024
#define DETAIL_TILE			0
#define CAST_SHADOWS		true
#define SIMPLE_LIGHTING		true
#define LIGHT_ANGLE			20.0f
#define AMBIENT				120
#define DAY_TIME			IDC_SUNNY_DAY
#define DRAW_CROSSHAIR		true
#define FULLSCREEN			false
#define RESOLUTION			IDC_SC1024
#define FOV_Y				90.0f
#define NEAR_PLANE			1.0f
#define FAR_PLANE			3000.0f

#define REMOVE_ARTEFACTS	true
#define SHOW_STATS			true
#define CHECK_COLLISION		true
#define DRAW_HBODY			true

#define USE_HSR				true
#define MAX_TRI_IN_NODE		20
#define MIN_NODE_SIZE		20.0f

enum { VAL_FLOAT, VAL_INT, VAL_UINT, VAL_BYTE };


extern param_t & g_param;


class canvas_t
{
public:
	POINT origin;
	LONG height, width;

public:
	bool InRect( const POINT point ) const { return InRect( point.x, point.y ); };
	bool InRect( const LONG x, const LONG y ) const;
};


class CSetupDialog
{
public:
	static param_t m_Param;
	static param_t m_OldParam;

private:
	static canvas_t preview;
	static bool map_loaded;
	static HWND m_hDlg;

	static TCHAR buffer [ _MAX_PATH ];

public:
	static int APIENTRY Main();

private:
	static bool GetItemCheck( const unsigned item_id );
	static void SetItemCheck( const unsigned item_id, const bool check = true );
	static BOOL CheckRadioButton( const unsigned first, const unsigned last, const unsigned desired );
	static void ClickButton( const unsigned item_id );
	static UINT GetItemText( const unsigned item_id, LPTSTR buffer, int count );
	static BOOL SetItemText( const unsigned item_id, LPTSTR buffer );
	static BOOL GetItemValue( const unsigned item_id, const unsigned type, void * pValue );
	static BOOL SetItemValue( const unsigned item_id, const unsigned type, const void * pValue );

	static BOOL CALLBACK DlgProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam );
	static void LoadMapList();
	static bool LoadMap();
	static void PreviewMap( const bool in_paint = false );
	static bool SaveSettings( param_t & params );
	static bool LoadSettings( const param_t & params );
	static void InitSettings( bool defaults = true );
	static void DefaultSettings( param_t & params );
};

#endif