#include "stdafx.h"

#include "SetupDialog.h"
#include "Camera.h"


void CCamera::RotateHorz( const float degrees )
{
	if ( degrees != 0.0f )
		m_Quat.Mult( CQuat( degrees, 0.0f, 1.0f, 0.0f ) );
}

void CCamera::RotateVert( const float degrees )
{
	if ( degrees != 0.0f )
		m_Quat.Mult( CQuat( degrees, 1.0f, 0.0f, 0.0f ) );
}

void CCamera::Perspective( const float FOV, const float aspect, const float Near, const float Far )
{
	Set( FOV, aspect, Near, Far );

	proj.Reset();
	proj[ 0 ][ 0 ] = 1.0f / m_TanX;
	proj[ 1 ][ 1 ] = 1.0f / m_TanY;
	proj[ 2 ][ 2 ] = -( Far + Near ) / ( Far - Near );
	proj[ 2 ][ 3 ] = -2.0f * Far * Near / ( Far - Near );
	proj[ 3 ][ 2 ] = -1.0f;
}