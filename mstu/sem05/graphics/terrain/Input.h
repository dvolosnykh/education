#ifndef INPUT_H
#define INPUT_H


#include "Observer.h"


enum
{
	MOUSE_LBUTTON,
	MOUSE_RBUTTON,
	MOUSE_MBUTTON,
	MOUSE_BUTTONS_NUM
};


struct move_t
{
	bool front, back, left, right;
	bool accel, decel;
	float rot_x, rot_y;
};

#define IS_KEY_MSG( msg )		( WM_KEYFIRST <= ( msg ) && ( msg ) <= WM_KEYLAST )
#define IS_MOUSE_MSG( msg )		( WM_MOUSEFIRST <= ( msg ) && ( msg ) <= WM_MOUSELAST )
#define IS_INPUT_MSG( msg )		( IS_KEY_MSG( msg ) || IS_MOUSE_MSG( msg ) )

#define KEYS_NUM				( UCHAR_MAX + 1 )
#define BYTES_NUM( num )		( ( ( num - 1 ) >> 3 ) + 1 )


// Keys:
#define KEY_EXIT				VK_ESCAPE

#define KEY_SWITCH_MAIN_OBS		TCHAR( 'M' )
#define KEY_OBS_0				VK_F1
#define KEY_OBS_1				VK_F2
#define KEY_OBS_2				VK_F3
#define KEY_OBS_3				VK_F4
#define KEY_DEFAULTS			VK_F9
#define KEY_WND_MODE			VK_F12
#define KEY_DRAW_MODE			VK_F11

#define KEY_CHANGE_TYPE			TCHAR( 'T' )
#define KEY_ACCEL				TCHAR( 'A' )
#define KEY_DECEL				TCHAR( 'Z' )
#define KEY_MOVE_FRONT			VK_UP
#define KEY_MOVE_BACK			VK_DOWN
#define KEY_STRAFE_LEFT			VK_LEFT
#define KEY_STRAFE_RIGHT		VK_RIGHT
#define KEY_PITCH_UP			VK_HOME
#define KEY_PITCH_DOWN			VK_END
#define KEY_YAW_LEFT			VK_DELETE
#define KEY_YAW_RIGHT			VK_NEXT
#define KEY_ROLL_LEFT			VK_INSRET
#define KEY_ROLL_RIGHT			VK_PRIOR

#define KEY_TEX_ZOOM_IN			VK_OEM_PLUS
#define KEY_TEX_ZOOM_OUT		VK_OEM_MINUS
#define KEY_TEX_OFF_INC			TCHAR( 'A' )
#define KEY_TEX_OFF_DEC			TCHAR( 'Z' )
#define KEY_TEX_TOP				VK_HOME
#define KEY_TEX_BOTTOM			VK_END
#define KEY_TEX_LEFTMOST		VK_DELETE
#define KEY_TEX_RIGHTMOST		VK_NEXT
#define KEY_TEX_UP				VK_UP
#define KEY_TEX_DOWN			VK_DOWN
#define KEY_TEX_LEFT			VK_LEFT
#define KEY_TEX_RIGHT			VK_RIGHT

#define HBODY_MOVE				TCHAR( 'L' )
#define HBODY_MOVE_UP			VK_UP
#define HBODY_MOVE_DOWN			VK_DOWN
#define HBODY_MOVE_CCW			VK_LEFT
#define HBODY_MOVE_CW			VK_RIGHT


class CManip
{
protected:
	CTDArray< BYTE > field;

public:
	CManip() { Clear(); };
	virtual ~CManip() {};

	virtual void TranslateMessage( UINT msg, WPARAM wParam, LPARAM lParam ) = NULL;
	virtual void Process( move_t & move ) {};

	BYTE IsPressed( const unsigned index ) const { return ( field[ index ] ? TRUE : FALSE ); };
	void SetState( const unsigned index, const BYTE state ) { field[ index ] = state; };
	void Press( const unsigned index ) { SetState( index, TRUE ); };
	void Release( const unsigned index ) { SetState( index, FALSE ); };

	void Clear() { field.Reset( FALSE ); };
};

class CMouse : public CManip
{
protected:
	bool m_invert;
	float m_speed;

public:
	CMouse() : m_speed( 0.2f ), m_invert( true ) { field.Alloc( MOUSE_BUTTONS_NUM, false ); };
	virtual ~CMouse() {};

	void SetInvertMouse( const bool invert ) { m_invert = invert; };

	void TranslateMessage( UINT msg, WPARAM wParam, LPARAM lParam );
	void Process( move_t & move );
};


class CKeyBoard : public CManip
{
public:
	CKeyBoard() { field.Alloc( KEYS_NUM, false ); };
	virtual ~CKeyBoard() {};

	void TranslateMessage( UINT msg, WPARAM wParam, LPARAM lParam );
	void Process( move_t & move );

private:
	void KeyDownWhenDrawScenes( WPARAM key, LPARAM data );
	void KeyDownWhenDrawTexture( WPARAM key, LPARAM data );

	void ProcessWhenDrawScenes( move_t & move );
	void ProcessWhenDrawTexture();
};


class CInput : public CMouse, public CKeyBoard
{
public:
	CInput() {};
	~CInput() {};

	void Clear();
	void TranslateMessage( UINT msg, WPARAM wParam, LPARAM lParam );
	void Process();
};

#endif