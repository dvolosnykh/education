.386p


; ���������� ���� ��� GDT_&SEGM.
calc_base_seg	Macro	SEGM

	.errb	< SEGM >

	xor	EAX, EAX
	mov	AX, SEGM
	shl	EAX, 4
	mov	GDT_&SEGM&.base_i, AX
	shr	EAX, 8
	mov	GDT_&SEGM&.base_m, AH
EndM

; ���������� ���� ��� Table
table_address	Macro	Table

	.errb	< Table >

	mov	EAX, seg Table
	shl	EAX, 4
	add	AX, offset Table
	mov	Table&R.base, EAX
EndM


; ��������� �����������:
GDescr_t	struc
	limit	dw	?
	base_i	dw	?
	base_m	db	?
	attr1	db	?
	attr2	db	?
	base_h	db	?
GDescr_t	ends

; ��������� GDTR:
xDTR_t	struc
	limit	dw	?
	base	dd	?
xDTR_t	ends


SS_16	Segment		Stack 'STACK' use16
	dw	100h	dup ( ? )
SS_16_len	= 100h * 2
SS_16	EndS

SS_32	Segment		'STACK' use32
	dd	100h	dup ( ? )
SS_32_len	= 100h * 4
SS_32	EndS


DS_16	Segment		'DATA' use16

PE	= 00000001b
CMOS	= 70h
NMI	= 10000000b
A20	= 00000010b

msg_V86		db	"CPU is in V86 mode: unable to switch to PM.", 10, 13, '$'

DS_16	EndS


DS_32	Segment 	'DATA' use32

; �������� ��������:
rand	= 63

; ������� ���������� ������������:
GDT	label	byte
	NULL		GDescr_t	< 00000h, 0000h, 00h, 00000000b, 00000000b, 0 >	; ������� ����������.
	GDT_CS_16	GDescr_t	< 0FFFFh,     ?,   ?, 10011010b, 00000000b, 0 >
	GDT_DS_16	GDescr_t	< 0FFFFh,     ?,   ?, 10010010b, 00000000b, 0 >
	GDT_SS_16	GDescr_t	< 0FFFFh,     ?,   ?, 10010010b, 00000000b, 0 >
	GDT_CS_32	GDescr_t	< 0FFFFh,     ?,   ?, 10011010b, 01000000b, 0 >
	GDT_DS_32	GDescr_t	< 0FFFFh,     ?,   ?, 10010010b, 01000000b, 0 >
	GDT_SS_32	GDescr_t	< 0FFFFh,     ?,   ?, 10010010b, 01000000b, 0 >
	GDT_RAM_flat	GDescr_t	< 0FFFFh, 0000h, 00h, 10010010b, 11001111b, 0 >
	GDT_VM_16	GDescr_t	< 00FA0h, 8000h, 0Bh, 10010010b, 00000000b, 0 >
GDT_len	= $ - GDT

; ������� ���������� ������� ������������:
GDTR	xDTR_t	< GDT_len - 1, ? >

; ��������� ������������:
SEL_CS_16	= 0000000000001000b
SEL_DS_16	= 0000000000010000b
SEL_SS_16	= 0000000000011000b
SEL_CS_32	= 0000000000100000b
SEL_DS_32 	= 0000000000101000b
SEL_SS_32	= 0000000000110000b
SEL_RAM_flat	= 0000000000111000b
SEL_VM_16	= 0000000001000000b

; ���������:
msg		db	4 dup( ' ' ), " Mbytes of RAM."
msg_len		= $ - msg
rest_scr	= 80 * 25 - msg_len

DS_32	EndS


CS_16	Segment 	'CODE' use16
	Assume	CS:CS_16, DS:DS_16, SS:SS_16, ES:DS_32
main:
	mov	AX, DS_16
	mov	DS, AX

	mov	AX, DS_32
	mov	ES, AX

	; ���������, �� ������� �� ���������� �����.
	mov	EAX, CR0
	test	AL, PE
	jz	short no_V86
	jmp	in_V86
no_V86:

	; ��� ������ � 32-������ ������� ��������� ����� A20.
	in	AL, 92h
	or	AL, A20
	out	92h, AL

	calc_base_seg	CS_16
	calc_base_seg	DS_16
	calc_base_seg	SS_16
	calc_base_seg	CS_32
	calc_base_seg	DS_32
	calc_base_seg	SS_32

	; ���������� ��������� ������ GDT.
	table_address	GDT

	; ��������� GDT.
	lgdt	GDTR

	cli

	; ��������� ������������� ���������� (NMI).
	in	AL, CMOS
	or 	AL, NMI
	out	CMOS, AL

	; ������������� � ���������� �����.
	mov	EAX, CR0
	or	AL, PE
	mov	CR0, EAX

	; ��������� � CS 32-������ ��������.
	db	66h		; ������� ��������� ����������� ��������.
	db	0EAh		; ��� ������� �������� jmp.
	dd	offset PM_entry
	dw	SEL_CS_32

RM_entry:
	mov	AX, SEL_DS_16
	mov	DS, AX

	mov	AX, SEL_DS_32
	mov	ES, AX

	; ������������� � �������� �����.
	mov	EAX, CR0
	mov	BL, PE
	not	BL
	and	AL, BL
	mov	CR0, EAX

	; ��������� CS, DS, SS ��������� ����������� ��������.
	db	0EAh		; ��� ������� �������� jmp.
	dw	$ + 4
	dw	CS_16

	mov	AX, DS_16
	mov	DS, AX

	mov	AX, DS_32
	mov	ES, AX

	mov	AX, SS_16
	mov	SS, AX
	mov	SP, SS_16_len	; ������������ 16-������ ����.

	; ��������� ������������� ���������� (NMI).
	in	AL, CMOS
	mov	BL, NMI
	not	BL
	and	AL, BL
	out	CMOS, AL

	sti

	jmp	short quit

in_V86:
	mov	DX, offset msg_V86
	mov	AH, 9
	int	21h

quit:	mov	AH, 07h
	int	21h

	mov	AX, 4C00h
	int	21h
CS_16	EndS



CS_32	Segment 	'CODE' use32
	Assume	CS:CS_32, DS:DS_32, SS:SS_32
PM_entry:
	mov	AX, SEL_SS_32
	mov	SS, AX
	mov	ESP, SS_32_len	; ��������� 32-������� �����.

	; ������� RAM.
	mov	AX, SEL_RAM_flat
	mov	ES, AX

	mov	EDI, 100000h
	mov	ECX, 0FFFFFFFFh
	sub	ECX, ESI
	RAM_cycle:
		mov	AH, byte ptr ES:[ EDI ]
		mov	byte ptr ES:[ EDI ], rand
		mov	AL, byte ptr ES:[ EDI ]
		mov	byte ptr ES:[ EDI ], AH

		cmp	AL, rand
		jne	end_RAM_cycle

		inc	EDI
		loop	RAM_cycle
	end_RAM_cycle:

	; ���������� ������ � ����������.
	shr	EDI, 20

	; ������� ����� � ������ (���������� ������� ���������).
	mov	AX, SEL_DS_32
	mov	DS, AX

	mov	EDX, EDI
	test	EDX, EDX	; *** ���� ***
	jnz	non_zero
	mov	msg[ 0 ], '0'
	mov	msg[ 1 ], '$'
	jmp	output

non_zero:			; *** �� ���� ***
	xor	ESI, ESI
	mov	EAX, EDX
	mov	EBX, 10
	cycle:
		test	EAX, EAX
		jz	end_cycle

		xor	EDX, EDX
		div	EBX
		add	DL, '0'
		mov	msg[ ESI ], DL
		inc	ESI
		jmp	cycle
	end_cycle:

	xor	EDI, EDI
	reverse:
		dec	ESI

		cmp	ESI, EDI
		jna	end_reverse

		mov	AH, msg[ ESI ]
		xchg	AH, msg[ EDI ]
		mov	msg[ ESI ], AH

		inc	EDI
		jmp	reverse
	end_reverse:

output:
	; ����� �� �����.
	mov	AX, SEL_VM_16
	mov	ES, AX

	cld
	mov	ESI, offset msg
	xor	EDI, EDI
	mov	ECX, msg_len
	mov	AH, 07h
	cycle_output:
		lodsb
		stosw
	loop	cycle_output

	mov	AL, ' '		; ������� ���������� ����� ������.
	mov	AH, 07h
	mov	ECX, rest_scr
	rep	stosw

	; ��������� � CS �������� 16-������� �������� RM_Seg.
	db	66h		; ������� ��������� ����������� ��������.
	db	0EAh		; ��� ������� �������� jmp.
	dw	offset RM_entry
	dw	SEL_CS_16

CS_32	EndS


END	main