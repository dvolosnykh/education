#pragma once


#define MAX_VALUE	100


class CInfo
{
public:
	unsigned wait;		// Number of waiting.
	unsigned active;	// Number of active.
	HANDLE event;		// Wait on this.

	CInfo() : active( 0 ), wait( 0 ) {};
	~CInfo() {};

	void Activate( unsigned count );
};


class CSWMRG
{
public:
	unsigned value;	// stored value of the resource.
private:
	HANDLE mutex;	// Permits exclusive access to other members.
	CInfo readers;	// Info about readers.
	CInfo writers;	// Info about writers.

public:
	CSWMRG();
	~CSWMRG();

	void StartReading();	// Try to gain shared read-access.
	void StartWriting();	// Try to gain exclusive write-access.
	void StopReading();		// Done read-access.
	void StopWriting();		// Done write-access.
	bool IsFree() const { return ( readers.active == 0 && writers.active == 0 ); };

private:
	void Distribute();		// Distribute resource.
	bool ReadingAllowed() const { return ( writers.wait == 0 && writers.active == 0 ); };
	bool WritingAllowed() const { return IsFree(); };
};