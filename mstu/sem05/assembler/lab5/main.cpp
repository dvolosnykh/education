#include <time.h>
#include <tchar.h>
#include <process.h>
#include <windows.h>
#include <windowsx.h>

#include "Resource.h"

#include "SWMRG.h"


#define THREADS_NUM		16
#define RESOURCES_NUM	2


CSWMRG resource[ RESOURCES_NUM ];

HWND hwndW;
HWND hwndR;


unsigned __stdcall WriterThread( void * param )
{
	const unsigned thread_num = HIWORD( param );
	const unsigned res_num = LOWORD( param );

	resource[ res_num ].StartWriting();

		srand( ( unsigned )time( NULL ) );
		resource[ res_num ].value = rand() % MAX_VALUE;

		TCHAR sz[ 1024 ];
		wsprintf( sz, TEXT( "Writer %d: writing %d to %d" ),
			thread_num, resource[ res_num ].value, res_num );

		ListBox_SetCurSel( hwndW, ListBox_AddString( hwndW, sz ) );

		Sleep( 3000 );

	resource[ res_num ].StopWriting();

	return ( 0 );
}

unsigned __stdcall ReaderThread( void * param )
{
	const unsigned thread_num = HIWORD( param );
	const unsigned res_num = LOWORD( param );

	resource[ res_num ].StartReading();

		TCHAR sz[ 1024 ];
		wsprintf( sz, TEXT( "Reader %d: reading %d from %d" ), 
			thread_num, resource[ res_num ].value, res_num );

		ListBox_SetCurSel( hwndR, ListBox_AddString( hwndR, sz ) );

		Sleep( 3000 );

	resource[ res_num ].StopReading();

	return ( 0 );
}


BOOL Dlg_OnInitDialog( HWND hwnd, HWND, LPARAM )
{
	hwndW = GetDlgItem( hwnd, IDC_WRITERS );
	hwndR = GetDlgItem( hwnd, IDC_READERS );

	HINSTANCE hInst = ( HINSTANCE )GetWindowLongPtr( hwnd, GWLP_HINSTANCE );
	LPARAM lParam = ( LPARAM )LoadIcon( hInst, MAKEINTRESOURCE( IDI_QUEUE ) );

	SendMessage( hwnd, WM_SETICON, TRUE, lParam );
	SendMessage( hwnd, WM_SETICON, FALSE, lParam );

	srand( ( unsigned )time( NULL ) );

	// Create threads.
	unsigned i;
	for ( i = 0; i < THREADS_NUM; i++ )
	{
		const unsigned thread_type = rand() % 2;
		void * param = ( void * )MAKELONG( rand() % RESOURCES_NUM, i );

		HANDLE thread = ( HANDLE )_beginthreadex( NULL, 0,
			thread_type == 0 ? WriterThread : ReaderThread, param, 0, NULL );

		CloseHandle( thread );
	}

	return ( TRUE );
}

BOOL Dlg_OnCommand( HWND hwnd, int id, HWND, UINT )
{
	BOOL result;

	switch ( id )
	{
	case IDCANCEL:
		result = EndDialog( hwnd, id );
		break;

	default:
		result = FALSE;
		break;
	}

	return ( result );
}

int __stdcall Dlg_Proc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	int result;

	switch ( msg )
	{
	case WM_INITDIALOG:
		result = SetDlgMsgResult( hwnd, msg,
			HANDLE_WM_INITDIALOG( hwnd, wParam, lParam, Dlg_OnInitDialog ) );
		break;

	case WM_COMMAND:
		result = SetDlgMsgResult( hwnd, msg,
			HANDLE_WM_COMMAND( hwnd, wParam, lParam, Dlg_OnCommand ) );
		break;

	default:
		result = 0;
		break;
	}

	return ( result );
}


int __stdcall _tWinMain( HINSTANCE hinstExe, HINSTANCE, PTSTR, int )
{
	DialogBox( hinstExe, MAKEINTRESOURCE( IDD_QUEUE ), NULL, Dlg_Proc );

	return ( 0 );
}