#include <assert.h>
#include <windows.h>
#include <time.h>

#include "SWMRG.h"


void CInfo::Activate( unsigned count )
{
	assert( count <= wait );

	active += count;
	wait -= count;
}


CSWMRG::CSWMRG()
{
	srand( ( unsigned )time( NULL ) );
	value = rand() % MAX_VALUE;

	readers.event = CreateEvent( NULL, TRUE, TRUE, NULL );
	writers.event = CreateEvent( NULL, FALSE, TRUE, NULL );
	mutex = CreateMutex( NULL, FALSE, NULL );
}

CSWMRG::~CSWMRG()
{
	assert( IsFree() );

	CloseHandle( mutex );
	CloseHandle( writers.event );
	CloseHandle( readers.event );
}


void CSWMRG::StartReading()
{
	WaitForSingleObject( mutex, INFINITE );

		const bool can_read = ReadingAllowed();
		( can_read ? readers.active : readers.wait )++;

	ReleaseMutex( mutex );

	if ( !can_read )
		WaitForSingleObject( readers.event, INFINITE );

//	ResetEvent( readers.event );
	ResetEvent( writers.event );
}

void CSWMRG::StartWriting()
{
	WaitForSingleObject( mutex, INFINITE );

		const bool can_write = WritingAllowed();
		( can_write ? writers.active : writers.wait )++;

	ReleaseMutex( mutex );

	if ( !can_write )
		WaitForSingleObject( writers.event, INFINITE );
	else
		ResetEvent( writers.event );

	ResetEvent( readers.event );
}

void CSWMRG::StopReading()
{
	WaitForSingleObject( mutex, INFINITE );

		assert( readers.active > 0 );
		readers.active--;

		if ( IsFree() )
			Distribute();

	ReleaseMutex( mutex );
}

void CSWMRG::StopWriting()
{
	WaitForSingleObject( mutex, INFINITE );

		assert( writers.active > 0 );
		writers.active--;

		if ( IsFree() )
			Distribute();

	ReleaseMutex( mutex );
}

void CSWMRG::Distribute()
{
	if ( writers.wait > 0 )
	{
		assert( writers.active == 0 );

		// Allow writing for the only one waiting writer.
		writers.Activate( 1 );
		SetEvent( writers.event );
	}
	else if ( readers.wait > 0 )
	{
		assert( readers.active == 0 );

		// Allow reading for all the waiting readers.
		readers.Activate( readers.wait );
		SetEvent( readers.event );
	}
}