#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>

#define NUM	20

int pressed;

void int_handler( int sig_num )
{
	pressed = 1;
}

int main()
{
	int pf_d[ 2 ];
	char buf[] = "Hello!";

	pressed = 0;
	signal( SIGINT, int_handler );
	pause();

	if ( pipe( pf_d ) == -1 )
	{
		perror( "can't pipe" );
		exit( 1 );
	}

	int childPID = fork();

	if ( childPID == -1 )
	{
		perror( "can't fork" );
		exit( 1 );
	}
	else if ( childPID == 0 )
	{
		close( pf_d[ 1 ] );
		if ( read( pf_d[ 0 ], buf, sizeof( buf ) ) > 0 );
			printf( buf );

		exit( 0 );
	}
	else
	{
		if ( pressed )
		{
			close( pf_d[ 0 ] );
			write( pf_d[ 1 ], buf, sizeof( buf ) );
		}

		exit( 0 );
	}

	return ( 0 );
}
