#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>

int main()
{
	int childPID = fork();

	if ( childPID == -1 )
	{
		perror( "can't fork" );
		exit( 1 );
	}
	else if ( childPID == 0 )
	{
		sleep( 1 );

		printf( "CHILD: OwnPID = %d, ParentPID = %d, GroupPID = %d\n",
			getpid(), getppid(), getgid() );
		exit( 0 );
	}
	else
	{
		int status;
		wait( &status );

		printf( "PARENT: OwnPID = %d, ParentPID = %d, GroupPID = %d\n",
			getpid(), getppid(), getgid() );
		exit( 0 );
	}

	return ( 0 );
}
