#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

int main()
{
	int childPID = fork();

	if ( childPID == -1 )
	{
		perror( "can't fork" );
		exit( 1 );
	}
	else if ( childPID == 0 )
	{
		if ( execl( "/bin/ls", "ls", "-l", 0 ) < 0 )
		{
			perror( "can't exec" );
			exit( 1 );
		}
		else
		{
			printf( "CHILD: OwnPID = %d, ParentPID = %d, GroupPID = %d\n",
				getpid(), getppid(), getgid() );
			exit( 0 );
		}
	}
	else
	{
		printf( "PARENT: OwnPID = %d, ParentPID = %d, GroupPID = %d\n",
			getpid(), getppid(), getgid() );
		exit( 0 );
	}

	return ( 0 );
}
