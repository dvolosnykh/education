.386p


; ���������� ���� ��� GDT_&SEGM.
calc_base_seg	Macro	SEGM

	.errb	< SEGM >

	xor	EAX, EAX
	mov	AX, SEGM
	shl	EAX, 4
	mov	GDT_&SEGM&.base_i, AX
	shr	EAX, 8
	mov	GDT_&SEGM&.base_m, AH
EndM

; ���������� ��������� ������ Table
table_address	Macro	Table

	.errb	< Table >

	mov	EAX, seg Table
	shl	EAX, 4
	add	AX, offset Table
	mov	Table&R.base, EAX
EndM


; ��������� ����������� ���������� �������:
GDescr_t	struc
	limit		dw	?
	base_i		dw	?
	base_m		db	?
	attr1		db	?
	attr2		db	?
	base_h		db	?
GDescr_t	ends

; ��������� ����������� ������� ����������:
IDescr_t	struc
	offset_l	dw	?
	selector	dw	?
	stack_size	db	?
	access		db	?
	offset_h	dw	?
IDescr_t	ends

; ��������� ��������� ������:
xDTR_t	struc
	limit	dw	?
	base	dd	?
xDTR_t	ends

; ��������� �������:
symbol_t	struc
	char	db	?
	attrs	db	?
symbol_t	ends

; ��������� �������:
cursor_t	struc
	pos	label	word
	pos_l	db	?
	pos_h	db	?
	attrc	db	?
cursor_t	ends


; 16-������ ����.
SS_16	Segment		Stack 'STACK' use16
	dw	100h	dup ( ? )
SS_16_len	= 100h * 2
SS_16	EndS


; 32-������ ����.
SS_32	Segment		'STACK' use32
	dd	100h	dup ( ? )
SS_32_len	= 100h * 4
SS_32	EndS


; 16-������ ������� ������.
DS_16	Segment		'DATA' use16

PE	= 00000001b
CMOS	= 70h
NMI	= 10000000b
A20	= 00000010b
EOI	= 20h

msg_V86		db	"CPU is in V86 mode: unable to switch to PM.", 10, 13, '$'

DS_16	EndS


; 32-������ ������� ������.
DS_32	Segment 	'DATA' use32

; ������� ���������� ������������:
GDT	label	byte
	NULL		GDescr_t	< 00000h, 0000h, 00h, 00000000b, 00000000b, 0 >	; ������� ����������.
	GDT_CS_16	GDescr_t	< 0FFFFh,     ?,   ?, 10011010b, 00000000b, 0 >
	GDT_DS_16	GDescr_t	< 0FFFFh,     ?,   ?, 10010010b, 00000000b, 0 >
	GDT_SS_16	GDescr_t	< 0FFFFh,     ?,   ?, 10010010b, 00000000b, 0 >
	GDT_CS_32	GDescr_t	< 0FFFFh,     ?,   ?, 10011010b, 01000000b, 0 >
	GDT_DS_32	GDescr_t	< 0FFFFh,     ?,   ?, 10010010b, 01000000b, 0 >
	GDT_SS_32	GDescr_t	< 0FFFFh,     ?,   ?, 10010010b, 01000000b, 0 >
	GDT_VM_16	GDescr_t	< 00FA0h, 8000h, 0Bh, 10010010b, 00000000b, 0 >
GDT_len	= $ - GDT

; ������� ���������� ������� ������������:
GDTR		xDTR_t	< GDT_len - 1, ? >

; ��������� ������������:
SEL_CS_16	= 0000000000001000b
SEL_DS_16	= 0000000000010000b
SEL_SS_16	= 0000000000011000b
SEL_CS_32	= 0000000000100000b
SEL_DS_32 	= 0000000000101000b
SEL_SS_32	= 0000000000110000b
SEL_VM_16	= 0000000000111000b

; ������� ������������ ����������:
IDT	label	byte
	IDescr_t	8   dup( < small offset int_handler,     SEL_CS_32, 0, 8Eh, 0 > )
	IDescr_t		 < small offset irq0_7_handler,  SEL_CS_32, 0, 8Eh, 0 >
	IDescr_t		 < small offset irq1_handler,    SEL_CS_32, 0, 8Eh, 0 >
	IDescr_t	6   dup( < small offset irq0_7_handler,  SEL_CS_32, 0, 8Eh, 0 > )
	IDescr_t	97  dup( < small offset int_handler,     SEL_CS_32, 0, 8Eh, 0 > )
	IDescr_t	8   dup( < small offset irq8_15_handler, SEL_CS_32, 0, 8Eh, 0 > )
	IDescr_t	135 dup( < small offset int_handler,     SEL_CS_32, 0, 8Eh, 0 > )
IDT_len = $ - IDT

; �������� ������� ������������ ����������:
IDTR		xDTR_t	< IDT_len - 1, ? >
IDTR_save	xDTR_t	< ?, ? >

; ������� �������������� scan-����� � ASCII-�������:

;	��� ������� ��������:
table_l		db	0, 0, '1234567890-=', 0, 0, 'qwertyuiop[]'
		db	0, 0, "asdfghjkl;'`", 0, '\zxcvbnm,./'
		db	0, '*', 0, ' ', 14 dup( 0 ), '-', 0, '-'
		db	0, '5', 0, '+', 177 dup( 0 )

;	��� �������� ��������:
table_h		db	0, 0, '!@#$%^&*()_+', 0, 0, 'QWERTYUIOP{}'
		db	0, 0, 'ASDFGHJKL:"~', 0, '|ZXCVBNM<>?'
		db	0, '*', 0, ' ', 14 dup( 0 ), '-', 0, '-'
		db	0, '5', 0, '+', 177 dup( 0 )

MAXX		= 80
MAXY		= 25
SCR_SIZE	= MAXY * MAXX

ESCAPE		= 01h
LSHIFT		= 2Ah
RSHIFT		= 36h
DOWN		= 00h
UP		= 80h

RETURN		= 1Ch
BACKSPACE	= 0Eh

ARROW_LEFT	= 4Bh
ARROW_RIGHT	= 4Dh
ARROW_UP	= 48h
ARROW_DOWN	= 50h

CRT		= 03D4h
CUR_POS_L	= 0Fh
CUR_POS_H	= 0Eh

escape	db	0
shift	db	0
symbol		symbol_t	< ?, 00000111b >
cursor		cursor_t	< 0, 0, 10001111b >

DS_32	EndS


CS_16	Segment 	'CODE' use16
	Assume	CS:CS_16, DS:DS_16, SS:SS_16, ES:DS_32
main:
	mov	AX, DS_16
	mov	DS, AX

	mov	AX, DS_32
	mov	ES, AX

	; ���������, �� ������� �� ���������� �����.
	mov	EAX, CR0
	test	AL, PE
	jz	no_V86
	jmp	in_V86
no_V86:

	; ��� ������ � 32-������ ������� ��������� ����� A20.
	in	AL, 92h
	or	AL, A20
	out	92h, AL

	calc_base_seg	CS_16
	calc_base_seg	DS_16
	calc_base_seg	SS_16
	calc_base_seg	CS_32
	calc_base_seg	DS_32
	calc_base_seg	SS_32

	table_address	GDT
	lgdt	GDTR		; ��������� GDT.

	table_address	IDT

	cli

	; ��������� ������������� ���������� (NMI).
	in	AL, CMOS
	or 	AL, NMI
	out	CMOS, AL

	sidt	IDTR_save	; ���������� ������� IDTR.
	lidt	IDTR		; ��������� IDT.

	; ������������� � ���������� �����.
	mov	EAX, CR0
	or	AL, PE
	mov	CR0, EAX

	; ��������� � CS 32-������ ��������.
	db	66h		; ������� ��������� ����������� ��������.
	db	0EAh		; ��� ������� �������� jmp.
	dd	large offset PM_entry
	dw	SEL_CS_32

RM_entry:
	mov	AX, SEL_DS_16
	mov	DS, AX

	mov	AX, SEL_DS_32
	mov	ES, AX

	; ������������� � �������� �����.
	mov	EAX, CR0
	mov	BL, PE
	not	BL
	and	AL, BL
	mov	CR0, EAX

	; ��������� CS, DS, SS ��������� ����������� ��������.
	db	0EAh		; ��� ������� �������� jmp.
	dw	$ + 4
	dw	CS_16

	mov	AX, DS_16
	mov	DS, AX

	mov	AX, DS_32
	mov	ES, AX

	mov	AX, SS_16
	mov	SS, AX
	mov	SP, SS_16_len	; ������������ 16-������ ����.

	lidt	IDTR_save

	; ��������� ������������� ���������� (NMI).
	in	AL, CMOS
	mov	BL, NMI
	not	BL
	and	AL, BL
	out	CMOS, AL

	sti

	jmp	short quit

in_V86:
	mov	DX, offset msg_V86
	mov	AH, 09h
	int	21h

	mov	AH, 07h
	int	21h
quit:
	mov	AX, 4C00h
	int	21h

CS_16	EndS


CS_32	Segment 	'CODE' use32
	Assume	CS:CS_32, DS:DS_32, SS:SS_32
PM_entry:
	mov	AX, SEL_DS_32
	mov	DS, AX

	mov	AX, SEL_SS_32
	mov	SS, AX
	mov	ESP, SS_32_len	; ��������� 32-������� �����.

	call	clrscr32
	call	mov_cur

	sti

	jmp	short $		; �������� ���.

PM_exit:
	cli

	; ��������� � CS �������� 16-������� �������� RM_Seg.
	db	66h		; ������� ��������� ����������� ��������.
	db	0EAh		; ��� ������� �������� jmp.
	dw	small offset RM_entry
	dw	SEL_CS_16

; ���������� �������� ����������.
int_handler:
iretd

; ���������� ���������� ���������� IRQ0, IRQ2 - IRQ7.
irq0_7_handler:
	push	EAX

	mov	AL, EOI
	out	20h, AL

	pop	EAX
iretd

; ���������� ���������� ���������� IRQ8 - IRQ15.
irq8_15_handler:
	push	EAX

	mov	AL, EOI
	out	0A1h, AL

	pop	EAX
iretd

; ���������� ����������� ���������� IRQ1 - �� ����������.
irq1_handler:
	push	EAX
	push	EBX
	push	EDX
	push	EDI
	push	ES

	mov	AX, SEL_VM_16
	mov	ES, AX

	in	AL, 60h		; ������ ����-��� � ����� ����������.

	case_ESCAPE:
		cmp	AL, ESCAPE
		jne	case_LSHIFT_DOWN

		mov	escape, 1

		jmp	end_case

	case_LSHIFT_DOWN:
		cmp	AL, LSHIFT + DOWN
		jne	case_LSHIFT_UP

		inc	shift

		jmp	end_case

	case_LSHIFT_UP:
		cmp	AL, LSHIFT + UP
		jne	case_RSHIFT_DOWN

		dec	shift

		jmp	end_case

	case_RSHIFT_DOWN:
		cmp	AL, RSHIFT + DOWN
		jne	case_RSHIFT_UP

		inc	shift

		jmp	end_case

	case_RSHIFT_UP:
		cmp	AL, RSHIFT + UP
		jne	case_ARROW_LEFT

		dec	shift

		jmp	end_case

	case_ARROW_LEFT:
		cmp	AL, ARROW_LEFT
		jne	case_ARROW_RIGHT

		dec	cursor.pos

		jmp	move_cursor

	case_ARROW_RIGHT:
		cmp	AL, ARROW_RIGHT
		jne	case_ARROW_UP

		inc	cursor.pos

		jmp	move_cursor

	case_ARROW_UP:
		cmp	AL, ARROW_UP
		jne	case_ARROW_DOWN

		sub	cursor.pos, MAXX

		jmp	move_cursor

	case_ARROW_DOWN:
		cmp	AL, ARROW_DOWN
		jne	case_RETURN

		add	cursor.pos, MAXX

		jmp	move_cursor

	case_RETURN:
		cmp	AL, RETURN
		jne	case_BACKSPACE

		xor	EDX, EDX
		mov	EAX, EDX
		mov	AX, cursor.pos
		mov	EBX, MAXX
		div	EBX

		sub	cursor.pos, DX
		add	cursor.pos, BX

		jmp	move_cursor

	case_BACKSPACE:
		cmp	AL, BACKSPACE
		jne	case_symbol

		dec	cursor.pos
		xor	EDI, EDI
		mov	DI, cursor.pos
		shl	EDI, 1

		mov	symbol.char, ' '
		mov	AX, word ptr symbol
		stosw

		jmp	move_cursor

	case_symbol:
		mov	AH, shift
		test	AH, AH
		jz	lower_case
			mov	EBX, offset table_h
			jmp	short exchange
		lower_case:
			mov	EBX, offset table_l
		exchange:

		xlat

		test	AL, AL
		jz	end_case

		xor	EDI, EDI
		mov	DI, cursor.pos
		shl	EDI, 1

		mov	symbol.char, AL
		mov	AX, word ptr symbol
		stosw

		inc	cursor.pos
	move_cursor:

		; ������������� ��������� �������: ������������� ������.
		cmp	cursor.pos, 0
		jnl	skip_add
			add	cursor.pos, SCR_SIZE
		skip_add:

		cmp	cursor.pos, SCR_SIZE - 1
		jng	skip_sub
			sub	cursor.pos, SCR_SIZE
		skip_sub:

		call	mov_cur
	end_case:

	in	AL, 61h		; ���������� ������ ����������.
	or	AL, 80h
	out	61h, AL

	mov	AL, EOI
	out	20h, AL

	pop	ES
	pop	EDI
	pop	EDX
	pop	EBX

	mov	AH, escape
	test	AH, AH
	jz	no_escape
		call	clrscr32
		pop	EAX

		jmp	PM_exit
	no_escape:

	pop	EAX
iretd

; ������� ������.
clrscr32	Proc	Near

	mov	AX, SEL_VM_16
	mov	ES, AX

	xor	EDI, EDI

	cld
	mov	AL, ' '
	mov	AH, symbol.attrs
	mov	ECX, MAXY * MAXX
	rep	stosw

	RetN

clrscr32	EndP

; ����������� �������
mov_cur		Proc	Near

	mov	DX, CRT

	mov	AL, CUR_POS_L
	mov	AH, cursor.pos_l
	out	DX, AX

	mov	AL, CUR_POS_H
	mov	AH, cursor.pos_h
	out	DX, AX

	RetN

mov_cur		EndP

CS_32	EndS


END	main