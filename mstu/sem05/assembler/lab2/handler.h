#ifndef HANDLER_H
#define HANDLER_H

	#ifdef __cplusplus
		#define __CPPARGS ...
	#else
		#define __CPPARGS
	#endif

	#define KEYBOARD	0x09
	#define TIMER		0x1C
	#define	DISK		0x13
	#define KEYBSERV	0x16
	#define	QUANTUM		0x28
	#define MULTIPLEX	0x2F
	#define DOS			0x21

	#define DOS_ENV		0x2C

	typedef	void interrupt handler_t( __CPPARGS );

	extern handler_t * old_int09;
	extern handler_t * old_int1C;
	extern handler_t * old_int13;
	extern handler_t * old_int28;
	extern handler_t * old_int2F;

	extern unsigned save_psp;

	void interrupt new_int09( __CPPARGS );
	void interrupt new_int1C( __CPPARGS );
	void interrupt new_int13( __CPPARGS );
	void interrupt new_int28( __CPPARGS );
	void interrupt new_int2F( __CPPARGS );

#endif