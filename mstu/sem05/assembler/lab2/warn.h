#ifndef	WARN_H
#define WARN_H

	extern char far * p_indos;
	extern char far * p_error;

	char far * get_indos();
	char far * get_error();

#endif