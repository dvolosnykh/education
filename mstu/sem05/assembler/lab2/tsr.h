#ifndef TSR_H
#define TSR_H

	#define MUX 0x80

	#define LOADED		0xFF
	#define NOT_LOADED	0x00
	#define CANT_LOAD	0x01

	#define	UNLOAD_SUCCESS	0x00
	#define UNLOAD_CANCEL	0x01
	#define UNLOAD_FAILED	0x02

	unsigned char load_state();
	void load();
	unsigned char try_unload();

#endif