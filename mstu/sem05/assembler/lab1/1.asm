include	utils.asm


SSeg	Segment		Stack 'STACK'
	db	100h dup( ? )
SSeg	EndS


DSeg	Segment		'DATA'

int08h	dd	?

DSeg	EndS


CSeg	Segment		'CODE'
	Assume	SS:SSeg, CS:CSeg, DS:DSeg

Extrn	unsigned_hexadecimal:	Near

main:
	mov	AX, DSeg
	mov	DS, AX

	mov	AX, 3508h
	int	21h
	mov	word ptr int08h, BX
	mov	word ptr int08h + 2, ES

	call	dword ptr int08h

;	mov	AX, 0
;	mov	DS, AX
;
;	mov	BX, 0020h
;	mov	AX, [ BX + 2 ]
;	push	AX
;	call	unsigned_hexadecimal
;	add	SP, 2
;
;	putch	':'
;
;	mov	AX, [ BX ]
;	push	AX
;	call	unsigned_hexadecimal
;	add	SP, 2

	getch
	halt	0

CSeg	EndS


END	main