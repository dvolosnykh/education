#include <mem.h>
#include <math.h>

#include "convert.h"
#include "keyboard.h"
#include "mathutil.h"


static void transform( vector_t & v, const matrix_t & m )
{
	vector_t temp = { 0, 0, 0, 0 };

	for ( unsigned i = 0; i < 4; i++ )
		for ( unsigned j = 0; j < 4; j++ )
			temp[ i ] += v[ j ] * m[ j ][ i ];

	memmove( v, temp, sizeof( vector_t ) );
}

point2d_t screen_dot( const point3d_t & dot, const view_t & view )
{
	vector_t dot_temp = { dot.x, dot.y, dot.z, 1 };

	transform( dot_temp, view.matrix );

	const point2d_t dot_scr =
	{
		dot_temp[ 0 ] + get_center( view ).x,
		dot_temp[ 1 ] + get_center( view ).y
	};

	return ( dot_scr );
}

void convert( view_t & view, const unsigned key )
{
	matrix_t & matrix = view.matrix;
	matrix_t transform;

	switch ( key )
	{
	case OX_PLUS:	case OX_MINUS:
		{
		double phi = ( key == OX_PLUS ? rot_angle : -rot_angle );
#ifndef ROT_RADIAN
		phi = degree_to_rad( phi );
#endif
		const double c = cos( phi );
		const double s = sin( phi );

		set( transform, 1,  0,  0,  0,
						0,  c,  s,  0,
						0, -s,  c,  0,
						0,  0,  0,  1 );
		}

		break;

	case OY_PLUS:	case OY_MINUS:
		{
		double phi = ( key == OY_PLUS ? rot_angle : -rot_angle );
#ifndef ROT_RADIAN
		phi = degree_to_rad( phi );
#endif
		const double c = cos( phi );
		const double s = sin( phi );

		set( transform, c,  0, -s,  0,
						0,  1,  0,  0,
						s,  0,  c,  0,
						0,  0,  0,  1 );
		}

		break;

	case OZ_PLUS:	case OZ_MINUS:
		{
		double phi = ( key == OZ_PLUS ? rot_angle : -rot_angle );
#ifndef ROT_RADIAN
		phi = degree_to_rad( phi );
#endif
		const double c = cos( phi );
		const double s = sin( phi );

		set( transform,  c,  s,  0,  0,
						-s,  c,  0,  0,
						 0,  0,  1,  0,
						 0,  0,  0,  1 );
		}

		break;
	}

	matrix_mult( matrix, transform );
}

point3d_t & get_center( view_t & view )
{
	return ( view.center );
}

matrix_t & get_matrix( view_t & view )
{
	return ( view.matrix );
}