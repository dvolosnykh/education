#ifndef INIT_H
#define INIT_H

	#include "convert.h"
	#include "list.h"

	int initialize( view_t & view, list_t & shp_list );

#endif