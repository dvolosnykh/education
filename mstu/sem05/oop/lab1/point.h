#ifndef POINT_H
#define POINT_H

	struct point3d_t
	{
		double x, y, z;
	};

	struct point2d_t
	{
		double x, y;
	};

#endif