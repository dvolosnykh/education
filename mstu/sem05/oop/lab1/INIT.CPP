#include "init.h"
#include "mathutil.h"
#include "convert.h"
#include "point.h"
#include "graph.h"
#include "list.h"


int initialize( view_t & view, list_t & shp_list )
{
	int OK = gr_initgraph( VGA, VGAHI, "" );

	if ( OK )
	{
		matrix_t & matrix = view.matrix;
		set( matrix, 1, 0, 0, 0,
					 0, 1, 0, 0,
					 0, 0, 1, 0,
					 0, 0, 0, 1 );

		point3d_t & c = view.center;
		c.x = gr_getmaxx() / 2;
		c.y = gr_getmaxy() / 2;
		c.z = 0;

		init_list( shp_list );
	}

	return ( OK );
}