#ifndef BAR3D_H
#define BAR3D_H

	#include "point.h"
	#include "convert.h"

	struct bar3d_t
	{
		point3d_t vertex[ 8 ];
	};

	void draw_bar3d( const bar3d_t & bar3d, const view_t & view );

#endif