#ifndef GRAPH_H
#define GRAPH_H

	#include <graphics.h>

	int gr_initgraph( int gDriver, int gMode, char * path );
	void gr_closegraph();
	int gr_getmaxx();
	int gr_getmaxy();
	int gr_getcolor();
	void gr_setcolor( int color );
	void gr_clrscr();
	void gr_moveto( int x, int y );
	void gr_lineto( int x, int y );

#endif