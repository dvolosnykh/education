#include "final.h"
#include "graph.h"
#include "list.h"


void finalize( list_t & shp_list )
{
	clear_list( shp_list );
	gr_closegraph();
}