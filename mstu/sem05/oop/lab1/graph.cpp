#include "graph.h"


int gr_initgraph( int gDriver, int gMode, char * path )
{
	initgraph( &gDriver, &gMode, path );
	return ( graphresult() == grOk );
}

void gr_closegraph()
{
	closegraph();
}

int gr_getmaxx()
{
	return getmaxx();
}

int gr_getmaxy()
{
	return getmaxy();
}

int gr_getcolor()
{
	return getcolor();
}

void gr_setcolor( int color )
{
	setcolor( color );
}

void gr_clrscr()
{
	cleardevice();
}

void gr_moveto( int x, int y )
{
	moveto( x, y );
}

void gr_lineto( int x, int y )
{
	lineto( x, y );
}