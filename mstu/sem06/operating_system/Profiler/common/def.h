#pragma once



#define MAX_PROCESS_NAME_LEN	260



typedef struct _PROCESS
{
	ULONG ThreadCount;
	LARGE_INTEGER CreateTime;
	LARGE_INTEGER UserTime;
	LARGE_INTEGER KernelTime;
	TCHAR ProcessName[ MAX_PROCESS_NAME_LEN + 1 ];
	LONG BasePriority;
	ULONG ProcessId;
	ULONG ParentId;
} PROCESS, *PPROCESS;


typedef struct _THREAD
{
	LARGE_INTEGER KernelTime;
	LARGE_INTEGER UserTime;
	LARGE_INTEGER CreateTime;
	ULONG WaitTime;
	ULONG ThreadId;
	LONG Priority;
	LONG BasePriority;
	ULONG ContextSwitchCount;
} THREAD, *PTHREAD;