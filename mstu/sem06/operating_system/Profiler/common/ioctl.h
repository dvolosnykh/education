#pragma once



#define FILE_DEVICE_USER		0x8000
#define IOCTL_USER				0x800


#define FILE_DEVICE_SPECTATOR	FILE_DEVICE_USER + 0


#define IOCTL_CODE( _code_ )	CTL_CODE	\
(											\
	FILE_DEVICE_SPECTATOR,					\
	IOCTL_USER + _code_,					\
	METHOD_BUFFERED,						\
	FILE_ANY_ACCESS							\
)


#define IOCTL_LOCK_INFO				IOCTL_CODE(  0 )
#define IOCTL_UNLOCK_INFO			IOCTL_CODE(  1 )
#define IOCTL_LOAD_INFO				IOCTL_CODE(  2 )
#define IOCTL_FREE_INFO				IOCTL_CODE(  3 )
#define IOCTL_PROCESS_FIRST			IOCTL_CODE(  4 )
#define IOCTL_PROCESS_NEXT			IOCTL_CODE(  5 )
#define IOCTL_THREAD_FIRST			IOCTL_CODE(  6 )
#define IOCTL_THREAD_NEXT			IOCTL_CODE(  7 )
#define IOCTL_OPEN_THREAD			IOCTL_CODE(  8 )
#define IOCTL_CLOSE_THREAD			IOCTL_CODE(  9 )
#define IOCTL_GET_THREAD_CONTEXT	IOCTL_CODE( 10 )
#define IOCTL_LAST_CLIENT			IOCTL_CODE( 11 )