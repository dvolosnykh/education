#include "stdafx.h"

#include "FuncAddr.h"
#include "SysInfo.h"
#include "PEfile.h"



AddrZwSuspendThread		ZwSuspendThread		= NULL;
AddrZwResumeThread		ZwResumeThread		= NULL;
//AddrZwGetContextThread	ZwGetContextThread	= NULL;



PVOID FindModuleAddr( PCSTR targetName )
{
	BEGIN_FUNC( FindModuleAddr );

	PVOID targetAddr = NULL;
	PSYSTEM_MODULES_INFORMATION pInfo = NULL;
	NTSTATUS status = STATUS_SUCCESS;
	ULONG requiredSpace = 0;
	BOOLEAN ok = TRUE;

	if ( ok )
	{
		status = ZwQuerySystemInformation( SystemModuleInformation, NULL, 0, &requiredSpace );
		ok = NT_SUCCESS( status ) || status == STATUS_INFO_LENGTH_MISMATCH;
		if ( ok )
			DBG_REPORT_RETVAL( ZwQuerySystemInformation, %u, requiredSpace );
		else
			DBG_REPORT_FAILURE( ZwQuerySystemInformation, status );
	}

	if ( ok )
	{
		status = _ExAllocatePool( pInfo, PagedPool, requiredSpace );
		ok = NT_SUCCESS( status );
	}

	if ( ok )
	{
		status = ZwQuerySystemInformation( SystemModuleInformation, pInfo, requiredSpace, NULL );
		if ( ok = NT_SUCCESS( status ) )
			DBG_REPORT_RETVAL( ZwQuerySystemInformation, %u, pInfo->Count );
		else
			DBG_REPORT_FAILURE( ZwQuerySystemInformation, status );
	}

	if ( ok )
	{
		for ( ULONG i = 0; targetAddr == NULL && i < pInfo->Count; i++ )
		{
			PCSTR moduleName = pInfo->module[ i ].ImageName + pInfo->module[ i ].ModuleNameOffset;
			if ( _stricmp( moduleName, targetName ) == 0 )
				targetAddr = ( PVOID )pInfo->module[ i ].Base;
		}

		_ExFreePool( pInfo );
	}

	DBG_PRINT(( PREFIX "%s = 0x%08X.", targetName, targetAddr ));

	END_FUNC( FindModuleAddr );
	return ( targetAddr );
}



PVOID FindFuncAddr( PVOID moduleAddr, PCSTR targetName )
{
	BEGIN_FUNC( FindFuncAddr );

	PUCHAR baseAddr = PUCHAR( moduleAddr );

	IMAGE_DOS_HEADER dosHeader = *( PIMAGE_DOS_HEADER )( baseAddr );
	IMAGE_NT_HEADERS ntHeaders = *( PIMAGE_NT_HEADERS )( baseAddr + dosHeader.e_lfanew );
	IMAGE_DATA_DIRECTORY exportDirEntry = ntHeaders.OptionalHeader.DataDirectory[ IMAGE_DIRECTORY_ENTRY_EXPORT ];
	ULONG dirMinAddr = exportDirEntry.VirtualAddress;
	ULONG dirMaxAddr = dirMinAddr + exportDirEntry.Size;
	IMAGE_EXPORT_DIRECTORY exportDir = *( PIMAGE_EXPORT_DIRECTORY )( baseAddr + dirMinAddr );
	PULONG functions = PULONG( baseAddr + exportDir.AddressOfFunctions );
	PULONG nameOffsets = PULONG( baseAddr + exportDir.AddressOfNames );
	PSHORT ordinals = PSHORT( baseAddr + exportDir.AddressOfNameOrdinals );

	PVOID targetAddr = NULL;
	for ( ULONG i = 0; targetAddr == NULL && i < exportDir.NumberOfNames; i++ )
	{
		ULONG ord = ordinals[ i ];

//		if ( functions[ ord ] < dirMinAddr || functions[ ord ] >= dirMaxAddr )
//		{
			PCSTR funcName = PSTR( baseAddr + nameOffsets[ i ] );
			if ( strcmp( funcName, targetName ) == 0 )
				targetAddr = baseAddr + functions[ ord ];
//		}
	}

	DBG_PRINT(( PREFIX "%s = 0x%08X.", targetName, targetAddr ));

	END_FUNC( FindFuncAddr );
	return ( targetAddr );
}



BOOLEAN TuneFuncAddresses()
{
	BEGIN_FUNC( TuneFuncAddresses );

	BOOLEAN ok = TRUE;
	PVOID ntdllAddr = NULL;

	if ( ok )
	{
		ntdllAddr = FindModuleAddr( "ntdll.dll" );
		ok = ntdllAddr != NULL;
	}

	if ( ok )
	{
		ZwSuspendThread = ( AddrZwSuspendThread )FindFuncAddr( ntdllAddr, "ZwSuspendThread" );
		ok = ZwSuspendThread != NULL;
	}

	if ( ok )
	{
		ZwResumeThread = ( AddrZwResumeThread )FindFuncAddr( ntdllAddr, "ZwResumeThread" );
		ok = ZwResumeThread != NULL;
	}

//	if ( ok )
//	{
//		ZwGetContextThread = ( AddrZwGetContextThread )FindFuncAddr( ntdllAddr, "ZwGetContextThread" );
//		ok = ZwGetContextThread != NULL;
//	}

	END_FUNC( TuneFuncAddresses );
	return ( ok );
}