//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Profiler.rc
//
#define IDC_MYICON                      2
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDI_PROFILER                    107
#define IDI_SMALL                       108
#define IDC_PROFILER                    109
#define IDD_PROFILER_DIALOG             129
#define IDC_PROCESSES_LIST              1001
#define IDC_PROCESS_INFO                1002
#define IDC_THREADS_LIST                1003
#define IDC_THREAD_INFO                 1004
#define IDC_THREAD_CONTEXT              1005
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
