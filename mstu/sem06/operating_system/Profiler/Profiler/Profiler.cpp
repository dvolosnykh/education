#include "stdafx.h"

#include "Dialog.h"



int APIENTRY _tWinMain( HINSTANCE hInstance, HINSTANCE, LPTSTR lpCmdLine, int nCmdShow )
{
	return CDialog::DoModal( hInstance );
}