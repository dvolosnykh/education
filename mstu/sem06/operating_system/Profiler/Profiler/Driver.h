#pragma once

#include "def.h"



#define DRIVER_NAME		_T( "Spectator" )
#define DISPLAY_NAME	_T( "Spectator Profiler" )
#define BINARY_NAME		_T( "Spectator.sys" )



class CDriver
{
private:
	enum
	{
		DRIVER_NAME_MAX_LEN = 15,
		DISPLAY_NAME_MAX_LEN = 100,
		BINARY_PATH_MAX_LEN = MAX_PATH
	};

	BOOL _exists, _running, _opened;
	HANDLE _hFile;

	TCHAR _szDriverName[ DRIVER_NAME_MAX_LEN + 1 ];
	TCHAR _szDisplayName[ DISPLAY_NAME_MAX_LEN + 1 ];
	TCHAR _szBinaryPath[ BINARY_PATH_MAX_LEN + 1 ];

public:
	CDriver() { Init(); };
	~CDriver() { Deinit(); };

	void Init();
	void Deinit();
	void SetDriver( LPCTSTR szDriverName, LPCTSTR szDisplayName, LPCTSTR szBinaryName );

	BOOL IsLastClient();

	BOOL LockInfo();
	void UnlockInfo();

	BOOL ProcessFirst( PROCESS & process );
	BOOL ProcessNext( PROCESS & process );
	BOOL ProcessFind( PROCESS & process, DWORD pid );

	BOOL ThreadFirst( THREAD & thread );
	BOOL ThreadNext( THREAD & thread );
	BOOL ThreadFind( THREAD & thread, DWORD tid );

	BOOL OpenThread( HANDLE * phThread, DWORD dwDesiredAccess, DWORD tid );
	BOOL CloseThread( HANDLE hThread );
	BOOL GetThreadContext( CONTEXT & context, HANDLE hThread );

	BOOL Start();
	BOOL Stop();

private:
	BOOL CreateService( SC_HANDLE hScm );
	BOOL DeleteService( SC_HANDLE hScm );
	BOOL StartService( SC_HANDLE hScm );
	BOOL StopService( SC_HANDLE hScm );
	BOOL OpenFile();
	BOOL CloseFile();
	DWORD Status( SC_HANDLE hScm );
	void ClearStrings();
};