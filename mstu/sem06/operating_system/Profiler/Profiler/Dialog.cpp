#include "stdafx.h"
#include <stdio.h>

#include "resource.h"
#include "Dialog.h"



HWND CDialog::_hdlg = NULL;
HINSTANCE CDialog::_hInst = NULL;
HFONT CDialog::_hFont = NULL;

HANDLE CDialog::_hTimer = NULL;

CDriver CDialog::_spectator;
DWORD CDialog::_pid = DWORD( -1 );
DWORD CDialog::_tid = DWORD( -1 );
DWORD CDialog::_updatePeriod = 100;



int APIENTRY CDialog::DoModal( HINSTANCE hInstance )
{
	return ( int )DialogBox( hInstance, ( LPCTSTR )IDD_PROFILER_DIALOG,
		NULL, ( DLGPROC )DlgProc );
}



VOID CALLBACK CDialog::OnTimer( PVOID, BOOLEAN )
{
	ShowInfo();
}



BOOL CALLBACK CDialog::DlgProc( HWND hdlg, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch ( msg )
	{
		HANDLE_MSG( hdlg, WM_INITDIALOG,	OnInitDialog );
		HANDLE_MSG( hdlg, WM_DESTROY,		OnDestroy );
		HANDLE_MSG( hdlg, WM_COMMAND,		OnCommand );
		default:	return ( FALSE );
	}
}



BOOL CDialog::OnInitDialog( HWND hdlg, HWND hwndFocus, LPARAM lParam )
{
	_hdlg = hdlg;

	BOOL ok = TRUE;

	// create font:
	if ( ok )
	{
		_hFont = CreateFont( 14, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, RUSSIAN_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DRAFT_QUALITY, FIXED_PITCH | FF_MODERN, NULL /*_T( "Courier New" )*/ );
		SetWindowFont( GetDlgItem( _hdlg, IDC_PROCESSES_LIST ), _hFont, FALSE );
		SetWindowFont( GetDlgItem( _hdlg, IDC_PROCESS_INFO ), _hFont, FALSE );
		SetWindowFont( GetDlgItem( _hdlg, IDC_THREADS_LIST ), _hFont, FALSE );
		SetWindowFont( GetDlgItem( _hdlg, IDC_THREAD_INFO ), _hFont, FALSE );
		SetWindowFont( GetDlgItem( _hdlg, IDC_THREAD_CONTEXT ), _hFont, FALSE );
	}

	// start driver:
	if ( ok )
	{
		_spectator.SetDriver( DRIVER_NAME, DISPLAY_NAME, BINARY_NAME );
		while ( ok && !_spectator.Start() )
		{
			const int answer = QUERY( _T( "��������� �������?" ), _T( "������� �� ��� ����������." ) );
			ok = answer == IDYES;
		}
	}

	// refresh processes and threads lists:
	if ( ok )
		RefreshProcessList();

	if ( ok )
	{
		ok = CreateTimerQueueTimer
		(
			&_hTimer,
			NULL,
			OnTimer,
			NULL,
			0,
			_updatePeriod,
			0
		);
	}

	// close dialog if something was wrong:
	if ( !ok )
		EndDialog( hdlg, FALSE );

	return ( ok );
}



void CDialog::OnDestroy( HWND hdlg )
{
	// stop and delete timer:
	DeleteTimerQueueTimer( NULL, _hTimer, NULL );

	// unload driver:
	if ( _spectator.IsLastClient() )
		_spectator.Stop();

	// delete font:
	if ( _hFont != NULL )
		DeleteFont( _hFont );
}



void CDialog::OnCommand( HWND hdlg, int id, HWND hwndCtl, UINT codeNotify )
{
	switch ( id )
	{
	case IDC_PROCESSES_LIST:
		switch ( codeNotify )
		{
			case CBN_DROPDOWN:
			{
				RefreshProcessList();
			}
			break;

			case CBN_SELCHANGE:
			{
				_pid = SelectedProcessId();
				RefreshThreadList( _pid );
			}
			break;
		}
		break;

	case IDC_THREADS_LIST:
		switch ( codeNotify )
		{
			case CBN_DROPDOWN:
			{
				RefreshThreadList( _pid );
			}
			break;

			case CBN_SELCHANGE:
			{
				_tid = SelectedThreadId();
			}
			break;
		}
		break;

	case IDCANCEL:
		DestroyWindow( hdlg );
		break;
	}
}



void CDialog::RefreshProcessList()
{
	if ( _spectator.LockInfo() )
	{
		HWND hwndProcesses = GetDlgItem( _hdlg, IDC_PROCESSES_LIST );

		SetWindowRedraw( hwndProcesses, FALSE );
		ComboBox_ResetContent( hwndProcesses );

		PROCESS process;
		for ( BOOL ok = _spectator.ProcessFirst( process );
			ok;
			ok = _spectator.ProcessNext( process ) )
		{
			LPCTSTR pszProcessName = _tcsrchr( process.ProcessName, TCHAR( '\\' ) );
			if ( pszProcessName == NULL )
				pszProcessName = process.ProcessName;
			else
				pszProcessName++;

			TCHAR sz[ 1024 ];
			wsprintf( sz, _T( "%-20s (PID: 0x%08X)" ), pszProcessName, process.ProcessId );
			const int index = ComboBox_AddString( hwndProcesses, sz );
			ComboBox_SetItemData( hwndProcesses, index, process.ProcessId );

			if ( process.ProcessId == _pid )
				ComboBox_SetCurSel( hwndProcesses, index );
		}

		_spectator.UnlockInfo();

		if ( ComboBox_GetCurSel( hwndProcesses ) == CB_ERR )
			ComboBox_SetCurSel( hwndProcesses, 0 );

		SetWindowRedraw( hwndProcesses, TRUE );
		InvalidateRect( hwndProcesses, NULL, FALSE );

		FORWARD_WM_COMMAND( _hdlg, IDC_PROCESSES_LIST, hwndProcesses, CBN_SELCHANGE, PostMessage );
	}
}



void CDialog::RefreshThreadList( DWORD pid )
{
	if ( _spectator.LockInfo() )
	{
		PROCESS process;
		if ( _spectator.ProcessFind( process, pid ) )
		{
			HWND hwndThreads = GetDlgItem( _hdlg, IDC_THREADS_LIST );

			SetWindowRedraw( hwndThreads, FALSE );
			ComboBox_ResetContent( hwndThreads );

			THREAD thread;
			for ( BOOL ok = _spectator.ThreadFirst( thread );
				ok;
				ok = _spectator.ThreadNext( thread ) )
			{
				TCHAR sz[ 1024 ];
				wsprintf( sz, _T( "TID: 0x%08X      (���������: %4u)" ), thread.ThreadId, thread.BasePriority );
				const int index = ComboBox_AddString( hwndThreads, sz );
				ComboBox_SetItemData( hwndThreads, index, thread.ThreadId );

				if ( thread.ThreadId == _tid )
					ComboBox_SetCurSel( hwndThreads, index );
			}

			_spectator.UnlockInfo();

			if ( ComboBox_GetCurSel( hwndThreads ) == CB_ERR )
				ComboBox_SetCurSel( hwndThreads, 0 );

			SetWindowRedraw( hwndThreads, TRUE );
			InvalidateRect( hwndThreads, NULL, FALSE );

			FORWARD_WM_COMMAND
			(
				_hdlg,
				IDC_THREADS_LIST,
				hwndThreads,
				CBN_SELCHANGE,
				PostMessage
			);
		}
	}
}



BOOL CDialog::ShowInfo()
{
	BOOL ok = TRUE;
	BOOL locked = FALSE;

	// Get process info:
	PROCESS process;
	if ( locked = _spectator.LockInfo() )
	{
		ok = _spectator.ProcessFind( process, _pid );
		_spectator.UnlockInfo();
	}

	HWND hwndProcessInfo = GetDlgItem( _hdlg, IDC_PROCESS_INFO );

	if ( !ok )
	{
		SetText( hwndProcessInfo, _T( "������� �� ������." ) );
	}
	else if ( locked )
	{
		SetText
		(
			hwndProcessInfo,
			_T( "ProcessID        = 0x%08X\r\n" )
			_T( "ParentPID        = 0x%08X\r\n" )
			_T( "BasePriority     = %10u\r\n" )
			_T( "ThreadCount      = %10u\r\n" )
			_T( "KernelTime       = %10u\r\n" )
			_T( "UserTime         = %10u" ),
			process.ProcessId,
			process.ParentId,
			process.BasePriority,
			process.ThreadCount,
			process.KernelTime,
			process.UserTime
		);
	}
	else
	{
		SetText( hwndProcessInfo, _T( "������� ��� �������� ������." ) );
	}

	InvalidateRect( hwndProcessInfo, NULL, TRUE );

	// Get thread info:
	THREAD thread;
	if ( ok )
	{
		if ( locked = _spectator.LockInfo() )
		{
			ok = _spectator.ThreadFind( thread, _tid );
			_spectator.UnlockInfo();
		}
	}

	HWND hwndThreadInfo = GetDlgItem( _hdlg, IDC_THREAD_INFO );

	if ( !ok )
	{
		SetText( hwndThreadInfo, _T( "����� �� ������." ) );
	}
	else if ( locked )
	{
		SetText
		(
			hwndThreadInfo,
			_T( "ThreadID         = 0x%08X\r\n" )
			_T( "BasePriority     = %10u\r\n" )
			_T( "Priority         = %10u\r\n" )
			_T( "ContextSwitches  = %10u\r\n" )
			_T( "KernelTime       = %10u\r\n" )
			_T( "UserTime         = %10u\r\n" )
			_T( "WaitTime         = %10u" ),
			thread.ThreadId,
			thread.BasePriority,
			thread.Priority,
			thread.ContextSwitchCount,
			thread.KernelTime,
			thread.UserTime,
			thread.WaitTime
		);
	}
	else
	{
		SetText( hwndProcessInfo, _T( "������� ��� �������� ������." ) );
	}

	InvalidateRect( hwndThreadInfo, NULL, TRUE );

	// Get thread context:
	HWND hwndThreadContext = GetDlgItem( _hdlg, IDC_THREAD_CONTEXT );
	if ( ok )
	{
		CONTEXT context;
		HANDLE hThread = NULL;

		if ( ok )
		{
			ok = _spectator.OpenThread
			(
				&hThread,
				THREAD_GET_CONTEXT | THREAD_SUSPEND_RESUME,
				_tid
			);

			ok = ok && hThread != NULL;
			if ( !ok )
				SetText( hwndThreadContext, _T( "������ ��� ��������� ����������� ������." ) );
		}

		if ( ok )
		{
			DWORD suspendCount = SuspendThread( hThread );
			ok = suspendCount != DWORD( -1 );
			if ( !ok )
				SetText( hwndThreadContext, _T( "������ ��� ��������� ������ ��� ��������� ���������." ) );
		}

		if ( ok )
		{
			context.ContextFlags = CONTEXT_FULL;
			ok = _spectator.GetThreadContext( context, hThread );
			if ( !ok )
				SetText( hwndThreadContext, _T( "������ ��� ��������� ���������." ) );
		}

		if ( ok )
		{
			DWORD suspendCount = ResumeThread( hThread );
			ok = suspendCount != DWORD( -1 );
			if ( !ok )
				SetText( hwndThreadContext, _T( "������ ��� ������������� ������ ����� ��������� ���������." ) );
		}

		if ( ok )
		{
			ok = _spectator.CloseThread( hThread );
			if ( !ok )
				SetText( hwndThreadContext, _T( "������ ��� ������� ���������� ���������� ������." ) );
		}
		
		if ( ok )
		{
			SetText
			(
				hwndThreadContext,
				_T( "CS\t= 0x%08X\r\n" )
				_T( "DS\t= 0x%08X\r\n" )
				_T( "SS\t= 0x%08X\r\n" )
				_T( "ES\t= 0x%08X\r\n" )
				_T( "FS\t= 0x%08X\r\n" )
				_T( "GS\t= 0x%08X\r\n" )
				_T( "GS\t= 0x%08X\r\n" )
				_T( "EAX\t= 0x%08X\r\n" )
				_T( "EBX\t= 0x%08X\r\n" )
				_T( "ECX\t= 0x%08X\r\n" )
				_T( "EDX\t= 0x%08X\r\n" )
				_T( "EDI\t= 0x%08X\r\n" )
				_T( "ESI\t= 0x%08X\r\n" )
				_T( "EBP\t= 0x%08X\r\n" )
				_T( "EIP\t= 0x%08X\r\n" )
				_T( "ESP\t= 0x%08X\r\n" )
				_T( "EFLAGS\t= 0x%08X" ),
				context.SegCs,
				context.SegDs,
				context.SegSs,
				context.SegEs,
				context.SegFs,
				context.SegGs,
				context.SegGs,
				context.Eax,
				context.Ebx,
				context.Ecx,
				context.Edx,
				context.Edi,
				context.Esi,
				context.Ebp,
				context.Eip,
				context.Esp,
				context.EFlags
			);
		}
	}
	else
	{
		SetText( hwndThreadContext, _T( "����� �� ������." ) );
	}

	InvalidateRect( hwndThreadContext, NULL, TRUE );

	return ( ok );
}



DWORD CDialog::SelectedProcessId()
{
	HWND hwndCtl = GetDlgItem( _hdlg, IDC_PROCESSES_LIST );
	int index = ComboBox_GetCurSel( hwndCtl );
	return ( DWORD )( index == CB_ERR ? -1 : ComboBox_GetItemData( hwndCtl, index ) );
}



DWORD CDialog::SelectedThreadId()
{
	HWND hwndCtl = GetDlgItem( _hdlg, IDC_THREADS_LIST );
	int index = ComboBox_GetCurSel( hwndCtl );
	return ( DWORD )( index == CB_ERR ? -1 : ComboBox_GetItemData( hwndCtl, index ) );
}



void CDialog::SetText( HWND hwnd, PCTSTR format, ... )
{
	va_list argList;
	va_start( argList, format );

	const int len = _vsctprintf( format, argList );
	LPTSTR psz = new TCHAR [ len + 1 ];
	_vstprintf( psz, format, argList );
	SetWindowText( hwnd, psz );
	delete psz;

	va_end( argList );
}