#include "stdafx.h"
#include "Driver.h"
#include "ioctl.h"
#include <stdio.h>


void CDriver::Init()
{
	_exists = FALSE;
	_running = FALSE;
}



void CDriver::Deinit()
{
	if ( ( _running || _exists ) && IsLastClient() )
		Stop();
}



void CDriver::SetDriver( LPCTSTR szDriverName, LPCTSTR szDisplayName, LPCTSTR szBinaryName )
{
	Deinit();

	_tcsncpy( _szDriverName, szDriverName, DRIVER_NAME_MAX_LEN );
	_szDriverName[ DRIVER_NAME_MAX_LEN ] = TCHAR( '\0' );

	_tcsncpy( _szDisplayName, szDisplayName, DISPLAY_NAME_MAX_LEN );
	_szDisplayName[ DISPLAY_NAME_MAX_LEN ] = TCHAR( '\0' );

	::GetCurrentDirectory( BINARY_PATH_MAX_LEN, _szBinaryPath );
	const size_t len = _tcslen( _szBinaryPath );
	_szBinaryPath[ len ] = TCHAR( '\\' );
	_szBinaryPath[ len + 1 ] = TCHAR( '\0' );
	_tcscat( _szBinaryPath, szBinaryName );
	_szBinaryPath[ BINARY_PATH_MAX_LEN ] = TCHAR( '\0' );
}



BOOL CDriver::IsLastClient()
{
	DWORD bytesReturned;	// unused, but compulsory.

	BOOL ok = DeviceIoControl
	(
		_hFile,
		IOCTL_LAST_CLIENT,
		NULL, 0,
		NULL, 0,
		&bytesReturned, NULL
	);

	return ( ok );
}


BOOL CDriver::LockInfo()
{
	DWORD bytesReturned;	// unused, but compulsory.

	BOOL ok = DeviceIoControl
	(
		_hFile,
		IOCTL_LOCK_INFO,
		NULL, 0,
		NULL, 0,
		&bytesReturned, NULL
	);

	return ( ok );
}



void CDriver::UnlockInfo()
{
	DWORD bytesReturned;	// unused, but compulsory.

	BOOL ok = DeviceIoControl
	(
		_hFile,
		IOCTL_UNLOCK_INFO,
		NULL, 0,
		NULL, 0,
		&bytesReturned, NULL
	);
}



BOOL CDriver::ProcessFirst( PROCESS & process )
{
	DWORD bytesReturned;

	BOOL ok = DeviceIoControl
	(
		_hFile,
		IOCTL_PROCESS_FIRST,
		NULL, 0,
		&process, sizeof( process ),
		&bytesReturned, NULL
	);

	if ( !ok )
	{
		REPORT
		(
			_T( "�� ������� �������� ���������� � ��������." ),
			_T( "������ ��� ��������� � ��������." )
		);
	}

	return ( ok && bytesReturned > 0 );
}



BOOL CDriver::ProcessNext( PROCESS & process )
{
	DWORD bytesReturned;

	BOOL ok = DeviceIoControl
	(
		_hFile,
		IOCTL_PROCESS_NEXT,
		NULL, 0,
		&process, sizeof( process ),
		&bytesReturned, NULL
	);

	if ( !ok )
	{
		REPORT
		(
			_T( "�� ������� �������� ���������� � ��������." ),
			_T( "������ ��� ��������� � ��������." )
		);
	}

	return ( ok && bytesReturned > 0 );
}



BOOL CDriver::ProcessFind( PROCESS & process, DWORD pid )
{
	BOOL ok;
	for ( ok = ProcessFirst( process );
		ok && process.ProcessId != pid;
		ok = ProcessNext( process ) );

	return ( ok && process.ProcessId == pid );
}



BOOL CDriver::ThreadFirst( THREAD & thread )
{
	DWORD bytesReturned;

	BOOL ok = DeviceIoControl
	(
		_hFile,
		IOCTL_THREAD_FIRST,
		NULL, 0,
		&thread, sizeof( thread ),
		&bytesReturned, NULL
	);

	if ( !ok )
	{
		REPORT
		(
			_T( "�� ������� �������� ���������� � ������." ),
			_T( "������ ��� ��������� � ��������." )
		);
	}

	return ( ok && bytesReturned > 0 );
}



BOOL CDriver::ThreadNext( THREAD & thread )
{
	DWORD bytesReturned;

	BOOL ok = DeviceIoControl
	(
		_hFile,
		IOCTL_THREAD_NEXT,
		NULL, 0,
		&thread, sizeof( thread ),
		&bytesReturned, NULL
	);

	if ( !ok )
	{
		REPORT
		(
			_T( "�� ������� �������� ���������� � ������." ),
			_T( "������ ��� ��������� � ��������." )
		);
	}

	return ( ok && bytesReturned > 0 );
}



BOOL CDriver::ThreadFind( THREAD & thread, DWORD tid )
{
	BOOL ok;
	for ( ok = ThreadFirst( thread );
		ok && thread.ThreadId != tid;
		ok = ThreadNext( thread ) );

	return ( ok && thread.ThreadId == tid );
}



BOOL CDriver::OpenThread( HANDLE * phThread, DWORD dwDesiredAccess, DWORD tid )
{
	DWORD bytesReturned;	// unused, but compulsory.

	DWORD param[] = { dwDesiredAccess, tid };
	BOOL ok = DeviceIoControl
	(
		_hFile,
		IOCTL_OPEN_THREAD,
		param, sizeof( param ),
		phThread, sizeof( *phThread ),
		&bytesReturned, NULL
	);

	return ( ok );
}



BOOL CDriver::CloseThread( HANDLE hThread )
{
	DWORD bytesReturned;	// unused, but compulsory.

	BOOL ok = DeviceIoControl
	(
		_hFile,
		IOCTL_CLOSE_THREAD,
		&hThread, sizeof( hThread ),
		NULL, 0,
		&bytesReturned, NULL
	);

	return ( ok );
}



BOOL CDriver::GetThreadContext( CONTEXT & context, HANDLE hThread )
{
	DWORD bytesReturned;

	BYTE buffer[ sizeof( context ) + sizeof( hThread ) ];
	memcpy( buffer, &context, sizeof( context ) );
	memcpy( buffer + sizeof( context ), &hThread, sizeof( hThread ) );

	BOOL ok = DeviceIoControl
	(
		_hFile,
		IOCTL_GET_THREAD_CONTEXT,
		buffer, sizeof( buffer ),
		&context, sizeof( context ),
		&bytesReturned, NULL
	);

	return ( ok && bytesReturned > 0 );
}



BOOL CDriver::Start()
{
	BOOL ok = TRUE;

	SC_HANDLE hScm = ::OpenSCManager
	(
		NULL,
		NULL,
		SC_MANAGER_ALL_ACCESS
	);

	if ( hScm == NULL )
	{
		REPORT
		(
			_T( "������ ��� ������� �������� ������." ),
			_T( "������ SCManager'�." )
		);
		ok = FALSE;
	}
	else if ( !CreateService( hScm ) )
	{
		::CloseServiceHandle( hScm );
		ok = FALSE;
	}
	else if ( !StartService( hScm ) )
	{
		::CloseServiceHandle( hScm );
		ok = FALSE;
	}
	else if ( !OpenFile() )
	{
		::CloseServiceHandle( hScm );
		ok = FALSE;
	}

	return ( ok );
}



BOOL CDriver::Stop()
{
	BOOL ok = TRUE;

	SC_HANDLE hScm = ::OpenSCManager
	(
		NULL,
		NULL,
		SC_MANAGER_ALL_ACCESS
	);

	if ( hScm == NULL )
	{
		REPORT
		(
			_T( "������ ��� ������� �������� ������." ),
			_T( "������ SCManager'�." )
		);
		ok = FALSE;
	}
	else if ( !CloseFile() )
	{
		::CloseServiceHandle( hScm );
		ok = FALSE;
	}
	else if( !StopService( hScm ) )
	{
		::CloseServiceHandle( hScm );
		ok = FALSE;
	}
	else if ( !DeleteService( hScm ) )
	{
		::CloseServiceHandle( hScm );
		ok = FALSE;
	}

	return ( ok );
}



BOOL CDriver::CreateService( SC_HANDLE hScm )
{
	if ( !_exists )
	{
		SC_HANDLE hService = ::CreateService
		(
			hScm,
			_szDriverName,
			_szDisplayName,
			SERVICE_ALL_ACCESS,
			SERVICE_KERNEL_DRIVER,
			SERVICE_DEMAND_START,
			SERVICE_ERROR_NORMAL,
			_szBinaryPath,
			NULL, NULL, NULL, NULL, NULL
		);

		if ( _exists = hService != NULL )
			::CloseServiceHandle( hService );
	}

	if ( !_exists )
	{
		DWORD error = GetLastError();
		switch ( error )
		{
		case ERROR_SERVICE_EXISTS:
			_exists = TRUE;
			break;

			default:
			{
				TCHAR text[ 1024 ];
				_stprintf( text, _T( "��� ������: %u." ), error );
				REPORT( text, _T( "������ �� ������." ) );
			}
			break;
		}
	}

	return ( _exists );
}



BOOL CDriver::DeleteService( SC_HANDLE hScm )
{
	if ( _exists )
	{
		SC_HANDLE hService = ::OpenService
		(
			hScm,
			_szDriverName,
			SERVICE_ALL_ACCESS
		);

		if ( hService != NULL )
		{
			_exists = !::DeleteService( hService );

			::CloseServiceHandle( hService );
		}
	}

	if ( _exists )
	{
		DWORD error = GetLastError();
		switch ( error )
		{
			default:
			{
				TCHAR text[ 1024 ];
				_stprintf( text, _T( "��� ������: %u." ), error );
				REPORT( text, _T( "������ �� �����." ) );
			}
			break;
		}
	}

	return ( !_exists );
}



BOOL CDriver::StartService( SC_HANDLE hScm )
{
	if ( !_running )
	{
		SC_HANDLE hService = ::OpenService
		(
			hScm,
			_szDriverName,
			SERVICE_ALL_ACCESS
		);

		if ( hService != NULL )
		{
			_running = ::StartService( hService, 0, NULL );

			::CloseServiceHandle( hService );
		}
	}

	if ( !_running )
	{
		DWORD error = GetLastError();
		switch ( error )
		{
			case ERROR_SERVICE_ALREADY_RUNNING:
				_running = TRUE;
				break;

			case ERROR_FILE_NOT_FOUND:
			{
				LPCTSTR intro = _T
				(
					"�������: ���� ������� �� ������.\n"
					"�������������� ���� � �����:n\n"
				);

				size_t len = _tcslen( intro ) + _tcslen( _szBinaryPath );
				LPTSTR text = new TCHAR [ len + 1 ];

				_tcscpy( text, intro );
				_tcscat( text, _szBinaryPath );
				REPORT( text, _T( "������ �� �������." ) );

				delete text;
			}
			break;

			default:
			{
				TCHAR text[ 1024 ];
				_stprintf( text, _T( "��� ������: %u." ), error );
				REPORT( text, _T( "������ �� �������." ) );
			}
			break;
		}
	}

	return ( _running );
}



BOOL CDriver::StopService( SC_HANDLE hScm )
{
	if ( _running )
	{
		SC_HANDLE hService = ::OpenService
		(
			hScm,
			_szDriverName,
			SERVICE_ALL_ACCESS
		);

		if ( hService != NULL )
		{
			SERVICE_STATUS serviceStatus;	// unused, but compulsory.
			_running = !::ControlService
			(
				hService,
				SERVICE_CONTROL_STOP,
				&serviceStatus
			);

			::CloseServiceHandle( hService );
		}
	}

	if ( _running )
	{
		DWORD error = GetLastError();
		switch ( error )
		{
//			case ERROR_SERVICE_NOT_ACTIVE:
//				break;

			default:
			{
				TCHAR text[ 1024 ];
				_stprintf( text, _T( "��� ������: %u." ), error );
				REPORT( text, _T( "������ �� ����������." ) );
			}
			break;
		}
	}

	return ( !_running );
}

BOOL CDriver::OpenFile()
{
	_hFile = ::CreateFile
	(
		_T( "\\\\.\\Spectator" ),
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);

	_opened = _hFile != INVALID_HANDLE_VALUE;
	if ( !_opened )
	{
		REPORT
		(
			_T( "������ ��� ������� �������� ������." ),
			_T( "������ ��������." )
		);
	}

	return ( _opened );
}

BOOL CDriver::CloseFile()
{
	if ( _opened )
		_opened = !::CloseHandle( _hFile );

	return ( !_opened );
}



DWORD CDriver::Status( SC_HANDLE hScm )
{
	SC_HANDLE hService = ::OpenService
	(
		hScm,
		_szDriverName,
		SERVICE_ALL_ACCESS
	);

	SERVICE_STATUS serviceStatus;
	BOOL ok = QueryServiceStatus( hService, &serviceStatus );

#ifdef _DEBUG
	DWORD error = GetLastError();
#endif

	::CloseServiceHandle( hService );

	return ( serviceStatus.dwCurrentState );
}


void CDriver::ClearStrings()
{
	_szDriverName[ 0 ] = TCHAR( '\0' );
	_szDisplayName[ 0 ] = TCHAR( '\0' );
	_szBinaryPath[ 0 ] = TCHAR( '\0' );
}