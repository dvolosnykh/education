#pragma once

#include "Driver.h"



class CDialog
{
public:
	static HWND _hdlg;
	static HINSTANCE _hInst;
	static HFONT _hFont;

	static HANDLE _hTimer;

	static CDriver _spectator;
	static DWORD _pid;
	static DWORD _tid;
	static DWORD _updatePeriod;		// ms

public:
	static int APIENTRY DoModal( HINSTANCE hInstance );

private:
	static VOID CALLBACK OnTimer( PVOID, BOOLEAN );

	static BOOL CALLBACK DlgProc( HWND hdlg, UINT message, WPARAM wParam, LPARAM lParam );
	static BOOL OnInitDialog( HWND hdlg, HWND hwndFocus, LPARAM lParam );
	static void OnDestroy( HWND hdlg );
	static void OnCommand( HWND hdlg, int id, HWND hwndCtl, UINT codeNotify );

	static void RefreshProcessList();
	static void RefreshThreadList( DWORD pid );

	static BOOL ShowInfo();

	static DWORD SelectedProcessId();
	static DWORD SelectedThreadId();

	static void SetText( HWND hwnd, PCTSTR pszFormat, ... );
};