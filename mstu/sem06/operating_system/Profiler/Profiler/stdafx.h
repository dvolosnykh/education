#pragma once


#define _WIN32_WINNT	0x0500


#define WIN32_LEAN_AND_MEAN


#include <windows.h>
#include <windowsx.h>
#include <tchar.h>
#include <winioctl.h>


#pragma warning( disable: 4065 )


#define REPORT( _text_, _caption_ )	MessageBox( NULL, _text_, _caption_, MB_OK | MB_ICONERROR | MB_TASKMODAL )
#define QUERY( _text_, _caption_ )	MessageBox( NULL, _text_, _caption_, MB_YESNO | MB_ICONQUESTION | MB_TASKMODAL )