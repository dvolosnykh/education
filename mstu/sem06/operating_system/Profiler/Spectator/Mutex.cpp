#include "stdafx.h"

#include "Mutex.h"



#ifdef ALLOC_PRAGMA
#pragma alloc_text( "PAGE", LockMutex )
#pragma alloc_text( "PAGE", UnlockMutex )
#pragma alloc_text( "PAGE", CreateMutex )
#pragma alloc_text( "PAGE", DestroyMutex )
#endif



static PKMUTEX g_pMutex = NULL;



NTSTATUS LockMutex( PLARGE_INTEGER pTimeout /* = NULL */ )
{
	return KeWaitForMutexObject
	(
		g_pMutex,
		Executive,
		KernelMode,
		FALSE,
		pTimeout
	);
}



void UnlockMutex()
{
	KeReleaseMutex( g_pMutex, FALSE );
}



NTSTATUS CreateMutex()
{
	BEGIN_FUNC( CreateMutex );

	NTSTATUS status = STATUS_SUCCESS;

	status = _ExAllocatePool( g_pMutex, NonPagedPool, sizeof( KMUTEX ) );
	if ( NT_SUCCESS( status ) )
	{
		KeInitializeMutex( g_pMutex, 0 );
		status = STATUS_SUCCESS;
	}

	END_FUNC( CreateMutex );
	return ( status );
}



void DestroyMutex()
{
	BEGIN_FUNC( DestroyMutex );

	_ExFreePool( g_pMutex );

	END_FUNC( DestroyMutex );
}