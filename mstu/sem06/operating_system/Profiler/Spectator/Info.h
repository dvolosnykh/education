#pragma once

#include "SysInfo.h"
#include "def.h"



#define CLOSE_WAIT	50	// ms



extern "C"
{
NTSTATUS LockInfo( PLARGE_INTEGER pTimeout = NULL );
void UnlockInfo();

NTSTATUS ReloadInfo();
void FreeInfo();

void ProcessConvert( PPROCESS pProcess, PSYSTEM_PROCESS pSysProcess );
PSYSTEM_PROCESS ProcessFirst();
PSYSTEM_PROCESS ProcessNext();

void ThreadConvert( PTHREAD pThread, PSYSTEM_THREAD pSysThread );
PSYSTEM_THREAD ThreadFirst();
PSYSTEM_THREAD ThreadNext();
}