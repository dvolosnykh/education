#pragma once



extern "C"
{
HANDLE _OpenThread( ULONG desiredAccess, ULONG tid );
NTSTATUS _CloseThread( HANDLE hThread );
NTSTATUS _GetThreadContext( HANDLE hThread, PCONTEXT pContext );
}



#undef THREAD_SUSPEND_RESUME
#undef THREAD_GET_CONTEXT

#define THREAD_TERMINATE               (0x0001)  
#define THREAD_SUSPEND_RESUME          (0x0002)  
#define THREAD_GET_CONTEXT             (0x0008)  
#define THREAD_SET_CONTEXT             (0x0010)  
#define THREAD_SET_INFORMATION         (0x0020)  
#define THREAD_QUERY_INFORMATION       (0x0040)  
#define THREAD_SET_THREAD_TOKEN        (0x0080)
#define THREAD_IMPERSONATE             (0x0100)
#define THREAD_DIRECT_IMPERSONATION    (0x0200)