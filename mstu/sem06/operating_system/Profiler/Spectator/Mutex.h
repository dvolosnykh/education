#pragma once



extern "C"
{
NTSTATUS LockMutex( PLARGE_INTEGER pTimeout = NULL );
void UnlockMutex();

NTSTATUS CreateMutex();
void DestroyMutex();
}