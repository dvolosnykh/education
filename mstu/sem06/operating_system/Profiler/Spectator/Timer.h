#pragma once



extern "C"
{
VOID UpdateThread( IN PVOID );

NTSTATUS CreateTimer();
void DestroyTimer();

LONG GetTimerPeriod();
void SetTimerPeriod( LONG newPeriod );
}