#pragma once


extern "C"
{
#include <ntddk.h>
}
#include "pool.h"


#pragma warning( disable: 4311, 4312, 4390 )


#define msTo100ns( _ms_ )	( _ms_ * 10000 )


#if DBG
#define PREFIX	"[Spectator] "
#define DBG_PRINT( _x_ )	DbgPrint _x_
#define BEGIN_DISPATCH( _dispatchFuncName_ )											\
	DBG_PRINT(( PREFIX ));																\
	DBG_PRINT(( PREFIX "          ********** " #_dispatchFuncName_ " **********" ));	\
	DBG_PRINT(( PREFIX ))
#define END_DISPATCH( _dispatchFuncName_ )											\
	DBG_PRINT(( PREFIX ));															\
	DBG_PRINT(( PREFIX "               ***** " #_dispatchFuncName_ " *****" ));		\
	DBG_PRINT(( PREFIX ))
#define BEGIN_FUNC( _funcName_ )	\
	DBG_PRINT(( PREFIX ));			\
	DBG_PRINT(( PREFIX "  ======== " #_funcName_ " ========" ))
#define END_FUNC( _funcName_ )		\
	DBG_PRINT(( PREFIX "      ==== " #_funcName_ " ====" ))
#define BEGIN_DPC( _funcName_ )		\
	DBG_PRINT(( PREFIX ));			\
	DBG_PRINT(( PREFIX "            ^^^^^^^^ " #_funcName_ " ^^^^^^^^" ))
#define END_DPC( _funcName_ )											\
	DBG_PRINT(( PREFIX "                ^^^^ " #_funcName_ " ^^^^" ));	\
	DBG_PRINT(( PREFIX ))
#define DBG_REPORT_FAILURE( _funcName_, _status_ )	\
	DBG_PRINT(( PREFIX #_funcName_ ": failed with status = 0x%08X.", _status_ ))
#define DBG_REPORT_SUCCESS( _funcName_ )	\
	DBG_PRINT(( PREFIX #_funcName_ ": succeeded." ))
#define DBG_REPORT_RETVAL( _funcName_, _format_, _retVal_ )		\
	DBG_PRINT(( PREFIX #_funcName_ ": " #_retVal_ " = " #_format_ ".", _retVal_ ))
#define DBG_REPORT( _funcName_, _status_ )		\
	if ( NT_SUCCESS( _status_ ) )				\
		DBG_REPORT_SUCCESS( _funcName_ );		\
	else										\
		DBG_REPORT_FAILURE( _funcName_, _status_ );
#define DBG_REQUEST( _request_ )	\
	DBG_PRINT(( PREFIX "Request: " #_request_ ))
#define TRAP() DbgBreakPoint()
#else		// !DBG
#define PREFIX
#define DBG_PRINT( _x_ )
#define BEGIN_DISPATCH( _dispatchFuncName_ )
#define END_DISPATCH( _dispatchFuncName_ )
#define BEGIN_FUNC( _funcName_ )
#define END_FUNC( _funcName_ )
#define BEGIN_DPC( _funcName_ )
#define END_DPC( _funcName_ )
#define DBG_REPORT_FAILURE( _funcName_, _status_ )
#define DBG_REPORT_SUCCESS( _funcName_ )
#define DBG_REPORT_RETVAL( _funcName_, _format_, _retVal_ )
#define DBG_REPORT( _funcName_, _status_ )
#define DBG_REQUEST( _request_ )
#define TRAP()
#endif		// !DBG