#pragma once



#define SPECTATOR_POOL_TAG		'SPEC'



#define _ExAllocatePool( _ptr_, _poolType_, _size_ )	\
	__ExAllocatePool( ( PVOID * )&_ptr_, _poolType_, _size_ )

#define _ExFreePool( _ptr_ )	\
	__ExFreePool( ( PVOID * )&_ptr_ )


NTSTATUS __ExAllocatePool( PVOID * pPtr, IN POOL_TYPE poolType, IN SIZE_T size );
void __ExFreePool( PVOID * pPtr );