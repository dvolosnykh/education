#pragma once



#define SYMBOLIC_LINK	L"\\DosDevices\\Spectator"
#define DEVICE_NAME		L"\\Device\\Spectator"



typedef struct _DEVICE_EXTENSION
{
	PDEVICE_OBJECT pDeviceObject;
	UNICODE_STRING deviceName;
	UNICODE_STRING symbolicLink;
	ULONG clientCount;
} DEVICE_EXTENSION, *PDEVICE_EXTENSION;