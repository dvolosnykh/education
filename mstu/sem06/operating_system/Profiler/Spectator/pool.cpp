#include "stdafx.h"



NTSTATUS __ExAllocatePool( PVOID * pPtr, IN POOL_TYPE poolType, IN SIZE_T size )
{
	*pPtr = ExAllocatePoolWithTag
	(
		poolType,
		size,
		SPECTATOR_POOL_TAG
	);

	NTSTATUS status;

	if ( *pPtr != NULL )
	{
		status = STATUS_SUCCESS;
		DBG_PRINT(( PREFIX "ExAllocatePool: 0x%08X, %u", *pPtr, size ));
	}
	else
	{
		status = STATUS_INSUFFICIENT_RESOURCES;
		DBG_REPORT_FAILURE( ExAllocatePool, status );
	}

	return ( status );
}



void __ExFreePool( PVOID * pPtr )
{
	DBG_REPORT_RETVAL( ExFreePool, 0x%08X, *pPtr );

	if ( *pPtr != NULL )
	{
		ExFreePoolWithTag( *pPtr, SPECTATOR_POOL_TAG );
		*pPtr = NULL;
	}
}