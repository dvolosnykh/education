#include "stdafx.h"

#include "Spectator.h"
#include "ioctl.h"
#include "Info.h"
#include "def.h"
#include "Timer.h"
#include "Mutex.h"
#include "Threads.h"



#define COMPLETE_IRP( pIrp, status, size )			\
{													\
	pIrp->IoStatus.Status = status;					\
	pIrp->IoStatus.Information = size;				\
	IoCompleteRequest( pIrp, IO_NO_INCREMENT );		\
}



extern "C"
{
NTSTATUS NTAPI SpectatorDriverEntry( IN PDRIVER_OBJECT pDriverObject, IN PUNICODE_STRING pRegistryPath );
VOID NTAPI SpectatorDriverUnload( IN PDRIVER_OBJECT pDriverObject );
NTSTATUS NTAPI SpectatorDispatchCreate( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp );
NTSTATUS NTAPI SpectatorDispatchClose( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp );
NTSTATUS NTAPI SpectatorDispatchDeviceControl( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp );
}



#ifdef ALLOC_PRAGMA
#pragma alloc_text( "INIT", SpectatorDriverEntry )
#pragma alloc_text( "PAGE", SpectatorDriverUnload )
#pragma alloc_text( "PAGE", SpectatorDispatchCreate )
#pragma alloc_text( "PAGE", SpectatorDispatchClose )
#pragma alloc_text( "PAGE", SpectatorDispatchDeviceControl )
#endif



NTSTATUS NTAPI SpectatorDriverEntry( IN PDRIVER_OBJECT pDriverObject, IN PUNICODE_STRING pRegistryPath )
{
	BEGIN_DISPATCH( DriverEntry );

	PDEVICE_OBJECT pDeviceObject = NULL;
	PDEVICE_EXTENSION pDeviceExtension = NULL;
	UNICODE_STRING symbolicLink;
	UNICODE_STRING deviceName;
	NTSTATUS status = STATUS_SUCCESS;
	BOOLEAN ok = TRUE;

	if ( ok = pDriverObject != NULL )
	{
		DBG_PRINT(( PREFIX "pDriverObject = 0x%08X.", pDriverObject ));
		DBG_PRINT(( PREFIX "RegistryPath = %ws.", pRegistryPath->Buffer ));
	}
	else
	{
		DBG_PRINT(( PREFIX "Exception: pDriverObject = 0x%08X.", pDriverObject ));
	}

	if ( ok )
	{
		// Specifying entry-points:
		pDriverObject->DriverUnload = SpectatorDriverUnload;

		PDRIVER_DISPATCH * majorFunction = pDriverObject->MajorFunction;
		majorFunction[ IRP_MJ_CREATE         ] = SpectatorDispatchCreate;
		majorFunction[ IRP_MJ_CLOSE          ] = SpectatorDispatchClose;
		majorFunction[ IRP_MJ_DEVICE_CONTROL ] = SpectatorDispatchDeviceControl;

		// Create device:
		RtlInitUnicodeString( &deviceName, DEVICE_NAME );

		status = IoCreateDevice
		(
			pDriverObject,
			sizeof( DEVICE_EXTENSION ),
			&deviceName,
			FILE_DEVICE_SPECTATOR,
			FILE_DEVICE_SECURE_OPEN,
			FALSE,
			&pDeviceObject
		);
		if ( ok = NT_SUCCESS( status ) )
		{
			pDeviceExtension = ( PDEVICE_EXTENSION )pDeviceObject->DeviceExtension;
			pDeviceExtension->pDeviceObject = pDeviceObject;
			pDeviceExtension->deviceName = deviceName;
			DBG_REPORT_RETVAL( IoCreateDevice, 0x%08X, pDeviceObject );
		}
		else
			DBG_REPORT_FAILURE( IoCreateDevice, status );
	}

	if ( ok )
	{
		// Create symbolic link:
		RtlInitUnicodeString( &symbolicLink, SYMBOLIC_LINK );
		status = IoCreateSymbolicLink( &symbolicLink, &deviceName );
		if ( ok = NT_SUCCESS( status ) )
		{
			pDeviceExtension->symbolicLink = symbolicLink;
			DBG_REPORT_RETVAL( IoCreateSymbolicLink, %ws, symbolicLink.Buffer );
		}
		else
		{
			IoDeleteDevice( pDeviceObject );
			DBG_REPORT_FAILURE( IoCreateSymbolicLink, status );
		}
	}

	if ( ok )
	{
		status = CreateMutex();
		ok = NT_SUCCESS( status );
		if ( !ok )
		{
			IoDeleteSymbolicLink( &symbolicLink );
			IoDeleteDevice( pDeviceObject );
		}
	}

	if ( ok )
	{
		if ( LockInfo() == STATUS_SUCCESS )
		{
			ReloadInfo();
			UnlockInfo();
		}
	}

	if ( ok )
	{
		status = CreateTimer();
		ok = NT_SUCCESS( status );
		if ( !ok )
		{
			DestroyMutex();
			IoDeleteSymbolicLink( &symbolicLink );
			IoDeleteDevice( pDeviceObject );
		}
	}

	if ( ok )
	{
		pDeviceExtension->clientCount = 0;
		status = STATUS_SUCCESS;
	}

	END_DISPATCH( DriverEntry );
	return ( status );
}



VOID NTAPI SpectatorDriverUnload( IN PDRIVER_OBJECT pDriverObject )
{
	PAGED_CODE();

	BEGIN_DISPATCH( DriverUnload );

	DBG_PRINT(( PREFIX "pDriverObject = 0x%08X", pDriverObject ));

	PDEVICE_OBJECT pDeviceObject = pDriverObject->DeviceObject;
	PDEVICE_EXTENSION pDeviceExtension =
		( PDEVICE_EXTENSION )pDeviceObject->DeviceExtension;

	DBG_PRINT(( PREFIX "Number of clients = %u",
		pDeviceExtension->clientCount ));

	if ( pDeviceExtension->clientCount > 0 )
	{
		DBG_PRINT(( PREFIX "There are still some clients using the driver." ));
		DBG_PRINT(( PREFIX "Driver has not been unloaded." ));
	}
	else
	{
		DestroyTimer();
		DestroyMutex();

		// Delete symbolic link:
		PUNICODE_STRING pSymbolicLink = &pDeviceExtension->symbolicLink;
		DBG_PRINT(( PREFIX "Deleting symbolicLink = %ws", pSymbolicLink->Buffer ));
		IoDeleteSymbolicLink( pSymbolicLink );

		// Delete device object:
		DBG_PRINT(( PREFIX "Deleting pDeviceObject = 0x%08X",
			pDeviceExtension->pDeviceObject ));
		IoDeleteDevice( pDeviceObject );

		DBG_PRINT(( PREFIX "Driver has been unloaded." ));
	}

	END_DISPATCH( DriverUnload );
}



NTSTATUS NTAPI SpectatorDispatchCreate( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp )
{
	PAGED_CODE();

	BEGIN_DISPATCH( DispatchCreate );

	DBG_PRINT(( PREFIX "pDeviceObject = 0x%08X", pDeviceObject ));

	PDEVICE_EXTENSION pDeviceExtension =
		( PDEVICE_EXTENSION )pDeviceObject->DeviceExtension;

	pDeviceExtension->clientCount++;

	DBG_PRINT(( PREFIX "Number of clients = %u",
		pDeviceExtension->clientCount ));

	COMPLETE_IRP( pIrp, STATUS_SUCCESS, 0 );

	END_DISPATCH( DispatchCreate );
	return ( STATUS_SUCCESS );
}



NTSTATUS NTAPI SpectatorDispatchClose( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp )
{
	PAGED_CODE();

	BEGIN_DISPATCH( DispatchClose );

	DBG_PRINT(( PREFIX "pDeviceObject = 0x%08X", pDeviceObject ));

	PDEVICE_EXTENSION pDeviceExtension =
		( PDEVICE_EXTENSION )pDeviceObject->DeviceExtension;

	NTSTATUS status;
	if ( pDeviceExtension->clientCount > 0 )
	{
		pDeviceExtension->clientCount--;
		DBG_PRINT(( PREFIX "Number of clients = %u", pDeviceExtension->clientCount ));
		status = STATUS_SUCCESS;
	}
	else
	{
		DBG_PRINT(( PREFIX "Exceeding number of close-packets." ));
		status = STATUS_UNSUCCESSFUL;
	}

	COMPLETE_IRP( pIrp, STATUS_SUCCESS, 0 );

	END_DISPATCH( DispatchClose );
	return ( status );
}



NTSTATUS NTAPI SpectatorDispatchDeviceControl( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp )
{
	PAGED_CODE();

	BEGIN_DISPATCH( DispatchDeviceControl );

	DBG_PRINT(( PREFIX "pDeviceObject = 0x%08X", pDeviceObject ));

	NTSTATUS status = STATUS_SUCCESS;
	ULONG bytesNum = 0;

	PIO_STACK_LOCATION pIrpStack = IoGetCurrentIrpStackLocation( pIrp );
	PDEVICE_EXTENSION pDeviceExtension =
		( PDEVICE_EXTENSION )pDeviceObject->DeviceExtension;

	ULONG ioctlCode = pIrpStack->Parameters.DeviceIoControl.IoControlCode;
	switch ( ioctlCode )
	{
		case IOCTL_LAST_CLIENT:
		{
			DBG_REQUEST( IOCTL_LAST_CLIENT );
			if ( pDeviceExtension->clientCount == 1 )
			{
				DBG_PRINT(( PREFIX "Last client." ));
				status = STATUS_SUCCESS;
			}
			else
			{
				DBG_PRINT(( PREFIX "More than one clients." ));
				status = STATUS_UNSUCCESSFUL;
			}
		}
		break;

		case IOCTL_LOCK_INFO:
		{
			DBG_REQUEST( IOCTL_LOCK_INFO );
			LARGE_INTEGER timeout = RtlConvertLongToLargeInteger
				( - msTo100ns( GetTimerPeriod() / 10 ) );
			status = LockInfo( &timeout );
			if ( status == STATUS_TIMEOUT )
				status = STATUS_UNSUCCESSFUL;
		}
		break;

		case IOCTL_UNLOCK_INFO:
		{
			DBG_REQUEST( IOCTL_UNLOCK_INFO );
			UnlockInfo();
			status = STATUS_SUCCESS;
		}
		break;

		case IOCTL_PROCESS_FIRST:
		case IOCTL_PROCESS_NEXT:
		{
			switch ( ioctlCode )
			{
			case IOCTL_PROCESS_FIRST:	DBG_REQUEST( IOCTL_PROCESS_FIRST );	break;
			case IOCTL_PROCESS_NEXT:	DBG_REQUEST( IOCTL_PROCESS_NEXT );	break;
			}

			BOOLEAN ok = TRUE;
			ULONG len = 0;
			PSYSTEM_PROCESS pSysProcess = NULL;

			if ( ok )
			{
				len = pIrpStack->Parameters.DeviceIoControl.OutputBufferLength;
				ok = len == sizeof( PROCESS );
				if ( !ok )
				{
					DBG_PRINT(( PREFIX "Exception: output buffer length mismatch." ));
					status = STATUS_UNSUCCESSFUL;
				}
			}

			if ( ok )
			{
				switch ( ioctlCode )
				{
				case IOCTL_PROCESS_FIRST:	pSysProcess = ProcessFirst();	break;
				case IOCTL_PROCESS_NEXT:	pSysProcess = ProcessNext();	break;
				}
				ok = pSysProcess != NULL;
				if ( !ok )
				{
					DBG_PRINT(( PREFIX "End of list." ));
					status = STATUS_SUCCESS;
					bytesNum = 0;
				}
			}

			if ( ok )
			{
				PPROCESS pProcess = ( PPROCESS )pIrp->AssociatedIrp.SystemBuffer;
				ProcessConvert( pProcess, pSysProcess );

				status = STATUS_SUCCESS;
				bytesNum = len;
			}
		}
		break;

		case IOCTL_THREAD_FIRST:
		case IOCTL_THREAD_NEXT:
		{
			switch ( ioctlCode )
			{
			case IOCTL_THREAD_FIRST:	DBG_REQUEST( IOCTL_THREAD_FIRST );	break;
			case IOCTL_THREAD_NEXT:		DBG_REQUEST( IOCTL_THREAD_NEXT );	break;
			}

			BOOLEAN ok = TRUE;
			ULONG len = 0;
			PSYSTEM_THREAD pSysThread = NULL;

			if ( ok )
			{
				len = pIrpStack->Parameters.DeviceIoControl.OutputBufferLength;
				ok = len == sizeof( THREAD );
				if ( !ok )
				{
					DBG_PRINT(( PREFIX "Exception: output buffer length mismatch." ));
					status = STATUS_UNSUCCESSFUL;
				}
			}

			if ( ok )
			{
				switch ( ioctlCode )
				{
				case IOCTL_THREAD_FIRST:	pSysThread = ThreadFirst();	break;
				case IOCTL_THREAD_NEXT:		pSysThread = ThreadNext();	break;
				}
				ok = pSysThread != NULL;
				if ( !ok )
				{
					DBG_PRINT(( PREFIX "End of list." ));
					status = STATUS_SUCCESS;
					bytesNum = 0;
				}
			}

			if ( ok )
			{
				PTHREAD pThread = ( PTHREAD )pIrp->AssociatedIrp.SystemBuffer;
				ThreadConvert( pThread, pSysThread );

				status = STATUS_SUCCESS;
				bytesNum = len;
			}
		}
		break;

		case IOCTL_OPEN_THREAD:
		{
			DBG_REQUEST( IOCTL_OPEN_THREAD );

			BOOLEAN ok = TRUE;
			ULONG len = 0;

			if ( ok )
			{
				len = pIrpStack->Parameters.DeviceIoControl.InputBufferLength;
				ok = len == 2 * sizeof( ULONG );
				if ( !ok )
				{
					DBG_PRINT(( PREFIX "Exception: input buffer length mismatch." ));
					status = STATUS_UNSUCCESSFUL;
					bytesNum = 0;
				}
			}

			if ( ok )
			{
				len = pIrpStack->Parameters.DeviceIoControl.OutputBufferLength;
				ok = len == sizeof( HANDLE );
				if ( !ok )
				{
					DBG_PRINT(( PREFIX "Exception: output buffer length mismatch." ));
					status = STATUS_UNSUCCESSFUL;
					bytesNum = 0;
				}
			}

			if ( ok )
			{
				PULONG param = ( PULONG )pIrp->AssociatedIrp.SystemBuffer;
				ULONG desiredAccess = param[ 0 ];
				ULONG tid = param[ 1 ];
				HANDLE * phThread = ( HANDLE * )pIrp->AssociatedIrp.SystemBuffer;
				*phThread = _OpenThread( desiredAccess, tid );
				ok = *phThread != NULL;
				if ( !ok )
				{
					status = STATUS_UNSUCCESSFUL;
					bytesNum = 0;
				}
			}

			if ( ok )
			{
				status = STATUS_SUCCESS;
				bytesNum = len;
			}
		}
		break;

		case IOCTL_CLOSE_THREAD:
		{
			DBG_REQUEST( IOCTL_CLOSE_THREAD );

			BOOLEAN ok = TRUE;
			ULONG len = 0;

			if ( ok )
			{
				len = pIrpStack->Parameters.DeviceIoControl.InputBufferLength;
				ok = len == sizeof( HANDLE );
				if ( !ok )
				{
					DBG_PRINT(( PREFIX "Exception: input buffer length mismatch." ));
					status = STATUS_UNSUCCESSFUL;
				}
			}

			if ( ok )
			{
				HANDLE hThread = *( HANDLE * )pIrp->AssociatedIrp.SystemBuffer;
				_CloseThread( hThread );

				status = STATUS_SUCCESS;
			}
		}
		break;

		case IOCTL_GET_THREAD_CONTEXT:
		{
			DBG_REQUEST( IOCTL_GET_THREAD_CONTEXT );

			BOOLEAN ok = TRUE;
			BOOLEAN opened = FALSE;
			BOOLEAN suspended = FALSE;
			ULONG len = 0;

			if ( ok )
			{
				len = pIrpStack->Parameters.DeviceIoControl.InputBufferLength;
				ok = len == sizeof( CONTEXT ) + sizeof( HANDLE );
				if ( !ok )
				{
					DBG_PRINT(( PREFIX "Exception: input buffer length mismatch." ));
					status = STATUS_UNSUCCESSFUL;
				}
			}

			if ( ok )
			{
				len = pIrpStack->Parameters.DeviceIoControl.OutputBufferLength;
				ok = len == sizeof( CONTEXT );
				if ( !ok )
				{
					DBG_PRINT(( PREFIX "Exception: output buffer length mismatch." ));
					status = STATUS_UNSUCCESSFUL;
				}
			}

			if ( ok )
			{
				PCONTEXT pContext = ( PCONTEXT )pIrp->AssociatedIrp.SystemBuffer;
				HANDLE hThread = *( HANDLE * )( PUCHAR( pIrp->AssociatedIrp.SystemBuffer ) + sizeof( CONTEXT ) );
				status = _GetThreadContext( hThread, pContext );
				ok = NT_SUCCESS( status );
				if ( !ok )
				{
					status = STATUS_UNSUCCESSFUL;
					bytesNum = 0;
				}
			}

			if ( ok )
			{
				status = STATUS_SUCCESS;
				bytesNum = len;
			}
		}
		break;

		default:
		{
			DBG_PRINT(( PREFIX "Unsupported IOCTL-request: 0x%08X", ioctlCode ));

			status = STATUS_INVALID_DEVICE_REQUEST;
			bytesNum = 0;
		}
		break;
	}

	COMPLETE_IRP( pIrp, status, bytesNum );

	END_DISPATCH( DispatchDeviceControl );

	return ( status );
}