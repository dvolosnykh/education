#include "stdafx.h"


#include "Info.h"
#include "Mutex.h"



extern "C" BOOLEAN ProcessProhibited( PSYSTEM_PROCESS pProcess );



#ifdef ALLOC_PRAGMA
#pragma alloc_text( "PAGE", LockInfo )
#pragma alloc_text( "PAGE", UnlockInfo )
#pragma alloc_text( "PAGE", ReloadInfo )
#pragma alloc_text( "PAGE", FreeInfo )
#pragma alloc_text( "PAGE", ProcessConvert )
#pragma alloc_text( "PAGE", ProcessProhibited )
#pragma alloc_text( "PAGE", ProcessFirst )
#pragma alloc_text( "PAGE", ProcessNext )
#pragma alloc_text( "PAGE", ThreadConvert )
#pragma alloc_text( "PAGE", ThreadFirst )
#pragma alloc_text( "PAGE", ThreadNext )
#endif



static PUCHAR g_pInfo = NULL;



NTSTATUS LockInfo( PLARGE_INTEGER pTimeout /* = NULL */ )
{
	PAGED_CODE();

	BEGIN_FUNC( LockInfo );

	if ( pTimeout == NULL )
		DBG_PRINT(( PREFIX "Timeout: infinite" ));
	else
		DBG_PRINT(( PREFIX "Timeout: %u", *pTimeout ));

	NTSTATUS status = LockMutex( pTimeout );

	if ( status == STATUS_TIMEOUT )
	{
		DBG_REPORT_RETVAL( LockInfo, %s, STATUS_TIMEOUT );
	}
	else
	{
		DBG_REPORT_SUCCESS( LockInfo );
	}

	END_FUNC( LockInfo );
	return ( status );
}



void UnlockInfo()
{
	PAGED_CODE();

	BEGIN_FUNC( UnlockInfo );

	UnlockMutex();
	DBG_REPORT_SUCCESS( UnlockInfo );

	END_FUNC( UnlockInfo );
}



NTSTATUS ReloadInfo()
{
	PAGED_CODE();

	BEGIN_FUNC( ReloadInfo );

	NTSTATUS status = STATUS_SUCCESS;
	BOOLEAN ok = TRUE;
	ULONG requiredSpace = 0;

	if ( ok )
	{
		status = ZwQuerySystemInformation
		(
			SystemProcessesAndThreadsInformation,
			NULL,
			0,
			&requiredSpace
		);
		ok = NT_SUCCESS( status ) || status == STATUS_INFO_LENGTH_MISMATCH;
		if ( ok )
			DBG_REPORT_RETVAL( ZwQuerySystemInformation, %u, requiredSpace );
		else
			DBG_REPORT_FAILURE( ZwQuerySystemInformation, status );
	}

	if ( ok )
	{
		status = _ExAllocatePool( g_pInfo, PagedPool, requiredSpace );
		ok = NT_SUCCESS( status );
	}

	if ( ok )
	{
		status = ZwQuerySystemInformation
		(
			SystemProcessesAndThreadsInformation,
			g_pInfo,
			requiredSpace,
			NULL
		);

		DBG_REPORT( ZwQuerySystemInformation, status );
	}

	END_FUNC( ReloadInfo );
	return ( STATUS_SUCCESS );
}



void FreeInfo()
{
	PAGED_CODE();

	BEGIN_FUNC( FreeInfo );

	_ExFreePool( g_pInfo );

	END_FUNC( FreeInfo );
}



static PSYSTEM_PROCESS	g_pProcess = NULL;



void ProcessConvert( PPROCESS pProcess, PSYSTEM_PROCESS pSysProcess )
{
	PAGED_CODE();

	BEGIN_FUNC( ProcessConvert );

	pProcess->ThreadCount	= pSysProcess->ThreadCount;
	pProcess->CreateTime	= pSysProcess->CreateTime;
	pProcess->UserTime		= pSysProcess->UserTime;
	pProcess->KernelTime	= pSysProcess->KernelTime;
	pProcess->BasePriority	= pSysProcess->BasePriority;
	pProcess->ProcessId		= pSysProcess->ProcessId;
	pProcess->ParentId		= pSysProcess->InheritedFromProcessId;

	PUNICODE_STRING pUniStr = &pSysProcess->ProcessName;
#ifndef UNICODE

	ANSI_STRING ansiStr;
	RtlUnicodeStringToAnsiString( &ansiStr, pUniStr, TRUE );

	USHORT len = ansiStr.Length;
	if ( len > MAX_PROCESS_NAME_LEN )
		len = MAX_PROCESS_NAME_LEN;

	RtlCopyMemory( pProcess->ProcessName, ansiStr.Buffer, len * sizeof( TCHAR ) );
	pProcess->ProcessName[ len ] = TCHAR( '\0' );

	RtlFreeAnsiString( &ansiStr );

#else

	USHORT len = pUniStr->Length;
	if ( len > MAX_PROCESS_NAME_LEN )
		len = MAX_PROCESS_NAME_LEN;
	RtlCopyMemory( pProcess->ProcessName, pUniStr->Buffer, len * sizeof( TCHAR ) );
	pProcess->ProcessName[ len ] = TCHAR( '\0' );

#endif

	DBG_PRINT(( PREFIX "ProcessName  = \"%s\"", pProcess->ProcessName ));
	DBG_PRINT(( PREFIX "ProcessId    = 0x%08X",	pProcess->ProcessId ));
	DBG_PRINT(( PREFIX "ParentId     = 0x%08X",	pProcess->ParentId ));
	DBG_PRINT(( PREFIX "ThreadCount  = %u",		pProcess->ThreadCount ));
	DBG_PRINT(( PREFIX "BasePriority = %u",		pProcess->BasePriority ));
	DBG_PRINT(( PREFIX "CreateTime   = %u",		pProcess->CreateTime ));
	DBG_PRINT(( PREFIX "UserTime     = %u",		pProcess->UserTime ));
	DBG_PRINT(( PREFIX "KernelTime   = %u",		pProcess->KernelTime ));

	END_FUNC( ProcessConvert );
}



BOOLEAN ProcessProhibited( PSYSTEM_PROCESS pProcess )
{
	PAGED_CODE();

	BOOLEAN prohibited = FALSE;

	if ( pProcess == NULL )
	{
		prohibited = FALSE;
	}
	else if ( pProcess->ProcessId == 0 ||
		pProcess->ProcessId == ( ULONG )PsGetCurrentProcessId() )
	{
		prohibited = TRUE;
	}
	else
	{
		UNICODE_STRING uniStr;
		RtlInitUnicodeString( &uniStr, L"system" );
		LONG result = RtlCompareUnicodeString
		(
			&pProcess->ProcessName,
			&uniStr,
			TRUE
		);

		prohibited = result == 0;
	}

	return ( prohibited );
}



PSYSTEM_PROCESS ProcessFirst()
{
	PAGED_CODE();

	g_pProcess = PSYSTEM_PROCESS( g_pInfo );

	if ( ProcessProhibited( g_pProcess ) )
		g_pProcess = ProcessNext();

	return ( g_pProcess );
}



PSYSTEM_PROCESS ProcessNext()
{
	PAGED_CODE();

	if ( g_pProcess == NULL )
	{
		g_pProcess = ProcessFirst();
	}
	else
	{
		ULONG offset = g_pProcess->NextEntryDelta;
		if ( offset > 0 )
			g_pProcess = PSYSTEM_PROCESS( PUCHAR( g_pProcess ) + offset );
		else
			g_pProcess = NULL;
	}

	if ( ProcessProhibited( g_pProcess ) )
		g_pProcess = ProcessNext();

	return ( g_pProcess );
}



static PSYSTEM_THREAD	g_pThread = NULL;



void ThreadConvert( PTHREAD pThread, PSYSTEM_THREAD pSysThread )
{
	PAGED_CODE();
	
	BEGIN_FUNC( ThreadConvert );

	pThread->KernelTime			= pSysThread->KernelTime;
	pThread->UserTime			= pSysThread->UserTime;
	pThread->CreateTime			= pSysThread->CreateTime;
	pThread->WaitTime			= pSysThread->WaitTime;
	pThread->ThreadId			= ( ULONG )pSysThread->ClientId.UniqueThread;
	pThread->Priority			= pSysThread->Priority;
	pThread->BasePriority		= pSysThread->BasePriority;
	pThread->ContextSwitchCount	= pSysThread->ContextSwitchCount;

	DBG_PRINT(( PREFIX "ThreadId            = 0x%08X",	pThread->ThreadId ));
	DBG_PRINT(( PREFIX "ContextSwitchCount  = %u",		pThread->ContextSwitchCount ));
	DBG_PRINT(( PREFIX "Priority            = %u",		pThread->Priority ));
	DBG_PRINT(( PREFIX "BasePriority        = %u",		pThread->BasePriority ));
	DBG_PRINT(( PREFIX "CreateTime          = %u",		pThread->CreateTime ));
	DBG_PRINT(( PREFIX "WaitTime            = %u",		pThread->WaitTime ));
	DBG_PRINT(( PREFIX "UserTime            = %u",		pThread->UserTime ));
	DBG_PRINT(( PREFIX "KernelTime          = %u",		pThread->KernelTime ));
	
	END_FUNC( ThreadConvert );
}

PSYSTEM_THREAD ThreadFirst()
{
	PAGED_CODE();

	if ( g_pProcess == NULL )
	{
		g_pThread = NULL;
	}
	else
	{
		g_pThread = PSYSTEM_THREAD( g_pProcess->Threads );
	}

	return ( g_pThread );
}



PSYSTEM_THREAD ThreadNext()
{
	PAGED_CODE();

	if ( g_pProcess == NULL )
	{
		g_pThread = NULL;
	}
	else if ( g_pThread == NULL )
	{
		g_pThread = ThreadFirst();
	}
	else
	{
		ULONG index = ULONG( g_pThread - g_pProcess->Threads + 1 );
		if ( index < g_pProcess->ThreadCount )
		{
			g_pThread = g_pProcess->Threads + index;
		}
		else
		{
			g_pThread = NULL;
		}
	}

	return ( g_pThread );
}