#include "stdafx.h"

#include "Threads.h"



extern "C" NTSYSAPI NTSTATUS NTAPI ZwOpenThread
(
	OUT PHANDLE ThreadHandle,
	IN ACCESS_MASK DesiredAccess,
	IN POBJECT_ATTRIBUTES ObjectAttributes,
	IN PCLIENT_ID ClientId
);

extern "C" NTSYSAPI NTSTATUS NTAPI PsGetContextThread
(
	IN PETHREAD pThread,
	OUT PCONTEXT pContext,
	IN KPROCESSOR_MODE PrevMode
);



#ifdef ALLOC_PRAGMA
#pragma alloc_text( "PAGE", _OpenThread )
#pragma alloc_text( "PAGE", _CloseThread )
#pragma alloc_text( "PAGE", _GetThreadContext )
#endif



HANDLE _OpenThread( ULONG desiredAccess, ULONG tid )
{
	BEGIN_FUNC( OpenThread );

	CLIENT_ID clientId;
	clientId.UniqueProcess = NULL;
	clientId.UniqueThread = ( HANDLE )tid;

	OBJECT_ATTRIBUTES objectAttributes;
	RtlZeroMemory( &objectAttributes, sizeof( objectAttributes ) );

	HANDLE hThread = NULL;
	NTSTATUS status = ZwOpenThread
	(
		&hThread,
		desiredAccess,
		&objectAttributes,
		&clientId
	);

	if ( !NT_SUCCESS( status ) )
	{
		DBG_REPORT_FAILURE( ZwOpenThread, status );
		DBG_PRINT(( PREFIX "ThreadID = 0x%08X", tid ));
		hThread = NULL;
	}
	else
	{
		DBG_REPORT_RETVAL( ZwOpenThread, 0x%08X, hThread );
	}

	END_FUNC( OpenThread );
	return ( hThread );
}



NTSTATUS _CloseThread( HANDLE hThread )
{
	BEGIN_FUNC( CloseThread );

	DBG_PRINT(( PREFIX "hThread = 0x%08X", hThread ));

	NTSTATUS status = ZwClose( hThread );
	DBG_REPORT( ZwClose, status );

	END_FUNC( CloseThread );
	return ( status );
}



NTSTATUS _GetThreadContext( HANDLE hThread, PCONTEXT pContext )
{
	BEGIN_FUNC( GetThreadContext );

	DBG_PRINT(( PREFIX "hThread = 0x%08X", hThread ));

	PETHREAD pThread = NULL;
	NTSTATUS status = ObReferenceObjectByHandle
	(
		hThread,
		THREAD_GET_CONTEXT,		// desiredAccess - from OpenThread
		NULL,
		KernelMode,
		( PVOID * )&pThread,
		NULL
	);

	if ( !NT_SUCCESS( status ) )
	{
		DBG_REPORT_FAILURE( ObReferenceObjectByHandle, status );
	}
	else
	{
		DBG_REPORT_RETVAL( ObReferenceObjectByHandle, 0x%08X, pThread );

		status = PsGetContextThread( pThread, pContext, KernelMode );
		if ( !NT_SUCCESS( status ) )
		{
			DBG_REPORT_FAILURE( PsGetContextThread, status );
		}
		else
		{
			DBG_PRINT(( PREFIX "CS     = 0x%08X" , pContext->SegCs ));
			DBG_PRINT(( PREFIX "DS     = 0x%08X" , pContext->SegDs ));
			DBG_PRINT(( PREFIX "SS     = 0x%08X" , pContext->SegSs ));
			DBG_PRINT(( PREFIX "ES     = 0x%08X" , pContext->SegEs ));
			DBG_PRINT(( PREFIX "FS     = 0x%08X" , pContext->SegFs ));
			DBG_PRINT(( PREFIX "GS     = 0x%08X" , pContext->SegGs ));
			DBG_PRINT(( PREFIX "GS     = 0x%08X" , pContext->SegGs ));
			DBG_PRINT(( PREFIX "EAX    = 0x%08X" , pContext->Eax ));
			DBG_PRINT(( PREFIX "EBX    = 0x%08X" , pContext->Ebx ));
			DBG_PRINT(( PREFIX "ECX    = 0x%08X" , pContext->Ecx ));
			DBG_PRINT(( PREFIX "EDX    = 0x%08X" , pContext->Edx ));
			DBG_PRINT(( PREFIX "EDI    = 0x%08X" , pContext->Edi ));
			DBG_PRINT(( PREFIX "ESI    = 0x%08X" , pContext->Esi ));
			DBG_PRINT(( PREFIX "EBP    = 0x%08X" , pContext->Ebp ));
			DBG_PRINT(( PREFIX "EIP    = 0x%08X" , pContext->Eip ));
			DBG_PRINT(( PREFIX "ESP    = 0x%08X" , pContext->Esp ));
			DBG_PRINT(( PREFIX "EFLAGS = 0x%08X" , pContext->EFlags ));
		}

		ObDereferenceObject( pThread );
	}

	END_FUNC( GetThreadContext );
	return ( status );
}