#include "stdafx.h"

#include "Info.h"
#include "Timer.h"



#ifdef ALLOC_PRAGMA
#pragma alloc_text( "PAGE", UpdateThread )
#pragma alloc_text( "PAGE", CreateTimer )
#pragma alloc_text( "PAGE", DestroyTimer )
#pragma alloc_text( "PAGE", GetTimerPeriod )
#pragma alloc_text( "PAGE", SetTimerPeriod )
#endif



static BOOLEAN g_quit = FALSE;
static LONG g_timerPeriod = 1000;	// ms

static PKTHREAD g_pUpdateThread = NULL;
static PKEVENT g_pUpdateEvent = NULL;
static PKTIMER g_pTimer = NULL;
static PKDPC g_pTimerDpc = NULL;



VOID UpdateThreadFunc( IN PVOID )
{
	PAGED_CODE();

	for ( g_quit = FALSE; ; )
	{
		// check if OnTime has set update-event:
		KeWaitForSingleObject
		(
			g_pUpdateEvent,
			Executive,
			KernelMode,
			FALSE,
			NULL
		);

	if ( g_quit ) break;

		// update info:
//		LARGE_INTEGER timeout = RtlConvertLongToLargeInteger
//			( - msTo100ns( g_timerPeriod / 10 ) );
		if ( LockInfo() == STATUS_SUCCESS )
		{
			ReloadInfo();
			UnlockInfo();
		}
	}

	FreeInfo();

	DBG_PRINT(( PREFIX "Update-thread has been terminated." ));
	PsTerminateSystemThread( STATUS_SUCCESS );
}



static void OnTime( IN PKDPC, IN PVOID, IN PVOID, IN PVOID )
{
	BEGIN_DPC( OnTime );

	KeSetEvent( g_pUpdateEvent, 0, FALSE );

	END_DPC( OnTime );
}



NTSTATUS CreateTimer()
{
	PAGED_CODE();

	BEGIN_FUNC( CreateTimer );

	BOOLEAN ok = TRUE;
	NTSTATUS status = STATUS_SUCCESS;

	// allocate space for synchronization objects:
	if ( ok )
	{
		status = _ExAllocatePool
		(
			g_pTimer,
			NonPagedPool,
			sizeof( KTIMER )
		);
		ok = NT_SUCCESS( status );
	}

	if ( ok )
	{
		status = _ExAllocatePool
		(
			g_pTimerDpc,
			NonPagedPool,
			sizeof( KDPC )
		);
		ok = NT_SUCCESS( status );
	}

	if ( ok )
	{
		status = _ExAllocatePool
		(
			g_pUpdateEvent,
			NonPagedPool,
			sizeof( KEVENT )
		);
		ok = NT_SUCCESS( status );
	}

	HANDLE hThread = NULL;
	if ( ok )
	{
		// initialize synchronization objects:
		KeInitializeTimerEx( g_pTimer, SynchronizationTimer );
		KeInitializeDpc( g_pTimerDpc, OnTime, NULL );
		KeInitializeEvent( g_pUpdateEvent, SynchronizationEvent, FALSE );

		// create update-thread:
		OBJECT_ATTRIBUTES objectAttributes;
		InitializeObjectAttributes
		(
			&objectAttributes,
			NULL,
			OBJ_KERNEL_HANDLE,
			NULL,
			NULL
		);
		status = PsCreateSystemThread
		(
			&hThread,
			THREAD_ALL_ACCESS,
			&objectAttributes,
			NULL,
			NULL,
			UpdateThreadFunc,
			NULL
		);

		if ( !( ok = NT_SUCCESS( status ) ) )
		{
			DBG_REPORT_FAILURE( PsCreateSystemThread, status );
			hThread = NULL;
		}
		else
		{
			DBG_REPORT_RETVAL( PsCreateSystemThread, 0x%08X, hThread );
		}
	}

	if ( ok )
	{
		ObReferenceObjectByHandle
		(
			hThread,
			THREAD_ALL_ACCESS,
			NULL,
			KernelMode,
			( PVOID * )&g_pUpdateThread,
			NULL
		);

		ZwClose( hThread );

		// start timer:
		LARGE_INTEGER dueTime = RtlConvertLongToLargeInteger( 0 );
		BOOLEAN existed = KeSetTimerEx
		(
			g_pTimer,
			dueTime,
			g_timerPeriod,
			g_pTimerDpc
		);
		DBG_PRINT(( PREFIX "KeSetTimerEx: %s",
			existed ? "Timer already queued." : "Timer successfully queued." ));
	}

	if ( !ok )
		_ExFreePool( g_pUpdateEvent );
	if ( g_pUpdateEvent == NULL )
		_ExFreePool( g_pTimerDpc );
	if ( g_pTimerDpc == NULL )
		_ExFreePool( g_pTimer );

	END_FUNC( CreateTimer );
	return ( STATUS_SUCCESS );
}



void DestroyTimer()
{
	PAGED_CODE();

	BEGIN_FUNC( DestroyTimer );

	// wait until update-thread terminates:
	g_quit = TRUE;
	KeWaitForSingleObject
	(
		g_pUpdateThread,
		Suspended,	//Executive
		KernelMode,
		FALSE,
		NULL
	);

	ObDereferenceObject( g_pUpdateThread );
	
	// stop timer:
	BOOLEAN result = KeCancelTimer( g_pTimer );
	DBG_PRINT(( PREFIX "KeCancelTimer: %s",
		result ? "Timer is still in queue." : "Timer successfully dequeued." ));

	// free allocated stuff:
	_ExFreePool( g_pUpdateEvent );
	_ExFreePool( g_pTimerDpc );
	_ExFreePool( g_pTimer );

	END_FUNC( DestroyTimer );
}



LONG GetTimerPeriod()
{
	PAGED_CODE();

	return ( g_timerPeriod );
}

void SetTimerPeriod( LONG newPeriod )
{
	PAGED_CODE();

	g_timerPeriod = newPeriod;
}