#pragma once



typedef NTSTATUS ( NTAPI * AddrZwSuspendThread )
(
	IN HANDLE ThreadHandle,
	OUT PULONG PreviousSuspendCount OPTIONAL
);

typedef NTSTATUS ( NTAPI * AddrZwResumeThread )
(
	IN HANDLE ThreadHandle,
	OUT PULONG PreviousSuspendCount OPTIONAL
);
/*
typedef NTSTATUS ( NTAPI * AddrZwGetContextThread )
(
	IN HANDLE ThreadHandle,
	OUT PCONTEXT Context
);
*/


extern "C" AddrZwSuspendThread		ZwSuspendThread;
extern "C" AddrZwResumeThread		ZwResumeThread;
//extern "C" AddrZwGetContextThread	ZwGetContextThread;



BOOLEAN TuneFuncAddresses();