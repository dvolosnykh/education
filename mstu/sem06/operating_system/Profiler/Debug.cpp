#include "stdafx.h"



BOOL EnableDebug( BOOL enable )
{
	BOOL ok = FALSE;
	HANDLE hToken;

	if ( OpenProcessToken( GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken ) )
	{
		TOKEN_PRIVILEGES tp;
		tp.PrivilegeCount = 1;
		LookupPrivilegeValue( NULL, SE_DEBUG_NAME, &tp.Privileges[ 0 ].Luid );

		tp.Privileges[ 0 ].Attributes = enable ? SE_PRIVILEGE_ENABLED : 0;
		AdjustTokenPrivileges( hToken, FALSE, &tp, sizeof( tp ), NULL, NULL );

		ok = GetLastError() == ERROR_SUCCESS;

		CloseHandle( hToken );
	}

	return ( ok );
}