#pragma once



extern "C"
{
#include <ntddk.h>
}



#define SYMBOLIC_LINK	L"\\DosDevices\\Spectator"
#define DEVICE_NAME	L"\\Device\\Spectator"

#define MAX_BUFFER_SIZE		256



typedef struct _DEVICE_EXTENSION
{
	PDEVICE_OBJECT pDeviceObject;
	UNICODE_STRING deviceName;
	UNICODE_STRING symbolicLink;
	ULONG clientCount;
	UCHAR inputBuffer[ MAX_BUFFER_SIZE ];
	ULONG inputLength;
	PUCHAR pCurrentByte;
	ULONG transferredBytesNum;
	ULONG leftBytesNum;
	PKINTERRUPT pInterruptObject;
	KDPC objectCompleteIoDpc;
	PIRP pProcessedIrp;
} DEVICE_EXTENSION, *PDEVICE_EXTENSION;



#define PORT_BASE   PUCHAR( 0x3F8 )   // ����� ����� COM1
                    //PUCHAR( 0x2F8 ) // ����� ����� COM2
#define PORT_IRQ    0x04    // COM1 Irq
                    //0x03  // COM2 Irq

#define TRANSMIT_SPEED 0x1  // �������� �������� �������
                            // (��� ��������� �������� �������� ������)
// Base+0 - ������� ������ (��� DLAB=0)
//          ������: ������� ���������
//          ������: ������� �����������
// Base+0 - ������� ���� �������� ������� (��� DLAB=1)
#define DATA_REG            0x0
// Base+1 - ������� ���������� ������������ (��� DLAB=0)
// Base+1 - ������� ���� �������� ������� (��� DLAB=1)
#define INT_CONTROL_REG     0x1
                                 // ���������� ����������:
#define ICR_RESEIVEREADY    0x01 // ��� 0 - �� ���������� ����������� ������
#define ICR_SENDREADY       0x02 // ��� 1 - �� ���������� ������������ ������
#define ICR_ERROR           0x04 // ��� 2 - �� ������ �����
#define ICR_MODEM           0x08 // ��� 3 - �� ������� ������
//��� 4 - ��������� sleep mode (������ ��� ���������������� 16750)
//��� 5 - ��������� ������ ������� ����������� (������ ��� ���������������� 16750)
//��� 6-7 ���������������
// ���������� ����������� ����������
#define ICR_ENABLE          ICR_RESEIVEREADY|ICR_SENDREADY

// Base+2 (������) - ������� ������������� ������������
// Base+2 (������) - ������� ���������� ��������
#define INT_STATE_REG       0x2
// ���� 6-7 = 11 - ����� fifo 16550�
#define IIR_FIFONOTUSED     0x80 // ���� 6-7 = 10 - ����� FIFO �������, �� �� ������������
#define IIR_FIFOREGULAR     0x00 // ���� 6-7 = 00 - ������� �����
// ��� 5 - ��������� 64-��������� ������ FIFO (������ ��� ���������������� 16750)
// ��� 4 - ���������������
// � ������ FIFO:
// ���� 3-1 = 011 - ����� �����; ����� ����������� ������� �������� ��������� �����
// ���� 3-1 = 010 - ������ ������; ����� ����������� ������� ������
// ���� 3-1 = 110 - ���������� ����-���� ������/�������� ������� (��� �������� ������);
//                  ����� ����������� ������� ������
// ���� 3-1 = 001 - ������� ������ ����; ����� ����������� ������� ������
// ���� 3-1 = 000 - ��������� ��������� ������; ����� ����������� ������� �������� ��������� ������

// � ������� ������:
// ��� 3 - ���������� �� ����-���� ������ (�� ������������ ��� ��������� 8250 � 16450)
// ���� 2-1 - ������� ���������� � ��������� �����������:
#define IIR_CRASHINT   0x06 // ���� 2-1 = 11 - ����� �����; ����� ����������� ������� �������� ��������� �����
#define IIR_WRITEINT   0x04 // ���� 2-1 = 10 - ������ ������; ����� ����������� ������� ������
// ���� 2-1 = 01 - ������� ������; ����� ����������� ������� ������
// ���� 2-1 = 00 - ��������� ��������� ������; ����� ����������� ������� �������� ��������� ������
#define IIR_WAIT        0x01 // ��� 0 - ��� ���������� ����������

#define LINE_CONTROL_REG    0x3
// Base+3 - ������� �������� �����
// ��� 7 - ���������� �������� � �������� ������� (DLAB)
#define LCR_NODLAB          0x7B // ����� ���� DLAB � LCR, 1 ����-���
// ��� 6 - ������������ ������ ����� (������� �����)
// ��� 5 - �������������� ������������ ���� �������� (������������� � �������� ���� �������� �������� ���� 4)
// ��� 4 - �������� ���� �������� (��. ��� 5, ��� �������� = 1 ��� 0)
// ��� 3 - ���������� ������������ ����
// ��� 2 - ���������� ����-��� (0: 1 ����-���, 1: 2 ����-����)
// ���� 1-0 - ���������� ��� ������
#define LCR_8BITS           0x03 // ���� 1-0 = 11 - ��������� ������� ������������� ����� 8 ���
// ���� 1-0 = 10 - 7 ���
// ���� 1-0 = 01 - 6 ���
// ���� 1-0 = 00 - 5 ���

#define MODEM_CONTROL_REG   0x4
// Base+4 - ������� ���������� �������
// ���� 7-5 ��������������� � ������ ���� = 000
#define MCR_LOOPBACK        0x10 // ��� 4 - Loopback mode � MCR
#define MCR_OUT2            0x08 // ��� 3 - ���������� ���������� ����� OUT2
#define MCR_OUT1            0x04 // ��� 2 - ���������� ���������� ����� OUT1
#define MCR_ENABLEOUTS      MCR_OUT1|MCR_OUT2
#define MCR_RTS             0x02 // ��� 1 - ���������� ���������� ����� RTS
#define MCR_DTR             0x01 // ��� 0 - ���������� ���������� ����� DTR

#define LINE_STATUS_REG     0x5
// Base+5 - ������� ������� �����
// ��� 7 - ������ �������� ������ � ������ FIFO
// ��� 6 - ������� ����������� ����
// ��� 5 - ������� ����������� ����� ������� ���� ��� ��������
// ��� 4 - ��������� ������ �����
// ��� 3 - �������� ����-���
// ��� 2 - ������ ������������ ���� (���� ��������)
// ��� 1 - ������ ��������� �������
// ��� 0 - ���������� �������� ������
#define MODEM_STATUS_REG    0x6
// Base+6 - ������� ������� ������
#define MSR_DCD             0x80  // ��� 7 - ��������� ����� DCD
#define MSR_RI              0x40  // ��� 6 - ��������� ����� RI
#define MSR_DSR             0x20  // ��� 5 - ��������� ����� DSR
#define MSR_CTS             0x10  // ��� 4 - ��������� ����� CTS
// ��� 3 - ���������� ��������� ����� DCD
// ��� 2 - ���������� ��������� ����� RI
// ��� 1 - ���������� ��������� ����� DSR
// ��� 0 - ���������� ��������� ����� CTS

// ������� ������ � �������� � ������ �� ���������
// WRITE_PORT_UCHAR - ���������������� HAL: OUT port, byte
// READ_PORT_UCHAR - ���������������� HAL:  IN byte, port
#define WriteModemControlReg( portBase, byte )	WRITE_PORT_UCHAR( portBase + MODEM_CONTROL_REG, byte )
#define ReadModemControlReg( portBase )		READ_PORT_UCHAR( portBase + MODEM_CONTROL_REG )

#define WriteDataReg( portBase, byte )		WRITE_PORT_UCHAR( portBase + DATA_REG, byte )
#define ReadDataReg( portBase )			READ_PORT_UCHAR( portBase + DATA_REG )

#define WriteIntControlReg( portBase, byte )	WRITE_PORT_UCHAR( portBase + INT_CONTROL_REG, byte )
#define ReadIntControlReg( portBase )		READ_PORT_UCHAR( portBase + INT_CONTROL_REG )

#define WriteIntStateReg( portBase, byte )	WRITE_PORT_UCHAR( portBase + INT_STATE_REG, byte )
#define ReadIntStateReg( portBase )		READ_PORT_UCHAR( portBase + INT_STATE_REG )

#define WriteLineControlReg( portBase, byte )	WRITE_PORT_UCHAR( portBase + LINE_CONTROL_REG, byte )
#define ReadLineControlReg( portBase )		READ_PORT_UCHAR( portBase + LINE_CONTROL_REG )

#define WriteLineStatusReg( portBase, byte )	WRITE_PORT_UCHAR( portBase + LINE_STATUS_REG, byte )
#define ReadLineStatusReg( portBase )		READ_PORT_UCHAR( portBase + LINE_STATUS_REG )

#define WriteModemStatusReg( portBase, byte )	WRITE_PORT_UCHAR( portBase + MODEM_STATUS_REG, byte )
#define ReadModemStatusReg( portBase )		READ_PORT_UCHAR( portBase + MODEM_STATUS_REG )

#define EnableAllInterrupts( portBase )		WriteIntControlReg( portBase, ICR_ENABLE )
#define DisableAllInterrupts( portBase )	WriteIntControlReg( portBase, 0 )

#define FIFOEnable( portBase )			WriteIntStateReg( portBase, IIR_FIFOREGULAR )
#define FIFODisable( portBase )			WriteIntStateReg( portBase, IIR_FIFONOTUSED )



#if DBG
#define PREFIX	"[COM-port] "
#define DBG_PRINT( _x_ )	DbgPrint _x_
#define BEGIN_DISPATCH( _dispatchFuncName_ )							\
	DBG_PRINT(( PREFIX ));									\
	DBG_PRINT(( PREFIX "          ********** " #_dispatchFuncName_ " **********" ));	\
	DBG_PRINT(( PREFIX ))
#define END_DISPATCH( _dispatchFuncName_ )						\
	DBG_PRINT(( PREFIX ));								\
	DBG_PRINT(( PREFIX "               ***** " #_dispatchFuncName_ " *****" ));	\
	DBG_PRINT(( PREFIX ))
#define BEGIN_FUNC( _funcName_ )					\
	DBG_PRINT(( PREFIX ));						\
	DBG_PRINT(( PREFIX "  ======== " #_funcName_ " ========" ))
#define END_FUNC( _funcName_ )						\
	DBG_PRINT(( PREFIX "      ==== " #_funcName_ " ====" ))
#define BEGIN_DPC( _funcName_ )							\
	DBG_PRINT(( PREFIX ));							\
	DBG_PRINT(( PREFIX "            ^^^^^^^^ " #_funcName_ " ^^^^^^^^" ))
#define END_DPC( _funcName_ )							\
	DBG_PRINT(( PREFIX "                ^^^^ " #_funcName_ " ^^^^" ));	\
	DBG_PRINT(( PREFIX ))
#define BEGIN_ISR( _funcName_ )							\
	DBG_PRINT(( PREFIX ));							\
	DBG_PRINT(( PREFIX "            %%%%%%%% " #_funcName_ " %%%%%%%%" ))
#define END_ISR( _funcName_ )							\
	DBG_PRINT(( PREFIX "                %%%% " #_funcName_ " %%%%" ));	\
	DBG_PRINT(( PREFIX ))
#define DBG_REPORT_FAILURE( _funcName_, _status_ )					\
	DBG_PRINT(( PREFIX #_funcName_ ": failed with status = 0x%08X.", _status_ ))
#define DBG_REPORT_SUCCESS( _funcName_ )			\
	DBG_PRINT(( PREFIX #_funcName_ ": succeeded." ))
#define DBG_REPORT_RETVAL( _funcName_, _format_, _retVal_ )				\
	DBG_PRINT(( PREFIX #_funcName_ ": " #_retVal_ " = " #_format_ ".", _retVal_ ))
#define DBG_REPORT( _funcName_, _status_ )			\
	if ( NT_SUCCESS( _status_ ) )				\
		DBG_REPORT_SUCCESS( _funcName_ );		\
	else							\
		DBG_REPORT_FAILURE( _funcName_, _status_ );
#define DBG_REQUEST( _request_ )			\
	DBG_PRINT(( PREFIX "Request: " #_request_ ))
#define TRAP() DbgBreakPoint()
#else		// !DBG
#define PREFIX
#define DBG_PRINT( _x_ )
#define BEGIN_DISPATCH( _dispatchFuncName_ )
#define END_DISPATCH( _dispatchFuncName_ )
#define BEGIN_FUNC( _funcName_ )
#define END_FUNC( _funcName_ )
#define BEGIN_DPC( _funcName_ )
#define END_DPC( _funcName_ )
#define BEGIN_ISR( _funcName_ )
#define END_ISR( _funcName_ )
#define DBG_REPORT_FAILURE( _funcName_, _status_ )
#define DBG_REPORT_SUCCESS( _funcName_ )
#define DBG_REPORT_RETVAL( _funcName_, _format_, _retVal_ )
#define DBG_REPORT( _funcName_, _status_ )
#define DBG_REQUEST( _request_ )
#define TRAP()
#endif		// !DBG