#include "driver.h"



#define COMPLETE_IRP( pIrp, status, size )			\
	PIRP( pIrp )->IoStatus.Status = NTSTATUS( status );	\
	PIRP( pIrp )->IoStatus.Information = ULONG( size );	\
	IoCompleteRequest( PIRP( pIrp ), IO_NO_INCREMENT );



extern "C"
{
NTSTATUS DriverEntry( IN PDRIVER_OBJECT pDriverObject, IN PUNICODE_STRING pRegistryPath );
VOID DriverUnload( IN PDRIVER_OBJECT pDriverObject );
NTSTATUS DispatchCreate( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp );
NTSTATUS DispatchClose( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp );
NTSTATUS DispatchRead( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp );
NTSTATUS DispatchWrite( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp );
}
VOID DriverStartIo( PDEVICE_OBJECT pDeviceObject, PIRP pIrp );
BOOLEAN Isr( IN PKINTERRUPT pInterruptObject, IN PVOID pContext );
VOID CompleteIoDpc( IN PKDPC pDpc, IN PVOID pContext, IN PVOID pArg1, IN PVOID pArg2 );
VOID CancelRoutine( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp );
VOID SendByte( IN UCHAR bits );
BOOLEAN WriteToPort( IN PVOID pContext );


#ifdef ALLOC_PRAGMA
#pragma alloc_text( "INIT", DriverEntry )
#pragma alloc_text( "PAGE", DriverUnload )
#pragma alloc_text( "PAGE", DispatchCreate )
#pragma alloc_text( "PAGE", DispatchClose )
#pragma alloc_text( "PAGE", DispatchRead )
#pragma alloc_text( "PAGE", DispatchWrite )
#endif



NTSTATUS DriverEntry( IN PDRIVER_OBJECT pDriverObject, IN PUNICODE_STRING pRegistryPath )
{
	BEGIN_DISPATCH( DriverEntry );

	DBG_PRINT(( PREFIX "pDriverObject = 0x%08X.", pDriverObject ));
	DBG_PRINT(( PREFIX "RegistryPath = %ws.", pRegistryPath->Buffer ));

	PDEVICE_OBJECT pDeviceObject = NULL;
	PDEVICE_EXTENSION pDeviceExtension = NULL;
	UNICODE_STRING symbolicLink;
	UNICODE_STRING deviceName;
	NTSTATUS status = STATUS_SUCCESS;
	BOOLEAN ok = TRUE;

	if ( ok )
	{
		// Specifying entry-points:
		pDriverObject->DriverUnload = DriverUnload;
		pDriverObject->DriverStartIo = DriverStartIo;

		PDRIVER_DISPATCH * majorFunction = pDriverObject->MajorFunction;
		majorFunction[ IRP_MJ_CREATE ] = DispatchCreate;
		majorFunction[ IRP_MJ_CLOSE  ] = DispatchClose;
		majorFunction[ IRP_MJ_READ   ] = DispatchRead;
		majorFunction[ IRP_MJ_WRITE  ] = DispatchWrite;

		// Create device:
		RtlInitUnicodeString( &deviceName, DEVICE_NAME );

		status = IoCreateDevice
		(
			pDriverObject,
			sizeof( DEVICE_EXTENSION ),
			&deviceName,
			FILE_DEVICE_UNKNOWN,
			FILE_DEVICE_SECURE_OPEN,
			FALSE,
			&pDeviceObject
		);
		if ( ok = NT_SUCCESS( status ) )
		{
			pDeviceObject->Flags |= DO_BUFFERED_IO;

			pDeviceExtension = ( PDEVICE_EXTENSION )pDeviceObject->DeviceExtension;
			pDeviceExtension->pDeviceObject = pDeviceObject;
			pDeviceExtension->deviceName = deviceName;

			DBG_REPORT_RETVAL( IoCreateDevice, 0x%08X, pDeviceObject );
		}
		else
		{
			DBG_REPORT_FAILURE( IoCreateDevice, status );
		}
	}

	if ( ok )
	{
		// Create symbolic link:
		RtlInitUnicodeString( &symbolicLink, SYMBOLIC_LINK );
		status = IoCreateSymbolicLink( &symbolicLink, &deviceName );
		if ( ok = NT_SUCCESS( status ) )
		{
			pDeviceExtension->symbolicLink = symbolicLink;
			DBG_REPORT_RETVAL( IoCreateSymbolicLink, %ws, symbolicLink.Buffer );
		}
		else
		{
			IoDeleteDevice( pDeviceObject );
			DBG_REPORT_FAILURE( IoCreateSymbolicLink, status );
		}
	}

	if ( ok )
	{
		// register DPC-routine:
		KeInitializeDpc
		(
			&pDeviceExtension->objectCompleteIoDpc,
			CompleteIoDpc,
			pDeviceObject
		);
	}

	if ( ok )
	{
		DisableAllInterrupts( PORT_BASE );

		// ��������� �������� �������� ������
		// (DLAB=1, Data Register - ������� ����� �������� �������,
		// Interrupt Control register - ������� ����� �������� �������)
		WriteLineControlReg( PORT_BASE, ~LCR_NODLAB );
		WriteDataReg( PORT_BASE, TRANSMIT_SPEED );
		WriteIntControlReg( PORT_BASE, 0 );

		// ��������� ������� ������������ ������ ������ 8 ��� (��� DLAB = 0)
		UCHAR bLineCtrl = ReadLineControlReg( PORT_BASE );
		WriteLineControlReg( PORT_BASE, ( bLineCtrl & LCR_NODLAB ) | LCR_8BITS );

		KIRQL irql;
		KAFFINITY affinity;
		ULONG vector = HalGetInterruptVector
		(
			Isa,
			0,
			PORT_IRQ,
			PORT_IRQ,
			&irql,
			&affinity
		);

		DBG_PRINT(( PREFIX "HalGetInterruptVector: IRQL = %u, Affinity = %u, Vector = 0x%08X" ));

		status = IoConnectInterrupt
		(
			&pDeviceExtension->pInterruptObject,
			Isr,
			pDeviceExtension,
			NULL,
			vector,
			irql,
			irql,
			Latched,
			TRUE,
			affinity,
			FALSE
		);
		ok = NT_SUCCESS( status );
		if ( !ok )
		{
			IoDeleteSymbolicLink( &symbolicLink );
			IoDeleteDevice( pDeviceObject );
		}

		// �������� ����� OUT1 � OUT2 ��� ����, ����� ���������� �� �������������
		WriteModemControlReg( PORT_BASE, MCR_OUT2 | MCR_OUT1 );

		EnableAllInterrupts( PORT_BASE );
	}

	if ( ok )
	{
		pDeviceExtension->inputLength = 0;
		pDeviceExtension->clientCount = 0;
		status = STATUS_SUCCESS;
	}

	END_DISPATCH( DriverEntry );
	return ( status );
}



VOID DriverUnload( IN PDRIVER_OBJECT pDriverObject )
{
	PAGED_CODE();

	BEGIN_DISPATCH( DriverUnload );

	DBG_PRINT(( PREFIX "pDriverObject = 0x%08X", pDriverObject ));

	PDEVICE_OBJECT pDeviceObject = pDriverObject->DeviceObject;
	PDEVICE_EXTENSION pDeviceExtension =
		( PDEVICE_EXTENSION )pDeviceObject->DeviceExtension;

	DisableAllInterrupts( PORT_BASE );

	// Remove our DPC-object:
	KeRemoveQueueDpc( &pDeviceExtension->objectCompleteIoDpc );

	// Turn off interrupts:
	IoDisconnectInterrupt( pDeviceExtension->pInterruptObject );

	EnableAllInterrupts( PORT_BASE );

	// Delete symbolic link:
	PUNICODE_STRING pSymbolicLink = &pDeviceExtension->symbolicLink;
	DBG_PRINT(( PREFIX "Deleting symbolicLink = %ws", pSymbolicLink->Buffer ));
	IoDeleteSymbolicLink( pSymbolicLink );

	// Delete device object:
	DBG_PRINT(( PREFIX "Deleting pDeviceObject = 0x%08X",
		pDeviceExtension->pDeviceObject ));
	IoDeleteDevice( pDeviceObject );

	END_DISPATCH( DriverUnload );
}



NTSTATUS DispatchCreate( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp )
{
	PAGED_CODE();

	BEGIN_DISPATCH( DispatchCreate );

	PDEVICE_EXTENSION pDeviceExtension =
		( PDEVICE_EXTENSION )pDeviceObject->DeviceExtension;

	NTSTATUS status;
	if ( pDeviceExtension->clientCount == 0 )
	{
		pDeviceExtension->clientCount++;
		status = STATUS_SUCCESS;
	}
	else
	{
		DBG_PRINT(( PREFIX "Exclusive access only." ));
		status = STATUS_UNSUCCESSFUL;
	}

	COMPLETE_IRP( pIrp, STATUS_SUCCESS, 0 );

	END_DISPATCH( DispatchCreate );
	return ( status );
}



NTSTATUS DispatchClose( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp )
{
	PAGED_CODE();

	BEGIN_DISPATCH( DispatchClose );

	PDEVICE_EXTENSION pDeviceExtension =
		( PDEVICE_EXTENSION )pDeviceObject->DeviceExtension;

	NTSTATUS status;
	if ( pDeviceExtension->clientCount > 0 )
	{
		pDeviceExtension->clientCount--;
		status = STATUS_SUCCESS;
	}
	else
	{
		DBG_PRINT(( PREFIX "Exceeding number of close-packets." ));
		status = STATUS_UNSUCCESSFUL;
	}

	COMPLETE_IRP( pIrp, STATUS_SUCCESS, 0 );

	END_DISPATCH( DispatchClose );
	return ( status );
}



NTSTATUS DispatchRead( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp )
{
	PAGED_CODE();

	BEGIN_DISPATCH( DispatchRead );

	PIO_STACK_LOCATION pIrpStack = IoGetCurrentIrpStackLocation( pIrp );

	NTSTATUS status = STATUS_SUCCESS;

	ULONG readLen = pIrpStack->Parameters.Read.Length;
	DBG_PRINT(( PREFIX "Attempting to read %u bytes.", readLen ));

	if ( readLen > 0 )
	{
		status = STATUS_PENDING;
		IoMarkIrpPending( pIrp );
		IoStartPacket( pDeviceObject, pIrp, 0, CancelRoutine );
	}
	else
	{
		status = STATUS_SUCCESS;
		COMPLETE_IRP( pIrp, status, 0 );
	}

	END_DISPATCH( DispatchRead );
	return ( status );
}



NTSTATUS DispatchWrite( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp )
{
	PAGED_CODE();

	BEGIN_DISPATCH( DispatchWrite );

	PIO_STACK_LOCATION pIrpStack = IoGetCurrentIrpStackLocation( pIrp );

	NTSTATUS status = STATUS_SUCCESS;

	ULONG writeLen = pIrpStack->Parameters.Write.Length;
	DBG_PRINT(( PREFIX "Attempting to write %u bytes.", writeLen ));

	if ( writeLen > 0 )
	{
		status = STATUS_PENDING;
		IoMarkIrpPending( pIrp );
		IoStartPacket( pDeviceObject, pIrp, 0, CancelRoutine );
	}
	else
	{
		status = STATUS_SUCCESS;
		COMPLETE_IRP( pIrp, status, 0 );
	}

	END_DISPATCH( DispatchWrite );
	return ( status );
}



VOID DriverStartIo( PDEVICE_OBJECT pDeviceObject, PIRP pIrp )
{
	BEGIN_FUNC( StartIo );

	PDEVICE_EXTENSION pDeviceExtension =
		( PDEVICE_EXTENSION )pDeviceObject->DeviceExtension;
	PIO_STACK_LOCATION pIrpStack = IoGetCurrentIrpStackLocation( pIrp );

	switch ( pIrpStack->MajorFunction )
	{
		case IRP_MJ_READ:
		{
			ULONG readLen = pIrpStack->Parameters.Read.Length;
			if ( readLen > MAX_BUFFER_SIZE )
				readLen = MAX_BUFFER_SIZE;

			PUCHAR userBuffer = ( PUCHAR )pIrp->AssociatedIrp.SystemBuffer;
			PUCHAR inputBuffer = pDeviceExtension->inputBuffer;
			PULONG pInputLength = &pDeviceExtension->inputLength;

			for ( ULONG i = 0; i < readLen && i < *pInputLength; i++ )
				userBuffer[ i ] = inputBuffer[ i ];

			if ( *pInputLength > readLen )
			{
				for ( ULONG i = readLen; i < *pInputLength; i++ )
					inputBuffer[ i - readLen ] = inputBuffer[ i ];

				*pInputLength -= readLen;
			}
			else
			{
				*pInputLength = 0;
			}

			COMPLETE_IRP( pIrp, STATUS_SUCCESS, readLen );
		}
		break;

		case IRP_MJ_WRITE:
		{
			pDeviceExtension->pProcessedIrp = pIrp;
			pDeviceExtension->pCurrentByte = PUCHAR( pIrp->AssociatedIrp.SystemBuffer );
			pDeviceExtension->transferredBytesNum = 0;
			pDeviceExtension->leftBytesNum = pIrpStack->Parameters.Write.Length;

			KeSynchronizeExecution
			(
				pDeviceExtension->pInterruptObject,
				WriteToPort,
				&pDeviceExtension->pCurrentByte
			);
		}
		break;
	}

	END_FUNC( StartIo );
}



BOOLEAN Isr( IN PKINTERRUPT pInterruptObject, IN PVOID pContext )
{
	BEGIN_FUNC( Isr );

	BOOLEAN ok = TRUE;
	PDEVICE_EXTENSION pDeviceExtension = ( PDEVICE_EXTENSION )pContext;

	// ���� ������������ ���������� �� �������� ����������� �� �������� ������,
	// �� ��������� ���������� �����������
	UCHAR intState = ReadIntStateReg( PORT_BASE );

	DBG_PRINT(( PREFIX "Interrupt identification register state = 0x%08X", intState ));

	// ���������� �� ������ �����
	if ( ( intState & IIR_CRASHINT ) == IIR_CRASHINT )
	{
		KeInsertQueueDpc
		(
			&pDeviceExtension->objectCompleteIoDpc,
			( PVOID )pDeviceExtension->pProcessedIrp,
			( PVOID )pDeviceExtension->transferredBytesNum
		);

		ok = TRUE;
	}
	else if ( ( intState & IIR_WRITEINT ) != IIR_WRITEINT )
	{
		// ���������� �� �� ������ ������
		DBG_PRINT(( PREFIX "Isr: appeared not after writing." ));
		ok = FALSE;
	}
	else
	{
		UCHAR byte = ReadDataReg( PORT_BASE );
		pDeviceExtension->transferredBytesNum++;
		pDeviceExtension->leftBytesNum--;

		if ( pDeviceExtension->inputLength < MAX_BUFFER_SIZE )
			pDeviceExtension->inputBuffer[ pDeviceExtension->inputLength++ ] = byte;

		if ( pDeviceExtension->leftBytesNum > 0 )
		{
			SendByte( *pDeviceExtension->pCurrentByte );
			pDeviceExtension->pCurrentByte++;
		}
		else
		{
			KeInsertQueueDpc
			(
				&pDeviceExtension->objectCompleteIoDpc,
				( PVOID )pDeviceExtension->pProcessedIrp,
				( PVOID )pDeviceExtension->transferredBytesNum
			);
		}

		ok = TRUE;
	}

	END_FUNC( Isr );
	return ( ok );
}



VOID CompleteIoDpc( IN PKDPC pDpc, IN PVOID pContext, IN PVOID pArg1, IN PVOID pArg2 )
{
	BEGIN_FUNC( CompleteIoDpc );

	DBG_PRINT(( PREFIX "Data transmision has completed." ));

	PIRP pIrp = ( PIRP )pArg1;
	ULONG size = ( ULONG )pArg2;
	COMPLETE_IRP( pIrp, STATUS_SUCCESS, size );

	PDEVICE_OBJECT pDeviceObject = ( PDEVICE_OBJECT )pContext;
	IoStartNextPacket( pDeviceObject, TRUE );

	END_FUNC( CompleteIoDpc );
}



VOID CancelRoutine( IN PDEVICE_OBJECT pDeviceObject, IN PIRP pIrp )
{
	BEGIN_FUNC( CancelRoutine );

	PDEVICE_EXTENSION pDeviceExtension =
		( PDEVICE_EXTENSION )pDeviceObject->DeviceExtension;

	BOOLEAN cancelable = TRUE;
	if ( pIrp != pDeviceObject->CurrentIrp )
	{
		cancelable = KeRemoveEntryDeviceQueue
		(
			&pDeviceObject->DeviceQueue,
			&pIrp->Tail.Overlay.DeviceQueueEntry
		);
	}

	if ( cancelable )
	{
		IoReleaseCancelSpinLock( pIrp->CancelIrql );
		COMPLETE_IRP( pIrp, STATUS_CANCELLED, 0 );
	}

	END_FUNC( CancelRoutine );
}



VOID SendByte( IN UCHAR bits )
{
	BEGIN_FUNC( SendByte );

	// ���������� ���� ����������
	//WriteModemControlReg( PORT_BASE, MCR_ENABLEOUTS | MCR_RTS | MCR_DTR );

	// ��������� Looopback mode ��� ������ ���������� ������ � ������� ���������
	WriteModemControlReg( PORT_BASE, MCR_LOOPBACK | MCR_RTS | MCR_DTR );
	WriteDataReg( PORT_BASE, bits );     // ������ � ���������� (��� ����������, �.�. ��� Loopback ��� �� ���������)
	KeStallExecutionProcessor( 100 );

	// ���������� ���� ����������
	WriteModemControlReg( PORT_BASE, MCR_ENABLEOUTS | MCR_RTS | MCR_DTR );
	WriteDataReg( PORT_BASE, bits );     // �������� ������, ������ ��������� ����������

	END_FUNC( SendByte );
}



BOOLEAN WriteToPort( IN PVOID pContext )
{
	BEGIN_FUNC( WriteToPort );

	PUCHAR * ppCurrentByte = ( PUCHAR * )pContext;

	SendByte( **ppCurrentByte );
	( *ppCurrentByte )++;

	END_FUNC( WriteToPort );
	return ( TRUE );
}