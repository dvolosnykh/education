using System;
using System.Management;
using System.Text; 



namespace WMI
{
	class DeviceAnalyzer
	{
		public static void Main( string[] args )
		{
			Console.WriteLine( ConvertToNum( GetHardwareReport() ) );
			Console.Read();
		}

		private static uint ConvertToNum( string report )
		{
			uint num = 0;
			foreach ( char c in report )
				num += c;

			return ( num );
		}

		private static string GetHardwareReport()
		{
			return
			(
				GetProperty( "Win32_ComputerSystem", "UserName" ) +
				GetProperty( "Win32_Processor", "ProcessorId" ) +
				GetProperty( "Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled = TRUE" ) +
				GetProperty( "Win32_DiskDrive", "PNPDeviceId" )
			);
		}

		private static string GetProperty( string device_class, string property )
		{
			string query_str = "SELECT * FROM " + device_class;
			return GetPropertyQuery( query_str, property );
		}

		private static string GetProperty( string device_class, string property, string where_section )
		{
			string query_str = "SELECT * FROM " + device_class + " WHERE " + where_section;
			return GetPropertyQuery( query_str, property );
		}

		private static string GetPropertyQuery( string query_str, string property )
		{
			ObjectQuery query = new ObjectQuery( query_str );
			ManagementObjectSearcher searcher = new ManagementObjectSearcher( query );
			ManagementObjectCollection searchResults = searcher.Get();

			StringBuilder sb = new StringBuilder();

			foreach ( ManagementObject resultObject in searchResults )
			{
				PropertyData propData = resultObject.Properties[ property ];
				sb.AppendFormat( "{0}\r\n", propData.Value );
			}

			return sb.ToString();
		}
	}
}