#include <tchar.h>
#include <stdio.h>

#include "common.h"


void ReadKeyDN( VALUE & d, VALUE & n, LPCTSTR const keyfile );
void Decode( LPCTSTR const src, LPCTSTR const dest, const VALUE d, const VALUE n );


int _tmain( int argc, _TCHAR * argv[] )
{
	const TCHAR * const keyfile = argv[ 1 ];
	const TCHAR * const src = argv[ 2 ];
	const TCHAR * const dest = argv[ 3 ];

	VALUE d, n;
	ReadKeyDN( d, n, keyfile );

	Decode( src, dest, d, n );

	return ( 0 );
}

void ReadKeyDN( VALUE & d, VALUE & n, LPCTSTR const keyfile )
{
	FILE * pf = _tfopen( keyfile, _T( "rb" ) );

	fread( &n, sizeof( n ), 1, pf );
	fread( &d, sizeof( d ), 1, pf );

	fclose( pf );
}

VALUE DecodeBlock( const VALUE p, const VALUE d, const VALUE n )
{
	return power_mod( p, d, n );
}

void Decode( LPCTSTR const src, LPCTSTR const dest, const VALUE d, const VALUE n )
{
	FILE * psrc = _tfopen( src, _T( "rb" ) );
	FILE * pdest = _tfopen( dest, _T( "wb" ) );

	VALUE a;
	UCHAR b;
	for ( ; fread( &a, sizeof( VALUE ), 1, psrc );
		fwrite( &b, sizeof( UCHAR ), 1, pdest ) )
	{
		b = ( UCHAR )DecodeBlock( a, d, n );
	}

	fclose( pdest );
	fclose( psrc );
}