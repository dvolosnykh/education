#include <tchar.h>
#include <math.h>

#define ALLOW_OUTPUT
#ifdef ALLOW_OUTPUT
#include <iostream>
using namespace std;
#else
#include <stdio.h>
#endif

#include "common.h"
#include "PrimeNums.h"


void GenKey( VALUE & n, VALUE & d, VALUE & e );


int _tmain( int argc, _TCHAR * argv[] )
{
	CPrimeNums::InitFor( PN_USE );
	ResetRandChain();

	VALUE n, d, e;
	GenKey( n, d, e );

	const TCHAR * const keyfile = argv[ 1 ];
	FILE * pf = _tfopen( keyfile, _T( "wb" ) );

	fwrite( &n, sizeof( n ), 1, pf );
	fwrite( &d, sizeof( d ), 1, pf );
	fwrite( &e, sizeof( e ), 1, pf );

	fclose( pf );
	return ( 0 );
}

//==========================================================================

void choose_n_pq_c( VALUE & n, VALUE & p, VALUE & q, VALUE & c );
bool choose_de( VALUE & d, VALUE & e, const VALUE c );

void GenKey( VALUE & n, VALUE & d, VALUE & e )
{
	VALUE p, q, c;

	do
	{
		choose_n_pq_c( n, p, q, c );
	}
	while ( !choose_de( d, e, c ) );

#ifdef ALLOW_OUTPUT
	cout << endl << endl << "-------------------------------" << endl;

	cout << "n = " << n << "\tp = " << p << "\tq = " << q << endl
		<< "c = " << c << endl
		<< "d = " << d << endl
		<< "e = " << e << endl << endl;

	char cc;
	cin.get( cc );
#endif
}

void choose_n_pq_c( VALUE & n, VALUE & p, VALUE & q, VALUE & c )
{
	const VALUE count = CPrimeNums::GetCount();

	VALUE low, high;
	do
	{
		p = CPrimeNums::Get( RandValue( 0, count - 1 ) );
		q = CPrimeNums::Get( RandValue( 0, count - 1 ) );

		const double q_inv = 1.0 / q;
		low = ( VALUE )( UCHAR_MAX * q_inv );
		high = ( VALUE )( VALUE_MAX * q_inv );
	}
	while ( p <= low || high < p );

	n = p * q;
	c = n - p - q + 1; // = ( p - 1 ) * ( q - 1 );

#ifdef ALLOW_OUTPUT
	cout << "Found: n = " << n << endl
		<< "       p = " << p << endl
		<< "       q = " << q << endl
		<< "       c = " << c << endl;
#endif
}

inline bool is_common_dividor( const VALUE i, const VALUE c, const VALUE d )
{
	return ( ( c % i == 0 ) && ( d % i == 0 ) );
}

inline bool is_even( const VALUE & num )
{
	return ( ( num & 0x1 ) == 0 );
}

bool is_coprime( const VALUE c, const VALUE d )
{
	bool coprime;
	if ( is_even( c ) && is_even( d ) )
		coprime = false;
	else
	{
		const VALUE count = CPrimeNums::GetCount();

		const VALUE lim_c = ( VALUE )sqrt( ( double )c );
		const VALUE lim_d = ( VALUE )sqrt( ( double )d );
		register const VALUE lim = min( lim_c, lim_d );

		coprime = true;
		for ( register VALUE i = 0; i < count && coprime; i++ )
		{
			const VALUE p = CPrimeNums::Get( i );
			coprime = p > lim || !is_common_dividor( p, c, d );
		}
	}

	return ( coprime );
}

bool choose_de( VALUE & d, VALUE & e, const VALUE c )
{
	bool found = false;
	for ( VALUE ed = 1; !found && ed <= VALUE_MAX - c; )
	{
		const VALUE lim = ( VALUE )sqrt( ( double )( ed += c ) );

		for ( VALUE i = 0; !found && ( d = CPrimeNums::Get( i ) ) <= lim; i++ )
		{
			e = ed / d;
			found = e != 1 && ed == e * d && is_coprime( c, d );
		}
	}

#ifdef ALLOW_OUTPUT
	if ( found )
		cout << "Found: d = " << d << "\te = " << e << endl;
	else
		cout << "Failed: finding d, e..." << endl;
#endif

	return ( found );
}