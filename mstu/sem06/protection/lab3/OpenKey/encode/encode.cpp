#include <tchar.h>
#include <stdio.h>

#include "common.h"


void ReadKeyEN( VALUE & e, VALUE & n, LPCTSTR const keyfile );
void Encode( LPCTSTR const src, LPCTSTR const dest, const VALUE e, const VALUE n );


int _tmain( int argc, _TCHAR * argv[] )
{
	const TCHAR * const keyfile = argv[ 1 ];
	const TCHAR * const src = argv[ 2 ];
	const TCHAR * const dest = argv[ 3 ];

	VALUE e, n;
	ReadKeyEN( e, n, keyfile );

	Encode( src, dest, e, n );

	return ( 0 );
}

void ReadKeyEN( VALUE & e, VALUE & n, LPCTSTR const keyfile )
{
	FILE * pf = _tfopen( keyfile, _T( "rb" ) );

	fread( &n, sizeof( n ), 1, pf );
	fseek( pf, sizeof( VALUE ), SEEK_CUR );
	fread( &e, sizeof( e ), 1, pf );

	fclose( pf );
}

VALUE EncodeBlock( const VALUE p, const VALUE e, const VALUE n )
{
	return power_mod( p, e, n );
}

void Encode( LPCTSTR const src, LPCTSTR const dest, const VALUE e, const VALUE n )
{
	FILE * psrc = _tfopen( src, _T( "rb" ) );
	FILE * pdest = _tfopen( dest, _T( "wb" ) );

	UCHAR a;
	VALUE b;
	for ( ; fread( &a, 1, sizeof( a ), psrc );
		fwrite( &b, 1, sizeof( b ), pdest ) )
	{
		b = EncodeBlock( a, e, n );
	}

	fclose( pdest );
	fclose( psrc );
}