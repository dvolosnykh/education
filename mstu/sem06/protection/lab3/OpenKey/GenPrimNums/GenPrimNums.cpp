#include <tchar.h>

#include "PrimeNums.h"

#define ALLOW_OUTPUT

int _tmain( int argc, _TCHAR* argv[] )
{
	CPrimeNums::InitFor( PN_GEN );
	CPrimeNums::PrepareNums();
#ifdef ALLOW_OUTPUT
	CPrimeNums::Print();
#endif

	return ( 0 );
}

