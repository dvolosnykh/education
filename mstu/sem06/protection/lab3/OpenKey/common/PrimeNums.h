#pragma once


#include "Storage.h"


enum { PN_USE, PN_GEN };


class CPrimeNums
{
private:
	static CStorage _storage;

public:
	static void InitFor( const USHORT init_type );

	static VALUE GetCount() { return _storage.GetCount(); };
	static VALUE Get( const VALUE index ) { return _storage.Get( index ); };

	static void PrepareNums();
	static void Print();

private:
	static bool PrepareDecade( const VALUE p_dec );
	static bool IsPrime( const VALUE num );
};