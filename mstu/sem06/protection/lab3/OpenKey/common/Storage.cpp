#include "Storage.h"

#include <tchar.h>



void CStorage::Lock( const USHORT lock_type )
{
	if ( _file != NULL )
		Unlock();

	LPCTSTR mode;
	switch ( lock_type )
	{
	case LOCK_R:	mode = _T( "rb" );	break;
	case LOCK_W:	mode = _T( "wb+" );	break;
	}

	_file = _tfopen( PN_FILE, mode );
}

void CStorage::Unlock()
{
	fclose( _file );
}

VALUE CStorage::GetCount() const
{
	fseek( _file, 0, SEEK_END );
	return ( VALUE )( ftell( _file ) / sizeof( VALUE ) );
}

VALUE CStorage::Get() const
{
	VALUE number;
	fread( &number, sizeof( number ), 1, _file );
	return ( number );
}

void CStorage::Put( const VALUE & number )
{
	fwrite( &number, sizeof( number ), 1, _file );
}

VALUE CStorage::Get( const VALUE index ) const
{
	Seek( index );
	return Get();
}

void CStorage::Put( const VALUE index, const VALUE & number )
{
	Seek( index );
	Put( number );
}

void CStorage::Seek( const VALUE index ) const
{
	fseek( _file, index * sizeof( VALUE ), SEEK_SET );
}