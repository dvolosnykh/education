#include "common.h"

#include <time.h>
#include <stdlib.h>


VALUE power_mod( VALUE p, VALUE e, const VALUE n )
{
	register VALUE r;
	for ( r = 1; e > 0; )
	{
		if ( e & 0x1 )
			r = ( r * p ) % n;
		p = ( p * p ) % n;
		e >>= 1;
	}

	return ( r );
}

static double RandDbl()
{
	return ( rand() / ( double )RAND_MAX );
}

VALUE RandValue( const VALUE a, const VALUE b )
{
	return ( a + ( VALUE )( ( b - a ) * RandDbl() ) );
}

void ResetRandChain()
{
	srand( ( unsigned )time( NULL ) );
}