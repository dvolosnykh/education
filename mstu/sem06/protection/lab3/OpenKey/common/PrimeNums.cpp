#include "PrimeNums.h"

#include <math.h>

#include <iostream>
using namespace std;


#define ALLOW_OUTPUT_IN_PROCESS


CStorage CPrimeNums::_storage;



void CPrimeNums::InitFor( const USHORT init_type )
{
	USHORT lock_type;
	switch ( init_type )
	{
	case PN_USE:	lock_type = LOCK_R;	break;
	case PN_GEN:	lock_type = LOCK_W;	break;
	}

	_storage.Lock( lock_type );
}

void CPrimeNums::PrepareNums()
{
	for ( VALUE p = 0; PrepareDecade( p ); p += 10 );
}

void CPrimeNums::Print()
{
	const VALUE count = _storage.GetCount();
	cout << "num : " << ( ULONG )count << endl;

	_storage.Seek( 0 );
	for ( VALUE i = 0; i < count; i++ )
		cout << ( ULONG )_storage.Get() << endl;

	char cc;
	cin.get( cc );
}

bool CPrimeNums::PrepareDecade( const VALUE p_dec )
{
	static VALUE count;
    bool goon;

	if ( p_dec == 0 )
	{
		const VALUE pp[] = { 2, 3, 5, 7 };
		const USHORT pp_len = sizeof( pp ) / sizeof( pp[ 0 ] );

		count = 0;
		for ( USHORT i = 0; i < pp_len; i++ )
		{
			_storage.Put( count++, pp[ i ] );

#ifdef ALLOW_OUTPUT_IN_PROCESS
			cout << ( ULONG )pp[ i ] << endl;
#endif
		}

		goon = true;
	}
	else
	{
		const VALUE pp[] = { 1, 3, 7, 9 };
		const USHORT pp_len = sizeof( pp ) / sizeof( pp[ 0 ] );

		USHORT i;
		for ( i = 0; i < pp_len && p_dec <= VALUE_MAX - pp[ i ]; i++ )
		{
			const VALUE p = p_dec + pp[ i ];
			if ( IsPrime( p ) )
			{
				_storage.Put( count++, p );
#ifdef ALLOW_OUTPUT_IN_PROCESS
				cout << ( ULONG )p << endl;
#endif
			}
		}

		goon = i >= pp_len;
	}

	return ( goon );
}

bool CPrimeNums::IsPrime( const VALUE num )
{
	bool prime;
	if ( _storage.IsEmpty() )
		prime = true;
	else
	{
		const VALUE lim = ( VALUE )sqrt( ( double )num );

		VALUE p;
		for ( register VALUE i = 0; ; i++ )
		{
			p = _storage.Get( i );

		if ( p > lim || ( num % p == 0 ) ) break;
		}

		prime = p > lim;
	}

	return ( prime );
}