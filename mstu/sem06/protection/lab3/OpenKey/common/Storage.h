#pragma once

#include <stdio.h>

#include "value.h"


#define PN_FILE	_T( "pn.dat" )


enum { LOCK_R, LOCK_W };



class CStorage
{
private:
    FILE * _file;

public:
	CStorage() : _file( NULL ) {};
	~CStorage() { Unlock(); };

	void Lock( const USHORT lock_type );
	void Unlock();

	VALUE GetCount() const;
	bool IsEmpty() const { return ( GetCount() == 0 ); };
	VALUE Get() const;
	void Put( const VALUE & number );
	VALUE Get( const VALUE index ) const;
	void Put( const VALUE index, const VALUE & number );
	void Seek( const VALUE index ) const;
};