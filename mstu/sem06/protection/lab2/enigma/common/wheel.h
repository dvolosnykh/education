#pragma once



#include "alphabet.h"
#include "types.h"



#define MIX_LINKS



class CWheel
{
private:
	index_t _counter;	// number of rotations.
	symbol_t * _symbol;	// _symbol[ _counter + s ] is the result of
						// encoding symbol s.
public:
	CWheel() { _symbol = new symbol_t [ CAlphabet::Length() ]; };
	~CWheel() { delete _symbol; };

	void InitLinks();
//	void InitLinks( symbol_t * buff );
//	void GenLinks( symbol_t * buff );
	void Reset() { _counter = 0; };
	bool Rotate();
	symbol_t EncodeSymbol( const symbol_t s ) const;
	symbol_t DecodeSymbol( const symbol_t s ) const;
//	void PrintLinks() const;

private:
	static int CWheel::Rand();
	index_t MakeIndex( index_t index ) const;
	void SwapLinks( const index_t i, const index_t j );
	index_t SearchSymbol( const symbol_t s ) const;
};