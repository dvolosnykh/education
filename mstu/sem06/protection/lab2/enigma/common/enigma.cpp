#include "enigma.h"



#include <fstream>
using namespace std;



void CEnigma::InitLinks( const index_t init_value )
{
	srand( ( unsigned )init_value );

	for ( index_t i = 0; i < WHEELS_NUM; i++ )
		_wheel[ i ].InitLinks();
}
/*
void CEnigma::InitLinks( char * keyfile )
{
	ifstream fin( keyfile, ios::in | ios::binary );

	symbol_t * buff = new symbol_t [ CAlphabet::Length() ];

	for ( index_t i = 0; i < WHEELS_NUM; i++ )
	{
		fin.read( ( char * )buff, CAlphabet::Length() );
		_wheel[ i ].InitLinks( buff );
	}

	delete buff;
}

void CEnigma::GenLinks( char * keyfile )
{
	ofstream fout( keyfile, ios::out | ios::binary );

	symbol_t * buff = new symbol_t [ CAlphabet::Length() ];

	for ( index_t i = 0; i < WHEELS_NUM; i++ )
	{
		_wheel[ i ].GenLinks( buff );
		fout.write( ( char * )buff, CAlphabet::Length() );
	}

	delete buff;
}
*/
void CEnigma::EncodeFile( char * in, char * out )
{
	ifstream fin( in, ios::in | ios::binary );
	ofstream fout( out, ios::out | ios::binary );

	for ( ResetWheels(); ; RotateWheels() )
	{
		const symbol_t s = fin.get();

	if ( fin.eof() ) break;

		fout.put( EncodeSymbol( s ) );
	}
}

void CEnigma::DecodeFile( char * in, char * out )
{
	ifstream fin( in, ios::in | ios::binary );
	ofstream fout( out, ios::out | ios::binary );

	for ( ResetWheels(); ; RotateWheels() )
	{
		const symbol_t s = fin.get();

	if ( fin.eof() ) break;

		fout.put( DecodeSymbol( s ) );
	}
}

void CEnigma::EncodeText( symbol_t * in, symbol_t * out )
{
	ResetWheels();

	for ( ; *in != '\0'; in++, out++ )
	{
		*out = EncodeSymbol( *in );
		RotateWheels();
	}

	*out = '\0';
}

void CEnigma::DecodeText( symbol_t * in, symbol_t * out )
{
	ResetWheels();

	for ( ; *in != '\0'; in++, out++ )
	{
		*out = DecodeSymbol( *in );
		RotateWheels();
	}

	*out = '\0';
}

symbol_t CEnigma::EncodeSymbol( symbol_t s )
{
	for ( index_t i = 0; i < WHEELS_NUM; i++ )
		s = _wheel[ i ].EncodeSymbol( s );

	return ( s );
}

symbol_t CEnigma::DecodeSymbol( symbol_t s )
{
	for ( index_t i = WHEELS_NUM - 1; i >= 0; i-- )
		s = _wheel[ i ].DecodeSymbol( s );

	return ( s );
}



void CEnigma::ResetWheels()
{
	for ( index_t i = 0; i < WHEELS_NUM; i++ )
		_wheel[ i ].Reset();
}

void CEnigma::RotateWheels()
{
#ifdef ROTATE_WHEELS
	bool period = true;

	for ( index_t i = 0; i < WHEELS_NUM; i++ )
		if ( period )
			period = _wheel[ i ].Rotate();
#endif
}
/*
void CEnigma::PrintLinks() const
{
	for ( index_t i = 0; i < WHEELS_NUM; i++ )
	{
		cout << "Wheel #" << i << ": " << endl;
		_wheel[ i ].PrintLinks();
	}
}
*/