#include "wheel.h"

#include <stdlib.h>


void CWheel::InitLinks()
{
	for ( register index_t i = 0; i < CAlphabet::Length(); i++ )
		_symbol[ i ] = CAlphabet::GetSymbol( i );

#ifdef MIX_LINKS
	for ( register index_t i = 0; i < CAlphabet::Length(); i++ )
		SwapLinks( Rand(), Rand() );
#endif
}
/*
void CWheel::InitLinks( symbol_t * buff )
{
	for ( register index_t i = 0; i < CAlphabet::Length(); i++ )
		_symbol[ i ] = buff[ i ];
}

void CWheel::GenLinks( symbol_t * buff )
{
	InitLinks();

	for ( register index_t i = 0; i < CAlphabet::Length(); i++ )
		buff[ i ] = _symbol[ i ];
}
*/
bool CWheel::Rotate()
{
	++_counter %= CAlphabet::Length();

	return ( bool )( _counter == 0 );
}

symbol_t CWheel::EncodeSymbol( const symbol_t s ) const
{
	index_t index = CAlphabet::GetIndex( s );
	index = MakeIndex( index );
	return _symbol[ index ];
}

symbol_t CWheel::DecodeSymbol( const symbol_t s ) const
{
	const index_t index = SearchSymbol( s ) % CAlphabet::Length();
	return CAlphabet::GetSymbol( index );
}


int CWheel::Rand()
{
	return ( rand() % CAlphabet::Length() );
}

index_t CWheel::MakeIndex( index_t index ) const
{
	index -= _counter;

	if ( index < 0 )
		index += CAlphabet::Length();

	return ( index );
}

void CWheel::SwapLinks( const index_t i, const index_t j )
{
	symbol_t temp = _symbol[ i ];
	_symbol[ i ] = _symbol[ j ];
	_symbol[ j ] = temp;
}

index_t CWheel::SearchSymbol( const symbol_t s ) const
{
	bool found = false;
	register index_t i;
	for ( i = 0; !found && i < CAlphabet::Length(); i++ )
		if ( _symbol[ i ] == s )
			found = true;

	index_t index = --i;
	index += _counter;

	if ( index >= CAlphabet::Length() )
		index -= CAlphabet::Length();

	return ( index );
}
/*
void CWheel::PrintLinks() const
{
	for ( register index_t i = 0; i < CAlphabet::Length(); i++ )
		cout << _symbol[ i ];
	cout << endl;
}
*/