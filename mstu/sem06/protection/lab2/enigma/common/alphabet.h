#pragma once


#include "types.h"



//#define OWN_ALPHABET



class CAlphabet
{
private:
#ifdef OWN_ALPHABET
	static index_t _len;
#else
	static const index_t _len;
	static symbol_t _symbol[];
#endif

public:
	CAlphabet();

	static void Init();
	static char GetSymbol( const index_t index );
	static index_t GetIndex( const symbol_t s );
	static index_t Length() { return ( _len ); };
//	static void Print();

#ifdef OWN_ALPHABET
private:
	static void SortSymbols() { QuickSort( _symbol, 0, _len - 1 ); };
	static index_t SearchSymbol( const symbol_t s ) { return BinarySearch( s, _symbol, _len ); };

	static void QuickSort( symbol_t * const a, const index_t low, const index_t high );
	static index_t BinarySearch( const symbol_t s, symbol_t * const a, const index_t n );
#endif
};