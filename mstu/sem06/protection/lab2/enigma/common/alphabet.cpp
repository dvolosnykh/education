#include "alphabet.h"

#include <limits.h>


#ifdef OWN_ALPHABET
symbol_t CAlphabet::_symbol[] = "`1234567890-=~!@#$%^&*()_+ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz{}:\"<>?[];',./\\|\t\n\b\a";
index_t CAlphabet::_len = sizeof( CAlphabet::_symbol ) / sizeof( CAlphabet::_symbol[ 0 ] );
#else
symbol_t CAlphabet::_symbol[ UCHAR_MAX + 1 ];
const index_t CAlphabet::_len = UCHAR_MAX + 1;
#endif



static CAlphabet abc;



CAlphabet::CAlphabet()
{
#ifdef OWN_ALPHABET
	SortSymbols();
#else
	Init();
#endif
}

void CAlphabet::Init()
{
	for ( register index_t i = 0; i < _len; i++ )
		_symbol[ i ] = ( symbol_t )( i );
}

char CAlphabet::GetSymbol( const index_t index )
{
#ifdef OWN_ALPHABET
	return _symbol[ index ];
#else
	return ( symbol_t )( index );
#endif
}

index_t CAlphabet::GetIndex( const symbol_t s )
{
#ifdef OWN_ALPHABET
	return SearchSymbol( s );
#else
	return ( index_t )( s );
#endif
}

#ifdef OWN_ALPHABET
void CAlphabet::QuickSort( symbol_t * const a, const index_t low, const index_t high )
{
	register index_t mid = low + high >> 1;

	for ( ; ; )
	{
		register index_t i, j;
		for ( i = low; a[ i ] <= a[ mid ] && i < mid; i++ );
		for ( j = high; a[ j ] >= a[ mid ] && j > mid; j-- );

	if ( i == mid && j == mid ) break;

		symbol_t temp = a[ i ];
		a[ i ] = a[ j ];
		a[ j ] = temp;

		if ( i == mid )
			mid++;
		else if ( j == mid )
			mid--;
	}

	if ( mid > low + 1 )
		QuickSort( a, low, mid - 1 );

	if ( mid < high - 1 )
		QuickSort( a, mid + 1, high );
}

index_t CAlphabet::BinarySearch( const symbol_t s, symbol_t * const a, const index_t n )
{
	register index_t low = 0;
	register index_t high = n - 1;

	register index_t index;
	for ( bool found = false; !found && high >= low; )
	{
		index = low + high >> 1;

		if ( s == a[ index ] )
			found = true;
		else if ( s > a[ index ] )
			low = index + 1;
		else
			high = index - 1;
	}

	return ( index );
}
#endif
/*
void CAlphabet::Print()
{
	cout << "Alphabet: " << endl;
	for ( register index_t i = 0; i < _len; i++ )
		cout << _symbol[ i ];
	cout << endl;
}
*/