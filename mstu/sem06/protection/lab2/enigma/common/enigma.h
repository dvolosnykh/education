#pragma once



#include <limits.h>

#include "wheel.h"
#include "types.h"



#define ROTATE_WHEELS



class CEnigma
{
private:
	enum { WHEELS_NUM = 3 };

	CWheel _wheel[ WHEELS_NUM ];

public:
	enum { MAX_TEXT_LEN = UCHAR_MAX };
	void InitLinks( const index_t init_value );
//	void InitLinks( char * keyfile );
//	void GenLinks( char * keyfile );

	void EncodeFile( char * in, char * out );
	void DecodeFile( char * in, char * out );
	void EncodeText( symbol_t * in, symbol_t * out );
	void DecodeText( symbol_t * in, symbol_t * out );
	symbol_t EncodeSymbol( symbol_t s );
	symbol_t DecodeSymbol( symbol_t s );
//	void PrintLinks() const;

private:
	void ResetWheels();
	void RotateWheels();
};