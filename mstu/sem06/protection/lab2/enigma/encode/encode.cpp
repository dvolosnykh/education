#include <tchar.h>
#include <stdlib.h>

#include "enigma.h"


int main( int argc, char * argv[] )
{
	const index_t init_value = atoi( argv[ 1 ] );
	char * const src = argv[ 2 ];
	char * const dest = argv[ 3 ];

	CEnigma machine;
	machine.InitLinks( init_value );
	machine.EncodeFile( src, dest );

	return ( 0 );
}