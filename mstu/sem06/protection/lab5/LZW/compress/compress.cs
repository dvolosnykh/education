using System;
using System.IO;
using LZW;


namespace LZW.Compress
{
	class ClassMain
	{
		[STAThread]
		static void Main( string[] args )
		{
			InputStream src = new InputStream( args[ 0 ] );
			OutputStream dest = new OutputStream( args[ 1 ] );

			while ( Action.Do( dest, src ) );

			src.Close();
			dest.Close();

//			Console.ReadLine();
		}
	}


	class Action
	{
		public static bool Do( OutputStream dest, InputStream src )
		{
			Table table = new Table();
			Byte b = new Byte();

			bool filled = false;

			if ( src.ReadBlock( b ) )
			{
				table.Add( new Line( b.Value, -1 ) );

				int i;
				for ( i = 0; !filled && src.ReadBlock( b ); )
				{
					table[ i ].Suffix.Value = b.Value;

					int index = table.IndexOf( table[ i ] );

					if ( index < i )
					{
						table[ i ].Prefix.Value = index + Table.RootSymbolsNum;
						table[ i ].Suffix.Value = -1;
					}
					else
					{
						dest.WriteBlock( table[ i ].Prefix );

						int add_index = table.Add( new Line( table[ i ].Suffix, -1 ) );
						filled = add_index < 0;

						i++;
					}
				}

				dest.WriteBlock( table[ i ].Prefix );
			}

//			table.Print();

			return ( filled );
		}
	}
}
