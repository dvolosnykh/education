using System;
using System.IO;
using System.Collections;
using LZW;


namespace LZW.Decompress
{
	class ClassMain
	{
		[STAThread]
		static void Main( string[] args )
		{
			InputStream src = new InputStream( args[ 0 ] );
			OutputStream dest = new OutputStream( args[ 1 ] );

			while ( Action.Do( dest, src ) );

			src.Close();
			dest.Close();

//			Console.ReadLine();
		}
	}


	class Action
	{
		public static bool Do( OutputStream dest, InputStream src )
		{
			Table table = new Table();
			Field f = new Field();

			bool filled = false;

			if ( src.ReadBlock( f ) )
			{
				table.Add( new Line( f.Value, -1 ) );
				dest.WriteBlock( new Byte( f.Value ) );

				int i;
				for ( i = 0; !filled && src.ReadBlock( f ); i++ )
				{
					table[ i ].Suffix.Value = f.Value;

					int add_index = table.Add( new Line( table[ i ].Suffix.Value, -1 ) );
					filled = add_index < 0;

					if ( table[ i ].Suffix.Value < Table.RootSymbolsNum )
					{
						dest.WriteBlock( new Byte( table[ i ].Suffix.Value ) );
					}
					else
					{
						int index = table[ i ].Suffix.Value - Table.RootSymbolsNum;

						do
						{
							int sub_index = table[ i ].Suffix.Value - Table.RootSymbolsNum;
							table[ i ].Suffix.Value = table[ sub_index ].Prefix.Value;
						}
						while ( table[ i ].Suffix.Value >= Table.RootSymbolsNum );

						// output
						ArrayList bytes = new ArrayList();

						while ( index >= 0 )
						{
							bytes.Insert( 0, new Byte( table[ index ].Suffix.Value ) );

							if ( table[ index ].Prefix.Value < Table.RootSymbolsNum )
								bytes.Insert( 0, new Byte ( table[ index ].Prefix.Value ) );

							index = table[ index ].Prefix.Value - Table.RootSymbolsNum;
						}

						for ( int j = 0; j < bytes.Count; j++ )
							dest.WriteBlock( bytes[ j ] as Byte );

						bytes.Clear();
					}
				}
			}

//			table.Print();

			return ( filled );
		}
	}
}
