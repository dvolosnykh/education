using System;
using System.IO;


namespace LZW
{
	public abstract class MyStream : FileStream
	{
		protected int _bc = 0;
		protected Byte _byte = new Byte();


		public MyStream( string path, FileMode mode )
			: base( path, mode ) {}


		public override long Length
		{
			get { return ( base.Length << 3 ); }
		}

		public override long Position
		{
			get
			{
				long pos = base.Position;

				if ( pos > 0 )
				{
					if ( _bc > 0 ) pos--;
					pos <<= 3;
				}

				pos += _bc;

				return ( pos );
			}
		}
	}

	public class InputStream : MyStream
	{
		public InputStream( string path )
			: base( path, FileMode.Open ) {}


		public bool ReadBlock( Block block )
		{
			int i;
			for ( i = 0; i < block.BitsNum && Position < Length; i++ )
			{
				if ( _bc == 0 ) _byte.Value = ReadByte();

				if ( _byte.Value >= 0 )
				{
					block[ i ] = _byte[ _bc++ ];
					if ( _bc == _byte.BitsNum ) _bc = 0;
				}
			}

			return ( i == block.BitsNum );
		}
	}

	public class OutputStream : MyStream
	{
		public OutputStream( string path )
			: base( path, FileMode.Create ) {}


		public void WriteBlock( Block block )
		{
//			if ( block.Value < Table.RootSymbolsNum )
//				Console.Write( "{0}\t", ( char )block.Value );
//			else
//				Console.Write( "{0}\t", block.Value );

			for ( int i = 0; i < block.BitsNum; i++ )
			{
				_byte[ _bc++ ] = block[ i ];
				if ( _bc == _byte.BitsNum ) _bc = 0;
				if ( _bc == 0 )
				{
					WriteByte( ( byte )_byte.Value );
					_byte.Value = 0;
				}
			}
		}

		public override void Close()
		{
			if ( _bc != 0 )
				WriteByte( ( byte )_byte.Value );
			base.Close();
		}

	}
}
