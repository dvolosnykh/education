using System;
using System.IO;


namespace LZW
{
	public abstract class Block
	{
		protected int _bitsNum;
		protected int _value;


		public Block( int bitsNum, int initValue )
		{
			_bitsNum = bitsNum;
			_value = initValue;
		}


		private int BitMask( int index )
		{
			return ( 0x01 << index );
		}

		public bool this [ int index ]
		{
			get
			{
				int bit = BitMask( index );

				return ( _value & bit ) != 0;
			}
			set
			{
				int bit = BitMask( index );

				if ( value )
					_value |= bit;
				else
					_value &= ~bit;
			}
		}

		public int Value
		{
			get { return ( _value ); }
			set { _value = value; }
		}

		public int BitsNum
		{
			get { return ( _bitsNum ); }
		}


		public static bool operator == ( Block op1, Block op2 )
		{
			return ( op1.Value == op2.Value );
		}

		public static bool operator != ( Block op1, Block op2 )
		{
			return !( op1 == op2 );
		}
	}

	public class Byte : Block
	{
		public Byte( int initValue ) : base( 8, initValue ) {}
		public Byte() : this( 0 ) {}
	}

	public class Field : Block
	{
		protected static int _max = 0x1FF;


		public Field( int initValue ) : base( 9, initValue ) {}
		public Field() : this( 0 ) {}


		public static int Max
		{
			get { return ( _max ); }
		}
	}
}
