using System;
using System.IO;
using System.Collections;


namespace LZW
{
	public class Line
	{
		private Field _prefix = new Field();
		private Field _suffix = new Field();


		public Line( int prefix, int suffix )
		{
			_prefix.Value = prefix;
			_suffix.Value = suffix;
		}

		public Line( Field prefix, Field suffix )
		{
			_prefix.Value = prefix.Value;
			_suffix.Value = suffix.Value;
		}

		public Line( int prefix, Field suffix )
		{
			_prefix.Value = prefix;
			_suffix.Value = suffix.Value;
		}

		public Line( Field prefix, int suffix )
		{
			_prefix.Value = prefix.Value;
			_suffix.Value = suffix;
		}

		public Line( Line line )
		{
			_prefix.Value = line.Prefix.Value;
			_suffix.Value = line.Suffix.Value;
		}


		public LZW.Field Prefix
		{
			get { return ( _prefix ); }
			set { _prefix = value; }
		}

		public LZW.Field Suffix
		{
			get { return ( _suffix ); }
			set { _suffix = value; }
		}

		public static bool operator == ( Line op1, Line op2 )
		{
			return
			(
				op1.Prefix == op2.Prefix &&
				op1.Suffix == op2.Suffix
			);
		}

		public static bool operator != ( Line op1, Line op2 )
		{
			return !( op1 == op2 );
		}
	}


	public class Table : ArrayList
	{
		private static int _rootSymbolsNum = 0x100;


		public Table() {}


		public void Print()
		{
			if ( Count > 0 )
			{
				for ( int i = 0; i < Count; i++ )
				{
					Console.Write( "\n{0}:", _rootSymbolsNum + i );

					Field prefix = this[ i ].Prefix;
					if ( prefix.Value < _rootSymbolsNum )
						Console.Write( "\t{0}", ( char )prefix.Value );
					else
						Console.Write( "\t{0}", prefix.Value );

					Field suffix = this[ i ].Suffix;
					if ( suffix.Value < _rootSymbolsNum )
						Console.Write( "\t{0}", ( char )suffix.Value );
					else
						Console.Write( "\t{0}", suffix.Value );
				}

				Console.WriteLine();
			}
			else
			{
				Console.WriteLine( "< empty >" );
			}
		}


		public static int RootSymbolsNum
		{
			get { return ( _rootSymbolsNum ); }
		}


		public new Line this[ int index ]
		{
			get { return ( base[ index ] as Line ); }
			set { base[ index ] = value; }
		}

		public override int IndexOf( object value )
		{
			Line line = value as Line;

			int index = 0;
			for ( ; index < Count && this[ index ] != line; index++ );

			if ( index >= Count ) index = -1;

			return ( index );
		}

		public override int Add( object value )
		{
			Line temp = value as Line;
			Line line = new Line( temp.Prefix.Value, temp.Suffix.Value );

			int index = base.Add( line );

			if ( Count == Field.Max - _rootSymbolsNum + 1 )
				index = -1;

			return ( index );
		}
	}
}
