using System;
using DES;


namespace Decipher
{
	class MainClass
	{
		[STAThread]
		static void Main( string[] args )
		{
			string src = args[ 0 ];
			string dest = args[ 1 ];
			string key = args[ 2 ];

			DES.Decipher.Do( dest, src, key );
		}
	}
}
