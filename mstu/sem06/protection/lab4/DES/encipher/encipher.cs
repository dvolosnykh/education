using System;
using DES;


namespace Encipher
{
	class MainClass
	{
		[STAThread]
		static void Main( string[] args )
		{
			string src = args[ 0 ];
			string dest = args[ 1 ];
			string key = args[ 2 ];

			DES.Encipher.Do( dest, src, key );
		}
	}
}
