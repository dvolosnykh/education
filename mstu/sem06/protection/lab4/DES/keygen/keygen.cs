using System;
using System.IO;


namespace keygen
{
	class MainClass
	{
		[STAThread]
		static void Main( string[] args )
		{
			string dest = args[ 0 ];
			BinaryWriter bw = new BinaryWriter( File.Open( dest, FileMode.Create ) );

			Random rand = new Random();
			long rand_value = rand.Next();

			bw.Write( rand_value );
		}
	}
}
