using System;
using System.IO;


namespace DES
{
	public class Action
	{
		protected static Key64 ReadKey( string key_file )
		{
			BinaryReader br = new BinaryReader( File.Open( key_file, FileMode.Open ) ); 
			return new Key64( br.ReadUInt64() );
		}

		protected static ulong ReadBlock( Stream stream, out ulong block )
		{
			ulong count = 0;
			for ( block = 0; count < 8; count++ )
			{
				int c = stream.ReadByte();

			if ( c == -1 ) break;

				block <<= 8;
				block |= ( byte )c;
			}

			return ( count );
		}

		protected static void WriteBlock( Stream stream, ulong block, long num )
		{
			for ( long i = num - 1; i >= 0; i-- )
			{
				int shift = ( int )( 8 * i );
				ulong mask = ( ulong )0xFF << shift;
				ulong val = ( block & mask ) >> shift;
				stream.WriteByte( ( byte )val );
			}
		}
	}

	public class Encipher : Action
	{
		private static ulong GetLength( FileStream fs )
		{
			return ( ulong )fs.Length;
		}

		public static void Do( string dest, string src, string key_file )
		{
			Key64 key = ReadKey( key_file );

			FileStream fsDest = new FileStream( dest, FileMode.Create );
			FileStream fsSrc = new FileStream( src, FileMode.Open );

			ulong length = GetLength( fsSrc );
			WriteBlock( fsDest, length, 8 );

			BlockLR block = new BlockLR();
			for ( ; ; )
			{
				ulong val;
			if ( ReadBlock( fsSrc, out val ) == 0 ) break;
				block.Value = val;

				block.Encipher( key );
				WriteBlock( fsDest, block.Value, 8 );
			}

			fsSrc.Close();
			fsDest.Close();
		}
	}

	public class Decipher : Action
	{
		private static ulong GetLength( Stream stream )
		{
			ulong length;
			ReadBlock( stream, out length );

			return ( length );
		}

		public static void Do( string dest, string src, string key_file )
		{
			Key64 key = ReadKey( key_file );

			FileStream fsDest = new FileStream( dest, FileMode.Create );
			FileStream fsSrc = new FileStream( src, FileMode.Open );

			ulong length = GetLength( fsSrc );

			bool quit = false;
			BlockLR block = new BlockLR();
			ulong val;
			for ( long num = 8; !quit && ReadBlock( fsSrc, out val ) != 0; )
			{
				block.Value = val;

				if ( fsSrc.Position == fsSrc.Length )
				{
					num = ( long )( length % 8 );
					if ( num == 0 ) num = 8;

					quit = true;
				}

				block.Decipher( key );
				WriteBlock( fsDest, block.Value, num );
			}

			fsSrc.Close();
			fsDest.Close();
		}
	}



	public abstract class Block : ICloneable
	{
		protected byte _bitsNum;
		protected ulong _value;


		public Block( byte bitsNum, ulong initValue )
		{
			_bitsNum = bitsNum;
			_value = initValue;
		}


		private ulong BitMask( int index )
		{
			return ( ulong )0x01 << _bitsNum - index;
		}

		public bool this [ int index ]
		{
			get
			{
				ulong bit = BitMask( index );

				return ( _value & bit ) != 0;
			}
			set
			{
				ulong bit = BitMask( index );

				if ( value )
					_value |= bit;
				else
					_value &= ~bit;
			}
		}

		public ulong Value
		{
			get { return ( _value ); }
			set { _value = value; }
		}

		public byte BitsNum
		{
			get { return ( _bitsNum ); }
		}


		public object Clone()
		{
			return this.MemberwiseClone();
		}
	}

	public class Block4 : Block
	{
		public Block4( ulong initValue ) : base( 4, initValue ) {}
		public Block4() : this( 0 ) {}
	}

	public class Block6 : Block
	{
		public Block6( ulong initValue ) : base( 6, initValue ) {}
		public Block6() : this( 0 ) {}
	}

	public class Block28 : Block
	{
		public Block28( ulong initValue ) : base( 28, initValue ) {}
		public Block28() : this( 0 ) {}

		public void Rotate( int num )
		{
			_value = ( _value << num ) | ( _value >> _bitsNum - num );
			_value &= 0x000FFFFFFF;
		}
	}

	public class Block32 : Block
	{
		public Block32( ulong initValue ) : base( 32, initValue ) {}
		public Block32() : this( 0 ) {}

		public Block32( BlockS[] ss ) : base( 32, 0 )
		{
			_value = 0;
			for ( int i = 0; i < ss.Length; i++ )
			{
				_value <<= 4;
				_value |= ss[ i ].Value;
			}
		}

		#region The Choice Function f

		public Block32 F( Key48 K )
		{
			Block48 BB = E() ^ ( Block48 )K;

//			Console.WriteLine( "BB:\t0x{0:x}", BB.Value );

			BlockB[] bb = new BlockB[ 8 ];
			for ( int i = 0; i < bb.Length; i++ )
				bb[ i ] = BB.Get( i + 1 );

			Block32 SS = S_selection( bb );

//			Console.WriteLine( "SS:\t0x{0:x}", SS.Value );

			SS.P();

//			Console.WriteLine( "SS.P:\t0x{0:x}", SS.Value );

			return ( SS );
		}

		#region E bit-selection

		private static byte[] E_table =
		{
			32,  1,  2,  3,  4,  5,
			 4,  5,  6,  7,  8,  9,
			 8,  9, 10, 11, 12, 13,
			12, 13, 14, 15, 16, 17,
			16, 17, 18, 19, 20, 21,
			20, 21, 22, 23, 24, 25,
			24, 25, 26, 27, 28, 29,
			28, 29, 30, 31, 32,  1
		};

		private Block48 E()
		{
			Block48 E_R = new Block48();

			for ( int i = 0; i < E_table.Length; i++ )
				E_R[ i + 1 ] = this[ E_table[ i ] ];

			return ( E_R );
		}

		#endregion

		#region Selection functions S1, ..., S8

		private static byte[ , , ] S_table =
		{
			{	// S1
				{ 14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7 },
				{  0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8 },
				{  4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0 },
				{ 15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13 }
			},
			{	// S2
				{ 15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10 },
				{  3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5 },
				{  0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15 },
				{ 13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9 }
			},
			{	// S3
				{ 10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8 },
				{ 13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1 },
				{ 13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7 },
				{  1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12 }
			},
			{	// S4
				{  7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15 },
				{ 13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9 },
				{ 10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4 },
				{  3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14 }
			},
			{	// S5
				{  2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9 },
				{ 14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6 },
				{  4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14 },
				{ 11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3 }
			},
			{	// S6
				{ 12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11 },
				{ 10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8 },
				{  9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6 },
				{  4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13 }
			},
			{	// S7
				{  4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1 },
				{ 13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6 },
				{  1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2 },
				{  6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12 }
			},
			{	// S8
				{ 13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7 },
				{  1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2 },
				{  7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8 },
				{  2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11 }
			}
		};

		private BlockS S( int i, BlockB block )
		{
			return new BlockS( S_table[ i - 1, block.Row, block.Column ] );
		}

		private Block32 S_selection( BlockB[] bb )
		{
			BlockS[] ss = new BlockS [ bb.Length ];

			for ( int i = 0; i < bb.Length; i++ )
				ss[ i ] = S( i + 1, bb[ i ] );

			return new Block32( ss );
		}

		#endregion

		#endregion


		#region Primitive permutation function P

		private static byte[] P_table =
		{
			16,  7, 20, 12,
			29, 12, 28, 17,
			 1, 15, 23, 26,
			 5, 18, 31, 10,
			 2,  8, 24, 14,
			32, 27,  3,  9,
			19, 13, 30,  6,
			22, 11,  4, 25
		};

		public void P()
		{
			Block32 initial = ( Block32 )this.Clone();

			for ( int i = 0; i < P_table.Length; i++ )
				this[ i + 1 ] = initial[ P_table[ i ] ];
		}

		#endregion

		public static Block32 operator ^ ( Block32 op1, Block32 op2 )
		{
			return new Block32( op1.Value ^ op2.Value );
		}
	}

	public class Block48 : Block
	{
		public Block48( ulong initValue ) : base( 48, initValue ) {}
		public Block48() : this( 0 ) {}


		public BlockB Get( int i )
		{
			int shift = 6 * ( 8 - i );

			return new BlockB( ( _value >> shift ) & 0x3F );
		}

		public static Block48 operator ^ ( Block48 op1, Block48 op2 )
		{
			return new Block48( op1.Value ^ op2.Value );
		}
	}

	public class Block56 : Block
	{
		public Block56( ulong initValue ) : base( 56, initValue ) {}
		public Block56() : this( 0 ) {}
	}

	public class Block64 : Block
	{
		public Block64( ulong initValue ) : base( 64, initValue ) {}
		public Block64() : this( 0 ) {}
	}


	public class BlockS : Block4
	{
		public BlockS( ulong initValue ) : base( initValue ) {}
		public BlockS() : base() {}
	}

	public class BlockB : Block6
	{
		public BlockB( ulong initValue ) : base( initValue ) {}
		public BlockB() : base() {}


		public int Row
		{
			get
			{
				ulong bit6 = 0x01 & _value;
				ulong bit1 = ( 0x01 << 1 ) & ( _value >> 4 );

				return ( int )( bit1 | bit6 );
			}
		}

		public int Column
		{
			get { return ( int )( _value >> 1 ) & 0x0F; }
		}
	}


	public class Key48 : Block48
	{
		public Key48( ulong initValue ) : base( initValue ) {}
		public Key48() : base() {}
	}

	public class Key56 : Block56
	{
		public Key56( ulong initValue ) : base( initValue ) {}
		public Key56() : base() {}

		public Key56( Key64 key64 )
		{
			for ( int i = 1, j = 1; i <= key64.BitsNum; i++ )
			{
				if ( i % 8 != 0 )
				{
					this[ j ] = key64[ i ];
					j++;
				}
			}
		}

		public Key56( Block28 C, Block28 D )
		{
			_value = ( C.Value << 28 ) | D.Value;
		}

		#region Left shifts

		private static byte[] NumLS =
		{
			1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
		};

		public void LeftShifts( int i )
		{
			Block28 C = new Block28( _value >> 28 );
			Block28 D = new Block28( _value & 0x000FFFFFFF );

			byte ls = NumLS[ i - 1 ];
			C.Rotate( ls );
			D.Rotate( ls );

			_value = ( new Key56( C, D ) ).Value;
		}

		#endregion

		#region Permuted choice 2

		private static byte[] PC_2_table =
		{
			14, 17, 11, 24,  1,  5,
			 3, 28, 15,  6, 21, 10,
			23, 19, 12,  4, 26,  8,
			16,  7, 27, 20, 13,  2,
			41, 52, 31, 37, 47, 55,
			30, 40, 51, 45, 33, 48,
			44, 49, 39, 56, 34, 53,
			46, 42, 50, 36, 29, 32
		};

		public Key48 PC_2()
		{
			Key48 permuted = new Key48();

			for ( int i = 0; i < PC_2_table.Length; i++ )
				permuted[ i + 1 ] = this[ PC_2_table[ i ] ];

			return ( permuted );
		}

		#endregion
	}

	public class Key64 : Block64
	{
		public Key64( ulong initValue ) : base( initValue ) {}
		public Key64() : base() {}


		#region Permuted choice 1

		private static byte[] PC_1_table =
		{
			57, 49, 41, 33, 25, 17,  9,
			 1, 58, 50, 42, 34, 26, 18,
			10,  2, 59, 51, 43, 35, 27,
			19, 11,  3, 60, 52, 44, 36,

			63, 55, 47, 39, 31, 23, 15,
			 7, 62, 54, 46, 38, 30, 22,
			14,  6, 61, 53, 45, 37, 29,
			21, 13,  5, 28, 20, 12,  4
		};

		public Key56 PC_1()
		{
			Key56 permuted = new Key56();

			for ( int i = 0; i < PC_1_table.Length; i++ )
				permuted[ i + 1 ] = this[ PC_1_table[ i ] ];

			return ( permuted );
		}

		#endregion
	}



	public class BlockLR : Block64
	{
		public BlockLR( ulong initValue ) : base( initValue ) {}
		public BlockLR() : base() {}

		public BlockLR( Block32 L, Block32 R )
		{
			_value = ( L.Value << 32 ) | R.Value;
		}

		#region Cyphering

		public void Encipher( Key64 key )
		{
//			Console.WriteLine( "\n\n================= ENCIPHER =================\n" );

			Key48[] keys = new Key48[ 16 ];
			GenKeys( key, ref keys );

			IP();

			Block32 L = Left;
			Block32 R = Right;

//			Console.WriteLine( "L = 0x{0:x}\tR = 0x{1:x}", L.Value, R.Value );

			for ( int i = 0; i < keys.Length; i++ )
			{
//				Console.WriteLine( "\n------- Stage {0}: -------", i + 1 );

				Block32 tempL = ( Block32 )L.Clone();
				L = ( Block32 )R.Clone();
				R = tempL ^ R.F( keys[ i ] );

//				Console.WriteLine( "L = 0x{0:x}\tR = 0x{1:x}", L.Value, R.Value );
			}

			Left = L;
			Right = R;

			IP_1();
		}

		public void Decipher( Key64 key )
		{
//			Console.WriteLine( "\n\n================= DECIPHER =================\n" );

			Key48[] keys = new Key48[ 16 ];
			GenKeys( key, ref keys );

			IP();

			Block32 L = Left;
			Block32 R = Right;

//			Console.WriteLine( "L = 0x{0:x}\tR = 0x{1:x}", L.Value, R.Value );

			for ( int i = keys.Length - 1; i >= 0; i-- )
			{
//				Console.WriteLine( "\n------- Stage {0}: -------", i + 1 );

				Block32 tempR = ( Block32 )R.Clone();
				R = ( Block32 )L.Clone();
				L = tempR ^ L.F( keys[ i ] );

//				Console.WriteLine( "L = 0x{0:x}\tR = 0x{1:x}", L.Value, R.Value );
			}

			Left = L;
			Right = R;

			IP_1();
		}

		private void GenKeys( Key64 src, ref Key48[] dest )
		{
			Key56 pKey = src.PC_1();

			for ( int i = 0; i < dest.Length; i++ )
			{
				pKey.LeftShifts( i + 1 );
				dest[ i ] = pKey.PC_2();
			}
		}

		#endregion

		#region Initial and inverse permutations

		private static byte[] IP_table =
		{
			58, 50, 42, 34, 26, 18, 10, 2,
			60, 52, 44, 36, 28, 20, 12, 4,
			62, 54, 46, 38, 30, 22, 14, 6,
			64, 56, 48, 40, 32, 24, 16, 8,
			57, 49, 41, 33, 25, 17,  9, 1,
			59, 51, 43, 35, 27, 19, 11, 3,
			61, 53, 45, 37, 29, 21, 13, 5,
			63, 55, 47, 39, 31, 23, 15, 7
		};

		private void IP()
		{
			BlockLR initial = ( BlockLR )this.Clone();

			for ( int i = 0; i < IP_table.Length; i++ )
				this[ i + 1 ] = initial[ IP_table[ i ] ];
		}

		private void IP_1()
		{
			BlockLR initial = ( BlockLR )this.Clone();

			for ( int i = 0; i < IP_table.Length; i++ )
				this[ IP_table[ i ] ] = initial[ i + 1 ];
		}

		#endregion

		public Block32 Right
		{
			get
			{
				return new Block32( _value & 0x00000000FFFFFFFF );
			}
			set
			{
				_value &= 0xFFFFFFFF00000000;
				_value |= value.Value;
			}
		}

		public Block32 Left
		{
			get
			{
				return new Block32( _value >> 32 );
			}
			set
			{
				_value &= 0x00000000FFFFFFFF;
				_value |= value.Value << 32;
			}
		}
	}
}
