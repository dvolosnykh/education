function lab3
    omega = 1.8; eps = 1e-3;
    N = 50;
    a = 1; b = 1;
    
    u1 = PoissonEquation( [ 0 a ], [ 0 b ], N, omega, eps );
    u2 = PoissonEquation( [ 0 a ], [ 0 b ], 2 * N, omega, eps );
    delta = u2( 1 : 2 : 2 * N + 1, 1 : 2 : 2 * N + 1 ) - u1;
    
    [ Inf, 0 : b / N : b;
    ( 0 : a / N : a )', u1 ]
    
    [ Inf, 0 : b / N : b;
    ( 0 : a / N : a )', delta ]
end

function u = PoissonEquation( xBounds, yBounds, N, omega, eps )
    xStep = ( xBounds( 2 ) - xBounds( 1 ) ) / N;
    yStep = ( yBounds( 2 ) - yBounds( 1 ) ) / N;
    alpha = ( omega * ( xStep * yStep ) ^ 2 ) / ( 2 * ( xStep ^ 2 + yStep ^ 2 ) );
    
    limit = eps * ( 2 - omega ) / ( 2 * ( omega - 1 ) );
    
    xPivots = xBounds( 1 ) : xStep : xBounds( 2 );
    yPivots = yBounds( 1 ) : yStep : yBounds( 2 );
    
    f = Precalc_f( xPivots, yPivots );

    u = zeros( N + 1 );
    u( :, 1 ) = Bottom( xPivots );
    u( :, N + 1 ) = Top( xPivots );
    u( 1, : ) = Left( yPivots )';
    u( N + 1, : ) = Right( yPivots )';

    stop = false;
    iterationsCount = 0;
    while ( ~stop )
        uOld = u;
        
        for i = 2 : N
            for j = 2 : N
                u( i, j ) = alpha * ( ...
                        ( u( i - 1, j ) + u( i + 1, j ) ) / xStep ^ 2 + ...
                        ( u( i, j - 1 ) + u( i, j + 1 ) ) / yStep ^ 2 + ...
                        f( i, j ) ...
                    ) + ( 1 - omega ) * u( i, j );
            end
        end

        iterationsCount = iterationsCount + 1;
        stop = all( all( abs( u - uOld ) < limit ) );
    end
    
    iterationsCount
    surf( xPivots, yPivots, u );
    rotate3d on
    pause;
end

function left = Left( y )
    left = y - y .^ 2;
end

function right = Right( y )
    right = ( y  - y .^ 2 ) .* exp( y );
end

function bottom = Bottom( x )
    bottom = x - x .^ 2;
end

function top = Top( x )
    top = x - x .^ 2;
end

function f = Precalc_f( xPivots, yPivots )
    f = zeros( length( xPivots ), length( yPivots ) );
    for i = 1 : length( xPivots )
        x = xPivots( i );
        for j = 1 : length( yPivots )
            y = yPivots( j );
            f( i, j ) = x .^ 2 - y .^ 2 - x + y;
        end
    end
end