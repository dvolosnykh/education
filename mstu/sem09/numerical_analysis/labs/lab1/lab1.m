function lab1
    HeatingRod( 1, 1, 2 );
end

function HeatingRod( length, linearDensity, linearSpecificHeatCapacity )
    stepsCount = 10;
    dl = length / stepsCount;
    
    layersCount = 20;
    dt = 0.05;
    stopTime = dt * layersCount;
    
    alpha = dt / ( linearSpecificHeatCapacity * linearDensity * dl ^ 2 );
    
    temperature = zeros( layersCount + 1, stepsCount + 1 );
    temperature( 1, : ) = InitialTemperature( 0 : dl : length );

    for layer = 2 : layersCount + 1
        time = dt * ( layer - 1 );
        
        temperature( layer, : ) = temperature( layer - 1, : );
        for dummy = 1 : 3
            conductivity = HeatConductivity( 0.5, 2, 1.25, temperature( layer, : ) );
            averageConductivity = filter( [ 1 1 ], 2, conductivity );

            A = zeros( stepsCount + 1 );
            B = temperature( layer - 1, : )';

            A( 1, 1 : 2 ) = [ -1 1 ];
            B( 1 ) = dl * HeatFlowLeft( time ) / averageConductivity( 2 );

            A( stepsCount + 1, stepsCount : stepsCount + 1 ) = [ -1 1 ];
            B( stepsCount + 1 ) = dl * HeatFlowRight( time ) / averageConductivity( stepsCount + 1 );

            for row = 2 : stepsCount
                A( row, row - 1 : row + 1 ) = [
                    -alpha * averageConductivity( row ), ...
                    alpha * sum( averageConductivity( row : row + 1 ) ) + 1, ...
                    -alpha * averageConductivity( row + 1 )
                ];
            end

            temperature( layer, : ) = ( A \ B )';
        end
    end
    
    middle = floor( stepsCount / 2 );
    [ temperatureMiddleMax, layerMax ] = max( temperature( :, middle ) );
    temperatureMiddleMax
    timeMax = dt * ( layerMax - 1 )

    temperature
    surf( 0 : dl : length, 0 : dt : stopTime, temperature );
    rotate3d on
end

function temperature = InitialTemperature( x )
    temperature = 0.2 * ones( size( x ) );
end

function conductivity = HeatConductivity( a, b, sigma, temperature )
    conductivity = a + b * temperature .^ sigma;
end

function flow = HeatFlowLeft( time )
    flow = 0;
end

function flow = HeatFlowRight( time )
    t0 = 0.5;
    Q = 10;
    
    if ( time < t0 )
        flow = Q;
    else
        flow = 0;
    end
end
