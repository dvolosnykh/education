function lab2
    StringOscillation( 1, 1 );
end

function StringOscillation( length, a )
    layersCount = 100;
    dt = 0.01;
    stopTime = dt * layersCount;
    
    dl = 0.02;
    stepsCount = length / dl;
    
    alpha = ( a * dt / dl ) ^ 2;
    
    deviation = zeros( layersCount + 1, stepsCount + 1 );
    deviation( 1, : ) = InitialDeviation( 0 : dl : length );
    
    deviation( 2, 1 ) = TrajectoryLeft( dt );
    f = circshift( filter( [ 1 -2 1 ], 1, deviation( 1, : ) ), [ 0, -1 ] );
    deviation( 2, 2 : stepsCount ) = deviation( 1, 2 : stepsCount ) + 2 * dt * sin( dt ) + ( alpha / 2 ) * f( 2 : stepsCount );
    deviation( 2, stepsCount + 1 ) = TrajectoryRight( dt );

    for layer = 3 : layersCount + 1
        time = dt * ( layer - 1 );

        deviation( layer, 1 ) = TrajectoryLeft( time );
        f = circshift( filter( [ 1 -2 1 ], 1, deviation( layer - 1, : ) ), [ 0, -1 ] );
        deviation( layer, 2 : stepsCount ) = 2 * deviation( layer - 1, 2 : stepsCount ) - deviation( layer - 2, 2 : stepsCount ) + alpha * f( 2 : stepsCount );
        deviation( layer, stepsCount + 1 ) = TrajectoryRight( time );
    end
    
    [ ( 0 : dt : stopTime )',deviation ]
    surf( 0 : dl : length, 0 : dt : stopTime, deviation );
    rotate3d on
end

function deviation = InitialDeviation( x )
    deviation = x .* ( x + 1 );
end

function deviation = TrajectoryLeft( time )
    deviation = 0;
end

function deviation = TrajectoryRight( time )
    deviation = 2 - time;
end