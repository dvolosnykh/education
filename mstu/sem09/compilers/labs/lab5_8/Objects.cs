﻿using System.Collections.Generic;

namespace Ada95
{
	public class Object
	{
		public string ID;
		public string ValueType;

		public Object( string id, string type )
		{
			ID = id;
			ValueType = type;
		}
	}

	public class ArrayObject : Object
	{
		public List< Range > Dimensions;
		public string ComponentType;

		public ArrayObject( string id, List< Range > dimensions, string componentType )
			: base( id, "array" )
		{
			Dimensions = dimensions;
			ComponentType = componentType;
		}
	}

	public class Range
	{
		public string Low;
		public string High;

		public Range( string low, string high )
		{
			Low = low;
			High = high;
		}
	}
}