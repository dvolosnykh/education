grammar Ada95;

options
{
	language = CSharp2;
	memoize = true;
	output = AST;
	ASTLabelType = CommonTree;
}

tokens
{
	ORDINARY_FIXED;
	DECIMAL_FIXED;
	ID_LIST;
	ARRAY_OBJECT;
	UNCONSTRAINED_ARRAY;
	CONSTRAINED_ARRAY;
	ENUMERATION;
	REF;
	ITEM;
	BODY;
	BLOCK;
	SIMPLE_STATEMENT;
	COMPOUND_STATEMENT;
	EXPLICIT_DEREFERENCE;
	INDEXED_COMPONENT;
	SLICE;
	SELECTED_COMPONENT;
	OBJECT;
	ATTRIBUTE_REFERENCE;
	COMPONENT;
	PARAMETER;
	DISCRIMINANT_SPECIFICATION;
	QUALIFIED_BY_EXPRESSION;
	QUALIFIED_BY_AGREGATE;
	COMPONENT_LIST;
	EXPRESSION;
	DISCRETE_RANGE;
	IN_RANGE;
	IN_SUBTYPE;
	LABEL;
}

scope SymbolTable
{
	SortedSet< string > Types;
	SortedSet< string > Functions;
	SortedSet< string > Procedures;
	SortedSet< string > Objects;
}
scope Symbols
{
	SortedSet< string > IDs;
}

@parser::namespace { Ada95 }
@parser::header
{
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Text;
}
@parser::members
{
	// symbols related stuff.
	private bool IsType( string id )
	{
		bool found = false;
		for ( int i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Types.Contains( id ) );

		return ( found );
	}
	
	private bool IsFunction( string id )
	{
		bool found = false;
		for ( int i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Functions.Contains( id ) );

		return ( found );
	}
	
	private bool IsProcedure( string id )
	{
		bool found = false;
		for ( int i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Procedures.Contains( id ) );

		return ( found );
	}
	
	private bool IsObject( string id )
	{
		bool found = false;
		for ( int i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Objects.Contains( id ) );
			
		return ( found );
	}
		
	// errors related stuff.
	public StringBuilder Errors = new StringBuilder();
		
	public override void EmitErrorMessage( string message )
	{
		Errors.AppendLine( message );
	}
}

@lexer::namespace { Ada95 }

public program
	scope SymbolTable;
	@init
	{
		$SymbolTable::Types = new SortedSet< string >();
		$SymbolTable::Functions = new SortedSet< string >();
		$SymbolTable::Procedures = new SortedSet< string >();
		$SymbolTable::Objects = new SortedSet< string >();
	}
	:	declarative_part ;

basic_declaration
	:
		type_declaration |
		//subtype_declaration |
		object_declaration |
		number_declaration |
		subprogram_declaration
	;

defining_identifier
	:	IDENTIFIER ;

type_declaration
	:	full_type_declaration ;

full_type_declaration
	:
		TYPE^ id = defining_identifier /*known_discriminant_part?*/ IS! type_definition SEMICOLON!
		{
		 	// TODO: discriminant
		 	$SymbolTable::Types.Add( $id.text );
		}
	;

type_definition
	:
		enumeration_type_definition |
		integer_type_definition |
		real_type_definition |
		array_type_definition |
		record_type_definition |
		//access_type_definition |
		derived_type_definition
	;

subtype_declaration
	:	SUBTYPE^ defining_identifier IS! subtype_indication SEMICOLON! ;

subtype_mark
	:	IDENTIFIER ;

subtype_indication
	:	subtype_mark^ constraint? ;

constraint
	:
		scalar_constraint //|
		//composite_constraint
	;

scalar_constraint
	:
		range_constraint |
		digits_constraint 
	;
/*
composite_constraint :
	index_constraint |
	discriminant_constraint ;
*/
number_declaration
	:	defining_identifier_list COLON! CONSTANT^ ASSIGN! expression SEMICOLON! ;

derived_type_definition
	:	ABSTRACT? NEW^ subtype_indication record_extension_part? ;

object_declaration
	scope Symbols;
	@init
	{
		$Symbols::IDs = new SortedSet< string >();
	}
	:
		defining_identifier_list COLON ALIASED? CONSTANT?
		(
			subtype_indication ( ASSIGN expression )?		-> ^( OBJECT defining_identifier_list subtype_indication expression? ) |
			array_type_definition ( ASSIGN expression )?	-> ^( ARRAY_OBJECT defining_identifier_list array_type_definition expression? )
		) SEMICOLON { $SymbolTable::Objects.UnionWith( $Symbols::IDs ); }
	;

defining_identifier_list
	:
		id = defining_identifier { $Symbols::IDs.Add( $id.text ); } ( COMMA id = defining_identifier { $Symbols::IDs.Add( $id.text ); } )*
			-> ^( ID_LIST defining_identifier+ )
	;

range_constraint
	:	RANGE! range ;

range
	:
		//range_attribute_reference |
		simple_expression DOTDOT simple_expression -> ^( RANGE simple_expression simple_expression )
	;

enumeration_type_definition
	:	LPARANTHESIS enumeration_literal_specification ( COMMA enumeration_literal_specification )* RPARANTHESIS -> ^( ENUMERATION enumeration_literal_specification+ ) ;

enumeration_literal_specification
	:
		defining_identifier |
		defining_character_literal
	;

defining_character_literal
	:	CHARACTER_LITERAL ;

integer_type_definition
	:
		signed_integer_type_definition |
		modular_type_definition
	;

signed_integer_type_definition
	:	RANGE^ simple_expression DOTDOT! simple_expression ;

modular_type_definition
	:	MOD^ expression ;

real_type_definition
	:
		floating_point_definition |
		fixed_point_definition
	;

floating_point_definition
	:	DIGITS^ expression real_range_specification? ;

real_range_specification
	:	RANGE^ simple_expression DOTDOT! simple_expression ;

fixed_point_definition
	:
		( ordinary_fixed_point_definition )=>	ordinary_fixed_point_definition |
												decimal_fixed_point_definition
	;

ordinary_fixed_point_definition
	:	DELTA expression real_range_specification -> ^( ORDINARY_FIXED expression real_range_specification ) ;

decimal_fixed_point_definition
	:	DELTA expression DIGITS expression real_range_specification? -> ^( DECIMAL_FIXED expression expression real_range_specification? ) ;

digits_constraint
	:	DIGITS^ expression range_constraint? ;

array_type_definition
	:
		unconstrained_array_definition |
		constrained_array_definition
	;

unconstrained_array_definition
	:
		ARRAY LPARANTHESIS index_subtype_definition ( COMMA index_subtype_definition )* RPARANTHESIS OF component_definition
			-> ^( UNCONSTRAINED_ARRAY index_subtype_definition+ OF component_definition )
	;

index_subtype_definition
	:	subtype_mark RANGE BOX ;

constrained_array_definition
	:
		ARRAY LPARANTHESIS discrete_subtype_definition ( COMMA discrete_subtype_definition )* RPARANTHESIS OF component_definition
			-> ^( CONSTRAINED_ARRAY discrete_subtype_definition+ OF component_definition )
	;

discrete_subtype_definition
	:
		subtype_indication |	// discrete_ subtype_indication
		range
	;

component_definition
	:	ALIASED? subtype_indication ;

//index_constraint :  '(' discrete_range ( ',' discrete_range )* ')' ;

discrete_range
	:
		{ IsType( input.LT( 1 ).Text ) }?=>	subtype_indication |	// discrete_ subtype_indication
											range
	;

known_discriminant_part
	:	LPARANTHESIS! discriminant_specification ( SEMICOLON! discriminant_specification)* RPARANTHESIS! ;

discriminant_specification
	scope Symbols;
	@init
	{
		$Symbols::IDs = new SortedSet< string >();
	}
	:
		defining_identifier_list COLON ( subtype_mark /* | access_definition*/ )( ASSIGN default_expression )?
			{ $SymbolTable::Objects.UnionWith( $Symbols::IDs ); }
			-> ^( DISCRIMINANT_SPECIFICATION defining_identifier_list subtype_mark default_expression? )
	;

default_expression
	:	expression ;
/*
discriminant_constraint :	'(' discriminant_association ( ',' discriminant_association )* ')' ;

discriminant_association :	( selector_name ( '|' selector_name )* '=>' )? expression ;	// discriminant_ selector_name
*/
record_type_definition
	:	( ABSTRACT? TAGGED )? LIMITED? record_definition -> ^( RECORD ( ABSTRACT? TAGGED )? LIMITED? record_definition ) ;

record_definition
	:
		RECORD! component_list END! RECORD! |
		NULL! RECORD!
	;

component_list
	:
		( component_item* variant_part )=>	component_item* variant_part	-> ^( COMPONENT_LIST component_item* variant_part ) |
											component_item+					-> ^( COMPONENT_LIST component_item+ ) |
											NULL! SEMICOLON!
	;

component_item
	:	component_declaration ;

component_declaration
	scope Symbols;
	@init
	{
		$Symbols::IDs = new SortedSet< string >();
	}
	:
		defining_identifier_list COLON component_definition ( ASSIGN default_expression )? SEMICOLON
			{ $SymbolTable::Objects.UnionWith( $Symbols::IDs ); }
			-> ^( COMPONENT defining_identifier_list component_definition default_expression? )
	;

variant_part
	:
		CASE^ direct_name IS!
		(
			( variant* other_variant )=>	variant* other_variant |
											variant+
		) END! CASE! SEMICOLON!
	;

variant
	:	WHEN^ discrete_choice_list ASSOCIATION! component_list ;
	
other_variant
	:	WHEN! OTHERS^ ASSOCIATION! component_list ;

discrete_choice_list
	:	discrete_choice ( CHOICE! discrete_choice )* ;

discrete_choice
	:
		( discrete_range )=>	discrete_range |
								expression
	;

record_extension_part
	:	WITH^ record_definition ;
/*
access_type_definition :
	access_to_object_definition |
	access_to_subprogram_definition ;

access_to_object_definition :	'access' general_access_modifier? subtype_indication ;

general_access_modifier :
	'all' |
	'constant' ;

access_to_subprogram_definition :
	'access' 'protected'? ( 'procedure' parameter_profile | 'function' parameter_and_result_profile ) ;
*/
access_definition
	:	ACCESS subtype_mark ;

declarative_part
	:	declarative_item* -> ^( DECLARE declarative_item* ) ;

declarative_item
	:
		( basic_declarative_item )=>	basic_declarative_item	-> ^( ITEM basic_declarative_item ) |
										body					-> ^( BODY body )
	;

basic_declarative_item
	:	basic_declaration ;

body
	:	proper_body ;

proper_body
	:	subprogram_body ;

name
	:
		{ IsFunction( input.LT( 1 ).Text ) }?=>	function_call |
		{ IsType( input.LT( 1 ).Text ) }?=>		type_conversion |
		{ IsObject( input.LT( 1 ).Text ) }?=>	( options{ backtrack = true; } :
													slice					-> slice |
													indexed_component		-> indexed_component |
													explicit_dereference	-> explicit_dereference |
													selected_component		-> selected_component |
													direct_name				-> ^( OBJECT direct_name )
												) |
												attribute_reference |
												CHARACTER_LITERAL |
		// erroneous alternatives
		( options{ backtrack = true; } :
			type_conversion			-> type_conversion | 		// conversion to an unknown type or calling unknown function or indexing unknown object.
			indexed_component		-> indexed_component |		// indexing unknown object or calling unknown function.
			slice					-> slice |					// slicing unknown object.
			explicit_dereference	-> explicit_dereference |	// explicitly referencing unknown object.
			selected_component		-> selected_component |		// selecting component of an unknown object.
			function_call			-> function_call			// calling unknown function or using unknown object
		)
	;

direct_name
	:	IDENTIFIER ;

explicit_dereference
	:	direct_name DOT ALL -> ^( EXPLICIT_DEREFERENCE direct_name ) ;
	
indexed_component
	:	direct_name LPARANTHESIS expression ( COMMA expression )* RPARANTHESIS -> ^( INDEXED_COMPONENT direct_name expression+ ) ;

slice
	:	direct_name LPARANTHESIS discrete_range RPARANTHESIS -> ^( SLICE direct_name discrete_range ) ;

selected_component
	:	direct_name DOT selector_name -> ^( SELECTED_COMPONENT direct_name selector_name ) ;

selector_name
	:
		IDENTIFIER |
		CHARACTER_LITERAL |
		operator_symbol
	;

attribute_reference
	:	direct_name APOSTROPHE attribute_designator -> ^( ATTRIBUTE_REFERENCE direct_name attribute_designator ) ;

attribute_designator
	:	IDENTIFIER ( LPARANTHESIS! expression RPARANTHESIS! )? ;

/*
range_attribute_reference :	name APOSTROPHE range_attribute_designator ;

range_attribute_designator :	'Range' ( '(' expression ')' )? ;

aggregate :
	record_aggregate |
	extension_aggregate |
	array_aggregate ;

record_aggregate : '(' record_component_association_list ')' ;

record_component_association_list :
	record_component_association ( ',' record_component_association )* |
	NULL RECORD ;

record_component_association :	( component_choice_list '=>' )? expression ;

component_choice_list :
	selector_name ( '|' selector_name )* |	// component_ selector_name
	OTHERS ;

extension_aggregate :	'(' ancestor_part 'with' record_component_association_list ')' ;

ancestor_part :
	expression |
	subtype_mark ;

array_aggregate :
	positional_array_aggregate |
	named_array_aggregate ;

positional_array_aggregate :
	'(' expression ( ',' expression )+ ')' |
	'(' expression ( ',' expression )* ',' 'others' '=>' expression ')' ;

named_array_aggregate :	'(' array_component_association ( ',' array_component_association )* ')' ;

array_component_association :	discrete_choice_list '=>' expression ;
*/
expression
	:
		relation
		(
			( AND relation )+		-> ^( EXPRESSION AND relation+ ) |
			( AND THEN relation )+	-> ^( EXPRESSION THEN relation+ ) |
			( OR relation )+		-> ^( EXPRESSION OR relation+ ) |			
			( OR ELSE relation )+	-> ^( EXPRESSION ELSE relation+ ) |
			( XOR relation )+		-> ^( EXPRESSION XOR relation+ ) |
			/*empty*/				-> ^( EXPRESSION relation )
		)
	;

relation
	:
		simple_expression
		(
			relational_operator simple_expression -> ^( relational_operator simple_expression simple_expression ) |
			NOT? IN
			(
				( range )=>	range			-> ^( IN_RANGE simple_expression range NOT? ) |
							subtype_mark	-> ^( IN_SUBTYPE simple_expression subtype_mark NOT? )
			) |
			/*empty*/ -> simple_expression
		)
	;

simple_expression
	:	unary_adding_operator? term ( binary_adding_operator^ term )* ;

term
	:	factor ( multiplying_operator^ factor )* ;

factor
	:
		( primary POW )=>	primary POW^ primary |
							primary |
							ABS^ primary |
							NOT^ primary
	;

primary
	:
		NUMERIC_LITERAL |
		NULL |
		STRING_LITERAL |
		//aggregate |
		name |
		qualified_expression |
	//	allocator |
		LPARANTHESIS! expression RPARANTHESIS!
	;

relational_operator
	:	EQ | NEQ | LESS | LEQ | GREATER | GEQ ;

binary_adding_operator
	:	PLUS | MINUS | CONCAT ;

unary_adding_operator
	:	PLUS | MINUS ;

multiplying_operator
	:	MULT | DIV | MOD | REM ;
	
type_conversion
	:	direct_name LPARANTHESIS expression RPARANTHESIS -> ^( TYPE direct_name expression ) ;

qualified_expression
	:
		subtype_mark APOSTROPHE
		(
			LPARANTHESIS expression RPARANTHESIS -> ^( QUALIFIED_BY_EXPRESSION subtype_mark expression ) //|
			//aggregate -> ^( QUALIFIED_BY_AGREGATE subtype_mark expression )
		)
	;

//allocator : 'new' ( subtype_indication | qualified_expression ) ;

sequence_of_statements
	:	statement+ ;

statement
	:
		label*
		(
			simple_statement	-> ^( SIMPLE_STATEMENT label* simple_statement ) |
			compound_statement	-> ^( COMPOUND_STATEMENT label* compound_statement )
		)
	;

simple_statement
	:
		( assignment_statement )=>	assignment_statement |
									procedure_call_statement |
									return_statement |
									exit_statement |
									goto_statement |
									null_statement
	;

compound_statement
	:
		if_statement |
		case_statement |
		loop_statement |
		block_statement
	;

null_statement
	:	NULL! SEMICOLON! ;

label
	:	LLABELBRACKET statement_identifier RLABELBRACKET -> ^( LABEL statement_identifier ) ;

statement_identifier
	:	IDENTIFIER ;

assignment_statement
	:	name ASSIGN^ expression SEMICOLON! ;

if_statement
	:	IF^ condition THEN! sequence_of_statements ( ELSIF! condition THEN! sequence_of_statements )* ( ELSE sequence_of_statements )? END! IF! SEMICOLON! ;

condition
	:	expression ;

case_statement
	:
		CASE^ expression IS!
		(
			( case_statement_alternative* other_case_statement_alternative )=>	case_statement_alternative* other_case_statement_alternative |
																				case_statement_alternative+
		) END! CASE! SEMICOLON!
	;

case_statement_alternative
	:	WHEN^ discrete_choice_list ASSOCIATION! sequence_of_statements ;

other_case_statement_alternative
	:	WHEN! OTHERS^ ASSOCIATION! sequence_of_statements ;

loop_statement
	scope SymbolTable;
	@init
	{
		$SymbolTable::Types = new SortedSet< string >();
		$SymbolTable::Functions = new SortedSet< string >();
		$SymbolTable::Procedures = new SortedSet< string >();
		$SymbolTable::Objects = new SortedSet< string >();
	}
	:	( statement_identifier COLON! )? iteration_scheme? LOOP^ handled_sequence_of_statements END! LOOP! statement_identifier? SEMICOLON! ;

iteration_scheme
	:
		WHILE^ condition |
		FOR^ loop_parameter_specification
	;

loop_parameter_specification
	:
		i = defining_identifier
		{ $SymbolTable::Objects.Add( $i.text ); }
		IN^ REVERSE? discrete_subtype_definition
	;

block_statement
	scope SymbolTable;
	@init
	{
		$SymbolTable::Types = new SortedSet< string >();
		$SymbolTable::Functions = new SortedSet< string >();
		$SymbolTable::Procedures = new SortedSet< string >();
		$SymbolTable::Objects = new SortedSet< string >();
	}
	:
		( statement_identifier COLON )? ( DECLARE declarative_part )? BEGIN sequence_of_statements END statement_identifier? SEMICOLON
			-> ^( BLOCK statement_identifier? declarative_part? sequence_of_statements statement_identifier? )
	;

exit_statement
	:	EXIT^ statement_identifier? ( WHEN! condition )? SEMICOLON! ;

goto_statement
	:	GOTO^ statement_identifier SEMICOLON! ;

subprogram_declaration
	:	subprogram_specification[ false ] ( IS! ABSTRACT )? SEMICOLON! ;

subprogram_specification[ bool withBody ]
	:
		PROCEDURE^ procID = defining_program_unit_name  parameter_profile[ withBody ]
		{
			int prev = $SymbolTable.Count - ( withBody ? 2 : 1 );
			$SymbolTable[ prev ]::Procedures.Add( $procID.text );
		} |
		FUNCTION^ funcID = defining_designator parameter_and_result_profile[ withBody ]
		{
			int prev = $SymbolTable.Count - ( withBody ? 2 : 1 );
			$SymbolTable[ prev ]::Functions.Add( $funcID.text );
		}
	;

designator
	:
		IDENTIFIER |
		operator_symbol
	;

defining_designator
	:
		defining_program_unit_name |
		defining_operator_symbol
	;

defining_program_unit_name
	:	defining_identifier ;

operator_symbol
	:	STRING_LITERAL ;

defining_operator_symbol
	:	operator_symbol ;

parameter_profile[ bool withBody ]
	:	formal_part[ withBody ]? ;

parameter_and_result_profile[ bool withBody ]
	:	formal_part[ withBody ]? RETURN subtype_mark ;

formal_part[ bool withBody ]
	:	LPARANTHESIS! parameter_specification[ withBody ] ( SEMICOLON! parameter_specification[ withBody ] )* RPARANTHESIS! ;

parameter_specification[ bool withBody ]
	scope Symbols;
	@init
	{
		$Symbols::IDs = new SortedSet< string >();
	}
	:
		defining_identifier_list COLON ( mode subtype_mark /*| access_definition*/ ) ( ASSIGN default_expression )?
			{ if ( withBody ) $SymbolTable::Objects.UnionWith( $Symbols::IDs ); }
			-> ^( PARAMETER defining_identifier_list mode subtype_mark default_expression? )+
	;

mode
	:
		/*empty*/	-> IN |
		IN |
		IN OUT		-> REF |
		OUT
	;

subprogram_body
	scope SymbolTable;
	@init
	{
		$SymbolTable::Types = new SortedSet< string >();
		$SymbolTable::Functions = new SortedSet< string >();
		$SymbolTable::Procedures = new SortedSet< string >();
		$SymbolTable::Objects = new SortedSet< string >();
	}
	:	subprogram_specification[ true ] IS! declarative_part BEGIN! handled_sequence_of_statements END! designator? SEMICOLON! ;

handled_sequence_of_statements
	:	sequence_of_statements ;

procedure_call_statement
	:	direct_name actual_parameter_part? SEMICOLON -> ^( PROCEDURE direct_name actual_parameter_part? ) ;
	
function_call
	:	direct_name actual_parameter_part? -> ^( FUNCTION direct_name actual_parameter_part? ) ;

actual_parameter_part
	:	LPARANTHESIS! parameter_association ( COMMA! parameter_association )* RPARANTHESIS! ;

parameter_association
	:	( selector_name ASSOCIATION )? explicit_actual_parameter -> ^( PARAMETER explicit_actual_parameter selector_name? ) ;

explicit_actual_parameter
	:	expression ;

return_statement
	:	RETURN^ expression? SEMICOLON! ;


DELTA			:	'delta' ;
WITH			:	'with' ;
NEW				:	'new' ;
ARRAY			:	'array' ;
OF				:	'of' ;
CONSTANT		:	'constant' ;
ALIASED			:	'aliased' ;
IF				:	'if' ;
ELSIF			:	'elsif' ;
WHILE			:	'while' ;
FOR				:	'for' ;
LOOP			:	'loop' ;
REVERSE			:	'reverse' ;
GOTO			:	'goto' ;
EXIT			:	'exit' ;
DECLARE			:	'declare' ;
PROCEDURE		:	'procedure' ;
FUNCTION		:	'function' ;
ABSTRACT		:	'abstract' ;
TAGGED			:	'tagged' ;
LIMITED			:	'limited' ;
ACCESS			:	'access' ;
ALL				:	'all' ;
RETURN			:	'return' ;
DIGITS			:	'digits' ;
RANGE			:	'range' ;
TYPE			:	'type' ;
SUBTYPE			:	'subtype' ;
IS				:	'is' ;
RECORD			:	'record' ;
CASE			:	'case' ;
WHEN			:	'when' ;
OTHERS			:	'others' ;
BEGIN			:	'begin' ;
END				:	'end' ;
NULL			:	'null' ;
IN				:	'in' ;
OUT				:	'out' ;
NOT				:	'not' ;
AND				:	'and' ;
OR				:	'or' ;
XOR				:	'xor' ;
THEN			:	'then' ;
ELSE			:	'else' ;
CHOICE			:	'|' ;
POW				:	'**' ;
MULT			:	'*' ;
DIV				:	'/' ;
ABS				:	'abs' ;
MOD				:	'mod' ;
REM				:	'rem' ;
LLABELBRACKET	:	'<<' ;
RLABELBRACKET	:	'>>' ;
SEMICOLON		:	';' ;
ASSOCIATION		:	'=>' ;
COLON			:	':' ;
COMMA			:	',' ;
APOSTROPHE		:	'\'' ;
DOT				:	'.' ;
DOTDOT			:	'..' ;
ASSIGN			:	':=' ;
LPARANTHESIS	:	'(' ;
RPARANTHESIS	:	')' ;
BOX				:	'<>' ;
EQ				:	'=' ;
NEQ				:	'/=' ;
LESS			:	'<' ;
LEQ				:	'<=' ;
GREATER			:	'>' ;
GEQ				:	'>=' ;
PLUS			:	'+' ;
MINUS			:	'-' ;
CONCAT			:	'&' ;


IDENTIFIER
	:	IDENTIFIER_LETTER ( '_'? ( IDENTIFIER_LETTER | DIGIT ) )* ;
	
NUMERIC_LITERAL
	:	DECIMAL_LITERAL | BASED_LITERAL;

fragment
DECIMAL_LITERAL
	:	NUMERAL ( '.' NUMERAL )? EXPONENT? ;

fragment
BASED_LITERAL
	:	BASE '#' BASED_NUMERAL( '.' BASED_NUMERAL )? '#' EXPONENT?;

fragment
EXPONENT
	:	( 'e' | 'E' ) ( '+' | '-' )? NUMERAL ;

fragment	
NUMERAL
	:	DIGIT ( '_'? DIGIT )* ;

fragment
DIGIT
	:	'0'..'9' ;

fragment
BASE
	:	NUMERAL ;

fragment
BASED_NUMERAL
	:	EXTENDED_DIGIT ( '_'? EXTENDED_DIGIT )* ;

fragment
EXTENDED_DIGIT
	:	DIGIT | 'a'..'f' | 'A'..'F' ;

STRING_LITERAL
	:	'"' ( '""' | ~'"' )* '"' ;

CHARACTER_LITERAL
	:	{ input.LA( 3 ) == '\'' }?=> '\'' ( '\'\'' | ~'\'' ) '\'' ;

fragment
IDENTIFIER_LETTER
	:	'a'..'z' | 'A'..'Z' ;

COMMENT
	:	'--' ~( '\n' | '\r' )* ( '\r\n' | '\r' | '\n' )? { $channel = HIDDEN; } ;

WS
	:	( ' ' | '\t' | '\r' | '\n' )+ { $channel = HIDDEN; } ;