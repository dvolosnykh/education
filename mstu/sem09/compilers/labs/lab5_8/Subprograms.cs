﻿using System.Collections.Generic;

namespace Ada95
{
	public class Function
	{
		public string ReturnType;
		public string Name;
		public List< Parameter > Parameters;
		
		public Function( string returnType, string name, List< Parameter > parameters )
		{
			ReturnType = returnType;
			Name = name;
			Parameters = parameters;
		}
	}

	public class Procedure
	{
		public string ReturnType = "void";
		public string Name;
		public List< Parameter > Parameters;
		
		public Procedure( string name, List< Parameter > parameters )
		{
			Name = name;
			Parameters = parameters;
		}
	}

	public class Parameter
	{
		public string Mode;
		public string ValueType;
		public string ID;
		public string DefaultValue;

		public Parameter( string mode, string type, string id, string defaultValue )
		{
			Mode = mode;
			ValueType = type;
			ID = id;
			DefaultValue = defaultValue;
		}
	}
}