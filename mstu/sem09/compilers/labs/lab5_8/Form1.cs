﻿using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace lab5_8
{
	public partial class Form1 : Form
	{
		private enum State { MODIFIED, SAVED, BUILT };

		private State __state = State.SAVED;
		private string __filename = string.Empty;
		CodeGenerator __generator = new CodeGenerator();

		public Form1()
		{
			InitializeComponent();
			this.WindowState = FormWindowState.Maximized;
			editorTextBox_SelectionChanged( this, new EventArgs() );

			using ( StreamReader s = new StreamReader( "..\\..\\test.adb" ) )
			{
				editorTextBox.Text = s.ReadToEnd();
			}
		}

		private void openToolStripMenuItem_Click( object sender, EventArgs e )
		{
			if ( WarnIfModified() && openFileDialog.ShowDialog() == DialogResult.OK )
			{
				__filename = openFileDialog.FileName;
				using ( StreamReader reader = new StreamReader( __filename ) )
				{
					editorTextBox.Text = reader.ReadToEnd();
					__state = State.SAVED;
				}
			}
		}

		private void saveToolStripMenuItem_Click( object sender, EventArgs e )
		{
			bool save = false;
			if ( __filename != string.Empty )
			{
				save = true;
			}
			else if ( saveFileDialog.ShowDialog() == DialogResult.OK )
			{
				__filename = saveFileDialog.FileName;
				save = true;
			}

			if ( save )
			{
				using ( StreamWriter writer = new StreamWriter( __filename ) )
				{
					writer.Write( editorTextBox.Text );
				}

				__state = State.SAVED;
			}
		}

		private void newToolStripMenuItem_Click( object sender, EventArgs e )
		{
			if ( WarnIfModified() && openFileDialog.ShowDialog() == DialogResult.OK )
			{
				editorTextBox.Text = string.Empty;
				__filename = string.Empty;
				__state = State.SAVED;
			}
		}

		private void quitToolStripMenuItem_Click( object sender, EventArgs e )
		{
			this.Close();
		}

		private void buildProgramToolStripMenuItem_Click( object sender, EventArgs e )
		{
			if ( __state == State.MODIFIED )
				saveToolStripMenuItem_Click( sender, e );

			if ( __state == State.SAVED )
			{
				StringBuilder sb = new StringBuilder();
				sb.AppendLine( "--- Build started... ---" );

				string output = string.Empty;
				bool success = __generator.Build( editorTextBox.Text, ref output );
				sb.Append( output );
				outputTextBox.Text = sb.ToString();
				outputTextBox.Update();

				if ( success )
					__state = State.BUILT;
			}
		}

		private void runProgramToolStripMenuItem_Click( object sender, EventArgs e )
		{
			if ( __state != State.BUILT )
				buildProgramToolStripMenuItem_Click( sender, e );

			if ( __state == State.BUILT )
			{
				AllocConsole();

				Exception exception = __generator.Run();
				if ( exception != null )
					MessageBox.Show( exception.Message, exception.Source, MessageBoxButtons.OK, MessageBoxIcon.Error );
				
				FreeConsole();
			}
		}

		private bool WarnIfModified()
		{
			bool goon = ( __state != State.MODIFIED );
			if ( !goon )
			{
				DialogResult choice = MessageBox.Show( "File was modified. Save it?", "Warning", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning );

				if ( choice == System.Windows.Forms.DialogResult.Yes )
					saveToolStripMenuItem_Click( saveToolStripMenuItem, new EventArgs() );

				goon = ( choice != DialogResult.Cancel );
			}

			return ( goon );
		}

		private void editorTextBox_TextChanged( object sender, EventArgs e )
		{
			__state = State.MODIFIED;
		}

		[ DllImport( "kernel32.dll" ) ]
        private static extern bool AllocConsole();

        [ DllImport( "kernel32.dll" ) ]
		private static extern bool FreeConsole();

		private void editorTextBox_SelectionChanged( object sender, EventArgs e )
		{
			int position = this.editorTextBox.SelectionStart;
			int lineIndex = this.editorTextBox.GetLineFromCharIndex( position );
			int lineStartPos = this.editorTextBox.GetFirstCharIndexFromLine( lineIndex );
			int columnIndex = position - lineStartPos;

			this.toolStripLine.Text = ( lineIndex + 1 ).ToString();
			this.toolStripColumn.Text = ( columnIndex + 1 ).ToString();
		}
	}
}
