﻿using System.Collections.Generic;

namespace Ada95
{
	#region type

	public abstract class Type //: ICloneable
	{
		public string DefiningIdentifier;
	}

	#region simple

	public abstract class Simple : Type {}

	#region scalar

	public abstract class Scalar : Simple {}

	#region real

	public abstract class Real : Scalar {}

	public class UniversalReal : Real {}

	public class RootReal : Real {}

	public class Float : RootReal {}

	public class Fixed : RootReal {}

	public class OrdinaryFixed : Fixed {}

	public class DecimalFixed : Fixed {}

	#endregion

	#region discrete

	public abstract class Discrete : Scalar {}
	
	#region integer

	public abstract class Integer : Discrete {}

	public class UniversalInteger : Integer {}

	public class RootInteger : Integer
	{
		private RootInteger __parent;

		public RootInteger( RootInteger parent )
		{
			__parent = parent;
		}
	}

	public class SignedInteger : RootInteger
	{
		public int Min, Max;

		public SignedInteger( int min, int max, RootInteger parent )
			: base( parent )
		{
			Min = min;
			Max = max;
		}
	}

	public class ModularInteger : RootInteger
	{
		public uint Modulus;

		public ModularInteger( uint modulus, RootInteger parent )
			: base( parent )
		{
			Modulus = modulus;
		}
	}
 
	#endregion

	#region enumerable

	public abstract class Enumerable : Discrete {}

	class Boolean : Enumerable
	{
		public Boolean()
		{
			DefiningIdentifier = "Boolean";
		}
	}

	public class UserEnumerable : Enumerable
	{
	}

	#endregion

	#endregion

	#endregion

	#region acesses.

	public abstract class Access : Scalar {}

	public abstract class ObjectAccess : Access {}

	public abstract class SubprogramAccess : Access {}

	#endregion

	#endregion

	#region compound

	public abstract class Compound : Type {}

	#region array

	public abstract class Array : Compound
	{
		public Array()
		{
		}
	}

	public class String : Array
	{
		public String()
		{
			DefiningIdentifier = "String";
		}
	}

	public class UserArray : Array
	{
	}

	#endregion

	#region record

	public class Record : Compound
	{
		public SortedDictionary< string, Discriminant > Discriminants;
		public SortedDictionary< string, Component > Components;
		public SortedDictionary< string, Variant > Variants;
	}

	#region Discriminants

	public class Discriminant
	{
	}

	#endregion

	#region Components

	public class Component
	{
		public string Identifier;
		public Type Definition;
		public object DefaultExpression;	// object -> SubtypeIndication ?
	}

	public class SubtypeIndication
	{
		string Name;
	}

	#endregion

	#region Variants

	public class Variant
	{
	}

	#endregion

	#endregion

	public abstract class TaggedRecord : Compound {}

	public abstract class Task : Compound {}

	public abstract class Protected : Compound {}

	#endregion

	#endregion

	class Slice : Array
	{
		private int __lowerBound, __upperBound;

		public Slice( int lowerBound, int upperBound )
		{
			__lowerBound = lowerBound;
			__upperBound = upperBound;
		}

		// TODO : overload methods of 'Array'
	}
}
