﻿using Antlr.Runtime;

namespace Ada95
{
	class CompileException : RecognitionException
	{
		public new string Message;

		public CompileException( string message, IIntStream input )
			: base( input )
		{
			Message = message;
		}
	}

	class UnknownFunction : CompileException
	{
		public UnknownFunction( string name, IIntStream input )
			: base( "Calling unknown function or using unknown object: '" + name + "'.", input ) {}
	}

	class UnknownProcedure : CompileException
	{
		public UnknownProcedure( string name, IIntStream input )
			: base( "Calling unknown procedure: '" + name + "'.", input ) {}
	}

	class UnknownIndexedComponent : CompileException
	{
		public UnknownIndexedComponent( string name, IIntStream input )
			: base( "Indexing unknown object or calling unknown function: '" + name + "'.", input ) {}
	}

	class UnexpectedType : CompileException
	{
		public UnexpectedType( string encountered, string expected, IIntStream input )
			: base( "Type mismatched: unable to convert from '" + encountered + "' to '" + expected + "'.", input ) {}
	}
}