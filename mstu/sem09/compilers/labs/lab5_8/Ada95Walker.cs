// $ANTLR 3.2 Sep 23, 2009 12:02:23 Ada95Walker.g 2009-12-23 07:03:04

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Text;


using System;
using Antlr.Runtime;
using Antlr.Runtime.Tree;using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;


using Antlr.StringTemplate;
using Antlr.StringTemplate.Language;
using Hashtable = System.Collections.Hashtable;
namespace  Ada95 
{
public partial class Ada95Walker : TreeParser
{
    public static readonly string[] tokenNames = new string[] 
	{
        "<invalid>", 
		"<EOR>", 
		"<DOWN>", 
		"<UP>", 
		"ORDINARY_FIXED", 
		"DECIMAL_FIXED", 
		"ID_LIST", 
		"ARRAY_OBJECT", 
		"UNCONSTRAINED_ARRAY", 
		"CONSTRAINED_ARRAY", 
		"ENUMERATION", 
		"REF", 
		"ITEM", 
		"BODY", 
		"BLOCK", 
		"SIMPLE_STATEMENT", 
		"COMPOUND_STATEMENT", 
		"EXPLICIT_DEREFERENCE", 
		"INDEXED_COMPONENT", 
		"SLICE", 
		"SELECTED_COMPONENT", 
		"OBJECT", 
		"ATTRIBUTE_REFERENCE", 
		"COMPONENT", 
		"PARAMETER", 
		"DISCRIMINANT_SPECIFICATION", 
		"QUALIFIED_BY_EXPRESSION", 
		"QUALIFIED_BY_AGREGATE", 
		"COMPONENT_LIST", 
		"EXPRESSION", 
		"DISCRETE_RANGE", 
		"IN_RANGE", 
		"IN_SUBTYPE", 
		"LABEL", 
		"IDENTIFIER", 
		"TYPE", 
		"IS", 
		"SEMICOLON", 
		"SUBTYPE", 
		"COLON", 
		"CONSTANT", 
		"ASSIGN", 
		"ABSTRACT", 
		"NEW", 
		"ALIASED", 
		"COMMA", 
		"RANGE", 
		"DOTDOT", 
		"LPARANTHESIS", 
		"RPARANTHESIS", 
		"CHARACTER_LITERAL", 
		"MOD", 
		"DIGITS", 
		"DELTA", 
		"ARRAY", 
		"OF", 
		"BOX", 
		"TAGGED", 
		"LIMITED", 
		"RECORD", 
		"END", 
		"NULL", 
		"CASE", 
		"WHEN", 
		"ASSOCIATION", 
		"OTHERS", 
		"CHOICE", 
		"WITH", 
		"ACCESS", 
		"DOT", 
		"ALL", 
		"APOSTROPHE", 
		"AND", 
		"THEN", 
		"OR", 
		"ELSE", 
		"XOR", 
		"NOT", 
		"IN", 
		"POW", 
		"ABS", 
		"NUMERIC_LITERAL", 
		"STRING_LITERAL", 
		"EQ", 
		"NEQ", 
		"LESS", 
		"LEQ", 
		"GREATER", 
		"GEQ", 
		"PLUS", 
		"MINUS", 
		"CONCAT", 
		"MULT", 
		"DIV", 
		"REM", 
		"LLABELBRACKET", 
		"RLABELBRACKET", 
		"IF", 
		"ELSIF", 
		"LOOP", 
		"WHILE", 
		"FOR", 
		"REVERSE", 
		"DECLARE", 
		"BEGIN", 
		"EXIT", 
		"GOTO", 
		"PROCEDURE", 
		"FUNCTION", 
		"RETURN", 
		"OUT", 
		"IDENTIFIER_LETTER", 
		"DIGIT", 
		"DECIMAL_LITERAL", 
		"BASED_LITERAL", 
		"NUMERAL", 
		"EXPONENT", 
		"BASE", 
		"BASED_NUMERAL", 
		"EXTENDED_DIGIT", 
		"COMMENT", 
		"WS"
    };

    public const int FUNCTION = 108;
    public const int EXPONENT = 116;
    public const int WHILE = 100;
    public const int MOD = 51;
    public const int UNCONSTRAINED_ARRAY = 8;
    public const int CASE = 62;
    public const int NEW = 43;
    public const int NOT = 77;
    public const int IN_SUBTYPE = 32;
    public const int SUBTYPE = 38;
    public const int EOF = -1;
    public const int BASED_LITERAL = 114;
    public const int TYPE = 35;
    public const int ATTRIBUTE_REFERENCE = 22;
    public const int STRING_LITERAL = 82;
    public const int GREATER = 87;
    public const int POW = 79;
    public const int ENUMERATION = 10;
    public const int LOOP = 99;
    public const int BEGIN = 104;
    public const int PARAMETER = 24;
    public const int LESS = 85;
    public const int RETURN = 109;
    public const int EXPLICIT_DEREFERENCE = 17;
    public const int BASE = 117;
    public const int ALIASED = 44;
    public const int BODY = 13;
    public const int GEQ = 88;
    public const int APOSTROPHE = 71;
    public const int EQ = 83;
    public const int GOTO = 106;
    public const int RLABELBRACKET = 96;
    public const int COMMENT = 120;
    public const int ARRAY = 54;
    public const int EXIT = 105;
    public const int RECORD = 59;
    public const int CONSTRAINED_ARRAY = 9;
    public const int CONCAT = 91;
    public const int NULL = 61;
    public const int ELSE = 75;
    public const int CHARACTER_LITERAL = 50;
    public const int DELTA = 53;
    public const int SEMICOLON = 37;
    public const int INDEXED_COMPONENT = 18;
    public const int MULT = 92;
    public const int COMPONENT_LIST = 28;
    public const int OF = 55;
    public const int ABS = 80;
    public const int WS = 121;
    public const int OUT = 110;
    public const int EXTENDED_DIGIT = 119;
    public const int COMPOUND_STATEMENT = 16;
    public const int OR = 74;
    public const int CONSTANT = 40;
    public const int NUMERAL = 115;
    public const int ELSIF = 98;
    public const int REVERSE = 102;
    public const int END = 60;
    public const int DECIMAL_LITERAL = 113;
    public const int OTHERS = 65;
    public const int LIMITED = 58;
    public const int IDENTIFIER_LETTER = 111;
    public const int NUMERIC_LITERAL = 81;
    public const int DIGITS = 52;
    public const int FOR = 101;
    public const int DISCRIMINANT_SPECIFICATION = 25;
    public const int DOTDOT = 47;
    public const int ABSTRACT = 42;
    public const int QUALIFIED_BY_EXPRESSION = 26;
    public const int SLICE = 19;
    public const int ID_LIST = 6;
    public const int AND = 72;
    public const int IF = 97;
    public const int SELECTED_COMPONENT = 20;
    public const int BOX = 56;
    public const int ORDINARY_FIXED = 4;
    public const int THEN = 73;
    public const int IN = 78;
    public const int RPARANTHESIS = 49;
    public const int OBJECT = 21;
    public const int COMMA = 45;
    public const int IS = 36;
    public const int IDENTIFIER = 34;
    public const int DECIMAL_FIXED = 5;
    public const int ALL = 70;
    public const int ACCESS = 68;
    public const int BASED_NUMERAL = 118;
    public const int PLUS = 89;
    public const int DIGIT = 112;
    public const int IN_RANGE = 31;
    public const int DOT = 69;
    public const int COMPONENT = 23;
    public const int EXPRESSION = 29;
    public const int CHOICE = 66;
    public const int DISCRETE_RANGE = 30;
    public const int WITH = 67;
    public const int LLABELBRACKET = 95;
    public const int XOR = 76;
    public const int ITEM = 12;
    public const int SIMPLE_STATEMENT = 15;
    public const int RANGE = 46;
    public const int ARRAY_OBJECT = 7;
    public const int MINUS = 90;
    public const int REM = 94;
    public const int PROCEDURE = 107;
    public const int REF = 11;
    public const int COLON = 39;
    public const int LPARANTHESIS = 48;
    public const int NEQ = 84;
    public const int LABEL = 33;
    public const int WHEN = 63;
    public const int TAGGED = 57;
    public const int BLOCK = 14;
    public const int ASSIGN = 41;
    public const int DECLARE = 103;
    public const int QUALIFIED_BY_AGREGATE = 27;
    public const int DIV = 93;
    public const int ASSOCIATION = 64;
    public const int LEQ = 86;

    // delegates
    // delegators

    protected class Symbols_scope 
    {
        protected internal SortedSet< string > IDs;
    }
    protected Stack Symbols_stack = new Stack();
    protected class SymbolTable_scope 
    {
        protected internal SortedDictionary< string, string > Types;
        protected internal SortedDictionary< string, List< Function > > Functions;
        protected internal SortedDictionary< string, List< Procedure > > Procedures;
        protected internal SortedDictionary< string, Object > Objects;
    }
    protected Stack SymbolTable_stack = new Stack();
    protected class ActualParameters_scope 
    {
        protected internal List< string > Types;
        protected internal List< string > Values;
    }
    protected Stack ActualParameters_stack = new Stack();



        public Ada95Walker(ITreeNodeStream input)
    		: this(input, new RecognizerSharedState()) {
        }

        public Ada95Walker(ITreeNodeStream input, RecognizerSharedState state)
    		: base(input, state) {
            InitializeCyclicDFAs();

             
        }
        
    protected StringTemplateGroup templateLib =
      new StringTemplateGroup("Ada95WalkerTemplates", typeof(AngleBracketTemplateLexer));

    public StringTemplateGroup TemplateLib
    {
     	get { return this.templateLib; }
     	set { this.templateLib = value; }
    }

    /// <summary> Allows convenient multi-value initialization:
    ///  "new STAttrMap().Add(...).Add(...)"
    /// </summary>
    protected class STAttrMap : Hashtable
    {
      public STAttrMap Add(string attrName, object value) 
      {
        base.Add(attrName, value);
        return this;
      }
      public STAttrMap Add(string attrName, int value) 
      {
        base.Add(attrName, value);
        return this;
      }
    }

    override public string[] TokenNames {
		get { return Ada95Walker.tokenNames; }
    }

    override public string GrammarFileName {
		get { return "Ada95Walker.g"; }
    }


    	// symbols related stuff.
    	private bool IsType( string id )
    	{
    		bool found = false;
    		for ( int i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Types.ContainsKey( id ) );

    		return ( found );
    	}
    	
    	private string GetType( string adaType )
    	{
    		adaType = adaType.ToLower();
    		
    		bool found = false;
    		int i;
    		for ( i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Types.ContainsKey( adaType ) );

    		return ( found ? ((SymbolTable_scope)SymbolTable_stack[ i + 1 ]).Types[ adaType ] : null );
    	}
    	
    	private bool IsFunction( string id )
    	{
    		bool found = false;
    		for ( int i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Functions.ContainsKey( id ) );

    		return ( found );
    	}
    	
    	private List< Function > GetFunction( string id )
    	{	
    		bool found = false;
    		int i;
    		for ( i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Functions.ContainsKey( id ) );

    		return ( found ? ((SymbolTable_scope)SymbolTable_stack[ i + 1 ]).Functions[ id ] : null );
    	}
    	
    	private bool IsProcedure( string id )
    	{
    		bool found = false;
    		for ( int i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Procedures.ContainsKey( id ) );

    		return ( found );
    	}
    	
    	private List< Procedure > GetProcedure( string id )
    	{	
    		bool found = false;
    		int i;
    		for ( i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Procedures.ContainsKey( id ) );

    		return ( found ? ((SymbolTable_scope)SymbolTable_stack[ i + 1 ]).Procedures[ id ] : null );
    	}
    	
    	private bool IsObject( string id )
    	{
    		bool found = false;
    		for ( int i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Objects.ContainsKey( id ) );
    			
    		return ( found );
    	}
    	
    	private Object GetObject( string id )
    	{	
    		bool found = false;
    		int i;
    		for ( i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Objects.ContainsKey( id ) );

    		return ( found ? ((SymbolTable_scope)SymbolTable_stack[ i + 1 ]).Objects[ id ] : null );
    	}
    	
    	private bool IsString( string id )
    	{
    		return ( true );
    	}
    	
    	private Function FindBest( List< Function > functions, List< string > argTypes )
    	{
    		bool found = false;
    		int i;
    		for ( i = 0; !found && i < functions.Count; ++i )
    			found = __ArgTypesMatch( functions[ i ].Parameters, argTypes );
    		
    		return ( found ? functions[ i - 1 ] : null );
    	}
    	
    	private Procedure FindBest( List< Procedure > procedures, List< string > argTypes )
    	{
    		bool found = false;
    		int i;
    		for ( i = 0; !found && i < procedures.Count; ++i )
    			found = __ArgTypesMatch( procedures[ i ].Parameters, argTypes );
    		
    		return ( found ? procedures[ i - 1 ] : null );
    	}
    	
    	private bool __ArgTypesMatch( List< Parameter > parameters, List< string > argTypes )
    	{
    		bool match = ( parameters.Count == argTypes.Count );
    		for ( int i = 0; match && i < parameters.Count; ++i )
    			match = parameters[ i ].ValueType == argTypes[ i ];
    			
    		return ( match );
    	}
    		
    	// errors related stuff.
    	public StringBuilder Errors = new StringBuilder();	
    	
    	public override void EmitErrorMessage( string message )
    	{
    		Errors.AppendLine( message );
    	}
    	
    	public override string GetErrorMessage( RecognitionException re, string[] tokenNames )
    	{
    		return ( re is CompileException ? ( re as CompileException ).Message : base.GetErrorMessage( re, tokenNames ) );
    	}


    public class program_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "program"
    // Ada95Walker.g:165:8: public program : d= declarative_part -> Program(declarativePart= $d.st );
    public Ada95Walker.program_return program() // throws RecognitionException [1]
    {   
        SymbolTable_stack.Push(new SymbolTable_scope());

        Ada95Walker.program_return retval = new Ada95Walker.program_return();
        retval.Start = input.LT(1);

        Ada95Walker.declarative_part_return d = default(Ada95Walker.declarative_part_return);



        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types =  new SortedDictionary< string, string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Functions =  new SortedDictionary< string, List< Function > >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Procedures =  new SortedDictionary< string, List< Procedure > >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Objects =  new SortedDictionary< string, Object >();
        		
        		// predefined types.
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types.Add( "boolean", "bool" );
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types.Add( "integer", "int" );
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types.Add( "mod", "uint" );
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types.Add( "string", "string" );
        		
        		// predefined functions and procedures.
        		
        		// Ada.Text_IO
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Procedures.Add
        		( "Put", new List< Procedure >
        			( new Procedure[]
        				{
        					new Procedure( "Put", new List< Parameter >( new Parameter[] { new Parameter( "", "int", "value", "" ) } ) ),
        					new Procedure( "Put", new List< Parameter >( new Parameter[] { new Parameter( "", "uint", "value", "" ) } ) ),
        					new Procedure( "Put", new List< Parameter >( new Parameter[] { new Parameter( "", "bool", "flag", "" ) } ) ),
        					new Procedure( "Put", new List< Parameter >( new Parameter[] { new Parameter( "", "string", "str", "" ) } ) )
        				}
        			)
        		);
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Procedures.Add
        		( "Get", new List< Procedure >
        			( new Procedure[]
        				{
        					new Procedure( "Get", new List< Parameter >( new Parameter[] { new Parameter( "out", "int", "value", "" ) } ) ),
        					new Procedure( "Get", new List< Parameter >( new Parameter[] { new Parameter( "out", "uint", "value", "" ) } ) ),
        					new Procedure( "Get", new List< Parameter >( new Parameter[] { new Parameter( "out", "bool", "flag", "" ) } ) ),
        					new Procedure( "Get", new List< Parameter >( new Parameter[] { new Parameter( "out", "string", "str", "" ) } ) )
        				}
        			)
        		);
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Procedures.Add( "New_Line", new List< Procedure >( new Procedure[] { new Procedure( "New_Line", new List< Parameter >() ) } ) );
        	
        try 
    	{
            // Ada95Walker.g:207:2: (d= declarative_part -> Program(declarativePart= $d.st ))
            // Ada95Walker.g:207:4: d= declarative_part
            {
            	PushFollow(FOLLOW_declarative_part_in_program105);
            	d = declarative_part();
            	state.followingStackPointer--;



            	// TEMPLATE REWRITE
            	// 207:25: -> Program(declarativePart= $d.st )
            	{
            	    retval.ST = templateLib.GetInstanceOf("Program",
            	  new STAttrMap().Add("declarativePart",  ((d != null) ? d.ST : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            SymbolTable_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "program"

    public class basic_declaration_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "basic_declaration"
    // Ada95Walker.g:209:1: basic_declaration : (typeDecl= type_declaration -> { $typeDecl.st } | objDecl= object_declaration -> { $objDecl.st } | numDecl= number_declaration -> { $numDecl.st } | subProgDecl= subprogram_declaration -> { $subProgDecl.st });
    public Ada95Walker.basic_declaration_return basic_declaration() // throws RecognitionException [1]
    {   
        Ada95Walker.basic_declaration_return retval = new Ada95Walker.basic_declaration_return();
        retval.Start = input.LT(1);

        Ada95Walker.type_declaration_return typeDecl = default(Ada95Walker.type_declaration_return);

        Ada95Walker.object_declaration_return objDecl = default(Ada95Walker.object_declaration_return);

        Ada95Walker.number_declaration_return numDecl = default(Ada95Walker.number_declaration_return);

        Ada95Walker.subprogram_declaration_return subProgDecl = default(Ada95Walker.subprogram_declaration_return);


        try 
    	{
            // Ada95Walker.g:210:2: (typeDecl= type_declaration -> { $typeDecl.st } | objDecl= object_declaration -> { $objDecl.st } | numDecl= number_declaration -> { $numDecl.st } | subProgDecl= subprogram_declaration -> { $subProgDecl.st })
            int alt1 = 4;
            switch ( input.LA(1) ) 
            {
            case TYPE:
            	{
                alt1 = 1;
                }
                break;
            case ARRAY_OBJECT:
            case OBJECT:
            	{
                alt1 = 2;
                }
                break;
            case CONSTANT:
            	{
                alt1 = 3;
                }
                break;
            case PROCEDURE:
            case FUNCTION:
            	{
                alt1 = 4;
                }
                break;
            	default:
            	    NoViableAltException nvae_d1s0 =
            	        new NoViableAltException("", 1, 0, input);

            	    throw nvae_d1s0;
            }

            switch (alt1) 
            {
                case 1 :
                    // Ada95Walker.g:211:3: typeDecl= type_declaration
                    {
                    	PushFollow(FOLLOW_type_declaration_in_basic_declaration134);
                    	typeDecl = type_declaration();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 211:34: -> { $typeDecl.st }
                    	{
                    	    retval.ST =  ((typeDecl != null) ? typeDecl.ST : null) ;
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:213:3: objDecl= object_declaration
                    {
                    	PushFollow(FOLLOW_object_declaration_in_basic_declaration154);
                    	objDecl = object_declaration();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 213:34: -> { $objDecl.st }
                    	{
                    	    retval.ST =  ((objDecl != null) ? objDecl.ST : null) ;
                    	}


                    }
                    break;
                case 3 :
                    // Ada95Walker.g:214:3: numDecl= number_declaration
                    {
                    	PushFollow(FOLLOW_number_declaration_in_basic_declaration170);
                    	numDecl = number_declaration();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 214:34: -> { $numDecl.st }
                    	{
                    	    retval.ST =  ((numDecl != null) ? numDecl.ST : null) ;
                    	}


                    }
                    break;
                case 4 :
                    // Ada95Walker.g:215:3: subProgDecl= subprogram_declaration
                    {
                    	PushFollow(FOLLOW_subprogram_declaration_in_basic_declaration186);
                    	subProgDecl = subprogram_declaration();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 215:40: -> { $subProgDecl.st }
                    	{
                    	    retval.ST =  ((subProgDecl != null) ? subProgDecl.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "basic_declaration"

    public class defining_identifier_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "defining_identifier"
    // Ada95Walker.g:218:1: defining_identifier : IDENTIFIER ;
    public Ada95Walker.defining_identifier_return defining_identifier() // throws RecognitionException [1]
    {   
        Ada95Walker.defining_identifier_return retval = new Ada95Walker.defining_identifier_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:219:2: ( IDENTIFIER )
            // Ada95Walker.g:219:4: IDENTIFIER
            {
            	Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_defining_identifier202); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "defining_identifier"

    public class type_declaration_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "type_declaration"
    // Ada95Walker.g:221:1: type_declaration : full_type_declaration ;
    public Ada95Walker.type_declaration_return type_declaration() // throws RecognitionException [1]
    {   
        Ada95Walker.type_declaration_return retval = new Ada95Walker.type_declaration_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:222:2: ( full_type_declaration )
            // Ada95Walker.g:222:4: full_type_declaration
            {
            	PushFollow(FOLLOW_full_type_declaration_in_type_declaration212);
            	full_type_declaration();
            	state.followingStackPointer--;


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "type_declaration"

    protected class full_type_declaration_scope 
    {
        protected internal string typeName;
    }
    protected Stack full_type_declaration_stack = new Stack();

    public class full_type_declaration_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "full_type_declaration"
    // Ada95Walker.g:224:1: full_type_declaration : ^( TYPE id= defining_identifier type_definition ) ;
    public Ada95Walker.full_type_declaration_return full_type_declaration() // throws RecognitionException [1]
    {   
        full_type_declaration_stack.Push(new full_type_declaration_scope());
        Ada95Walker.full_type_declaration_return retval = new Ada95Walker.full_type_declaration_return();
        retval.Start = input.LT(1);

        Ada95Walker.defining_identifier_return id = default(Ada95Walker.defining_identifier_return);


        try 
    	{
            // Ada95Walker.g:229:2: ( ^( TYPE id= defining_identifier type_definition ) )
            // Ada95Walker.g:230:3: ^( TYPE id= defining_identifier type_definition )
            {
            	Match(input,TYPE,FOLLOW_TYPE_in_full_type_declaration232); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_defining_identifier_in_full_type_declaration238);
            	id = defining_identifier();
            	state.followingStackPointer--;

            	 ((full_type_declaration_scope)full_type_declaration_stack.Peek()).typeName =  ((id != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(id.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(id.Start)) : null); 
            	PushFollow(FOLLOW_type_definition_in_full_type_declaration242);
            	type_definition();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            full_type_declaration_stack.Pop();
        }
        return retval;
    }
    // $ANTLR end "full_type_declaration"

    public class type_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "type_definition"
    // Ada95Walker.g:233:1: type_definition : ( enumeration_type_definition | integer_type_definition | array_type_definition | record_type_definition | derived_type_definition );
    public Ada95Walker.type_definition_return type_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.type_definition_return retval = new Ada95Walker.type_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:234:2: ( enumeration_type_definition | integer_type_definition | array_type_definition | record_type_definition | derived_type_definition )
            int alt2 = 5;
            switch ( input.LA(1) ) 
            {
            case ENUMERATION:
            	{
                alt2 = 1;
                }
                break;
            case RANGE:
            case MOD:
            	{
                alt2 = 2;
                }
                break;
            case UNCONSTRAINED_ARRAY:
            case CONSTRAINED_ARRAY:
            	{
                alt2 = 3;
                }
                break;
            case RECORD:
            	{
                alt2 = 4;
                }
                break;
            case NEW:
            	{
                alt2 = 5;
                }
                break;
            	default:
            	    NoViableAltException nvae_d2s0 =
            	        new NoViableAltException("", 2, 0, input);

            	    throw nvae_d2s0;
            }

            switch (alt2) 
            {
                case 1 :
                    // Ada95Walker.g:235:3: enumeration_type_definition
                    {
                    	PushFollow(FOLLOW_enumeration_type_definition_in_type_definition257);
                    	enumeration_type_definition();
                    	state.followingStackPointer--;


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:236:3: integer_type_definition
                    {
                    	PushFollow(FOLLOW_integer_type_definition_in_type_definition263);
                    	integer_type_definition();
                    	state.followingStackPointer--;


                    }
                    break;
                case 3 :
                    // Ada95Walker.g:238:3: array_type_definition
                    {
                    	PushFollow(FOLLOW_array_type_definition_in_type_definition272);
                    	array_type_definition();
                    	state.followingStackPointer--;


                    }
                    break;
                case 4 :
                    // Ada95Walker.g:239:3: record_type_definition
                    {
                    	PushFollow(FOLLOW_record_type_definition_in_type_definition278);
                    	record_type_definition();
                    	state.followingStackPointer--;


                    }
                    break;
                case 5 :
                    // Ada95Walker.g:241:3: derived_type_definition
                    {
                    	PushFollow(FOLLOW_derived_type_definition_in_type_definition287);
                    	derived_type_definition();
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "type_definition"

    public class subtype_declaration_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "subtype_declaration"
    // Ada95Walker.g:244:1: subtype_declaration : ^( SUBTYPE defining_identifier subtype_indication ) ;
    public Ada95Walker.subtype_declaration_return subtype_declaration() // throws RecognitionException [1]
    {   
        Ada95Walker.subtype_declaration_return retval = new Ada95Walker.subtype_declaration_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:245:2: ( ^( SUBTYPE defining_identifier subtype_indication ) )
            // Ada95Walker.g:245:4: ^( SUBTYPE defining_identifier subtype_indication )
            {
            	Match(input,SUBTYPE,FOLLOW_SUBTYPE_in_subtype_declaration300); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_defining_identifier_in_subtype_declaration302);
            	defining_identifier();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_subtype_indication_in_subtype_declaration304);
            	subtype_indication();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "subtype_declaration"

    public class subtype_mark_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "subtype_mark"
    // Ada95Walker.g:247:1: subtype_mark : IDENTIFIER ;
    public Ada95Walker.subtype_mark_return subtype_mark() // throws RecognitionException [1]
    {   
        Ada95Walker.subtype_mark_return retval = new Ada95Walker.subtype_mark_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:248:2: ( IDENTIFIER )
            // Ada95Walker.g:248:4: IDENTIFIER
            {
            	Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_subtype_mark316); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "subtype_mark"

    public class subtype_indication_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "subtype_indication"
    // Ada95Walker.g:250:1: subtype_indication : ^( subtype_mark ( constraint )? ) ;
    public Ada95Walker.subtype_indication_return subtype_indication() // throws RecognitionException [1]
    {   
        Ada95Walker.subtype_indication_return retval = new Ada95Walker.subtype_indication_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:251:2: ( ^( subtype_mark ( constraint )? ) )
            // Ada95Walker.g:251:4: ^( subtype_mark ( constraint )? )
            {
            	PushFollow(FOLLOW_subtype_mark_in_subtype_indication328);
            	subtype_mark();
            	state.followingStackPointer--;


            	if ( input.LA(1) == Token.DOWN )
            	{
            	    Match(input, Token.DOWN, null); 
            	    // Ada95Walker.g:251:20: ( constraint )?
            	    int alt3 = 2;
            	    int LA3_0 = input.LA(1);

            	    if ( (LA3_0 == RANGE || LA3_0 == DIGITS) )
            	    {
            	        alt3 = 1;
            	    }
            	    switch (alt3) 
            	    {
            	        case 1 :
            	            // Ada95Walker.g:251:20: constraint
            	            {
            	            	PushFollow(FOLLOW_constraint_in_subtype_indication330);
            	            	constraint();
            	            	state.followingStackPointer--;


            	            }
            	            break;

            	    }


            	    Match(input, Token.UP, null); 
            	}

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "subtype_indication"

    public class constraint_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "constraint"
    // Ada95Walker.g:253:1: constraint : scalar_constraint ;
    public Ada95Walker.constraint_return constraint() // throws RecognitionException [1]
    {   
        Ada95Walker.constraint_return retval = new Ada95Walker.constraint_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:254:2: ( scalar_constraint )
            // Ada95Walker.g:255:3: scalar_constraint
            {
            	PushFollow(FOLLOW_scalar_constraint_in_constraint345);
            	scalar_constraint();
            	state.followingStackPointer--;


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "constraint"

    public class scalar_constraint_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "scalar_constraint"
    // Ada95Walker.g:259:1: scalar_constraint : ( range_constraint | digits_constraint );
    public Ada95Walker.scalar_constraint_return scalar_constraint() // throws RecognitionException [1]
    {   
        Ada95Walker.scalar_constraint_return retval = new Ada95Walker.scalar_constraint_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:260:2: ( range_constraint | digits_constraint )
            int alt4 = 2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0 == RANGE) )
            {
                alt4 = 1;
            }
            else if ( (LA4_0 == DIGITS) )
            {
                alt4 = 2;
            }
            else 
            {
                NoViableAltException nvae_d4s0 =
                    new NoViableAltException("", 4, 0, input);

                throw nvae_d4s0;
            }
            switch (alt4) 
            {
                case 1 :
                    // Ada95Walker.g:261:3: range_constraint
                    {
                    	PushFollow(FOLLOW_range_constraint_in_scalar_constraint362);
                    	range_constraint();
                    	state.followingStackPointer--;


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:262:3: digits_constraint
                    {
                    	PushFollow(FOLLOW_digits_constraint_in_scalar_constraint368);
                    	digits_constraint();
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "scalar_constraint"

    public class number_declaration_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "number_declaration"
    // Ada95Walker.g:269:1: number_declaration : ^( CONSTANT defining_identifier_list expression[ out type ] ) ;
    public Ada95Walker.number_declaration_return number_declaration() // throws RecognitionException [1]
    {   
        Ada95Walker.number_declaration_return retval = new Ada95Walker.number_declaration_return();
        retval.Start = input.LT(1);


        		string type;
        	
        try 
    	{
            // Ada95Walker.g:274:2: ( ^( CONSTANT defining_identifier_list expression[ out type ] ) )
            // Ada95Walker.g:274:4: ^( CONSTANT defining_identifier_list expression[ out type ] )
            {
            	Match(input,CONSTANT,FOLLOW_CONSTANT_in_number_declaration389); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_defining_identifier_list_in_number_declaration391);
            	defining_identifier_list();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_expression_in_number_declaration393);
            	expression(out type);
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "number_declaration"

    public class derived_type_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "derived_type_definition"
    // Ada95Walker.g:276:1: derived_type_definition : ^( NEW subtype_indication ( record_extension_part )? ( ABSTRACT )? ) ;
    public Ada95Walker.derived_type_definition_return derived_type_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.derived_type_definition_return retval = new Ada95Walker.derived_type_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:277:2: ( ^( NEW subtype_indication ( record_extension_part )? ( ABSTRACT )? ) )
            // Ada95Walker.g:277:4: ^( NEW subtype_indication ( record_extension_part )? ( ABSTRACT )? )
            {
            	Match(input,NEW,FOLLOW_NEW_in_derived_type_definition408); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_subtype_indication_in_derived_type_definition410);
            	subtype_indication();
            	state.followingStackPointer--;

            	// Ada95Walker.g:277:30: ( record_extension_part )?
            	int alt5 = 2;
            	int LA5_0 = input.LA(1);

            	if ( (LA5_0 == WITH) )
            	{
            	    alt5 = 1;
            	}
            	switch (alt5) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:277:30: record_extension_part
            	        {
            	        	PushFollow(FOLLOW_record_extension_part_in_derived_type_definition412);
            	        	record_extension_part();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}

            	// Ada95Walker.g:277:53: ( ABSTRACT )?
            	int alt6 = 2;
            	int LA6_0 = input.LA(1);

            	if ( (LA6_0 == ABSTRACT) )
            	{
            	    alt6 = 1;
            	}
            	switch (alt6) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:277:53: ABSTRACT
            	        {
            	        	Match(input,ABSTRACT,FOLLOW_ABSTRACT_in_derived_type_definition415); 

            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "derived_type_definition"

    public class defining_identifier_list_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "defining_identifier_list"
    // Ada95Walker.g:280:1: defining_identifier_list : ^( ID_LIST (id= defining_identifier )+ ) ;
    public Ada95Walker.defining_identifier_list_return defining_identifier_list() // throws RecognitionException [1]
    {   
        Ada95Walker.defining_identifier_list_return retval = new Ada95Walker.defining_identifier_list_return();
        retval.Start = input.LT(1);

        Ada95Walker.defining_identifier_return id = default(Ada95Walker.defining_identifier_return);


        try 
    	{
            // Ada95Walker.g:281:2: ( ^( ID_LIST (id= defining_identifier )+ ) )
            // Ada95Walker.g:281:4: ^( ID_LIST (id= defining_identifier )+ )
            {
            	Match(input,ID_LIST,FOLLOW_ID_LIST_in_defining_identifier_list431); 

            	Match(input, Token.DOWN, null); 
            	// Ada95Walker.g:281:15: (id= defining_identifier )+
            	int cnt7 = 0;
            	do 
            	{
            	    int alt7 = 2;
            	    int LA7_0 = input.LA(1);

            	    if ( (LA7_0 == IDENTIFIER) )
            	    {
            	        alt7 = 1;
            	    }


            	    switch (alt7) 
            		{
            			case 1 :
            			    // Ada95Walker.g:281:17: id= defining_identifier
            			    {
            			    	PushFollow(FOLLOW_defining_identifier_in_defining_identifier_list439);
            			    	id = defining_identifier();
            			    	state.followingStackPointer--;

            			    	 ((Symbols_scope)Symbols_stack.Peek()).IDs.Add( ((id != null) ? input.TokenStream.ToString(
            			    	  input.TreeAdaptor.GetTokenStartIndex(id.Start),
            			    	  input.TreeAdaptor.GetTokenStopIndex(id.Start)) : null) ); 

            			    }
            			    break;

            			default:
            			    if ( cnt7 >= 1 ) goto loop7;
            		            EarlyExitException eee7 =
            		                new EarlyExitException(7, input);
            		            throw eee7;
            	    }
            	    cnt7++;
            	} while (true);

            	loop7:
            		;	// Stops C# compiler whining that label 'loop7' has no statements


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "defining_identifier_list"

    public class object_declaration_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "object_declaration"
    // Ada95Walker.g:283:1: object_declaration : ( ^( OBJECT defining_identifier_list subtype= subtype_indication (e= expression[ out exprType ] )? ) -> Object(variables= $Symbols::IDs type= type initExpr= $e.st ) | ^( ARRAY_OBJECT defining_identifier_list arrayType= array_type_definition (e= expression[ out exprType ] )? ) -> { $arrayType.st });
    public Ada95Walker.object_declaration_return object_declaration() // throws RecognitionException [1]
    {   
        Symbols_stack.Push(new Symbols_scope());

        Ada95Walker.object_declaration_return retval = new Ada95Walker.object_declaration_return();
        retval.Start = input.LT(1);

        Ada95Walker.subtype_indication_return subtype = default(Ada95Walker.subtype_indication_return);

        Ada95Walker.expression_return e = default(Ada95Walker.expression_return);

        Ada95Walker.array_type_definition_return arrayType = default(Ada95Walker.array_type_definition_return);



        		((Symbols_scope)Symbols_stack.Peek()).IDs =  new SortedSet< string >();
        		string type;
        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:291:2: ( ^( OBJECT defining_identifier_list subtype= subtype_indication (e= expression[ out exprType ] )? ) -> Object(variables= $Symbols::IDs type= type initExpr= $e.st ) | ^( ARRAY_OBJECT defining_identifier_list arrayType= array_type_definition (e= expression[ out exprType ] )? ) -> { $arrayType.st })
            int alt10 = 2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0 == OBJECT) )
            {
                alt10 = 1;
            }
            else if ( (LA10_0 == ARRAY_OBJECT) )
            {
                alt10 = 2;
            }
            else 
            {
                NoViableAltException nvae_d10s0 =
                    new NoViableAltException("", 10, 0, input);

                throw nvae_d10s0;
            }
            switch (alt10) 
            {
                case 1 :
                    // Ada95Walker.g:292:3: ^( OBJECT defining_identifier_list subtype= subtype_indication (e= expression[ out exprType ] )? )
                    {
                    	Match(input,OBJECT,FOLLOW_OBJECT_in_object_declaration473); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_defining_identifier_list_in_object_declaration475);
                    	defining_identifier_list();
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_subtype_indication_in_object_declaration481);
                    	subtype = subtype_indication();
                    	state.followingStackPointer--;

                    	// Ada95Walker.g:292:67: (e= expression[ out exprType ] )?
                    	int alt8 = 2;
                    	int LA8_0 = input.LA(1);

                    	if ( (LA8_0 == EXPRESSION) )
                    	{
                    	    alt8 = 1;
                    	}
                    	switch (alt8) 
                    	{
                    	    case 1 :
                    	        // Ada95Walker.g:292:69: e= expression[ out exprType ]
                    	        {
                    	        	PushFollow(FOLLOW_expression_in_object_declaration489);
                    	        	e = expression(out exprType);
                    	        	state.followingStackPointer--;


                    	        }
                    	        break;

                    	}


                    	Match(input, Token.UP, null); 

                    					type = GetType( ((subtype != null) ? input.TokenStream.ToString(
                    	  input.TreeAdaptor.GetTokenStartIndex(subtype.Start),
                    	  input.TreeAdaptor.GetTokenStopIndex(subtype.Start)) : null) );
                    					foreach ( string id in ((Symbols_scope)Symbols_stack.Peek()).IDs )
                    						((SymbolTable_scope)SymbolTable_stack.Peek()).Objects.Add( id, new Object( id, type ) );
                    				


                    	// TEMPLATE REWRITE
                    	// 298:4: -> Object(variables= $Symbols::IDs type= type initExpr= $e.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Object",
                    	  new STAttrMap().Add("variables",  ((Symbols_scope)Symbols_stack.Peek()).IDs ).Add("type",  type ).Add("initExpr",  ((e != null) ? e.ST : null) ));
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:299:3: ^( ARRAY_OBJECT defining_identifier_list arrayType= array_type_definition (e= expression[ out exprType ] )? )
                    {
                    	Match(input,ARRAY_OBJECT,FOLLOW_ARRAY_OBJECT_in_object_declaration538); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_defining_identifier_list_in_object_declaration540);
                    	defining_identifier_list();
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_array_type_definition_in_object_declaration546);
                    	arrayType = array_type_definition();
                    	state.followingStackPointer--;

                    	// Ada95Walker.g:299:78: (e= expression[ out exprType ] )?
                    	int alt9 = 2;
                    	int LA9_0 = input.LA(1);

                    	if ( (LA9_0 == EXPRESSION) )
                    	{
                    	    alt9 = 1;
                    	}
                    	switch (alt9) 
                    	{
                    	    case 1 :
                    	        // Ada95Walker.g:299:80: e= expression[ out exprType ]
                    	        {
                    	        	PushFollow(FOLLOW_expression_in_object_declaration554);
                    	        	e = expression(out exprType);
                    	        	state.followingStackPointer--;


                    	        }
                    	        break;

                    	}


                    	Match(input, Token.UP, null); 


                    	// TEMPLATE REWRITE
                    	// 300:4: -> { $arrayType.st }
                    	{
                    	    retval.ST =  ((arrayType != null) ? arrayType.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            Symbols_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "object_declaration"

    public class range_constraint_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "range_constraint"
    // Ada95Walker.g:305:1: range_constraint : range[ out r ] ;
    public Ada95Walker.range_constraint_return range_constraint() // throws RecognitionException [1]
    {   
        Ada95Walker.range_constraint_return retval = new Ada95Walker.range_constraint_return();
        retval.Start = input.LT(1);


        		Range r;
        	
        try 
    	{
            // Ada95Walker.g:310:2: ( range[ out r ] )
            // Ada95Walker.g:310:4: range[ out r ]
            {
            	PushFollow(FOLLOW_range_in_range_constraint590);
            	range(out r);
            	state.followingStackPointer--;


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "range_constraint"

    public class range_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "range"
    // Ada95Walker.g:312:1: range[ out Range r ] : ^( RANGE a= simple_expression[ out exprType ] b= simple_expression[ out exprType ] ) ;
    public Ada95Walker.range_return range(out Range r) // throws RecognitionException [1]
    {   
        Ada95Walker.range_return retval = new Ada95Walker.range_return();
        retval.Start = input.LT(1);

        Ada95Walker.simple_expression_return a = default(Ada95Walker.simple_expression_return);

        Ada95Walker.simple_expression_return b = default(Ada95Walker.simple_expression_return);



        		r = null;
        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:318:2: ( ^( RANGE a= simple_expression[ out exprType ] b= simple_expression[ out exprType ] ) )
            // Ada95Walker.g:320:3: ^( RANGE a= simple_expression[ out exprType ] b= simple_expression[ out exprType ] )
            {
            	Match(input,RANGE,FOLLOW_RANGE_in_range616); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_simple_expression_in_range625);
            	a = simple_expression(out exprType);
            	state.followingStackPointer--;

            	 if ( exprType != "int" ) throw new UnexpectedType( exprType, "int", input ); 
            	PushFollow(FOLLOW_simple_expression_in_range637);
            	b = simple_expression(out exprType);
            	state.followingStackPointer--;

            	 if ( exprType != "int" ) throw new UnexpectedType( exprType, "int", input ); 

            	Match(input, Token.UP, null); 
            	 r = new Range( ((a != null) ? a.ST : null).ToString(), ((b != null) ? b.ST : null).ToString() ); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "range"

    public class enumeration_type_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "enumeration_type_definition"
    // Ada95Walker.g:325:1: enumeration_type_definition : ^( ENUMERATION ( enumeration_literal_specification )+ ) ;
    public Ada95Walker.enumeration_type_definition_return enumeration_type_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.enumeration_type_definition_return retval = new Ada95Walker.enumeration_type_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:326:2: ( ^( ENUMERATION ( enumeration_literal_specification )+ ) )
            // Ada95Walker.g:326:4: ^( ENUMERATION ( enumeration_literal_specification )+ )
            {
            	Match(input,ENUMERATION,FOLLOW_ENUMERATION_in_enumeration_type_definition658); 

            	Match(input, Token.DOWN, null); 
            	// Ada95Walker.g:326:19: ( enumeration_literal_specification )+
            	int cnt11 = 0;
            	do 
            	{
            	    int alt11 = 2;
            	    int LA11_0 = input.LA(1);

            	    if ( (LA11_0 == IDENTIFIER || LA11_0 == CHARACTER_LITERAL) )
            	    {
            	        alt11 = 1;
            	    }


            	    switch (alt11) 
            		{
            			case 1 :
            			    // Ada95Walker.g:326:19: enumeration_literal_specification
            			    {
            			    	PushFollow(FOLLOW_enumeration_literal_specification_in_enumeration_type_definition660);
            			    	enumeration_literal_specification();
            			    	state.followingStackPointer--;


            			    }
            			    break;

            			default:
            			    if ( cnt11 >= 1 ) goto loop11;
            		            EarlyExitException eee11 =
            		                new EarlyExitException(11, input);
            		            throw eee11;
            	    }
            	    cnt11++;
            	} while (true);

            	loop11:
            		;	// Stops C# compiler whining that label 'loop11' has no statements


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "enumeration_type_definition"

    public class enumeration_literal_specification_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "enumeration_literal_specification"
    // Ada95Walker.g:328:1: enumeration_literal_specification : ( defining_identifier | defining_character_literal );
    public Ada95Walker.enumeration_literal_specification_return enumeration_literal_specification() // throws RecognitionException [1]
    {   
        Ada95Walker.enumeration_literal_specification_return retval = new Ada95Walker.enumeration_literal_specification_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:329:2: ( defining_identifier | defining_character_literal )
            int alt12 = 2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0 == IDENTIFIER) )
            {
                alt12 = 1;
            }
            else if ( (LA12_0 == CHARACTER_LITERAL) )
            {
                alt12 = 2;
            }
            else 
            {
                NoViableAltException nvae_d12s0 =
                    new NoViableAltException("", 12, 0, input);

                throw nvae_d12s0;
            }
            switch (alt12) 
            {
                case 1 :
                    // Ada95Walker.g:330:3: defining_identifier
                    {
                    	PushFollow(FOLLOW_defining_identifier_in_enumeration_literal_specification675);
                    	defining_identifier();
                    	state.followingStackPointer--;


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:331:3: defining_character_literal
                    {
                    	PushFollow(FOLLOW_defining_character_literal_in_enumeration_literal_specification681);
                    	defining_character_literal();
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "enumeration_literal_specification"

    public class defining_character_literal_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "defining_character_literal"
    // Ada95Walker.g:334:1: defining_character_literal : CHARACTER_LITERAL ;
    public Ada95Walker.defining_character_literal_return defining_character_literal() // throws RecognitionException [1]
    {   
        Ada95Walker.defining_character_literal_return retval = new Ada95Walker.defining_character_literal_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:335:2: ( CHARACTER_LITERAL )
            // Ada95Walker.g:335:4: CHARACTER_LITERAL
            {
            	Match(input,CHARACTER_LITERAL,FOLLOW_CHARACTER_LITERAL_in_defining_character_literal692); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "defining_character_literal"

    public class integer_type_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "integer_type_definition"
    // Ada95Walker.g:337:1: integer_type_definition : ( signed_integer_type_definition | modular_type_definition );
    public Ada95Walker.integer_type_definition_return integer_type_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.integer_type_definition_return retval = new Ada95Walker.integer_type_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:338:2: ( signed_integer_type_definition | modular_type_definition )
            int alt13 = 2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0 == RANGE) )
            {
                alt13 = 1;
            }
            else if ( (LA13_0 == MOD) )
            {
                alt13 = 2;
            }
            else 
            {
                NoViableAltException nvae_d13s0 =
                    new NoViableAltException("", 13, 0, input);

                throw nvae_d13s0;
            }
            switch (alt13) 
            {
                case 1 :
                    // Ada95Walker.g:339:3: signed_integer_type_definition
                    {
                    	PushFollow(FOLLOW_signed_integer_type_definition_in_integer_type_definition704);
                    	signed_integer_type_definition();
                    	state.followingStackPointer--;


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:340:3: modular_type_definition
                    {
                    	PushFollow(FOLLOW_modular_type_definition_in_integer_type_definition710);
                    	modular_type_definition();
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "integer_type_definition"

    public class signed_integer_type_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "signed_integer_type_definition"
    // Ada95Walker.g:343:1: signed_integer_type_definition : ^( RANGE a= simple_expression[ out temp ] b= simple_expression[ out temp ] ) ;
    public Ada95Walker.signed_integer_type_definition_return signed_integer_type_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.signed_integer_type_definition_return retval = new Ada95Walker.signed_integer_type_definition_return();
        retval.Start = input.LT(1);

        Ada95Walker.simple_expression_return a = default(Ada95Walker.simple_expression_return);

        Ada95Walker.simple_expression_return b = default(Ada95Walker.simple_expression_return);



        		string temp;
        	
        try 
    	{
            // Ada95Walker.g:348:2: ( ^( RANGE a= simple_expression[ out temp ] b= simple_expression[ out temp ] ) )
            // Ada95Walker.g:348:4: ^( RANGE a= simple_expression[ out temp ] b= simple_expression[ out temp ] )
            {
            	Match(input,RANGE,FOLLOW_RANGE_in_signed_integer_type_definition730); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_simple_expression_in_signed_integer_type_definition736);
            	a = simple_expression(out temp);
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_simple_expression_in_signed_integer_type_definition743);
            	b = simple_expression(out temp);
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "signed_integer_type_definition"

    public class modular_type_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "modular_type_definition"
    // Ada95Walker.g:350:1: modular_type_definition : ^( MOD expression[ out exprType ] ) ;
    public Ada95Walker.modular_type_definition_return modular_type_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.modular_type_definition_return retval = new Ada95Walker.modular_type_definition_return();
        retval.Start = input.LT(1);


        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:355:2: ( ^( MOD expression[ out exprType ] ) )
            // Ada95Walker.g:355:4: ^( MOD expression[ out exprType ] )
            {
            	Match(input,MOD,FOLLOW_MOD_in_modular_type_definition765); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_expression_in_modular_type_definition767);
            	expression(out exprType);
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "modular_type_definition"

    public class real_type_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "real_type_definition"
    // Ada95Walker.g:357:1: real_type_definition : ( floating_point_definition | fixed_point_definition );
    public Ada95Walker.real_type_definition_return real_type_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.real_type_definition_return retval = new Ada95Walker.real_type_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:358:2: ( floating_point_definition | fixed_point_definition )
            int alt14 = 2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0 == DIGITS) )
            {
                alt14 = 1;
            }
            else if ( ((LA14_0 >= ORDINARY_FIXED && LA14_0 <= DECIMAL_FIXED)) )
            {
                alt14 = 2;
            }
            else 
            {
                NoViableAltException nvae_d14s0 =
                    new NoViableAltException("", 14, 0, input);

                throw nvae_d14s0;
            }
            switch (alt14) 
            {
                case 1 :
                    // Ada95Walker.g:359:3: floating_point_definition
                    {
                    	PushFollow(FOLLOW_floating_point_definition_in_real_type_definition782);
                    	floating_point_definition();
                    	state.followingStackPointer--;


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:360:3: fixed_point_definition
                    {
                    	PushFollow(FOLLOW_fixed_point_definition_in_real_type_definition788);
                    	fixed_point_definition();
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "real_type_definition"

    public class floating_point_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "floating_point_definition"
    // Ada95Walker.g:363:1: floating_point_definition : ^( DIGITS expression[ out exprType ] ( real_range_specification )? ) ;
    public Ada95Walker.floating_point_definition_return floating_point_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.floating_point_definition_return retval = new Ada95Walker.floating_point_definition_return();
        retval.Start = input.LT(1);


        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:368:2: ( ^( DIGITS expression[ out exprType ] ( real_range_specification )? ) )
            // Ada95Walker.g:368:4: ^( DIGITS expression[ out exprType ] ( real_range_specification )? )
            {
            	Match(input,DIGITS,FOLLOW_DIGITS_in_floating_point_definition808); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_expression_in_floating_point_definition810);
            	expression(out exprType);
            	state.followingStackPointer--;

            	// Ada95Walker.g:368:41: ( real_range_specification )?
            	int alt15 = 2;
            	int LA15_0 = input.LA(1);

            	if ( (LA15_0 == RANGE) )
            	{
            	    alt15 = 1;
            	}
            	switch (alt15) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:368:41: real_range_specification
            	        {
            	        	PushFollow(FOLLOW_real_range_specification_in_floating_point_definition813);
            	        	real_range_specification();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "floating_point_definition"

    public class real_range_specification_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "real_range_specification"
    // Ada95Walker.g:370:1: real_range_specification : ^( RANGE simple_expression[ out temp ] simple_expression[ out temp ] ) ;
    public Ada95Walker.real_range_specification_return real_range_specification() // throws RecognitionException [1]
    {   
        Ada95Walker.real_range_specification_return retval = new Ada95Walker.real_range_specification_return();
        retval.Start = input.LT(1);


        		string temp;
        	
        try 
    	{
            // Ada95Walker.g:375:2: ( ^( RANGE simple_expression[ out temp ] simple_expression[ out temp ] ) )
            // Ada95Walker.g:375:4: ^( RANGE simple_expression[ out temp ] simple_expression[ out temp ] )
            {
            	Match(input,RANGE,FOLLOW_RANGE_in_real_range_specification835); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_simple_expression_in_real_range_specification837);
            	simple_expression(out temp);
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_simple_expression_in_real_range_specification840);
            	simple_expression(out temp);
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "real_range_specification"

    public class fixed_point_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "fixed_point_definition"
    // Ada95Walker.g:377:1: fixed_point_definition : ( ordinary_fixed_point_definition | decimal_fixed_point_definition );
    public Ada95Walker.fixed_point_definition_return fixed_point_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.fixed_point_definition_return retval = new Ada95Walker.fixed_point_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:378:2: ( ordinary_fixed_point_definition | decimal_fixed_point_definition )
            int alt16 = 2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0 == ORDINARY_FIXED) )
            {
                alt16 = 1;
            }
            else if ( (LA16_0 == DECIMAL_FIXED) )
            {
                alt16 = 2;
            }
            else 
            {
                NoViableAltException nvae_d16s0 =
                    new NoViableAltException("", 16, 0, input);

                throw nvae_d16s0;
            }
            switch (alt16) 
            {
                case 1 :
                    // Ada95Walker.g:379:3: ordinary_fixed_point_definition
                    {
                    	PushFollow(FOLLOW_ordinary_fixed_point_definition_in_fixed_point_definition855);
                    	ordinary_fixed_point_definition();
                    	state.followingStackPointer--;


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:380:3: decimal_fixed_point_definition
                    {
                    	PushFollow(FOLLOW_decimal_fixed_point_definition_in_fixed_point_definition861);
                    	decimal_fixed_point_definition();
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "fixed_point_definition"

    public class ordinary_fixed_point_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "ordinary_fixed_point_definition"
    // Ada95Walker.g:383:1: ordinary_fixed_point_definition : ^( ORDINARY_FIXED expression[ out exprType ] real_range_specification ) ;
    public Ada95Walker.ordinary_fixed_point_definition_return ordinary_fixed_point_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.ordinary_fixed_point_definition_return retval = new Ada95Walker.ordinary_fixed_point_definition_return();
        retval.Start = input.LT(1);


        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:388:2: ( ^( ORDINARY_FIXED expression[ out exprType ] real_range_specification ) )
            // Ada95Walker.g:388:4: ^( ORDINARY_FIXED expression[ out exprType ] real_range_specification )
            {
            	Match(input,ORDINARY_FIXED,FOLLOW_ORDINARY_FIXED_in_ordinary_fixed_point_definition881); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_expression_in_ordinary_fixed_point_definition883);
            	expression(out exprType);
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_real_range_specification_in_ordinary_fixed_point_definition886);
            	real_range_specification();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "ordinary_fixed_point_definition"

    public class decimal_fixed_point_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "decimal_fixed_point_definition"
    // Ada95Walker.g:390:1: decimal_fixed_point_definition : ^( DECIMAL_FIXED expression[ out temp ] expression[ out temp ] ( real_range_specification )? ) ;
    public Ada95Walker.decimal_fixed_point_definition_return decimal_fixed_point_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.decimal_fixed_point_definition_return retval = new Ada95Walker.decimal_fixed_point_definition_return();
        retval.Start = input.LT(1);


        		string temp;
        	
        try 
    	{
            // Ada95Walker.g:395:2: ( ^( DECIMAL_FIXED expression[ out temp ] expression[ out temp ] ( real_range_specification )? ) )
            // Ada95Walker.g:395:4: ^( DECIMAL_FIXED expression[ out temp ] expression[ out temp ] ( real_range_specification )? )
            {
            	Match(input,DECIMAL_FIXED,FOLLOW_DECIMAL_FIXED_in_decimal_fixed_point_definition907); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_expression_in_decimal_fixed_point_definition909);
            	expression(out temp);
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_expression_in_decimal_fixed_point_definition912);
            	expression(out temp);
            	state.followingStackPointer--;

            	// Ada95Walker.g:395:67: ( real_range_specification )?
            	int alt17 = 2;
            	int LA17_0 = input.LA(1);

            	if ( (LA17_0 == RANGE) )
            	{
            	    alt17 = 1;
            	}
            	switch (alt17) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:395:67: real_range_specification
            	        {
            	        	PushFollow(FOLLOW_real_range_specification_in_decimal_fixed_point_definition915);
            	        	real_range_specification();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "decimal_fixed_point_definition"

    public class digits_constraint_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "digits_constraint"
    // Ada95Walker.g:397:1: digits_constraint : ^( DIGITS expression[ out exprType ] ( range_constraint )? ) ;
    public Ada95Walker.digits_constraint_return digits_constraint() // throws RecognitionException [1]
    {   
        Ada95Walker.digits_constraint_return retval = new Ada95Walker.digits_constraint_return();
        retval.Start = input.LT(1);


        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:402:2: ( ^( DIGITS expression[ out exprType ] ( range_constraint )? ) )
            // Ada95Walker.g:402:4: ^( DIGITS expression[ out exprType ] ( range_constraint )? )
            {
            	Match(input,DIGITS,FOLLOW_DIGITS_in_digits_constraint937); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_expression_in_digits_constraint939);
            	expression(out exprType);
            	state.followingStackPointer--;

            	// Ada95Walker.g:402:41: ( range_constraint )?
            	int alt18 = 2;
            	int LA18_0 = input.LA(1);

            	if ( (LA18_0 == RANGE) )
            	{
            	    alt18 = 1;
            	}
            	switch (alt18) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:402:41: range_constraint
            	        {
            	        	PushFollow(FOLLOW_range_constraint_in_digits_constraint942);
            	        	range_constraint();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "digits_constraint"

    public class array_type_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "array_type_definition"
    // Ada95Walker.g:404:1: array_type_definition : ( unconstrained_array_definition | c= constrained_array_definition -> { $c.st });
    public Ada95Walker.array_type_definition_return array_type_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.array_type_definition_return retval = new Ada95Walker.array_type_definition_return();
        retval.Start = input.LT(1);

        Ada95Walker.constrained_array_definition_return c = default(Ada95Walker.constrained_array_definition_return);


        try 
    	{
            // Ada95Walker.g:405:2: ( unconstrained_array_definition | c= constrained_array_definition -> { $c.st })
            int alt19 = 2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0 == UNCONSTRAINED_ARRAY) )
            {
                alt19 = 1;
            }
            else if ( (LA19_0 == CONSTRAINED_ARRAY) )
            {
                alt19 = 2;
            }
            else 
            {
                NoViableAltException nvae_d19s0 =
                    new NoViableAltException("", 19, 0, input);

                throw nvae_d19s0;
            }
            switch (alt19) 
            {
                case 1 :
                    // Ada95Walker.g:406:3: unconstrained_array_definition
                    {
                    	PushFollow(FOLLOW_unconstrained_array_definition_in_array_type_definition958);
                    	unconstrained_array_definition();
                    	state.followingStackPointer--;


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:407:3: c= constrained_array_definition
                    {
                    	PushFollow(FOLLOW_constrained_array_definition_in_array_type_definition968);
                    	c = constrained_array_definition();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 407:36: -> { $c.st }
                    	{
                    	    retval.ST =  ((c != null) ? c.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "array_type_definition"

    public class unconstrained_array_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "unconstrained_array_definition"
    // Ada95Walker.g:410:1: unconstrained_array_definition : ^( UNCONSTRAINED_ARRAY ( index_subtype_definition )+ OF component_definition ) ;
    public Ada95Walker.unconstrained_array_definition_return unconstrained_array_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.unconstrained_array_definition_return retval = new Ada95Walker.unconstrained_array_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:411:2: ( ^( UNCONSTRAINED_ARRAY ( index_subtype_definition )+ OF component_definition ) )
            // Ada95Walker.g:411:4: ^( UNCONSTRAINED_ARRAY ( index_subtype_definition )+ OF component_definition )
            {
            	Match(input,UNCONSTRAINED_ARRAY,FOLLOW_UNCONSTRAINED_ARRAY_in_unconstrained_array_definition985); 

            	Match(input, Token.DOWN, null); 
            	// Ada95Walker.g:411:27: ( index_subtype_definition )+
            	int cnt20 = 0;
            	do 
            	{
            	    int alt20 = 2;
            	    int LA20_0 = input.LA(1);

            	    if ( (LA20_0 == IDENTIFIER) )
            	    {
            	        alt20 = 1;
            	    }


            	    switch (alt20) 
            		{
            			case 1 :
            			    // Ada95Walker.g:411:27: index_subtype_definition
            			    {
            			    	PushFollow(FOLLOW_index_subtype_definition_in_unconstrained_array_definition987);
            			    	index_subtype_definition();
            			    	state.followingStackPointer--;


            			    }
            			    break;

            			default:
            			    if ( cnt20 >= 1 ) goto loop20;
            		            EarlyExitException eee20 =
            		                new EarlyExitException(20, input);
            		            throw eee20;
            	    }
            	    cnt20++;
            	} while (true);

            	loop20:
            		;	// Stops C# compiler whining that label 'loop20' has no statements

            	Match(input,OF,FOLLOW_OF_in_unconstrained_array_definition990); 
            	PushFollow(FOLLOW_component_definition_in_unconstrained_array_definition992);
            	component_definition();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "unconstrained_array_definition"

    public class index_subtype_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "index_subtype_definition"
    // Ada95Walker.g:413:1: index_subtype_definition : subtype_mark RANGE BOX ;
    public Ada95Walker.index_subtype_definition_return index_subtype_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.index_subtype_definition_return retval = new Ada95Walker.index_subtype_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:414:2: ( subtype_mark RANGE BOX )
            // Ada95Walker.g:414:4: subtype_mark RANGE BOX
            {
            	PushFollow(FOLLOW_subtype_mark_in_index_subtype_definition1004);
            	subtype_mark();
            	state.followingStackPointer--;

            	Match(input,RANGE,FOLLOW_RANGE_in_index_subtype_definition1006); 
            	Match(input,BOX,FOLLOW_BOX_in_index_subtype_definition1008); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "index_subtype_definition"

    public class component_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "component_definition"
    // Ada95Walker.g:417:1: component_definition : ( ALIASED )? subtype_indication ;
    public Ada95Walker.component_definition_return component_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.component_definition_return retval = new Ada95Walker.component_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:418:2: ( ( ALIASED )? subtype_indication )
            // Ada95Walker.g:418:4: ( ALIASED )? subtype_indication
            {
            	// Ada95Walker.g:418:4: ( ALIASED )?
            	int alt21 = 2;
            	int LA21_0 = input.LA(1);

            	if ( (LA21_0 == ALIASED) )
            	{
            	    alt21 = 1;
            	}
            	switch (alt21) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:418:4: ALIASED
            	        {
            	        	Match(input,ALIASED,FOLLOW_ALIASED_in_component_definition1019); 

            	        }
            	        break;

            	}

            	PushFollow(FOLLOW_subtype_indication_in_component_definition1022);
            	subtype_indication();
            	state.followingStackPointer--;


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "component_definition"

    public class discrete_subtype_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "discrete_subtype_definition"
    // Ada95Walker.g:422:1: discrete_subtype_definition[ out Range dimension ] : range[ out dimension ] ;
    public Ada95Walker.discrete_subtype_definition_return discrete_subtype_definition(out Range dimension) // throws RecognitionException [1]
    {   
        Ada95Walker.discrete_subtype_definition_return retval = new Ada95Walker.discrete_subtype_definition_return();
        retval.Start = input.LT(1);


        		dimension = null;
        	
        try 
    	{
            // Ada95Walker.g:427:2: ( range[ out dimension ] )
            // Ada95Walker.g:429:3: range[ out dimension ]
            {
            	PushFollow(FOLLOW_range_in_discrete_subtype_definition1047);
            	range(out dimension);
            	state.followingStackPointer--;


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "discrete_subtype_definition"

    public class constrained_array_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "constrained_array_definition"
    // Ada95Walker.g:432:1: constrained_array_definition : ^( CONSTRAINED_ARRAY ( discrete_subtype_definition[ out dimension ] )+ OF componentType= component_definition ) -> ConstrainedArray(variables= $Symbols::IDs type= GetType( $componentType.text ) dimensions= dimensions );
    public Ada95Walker.constrained_array_definition_return constrained_array_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.constrained_array_definition_return retval = new Ada95Walker.constrained_array_definition_return();
        retval.Start = input.LT(1);

        Ada95Walker.component_definition_return componentType = default(Ada95Walker.component_definition_return);



        		List< Range > dimensions = new List< Range >();
        		Range dimension;
        	
        try 
    	{
            // Ada95Walker.g:438:2: ( ^( CONSTRAINED_ARRAY ( discrete_subtype_definition[ out dimension ] )+ OF componentType= component_definition ) -> ConstrainedArray(variables= $Symbols::IDs type= GetType( $componentType.text ) dimensions= dimensions ))
            // Ada95Walker.g:439:3: ^( CONSTRAINED_ARRAY ( discrete_subtype_definition[ out dimension ] )+ OF componentType= component_definition )
            {
            	Match(input,CONSTRAINED_ARRAY,FOLLOW_CONSTRAINED_ARRAY_in_constrained_array_definition1070); 

            	Match(input, Token.DOWN, null); 
            	// Ada95Walker.g:439:24: ( discrete_subtype_definition[ out dimension ] )+
            	int cnt22 = 0;
            	do 
            	{
            	    int alt22 = 2;
            	    int LA22_0 = input.LA(1);

            	    if ( (LA22_0 == RANGE) )
            	    {
            	        alt22 = 1;
            	    }


            	    switch (alt22) 
            		{
            			case 1 :
            			    // Ada95Walker.g:439:26: discrete_subtype_definition[ out dimension ]
            			    {
            			    	PushFollow(FOLLOW_discrete_subtype_definition_in_constrained_array_definition1074);
            			    	discrete_subtype_definition(out dimension);
            			    	state.followingStackPointer--;

            			    	 dimensions.Add( dimension ); 

            			    }
            			    break;

            			default:
            			    if ( cnt22 >= 1 ) goto loop22;
            		            EarlyExitException eee22 =
            		                new EarlyExitException(22, input);
            		            throw eee22;
            	    }
            	    cnt22++;
            	} while (true);

            	loop22:
            		;	// Stops C# compiler whining that label 'loop22' has no statements

            	Match(input,OF,FOLLOW_OF_in_constrained_array_definition1082); 
            	PushFollow(FOLLOW_component_definition_in_constrained_array_definition1088);
            	componentType = component_definition();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            				foreach ( string id in ((Symbols_scope)Symbols_stack.Peek()).IDs )
            					((SymbolTable_scope)SymbolTable_stack.Peek()).Objects.Add( id, new ArrayObject( id, dimensions, GetType( ((componentType != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(componentType.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(componentType.Start)) : null) ) ) );
            			


            	// TEMPLATE REWRITE
            	// 444:3: -> ConstrainedArray(variables= $Symbols::IDs type= GetType( $componentType.text ) dimensions= dimensions )
            	{
            	    retval.ST = templateLib.GetInstanceOf("ConstrainedArray",
            	  new STAttrMap().Add("variables",  ((Symbols_scope)Symbols_stack.Peek()).IDs ).Add("type",  GetType( ((componentType != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(componentType.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(componentType.Start)) : null) ) ).Add("dimensions",  dimensions ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "constrained_array_definition"

    public class discrete_range_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "discrete_range"
    // Ada95Walker.g:448:1: discrete_range : ( subtype_indication | range[ out r ] );
    public Ada95Walker.discrete_range_return discrete_range() // throws RecognitionException [1]
    {   
        Ada95Walker.discrete_range_return retval = new Ada95Walker.discrete_range_return();
        retval.Start = input.LT(1);


        		Range r;
        	
        try 
    	{
            // Ada95Walker.g:453:2: ( subtype_indication | range[ out r ] )
            int alt23 = 2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0 == IDENTIFIER) )
            {
                alt23 = 1;
            }
            else if ( (LA23_0 == RANGE) )
            {
                alt23 = 2;
            }
            else 
            {
                NoViableAltException nvae_d23s0 =
                    new NoViableAltException("", 23, 0, input);

                throw nvae_d23s0;
            }
            switch (alt23) 
            {
                case 1 :
                    // Ada95Walker.g:454:3: subtype_indication
                    {
                    	PushFollow(FOLLOW_subtype_indication_in_discrete_range1145);
                    	subtype_indication();
                    	state.followingStackPointer--;


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:455:3: range[ out r ]
                    {
                    	PushFollow(FOLLOW_range_in_discrete_range1152);
                    	range(out r);
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "discrete_range"

    public class known_discriminant_part_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "known_discriminant_part"
    // Ada95Walker.g:458:1: known_discriminant_part : ( discriminant_specification )+ ;
    public Ada95Walker.known_discriminant_part_return known_discriminant_part() // throws RecognitionException [1]
    {   
        Ada95Walker.known_discriminant_part_return retval = new Ada95Walker.known_discriminant_part_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:459:2: ( ( discriminant_specification )+ )
            // Ada95Walker.g:459:4: ( discriminant_specification )+
            {
            	// Ada95Walker.g:459:4: ( discriminant_specification )+
            	int cnt24 = 0;
            	do 
            	{
            	    int alt24 = 2;
            	    int LA24_0 = input.LA(1);

            	    if ( (LA24_0 == DISCRIMINANT_SPECIFICATION) )
            	    {
            	        alt24 = 1;
            	    }


            	    switch (alt24) 
            		{
            			case 1 :
            			    // Ada95Walker.g:459:4: discriminant_specification
            			    {
            			    	PushFollow(FOLLOW_discriminant_specification_in_known_discriminant_part1164);
            			    	discriminant_specification();
            			    	state.followingStackPointer--;


            			    }
            			    break;

            			default:
            			    if ( cnt24 >= 1 ) goto loop24;
            		            EarlyExitException eee24 =
            		                new EarlyExitException(24, input);
            		            throw eee24;
            	    }
            	    cnt24++;
            	} while (true);

            	loop24:
            		;	// Stops C# compiler whining that label 'loop24' has no statements


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "known_discriminant_part"

    public class discriminant_specification_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "discriminant_specification"
    // Ada95Walker.g:461:1: discriminant_specification : ^( DISCRIMINANT_SPECIFICATION defining_identifier_list subtype_mark ( default_expression[ out defExprType ] )? ) ;
    public Ada95Walker.discriminant_specification_return discriminant_specification() // throws RecognitionException [1]
    {   
        Symbols_stack.Push(new Symbols_scope());

        Ada95Walker.discriminant_specification_return retval = new Ada95Walker.discriminant_specification_return();
        retval.Start = input.LT(1);


        		((Symbols_scope)Symbols_stack.Peek()).IDs =  new SortedSet< string >();
        		string defExprType;
        	
        try 
    	{
            // Ada95Walker.g:468:2: ( ^( DISCRIMINANT_SPECIFICATION defining_identifier_list subtype_mark ( default_expression[ out defExprType ] )? ) )
            // Ada95Walker.g:469:3: ^( DISCRIMINANT_SPECIFICATION defining_identifier_list subtype_mark ( default_expression[ out defExprType ] )? )
            {
            	Match(input,DISCRIMINANT_SPECIFICATION,FOLLOW_DISCRIMINANT_SPECIFICATION_in_discriminant_specification1192); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_defining_identifier_list_in_discriminant_specification1194);
            	defining_identifier_list();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_subtype_mark_in_discriminant_specification1196);
            	subtype_mark();
            	state.followingStackPointer--;

            	// Ada95Walker.g:469:71: ( default_expression[ out defExprType ] )?
            	int alt25 = 2;
            	int LA25_0 = input.LA(1);

            	if ( (LA25_0 == EXPRESSION) )
            	{
            	    alt25 = 1;
            	}
            	switch (alt25) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:469:71: default_expression[ out defExprType ]
            	        {
            	        	PushFollow(FOLLOW_default_expression_in_discriminant_specification1198);
            	        	default_expression(out defExprType);
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            				foreach ( string id in ((Symbols_scope)Symbols_stack.Peek()).IDs )
            					((SymbolTable_scope)SymbolTable_stack.Peek()).Objects.Add( id, null );	// not objects! inside type declaration;
            			

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            Symbols_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "discriminant_specification"

    public class default_expression_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "default_expression"
    // Ada95Walker.g:476:1: default_expression[ out string type ] : expression[ out type ] ;
    public Ada95Walker.default_expression_return default_expression(out string type) // throws RecognitionException [1]
    {   
        Ada95Walker.default_expression_return retval = new Ada95Walker.default_expression_return();
        retval.Start = input.LT(1);


        		type = string.Empty;
        	
        try 
    	{
            // Ada95Walker.g:481:2: ( expression[ out type ] )
            // Ada95Walker.g:481:4: expression[ out type ]
            {
            	PushFollow(FOLLOW_expression_in_default_expression1225);
            	expression(out type);
            	state.followingStackPointer--;


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "default_expression"

    public class record_type_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "record_type_definition"
    // Ada95Walker.g:487:1: record_type_definition : ^( RECORD ( ( ABSTRACT )? TAGGED )? ( LIMITED )? record_definition ) ;
    public Ada95Walker.record_type_definition_return record_type_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.record_type_definition_return retval = new Ada95Walker.record_type_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:488:2: ( ^( RECORD ( ( ABSTRACT )? TAGGED )? ( LIMITED )? record_definition ) )
            // Ada95Walker.g:488:4: ^( RECORD ( ( ABSTRACT )? TAGGED )? ( LIMITED )? record_definition )
            {
            	Match(input,RECORD,FOLLOW_RECORD_in_record_type_definition1239); 

            	if ( input.LA(1) == Token.DOWN )
            	{
            	    Match(input, Token.DOWN, null); 
            	    // Ada95Walker.g:488:14: ( ( ABSTRACT )? TAGGED )?
            	    int alt27 = 2;
            	    int LA27_0 = input.LA(1);

            	    if ( (LA27_0 == ABSTRACT || LA27_0 == TAGGED) )
            	    {
            	        alt27 = 1;
            	    }
            	    switch (alt27) 
            	    {
            	        case 1 :
            	            // Ada95Walker.g:488:16: ( ABSTRACT )? TAGGED
            	            {
            	            	// Ada95Walker.g:488:16: ( ABSTRACT )?
            	            	int alt26 = 2;
            	            	int LA26_0 = input.LA(1);

            	            	if ( (LA26_0 == ABSTRACT) )
            	            	{
            	            	    alt26 = 1;
            	            	}
            	            	switch (alt26) 
            	            	{
            	            	    case 1 :
            	            	        // Ada95Walker.g:488:16: ABSTRACT
            	            	        {
            	            	        	Match(input,ABSTRACT,FOLLOW_ABSTRACT_in_record_type_definition1243); 

            	            	        }
            	            	        break;

            	            	}

            	            	Match(input,TAGGED,FOLLOW_TAGGED_in_record_type_definition1246); 

            	            }
            	            break;

            	    }

            	    // Ada95Walker.g:488:36: ( LIMITED )?
            	    int alt28 = 2;
            	    int LA28_0 = input.LA(1);

            	    if ( (LA28_0 == LIMITED) )
            	    {
            	        alt28 = 1;
            	    }
            	    switch (alt28) 
            	    {
            	        case 1 :
            	            // Ada95Walker.g:488:36: LIMITED
            	            {
            	            	Match(input,LIMITED,FOLLOW_LIMITED_in_record_type_definition1251); 

            	            }
            	            break;

            	    }

            	    PushFollow(FOLLOW_record_definition_in_record_type_definition1254);
            	    record_definition();
            	    state.followingStackPointer--;


            	    Match(input, Token.UP, null); 
            	}

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "record_type_definition"

    public class record_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "record_definition"
    // Ada95Walker.g:490:1: record_definition : ( component_list )? ;
    public Ada95Walker.record_definition_return record_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.record_definition_return retval = new Ada95Walker.record_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:491:2: ( ( component_list )? )
            // Ada95Walker.g:491:4: ( component_list )?
            {
            	// Ada95Walker.g:491:4: ( component_list )?
            	int alt29 = 2;
            	int LA29_0 = input.LA(1);

            	if ( (LA29_0 == COMPONENT_LIST) )
            	{
            	    alt29 = 1;
            	}
            	switch (alt29) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:491:4: component_list
            	        {
            	        	PushFollow(FOLLOW_component_list_in_record_definition1266);
            	        	component_list();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "record_definition"

    public class component_list_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "component_list"
    // Ada95Walker.g:493:1: component_list : ^( COMPONENT_LIST ( component_item )* ( variant_part )? ) ;
    public Ada95Walker.component_list_return component_list() // throws RecognitionException [1]
    {   
        Ada95Walker.component_list_return retval = new Ada95Walker.component_list_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:494:2: ( ^( COMPONENT_LIST ( component_item )* ( variant_part )? ) )
            // Ada95Walker.g:494:4: ^( COMPONENT_LIST ( component_item )* ( variant_part )? )
            {
            	Match(input,COMPONENT_LIST,FOLLOW_COMPONENT_LIST_in_component_list1279); 

            	if ( input.LA(1) == Token.DOWN )
            	{
            	    Match(input, Token.DOWN, null); 
            	    // Ada95Walker.g:494:22: ( component_item )*
            	    do 
            	    {
            	        int alt30 = 2;
            	        int LA30_0 = input.LA(1);

            	        if ( (LA30_0 == COMPONENT) )
            	        {
            	            alt30 = 1;
            	        }


            	        switch (alt30) 
            	    	{
            	    		case 1 :
            	    		    // Ada95Walker.g:494:22: component_item
            	    		    {
            	    		    	PushFollow(FOLLOW_component_item_in_component_list1281);
            	    		    	component_item();
            	    		    	state.followingStackPointer--;


            	    		    }
            	    		    break;

            	    		default:
            	    		    goto loop30;
            	        }
            	    } while (true);

            	    loop30:
            	    	;	// Stops C# compiler whining that label 'loop30' has no statements

            	    // Ada95Walker.g:494:38: ( variant_part )?
            	    int alt31 = 2;
            	    int LA31_0 = input.LA(1);

            	    if ( (LA31_0 == CASE) )
            	    {
            	        alt31 = 1;
            	    }
            	    switch (alt31) 
            	    {
            	        case 1 :
            	            // Ada95Walker.g:494:38: variant_part
            	            {
            	            	PushFollow(FOLLOW_variant_part_in_component_list1284);
            	            	variant_part();
            	            	state.followingStackPointer--;


            	            }
            	            break;

            	    }


            	    Match(input, Token.UP, null); 
            	}

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "component_list"

    public class component_item_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "component_item"
    // Ada95Walker.g:496:1: component_item : component_declaration ;
    public Ada95Walker.component_item_return component_item() // throws RecognitionException [1]
    {   
        Ada95Walker.component_item_return retval = new Ada95Walker.component_item_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:497:2: ( component_declaration )
            // Ada95Walker.g:497:4: component_declaration
            {
            	PushFollow(FOLLOW_component_declaration_in_component_item1297);
            	component_declaration();
            	state.followingStackPointer--;


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "component_item"

    public class component_declaration_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "component_declaration"
    // Ada95Walker.g:499:1: component_declaration : ^( COMPONENT defining_identifier_list component_definition ( default_expression[ out defExprType ] )? ) ;
    public Ada95Walker.component_declaration_return component_declaration() // throws RecognitionException [1]
    {   
        Symbols_stack.Push(new Symbols_scope());

        Ada95Walker.component_declaration_return retval = new Ada95Walker.component_declaration_return();
        retval.Start = input.LT(1);


        		((Symbols_scope)Symbols_stack.Peek()).IDs =  new SortedSet< string >();
        		string defExprType;
        	
        try 
    	{
            // Ada95Walker.g:506:2: ( ^( COMPONENT defining_identifier_list component_definition ( default_expression[ out defExprType ] )? ) )
            // Ada95Walker.g:507:3: ^( COMPONENT defining_identifier_list component_definition ( default_expression[ out defExprType ] )? )
            {
            	Match(input,COMPONENT,FOLLOW_COMPONENT_in_component_declaration1324); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_defining_identifier_list_in_component_declaration1326);
            	defining_identifier_list();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_component_definition_in_component_declaration1328);
            	component_definition();
            	state.followingStackPointer--;

            	// Ada95Walker.g:507:62: ( default_expression[ out defExprType ] )?
            	int alt32 = 2;
            	int LA32_0 = input.LA(1);

            	if ( (LA32_0 == EXPRESSION) )
            	{
            	    alt32 = 1;
            	}
            	switch (alt32) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:507:62: default_expression[ out defExprType ]
            	        {
            	        	PushFollow(FOLLOW_default_expression_in_component_declaration1330);
            	        	default_expression(out defExprType);
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            				foreach ( string id in ((Symbols_scope)Symbols_stack.Peek()).IDs )
            					((SymbolTable_scope)SymbolTable_stack.Peek()).Objects.Add( id, null );
            			

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            Symbols_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "component_declaration"

    public class variant_part_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "variant_part"
    // Ada95Walker.g:514:1: variant_part : ^( CASE direct_name ( variant )* ( other_variant )? ) ;
    public Ada95Walker.variant_part_return variant_part() // throws RecognitionException [1]
    {   
        Ada95Walker.variant_part_return retval = new Ada95Walker.variant_part_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:515:2: ( ^( CASE direct_name ( variant )* ( other_variant )? ) )
            // Ada95Walker.g:515:4: ^( CASE direct_name ( variant )* ( other_variant )? )
            {
            	Match(input,CASE,FOLLOW_CASE_in_variant_part1351); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_direct_name_in_variant_part1353);
            	direct_name();
            	state.followingStackPointer--;

            	// Ada95Walker.g:515:24: ( variant )*
            	do 
            	{
            	    int alt33 = 2;
            	    int LA33_0 = input.LA(1);

            	    if ( (LA33_0 == WHEN) )
            	    {
            	        alt33 = 1;
            	    }


            	    switch (alt33) 
            		{
            			case 1 :
            			    // Ada95Walker.g:515:24: variant
            			    {
            			    	PushFollow(FOLLOW_variant_in_variant_part1355);
            			    	variant();
            			    	state.followingStackPointer--;


            			    }
            			    break;

            			default:
            			    goto loop33;
            	    }
            	} while (true);

            	loop33:
            		;	// Stops C# compiler whining that label 'loop33' has no statements

            	// Ada95Walker.g:515:33: ( other_variant )?
            	int alt34 = 2;
            	int LA34_0 = input.LA(1);

            	if ( (LA34_0 == OTHERS) )
            	{
            	    alt34 = 1;
            	}
            	switch (alt34) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:515:33: other_variant
            	        {
            	        	PushFollow(FOLLOW_other_variant_in_variant_part1358);
            	        	other_variant();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "variant_part"

    public class variant_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "variant"
    // Ada95Walker.g:517:1: variant : ^( WHEN discrete_choice_list component_list ) ;
    public Ada95Walker.variant_return variant() // throws RecognitionException [1]
    {   
        Ada95Walker.variant_return retval = new Ada95Walker.variant_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:518:2: ( ^( WHEN discrete_choice_list component_list ) )
            // Ada95Walker.g:518:4: ^( WHEN discrete_choice_list component_list )
            {
            	Match(input,WHEN,FOLLOW_WHEN_in_variant1373); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_discrete_choice_list_in_variant1375);
            	discrete_choice_list();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_component_list_in_variant1377);
            	component_list();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "variant"

    public class other_variant_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "other_variant"
    // Ada95Walker.g:520:1: other_variant : ^( OTHERS component_list ) ;
    public Ada95Walker.other_variant_return other_variant() // throws RecognitionException [1]
    {   
        Ada95Walker.other_variant_return retval = new Ada95Walker.other_variant_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:521:2: ( ^( OTHERS component_list ) )
            // Ada95Walker.g:521:4: ^( OTHERS component_list )
            {
            	Match(input,OTHERS,FOLLOW_OTHERS_in_other_variant1392); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_component_list_in_other_variant1394);
            	component_list();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "other_variant"

    public class discrete_choice_list_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "discrete_choice_list"
    // Ada95Walker.g:523:1: discrete_choice_list : (c+= discrete_choice )+ -> DiscreteChoiceList(choices= $c );
    public Ada95Walker.discrete_choice_list_return discrete_choice_list() // throws RecognitionException [1]
    {   
        Ada95Walker.discrete_choice_list_return retval = new Ada95Walker.discrete_choice_list_return();
        retval.Start = input.LT(1);

        IList list_c = null;
        Ada95Walker.discrete_choice_return c = default(Ada95Walker.discrete_choice_return);
         c = null;
        try 
    	{
            // Ada95Walker.g:524:2: ( (c+= discrete_choice )+ -> DiscreteChoiceList(choices= $c ))
            // Ada95Walker.g:524:4: (c+= discrete_choice )+
            {
            	// Ada95Walker.g:524:4: (c+= discrete_choice )+
            	int cnt35 = 0;
            	do 
            	{
            	    int alt35 = 2;
            	    int LA35_0 = input.LA(1);

            	    if ( (LA35_0 == EXPRESSION || LA35_0 == IDENTIFIER || LA35_0 == RANGE) )
            	    {
            	        alt35 = 1;
            	    }


            	    switch (alt35) 
            		{
            			case 1 :
            			    // Ada95Walker.g:524:6: c+= discrete_choice
            			    {
            			    	PushFollow(FOLLOW_discrete_choice_in_discrete_choice_list1412);
            			    	c = discrete_choice();
            			    	state.followingStackPointer--;

            			    	if (list_c == null) list_c = new ArrayList();
            			    	list_c.Add(c.Template);


            			    }
            			    break;

            			default:
            			    if ( cnt35 >= 1 ) goto loop35;
            		            EarlyExitException eee35 =
            		                new EarlyExitException(35, input);
            		            throw eee35;
            	    }
            	    cnt35++;
            	} while (true);

            	loop35:
            		;	// Stops C# compiler whining that label 'loop35' has no statements



            	// TEMPLATE REWRITE
            	// 524:30: -> DiscreteChoiceList(choices= $c )
            	{
            	    retval.ST = templateLib.GetInstanceOf("DiscreteChoiceList",
            	  new STAttrMap().Add("choices",  list_c ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "discrete_choice_list"

    public class discrete_choice_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "discrete_choice"
    // Ada95Walker.g:526:1: discrete_choice : (e= expression[ out exprType ] -> DiscreteChoice(expr= $e.st ) | discrete_range );
    public Ada95Walker.discrete_choice_return discrete_choice() // throws RecognitionException [1]
    {   
        Ada95Walker.discrete_choice_return retval = new Ada95Walker.discrete_choice_return();
        retval.Start = input.LT(1);

        Ada95Walker.expression_return e = default(Ada95Walker.expression_return);



        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:531:2: (e= expression[ out exprType ] -> DiscreteChoice(expr= $e.st ) | discrete_range )
            int alt36 = 2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0 == EXPRESSION) )
            {
                alt36 = 1;
            }
            else if ( (LA36_0 == IDENTIFIER || LA36_0 == RANGE) )
            {
                alt36 = 2;
            }
            else 
            {
                NoViableAltException nvae_d36s0 =
                    new NoViableAltException("", 36, 0, input);

                throw nvae_d36s0;
            }
            switch (alt36) 
            {
                case 1 :
                    // Ada95Walker.g:532:3: e= expression[ out exprType ]
                    {
                    	PushFollow(FOLLOW_expression_in_discrete_choice1451);
                    	e = expression(out exprType);
                    	state.followingStackPointer--;

                    	 if ( exprType != "int" ) throw new UnexpectedType( exprType, "int", input ); 


                    	// TEMPLATE REWRITE
                    	// 534:4: -> DiscreteChoice(expr= $e.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("DiscreteChoice",
                    	  new STAttrMap().Add("expr",  ((e != null) ? e.ST : null) ));
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:535:3: discrete_range
                    {
                    	PushFollow(FOLLOW_discrete_range_in_discrete_choice1478);
                    	discrete_range();
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "discrete_choice"

    public class record_extension_part_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "record_extension_part"
    // Ada95Walker.g:538:1: record_extension_part : ^( WITH record_definition ) ;
    public Ada95Walker.record_extension_part_return record_extension_part() // throws RecognitionException [1]
    {   
        Ada95Walker.record_extension_part_return retval = new Ada95Walker.record_extension_part_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:539:2: ( ^( WITH record_definition ) )
            // Ada95Walker.g:539:4: ^( WITH record_definition )
            {
            	Match(input,WITH,FOLLOW_WITH_in_record_extension_part1491); 

            	if ( input.LA(1) == Token.DOWN )
            	{
            	    Match(input, Token.DOWN, null); 
            	    PushFollow(FOLLOW_record_definition_in_record_extension_part1493);
            	    record_definition();
            	    state.followingStackPointer--;


            	    Match(input, Token.UP, null); 
            	}

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "record_extension_part"

    public class access_definition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "access_definition"
    // Ada95Walker.g:554:1: access_definition : ACCESS subtype_mark ;
    public Ada95Walker.access_definition_return access_definition() // throws RecognitionException [1]
    {   
        Ada95Walker.access_definition_return retval = new Ada95Walker.access_definition_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:555:2: ( ACCESS subtype_mark )
            // Ada95Walker.g:555:4: ACCESS subtype_mark
            {
            	Match(input,ACCESS,FOLLOW_ACCESS_in_access_definition1506); 
            	PushFollow(FOLLOW_subtype_mark_in_access_definition1508);
            	subtype_mark();
            	state.followingStackPointer--;


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "access_definition"

    public class declarative_part_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "declarative_part"
    // Ada95Walker.g:557:1: declarative_part : ^( DECLARE (items+= declarative_item )* ) -> DeclarativePart(declarations= $items );
    public Ada95Walker.declarative_part_return declarative_part() // throws RecognitionException [1]
    {   
        Ada95Walker.declarative_part_return retval = new Ada95Walker.declarative_part_return();
        retval.Start = input.LT(1);

        IList list_items = null;
        Ada95Walker.declarative_item_return items = default(Ada95Walker.declarative_item_return);
         items = null;
        try 
    	{
            // Ada95Walker.g:558:2: ( ^( DECLARE (items+= declarative_item )* ) -> DeclarativePart(declarations= $items ))
            // Ada95Walker.g:558:4: ^( DECLARE (items+= declarative_item )* )
            {
            	Match(input,DECLARE,FOLLOW_DECLARE_in_declarative_part1520); 

            	if ( input.LA(1) == Token.DOWN )
            	{
            	    Match(input, Token.DOWN, null); 
            	    // Ada95Walker.g:558:15: (items+= declarative_item )*
            	    do 
            	    {
            	        int alt37 = 2;
            	        int LA37_0 = input.LA(1);

            	        if ( ((LA37_0 >= ITEM && LA37_0 <= BODY)) )
            	        {
            	            alt37 = 1;
            	        }


            	        switch (alt37) 
            	    	{
            	    		case 1 :
            	    		    // Ada95Walker.g:558:17: items+= declarative_item
            	    		    {
            	    		    	PushFollow(FOLLOW_declarative_item_in_declarative_part1528);
            	    		    	items = declarative_item();
            	    		    	state.followingStackPointer--;

            	    		    	if (list_items == null) list_items = new ArrayList();
            	    		    	list_items.Add(items.Template);


            	    		    }
            	    		    break;

            	    		default:
            	    		    goto loop37;
            	        }
            	    } while (true);

            	    loop37:
            	    	;	// Stops C# compiler whining that label 'loop37' has no statements


            	    Match(input, Token.UP, null); 
            	}


            	// TEMPLATE REWRITE
            	// 558:48: -> DeclarativePart(declarations= $items )
            	{
            	    retval.ST = templateLib.GetInstanceOf("DeclarativePart",
            	  new STAttrMap().Add("declarations",  list_items ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "declarative_part"

    public class declarative_item_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "declarative_item"
    // Ada95Walker.g:560:1: declarative_item : ( ^( ITEM i= basic_declarative_item ) -> { $i.st } | ^( BODY b= body ) -> { $b.st });
    public Ada95Walker.declarative_item_return declarative_item() // throws RecognitionException [1]
    {   
        Ada95Walker.declarative_item_return retval = new Ada95Walker.declarative_item_return();
        retval.Start = input.LT(1);

        Ada95Walker.basic_declarative_item_return i = default(Ada95Walker.basic_declarative_item_return);

        Ada95Walker.body_return b = default(Ada95Walker.body_return);


        try 
    	{
            // Ada95Walker.g:561:2: ( ^( ITEM i= basic_declarative_item ) -> { $i.st } | ^( BODY b= body ) -> { $b.st })
            int alt38 = 2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0 == ITEM) )
            {
                alt38 = 1;
            }
            else if ( (LA38_0 == BODY) )
            {
                alt38 = 2;
            }
            else 
            {
                NoViableAltException nvae_d38s0 =
                    new NoViableAltException("", 38, 0, input);

                throw nvae_d38s0;
            }
            switch (alt38) 
            {
                case 1 :
                    // Ada95Walker.g:562:3: ^( ITEM i= basic_declarative_item )
                    {
                    	Match(input,ITEM,FOLLOW_ITEM_in_declarative_item1560); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_basic_declarative_item_in_declarative_item1566);
                    	i = basic_declarative_item();
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 


                    	// TEMPLATE REWRITE
                    	// 562:40: -> { $i.st }
                    	{
                    	    retval.ST =  ((i != null) ? i.ST : null) ;
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:563:3: ^( BODY b= body )
                    {
                    	Match(input,BODY,FOLLOW_BODY_in_declarative_item1580); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_body_in_declarative_item1586);
                    	b = body();
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 


                    	// TEMPLATE REWRITE
                    	// 563:27: -> { $b.st }
                    	{
                    	    retval.ST =  ((b != null) ? b.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "declarative_item"

    public class basic_declarative_item_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "basic_declarative_item"
    // Ada95Walker.g:566:1: basic_declarative_item : d= basic_declaration -> { $d.st };
    public Ada95Walker.basic_declarative_item_return basic_declarative_item() // throws RecognitionException [1]
    {   
        Ada95Walker.basic_declarative_item_return retval = new Ada95Walker.basic_declarative_item_return();
        retval.Start = input.LT(1);

        Ada95Walker.basic_declaration_return d = default(Ada95Walker.basic_declaration_return);


        try 
    	{
            // Ada95Walker.g:567:2: (d= basic_declaration -> { $d.st })
            // Ada95Walker.g:567:4: d= basic_declaration
            {
            	PushFollow(FOLLOW_basic_declaration_in_basic_declarative_item1612);
            	d = basic_declaration();
            	state.followingStackPointer--;



            	// TEMPLATE REWRITE
            	// 567:26: -> { $d.st }
            	{
            	    retval.ST =  ((d != null) ? d.ST : null) ;
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "basic_declarative_item"

    public class body_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "body"
    // Ada95Walker.g:569:1: body : p= proper_body -> { $p.st };
    public Ada95Walker.body_return body() // throws RecognitionException [1]
    {   
        Ada95Walker.body_return retval = new Ada95Walker.body_return();
        retval.Start = input.LT(1);

        Ada95Walker.proper_body_return p = default(Ada95Walker.proper_body_return);


        try 
    	{
            // Ada95Walker.g:570:2: (p= proper_body -> { $p.st })
            // Ada95Walker.g:570:4: p= proper_body
            {
            	PushFollow(FOLLOW_proper_body_in_body1630);
            	p = proper_body();
            	state.followingStackPointer--;



            	// TEMPLATE REWRITE
            	// 570:20: -> { $p.st }
            	{
            	    retval.ST =  ((p != null) ? p.ST : null) ;
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "body"

    public class proper_body_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "proper_body"
    // Ada95Walker.g:572:1: proper_body : s= subprogram_body -> { $s.st };
    public Ada95Walker.proper_body_return proper_body() // throws RecognitionException [1]
    {   
        Ada95Walker.proper_body_return retval = new Ada95Walker.proper_body_return();
        retval.Start = input.LT(1);

        Ada95Walker.subprogram_body_return s = default(Ada95Walker.subprogram_body_return);


        try 
    	{
            // Ada95Walker.g:573:2: (s= subprogram_body -> { $s.st })
            // Ada95Walker.g:573:4: s= subprogram_body
            {
            	PushFollow(FOLLOW_subprogram_body_in_proper_body1648);
            	s = subprogram_body();
            	state.followingStackPointer--;



            	// TEMPLATE REWRITE
            	// 573:24: -> { $s.st }
            	{
            	    retval.ST =  ((s != null) ? s.ST : null) ;
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "proper_body"

    public class name_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "name"
    // Ada95Walker.g:575:1: name[ out string type ] : (f= function_call[ out type ] -> { $f.st } | type_conversion | slice | i= indexed_component[ out type ] -> { $i.st } | explicit_dereference | selected_component | ^( OBJECT n= direct_name ) -> Name(name= $n.text ) | attribute_reference | CHARACTER_LITERAL );
    public Ada95Walker.name_return name(out string type) // throws RecognitionException [1]
    {   
        Ada95Walker.name_return retval = new Ada95Walker.name_return();
        retval.Start = input.LT(1);

        Ada95Walker.function_call_return f = default(Ada95Walker.function_call_return);

        Ada95Walker.indexed_component_return i = default(Ada95Walker.indexed_component_return);

        Ada95Walker.direct_name_return n = default(Ada95Walker.direct_name_return);



        		type = string.Empty;
        	
        try 
    	{
            // Ada95Walker.g:580:2: (f= function_call[ out type ] -> { $f.st } | type_conversion | slice | i= indexed_component[ out type ] -> { $i.st } | explicit_dereference | selected_component | ^( OBJECT n= direct_name ) -> Name(name= $n.text ) | attribute_reference | CHARACTER_LITERAL )
            int alt39 = 9;
            switch ( input.LA(1) ) 
            {
            case FUNCTION:
            	{
                alt39 = 1;
                }
                break;
            case TYPE:
            	{
                alt39 = 2;
                }
                break;
            case SLICE:
            	{
                alt39 = 3;
                }
                break;
            case INDEXED_COMPONENT:
            	{
                alt39 = 4;
                }
                break;
            case EXPLICIT_DEREFERENCE:
            	{
                alt39 = 5;
                }
                break;
            case SELECTED_COMPONENT:
            	{
                alt39 = 6;
                }
                break;
            case OBJECT:
            	{
                alt39 = 7;
                }
                break;
            case ATTRIBUTE_REFERENCE:
            	{
                alt39 = 8;
                }
                break;
            case CHARACTER_LITERAL:
            	{
                alt39 = 9;
                }
                break;
            	default:
            	    NoViableAltException nvae_d39s0 =
            	        new NoViableAltException("", 39, 0, input);

            	    throw nvae_d39s0;
            }

            switch (alt39) 
            {
                case 1 :
                    // Ada95Walker.g:581:3: f= function_call[ out type ]
                    {
                    	PushFollow(FOLLOW_function_call_in_name1676);
                    	f = function_call(out type);
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 581:34: -> { $f.st }
                    	{
                    	    retval.ST =  ((f != null) ? f.ST : null) ;
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:582:3: type_conversion
                    {
                    	PushFollow(FOLLOW_type_conversion_in_name1688);
                    	type_conversion();
                    	state.followingStackPointer--;


                    }
                    break;
                case 3 :
                    // Ada95Walker.g:583:3: slice
                    {
                    	PushFollow(FOLLOW_slice_in_name1694);
                    	slice();
                    	state.followingStackPointer--;


                    }
                    break;
                case 4 :
                    // Ada95Walker.g:584:3: i= indexed_component[ out type ]
                    {
                    	PushFollow(FOLLOW_indexed_component_in_name1704);
                    	i = indexed_component(out type);
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 584:37: -> { $i.st }
                    	{
                    	    retval.ST =  ((i != null) ? i.ST : null) ;
                    	}


                    }
                    break;
                case 5 :
                    // Ada95Walker.g:585:3: explicit_dereference
                    {
                    	PushFollow(FOLLOW_explicit_dereference_in_name1715);
                    	explicit_dereference();
                    	state.followingStackPointer--;


                    }
                    break;
                case 6 :
                    // Ada95Walker.g:586:3: selected_component
                    {
                    	PushFollow(FOLLOW_selected_component_in_name1721);
                    	selected_component();
                    	state.followingStackPointer--;


                    }
                    break;
                case 7 :
                    // Ada95Walker.g:587:3: ^( OBJECT n= direct_name )
                    {
                    	Match(input,OBJECT,FOLLOW_OBJECT_in_name1729); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_direct_name_in_name1735);
                    	n = direct_name();
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 
                    	 type = GetObject( ((n != null) ? input.TokenStream.ToString(
                    	  input.TreeAdaptor.GetTokenStartIndex(n.Start),
                    	  input.TreeAdaptor.GetTokenStopIndex(n.Start)) : null) ).ValueType; 


                    	// TEMPLATE REWRITE
                    	// 587:74: -> Name(name= $n.text )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Name",
                    	  new STAttrMap().Add("name",  ((n != null) ? input.TokenStream.ToString(
                    	  input.TreeAdaptor.GetTokenStartIndex(n.Start),
                    	  input.TreeAdaptor.GetTokenStopIndex(n.Start)) : null) ));
                    	}


                    }
                    break;
                case 8 :
                    // Ada95Walker.g:588:3: attribute_reference
                    {
                    	PushFollow(FOLLOW_attribute_reference_in_name1758);
                    	attribute_reference();
                    	state.followingStackPointer--;


                    }
                    break;
                case 9 :
                    // Ada95Walker.g:589:6: CHARACTER_LITERAL
                    {
                    	Match(input,CHARACTER_LITERAL,FOLLOW_CHARACTER_LITERAL_in_name1767); 

                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "name"

    public class direct_name_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "direct_name"
    // Ada95Walker.g:592:1: direct_name : ( IDENTIFIER | operator_symbol );
    public Ada95Walker.direct_name_return direct_name() // throws RecognitionException [1]
    {   
        Ada95Walker.direct_name_return retval = new Ada95Walker.direct_name_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:593:2: ( IDENTIFIER | operator_symbol )
            int alt40 = 2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0 == IDENTIFIER) )
            {
                alt40 = 1;
            }
            else if ( (LA40_0 == STRING_LITERAL) )
            {
                alt40 = 2;
            }
            else 
            {
                NoViableAltException nvae_d40s0 =
                    new NoViableAltException("", 40, 0, input);

                throw nvae_d40s0;
            }
            switch (alt40) 
            {
                case 1 :
                    // Ada95Walker.g:594:3: IDENTIFIER
                    {
                    	Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_direct_name1780); 

                    }
                    break;
                case 2 :
                    // Ada95Walker.g:595:3: operator_symbol
                    {
                    	PushFollow(FOLLOW_operator_symbol_in_direct_name1786);
                    	operator_symbol();
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "direct_name"

    public class explicit_dereference_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "explicit_dereference"
    // Ada95Walker.g:599:1: explicit_dereference : ^( EXPLICIT_DEREFERENCE direct_name ) ;
    public Ada95Walker.explicit_dereference_return explicit_dereference() // throws RecognitionException [1]
    {   
        Ada95Walker.explicit_dereference_return retval = new Ada95Walker.explicit_dereference_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:600:2: ( ^( EXPLICIT_DEREFERENCE direct_name ) )
            // Ada95Walker.g:600:4: ^( EXPLICIT_DEREFERENCE direct_name )
            {
            	Match(input,EXPLICIT_DEREFERENCE,FOLLOW_EXPLICIT_DEREFERENCE_in_explicit_dereference1800); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_direct_name_in_explicit_dereference1802);
            	direct_name();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "explicit_dereference"

    public class indexed_component_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "indexed_component"
    // Ada95Walker.g:602:1: indexed_component[ out string type ] : ^( INDEXED_COMPONENT n= direct_name (e+= expression[ out exprType ] )+ ) -> IndexedComponent(array= array expressions= $e );
    public Ada95Walker.indexed_component_return indexed_component(out string type) // throws RecognitionException [1]
    {   
        Ada95Walker.indexed_component_return retval = new Ada95Walker.indexed_component_return();
        retval.Start = input.LT(1);

        IList list_e = null;
        Ada95Walker.direct_name_return n = default(Ada95Walker.direct_name_return);

        Ada95Walker.expression_return e = default(Ada95Walker.expression_return);
         e = null;

        		string exprType;
        		type = string.Empty;
        		ArrayObject array;
        	
        try 
    	{
            // Ada95Walker.g:609:2: ( ^( INDEXED_COMPONENT n= direct_name (e+= expression[ out exprType ] )+ ) -> IndexedComponent(array= array expressions= $e ))
            // Ada95Walker.g:610:3: ^( INDEXED_COMPONENT n= direct_name (e+= expression[ out exprType ] )+ )
            {
            	Match(input,INDEXED_COMPONENT,FOLLOW_INDEXED_COMPONENT_in_indexed_component1827); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_direct_name_in_indexed_component1833);
            	n = direct_name();
            	state.followingStackPointer--;


            				array = GetObject( ((n != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(n.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(n.Start)) : null) ) as ArrayObject;
            				if ( array == null )
            					throw new UnknownIndexedComponent( ((n != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(n.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(n.Start)) : null), input );
            				type = array.ComponentType;
            			
            	// Ada95Walker.g:617:3: (e+= expression[ out exprType ] )+
            	int cnt41 = 0;
            	do 
            	{
            	    int alt41 = 2;
            	    int LA41_0 = input.LA(1);

            	    if ( (LA41_0 == EXPRESSION) )
            	    {
            	        alt41 = 1;
            	    }


            	    switch (alt41) 
            		{
            			case 1 :
            			    // Ada95Walker.g:617:5: e+= expression[ out exprType ]
            			    {
            			    	PushFollow(FOLLOW_expression_in_indexed_component1847);
            			    	e = expression(out exprType);
            			    	state.followingStackPointer--;

            			    	if (list_e == null) list_e = new ArrayList();
            			    	list_e.Add(e.Template);


            			    }
            			    break;

            			default:
            			    if ( cnt41 >= 1 ) goto loop41;
            		            EarlyExitException eee41 =
            		                new EarlyExitException(41, input);
            		            throw eee41;
            	    }
            	    cnt41++;
            	} while (true);

            	loop41:
            		;	// Stops C# compiler whining that label 'loop41' has no statements


            	Match(input, Token.UP, null); 


            	// TEMPLATE REWRITE
            	// 617:42: -> IndexedComponent(array= array expressions= $e )
            	{
            	    retval.ST = templateLib.GetInstanceOf("IndexedComponent",
            	  new STAttrMap().Add("array",  array ).Add("expressions",  list_e ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "indexed_component"

    public class slice_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "slice"
    // Ada95Walker.g:619:1: slice : ^( SLICE direct_name discrete_range ) ;
    public Ada95Walker.slice_return slice() // throws RecognitionException [1]
    {   
        Ada95Walker.slice_return retval = new Ada95Walker.slice_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:620:2: ( ^( SLICE direct_name discrete_range ) )
            // Ada95Walker.g:620:4: ^( SLICE direct_name discrete_range )
            {
            	Match(input,SLICE,FOLLOW_SLICE_in_slice1885); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_direct_name_in_slice1887);
            	direct_name();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_discrete_range_in_slice1889);
            	discrete_range();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "slice"

    public class selected_component_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "selected_component"
    // Ada95Walker.g:622:1: selected_component : ^( SELECTED_COMPONENT direct_name selector_name ) ;
    public Ada95Walker.selected_component_return selected_component() // throws RecognitionException [1]
    {   
        Ada95Walker.selected_component_return retval = new Ada95Walker.selected_component_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:623:2: ( ^( SELECTED_COMPONENT direct_name selector_name ) )
            // Ada95Walker.g:623:4: ^( SELECTED_COMPONENT direct_name selector_name )
            {
            	Match(input,SELECTED_COMPONENT,FOLLOW_SELECTED_COMPONENT_in_selected_component1903); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_direct_name_in_selected_component1905);
            	direct_name();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_selector_name_in_selected_component1907);
            	selector_name();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "selected_component"

    public class selector_name_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "selector_name"
    // Ada95Walker.g:625:1: selector_name : ( IDENTIFIER | CHARACTER_LITERAL | operator_symbol );
    public Ada95Walker.selector_name_return selector_name() // throws RecognitionException [1]
    {   
        Ada95Walker.selector_name_return retval = new Ada95Walker.selector_name_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:626:2: ( IDENTIFIER | CHARACTER_LITERAL | operator_symbol )
            int alt42 = 3;
            switch ( input.LA(1) ) 
            {
            case IDENTIFIER:
            	{
                alt42 = 1;
                }
                break;
            case CHARACTER_LITERAL:
            	{
                alt42 = 2;
                }
                break;
            case STRING_LITERAL:
            	{
                alt42 = 3;
                }
                break;
            	default:
            	    NoViableAltException nvae_d42s0 =
            	        new NoViableAltException("", 42, 0, input);

            	    throw nvae_d42s0;
            }

            switch (alt42) 
            {
                case 1 :
                    // Ada95Walker.g:627:3: IDENTIFIER
                    {
                    	Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_selector_name1921); 

                    }
                    break;
                case 2 :
                    // Ada95Walker.g:628:3: CHARACTER_LITERAL
                    {
                    	Match(input,CHARACTER_LITERAL,FOLLOW_CHARACTER_LITERAL_in_selector_name1927); 

                    }
                    break;
                case 3 :
                    // Ada95Walker.g:629:3: operator_symbol
                    {
                    	PushFollow(FOLLOW_operator_symbol_in_selector_name1933);
                    	operator_symbol();
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "selector_name"

    public class attribute_reference_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "attribute_reference"
    // Ada95Walker.g:632:1: attribute_reference : ^( ATTRIBUTE_REFERENCE direct_name attribute_designator ) ;
    public Ada95Walker.attribute_reference_return attribute_reference() // throws RecognitionException [1]
    {   
        Ada95Walker.attribute_reference_return retval = new Ada95Walker.attribute_reference_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:633:2: ( ^( ATTRIBUTE_REFERENCE direct_name attribute_designator ) )
            // Ada95Walker.g:633:4: ^( ATTRIBUTE_REFERENCE direct_name attribute_designator )
            {
            	Match(input,ATTRIBUTE_REFERENCE,FOLLOW_ATTRIBUTE_REFERENCE_in_attribute_reference1946); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_direct_name_in_attribute_reference1948);
            	direct_name();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_attribute_designator_in_attribute_reference1950);
            	attribute_designator();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "attribute_reference"

    public class attribute_designator_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "attribute_designator"
    // Ada95Walker.g:635:1: attribute_designator : IDENTIFIER ( expression[ out exprType ] )? ;
    public Ada95Walker.attribute_designator_return attribute_designator() // throws RecognitionException [1]
    {   
        Ada95Walker.attribute_designator_return retval = new Ada95Walker.attribute_designator_return();
        retval.Start = input.LT(1);


        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:640:2: ( IDENTIFIER ( expression[ out exprType ] )? )
            // Ada95Walker.g:640:4: IDENTIFIER ( expression[ out exprType ] )?
            {
            	Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_attribute_designator1969); 
            	// Ada95Walker.g:640:15: ( expression[ out exprType ] )?
            	int alt43 = 2;
            	int LA43_0 = input.LA(1);

            	if ( (LA43_0 == EXPRESSION) )
            	{
            	    alt43 = 1;
            	}
            	switch (alt43) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:640:15: expression[ out exprType ]
            	        {
            	        	PushFollow(FOLLOW_expression_in_attribute_designator1971);
            	        	expression(out exprType);
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "attribute_designator"

    public class expression_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "expression"
    // Ada95Walker.g:680:1: expression[ out string type ] : ^( EXPRESSION ( THEN (rList+= relation[ out exprType ] )+ -> AndThen(relations= $rList ) | AND (rList+= relation[ out exprType ] )+ -> And(relations= $rList ) | ELSE (rList+= relation[ out exprType ] )+ -> OrElse(relations= $rList ) | OR (rList+= relation[ out exprType ] )+ -> Or(relations= $rList ) | XOR (rList+= relation[ out exprType ] )+ -> Xor(relations= $rList ) | r= relation[ out type ] -> { $r.st }) ) ;
    public Ada95Walker.expression_return expression(out string type) // throws RecognitionException [1]
    {   
        Ada95Walker.expression_return retval = new Ada95Walker.expression_return();
        retval.Start = input.LT(1);

        IList list_rList = null;
        Ada95Walker.relation_return r = default(Ada95Walker.relation_return);

        Ada95Walker.relation_return rList = default(Ada95Walker.relation_return);
         rList = null;

        		type = string.Empty;
        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:686:2: ( ^( EXPRESSION ( THEN (rList+= relation[ out exprType ] )+ -> AndThen(relations= $rList ) | AND (rList+= relation[ out exprType ] )+ -> And(relations= $rList ) | ELSE (rList+= relation[ out exprType ] )+ -> OrElse(relations= $rList ) | OR (rList+= relation[ out exprType ] )+ -> Or(relations= $rList ) | XOR (rList+= relation[ out exprType ] )+ -> Xor(relations= $rList ) | r= relation[ out type ] -> { $r.st }) ) )
            // Ada95Walker.g:687:3: ^( EXPRESSION ( THEN (rList+= relation[ out exprType ] )+ -> AndThen(relations= $rList ) | AND (rList+= relation[ out exprType ] )+ -> And(relations= $rList ) | ELSE (rList+= relation[ out exprType ] )+ -> OrElse(relations= $rList ) | OR (rList+= relation[ out exprType ] )+ -> Or(relations= $rList ) | XOR (rList+= relation[ out exprType ] )+ -> Xor(relations= $rList ) | r= relation[ out type ] -> { $r.st }) )
            {
            	Match(input,EXPRESSION,FOLLOW_EXPRESSION_in_expression1999); 

            	Match(input, Token.DOWN, null); 
            	// Ada95Walker.g:688:4: ( THEN (rList+= relation[ out exprType ] )+ -> AndThen(relations= $rList ) | AND (rList+= relation[ out exprType ] )+ -> And(relations= $rList ) | ELSE (rList+= relation[ out exprType ] )+ -> OrElse(relations= $rList ) | OR (rList+= relation[ out exprType ] )+ -> Or(relations= $rList ) | XOR (rList+= relation[ out exprType ] )+ -> Xor(relations= $rList ) | r= relation[ out type ] -> { $r.st })
            	int alt49 = 6;
            	switch ( input.LA(1) ) 
            	{
            	case THEN:
            		{
            	    alt49 = 1;
            	    }
            	    break;
            	case AND:
            		{
            	    alt49 = 2;
            	    }
            	    break;
            	case ELSE:
            		{
            	    alt49 = 3;
            	    }
            	    break;
            	case OR:
            		{
            	    alt49 = 4;
            	    }
            	    break;
            	case XOR:
            		{
            	    alt49 = 5;
            	    }
            	    break;
            	case EXPLICIT_DEREFERENCE:
            	case INDEXED_COMPONENT:
            	case SLICE:
            	case SELECTED_COMPONENT:
            	case OBJECT:
            	case ATTRIBUTE_REFERENCE:
            	case QUALIFIED_BY_EXPRESSION:
            	case EXPRESSION:
            	case IN_RANGE:
            	case IN_SUBTYPE:
            	case TYPE:
            	case CHARACTER_LITERAL:
            	case MOD:
            	case NULL:
            	case NOT:
            	case POW:
            	case ABS:
            	case NUMERIC_LITERAL:
            	case STRING_LITERAL:
            	case EQ:
            	case NEQ:
            	case LESS:
            	case LEQ:
            	case GREATER:
            	case GEQ:
            	case PLUS:
            	case MINUS:
            	case CONCAT:
            	case MULT:
            	case DIV:
            	case REM:
            	case FUNCTION:
            		{
            	    alt49 = 6;
            	    }
            	    break;
            		default:
            		    NoViableAltException nvae_d49s0 =
            		        new NoViableAltException("", 49, 0, input);

            		    throw nvae_d49s0;
            	}

            	switch (alt49) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:689:5: THEN (rList+= relation[ out exprType ] )+
            	        {
            	        	Match(input,THEN,FOLLOW_THEN_in_expression2010); 
            	        	// Ada95Walker.g:690:5: (rList+= relation[ out exprType ] )+
            	        	int cnt44 = 0;
            	        	do 
            	        	{
            	        	    int alt44 = 2;
            	        	    int LA44_0 = input.LA(1);

            	        	    if ( ((LA44_0 >= EXPLICIT_DEREFERENCE && LA44_0 <= ATTRIBUTE_REFERENCE) || LA44_0 == QUALIFIED_BY_EXPRESSION || LA44_0 == EXPRESSION || (LA44_0 >= IN_RANGE && LA44_0 <= IN_SUBTYPE) || LA44_0 == TYPE || (LA44_0 >= CHARACTER_LITERAL && LA44_0 <= MOD) || LA44_0 == NULL || LA44_0 == NOT || (LA44_0 >= POW && LA44_0 <= REM) || LA44_0 == FUNCTION) )
            	        	    {
            	        	        alt44 = 1;
            	        	    }


            	        	    switch (alt44) 
            	        		{
            	        			case 1 :
            	        			    // Ada95Walker.g:691:6: rList+= relation[ out exprType ]
            	        			    {
            	        			    	PushFollow(FOLLOW_relation_in_expression2027);
            	        			    	rList = relation(out exprType);
            	        			    	state.followingStackPointer--;

            	        			    	if (list_rList == null) list_rList = new ArrayList();
            	        			    	list_rList.Add(rList.Template);

            	        			    	 if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); 

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt44 >= 1 ) goto loop44;
            	        		            EarlyExitException eee44 =
            	        		                new EarlyExitException(44, input);
            	        		            throw eee44;
            	        	    }
            	        	    cnt44++;
            	        	} while (true);

            	        	loop44:
            	        		;	// Stops C# compiler whining that label 'loop44' has no statements

            	        	 type = "bool"; 


            	        	// TEMPLATE REWRITE
            	        	// 693:27: -> AndThen(relations= $rList )
            	        	{
            	        	    retval.ST = templateLib.GetInstanceOf("AndThen",
            	        	  new STAttrMap().Add("relations",  list_rList ));
            	        	}


            	        }
            	        break;
            	    case 2 :
            	        // Ada95Walker.g:694:5: AND (rList+= relation[ out exprType ] )+
            	        {
            	        	Match(input,AND,FOLLOW_AND_in_expression2065); 
            	        	// Ada95Walker.g:695:5: (rList+= relation[ out exprType ] )+
            	        	int cnt45 = 0;
            	        	do 
            	        	{
            	        	    int alt45 = 2;
            	        	    int LA45_0 = input.LA(1);

            	        	    if ( ((LA45_0 >= EXPLICIT_DEREFERENCE && LA45_0 <= ATTRIBUTE_REFERENCE) || LA45_0 == QUALIFIED_BY_EXPRESSION || LA45_0 == EXPRESSION || (LA45_0 >= IN_RANGE && LA45_0 <= IN_SUBTYPE) || LA45_0 == TYPE || (LA45_0 >= CHARACTER_LITERAL && LA45_0 <= MOD) || LA45_0 == NULL || LA45_0 == NOT || (LA45_0 >= POW && LA45_0 <= REM) || LA45_0 == FUNCTION) )
            	        	    {
            	        	        alt45 = 1;
            	        	    }


            	        	    switch (alt45) 
            	        		{
            	        			case 1 :
            	        			    // Ada95Walker.g:696:6: rList+= relation[ out exprType ]
            	        			    {
            	        			    	PushFollow(FOLLOW_relation_in_expression2082);
            	        			    	rList = relation(out exprType);
            	        			    	state.followingStackPointer--;

            	        			    	if (list_rList == null) list_rList = new ArrayList();
            	        			    	list_rList.Add(rList.Template);

            	        			    	 if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); 

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt45 >= 1 ) goto loop45;
            	        		            EarlyExitException eee45 =
            	        		                new EarlyExitException(45, input);
            	        		            throw eee45;
            	        	    }
            	        	    cnt45++;
            	        	} while (true);

            	        	loop45:
            	        		;	// Stops C# compiler whining that label 'loop45' has no statements

            	        	 type = "bool"; 


            	        	// TEMPLATE REWRITE
            	        	// 698:27: -> And(relations= $rList )
            	        	{
            	        	    retval.ST = templateLib.GetInstanceOf("And",
            	        	  new STAttrMap().Add("relations",  list_rList ));
            	        	}


            	        }
            	        break;
            	    case 3 :
            	        // Ada95Walker.g:699:5: ELSE (rList+= relation[ out exprType ] )+
            	        {
            	        	Match(input,ELSE,FOLLOW_ELSE_in_expression2120); 
            	        	// Ada95Walker.g:700:5: (rList+= relation[ out exprType ] )+
            	        	int cnt46 = 0;
            	        	do 
            	        	{
            	        	    int alt46 = 2;
            	        	    int LA46_0 = input.LA(1);

            	        	    if ( ((LA46_0 >= EXPLICIT_DEREFERENCE && LA46_0 <= ATTRIBUTE_REFERENCE) || LA46_0 == QUALIFIED_BY_EXPRESSION || LA46_0 == EXPRESSION || (LA46_0 >= IN_RANGE && LA46_0 <= IN_SUBTYPE) || LA46_0 == TYPE || (LA46_0 >= CHARACTER_LITERAL && LA46_0 <= MOD) || LA46_0 == NULL || LA46_0 == NOT || (LA46_0 >= POW && LA46_0 <= REM) || LA46_0 == FUNCTION) )
            	        	    {
            	        	        alt46 = 1;
            	        	    }


            	        	    switch (alt46) 
            	        		{
            	        			case 1 :
            	        			    // Ada95Walker.g:701:6: rList+= relation[ out exprType ]
            	        			    {
            	        			    	PushFollow(FOLLOW_relation_in_expression2137);
            	        			    	rList = relation(out exprType);
            	        			    	state.followingStackPointer--;

            	        			    	if (list_rList == null) list_rList = new ArrayList();
            	        			    	list_rList.Add(rList.Template);

            	        			    	 if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); 

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt46 >= 1 ) goto loop46;
            	        		            EarlyExitException eee46 =
            	        		                new EarlyExitException(46, input);
            	        		            throw eee46;
            	        	    }
            	        	    cnt46++;
            	        	} while (true);

            	        	loop46:
            	        		;	// Stops C# compiler whining that label 'loop46' has no statements

            	        	 type = "bool"; 


            	        	// TEMPLATE REWRITE
            	        	// 703:27: -> OrElse(relations= $rList )
            	        	{
            	        	    retval.ST = templateLib.GetInstanceOf("OrElse",
            	        	  new STAttrMap().Add("relations",  list_rList ));
            	        	}


            	        }
            	        break;
            	    case 4 :
            	        // Ada95Walker.g:704:5: OR (rList+= relation[ out exprType ] )+
            	        {
            	        	Match(input,OR,FOLLOW_OR_in_expression2175); 
            	        	// Ada95Walker.g:705:5: (rList+= relation[ out exprType ] )+
            	        	int cnt47 = 0;
            	        	do 
            	        	{
            	        	    int alt47 = 2;
            	        	    int LA47_0 = input.LA(1);

            	        	    if ( ((LA47_0 >= EXPLICIT_DEREFERENCE && LA47_0 <= ATTRIBUTE_REFERENCE) || LA47_0 == QUALIFIED_BY_EXPRESSION || LA47_0 == EXPRESSION || (LA47_0 >= IN_RANGE && LA47_0 <= IN_SUBTYPE) || LA47_0 == TYPE || (LA47_0 >= CHARACTER_LITERAL && LA47_0 <= MOD) || LA47_0 == NULL || LA47_0 == NOT || (LA47_0 >= POW && LA47_0 <= REM) || LA47_0 == FUNCTION) )
            	        	    {
            	        	        alt47 = 1;
            	        	    }


            	        	    switch (alt47) 
            	        		{
            	        			case 1 :
            	        			    // Ada95Walker.g:706:6: rList+= relation[ out exprType ]
            	        			    {
            	        			    	PushFollow(FOLLOW_relation_in_expression2192);
            	        			    	rList = relation(out exprType);
            	        			    	state.followingStackPointer--;

            	        			    	if (list_rList == null) list_rList = new ArrayList();
            	        			    	list_rList.Add(rList.Template);

            	        			    	 if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); 

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt47 >= 1 ) goto loop47;
            	        		            EarlyExitException eee47 =
            	        		                new EarlyExitException(47, input);
            	        		            throw eee47;
            	        	    }
            	        	    cnt47++;
            	        	} while (true);

            	        	loop47:
            	        		;	// Stops C# compiler whining that label 'loop47' has no statements

            	        	 type = "bool"; 


            	        	// TEMPLATE REWRITE
            	        	// 708:27: -> Or(relations= $rList )
            	        	{
            	        	    retval.ST = templateLib.GetInstanceOf("Or",
            	        	  new STAttrMap().Add("relations",  list_rList ));
            	        	}


            	        }
            	        break;
            	    case 5 :
            	        // Ada95Walker.g:709:5: XOR (rList+= relation[ out exprType ] )+
            	        {
            	        	Match(input,XOR,FOLLOW_XOR_in_expression2230); 
            	        	// Ada95Walker.g:710:5: (rList+= relation[ out exprType ] )+
            	        	int cnt48 = 0;
            	        	do 
            	        	{
            	        	    int alt48 = 2;
            	        	    int LA48_0 = input.LA(1);

            	        	    if ( ((LA48_0 >= EXPLICIT_DEREFERENCE && LA48_0 <= ATTRIBUTE_REFERENCE) || LA48_0 == QUALIFIED_BY_EXPRESSION || LA48_0 == EXPRESSION || (LA48_0 >= IN_RANGE && LA48_0 <= IN_SUBTYPE) || LA48_0 == TYPE || (LA48_0 >= CHARACTER_LITERAL && LA48_0 <= MOD) || LA48_0 == NULL || LA48_0 == NOT || (LA48_0 >= POW && LA48_0 <= REM) || LA48_0 == FUNCTION) )
            	        	    {
            	        	        alt48 = 1;
            	        	    }


            	        	    switch (alt48) 
            	        		{
            	        			case 1 :
            	        			    // Ada95Walker.g:711:6: rList+= relation[ out exprType ]
            	        			    {
            	        			    	PushFollow(FOLLOW_relation_in_expression2247);
            	        			    	rList = relation(out exprType);
            	        			    	state.followingStackPointer--;

            	        			    	if (list_rList == null) list_rList = new ArrayList();
            	        			    	list_rList.Add(rList.Template);

            	        			    	 if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); 

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt48 >= 1 ) goto loop48;
            	        		            EarlyExitException eee48 =
            	        		                new EarlyExitException(48, input);
            	        		            throw eee48;
            	        	    }
            	        	    cnt48++;
            	        	} while (true);

            	        	loop48:
            	        		;	// Stops C# compiler whining that label 'loop48' has no statements

            	        	 type = "bool"; 


            	        	// TEMPLATE REWRITE
            	        	// 713:27: -> Xor(relations= $rList )
            	        	{
            	        	    retval.ST = templateLib.GetInstanceOf("Xor",
            	        	  new STAttrMap().Add("relations",  list_rList ));
            	        	}


            	        }
            	        break;
            	    case 6 :
            	        // Ada95Walker.g:714:5: r= relation[ out type ]
            	        {
            	        	PushFollow(FOLLOW_relation_in_expression2289);
            	        	r = relation(out type);
            	        	state.followingStackPointer--;



            	        	// TEMPLATE REWRITE
            	        	// 714:30: -> { $r.st }
            	        	{
            	        	    retval.ST =  ((r != null) ? r.ST : null) ;
            	        	}


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "expression"

    public class relation_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "relation"
    // Ada95Walker.g:719:1: relation[ out string type ] : ( ^( EQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> Equal(left= $lhs.st right= $rhs.st ) | ^( NEQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> NotEqual(left= $lhs.st right= $rhs.st ) | ^( LESS lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> Less(left= $lhs.st right= $rhs.st ) | ^( LEQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> LessOrEqual(left= $lhs.st right= $rhs.st ) | ^( GREATER lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> Greater(left= $lhs.st right= $rhs.st ) | ^( GEQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> GreaterOrEqual(left= $lhs.st right= $rhs.st ) | ^( IN_RANGE e= simple_expression[ out leftType ] r= range[ out range_ ] (n= NOT )? ) -> InRange(expr= $e.st range= $r.st not= $n.text ) | ^( IN_SUBTYPE e= simple_expression[ out leftType ] s= subtype_mark (n= NOT )? ) -> InSubtype(expr= $e.st subtype= $s.st not= $n.text ) | e= simple_expression[ out type ] -> { $e.st });
    public Ada95Walker.relation_return relation(out string type) // throws RecognitionException [1]
    {   
        Ada95Walker.relation_return retval = new Ada95Walker.relation_return();
        retval.Start = input.LT(1);

        CommonTree n = null;
        Ada95Walker.simple_expression_return lhs = default(Ada95Walker.simple_expression_return);

        Ada95Walker.simple_expression_return rhs = default(Ada95Walker.simple_expression_return);

        Ada95Walker.simple_expression_return e = default(Ada95Walker.simple_expression_return);

        Ada95Walker.range_return r = default(Ada95Walker.range_return);

        Ada95Walker.subtype_mark_return s = default(Ada95Walker.subtype_mark_return);



        		string leftType;
        		string rightType;
        		type = string.Empty;
        		Range range_;
        	
        try 
    	{
            // Ada95Walker.g:727:2: ( ^( EQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> Equal(left= $lhs.st right= $rhs.st ) | ^( NEQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> NotEqual(left= $lhs.st right= $rhs.st ) | ^( LESS lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> Less(left= $lhs.st right= $rhs.st ) | ^( LEQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> LessOrEqual(left= $lhs.st right= $rhs.st ) | ^( GREATER lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> Greater(left= $lhs.st right= $rhs.st ) | ^( GEQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] ) -> GreaterOrEqual(left= $lhs.st right= $rhs.st ) | ^( IN_RANGE e= simple_expression[ out leftType ] r= range[ out range_ ] (n= NOT )? ) -> InRange(expr= $e.st range= $r.st not= $n.text ) | ^( IN_SUBTYPE e= simple_expression[ out leftType ] s= subtype_mark (n= NOT )? ) -> InSubtype(expr= $e.st subtype= $s.st not= $n.text ) | e= simple_expression[ out type ] -> { $e.st })
            int alt52 = 9;
            switch ( input.LA(1) ) 
            {
            case EQ:
            	{
                alt52 = 1;
                }
                break;
            case NEQ:
            	{
                alt52 = 2;
                }
                break;
            case LESS:
            	{
                alt52 = 3;
                }
                break;
            case LEQ:
            	{
                alt52 = 4;
                }
                break;
            case GREATER:
            	{
                alt52 = 5;
                }
                break;
            case GEQ:
            	{
                alt52 = 6;
                }
                break;
            case IN_RANGE:
            	{
                alt52 = 7;
                }
                break;
            case IN_SUBTYPE:
            	{
                alt52 = 8;
                }
                break;
            case EXPLICIT_DEREFERENCE:
            case INDEXED_COMPONENT:
            case SLICE:
            case SELECTED_COMPONENT:
            case OBJECT:
            case ATTRIBUTE_REFERENCE:
            case QUALIFIED_BY_EXPRESSION:
            case EXPRESSION:
            case TYPE:
            case CHARACTER_LITERAL:
            case MOD:
            case NULL:
            case NOT:
            case POW:
            case ABS:
            case NUMERIC_LITERAL:
            case STRING_LITERAL:
            case PLUS:
            case MINUS:
            case CONCAT:
            case MULT:
            case DIV:
            case REM:
            case FUNCTION:
            	{
                alt52 = 9;
                }
                break;
            	default:
            	    NoViableAltException nvae_d52s0 =
            	        new NoViableAltException("", 52, 0, input);

            	    throw nvae_d52s0;
            }

            switch (alt52) 
            {
                case 1 :
                    // Ada95Walker.g:728:3: ^( EQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] )
                    {
                    	Match(input,EQ,FOLLOW_EQ_in_relation2327); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_simple_expression_in_relation2333);
                    	lhs = simple_expression(out leftType);
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_simple_expression_in_relation2340);
                    	rhs = simple_expression(out rightType);
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 

                    				if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
                    				type = "bool";
                    			


                    	// TEMPLATE REWRITE
                    	// 732:5: -> Equal(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Equal",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:733:3: ^( NEQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] )
                    {
                    	Match(input,NEQ,FOLLOW_NEQ_in_relation2375); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_simple_expression_in_relation2381);
                    	lhs = simple_expression(out leftType);
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_simple_expression_in_relation2388);
                    	rhs = simple_expression(out rightType);
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 

                    				if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
                    				type = "bool";
                    			


                    	// TEMPLATE REWRITE
                    	// 737:5: -> NotEqual(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("NotEqual",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 3 :
                    // Ada95Walker.g:738:3: ^( LESS lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] )
                    {
                    	Match(input,LESS,FOLLOW_LESS_in_relation2423); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_simple_expression_in_relation2429);
                    	lhs = simple_expression(out leftType);
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_simple_expression_in_relation2436);
                    	rhs = simple_expression(out rightType);
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 

                    				if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
                    				type = "bool";
                    			


                    	// TEMPLATE REWRITE
                    	// 742:5: -> Less(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Less",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 4 :
                    // Ada95Walker.g:743:3: ^( LEQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] )
                    {
                    	Match(input,LEQ,FOLLOW_LEQ_in_relation2471); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_simple_expression_in_relation2477);
                    	lhs = simple_expression(out leftType);
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_simple_expression_in_relation2484);
                    	rhs = simple_expression(out rightType);
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 

                    				if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
                    				type = "bool";
                    			


                    	// TEMPLATE REWRITE
                    	// 747:5: -> LessOrEqual(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("LessOrEqual",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 5 :
                    // Ada95Walker.g:748:3: ^( GREATER lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] )
                    {
                    	Match(input,GREATER,FOLLOW_GREATER_in_relation2519); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_simple_expression_in_relation2525);
                    	lhs = simple_expression(out leftType);
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_simple_expression_in_relation2532);
                    	rhs = simple_expression(out rightType);
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 

                    				if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
                    				type = "bool";
                    			


                    	// TEMPLATE REWRITE
                    	// 752:5: -> Greater(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Greater",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 6 :
                    // Ada95Walker.g:753:3: ^( GEQ lhs= simple_expression[ out leftType ] rhs= simple_expression[ out rightType ] )
                    {
                    	Match(input,GEQ,FOLLOW_GEQ_in_relation2567); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_simple_expression_in_relation2573);
                    	lhs = simple_expression(out leftType);
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_simple_expression_in_relation2580);
                    	rhs = simple_expression(out rightType);
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 

                    				if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
                    				type = "bool";
                    			


                    	// TEMPLATE REWRITE
                    	// 757:5: -> GreaterOrEqual(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("GreaterOrEqual",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 7 :
                    // Ada95Walker.g:758:3: ^( IN_RANGE e= simple_expression[ out leftType ] r= range[ out range_ ] (n= NOT )? )
                    {
                    	Match(input,IN_RANGE,FOLLOW_IN_RANGE_in_relation2615); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_simple_expression_in_relation2621);
                    	e = simple_expression(out leftType);
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_range_in_relation2628);
                    	r = range(out range_);
                    	state.followingStackPointer--;

                    	// Ada95Walker.g:758:77: (n= NOT )?
                    	int alt50 = 2;
                    	int LA50_0 = input.LA(1);

                    	if ( (LA50_0 == NOT) )
                    	{
                    	    alt50 = 1;
                    	}
                    	switch (alt50) 
                    	{
                    	    case 1 :
                    	        // Ada95Walker.g:758:79: n= NOT
                    	        {
                    	        	n=(CommonTree)Match(input,NOT,FOLLOW_NOT_in_relation2637); 

                    	        }
                    	        break;

                    	}


                    	Match(input, Token.UP, null); 

                    				if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input );
                    				type = "bool";
                    			


                    	// TEMPLATE REWRITE
                    	// 762:5: -> InRange(expr= $e.st range= $r.st not= $n.text )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("InRange",
                    	  new STAttrMap().Add("expr",  ((e != null) ? e.ST : null) ).Add("range",  ((r != null) ? r.ST : null) ).Add("not",  ((n != null) ? n.Text : null) ));
                    	}


                    }
                    break;
                case 8 :
                    // Ada95Walker.g:763:3: ^( IN_SUBTYPE e= simple_expression[ out leftType ] s= subtype_mark (n= NOT )? )
                    {
                    	Match(input,IN_SUBTYPE,FOLLOW_IN_SUBTYPE_in_relation2681); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_simple_expression_in_relation2687);
                    	e = simple_expression(out leftType);
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_subtype_mark_in_relation2694);
                    	s = subtype_mark();
                    	state.followingStackPointer--;

                    	// Ada95Walker.g:763:72: (n= NOT )?
                    	int alt51 = 2;
                    	int LA51_0 = input.LA(1);

                    	if ( (LA51_0 == NOT) )
                    	{
                    	    alt51 = 1;
                    	}
                    	switch (alt51) 
                    	{
                    	    case 1 :
                    	        // Ada95Walker.g:763:74: n= NOT
                    	        {
                    	        	n=(CommonTree)Match(input,NOT,FOLLOW_NOT_in_relation2702); 

                    	        }
                    	        break;

                    	}


                    	Match(input, Token.UP, null); 

                    				if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input );
                    				type = "bool";
                    			


                    	// TEMPLATE REWRITE
                    	// 767:5: -> InSubtype(expr= $e.st subtype= $s.st not= $n.text )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("InSubtype",
                    	  new STAttrMap().Add("expr",  ((e != null) ? e.ST : null) ).Add("subtype",  ((s != null) ? s.ST : null) ).Add("not",  ((n != null) ? n.Text : null) ));
                    	}


                    }
                    break;
                case 9 :
                    // Ada95Walker.g:768:3: e= simple_expression[ out type ]
                    {
                    	PushFollow(FOLLOW_simple_expression_in_relation2748);
                    	e = simple_expression(out type);
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 768:37: -> { $e.st }
                    	{
                    	    retval.ST =  ((e != null) ? e.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "relation"

    public class simple_expression_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "simple_expression"
    // Ada95Walker.g:771:1: simple_expression[ out string type ] : ( ^( PLUS lhs= simple_expression[ out leftType ] rhs= term[ out rightType ] ) -> BinaryPlus(left= $lhs.st right= $rhs.st ) | ^( MINUS lhs= simple_expression[ out leftType ] rhs= term[ out rightType ] ) -> BinaryMinus(left= $lhs.st right= $rhs.st ) | ^( CONCAT lhs= simple_expression[ out leftType ] rhs= term[ out rightType ] ) -> { leftType == \"string\" }? StringConcat(left= $lhs.st right= $rhs.st ) -> ArrayConcat(left= $lhs.st right= $rhs.st ) | PLUS t= term[ out type ] -> UnaryPlus(term= $t.st ) | MINUS t= term[ out type ] -> UnaryMinus(term= $t.st ) | t= term[ out type ] -> { $t.st });
    public Ada95Walker.simple_expression_return simple_expression(out string type) // throws RecognitionException [1]
    {   
        Ada95Walker.simple_expression_return retval = new Ada95Walker.simple_expression_return();
        retval.Start = input.LT(1);

        Ada95Walker.simple_expression_return lhs = default(Ada95Walker.simple_expression_return);

        Ada95Walker.term_return rhs = default(Ada95Walker.term_return);

        Ada95Walker.term_return t = default(Ada95Walker.term_return);



        		string leftType;
        		string rightType;
        		type = string.Empty;
        	
        try 
    	{
            // Ada95Walker.g:778:2: ( ^( PLUS lhs= simple_expression[ out leftType ] rhs= term[ out rightType ] ) -> BinaryPlus(left= $lhs.st right= $rhs.st ) | ^( MINUS lhs= simple_expression[ out leftType ] rhs= term[ out rightType ] ) -> BinaryMinus(left= $lhs.st right= $rhs.st ) | ^( CONCAT lhs= simple_expression[ out leftType ] rhs= term[ out rightType ] ) -> { leftType == \"string\" }? StringConcat(left= $lhs.st right= $rhs.st ) -> ArrayConcat(left= $lhs.st right= $rhs.st ) | PLUS t= term[ out type ] -> UnaryPlus(term= $t.st ) | MINUS t= term[ out type ] -> UnaryMinus(term= $t.st ) | t= term[ out type ] -> { $t.st })
            int alt53 = 6;
            switch ( input.LA(1) ) 
            {
            case PLUS:
            	{
                int LA53_1 = input.LA(2);

                if ( (LA53_1 == DOWN) )
                {
                    alt53 = 1;
                }
                else if ( ((LA53_1 >= EXPLICIT_DEREFERENCE && LA53_1 <= ATTRIBUTE_REFERENCE) || LA53_1 == QUALIFIED_BY_EXPRESSION || LA53_1 == EXPRESSION || LA53_1 == TYPE || (LA53_1 >= CHARACTER_LITERAL && LA53_1 <= MOD) || LA53_1 == NULL || LA53_1 == NOT || (LA53_1 >= POW && LA53_1 <= STRING_LITERAL) || (LA53_1 >= MULT && LA53_1 <= REM) || LA53_1 == FUNCTION) )
                {
                    alt53 = 4;
                }
                else 
                {
                    NoViableAltException nvae_d53s1 =
                        new NoViableAltException("", 53, 1, input);

                    throw nvae_d53s1;
                }
                }
                break;
            case MINUS:
            	{
                int LA53_2 = input.LA(2);

                if ( (LA53_2 == DOWN) )
                {
                    alt53 = 2;
                }
                else if ( ((LA53_2 >= EXPLICIT_DEREFERENCE && LA53_2 <= ATTRIBUTE_REFERENCE) || LA53_2 == QUALIFIED_BY_EXPRESSION || LA53_2 == EXPRESSION || LA53_2 == TYPE || (LA53_2 >= CHARACTER_LITERAL && LA53_2 <= MOD) || LA53_2 == NULL || LA53_2 == NOT || (LA53_2 >= POW && LA53_2 <= STRING_LITERAL) || (LA53_2 >= MULT && LA53_2 <= REM) || LA53_2 == FUNCTION) )
                {
                    alt53 = 5;
                }
                else 
                {
                    NoViableAltException nvae_d53s2 =
                        new NoViableAltException("", 53, 2, input);

                    throw nvae_d53s2;
                }
                }
                break;
            case CONCAT:
            	{
                alt53 = 3;
                }
                break;
            case EXPLICIT_DEREFERENCE:
            case INDEXED_COMPONENT:
            case SLICE:
            case SELECTED_COMPONENT:
            case OBJECT:
            case ATTRIBUTE_REFERENCE:
            case QUALIFIED_BY_EXPRESSION:
            case EXPRESSION:
            case TYPE:
            case CHARACTER_LITERAL:
            case MOD:
            case NULL:
            case NOT:
            case POW:
            case ABS:
            case NUMERIC_LITERAL:
            case STRING_LITERAL:
            case MULT:
            case DIV:
            case REM:
            case FUNCTION:
            	{
                alt53 = 6;
                }
                break;
            	default:
            	    NoViableAltException nvae_d53s0 =
            	        new NoViableAltException("", 53, 0, input);

            	    throw nvae_d53s0;
            }

            switch (alt53) 
            {
                case 1 :
                    // Ada95Walker.g:779:3: ^( PLUS lhs= simple_expression[ out leftType ] rhs= term[ out rightType ] )
                    {
                    	Match(input,PLUS,FOLLOW_PLUS_in_simple_expression2776); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_simple_expression_in_simple_expression2785);
                    	lhs = simple_expression(out leftType);
                    	state.followingStackPointer--;

                    	 if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); 
                    	PushFollow(FOLLOW_term_in_simple_expression2797);
                    	rhs = term(out rightType);
                    	state.followingStackPointer--;

                    	 if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); 

                    	Match(input, Token.UP, null); 
                    	 type = "int"; 


                    	// TEMPLATE REWRITE
                    	// 782:23: -> BinaryPlus(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("BinaryPlus",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:783:3: ^( MINUS lhs= simple_expression[ out leftType ] rhs= term[ out rightType ] )
                    {
                    	Match(input,MINUS,FOLLOW_MINUS_in_simple_expression2834); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_simple_expression_in_simple_expression2843);
                    	lhs = simple_expression(out leftType);
                    	state.followingStackPointer--;

                    	 if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); 
                    	PushFollow(FOLLOW_term_in_simple_expression2855);
                    	rhs = term(out rightType);
                    	state.followingStackPointer--;

                    	 if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); 

                    	Match(input, Token.UP, null); 
                    	 type = "int"; 


                    	// TEMPLATE REWRITE
                    	// 786:23: -> BinaryMinus(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("BinaryMinus",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 3 :
                    // Ada95Walker.g:787:3: ^( CONCAT lhs= simple_expression[ out leftType ] rhs= term[ out rightType ] )
                    {
                    	Match(input,CONCAT,FOLLOW_CONCAT_in_simple_expression2892); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_simple_expression_in_simple_expression2901);
                    	lhs = simple_expression(out leftType);
                    	state.followingStackPointer--;

                    	 if ( leftType != "string" && leftType != "array" ) throw new UnexpectedType( leftType, "string/array", input ); 
                    	PushFollow(FOLLOW_term_in_simple_expression2913);
                    	rhs = term(out rightType);
                    	state.followingStackPointer--;

                    	 if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input ); 

                    	Match(input, Token.UP, null); 
                    	 type = "int"; 


                    	// TEMPLATE REWRITE
                    	// 790:23: -> { leftType == \"string\" }? StringConcat(left= $lhs.st right= $rhs.st )
                    	if ( leftType == "string" ) {
                    	    retval.ST = templateLib.GetInstanceOf("StringConcat",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}
                    	else // 791:8: -> ArrayConcat(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("ArrayConcat",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 4 :
                    // Ada95Walker.g:792:3: PLUS t= term[ out type ]
                    {
                    	Match(input,PLUS,FOLLOW_PLUS_in_simple_expression2984); 
                    	PushFollow(FOLLOW_term_in_simple_expression2990);
                    	t = term(out type);
                    	state.followingStackPointer--;


                    				if ( type != "int" ) throw new UnexpectedType( type, "int", input );
                    				type = "int";
                    			


                    	// TEMPLATE REWRITE
                    	// 796:5: -> UnaryPlus(term= $t.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("UnaryPlus",
                    	  new STAttrMap().Add("term",  ((t != null) ? t.ST : null) ));
                    	}


                    }
                    break;
                case 5 :
                    // Ada95Walker.g:797:3: MINUS t= term[ out type ]
                    {
                    	Match(input,MINUS,FOLLOW_MINUS_in_simple_expression3014); 
                    	PushFollow(FOLLOW_term_in_simple_expression3020);
                    	t = term(out type);
                    	state.followingStackPointer--;


                    				if ( type != "int" ) throw new UnexpectedType( type, "int", input );
                    				type = "int";
                    			


                    	// TEMPLATE REWRITE
                    	// 801:5: -> UnaryMinus(term= $t.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("UnaryMinus",
                    	  new STAttrMap().Add("term",  ((t != null) ? t.ST : null) ));
                    	}


                    }
                    break;
                case 6 :
                    // Ada95Walker.g:802:3: t= term[ out type ]
                    {
                    	PushFollow(FOLLOW_term_in_simple_expression3048);
                    	t = term(out type);
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 802:24: -> { $t.st }
                    	{
                    	    retval.ST =  ((t != null) ? t.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "simple_expression"

    public class term_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "term"
    // Ada95Walker.g:805:1: term[ out string type ] : ( ^( MULT lhs= term[ out leftType ] rhs= factor[ out rightType ] ) -> Multiply(left= $lhs.st right= $rhs.st ) | ^( DIV lhs= term[ out leftType ] rhs= factor[ out rightType ] ) -> Divide(left= $lhs.st right= $rhs.st ) | ^( MOD lhs= term[ out leftType ] rhs= factor[ out rightType ] ) -> Modulo(left= $lhs.st right= $rhs.st ) | ^( REM lhs= term[ out leftType ] rhs= factor[ out rightType ] ) -> Reminder(left= $lhs.st right= $rhs.st ) | f= factor[ out type ] -> { $f.st });
    public Ada95Walker.term_return term(out string type) // throws RecognitionException [1]
    {   
        Ada95Walker.term_return retval = new Ada95Walker.term_return();
        retval.Start = input.LT(1);

        Ada95Walker.term_return lhs = default(Ada95Walker.term_return);

        Ada95Walker.factor_return rhs = default(Ada95Walker.factor_return);

        Ada95Walker.factor_return f = default(Ada95Walker.factor_return);



        		string leftType;
        		string rightType;
        		type = string.Empty;
        	
        try 
    	{
            // Ada95Walker.g:812:2: ( ^( MULT lhs= term[ out leftType ] rhs= factor[ out rightType ] ) -> Multiply(left= $lhs.st right= $rhs.st ) | ^( DIV lhs= term[ out leftType ] rhs= factor[ out rightType ] ) -> Divide(left= $lhs.st right= $rhs.st ) | ^( MOD lhs= term[ out leftType ] rhs= factor[ out rightType ] ) -> Modulo(left= $lhs.st right= $rhs.st ) | ^( REM lhs= term[ out leftType ] rhs= factor[ out rightType ] ) -> Reminder(left= $lhs.st right= $rhs.st ) | f= factor[ out type ] -> { $f.st })
            int alt54 = 5;
            switch ( input.LA(1) ) 
            {
            case MULT:
            	{
                alt54 = 1;
                }
                break;
            case DIV:
            	{
                alt54 = 2;
                }
                break;
            case MOD:
            	{
                alt54 = 3;
                }
                break;
            case REM:
            	{
                alt54 = 4;
                }
                break;
            case EXPLICIT_DEREFERENCE:
            case INDEXED_COMPONENT:
            case SLICE:
            case SELECTED_COMPONENT:
            case OBJECT:
            case ATTRIBUTE_REFERENCE:
            case QUALIFIED_BY_EXPRESSION:
            case EXPRESSION:
            case TYPE:
            case CHARACTER_LITERAL:
            case NULL:
            case NOT:
            case POW:
            case ABS:
            case NUMERIC_LITERAL:
            case STRING_LITERAL:
            case FUNCTION:
            	{
                alt54 = 5;
                }
                break;
            	default:
            	    NoViableAltException nvae_d54s0 =
            	        new NoViableAltException("", 54, 0, input);

            	    throw nvae_d54s0;
            }

            switch (alt54) 
            {
                case 1 :
                    // Ada95Walker.g:813:3: ^( MULT lhs= term[ out leftType ] rhs= factor[ out rightType ] )
                    {
                    	Match(input,MULT,FOLLOW_MULT_in_term3076); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_term_in_term3085);
                    	lhs = term(out leftType);
                    	state.followingStackPointer--;

                    	 if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); 
                    	PushFollow(FOLLOW_factor_in_term3097);
                    	rhs = factor(out rightType);
                    	state.followingStackPointer--;

                    	 if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); 

                    	Match(input, Token.UP, null); 
                    	 type = "int"; 


                    	// TEMPLATE REWRITE
                    	// 816:23: -> Multiply(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Multiply",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:817:3: ^( DIV lhs= term[ out leftType ] rhs= factor[ out rightType ] )
                    {
                    	Match(input,DIV,FOLLOW_DIV_in_term3134); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_term_in_term3143);
                    	lhs = term(out leftType);
                    	state.followingStackPointer--;

                    	 if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); 
                    	PushFollow(FOLLOW_factor_in_term3155);
                    	rhs = factor(out rightType);
                    	state.followingStackPointer--;

                    	 if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); 

                    	Match(input, Token.UP, null); 
                    	 type = "int"; 


                    	// TEMPLATE REWRITE
                    	// 820:23: -> Divide(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Divide",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 3 :
                    // Ada95Walker.g:821:3: ^( MOD lhs= term[ out leftType ] rhs= factor[ out rightType ] )
                    {
                    	Match(input,MOD,FOLLOW_MOD_in_term3192); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_term_in_term3201);
                    	lhs = term(out leftType);
                    	state.followingStackPointer--;

                    	 if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); 
                    	PushFollow(FOLLOW_factor_in_term3213);
                    	rhs = factor(out rightType);
                    	state.followingStackPointer--;

                    	 if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); 

                    	Match(input, Token.UP, null); 
                    	 type = "int"; 


                    	// TEMPLATE REWRITE
                    	// 824:23: -> Modulo(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Modulo",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 4 :
                    // Ada95Walker.g:825:3: ^( REM lhs= term[ out leftType ] rhs= factor[ out rightType ] )
                    {
                    	Match(input,REM,FOLLOW_REM_in_term3250); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_term_in_term3259);
                    	lhs = term(out leftType);
                    	state.followingStackPointer--;

                    	 if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); 
                    	PushFollow(FOLLOW_factor_in_term3271);
                    	rhs = factor(out rightType);
                    	state.followingStackPointer--;

                    	 if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); 

                    	Match(input, Token.UP, null); 
                    	 type = "int"; 


                    	// TEMPLATE REWRITE
                    	// 828:23: -> Reminder(left= $lhs.st right= $rhs.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Reminder",
                    	  new STAttrMap().Add("left",  ((lhs != null) ? lhs.ST : null) ).Add("right",  ((rhs != null) ? rhs.ST : null) ));
                    	}


                    }
                    break;
                case 5 :
                    // Ada95Walker.g:829:3: f= factor[ out type ]
                    {
                    	PushFollow(FOLLOW_factor_in_term3310);
                    	f = factor(out type);
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 829:26: -> { $f.st }
                    	{
                    	    retval.ST =  ((f != null) ? f.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "term"

    public class factor_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "factor"
    // Ada95Walker.g:832:1: factor[ out string type ] : ( ^( POW b= primary[ out leftType ] i= primary[ out rightType ] ) -> Pow(base= $b.st index= $i.st ) | ^( ABS v= primary[ out leftType ] ) -> Abs(value= $v.st ) | ^( NOT v= primary[ out leftType ] ) -> Not(value= $v.st ) | p= primary[ out type ] -> { $p.st });
    public Ada95Walker.factor_return factor(out string type) // throws RecognitionException [1]
    {   
        Ada95Walker.factor_return retval = new Ada95Walker.factor_return();
        retval.Start = input.LT(1);

        Ada95Walker.primary_return b = default(Ada95Walker.primary_return);

        Ada95Walker.primary_return i = default(Ada95Walker.primary_return);

        Ada95Walker.primary_return v = default(Ada95Walker.primary_return);

        Ada95Walker.primary_return p = default(Ada95Walker.primary_return);



        		string leftType;
        		string rightType;
        		type = string.Empty;
        	
        try 
    	{
            // Ada95Walker.g:839:2: ( ^( POW b= primary[ out leftType ] i= primary[ out rightType ] ) -> Pow(base= $b.st index= $i.st ) | ^( ABS v= primary[ out leftType ] ) -> Abs(value= $v.st ) | ^( NOT v= primary[ out leftType ] ) -> Not(value= $v.st ) | p= primary[ out type ] -> { $p.st })
            int alt55 = 4;
            switch ( input.LA(1) ) 
            {
            case POW:
            	{
                alt55 = 1;
                }
                break;
            case ABS:
            	{
                alt55 = 2;
                }
                break;
            case NOT:
            	{
                alt55 = 3;
                }
                break;
            case EXPLICIT_DEREFERENCE:
            case INDEXED_COMPONENT:
            case SLICE:
            case SELECTED_COMPONENT:
            case OBJECT:
            case ATTRIBUTE_REFERENCE:
            case QUALIFIED_BY_EXPRESSION:
            case EXPRESSION:
            case TYPE:
            case CHARACTER_LITERAL:
            case NULL:
            case NUMERIC_LITERAL:
            case STRING_LITERAL:
            case FUNCTION:
            	{
                alt55 = 4;
                }
                break;
            	default:
            	    NoViableAltException nvae_d55s0 =
            	        new NoViableAltException("", 55, 0, input);

            	    throw nvae_d55s0;
            }

            switch (alt55) 
            {
                case 1 :
                    // Ada95Walker.g:840:3: ^( POW b= primary[ out leftType ] i= primary[ out rightType ] )
                    {
                    	Match(input,POW,FOLLOW_POW_in_factor3338); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_primary_in_factor3347);
                    	b = primary(out leftType);
                    	state.followingStackPointer--;

                    	 if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); 
                    	PushFollow(FOLLOW_primary_in_factor3359);
                    	i = primary(out rightType);
                    	state.followingStackPointer--;

                    	 if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); 

                    	Match(input, Token.UP, null); 
                    	 type = "int"; 


                    	// TEMPLATE REWRITE
                    	// 843:23: -> Pow(base= $b.st index= $i.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Pow",
                    	  new STAttrMap().Add("base",  ((b != null) ? b.ST : null) ).Add("index",  ((i != null) ? i.ST : null) ));
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:844:3: ^( ABS v= primary[ out leftType ] )
                    {
                    	Match(input,ABS,FOLLOW_ABS_in_factor3396); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_primary_in_factor3405);
                    	v = primary(out leftType);
                    	state.followingStackPointer--;

                    	 if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); 

                    	Match(input, Token.UP, null); 
                    	 type = "int"; 


                    	// TEMPLATE REWRITE
                    	// 846:23: -> Abs(value= $v.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Abs",
                    	  new STAttrMap().Add("value",  ((v != null) ? v.ST : null) ));
                    	}


                    }
                    break;
                case 3 :
                    // Ada95Walker.g:847:3: ^( NOT v= primary[ out leftType ] )
                    {
                    	Match(input,NOT,FOLLOW_NOT_in_factor3435); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_primary_in_factor3444);
                    	v = primary(out leftType);
                    	state.followingStackPointer--;

                    	 if ( leftType != "bool" ) throw new UnexpectedType( leftType, "bool", input ); 

                    	Match(input, Token.UP, null); 
                    	 type = "bool"; 


                    	// TEMPLATE REWRITE
                    	// 849:24: -> Not(value= $v.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Not",
                    	  new STAttrMap().Add("value",  ((v != null) ? v.ST : null) ));
                    	}


                    }
                    break;
                case 4 :
                    // Ada95Walker.g:850:3: p= primary[ out type ]
                    {
                    	PushFollow(FOLLOW_primary_in_factor3476);
                    	p = primary(out type);
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 850:27: -> { $p.st }
                    	{
                    	    retval.ST =  ((p != null) ? p.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "factor"

    public class primary_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "primary"
    // Ada95Walker.g:853:1: primary[ out string type ] : (num= NUMERIC_LITERAL -> NumericLiteral(number= $num.text.Replace( \"_\", \"\" ) ) | NULL | str= STRING_LITERAL -> StringLiteral(string= $str.text ) | n= name[ out type ] -> { $n.st } | q= qualified_expression -> { $q.st } | e= expression[ out type ] -> Subexpression(expr= $e.st ));
    public Ada95Walker.primary_return primary(out string type) // throws RecognitionException [1]
    {   
        Ada95Walker.primary_return retval = new Ada95Walker.primary_return();
        retval.Start = input.LT(1);

        CommonTree num = null;
        CommonTree str = null;
        Ada95Walker.name_return n = default(Ada95Walker.name_return);

        Ada95Walker.qualified_expression_return q = default(Ada95Walker.qualified_expression_return);

        Ada95Walker.expression_return e = default(Ada95Walker.expression_return);



        		type = string.Empty;
        	
        try 
    	{
            // Ada95Walker.g:858:2: (num= NUMERIC_LITERAL -> NumericLiteral(number= $num.text.Replace( \"_\", \"\" ) ) | NULL | str= STRING_LITERAL -> StringLiteral(string= $str.text ) | n= name[ out type ] -> { $n.st } | q= qualified_expression -> { $q.st } | e= expression[ out type ] -> Subexpression(expr= $e.st ))
            int alt56 = 6;
            switch ( input.LA(1) ) 
            {
            case NUMERIC_LITERAL:
            	{
                alt56 = 1;
                }
                break;
            case NULL:
            	{
                alt56 = 2;
                }
                break;
            case STRING_LITERAL:
            	{
                alt56 = 3;
                }
                break;
            case EXPLICIT_DEREFERENCE:
            case INDEXED_COMPONENT:
            case SLICE:
            case SELECTED_COMPONENT:
            case OBJECT:
            case ATTRIBUTE_REFERENCE:
            case TYPE:
            case CHARACTER_LITERAL:
            case FUNCTION:
            	{
                alt56 = 4;
                }
                break;
            case QUALIFIED_BY_EXPRESSION:
            	{
                alt56 = 5;
                }
                break;
            case EXPRESSION:
            	{
                alt56 = 6;
                }
                break;
            	default:
            	    NoViableAltException nvae_d56s0 =
            	        new NoViableAltException("", 56, 0, input);

            	    throw nvae_d56s0;
            }

            switch (alt56) 
            {
                case 1 :
                    // Ada95Walker.g:859:3: num= NUMERIC_LITERAL
                    {
                    	num=(CommonTree)Match(input,NUMERIC_LITERAL,FOLLOW_NUMERIC_LITERAL_in_primary3506); 
                    	 type = "int"; 


                    	// TEMPLATE REWRITE
                    	// 859:44: -> NumericLiteral(number= $num.text.Replace( \"_\", \"\" ) )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("NumericLiteral",
                    	  new STAttrMap().Add("number",  ((num != null) ? num.Text : null).Replace( "_", "" ) ));
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:860:3: NULL
                    {
                    	Match(input,NULL,FOLLOW_NULL_in_primary3528); 

                    }
                    break;
                case 3 :
                    // Ada95Walker.g:861:3: str= STRING_LITERAL
                    {
                    	str=(CommonTree)Match(input,STRING_LITERAL,FOLLOW_STRING_LITERAL_in_primary3538); 
                    	 type = "string"; 


                    	// TEMPLATE REWRITE
                    	// 861:45: -> StringLiteral(string= $str.text )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("StringLiteral",
                    	  new STAttrMap().Add("string",  ((str != null) ? str.Text : null) ));
                    	}


                    }
                    break;
                case 4 :
                    // Ada95Walker.g:863:3: n= name[ out type ]
                    {
                    	PushFollow(FOLLOW_name_in_primary3566);
                    	n = name(out type);
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 863:29: -> { $n.st }
                    	{
                    	    retval.ST =  ((n != null) ? n.ST : null) ;
                    	}


                    }
                    break;
                case 5 :
                    // Ada95Walker.g:864:3: q= qualified_expression
                    {
                    	PushFollow(FOLLOW_qualified_expression_in_primary3586);
                    	q = qualified_expression();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 864:32: -> { $q.st }
                    	{
                    	    retval.ST =  ((q != null) ? q.ST : null) ;
                    	}


                    }
                    break;
                case 6 :
                    // Ada95Walker.g:866:3: e= expression[ out type ]
                    {
                    	PushFollow(FOLLOW_expression_in_primary3606);
                    	e = expression(out type);
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 866:34: -> Subexpression(expr= $e.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Subexpression",
                    	  new STAttrMap().Add("expr",  ((e != null) ? e.ST : null) ));
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "primary"

    public class type_conversion_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "type_conversion"
    // Ada95Walker.g:869:1: type_conversion : ^( TYPE direct_name expression[ out exprType ] ) ;
    public Ada95Walker.type_conversion_return type_conversion() // throws RecognitionException [1]
    {   
        Ada95Walker.type_conversion_return retval = new Ada95Walker.type_conversion_return();
        retval.Start = input.LT(1);


        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:874:2: ( ^( TYPE direct_name expression[ out exprType ] ) )
            // Ada95Walker.g:874:4: ^( TYPE direct_name expression[ out exprType ] )
            {
            	Match(input,TYPE,FOLLOW_TYPE_in_type_conversion3644); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_direct_name_in_type_conversion3646);
            	direct_name();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_expression_in_type_conversion3648);
            	expression(out exprType);
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "type_conversion"

    public class qualified_expression_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "qualified_expression"
    // Ada95Walker.g:876:1: qualified_expression : ^( QUALIFIED_BY_EXPRESSION subtype_mark expression[ out exprType ] ) ;
    public Ada95Walker.qualified_expression_return qualified_expression() // throws RecognitionException [1]
    {   
        Ada95Walker.qualified_expression_return retval = new Ada95Walker.qualified_expression_return();
        retval.Start = input.LT(1);


        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:881:2: ( ^( QUALIFIED_BY_EXPRESSION subtype_mark expression[ out exprType ] ) )
            // Ada95Walker.g:882:3: ^( QUALIFIED_BY_EXPRESSION subtype_mark expression[ out exprType ] )
            {
            	Match(input,QUALIFIED_BY_EXPRESSION,FOLLOW_QUALIFIED_BY_EXPRESSION_in_qualified_expression3672); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_subtype_mark_in_qualified_expression3674);
            	subtype_mark();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_expression_in_qualified_expression3676);
            	expression(out exprType);
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "qualified_expression"

    public class sequence_of_statements_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "sequence_of_statements"
    // Ada95Walker.g:888:1: sequence_of_statements : (s+= statement )+ -> SequenceOfStatements(statements= $s );
    public Ada95Walker.sequence_of_statements_return sequence_of_statements() // throws RecognitionException [1]
    {   
        Ada95Walker.sequence_of_statements_return retval = new Ada95Walker.sequence_of_statements_return();
        retval.Start = input.LT(1);

        IList list_s = null;
        Ada95Walker.statement_return s = default(Ada95Walker.statement_return);
         s = null;
        try 
    	{
            // Ada95Walker.g:889:2: ( (s+= statement )+ -> SequenceOfStatements(statements= $s ))
            // Ada95Walker.g:889:4: (s+= statement )+
            {
            	// Ada95Walker.g:889:4: (s+= statement )+
            	int cnt57 = 0;
            	do 
            	{
            	    int alt57 = 2;
            	    int LA57_0 = input.LA(1);

            	    if ( ((LA57_0 >= SIMPLE_STATEMENT && LA57_0 <= COMPOUND_STATEMENT)) )
            	    {
            	        alt57 = 1;
            	    }


            	    switch (alt57) 
            		{
            			case 1 :
            			    // Ada95Walker.g:889:6: s+= statement
            			    {
            			    	PushFollow(FOLLOW_statement_in_sequence_of_statements3702);
            			    	s = statement();
            			    	state.followingStackPointer--;

            			    	if (list_s == null) list_s = new ArrayList();
            			    	list_s.Add(s.Template);


            			    }
            			    break;

            			default:
            			    if ( cnt57 >= 1 ) goto loop57;
            		            EarlyExitException eee57 =
            		                new EarlyExitException(57, input);
            		            throw eee57;
            	    }
            	    cnt57++;
            	} while (true);

            	loop57:
            		;	// Stops C# compiler whining that label 'loop57' has no statements



            	// TEMPLATE REWRITE
            	// 889:24: -> SequenceOfStatements(statements= $s )
            	{
            	    retval.ST = templateLib.GetInstanceOf("SequenceOfStatements",
            	  new STAttrMap().Add("statements",  list_s ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "sequence_of_statements"

    public class statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "statement"
    // Ada95Walker.g:891:1: statement : ( ^( SIMPLE_STATEMENT ( label )* s= simple_statement ) -> { $s.st } | ^( COMPOUND_STATEMENT ( label )* c= compound_statement ) -> { $c.st });
    public Ada95Walker.statement_return statement() // throws RecognitionException [1]
    {   
        Ada95Walker.statement_return retval = new Ada95Walker.statement_return();
        retval.Start = input.LT(1);

        Ada95Walker.simple_statement_return s = default(Ada95Walker.simple_statement_return);

        Ada95Walker.compound_statement_return c = default(Ada95Walker.compound_statement_return);


        try 
    	{
            // Ada95Walker.g:892:2: ( ^( SIMPLE_STATEMENT ( label )* s= simple_statement ) -> { $s.st } | ^( COMPOUND_STATEMENT ( label )* c= compound_statement ) -> { $c.st })
            int alt60 = 2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0 == SIMPLE_STATEMENT) )
            {
                alt60 = 1;
            }
            else if ( (LA60_0 == COMPOUND_STATEMENT) )
            {
                alt60 = 2;
            }
            else 
            {
                NoViableAltException nvae_d60s0 =
                    new NoViableAltException("", 60, 0, input);

                throw nvae_d60s0;
            }
            switch (alt60) 
            {
                case 1 :
                    // Ada95Walker.g:893:3: ^( SIMPLE_STATEMENT ( label )* s= simple_statement )
                    {
                    	Match(input,SIMPLE_STATEMENT,FOLLOW_SIMPLE_STATEMENT_in_statement3732); 

                    	if ( input.LA(1) == Token.DOWN )
                    	{
                    	    Match(input, Token.DOWN, null); 
                    	    // Ada95Walker.g:893:23: ( label )*
                    	    do 
                    	    {
                    	        int alt58 = 2;
                    	        int LA58_0 = input.LA(1);

                    	        if ( (LA58_0 == LABEL) )
                    	        {
                    	            alt58 = 1;
                    	        }


                    	        switch (alt58) 
                    	    	{
                    	    		case 1 :
                    	    		    // Ada95Walker.g:893:23: label
                    	    		    {
                    	    		    	PushFollow(FOLLOW_label_in_statement3734);
                    	    		    	label();
                    	    		    	state.followingStackPointer--;


                    	    		    }
                    	    		    break;

                    	    		default:
                    	    		    goto loop58;
                    	        }
                    	    } while (true);

                    	    loop58:
                    	    	;	// Stops C# compiler whining that label 'loop58' has no statements

                    	    PushFollow(FOLLOW_simple_statement_in_statement3741);
                    	    s = simple_statement();
                    	    state.followingStackPointer--;


                    	    Match(input, Token.UP, null); 
                    	}


                    	// TEMPLATE REWRITE
                    	// 893:54: -> { $s.st }
                    	{
                    	    retval.ST =  ((s != null) ? s.ST : null) ;
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:894:3: ^( COMPOUND_STATEMENT ( label )* c= compound_statement )
                    {
                    	Match(input,COMPOUND_STATEMENT,FOLLOW_COMPOUND_STATEMENT_in_statement3756); 

                    	Match(input, Token.DOWN, null); 
                    	// Ada95Walker.g:894:25: ( label )*
                    	do 
                    	{
                    	    int alt59 = 2;
                    	    int LA59_0 = input.LA(1);

                    	    if ( (LA59_0 == LABEL) )
                    	    {
                    	        alt59 = 1;
                    	    }


                    	    switch (alt59) 
                    		{
                    			case 1 :
                    			    // Ada95Walker.g:894:25: label
                    			    {
                    			    	PushFollow(FOLLOW_label_in_statement3758);
                    			    	label();
                    			    	state.followingStackPointer--;


                    			    }
                    			    break;

                    			default:
                    			    goto loop59;
                    	    }
                    	} while (true);

                    	loop59:
                    		;	// Stops C# compiler whining that label 'loop59' has no statements

                    	PushFollow(FOLLOW_compound_statement_in_statement3765);
                    	c = compound_statement();
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 


                    	// TEMPLATE REWRITE
                    	// 894:57: -> { $c.st }
                    	{
                    	    retval.ST =  ((c != null) ? c.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "statement"

    public class simple_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "simple_statement"
    // Ada95Walker.g:897:1: simple_statement : ( null_statement | a= assignment_statement -> { $a.st } | e= exit_statement -> { $e.st } | g= goto_statement -> { $g.st } | p= procedure_call_statement -> { $p.st } | r= return_statement -> { $r.st });
    public Ada95Walker.simple_statement_return simple_statement() // throws RecognitionException [1]
    {   
        Ada95Walker.simple_statement_return retval = new Ada95Walker.simple_statement_return();
        retval.Start = input.LT(1);

        Ada95Walker.assignment_statement_return a = default(Ada95Walker.assignment_statement_return);

        Ada95Walker.exit_statement_return e = default(Ada95Walker.exit_statement_return);

        Ada95Walker.goto_statement_return g = default(Ada95Walker.goto_statement_return);

        Ada95Walker.procedure_call_statement_return p = default(Ada95Walker.procedure_call_statement_return);

        Ada95Walker.return_statement_return r = default(Ada95Walker.return_statement_return);


        try 
    	{
            // Ada95Walker.g:898:2: ( null_statement | a= assignment_statement -> { $a.st } | e= exit_statement -> { $e.st } | g= goto_statement -> { $g.st } | p= procedure_call_statement -> { $p.st } | r= return_statement -> { $r.st })
            int alt61 = 6;
            switch ( input.LA(1) ) 
            {
            case UP:
            	{
                alt61 = 1;
                }
                break;
            case ASSIGN:
            	{
                alt61 = 2;
                }
                break;
            case EXIT:
            	{
                alt61 = 3;
                }
                break;
            case GOTO:
            	{
                alt61 = 4;
                }
                break;
            case PROCEDURE:
            	{
                alt61 = 5;
                }
                break;
            case RETURN:
            	{
                alt61 = 6;
                }
                break;
            	default:
            	    NoViableAltException nvae_d61s0 =
            	        new NoViableAltException("", 61, 0, input);

            	    throw nvae_d61s0;
            }

            switch (alt61) 
            {
                case 1 :
                    // Ada95Walker.g:899:3: null_statement
                    {
                    	PushFollow(FOLLOW_null_statement_in_simple_statement3784);
                    	null_statement();
                    	state.followingStackPointer--;


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:900:3: a= assignment_statement
                    {
                    	PushFollow(FOLLOW_assignment_statement_in_simple_statement3794);
                    	a = assignment_statement();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 900:29: -> { $a.st }
                    	{
                    	    retval.ST =  ((a != null) ? a.ST : null) ;
                    	}


                    }
                    break;
                case 3 :
                    // Ada95Walker.g:901:3: e= exit_statement
                    {
                    	PushFollow(FOLLOW_exit_statement_in_simple_statement3809);
                    	e = exit_statement();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 901:25: -> { $e.st }
                    	{
                    	    retval.ST =  ((e != null) ? e.ST : null) ;
                    	}


                    }
                    break;
                case 4 :
                    // Ada95Walker.g:902:3: g= goto_statement
                    {
                    	PushFollow(FOLLOW_goto_statement_in_simple_statement3826);
                    	g = goto_statement();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 902:25: -> { $g.st }
                    	{
                    	    retval.ST =  ((g != null) ? g.ST : null) ;
                    	}


                    }
                    break;
                case 5 :
                    // Ada95Walker.g:903:3: p= procedure_call_statement
                    {
                    	PushFollow(FOLLOW_procedure_call_statement_in_simple_statement3843);
                    	p = procedure_call_statement();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 903:32: -> { $p.st }
                    	{
                    	    retval.ST =  ((p != null) ? p.ST : null) ;
                    	}


                    }
                    break;
                case 6 :
                    // Ada95Walker.g:904:3: r= return_statement
                    {
                    	PushFollow(FOLLOW_return_statement_in_simple_statement3857);
                    	r = return_statement();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 904:26: -> { $r.st }
                    	{
                    	    retval.ST =  ((r != null) ? r.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "simple_statement"

    public class null_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "null_statement"
    // Ada95Walker.g:908:1: null_statement : ;
    public Ada95Walker.null_statement_return null_statement() // throws RecognitionException [1]
    {   
        Ada95Walker.null_statement_return retval = new Ada95Walker.null_statement_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:909:2: ()
            // Ada95Walker.g:909:4: 
            {
            }

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "null_statement"

    public class compound_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "compound_statement"
    // Ada95Walker.g:911:1: compound_statement : (i= if_statement -> { $i.st } | c= case_statement -> { $c.st } | l= loop_statement -> { $l.st } | b= block_statement -> { $b.st });
    public Ada95Walker.compound_statement_return compound_statement() // throws RecognitionException [1]
    {   
        Ada95Walker.compound_statement_return retval = new Ada95Walker.compound_statement_return();
        retval.Start = input.LT(1);

        Ada95Walker.if_statement_return i = default(Ada95Walker.if_statement_return);

        Ada95Walker.case_statement_return c = default(Ada95Walker.case_statement_return);

        Ada95Walker.loop_statement_return l = default(Ada95Walker.loop_statement_return);

        Ada95Walker.block_statement_return b = default(Ada95Walker.block_statement_return);


        try 
    	{
            // Ada95Walker.g:912:2: (i= if_statement -> { $i.st } | c= case_statement -> { $c.st } | l= loop_statement -> { $l.st } | b= block_statement -> { $b.st })
            int alt62 = 4;
            switch ( input.LA(1) ) 
            {
            case IF:
            	{
                alt62 = 1;
                }
                break;
            case CASE:
            	{
                alt62 = 2;
                }
                break;
            case LOOP:
            	{
                alt62 = 3;
                }
                break;
            case BLOCK:
            	{
                alt62 = 4;
                }
                break;
            	default:
            	    NoViableAltException nvae_d62s0 =
            	        new NoViableAltException("", 62, 0, input);

            	    throw nvae_d62s0;
            }

            switch (alt62) 
            {
                case 1 :
                    // Ada95Walker.g:913:3: i= if_statement
                    {
                    	PushFollow(FOLLOW_if_statement_in_compound_statement3890);
                    	i = if_statement();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 913:20: -> { $i.st }
                    	{
                    	    retval.ST =  ((i != null) ? i.ST : null) ;
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:914:3: c= case_statement
                    {
                    	PushFollow(FOLLOW_case_statement_in_compound_statement3904);
                    	c = case_statement();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 914:22: -> { $c.st }
                    	{
                    	    retval.ST =  ((c != null) ? c.ST : null) ;
                    	}


                    }
                    break;
                case 3 :
                    // Ada95Walker.g:915:3: l= loop_statement
                    {
                    	PushFollow(FOLLOW_loop_statement_in_compound_statement3918);
                    	l = loop_statement();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 915:22: -> { $l.st }
                    	{
                    	    retval.ST =  ((l != null) ? l.ST : null) ;
                    	}


                    }
                    break;
                case 4 :
                    // Ada95Walker.g:916:3: b= block_statement
                    {
                    	PushFollow(FOLLOW_block_statement_in_compound_statement3932);
                    	b = block_statement();
                    	state.followingStackPointer--;



                    	// TEMPLATE REWRITE
                    	// 916:23: -> { $b.st }
                    	{
                    	    retval.ST =  ((b != null) ? b.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "compound_statement"

    public class label_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "label"
    // Ada95Walker.g:919:1: label : ^( LABEL statement_identifier ) ;
    public Ada95Walker.label_return label() // throws RecognitionException [1]
    {   
        Ada95Walker.label_return retval = new Ada95Walker.label_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:920:2: ( ^( LABEL statement_identifier ) )
            // Ada95Walker.g:920:4: ^( LABEL statement_identifier )
            {
            	Match(input,LABEL,FOLLOW_LABEL_in_label3949); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_statement_identifier_in_label3951);
            	statement_identifier();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "label"

    public class statement_identifier_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "statement_identifier"
    // Ada95Walker.g:922:1: statement_identifier : IDENTIFIER ;
    public Ada95Walker.statement_identifier_return statement_identifier() // throws RecognitionException [1]
    {   
        Ada95Walker.statement_identifier_return retval = new Ada95Walker.statement_identifier_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:923:2: ( IDENTIFIER )
            // Ada95Walker.g:923:4: IDENTIFIER
            {
            	Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_statement_identifier3963); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "statement_identifier"

    public class assignment_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "assignment_statement"
    // Ada95Walker.g:925:1: assignment_statement : ^( ASSIGN var= name[ out varType ] e= expression[ out exprType ] ) -> Assign(variable= $var.st expr= $e.st );
    public Ada95Walker.assignment_statement_return assignment_statement() // throws RecognitionException [1]
    {   
        Ada95Walker.assignment_statement_return retval = new Ada95Walker.assignment_statement_return();
        retval.Start = input.LT(1);

        Ada95Walker.name_return var = default(Ada95Walker.name_return);

        Ada95Walker.expression_return e = default(Ada95Walker.expression_return);



        		string exprType;
        		string varType;
        	
        try 
    	{
            // Ada95Walker.g:931:2: ( ^( ASSIGN var= name[ out varType ] e= expression[ out exprType ] ) -> Assign(variable= $var.st expr= $e.st ))
            // Ada95Walker.g:932:3: ^( ASSIGN var= name[ out varType ] e= expression[ out exprType ] )
            {
            	Match(input,ASSIGN,FOLLOW_ASSIGN_in_assignment_statement3984); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_name_in_assignment_statement3990);
            	var = name(out varType);
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_expression_in_assignment_statement3997);
            	e = expression(out exprType);
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 
            	 if ( exprType != varType ) throw new UnexpectedType( exprType, varType, input ); 


            	// TEMPLATE REWRITE
            	// 934:4: -> Assign(variable= $var.st expr= $e.st )
            	{
            	    retval.ST = templateLib.GetInstanceOf("Assign",
            	  new STAttrMap().Add("variable",  ((var != null) ? var.ST : null) ).Add("expr",  ((e != null) ? e.ST : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "assignment_statement"

    public class if_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "if_statement"
    // Ada95Walker.g:937:1: if_statement : ^( IF (c+= condition s+= sequence_of_statements )+ ( ELSE elSeq= sequence_of_statements )? ) -> If(conditions= $c sequences= $s elseSequence= $elSeq.st );
    public Ada95Walker.if_statement_return if_statement() // throws RecognitionException [1]
    {   
        Ada95Walker.if_statement_return retval = new Ada95Walker.if_statement_return();
        retval.Start = input.LT(1);

        IList list_c = null;
        IList list_s = null;
        Ada95Walker.sequence_of_statements_return elSeq = default(Ada95Walker.sequence_of_statements_return);

        Ada95Walker.condition_return c = default(Ada95Walker.condition_return);
         c = null;
        Ada95Walker.sequence_of_statements_return s = default(Ada95Walker.sequence_of_statements_return);
         s = null;
        try 
    	{
            // Ada95Walker.g:938:2: ( ^( IF (c+= condition s+= sequence_of_statements )+ ( ELSE elSeq= sequence_of_statements )? ) -> If(conditions= $c sequences= $s elseSequence= $elSeq.st ))
            // Ada95Walker.g:938:4: ^( IF (c+= condition s+= sequence_of_statements )+ ( ELSE elSeq= sequence_of_statements )? )
            {
            	Match(input,IF,FOLLOW_IF_in_if_statement4040); 

            	Match(input, Token.DOWN, null); 
            	// Ada95Walker.g:938:10: (c+= condition s+= sequence_of_statements )+
            	int cnt63 = 0;
            	do 
            	{
            	    int alt63 = 2;
            	    int LA63_0 = input.LA(1);

            	    if ( (LA63_0 == EXPRESSION) )
            	    {
            	        alt63 = 1;
            	    }


            	    switch (alt63) 
            		{
            			case 1 :
            			    // Ada95Walker.g:938:12: c+= condition s+= sequence_of_statements
            			    {
            			    	PushFollow(FOLLOW_condition_in_if_statement4048);
            			    	c = condition();
            			    	state.followingStackPointer--;

            			    	if (list_c == null) list_c = new ArrayList();
            			    	list_c.Add(c.Template);

            			    	PushFollow(FOLLOW_sequence_of_statements_in_if_statement4054);
            			    	s = sequence_of_statements();
            			    	state.followingStackPointer--;

            			    	if (list_s == null) list_s = new ArrayList();
            			    	list_s.Add(s.Template);


            			    }
            			    break;

            			default:
            			    if ( cnt63 >= 1 ) goto loop63;
            		            EarlyExitException eee63 =
            		                new EarlyExitException(63, input);
            		            throw eee63;
            	    }
            	    cnt63++;
            	} while (true);

            	loop63:
            		;	// Stops C# compiler whining that label 'loop63' has no statements

            	// Ada95Walker.g:938:58: ( ELSE elSeq= sequence_of_statements )?
            	int alt64 = 2;
            	int LA64_0 = input.LA(1);

            	if ( (LA64_0 == ELSE) )
            	{
            	    alt64 = 1;
            	}
            	switch (alt64) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:938:60: ELSE elSeq= sequence_of_statements
            	        {
            	        	Match(input,ELSE,FOLLOW_ELSE_in_if_statement4061); 
            	        	PushFollow(FOLLOW_sequence_of_statements_in_if_statement4067);
            	        	elSeq = sequence_of_statements();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 


            	// TEMPLATE REWRITE
            	// 938:101: -> If(conditions= $c sequences= $s elseSequence= $elSeq.st )
            	{
            	    retval.ST = templateLib.GetInstanceOf("If",
            	  new STAttrMap().Add("conditions",  list_c ).Add("sequences",  list_s ).Add("elseSequence",  ((elSeq != null) ? elSeq.ST : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "if_statement"

    public class condition_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "condition"
    // Ada95Walker.g:940:1: condition : e= expression[ out exprType ] -> { $e.st };
    public Ada95Walker.condition_return condition() // throws RecognitionException [1]
    {   
        Ada95Walker.condition_return retval = new Ada95Walker.condition_return();
        retval.Start = input.LT(1);

        Ada95Walker.expression_return e = default(Ada95Walker.expression_return);



        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:945:2: (e= expression[ out exprType ] -> { $e.st })
            // Ada95Walker.g:946:3: e= expression[ out exprType ]
            {
            	PushFollow(FOLLOW_expression_in_condition4122);
            	e = expression(out exprType);
            	state.followingStackPointer--;

            	 if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); 


            	// TEMPLATE REWRITE
            	// 948:4: -> { $e.st }
            	{
            	    retval.ST =  ((e != null) ? e.ST : null) ;
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "condition"

    public class case_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "case_statement"
    // Ada95Walker.g:951:1: case_statement : ^( CASE e= expression[ out exprType ] (c+= case_statement_alternative )* (o= other_case_statement_alternative )? ) -> Case(expr= $e.st alternatives= $c other= $o.st );
    public Ada95Walker.case_statement_return case_statement() // throws RecognitionException [1]
    {   
        Ada95Walker.case_statement_return retval = new Ada95Walker.case_statement_return();
        retval.Start = input.LT(1);

        IList list_c = null;
        Ada95Walker.expression_return e = default(Ada95Walker.expression_return);

        Ada95Walker.other_case_statement_alternative_return o = default(Ada95Walker.other_case_statement_alternative_return);

        Ada95Walker.case_statement_alternative_return c = default(Ada95Walker.case_statement_alternative_return);
         c = null;

        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:956:2: ( ^( CASE e= expression[ out exprType ] (c+= case_statement_alternative )* (o= other_case_statement_alternative )? ) -> Case(expr= $e.st alternatives= $c other= $o.st ))
            // Ada95Walker.g:957:3: ^( CASE e= expression[ out exprType ] (c+= case_statement_alternative )* (o= other_case_statement_alternative )? )
            {
            	Match(input,CASE,FOLLOW_CASE_in_case_statement4156); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_expression_in_case_statement4162);
            	e = expression(out exprType);
            	state.followingStackPointer--;

            	 if ( exprType != "int" ) throw new UnexpectedType( exprType, "int", input ); 
            	// Ada95Walker.g:959:4: (c+= case_statement_alternative )*
            	do 
            	{
            	    int alt65 = 2;
            	    int LA65_0 = input.LA(1);

            	    if ( (LA65_0 == WHEN) )
            	    {
            	        alt65 = 1;
            	    }


            	    switch (alt65) 
            		{
            			case 1 :
            			    // Ada95Walker.g:959:6: c+= case_statement_alternative
            			    {
            			    	PushFollow(FOLLOW_case_statement_alternative_in_case_statement4178);
            			    	c = case_statement_alternative();
            			    	state.followingStackPointer--;

            			    	if (list_c == null) list_c = new ArrayList();
            			    	list_c.Add(c.Template);


            			    }
            			    break;

            			default:
            			    goto loop65;
            	    }
            	} while (true);

            	loop65:
            		;	// Stops C# compiler whining that label 'loop65' has no statements

            	// Ada95Walker.g:959:41: (o= other_case_statement_alternative )?
            	int alt66 = 2;
            	int LA66_0 = input.LA(1);

            	if ( (LA66_0 == OTHERS) )
            	{
            	    alt66 = 1;
            	}
            	switch (alt66) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:959:43: o= other_case_statement_alternative
            	        {
            	        	PushFollow(FOLLOW_other_case_statement_alternative_in_case_statement4189);
            	        	o = other_case_statement_alternative();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 


            	// TEMPLATE REWRITE
            	// 960:4: -> Case(expr= $e.st alternatives= $c other= $o.st )
            	{
            	    retval.ST = templateLib.GetInstanceOf("Case",
            	  new STAttrMap().Add("expr",  ((e != null) ? e.ST : null) ).Add("alternatives",  list_c ).Add("other",  ((o != null) ? o.ST : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "case_statement"

    public class case_statement_alternative_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "case_statement_alternative"
    // Ada95Walker.g:963:1: case_statement_alternative : ^( WHEN d= discrete_choice_list s= sequence_of_statements ) -> CaseAlternative(choices= $d.st statements= $s.st );
    public Ada95Walker.case_statement_alternative_return case_statement_alternative() // throws RecognitionException [1]
    {   
        Ada95Walker.case_statement_alternative_return retval = new Ada95Walker.case_statement_alternative_return();
        retval.Start = input.LT(1);

        Ada95Walker.discrete_choice_list_return d = default(Ada95Walker.discrete_choice_list_return);

        Ada95Walker.sequence_of_statements_return s = default(Ada95Walker.sequence_of_statements_return);


        try 
    	{
            // Ada95Walker.g:964:2: ( ^( WHEN d= discrete_choice_list s= sequence_of_statements ) -> CaseAlternative(choices= $d.st statements= $s.st ))
            // Ada95Walker.g:964:4: ^( WHEN d= discrete_choice_list s= sequence_of_statements )
            {
            	Match(input,WHEN,FOLLOW_WHEN_in_case_statement_alternative4237); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_discrete_choice_list_in_case_statement_alternative4243);
            	d = discrete_choice_list();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_sequence_of_statements_in_case_statement_alternative4249);
            	s = sequence_of_statements();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 


            	// TEMPLATE REWRITE
            	// 964:66: -> CaseAlternative(choices= $d.st statements= $s.st )
            	{
            	    retval.ST = templateLib.GetInstanceOf("CaseAlternative",
            	  new STAttrMap().Add("choices",  ((d != null) ? d.ST : null) ).Add("statements",  ((s != null) ? s.ST : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "case_statement_alternative"

    public class other_case_statement_alternative_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "other_case_statement_alternative"
    // Ada95Walker.g:966:1: other_case_statement_alternative : ^( OTHERS s= sequence_of_statements ) -> CaseAlternative(statements= $s.st );
    public Ada95Walker.other_case_statement_alternative_return other_case_statement_alternative() // throws RecognitionException [1]
    {   
        Ada95Walker.other_case_statement_alternative_return retval = new Ada95Walker.other_case_statement_alternative_return();
        retval.Start = input.LT(1);

        Ada95Walker.sequence_of_statements_return s = default(Ada95Walker.sequence_of_statements_return);


        try 
    	{
            // Ada95Walker.g:967:2: ( ^( OTHERS s= sequence_of_statements ) -> CaseAlternative(statements= $s.st ))
            // Ada95Walker.g:967:4: ^( OTHERS s= sequence_of_statements )
            {
            	Match(input,OTHERS,FOLLOW_OTHERS_in_other_case_statement_alternative4284); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_sequence_of_statements_in_other_case_statement_alternative4290);
            	s = sequence_of_statements();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 


            	// TEMPLATE REWRITE
            	// 967:43: -> CaseAlternative(statements= $s.st )
            	{
            	    retval.ST = templateLib.GetInstanceOf("CaseAlternative",
            	  new STAttrMap().Add("statements",  ((s != null) ? s.ST : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "other_case_statement_alternative"

    public class loop_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "loop_statement"
    // Ada95Walker.g:969:1: loop_statement : ^( LOOP ( statement_identifier )? (scheme= iteration_scheme )? s= handled_sequence_of_statements ( statement_identifier )? ) -> Loop(scheme= $scheme.st statements= $s.st );
    public Ada95Walker.loop_statement_return loop_statement() // throws RecognitionException [1]
    {   
        SymbolTable_stack.Push(new SymbolTable_scope());

        Ada95Walker.loop_statement_return retval = new Ada95Walker.loop_statement_return();
        retval.Start = input.LT(1);

        Ada95Walker.iteration_scheme_return scheme = default(Ada95Walker.iteration_scheme_return);

        Ada95Walker.handled_sequence_of_statements_return s = default(Ada95Walker.handled_sequence_of_statements_return);



        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types =  new SortedDictionary< string, string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Functions =  new SortedDictionary< string, List< Function > >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Procedures =  new SortedDictionary< string, List< Procedure > >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Objects =  new SortedDictionary< string, Object >();
        	
        try 
    	{
            // Ada95Walker.g:978:2: ( ^( LOOP ( statement_identifier )? (scheme= iteration_scheme )? s= handled_sequence_of_statements ( statement_identifier )? ) -> Loop(scheme= $scheme.st statements= $s.st ))
            // Ada95Walker.g:979:3: ^( LOOP ( statement_identifier )? (scheme= iteration_scheme )? s= handled_sequence_of_statements ( statement_identifier )? )
            {
            	Match(input,LOOP,FOLLOW_LOOP_in_loop_statement4332); 

            	Match(input, Token.DOWN, null); 
            	// Ada95Walker.g:979:11: ( statement_identifier )?
            	int alt67 = 2;
            	int LA67_0 = input.LA(1);

            	if ( (LA67_0 == IDENTIFIER) )
            	{
            	    alt67 = 1;
            	}
            	switch (alt67) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:979:11: statement_identifier
            	        {
            	        	PushFollow(FOLLOW_statement_identifier_in_loop_statement4334);
            	        	statement_identifier();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}

            	// Ada95Walker.g:979:33: (scheme= iteration_scheme )?
            	int alt68 = 2;
            	int LA68_0 = input.LA(1);

            	if ( ((LA68_0 >= WHILE && LA68_0 <= FOR)) )
            	{
            	    alt68 = 1;
            	}
            	switch (alt68) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:979:35: scheme= iteration_scheme
            	        {
            	        	PushFollow(FOLLOW_iteration_scheme_in_loop_statement4343);
            	        	scheme = iteration_scheme();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}

            	PushFollow(FOLLOW_handled_sequence_of_statements_in_loop_statement4352);
            	s = handled_sequence_of_statements();
            	state.followingStackPointer--;

            	// Ada95Walker.g:979:99: ( statement_identifier )?
            	int alt69 = 2;
            	int LA69_0 = input.LA(1);

            	if ( (LA69_0 == IDENTIFIER) )
            	{
            	    alt69 = 1;
            	}
            	switch (alt69) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:979:99: statement_identifier
            	        {
            	        	PushFollow(FOLLOW_statement_identifier_in_loop_statement4354);
            	        	statement_identifier();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 


            	// TEMPLATE REWRITE
            	// 980:4: -> Loop(scheme= $scheme.st statements= $s.st )
            	{
            	    retval.ST = templateLib.GetInstanceOf("Loop",
            	  new STAttrMap().Add("scheme",  ((scheme != null) ? scheme.ST : null) ).Add("statements",  ((s != null) ? s.ST : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            SymbolTable_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "loop_statement"

    public class iteration_scheme_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "iteration_scheme"
    // Ada95Walker.g:983:1: iteration_scheme : ( ^( WHILE c= condition ) -> While(condition= $c.st ) | ^( FOR l= loop_parameter_specification ) -> { $l.st });
    public Ada95Walker.iteration_scheme_return iteration_scheme() // throws RecognitionException [1]
    {   
        Ada95Walker.iteration_scheme_return retval = new Ada95Walker.iteration_scheme_return();
        retval.Start = input.LT(1);

        Ada95Walker.condition_return c = default(Ada95Walker.condition_return);

        Ada95Walker.loop_parameter_specification_return l = default(Ada95Walker.loop_parameter_specification_return);


        try 
    	{
            // Ada95Walker.g:984:2: ( ^( WHILE c= condition ) -> While(condition= $c.st ) | ^( FOR l= loop_parameter_specification ) -> { $l.st })
            int alt70 = 2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0 == WHILE) )
            {
                alt70 = 1;
            }
            else if ( (LA70_0 == FOR) )
            {
                alt70 = 2;
            }
            else 
            {
                NoViableAltException nvae_d70s0 =
                    new NoViableAltException("", 70, 0, input);

                throw nvae_d70s0;
            }
            switch (alt70) 
            {
                case 1 :
                    // Ada95Walker.g:985:3: ^( WHILE c= condition )
                    {
                    	Match(input,WHILE,FOLLOW_WHILE_in_iteration_scheme4395); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_condition_in_iteration_scheme4401);
                    	c = condition();
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 


                    	// TEMPLATE REWRITE
                    	// 985:32: -> While(condition= $c.st )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("While",
                    	  new STAttrMap().Add("condition",  ((c != null) ? c.ST : null) ));
                    	}


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:986:3: ^( FOR l= loop_parameter_specification )
                    {
                    	Match(input,FOR,FOLLOW_FOR_in_iteration_scheme4428); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_loop_parameter_specification_in_iteration_scheme4434);
                    	l = loop_parameter_specification();
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 


                    	// TEMPLATE REWRITE
                    	// 986:45: -> { $l.st }
                    	{
                    	    retval.ST =  ((l != null) ? l.ST : null) ;
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "iteration_scheme"

    public class loop_parameter_specification_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "loop_parameter_specification"
    // Ada95Walker.g:989:1: loop_parameter_specification : ^( IN i= defining_identifier (r= REVERSE )? discrete_subtype_definition[ out range ] ) -> For(index= $i.text range= range reverse= $r.text );
    public Ada95Walker.loop_parameter_specification_return loop_parameter_specification() // throws RecognitionException [1]
    {   
        Ada95Walker.loop_parameter_specification_return retval = new Ada95Walker.loop_parameter_specification_return();
        retval.Start = input.LT(1);

        CommonTree r = null;
        Ada95Walker.defining_identifier_return i = default(Ada95Walker.defining_identifier_return);



        		Range range;
        	
        try 
    	{
            // Ada95Walker.g:994:2: ( ^( IN i= defining_identifier (r= REVERSE )? discrete_subtype_definition[ out range ] ) -> For(index= $i.text range= range reverse= $r.text ))
            // Ada95Walker.g:995:3: ^( IN i= defining_identifier (r= REVERSE )? discrete_subtype_definition[ out range ] )
            {
            	Match(input,IN,FOLLOW_IN_in_loop_parameter_specification4462); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_defining_identifier_in_loop_parameter_specification4468);
            	i = defining_identifier();
            	state.followingStackPointer--;

            	// Ada95Walker.g:995:33: (r= REVERSE )?
            	int alt71 = 2;
            	int LA71_0 = input.LA(1);

            	if ( (LA71_0 == REVERSE) )
            	{
            	    alt71 = 1;
            	}
            	switch (alt71) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:995:35: r= REVERSE
            	        {
            	        	r=(CommonTree)Match(input,REVERSE,FOLLOW_REVERSE_in_loop_parameter_specification4476); 

            	        }
            	        break;

            	}

            	PushFollow(FOLLOW_discrete_subtype_definition_in_loop_parameter_specification4481);
            	discrete_subtype_definition(out range);
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 
            	 ((SymbolTable_scope)SymbolTable_stack.Peek()).Objects.Add( ((i != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(i.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(i.Start)) : null), new Object( ((i != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(i.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(i.Start)) : null), "int" ) ); 


            	// TEMPLATE REWRITE
            	// 997:4: -> For(index= $i.text range= range reverse= $r.text )
            	{
            	    retval.ST = templateLib.GetInstanceOf("For",
            	  new STAttrMap().Add("index",  ((i != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(i.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(i.Start)) : null) ).Add("range",  range ).Add("reverse",  ((r != null) ? r.Text : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "loop_parameter_specification"

    public class block_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "block_statement"
    // Ada95Walker.g:1000:1: block_statement : ^( BLOCK ( statement_identifier )? ( declarative_part )? sequence_of_statements ( statement_identifier )? ) ;
    public Ada95Walker.block_statement_return block_statement() // throws RecognitionException [1]
    {   
        SymbolTable_stack.Push(new SymbolTable_scope());

        Ada95Walker.block_statement_return retval = new Ada95Walker.block_statement_return();
        retval.Start = input.LT(1);


        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types =  new SortedDictionary< string, string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Functions =  new SortedDictionary< string, List< Function > >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Procedures =  new SortedDictionary< string, List< Procedure > >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Objects =  new SortedDictionary< string, Object >();
        	
        try 
    	{
            // Ada95Walker.g:1009:2: ( ^( BLOCK ( statement_identifier )? ( declarative_part )? sequence_of_statements ( statement_identifier )? ) )
            // Ada95Walker.g:1009:4: ^( BLOCK ( statement_identifier )? ( declarative_part )? sequence_of_statements ( statement_identifier )? )
            {
            	Match(input,BLOCK,FOLLOW_BLOCK_in_block_statement4544); 

            	Match(input, Token.DOWN, null); 
            	// Ada95Walker.g:1009:13: ( statement_identifier )?
            	int alt72 = 2;
            	int LA72_0 = input.LA(1);

            	if ( (LA72_0 == IDENTIFIER) )
            	{
            	    alt72 = 1;
            	}
            	switch (alt72) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:1009:13: statement_identifier
            	        {
            	        	PushFollow(FOLLOW_statement_identifier_in_block_statement4546);
            	        	statement_identifier();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}

            	// Ada95Walker.g:1009:35: ( declarative_part )?
            	int alt73 = 2;
            	int LA73_0 = input.LA(1);

            	if ( (LA73_0 == DECLARE) )
            	{
            	    alt73 = 1;
            	}
            	switch (alt73) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:1009:35: declarative_part
            	        {
            	        	PushFollow(FOLLOW_declarative_part_in_block_statement4549);
            	        	declarative_part();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}

            	PushFollow(FOLLOW_sequence_of_statements_in_block_statement4552);
            	sequence_of_statements();
            	state.followingStackPointer--;

            	// Ada95Walker.g:1009:76: ( statement_identifier )?
            	int alt74 = 2;
            	int LA74_0 = input.LA(1);

            	if ( (LA74_0 == IDENTIFIER) )
            	{
            	    alt74 = 1;
            	}
            	switch (alt74) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:1009:76: statement_identifier
            	        {
            	        	PushFollow(FOLLOW_statement_identifier_in_block_statement4554);
            	        	statement_identifier();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            SymbolTable_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "block_statement"

    public class exit_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "exit_statement"
    // Ada95Walker.g:1011:1: exit_statement : ^( EXIT ( statement_identifier )? (c= condition )? ) -> Exit(condition= $c.st );
    public Ada95Walker.exit_statement_return exit_statement() // throws RecognitionException [1]
    {   
        Ada95Walker.exit_statement_return retval = new Ada95Walker.exit_statement_return();
        retval.Start = input.LT(1);

        Ada95Walker.condition_return c = default(Ada95Walker.condition_return);


        try 
    	{
            // Ada95Walker.g:1012:2: ( ^( EXIT ( statement_identifier )? (c= condition )? ) -> Exit(condition= $c.st ))
            // Ada95Walker.g:1012:4: ^( EXIT ( statement_identifier )? (c= condition )? )
            {
            	Match(input,EXIT,FOLLOW_EXIT_in_exit_statement4569); 

            	if ( input.LA(1) == Token.DOWN )
            	{
            	    Match(input, Token.DOWN, null); 
            	    // Ada95Walker.g:1012:12: ( statement_identifier )?
            	    int alt75 = 2;
            	    int LA75_0 = input.LA(1);

            	    if ( (LA75_0 == IDENTIFIER) )
            	    {
            	        alt75 = 1;
            	    }
            	    switch (alt75) 
            	    {
            	        case 1 :
            	            // Ada95Walker.g:1012:12: statement_identifier
            	            {
            	            	PushFollow(FOLLOW_statement_identifier_in_exit_statement4571);
            	            	statement_identifier();
            	            	state.followingStackPointer--;


            	            }
            	            break;

            	    }

            	    // Ada95Walker.g:1012:34: (c= condition )?
            	    int alt76 = 2;
            	    int LA76_0 = input.LA(1);

            	    if ( (LA76_0 == EXPRESSION) )
            	    {
            	        alt76 = 1;
            	    }
            	    switch (alt76) 
            	    {
            	        case 1 :
            	            // Ada95Walker.g:1012:36: c= condition
            	            {
            	            	PushFollow(FOLLOW_condition_in_exit_statement4580);
            	            	c = condition();
            	            	state.followingStackPointer--;


            	            }
            	            break;

            	    }


            	    Match(input, Token.UP, null); 
            	}


            	// TEMPLATE REWRITE
            	// 1012:55: -> Exit(condition= $c.st )
            	{
            	    retval.ST = templateLib.GetInstanceOf("Exit",
            	  new STAttrMap().Add("condition",  ((c != null) ? c.ST : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "exit_statement"

    public class goto_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "goto_statement"
    // Ada95Walker.g:1014:1: goto_statement : ^( GOTO statement_identifier ) ;
    public Ada95Walker.goto_statement_return goto_statement() // throws RecognitionException [1]
    {   
        Ada95Walker.goto_statement_return retval = new Ada95Walker.goto_statement_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:1015:2: ( ^( GOTO statement_identifier ) )
            // Ada95Walker.g:1015:4: ^( GOTO statement_identifier )
            {
            	Match(input,GOTO,FOLLOW_GOTO_in_goto_statement4609); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_statement_identifier_in_goto_statement4611);
            	statement_identifier();
            	state.followingStackPointer--;


            	Match(input, Token.UP, null); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "goto_statement"

    public class subprogram_declaration_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "subprogram_declaration"
    // Ada95Walker.g:1017:1: subprogram_declaration : subprogram_specification[ false ] ( ABSTRACT )? ;
    public Ada95Walker.subprogram_declaration_return subprogram_declaration() // throws RecognitionException [1]
    {   
        Ada95Walker.subprogram_declaration_return retval = new Ada95Walker.subprogram_declaration_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:1018:2: ( subprogram_specification[ false ] ( ABSTRACT )? )
            // Ada95Walker.g:1018:4: subprogram_specification[ false ] ( ABSTRACT )?
            {
            	PushFollow(FOLLOW_subprogram_specification_in_subprogram_declaration4623);
            	subprogram_specification(false);
            	state.followingStackPointer--;

            	// Ada95Walker.g:1018:38: ( ABSTRACT )?
            	int alt77 = 2;
            	int LA77_0 = input.LA(1);

            	if ( (LA77_0 == ABSTRACT) )
            	{
            	    alt77 = 1;
            	}
            	switch (alt77) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:1018:38: ABSTRACT
            	        {
            	        	Match(input,ABSTRACT,FOLLOW_ABSTRACT_in_subprogram_declaration4626); 

            	        }
            	        break;

            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "subprogram_declaration"

    protected class subprogram_specification_scope 
    {
        protected internal List< Parameter > parameters;
        protected internal string returnType;
    }
    protected Stack subprogram_specification_stack = new Stack();

    public class subprogram_specification_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "subprogram_specification"
    // Ada95Walker.g:1020:1: subprogram_specification[ bool withBody ] : ( ^( PROCEDURE procID= defining_program_unit_name parameter_profile[ withBody ] ) | ^( FUNCTION funcID= defining_designator parameter_and_result_profile[ withBody ] ) );
    public Ada95Walker.subprogram_specification_return subprogram_specification(bool withBody) // throws RecognitionException [1]
    {   
        subprogram_specification_stack.Push(new subprogram_specification_scope());
        Ada95Walker.subprogram_specification_return retval = new Ada95Walker.subprogram_specification_return();
        retval.Start = input.LT(1);

        Ada95Walker.defining_program_unit_name_return procID = default(Ada95Walker.defining_program_unit_name_return);

        Ada95Walker.defining_designator_return funcID = default(Ada95Walker.defining_designator_return);



        		((subprogram_specification_scope)subprogram_specification_stack.Peek()).parameters =  new List< Parameter >();
        	
        try 
    	{
            // Ada95Walker.g:1030:2: ( ^( PROCEDURE procID= defining_program_unit_name parameter_profile[ withBody ] ) | ^( FUNCTION funcID= defining_designator parameter_and_result_profile[ withBody ] ) )
            int alt78 = 2;
            int LA78_0 = input.LA(1);

            if ( (LA78_0 == PROCEDURE) )
            {
                alt78 = 1;
            }
            else if ( (LA78_0 == FUNCTION) )
            {
                alt78 = 2;
            }
            else 
            {
                NoViableAltException nvae_d78s0 =
                    new NoViableAltException("", 78, 0, input);

                throw nvae_d78s0;
            }
            switch (alt78) 
            {
                case 1 :
                    // Ada95Walker.g:1031:3: ^( PROCEDURE procID= defining_program_unit_name parameter_profile[ withBody ] )
                    {
                    	Match(input,PROCEDURE,FOLLOW_PROCEDURE_in_subprogram_specification4655); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_defining_program_unit_name_in_subprogram_specification4661);
                    	procID = defining_program_unit_name();
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_parameter_profile_in_subprogram_specification4663);
                    	parameter_profile(withBody);
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 

                    					int prev = SymbolTable_stack.Count - ( withBody ? 2 : 1 );
                    					if ( !((SymbolTable_scope)SymbolTable_stack[ prev ]).Procedures.ContainsKey( ((procID != null) ? input.TokenStream.ToString(
                    	  input.TreeAdaptor.GetTokenStartIndex(procID.Start),
                    	  input.TreeAdaptor.GetTokenStopIndex(procID.Start)) : null) ) )
                    						((SymbolTable_scope)SymbolTable_stack[ prev ]).Procedures.Add( ((procID != null) ? input.TokenStream.ToString(
                    	  input.TreeAdaptor.GetTokenStartIndex(procID.Start),
                    	  input.TreeAdaptor.GetTokenStopIndex(procID.Start)) : null), new List< Procedure >() );
                    					Procedure procedure = new Procedure( ((procID != null) ? input.TokenStream.ToString(
                    	  input.TreeAdaptor.GetTokenStartIndex(procID.Start),
                    	  input.TreeAdaptor.GetTokenStopIndex(procID.Start)) : null), ((subprogram_specification_scope)subprogram_specification_stack.Peek()).parameters );
                    					((SymbolTable_scope)SymbolTable_stack[ prev ]).Procedures[ ((procID != null) ? input.TokenStream.ToString(
                    	  input.TreeAdaptor.GetTokenStartIndex(procID.Start),
                    	  input.TreeAdaptor.GetTokenStopIndex(procID.Start)) : null) ].Add( procedure );
                    					if ( withBody )
                    						((subprogram_body_scope)subprogram_body_stack.Peek()).routine =  procedure;
                    				

                    }
                    break;
                case 2 :
                    // Ada95Walker.g:1041:3: ^( FUNCTION funcID= defining_designator parameter_and_result_profile[ withBody ] )
                    {
                    	Match(input,FUNCTION,FOLLOW_FUNCTION_in_subprogram_specification4679); 

                    	Match(input, Token.DOWN, null); 
                    	PushFollow(FOLLOW_defining_designator_in_subprogram_specification4685);
                    	funcID = defining_designator();
                    	state.followingStackPointer--;

                    	PushFollow(FOLLOW_parameter_and_result_profile_in_subprogram_specification4687);
                    	parameter_and_result_profile(withBody);
                    	state.followingStackPointer--;


                    	Match(input, Token.UP, null); 

                    					int prev = SymbolTable_stack.Count - ( withBody ? 2 : 1 );
                    					if ( !((SymbolTable_scope)SymbolTable_stack[ prev ]).Functions.ContainsKey( ((funcID != null) ? input.TokenStream.ToString(
                    	  input.TreeAdaptor.GetTokenStartIndex(funcID.Start),
                    	  input.TreeAdaptor.GetTokenStopIndex(funcID.Start)) : null) ) )
                    						((SymbolTable_scope)SymbolTable_stack[ prev ]).Functions.Add( ((funcID != null) ? input.TokenStream.ToString(
                    	  input.TreeAdaptor.GetTokenStartIndex(funcID.Start),
                    	  input.TreeAdaptor.GetTokenStopIndex(funcID.Start)) : null), new List< Function >() );
                    					Function function = new Function( ((subprogram_specification_scope)subprogram_specification_stack.Peek()).returnType, ((funcID != null) ? input.TokenStream.ToString(
                    	  input.TreeAdaptor.GetTokenStartIndex(funcID.Start),
                    	  input.TreeAdaptor.GetTokenStopIndex(funcID.Start)) : null), ((subprogram_specification_scope)subprogram_specification_stack.Peek()).parameters );
                    					((SymbolTable_scope)SymbolTable_stack[ prev ]).Functions[ ((funcID != null) ? input.TokenStream.ToString(
                    	  input.TreeAdaptor.GetTokenStartIndex(funcID.Start),
                    	  input.TreeAdaptor.GetTokenStopIndex(funcID.Start)) : null) ].Add( function );
                    					if ( withBody )
                    						((subprogram_body_scope)subprogram_body_stack.Peek()).routine =  function;
                    				

                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            subprogram_specification_stack.Pop();
        }
        return retval;
    }
    // $ANTLR end "subprogram_specification"

    public class designator_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "designator"
    // Ada95Walker.g:1053:1: designator : ( IDENTIFIER | operator_symbol );
    public Ada95Walker.designator_return designator() // throws RecognitionException [1]
    {   
        Ada95Walker.designator_return retval = new Ada95Walker.designator_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:1054:2: ( IDENTIFIER | operator_symbol )
            int alt79 = 2;
            int LA79_0 = input.LA(1);

            if ( (LA79_0 == IDENTIFIER) )
            {
                alt79 = 1;
            }
            else if ( (LA79_0 == STRING_LITERAL) )
            {
                alt79 = 2;
            }
            else 
            {
                NoViableAltException nvae_d79s0 =
                    new NoViableAltException("", 79, 0, input);

                throw nvae_d79s0;
            }
            switch (alt79) 
            {
                case 1 :
                    // Ada95Walker.g:1055:3: IDENTIFIER
                    {
                    	Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_designator4708); 

                    }
                    break;
                case 2 :
                    // Ada95Walker.g:1056:3: operator_symbol
                    {
                    	PushFollow(FOLLOW_operator_symbol_in_designator4714);
                    	operator_symbol();
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "designator"

    public class defining_designator_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "defining_designator"
    // Ada95Walker.g:1059:1: defining_designator : ( defining_program_unit_name | defining_operator_symbol );
    public Ada95Walker.defining_designator_return defining_designator() // throws RecognitionException [1]
    {   
        Ada95Walker.defining_designator_return retval = new Ada95Walker.defining_designator_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:1060:2: ( defining_program_unit_name | defining_operator_symbol )
            int alt80 = 2;
            int LA80_0 = input.LA(1);

            if ( (LA80_0 == IDENTIFIER) )
            {
                alt80 = 1;
            }
            else if ( (LA80_0 == STRING_LITERAL) )
            {
                alt80 = 2;
            }
            else 
            {
                NoViableAltException nvae_d80s0 =
                    new NoViableAltException("", 80, 0, input);

                throw nvae_d80s0;
            }
            switch (alt80) 
            {
                case 1 :
                    // Ada95Walker.g:1061:3: defining_program_unit_name
                    {
                    	PushFollow(FOLLOW_defining_program_unit_name_in_defining_designator4727);
                    	defining_program_unit_name();
                    	state.followingStackPointer--;


                    }
                    break;
                case 2 :
                    // Ada95Walker.g:1062:3: defining_operator_symbol
                    {
                    	PushFollow(FOLLOW_defining_operator_symbol_in_defining_designator4733);
                    	defining_operator_symbol();
                    	state.followingStackPointer--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "defining_designator"

    public class defining_program_unit_name_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "defining_program_unit_name"
    // Ada95Walker.g:1065:1: defining_program_unit_name : defining_identifier ;
    public Ada95Walker.defining_program_unit_name_return defining_program_unit_name() // throws RecognitionException [1]
    {   
        Ada95Walker.defining_program_unit_name_return retval = new Ada95Walker.defining_program_unit_name_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:1066:2: ( defining_identifier )
            // Ada95Walker.g:1066:4: defining_identifier
            {
            	PushFollow(FOLLOW_defining_identifier_in_defining_program_unit_name4744);
            	defining_identifier();
            	state.followingStackPointer--;


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "defining_program_unit_name"

    public class operator_symbol_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "operator_symbol"
    // Ada95Walker.g:1068:1: operator_symbol : STRING_LITERAL ;
    public Ada95Walker.operator_symbol_return operator_symbol() // throws RecognitionException [1]
    {   
        Ada95Walker.operator_symbol_return retval = new Ada95Walker.operator_symbol_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:1069:2: ( STRING_LITERAL )
            // Ada95Walker.g:1069:4: STRING_LITERAL
            {
            	Match(input,STRING_LITERAL,FOLLOW_STRING_LITERAL_in_operator_symbol4754); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "operator_symbol"

    public class defining_operator_symbol_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "defining_operator_symbol"
    // Ada95Walker.g:1071:1: defining_operator_symbol : operator_symbol ;
    public Ada95Walker.defining_operator_symbol_return defining_operator_symbol() // throws RecognitionException [1]
    {   
        Ada95Walker.defining_operator_symbol_return retval = new Ada95Walker.defining_operator_symbol_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:1072:2: ( operator_symbol )
            // Ada95Walker.g:1072:4: operator_symbol
            {
            	PushFollow(FOLLOW_operator_symbol_in_defining_operator_symbol4764);
            	operator_symbol();
            	state.followingStackPointer--;


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "defining_operator_symbol"

    public class parameter_profile_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "parameter_profile"
    // Ada95Walker.g:1074:1: parameter_profile[ bool withBody ] : (f= formal_part[ withBody ] )? ;
    public Ada95Walker.parameter_profile_return parameter_profile(bool withBody) // throws RecognitionException [1]
    {   
        Ada95Walker.parameter_profile_return retval = new Ada95Walker.parameter_profile_return();
        retval.Start = input.LT(1);

        Ada95Walker.formal_part_return f = default(Ada95Walker.formal_part_return);


        try 
    	{
            // Ada95Walker.g:1075:2: ( (f= formal_part[ withBody ] )? )
            // Ada95Walker.g:1075:4: (f= formal_part[ withBody ] )?
            {
            	// Ada95Walker.g:1075:4: (f= formal_part[ withBody ] )?
            	int alt81 = 2;
            	int LA81_0 = input.LA(1);

            	if ( (LA81_0 == PARAMETER) )
            	{
            	    alt81 = 1;
            	}
            	switch (alt81) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:1075:6: f= formal_part[ withBody ]
            	        {
            	        	PushFollow(FOLLOW_formal_part_in_parameter_profile4781);
            	        	f = formal_part(withBody);
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "parameter_profile"

    public class parameter_and_result_profile_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "parameter_and_result_profile"
    // Ada95Walker.g:1077:1: parameter_and_result_profile[ bool withBody ] : (f= formal_part[ withBody ] )? RETURN t= subtype_mark ;
    public Ada95Walker.parameter_and_result_profile_return parameter_and_result_profile(bool withBody) // throws RecognitionException [1]
    {   
        Ada95Walker.parameter_and_result_profile_return retval = new Ada95Walker.parameter_and_result_profile_return();
        retval.Start = input.LT(1);

        Ada95Walker.formal_part_return f = default(Ada95Walker.formal_part_return);

        Ada95Walker.subtype_mark_return t = default(Ada95Walker.subtype_mark_return);


        try 
    	{
            // Ada95Walker.g:1078:2: ( (f= formal_part[ withBody ] )? RETURN t= subtype_mark )
            // Ada95Walker.g:1078:4: (f= formal_part[ withBody ] )? RETURN t= subtype_mark
            {
            	// Ada95Walker.g:1078:4: (f= formal_part[ withBody ] )?
            	int alt82 = 2;
            	int LA82_0 = input.LA(1);

            	if ( (LA82_0 == PARAMETER) )
            	{
            	    alt82 = 1;
            	}
            	switch (alt82) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:1078:6: f= formal_part[ withBody ]
            	        {
            	        	PushFollow(FOLLOW_formal_part_in_parameter_and_result_profile4802);
            	        	f = formal_part(withBody);
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}

            	Match(input,RETURN,FOLLOW_RETURN_in_parameter_and_result_profile4808); 
            	PushFollow(FOLLOW_subtype_mark_in_parameter_and_result_profile4814);
            	t = subtype_mark();
            	state.followingStackPointer--;

            	 ((subprogram_specification_scope)subprogram_specification_stack.Peek()).returnType =  GetType( ((t != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(t.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(t.Start)) : null) ); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "parameter_and_result_profile"

    public class formal_part_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "formal_part"
    // Ada95Walker.g:1080:1: formal_part[ bool withBody ] : ( parameter_specification[ withBody ] )+ ;
    public Ada95Walker.formal_part_return formal_part(bool withBody) // throws RecognitionException [1]
    {   
        Ada95Walker.formal_part_return retval = new Ada95Walker.formal_part_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:1081:2: ( ( parameter_specification[ withBody ] )+ )
            // Ada95Walker.g:1081:4: ( parameter_specification[ withBody ] )+
            {
            	// Ada95Walker.g:1081:4: ( parameter_specification[ withBody ] )+
            	int cnt83 = 0;
            	do 
            	{
            	    int alt83 = 2;
            	    int LA83_0 = input.LA(1);

            	    if ( (LA83_0 == PARAMETER) )
            	    {
            	        alt83 = 1;
            	    }


            	    switch (alt83) 
            		{
            			case 1 :
            			    // Ada95Walker.g:1081:4: parameter_specification[ withBody ]
            			    {
            			    	PushFollow(FOLLOW_parameter_specification_in_formal_part4827);
            			    	parameter_specification(withBody);
            			    	state.followingStackPointer--;


            			    }
            			    break;

            			default:
            			    if ( cnt83 >= 1 ) goto loop83;
            		            EarlyExitException eee83 =
            		                new EarlyExitException(83, input);
            		            throw eee83;
            	    }
            	    cnt83++;
            	} while (true);

            	loop83:
            		;	// Stops C# compiler whining that label 'loop83' has no statements


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "formal_part"

    public class parameter_specification_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "parameter_specification"
    // Ada95Walker.g:1083:1: parameter_specification[ bool withBody ] : ^( PARAMETER defining_identifier_list m= mode t= subtype_mark (e= default_expression[ out defExprType ] )? ) ;
    public Ada95Walker.parameter_specification_return parameter_specification(bool withBody) // throws RecognitionException [1]
    {   
        Symbols_stack.Push(new Symbols_scope());

        Ada95Walker.parameter_specification_return retval = new Ada95Walker.parameter_specification_return();
        retval.Start = input.LT(1);

        Ada95Walker.mode_return m = default(Ada95Walker.mode_return);

        Ada95Walker.subtype_mark_return t = default(Ada95Walker.subtype_mark_return);

        Ada95Walker.default_expression_return e = default(Ada95Walker.default_expression_return);



        		((Symbols_scope)Symbols_stack.Peek()).IDs =  new SortedSet< string >();
        		string defExprType;
        	
        try 
    	{
            // Ada95Walker.g:1090:2: ( ^( PARAMETER defining_identifier_list m= mode t= subtype_mark (e= default_expression[ out defExprType ] )? ) )
            // Ada95Walker.g:1091:3: ^( PARAMETER defining_identifier_list m= mode t= subtype_mark (e= default_expression[ out defExprType ] )? )
            {
            	Match(input,PARAMETER,FOLLOW_PARAMETER_in_parameter_specification4857); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_defining_identifier_list_in_parameter_specification4859);
            	defining_identifier_list();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_mode_in_parameter_specification4865);
            	m = mode();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_subtype_mark_in_parameter_specification4871);
            	t = subtype_mark();
            	state.followingStackPointer--;

            	// Ada95Walker.g:1091:67: (e= default_expression[ out defExprType ] )?
            	int alt84 = 2;
            	int LA84_0 = input.LA(1);

            	if ( (LA84_0 == EXPRESSION) )
            	{
            	    alt84 = 1;
            	}
            	switch (alt84) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:1091:69: e= default_expression[ out defExprType ]
            	        {
            	        	PushFollow(FOLLOW_default_expression_in_parameter_specification4879);
            	        	e = default_expression(out defExprType);
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            				foreach ( string id in ((Symbols_scope)Symbols_stack.Peek()).IDs )
            				{
            					string paramType = GetType( ((t != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(t.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(t.Start)) : null) );
            					((subprogram_specification_scope)subprogram_specification_stack.Peek()).parameters.Add( new Parameter( ( ((m != null) ? m.ST : null) != null ? ((m != null) ? m.ST : null).ToString() : string.Empty ), paramType, id, ( ((e != null) ? e.ST : null) != null ? ((e != null) ? e.ST : null).ToString() : null ) ) );
            					if ( withBody )
            						((SymbolTable_scope)SymbolTable_stack.Peek()).Objects.Add( id, new Object( id, paramType ) );
            				}
            			

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            Symbols_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "parameter_specification"

    public class mode_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "mode"
    // Ada95Walker.g:1103:1: mode : ( IN | REF -> Mode(mode= \"ref\" ) | OUT -> Mode(mode= \"out\" ));
    public Ada95Walker.mode_return mode() // throws RecognitionException [1]
    {   
        Ada95Walker.mode_return retval = new Ada95Walker.mode_return();
        retval.Start = input.LT(1);

        try 
    	{
            // Ada95Walker.g:1104:2: ( IN | REF -> Mode(mode= \"ref\" ) | OUT -> Mode(mode= \"out\" ))
            int alt85 = 3;
            switch ( input.LA(1) ) 
            {
            case IN:
            	{
                alt85 = 1;
                }
                break;
            case REF:
            	{
                alt85 = 2;
                }
                break;
            case OUT:
            	{
                alt85 = 3;
                }
                break;
            	default:
            	    NoViableAltException nvae_d85s0 =
            	        new NoViableAltException("", 85, 0, input);

            	    throw nvae_d85s0;
            }

            switch (alt85) 
            {
                case 1 :
                    // Ada95Walker.g:1105:3: IN
                    {
                    	Match(input,IN,FOLLOW_IN_in_mode4902); 

                    }
                    break;
                case 2 :
                    // Ada95Walker.g:1106:3: REF
                    {
                    	Match(input,REF,FOLLOW_REF_in_mode4908); 


                    	// TEMPLATE REWRITE
                    	// 1106:7: -> Mode(mode= \"ref\" )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Mode",
                    	  new STAttrMap().Add("mode",  "ref" ));
                    	}


                    }
                    break;
                case 3 :
                    // Ada95Walker.g:1107:3: OUT
                    {
                    	Match(input,OUT,FOLLOW_OUT_in_mode4927); 


                    	// TEMPLATE REWRITE
                    	// 1107:7: -> Mode(mode= \"out\" )
                    	{
                    	    retval.ST = templateLib.GetInstanceOf("Mode",
                    	  new STAttrMap().Add("mode",  "out" ));
                    	}


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "mode"

    public class handled_sequence_of_statements_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "handled_sequence_of_statements"
    // Ada95Walker.g:1111:1: handled_sequence_of_statements : s= sequence_of_statements -> { $s.st };
    public Ada95Walker.handled_sequence_of_statements_return handled_sequence_of_statements() // throws RecognitionException [1]
    {   
        Ada95Walker.handled_sequence_of_statements_return retval = new Ada95Walker.handled_sequence_of_statements_return();
        retval.Start = input.LT(1);

        Ada95Walker.sequence_of_statements_return s = default(Ada95Walker.sequence_of_statements_return);


        try 
    	{
            // Ada95Walker.g:1112:2: (s= sequence_of_statements -> { $s.st })
            // Ada95Walker.g:1112:4: s= sequence_of_statements
            {
            	PushFollow(FOLLOW_sequence_of_statements_in_handled_sequence_of_statements4956);
            	s = sequence_of_statements();
            	state.followingStackPointer--;



            	// TEMPLATE REWRITE
            	// 1112:31: -> { $s.st }
            	{
            	    retval.ST =  ((s != null) ? s.ST : null) ;
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "handled_sequence_of_statements"

    protected class subprogram_body_scope 
    {
        protected internal object routine;
    }
    protected Stack subprogram_body_stack = new Stack();

    public class subprogram_body_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "subprogram_body"
    // Ada95Walker.g:1114:1: subprogram_body : subprogram_specification[ true ] d= declarative_part s= handled_sequence_of_statements ( designator )? -> { $subprogram_body::routine is Function }? SubprogramBody(returnType= ( $subprogram_body::routine as Function ).ReturnType name= ( $subprogram_body::routine as Function ).Name params= ( $subprogram_body::routine as Function ).Parameters declarativePart= $d.st statements= $s.st ) -> SubprogramBody(returnType= ( $subprogram_body::routine as Procedure ).ReturnType name= ( $subprogram_body::routine as Procedure ).Name params= ( $subprogram_body::routine as Procedure ).Parameters declarativePart= $d.st statements= $s.st );
    public Ada95Walker.subprogram_body_return subprogram_body() // throws RecognitionException [1]
    {   
        SymbolTable_stack.Push(new SymbolTable_scope());
        subprogram_body_stack.Push(new subprogram_body_scope());
        Ada95Walker.subprogram_body_return retval = new Ada95Walker.subprogram_body_return();
        retval.Start = input.LT(1);

        Ada95Walker.declarative_part_return d = default(Ada95Walker.declarative_part_return);

        Ada95Walker.handled_sequence_of_statements_return s = default(Ada95Walker.handled_sequence_of_statements_return);



        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types =  new SortedDictionary< string, string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Functions =  new SortedDictionary< string, List< Function > >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Procedures =  new SortedDictionary< string, List< Procedure > >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Objects =  new SortedDictionary< string, Object >();
        	
        try 
    	{
            // Ada95Walker.g:1127:2: ( subprogram_specification[ true ] d= declarative_part s= handled_sequence_of_statements ( designator )? -> { $subprogram_body::routine is Function }? SubprogramBody(returnType= ( $subprogram_body::routine as Function ).ReturnType name= ( $subprogram_body::routine as Function ).Name params= ( $subprogram_body::routine as Function ).Parameters declarativePart= $d.st statements= $s.st ) -> SubprogramBody(returnType= ( $subprogram_body::routine as Procedure ).ReturnType name= ( $subprogram_body::routine as Procedure ).Name params= ( $subprogram_body::routine as Procedure ).Parameters declarativePart= $d.st statements= $s.st ))
            // Ada95Walker.g:1128:3: subprogram_specification[ true ] d= declarative_part s= handled_sequence_of_statements ( designator )?
            {
            	PushFollow(FOLLOW_subprogram_specification_in_subprogram_body4992);
            	subprogram_specification(true);
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_declarative_part_in_subprogram_body4999);
            	d = declarative_part();
            	state.followingStackPointer--;

            	PushFollow(FOLLOW_handled_sequence_of_statements_in_subprogram_body5005);
            	s = handled_sequence_of_statements();
            	state.followingStackPointer--;

            	// Ada95Walker.g:1128:92: ( designator )?
            	int alt86 = 2;
            	int LA86_0 = input.LA(1);

            	if ( (LA86_0 == IDENTIFIER || LA86_0 == STRING_LITERAL) )
            	{
            	    alt86 = 1;
            	}
            	switch (alt86) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:1128:92: designator
            	        {
            	        	PushFollow(FOLLOW_designator_in_subprogram_body5007);
            	        	designator();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}



            	// TEMPLATE REWRITE
            	// 1129:4: -> { $subprogram_body::routine is Function }? SubprogramBody(returnType= ( $subprogram_body::routine as Function ).ReturnType name= ( $subprogram_body::routine as Function ).Name params= ( $subprogram_body::routine as Function ).Parameters declarativePart= $d.st statements= $s.st )
            	if ( ((subprogram_body_scope)subprogram_body_stack.Peek()).routine is Function ) {
            	    retval.ST = templateLib.GetInstanceOf("SubprogramBody",
            	  new STAttrMap().Add("returnType",  ( ((subprogram_body_scope)subprogram_body_stack.Peek()).routine as Function ).ReturnType ).Add("name",  ( ((subprogram_body_scope)subprogram_body_stack.Peek()).routine as Function ).Name ).Add("params",  ( ((subprogram_body_scope)subprogram_body_stack.Peek()).routine as Function ).Parameters ).Add("declarativePart",  ((d != null) ? d.ST : null) ).Add("statements",  ((s != null) ? s.ST : null) ));
            	}
            	else // 1130:4: -> SubprogramBody(returnType= ( $subprogram_body::routine as Procedure ).ReturnType name= ( $subprogram_body::routine as Procedure ).Name params= ( $subprogram_body::routine as Procedure ).Parameters declarativePart= $d.st statements= $s.st )
            	{
            	    retval.ST = templateLib.GetInstanceOf("SubprogramBody",
            	  new STAttrMap().Add("returnType",  ( ((subprogram_body_scope)subprogram_body_stack.Peek()).routine as Procedure ).ReturnType ).Add("name",  ( ((subprogram_body_scope)subprogram_body_stack.Peek()).routine as Procedure ).Name ).Add("params",  ( ((subprogram_body_scope)subprogram_body_stack.Peek()).routine as Procedure ).Parameters ).Add("declarativePart",  ((d != null) ? d.ST : null) ).Add("statements",  ((s != null) ? s.ST : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            SymbolTable_stack.Pop();
            subprogram_body_stack.Pop();
        }
        return retval;
    }
    // $ANTLR end "subprogram_body"

    public class procedure_call_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "procedure_call_statement"
    // Ada95Walker.g:1134:1: procedure_call_statement : ^( PROCEDURE id= direct_name ( actual_parameter_part )? ) -> ProcedureCall(proc= procedure values= $ActualParameters::Values );
    public Ada95Walker.procedure_call_statement_return procedure_call_statement() // throws RecognitionException [1]
    {   
        ActualParameters_stack.Push(new ActualParameters_scope());

        Ada95Walker.procedure_call_statement_return retval = new Ada95Walker.procedure_call_statement_return();
        retval.Start = input.LT(1);

        Ada95Walker.direct_name_return id = default(Ada95Walker.direct_name_return);



        		((ActualParameters_scope)ActualParameters_stack.Peek()).Types =  new List< string >();
        		((ActualParameters_scope)ActualParameters_stack.Peek()).Values =  new List< string >();
        	
        		List< Procedure > procedures;
        		Procedure procedure;
        		
        		string[] modes;
        	
        try 
    	{
            // Ada95Walker.g:1146:2: ( ^( PROCEDURE id= direct_name ( actual_parameter_part )? ) -> ProcedureCall(proc= procedure values= $ActualParameters::Values ))
            // Ada95Walker.g:1147:3: ^( PROCEDURE id= direct_name ( actual_parameter_part )? )
            {
            	Match(input,PROCEDURE,FOLLOW_PROCEDURE_in_procedure_call_statement5138); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_direct_name_in_procedure_call_statement5144);
            	id = direct_name();
            	state.followingStackPointer--;

            	// Ada95Walker.g:1147:33: ( actual_parameter_part )?
            	int alt87 = 2;
            	int LA87_0 = input.LA(1);

            	if ( (LA87_0 == PARAMETER) )
            	{
            	    alt87 = 1;
            	}
            	switch (alt87) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:1147:33: actual_parameter_part
            	        {
            	        	PushFollow(FOLLOW_actual_parameter_part_in_procedure_call_statement5146);
            	        	actual_parameter_part();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            				procedures = GetProcedure( ((id != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(id.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(id.Start)) : null) );
            				if ( procedures == null )
            					throw new UnknownProcedure( ((id != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(id.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(id.Start)) : null), input );
            				procedure = FindBest( procedures, ((ActualParameters_scope)ActualParameters_stack.Peek()).Types );
            			


            	// TEMPLATE REWRITE
            	// 1154:3: -> ProcedureCall(proc= procedure values= $ActualParameters::Values )
            	{
            	    retval.ST = templateLib.GetInstanceOf("ProcedureCall",
            	  new STAttrMap().Add("proc",  procedure ).Add("values",  ((ActualParameters_scope)ActualParameters_stack.Peek()).Values ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            ActualParameters_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "procedure_call_statement"

    public class function_call_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "function_call"
    // Ada95Walker.g:1157:1: function_call[ out string type ] : ^( FUNCTION id= direct_name ( actual_parameter_part )? ) -> FunctionCall(func= function values= $ActualParameters::Values );
    public Ada95Walker.function_call_return function_call(out string type) // throws RecognitionException [1]
    {   
        ActualParameters_stack.Push(new ActualParameters_scope());

        Ada95Walker.function_call_return retval = new Ada95Walker.function_call_return();
        retval.Start = input.LT(1);

        Ada95Walker.direct_name_return id = default(Ada95Walker.direct_name_return);



        		((ActualParameters_scope)ActualParameters_stack.Peek()).Types =  new List< string >();
        		((ActualParameters_scope)ActualParameters_stack.Peek()).Values =  new List< string >();
        				
        		List< Function > functions;
        		Function function;
        		
        		type = string.Empty;
        	
        try 
    	{
            // Ada95Walker.g:1173:2: ( ^( FUNCTION id= direct_name ( actual_parameter_part )? ) -> FunctionCall(func= function values= $ActualParameters::Values ))
            // Ada95Walker.g:1174:3: ^( FUNCTION id= direct_name ( actual_parameter_part )? )
            {
            	Match(input,FUNCTION,FOLLOW_FUNCTION_in_function_call5211); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_direct_name_in_function_call5217);
            	id = direct_name();
            	state.followingStackPointer--;

            	// Ada95Walker.g:1174:32: ( actual_parameter_part )?
            	int alt88 = 2;
            	int LA88_0 = input.LA(1);

            	if ( (LA88_0 == PARAMETER) )
            	{
            	    alt88 = 1;
            	}
            	switch (alt88) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:1174:32: actual_parameter_part
            	        {
            	        	PushFollow(FOLLOW_actual_parameter_part_in_function_call5219);
            	        	actual_parameter_part();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 

            				functions = GetFunction( ((id != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(id.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(id.Start)) : null) );
            				if ( functions == null )
            					throw new UnknownFunction( ((id != null) ? input.TokenStream.ToString(
            	  input.TreeAdaptor.GetTokenStartIndex(id.Start),
            	  input.TreeAdaptor.GetTokenStopIndex(id.Start)) : null), input );
            				function = FindBest( functions, ((ActualParameters_scope)ActualParameters_stack.Peek()).Types );
            			


            	// TEMPLATE REWRITE
            	// 1181:3: -> FunctionCall(func= function values= $ActualParameters::Values )
            	{
            	    retval.ST = templateLib.GetInstanceOf("FunctionCall",
            	  new STAttrMap().Add("func",  function ).Add("values",  ((ActualParameters_scope)ActualParameters_stack.Peek()).Values ));
            	}


            }


            		type = function.ReturnType;
            	
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
            ActualParameters_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "function_call"

    public class actual_parameter_part_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "actual_parameter_part"
    // Ada95Walker.g:1184:1: actual_parameter_part : (p= parameter_association )+ ;
    public Ada95Walker.actual_parameter_part_return actual_parameter_part() // throws RecognitionException [1]
    {   
        Ada95Walker.actual_parameter_part_return retval = new Ada95Walker.actual_parameter_part_return();
        retval.Start = input.LT(1);

        Ada95Walker.parameter_association_return p = default(Ada95Walker.parameter_association_return);


        try 
    	{
            // Ada95Walker.g:1185:2: ( (p= parameter_association )+ )
            // Ada95Walker.g:1185:4: (p= parameter_association )+
            {
            	// Ada95Walker.g:1185:4: (p= parameter_association )+
            	int cnt89 = 0;
            	do 
            	{
            	    int alt89 = 2;
            	    int LA89_0 = input.LA(1);

            	    if ( (LA89_0 == PARAMETER) )
            	    {
            	        alt89 = 1;
            	    }


            	    switch (alt89) 
            		{
            			case 1 :
            			    // Ada95Walker.g:1185:6: p= parameter_association
            			    {
            			    	PushFollow(FOLLOW_parameter_association_in_actual_parameter_part5265);
            			    	p = parameter_association();
            			    	state.followingStackPointer--;

            			    	 ((ActualParameters_scope)ActualParameters_stack.Peek()).Values.Add( ((p != null) ? p.ST : null).ToString() ); 

            			    }
            			    break;

            			default:
            			    if ( cnt89 >= 1 ) goto loop89;
            		            EarlyExitException eee89 =
            		                new EarlyExitException(89, input);
            		            throw eee89;
            	    }
            	    cnt89++;
            	} while (true);

            	loop89:
            		;	// Stops C# compiler whining that label 'loop89' has no statements


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "actual_parameter_part"

    public class parameter_association_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "parameter_association"
    // Ada95Walker.g:1187:1: parameter_association : ^( PARAMETER p= explicit_actual_parameter (id= selector_name )? ) -> ParameterAssociation(param= $p.st selector= $id.st );
    public Ada95Walker.parameter_association_return parameter_association() // throws RecognitionException [1]
    {   
        Ada95Walker.parameter_association_return retval = new Ada95Walker.parameter_association_return();
        retval.Start = input.LT(1);

        Ada95Walker.explicit_actual_parameter_return p = default(Ada95Walker.explicit_actual_parameter_return);

        Ada95Walker.selector_name_return id = default(Ada95Walker.selector_name_return);


        try 
    	{
            // Ada95Walker.g:1188:2: ( ^( PARAMETER p= explicit_actual_parameter (id= selector_name )? ) -> ParameterAssociation(param= $p.st selector= $id.st ))
            // Ada95Walker.g:1188:4: ^( PARAMETER p= explicit_actual_parameter (id= selector_name )? )
            {
            	Match(input,PARAMETER,FOLLOW_PARAMETER_in_parameter_association5282); 

            	Match(input, Token.DOWN, null); 
            	PushFollow(FOLLOW_explicit_actual_parameter_in_parameter_association5288);
            	p = explicit_actual_parameter();
            	state.followingStackPointer--;

            	// Ada95Walker.g:1188:47: (id= selector_name )?
            	int alt90 = 2;
            	int LA90_0 = input.LA(1);

            	if ( (LA90_0 == IDENTIFIER || LA90_0 == CHARACTER_LITERAL || LA90_0 == STRING_LITERAL) )
            	{
            	    alt90 = 1;
            	}
            	switch (alt90) 
            	{
            	    case 1 :
            	        // Ada95Walker.g:1188:49: id= selector_name
            	        {
            	        	PushFollow(FOLLOW_selector_name_in_parameter_association5296);
            	        	id = selector_name();
            	        	state.followingStackPointer--;


            	        }
            	        break;

            	}


            	Match(input, Token.UP, null); 


            	// TEMPLATE REWRITE
            	// 1188:73: -> ParameterAssociation(param= $p.st selector= $id.st )
            	{
            	    retval.ST = templateLib.GetInstanceOf("ParameterAssociation",
            	  new STAttrMap().Add("param",  ((p != null) ? p.ST : null) ).Add("selector",  ((id != null) ? id.ST : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "parameter_association"

    public class explicit_actual_parameter_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "explicit_actual_parameter"
    // Ada95Walker.g:1190:1: explicit_actual_parameter : e= expression[ out argType ] -> { $e.st };
    public Ada95Walker.explicit_actual_parameter_return explicit_actual_parameter() // throws RecognitionException [1]
    {   
        Ada95Walker.explicit_actual_parameter_return retval = new Ada95Walker.explicit_actual_parameter_return();
        retval.Start = input.LT(1);

        Ada95Walker.expression_return e = default(Ada95Walker.expression_return);



        		string argType;
        	
        try 
    	{
            // Ada95Walker.g:1195:2: (e= expression[ out argType ] -> { $e.st })
            // Ada95Walker.g:1195:4: e= expression[ out argType ]
            {
            	PushFollow(FOLLOW_expression_in_explicit_actual_parameter5342);
            	e = expression(out argType);
            	state.followingStackPointer--;

            	 ((ActualParameters_scope)ActualParameters_stack.Peek()).Types.Add( argType ); 


            	// TEMPLATE REWRITE
            	// 1195:79: -> { $e.st }
            	{
            	    retval.ST =  ((e != null) ? e.ST : null) ;
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "explicit_actual_parameter"

    public class return_statement_return : TreeRuleReturnScope
    {
        private StringTemplate st;
        public StringTemplate ST    { get { return st; } set { st = value; } }
        public override object Template 		{ get { return st; } }
        public override string ToString() 		{ return (st == null) ? null : st.ToString(); }
    };

    // $ANTLR start "return_statement"
    // Ada95Walker.g:1197:1: return_statement : ^( RETURN (e= expression[ out exprType ] )? ) -> ReturnStatement(expr= $e.st );
    public Ada95Walker.return_statement_return return_statement() // throws RecognitionException [1]
    {   
        Ada95Walker.return_statement_return retval = new Ada95Walker.return_statement_return();
        retval.Start = input.LT(1);

        Ada95Walker.expression_return e = default(Ada95Walker.expression_return);



        		string exprType;
        	
        try 
    	{
            // Ada95Walker.g:1202:2: ( ^( RETURN (e= expression[ out exprType ] )? ) -> ReturnStatement(expr= $e.st ))
            // Ada95Walker.g:1202:4: ^( RETURN (e= expression[ out exprType ] )? )
            {
            	Match(input,RETURN,FOLLOW_RETURN_in_return_statement5368); 

            	if ( input.LA(1) == Token.DOWN )
            	{
            	    Match(input, Token.DOWN, null); 
            	    // Ada95Walker.g:1202:14: (e= expression[ out exprType ] )?
            	    int alt91 = 2;
            	    int LA91_0 = input.LA(1);

            	    if ( (LA91_0 == EXPRESSION) )
            	    {
            	        alt91 = 1;
            	    }
            	    switch (alt91) 
            	    {
            	        case 1 :
            	            // Ada95Walker.g:1202:16: e= expression[ out exprType ]
            	            {
            	            	PushFollow(FOLLOW_expression_in_return_statement5376);
            	            	e = expression(out exprType);
            	            	state.followingStackPointer--;


            	            }
            	            break;

            	    }


            	    Match(input, Token.UP, null); 
            	}


            	// TEMPLATE REWRITE
            	// 1202:52: -> ReturnStatement(expr= $e.st )
            	{
            	    retval.ST = templateLib.GetInstanceOf("ReturnStatement",
            	  new STAttrMap().Add("expr",  ((e != null) ? e.ST : null) ));
            	}


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "return_statement"

    // Delegated rules


	private void InitializeCyclicDFAs()
	{
	}

 

    public static readonly BitSet FOLLOW_declarative_part_in_program105 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_type_declaration_in_basic_declaration134 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_object_declaration_in_basic_declaration154 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_number_declaration_in_basic_declaration170 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subprogram_declaration_in_basic_declaration186 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_defining_identifier202 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_full_type_declaration_in_type_declaration212 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_TYPE_in_full_type_declaration232 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_full_type_declaration238 = new BitSet(new ulong[]{0x0808480000000700UL});
    public static readonly BitSet FOLLOW_type_definition_in_full_type_declaration242 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_enumeration_type_definition_in_type_definition257 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_integer_type_definition_in_type_definition263 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_array_type_definition_in_type_definition272 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_record_type_definition_in_type_definition278 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_derived_type_definition_in_type_definition287 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_SUBTYPE_in_subtype_declaration300 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_subtype_declaration302 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_indication_in_subtype_declaration304 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_subtype_mark316 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_subtype_indication328 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_constraint_in_subtype_indication330 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_scalar_constraint_in_constraint345 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_range_constraint_in_scalar_constraint362 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_digits_constraint_in_scalar_constraint368 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CONSTANT_in_number_declaration389 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_identifier_list_in_number_declaration391 = new BitSet(new ulong[]{0x0000000020000000UL});
    public static readonly BitSet FOLLOW_expression_in_number_declaration393 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_NEW_in_derived_type_definition408 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_subtype_indication_in_derived_type_definition410 = new BitSet(new ulong[]{0x0000040000000008UL,0x0000000000000008UL});
    public static readonly BitSet FOLLOW_record_extension_part_in_derived_type_definition412 = new BitSet(new ulong[]{0x0000040000000008UL});
    public static readonly BitSet FOLLOW_ABSTRACT_in_derived_type_definition415 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_ID_LIST_in_defining_identifier_list431 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_defining_identifier_list439 = new BitSet(new ulong[]{0x0000000400000008UL});
    public static readonly BitSet FOLLOW_OBJECT_in_object_declaration473 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_identifier_list_in_object_declaration475 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_indication_in_object_declaration481 = new BitSet(new ulong[]{0x0000000020000008UL});
    public static readonly BitSet FOLLOW_expression_in_object_declaration489 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_ARRAY_OBJECT_in_object_declaration538 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_identifier_list_in_object_declaration540 = new BitSet(new ulong[]{0x0000000000000300UL});
    public static readonly BitSet FOLLOW_array_type_definition_in_object_declaration546 = new BitSet(new ulong[]{0x0000000020000008UL});
    public static readonly BitSet FOLLOW_expression_in_object_declaration554 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_range_in_range_constraint590 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_RANGE_in_range616 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_range625 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_range637 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_ENUMERATION_in_enumeration_type_definition658 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_enumeration_literal_specification_in_enumeration_type_definition660 = new BitSet(new ulong[]{0x0004000400000008UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_enumeration_literal_specification675 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_character_literal_in_enumeration_literal_specification681 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CHARACTER_LITERAL_in_defining_character_literal692 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_signed_integer_type_definition_in_integer_type_definition704 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_modular_type_definition_in_integer_type_definition710 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_RANGE_in_signed_integer_type_definition730 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_signed_integer_type_definition736 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_signed_integer_type_definition743 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_MOD_in_modular_type_definition765 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_expression_in_modular_type_definition767 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_floating_point_definition_in_real_type_definition782 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_fixed_point_definition_in_real_type_definition788 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_DIGITS_in_floating_point_definition808 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_expression_in_floating_point_definition810 = new BitSet(new ulong[]{0x0000400000000008UL});
    public static readonly BitSet FOLLOW_real_range_specification_in_floating_point_definition813 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_RANGE_in_real_range_specification835 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_real_range_specification837 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_real_range_specification840 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_ordinary_fixed_point_definition_in_fixed_point_definition855 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_decimal_fixed_point_definition_in_fixed_point_definition861 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ORDINARY_FIXED_in_ordinary_fixed_point_definition881 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_expression_in_ordinary_fixed_point_definition883 = new BitSet(new ulong[]{0x0000400000000000UL});
    public static readonly BitSet FOLLOW_real_range_specification_in_ordinary_fixed_point_definition886 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_DECIMAL_FIXED_in_decimal_fixed_point_definition907 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_expression_in_decimal_fixed_point_definition909 = new BitSet(new ulong[]{0x0000000020000000UL});
    public static readonly BitSet FOLLOW_expression_in_decimal_fixed_point_definition912 = new BitSet(new ulong[]{0x0000400000000008UL});
    public static readonly BitSet FOLLOW_real_range_specification_in_decimal_fixed_point_definition915 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_DIGITS_in_digits_constraint937 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_expression_in_digits_constraint939 = new BitSet(new ulong[]{0x0000400000000008UL});
    public static readonly BitSet FOLLOW_range_constraint_in_digits_constraint942 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_unconstrained_array_definition_in_array_type_definition958 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_constrained_array_definition_in_array_type_definition968 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_UNCONSTRAINED_ARRAY_in_unconstrained_array_definition985 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_index_subtype_definition_in_unconstrained_array_definition987 = new BitSet(new ulong[]{0x0080000400000000UL});
    public static readonly BitSet FOLLOW_OF_in_unconstrained_array_definition990 = new BitSet(new ulong[]{0x0000100400000000UL});
    public static readonly BitSet FOLLOW_component_definition_in_unconstrained_array_definition992 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_index_subtype_definition1004 = new BitSet(new ulong[]{0x0000400000000000UL});
    public static readonly BitSet FOLLOW_RANGE_in_index_subtype_definition1006 = new BitSet(new ulong[]{0x0100000000000000UL});
    public static readonly BitSet FOLLOW_BOX_in_index_subtype_definition1008 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ALIASED_in_component_definition1019 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_indication_in_component_definition1022 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_range_in_discrete_subtype_definition1047 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CONSTRAINED_ARRAY_in_constrained_array_definition1070 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_discrete_subtype_definition_in_constrained_array_definition1074 = new BitSet(new ulong[]{0x0080400000000000UL});
    public static readonly BitSet FOLLOW_OF_in_constrained_array_definition1082 = new BitSet(new ulong[]{0x0000100400000000UL});
    public static readonly BitSet FOLLOW_component_definition_in_constrained_array_definition1088 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_subtype_indication_in_discrete_range1145 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_range_in_discrete_range1152 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_discriminant_specification_in_known_discriminant_part1164 = new BitSet(new ulong[]{0x0000000002000002UL});
    public static readonly BitSet FOLLOW_DISCRIMINANT_SPECIFICATION_in_discriminant_specification1192 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_identifier_list_in_discriminant_specification1194 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_discriminant_specification1196 = new BitSet(new ulong[]{0x0000000020000008UL});
    public static readonly BitSet FOLLOW_default_expression_in_discriminant_specification1198 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_expression_in_default_expression1225 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_RECORD_in_record_type_definition1239 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_ABSTRACT_in_record_type_definition1243 = new BitSet(new ulong[]{0x0200000000000000UL});
    public static readonly BitSet FOLLOW_TAGGED_in_record_type_definition1246 = new BitSet(new ulong[]{0x0400000010000008UL});
    public static readonly BitSet FOLLOW_LIMITED_in_record_type_definition1251 = new BitSet(new ulong[]{0x0000000010000008UL});
    public static readonly BitSet FOLLOW_record_definition_in_record_type_definition1254 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_component_list_in_record_definition1266 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_COMPONENT_LIST_in_component_list1279 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_component_item_in_component_list1281 = new BitSet(new ulong[]{0x4000000000800008UL});
    public static readonly BitSet FOLLOW_variant_part_in_component_list1284 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_component_declaration_in_component_item1297 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_COMPONENT_in_component_declaration1324 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_identifier_list_in_component_declaration1326 = new BitSet(new ulong[]{0x0000100400000000UL});
    public static readonly BitSet FOLLOW_component_definition_in_component_declaration1328 = new BitSet(new ulong[]{0x0000000020000008UL});
    public static readonly BitSet FOLLOW_default_expression_in_component_declaration1330 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_CASE_in_variant_part1351 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_direct_name_in_variant_part1353 = new BitSet(new ulong[]{0x8000000000000008UL,0x0000000000000002UL});
    public static readonly BitSet FOLLOW_variant_in_variant_part1355 = new BitSet(new ulong[]{0x8000000000000008UL,0x0000000000000002UL});
    public static readonly BitSet FOLLOW_other_variant_in_variant_part1358 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_WHEN_in_variant1373 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_discrete_choice_list_in_variant1375 = new BitSet(new ulong[]{0x0000000010000000UL});
    public static readonly BitSet FOLLOW_component_list_in_variant1377 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_OTHERS_in_other_variant1392 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_component_list_in_other_variant1394 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_discrete_choice_in_discrete_choice_list1412 = new BitSet(new ulong[]{0x0000400420000002UL});
    public static readonly BitSet FOLLOW_expression_in_discrete_choice1451 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_discrete_range_in_discrete_choice1478 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_WITH_in_record_extension_part1491 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_record_definition_in_record_extension_part1493 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_ACCESS_in_access_definition1506 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_access_definition1508 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_DECLARE_in_declarative_part1520 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_declarative_item_in_declarative_part1528 = new BitSet(new ulong[]{0x0000000000003008UL});
    public static readonly BitSet FOLLOW_ITEM_in_declarative_item1560 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_basic_declarative_item_in_declarative_item1566 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_BODY_in_declarative_item1580 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_body_in_declarative_item1586 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_basic_declaration_in_basic_declarative_item1612 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_proper_body_in_body1630 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subprogram_body_in_proper_body1648 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_function_call_in_name1676 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_type_conversion_in_name1688 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_slice_in_name1694 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_indexed_component_in_name1704 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_explicit_dereference_in_name1715 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_selected_component_in_name1721 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_OBJECT_in_name1729 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_direct_name_in_name1735 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_attribute_reference_in_name1758 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CHARACTER_LITERAL_in_name1767 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_direct_name1780 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_operator_symbol_in_direct_name1786 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_EXPLICIT_DEREFERENCE_in_explicit_dereference1800 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_direct_name_in_explicit_dereference1802 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_INDEXED_COMPONENT_in_indexed_component1827 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_direct_name_in_indexed_component1833 = new BitSet(new ulong[]{0x0000000020000000UL});
    public static readonly BitSet FOLLOW_expression_in_indexed_component1847 = new BitSet(new ulong[]{0x0000000020000008UL});
    public static readonly BitSet FOLLOW_SLICE_in_slice1885 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_direct_name_in_slice1887 = new BitSet(new ulong[]{0x0000400420000008UL});
    public static readonly BitSet FOLLOW_discrete_range_in_slice1889 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_SELECTED_COMPONENT_in_selected_component1903 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_direct_name_in_selected_component1905 = new BitSet(new ulong[]{0x0004000400000000UL,0x0000000000040000UL});
    public static readonly BitSet FOLLOW_selector_name_in_selected_component1907 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_selector_name1921 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CHARACTER_LITERAL_in_selector_name1927 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_operator_symbol_in_selector_name1933 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ATTRIBUTE_REFERENCE_in_attribute_reference1946 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_direct_name_in_attribute_reference1948 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_attribute_designator_in_attribute_reference1950 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_attribute_designator1969 = new BitSet(new ulong[]{0x0000000020000002UL});
    public static readonly BitSet FOLLOW_expression_in_attribute_designator1971 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_EXPRESSION_in_expression1999 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_THEN_in_expression2010 = new BitSet(new ulong[]{0x200C0009A47E0000UL,0x000010007FFFA000UL});
    public static readonly BitSet FOLLOW_relation_in_expression2027 = new BitSet(new ulong[]{0x200C0009A47E0008UL,0x000010007FFFA000UL});
    public static readonly BitSet FOLLOW_AND_in_expression2065 = new BitSet(new ulong[]{0x200C0009A47E0000UL,0x000010007FFFA000UL});
    public static readonly BitSet FOLLOW_relation_in_expression2082 = new BitSet(new ulong[]{0x200C0009A47E0008UL,0x000010007FFFA000UL});
    public static readonly BitSet FOLLOW_ELSE_in_expression2120 = new BitSet(new ulong[]{0x200C0009A47E0000UL,0x000010007FFFA000UL});
    public static readonly BitSet FOLLOW_relation_in_expression2137 = new BitSet(new ulong[]{0x200C0009A47E0008UL,0x000010007FFFA000UL});
    public static readonly BitSet FOLLOW_OR_in_expression2175 = new BitSet(new ulong[]{0x200C0009A47E0000UL,0x000010007FFFA000UL});
    public static readonly BitSet FOLLOW_relation_in_expression2192 = new BitSet(new ulong[]{0x200C0009A47E0008UL,0x000010007FFFA000UL});
    public static readonly BitSet FOLLOW_XOR_in_expression2230 = new BitSet(new ulong[]{0x200C0009A47E0000UL,0x000010007FFFA000UL});
    public static readonly BitSet FOLLOW_relation_in_expression2247 = new BitSet(new ulong[]{0x200C0009A47E0008UL,0x000010007FFFA000UL});
    public static readonly BitSet FOLLOW_relation_in_expression2289 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_EQ_in_relation2327 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2333 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2340 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_NEQ_in_relation2375 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2381 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2388 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_LESS_in_relation2423 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2429 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2436 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_LEQ_in_relation2471 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2477 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2484 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_GREATER_in_relation2519 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2525 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2532 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_GEQ_in_relation2567 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2573 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2580 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_IN_RANGE_in_relation2615 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2621 = new BitSet(new ulong[]{0x0000400000000000UL});
    public static readonly BitSet FOLLOW_range_in_relation2628 = new BitSet(new ulong[]{0x0000000000000008UL,0x0000000000002000UL});
    public static readonly BitSet FOLLOW_NOT_in_relation2637 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_IN_SUBTYPE_in_relation2681 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2687 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_relation2694 = new BitSet(new ulong[]{0x0000000000000008UL,0x0000000000002000UL});
    public static readonly BitSet FOLLOW_NOT_in_relation2702 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2748 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_PLUS_in_simple_expression2776 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_simple_expression2785 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_term_in_simple_expression2797 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_MINUS_in_simple_expression2834 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_simple_expression2843 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_term_in_simple_expression2855 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_CONCAT_in_simple_expression2892 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_simple_expression_in_simple_expression2901 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_term_in_simple_expression2913 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_PLUS_in_simple_expression2984 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_term_in_simple_expression2990 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_MINUS_in_simple_expression3014 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_term_in_simple_expression3020 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_term_in_simple_expression3048 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_MULT_in_term3076 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_term_in_term3085 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_factor_in_term3097 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_DIV_in_term3134 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_term_in_term3143 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_factor_in_term3155 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_MOD_in_term3192 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_term_in_term3201 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_factor_in_term3213 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_REM_in_term3250 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_term_in_term3259 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_factor_in_term3271 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_factor_in_term3310 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_POW_in_factor3338 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_primary_in_factor3347 = new BitSet(new ulong[]{0x200C0008247E0000UL,0x000010007E07A000UL});
    public static readonly BitSet FOLLOW_primary_in_factor3359 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_ABS_in_factor3396 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_primary_in_factor3405 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_NOT_in_factor3435 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_primary_in_factor3444 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_primary_in_factor3476 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NUMERIC_LITERAL_in_primary3506 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NULL_in_primary3528 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_STRING_LITERAL_in_primary3538 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_name_in_primary3566 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_qualified_expression_in_primary3586 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expression_in_primary3606 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_TYPE_in_type_conversion3644 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_direct_name_in_type_conversion3646 = new BitSet(new ulong[]{0x0000000020000000UL});
    public static readonly BitSet FOLLOW_expression_in_type_conversion3648 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_QUALIFIED_BY_EXPRESSION_in_qualified_expression3672 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_qualified_expression3674 = new BitSet(new ulong[]{0x0000000020000000UL});
    public static readonly BitSet FOLLOW_expression_in_qualified_expression3676 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_statement_in_sequence_of_statements3702 = new BitSet(new ulong[]{0x0000000000018002UL});
    public static readonly BitSet FOLLOW_SIMPLE_STATEMENT_in_statement3732 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_label_in_statement3734 = new BitSet(new ulong[]{0x0000020200000000UL,0x00002E0000000000UL});
    public static readonly BitSet FOLLOW_simple_statement_in_statement3741 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_COMPOUND_STATEMENT_in_statement3756 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_label_in_statement3758 = new BitSet(new ulong[]{0x4000020200004000UL,0x00002E0A00000000UL});
    public static readonly BitSet FOLLOW_compound_statement_in_statement3765 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_null_statement_in_simple_statement3784 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_assignment_statement_in_simple_statement3794 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_exit_statement_in_simple_statement3809 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_goto_statement_in_simple_statement3826 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_procedure_call_statement_in_simple_statement3843 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_return_statement_in_simple_statement3857 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_if_statement_in_compound_statement3890 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_case_statement_in_compound_statement3904 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_loop_statement_in_compound_statement3918 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_block_statement_in_compound_statement3932 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_LABEL_in_label3949 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_label3951 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_statement_identifier3963 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_assignment_statement3984 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_name_in_assignment_statement3990 = new BitSet(new ulong[]{0x0000000020000000UL});
    public static readonly BitSet FOLLOW_expression_in_assignment_statement3997 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_IF_in_if_statement4040 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_condition_in_if_statement4048 = new BitSet(new ulong[]{0x0000000000018000UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_if_statement4054 = new BitSet(new ulong[]{0x0000000020000008UL,0x0000000000000800UL});
    public static readonly BitSet FOLLOW_ELSE_in_if_statement4061 = new BitSet(new ulong[]{0x0000000000018000UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_if_statement4067 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_expression_in_condition4122 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CASE_in_case_statement4156 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_expression_in_case_statement4162 = new BitSet(new ulong[]{0x8000000000000008UL,0x0000000000000002UL});
    public static readonly BitSet FOLLOW_case_statement_alternative_in_case_statement4178 = new BitSet(new ulong[]{0x8000000000000008UL,0x0000000000000002UL});
    public static readonly BitSet FOLLOW_other_case_statement_alternative_in_case_statement4189 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_WHEN_in_case_statement_alternative4237 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_discrete_choice_list_in_case_statement_alternative4243 = new BitSet(new ulong[]{0x0000000000018000UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_case_statement_alternative4249 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_OTHERS_in_other_case_statement_alternative4284 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_other_case_statement_alternative4290 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_LOOP_in_loop_statement4332 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_loop_statement4334 = new BitSet(new ulong[]{0x0000000000018000UL,0x0000003000000000UL});
    public static readonly BitSet FOLLOW_iteration_scheme_in_loop_statement4343 = new BitSet(new ulong[]{0x0000000000018000UL,0x0000003000000000UL});
    public static readonly BitSet FOLLOW_handled_sequence_of_statements_in_loop_statement4352 = new BitSet(new ulong[]{0x0000000400000008UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_loop_statement4354 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_WHILE_in_iteration_scheme4395 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_condition_in_iteration_scheme4401 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_FOR_in_iteration_scheme4428 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_loop_parameter_specification_in_iteration_scheme4434 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_IN_in_loop_parameter_specification4462 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_loop_parameter_specification4468 = new BitSet(new ulong[]{0x0080400000000000UL,0x0000004000000000UL});
    public static readonly BitSet FOLLOW_REVERSE_in_loop_parameter_specification4476 = new BitSet(new ulong[]{0x0080400000000000UL});
    public static readonly BitSet FOLLOW_discrete_subtype_definition_in_loop_parameter_specification4481 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_BLOCK_in_block_statement4544 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_block_statement4546 = new BitSet(new ulong[]{0x0000000000018000UL,0x0000008000000000UL});
    public static readonly BitSet FOLLOW_declarative_part_in_block_statement4549 = new BitSet(new ulong[]{0x0000000000018000UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_block_statement4552 = new BitSet(new ulong[]{0x0000000400000008UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_block_statement4554 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_EXIT_in_exit_statement4569 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_exit_statement4571 = new BitSet(new ulong[]{0x0000000020000008UL,0x0000000000000800UL});
    public static readonly BitSet FOLLOW_condition_in_exit_statement4580 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_GOTO_in_goto_statement4609 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_goto_statement4611 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_subprogram_specification_in_subprogram_declaration4623 = new BitSet(new ulong[]{0x0000040000000002UL});
    public static readonly BitSet FOLLOW_ABSTRACT_in_subprogram_declaration4626 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_PROCEDURE_in_subprogram_specification4655 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_program_unit_name_in_subprogram_specification4661 = new BitSet(new ulong[]{0x0000000001000008UL});
    public static readonly BitSet FOLLOW_parameter_profile_in_subprogram_specification4663 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_FUNCTION_in_subprogram_specification4679 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_designator_in_subprogram_specification4685 = new BitSet(new ulong[]{0x0000000001000000UL,0x0000200000000000UL});
    public static readonly BitSet FOLLOW_parameter_and_result_profile_in_subprogram_specification4687 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_designator4708 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_operator_symbol_in_designator4714 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_program_unit_name_in_defining_designator4727 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_operator_symbol_in_defining_designator4733 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_defining_program_unit_name4744 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_STRING_LITERAL_in_operator_symbol4754 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_operator_symbol_in_defining_operator_symbol4764 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_formal_part_in_parameter_profile4781 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_formal_part_in_parameter_and_result_profile4802 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000200000000000UL});
    public static readonly BitSet FOLLOW_RETURN_in_parameter_and_result_profile4808 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_parameter_and_result_profile4814 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_parameter_specification_in_formal_part4827 = new BitSet(new ulong[]{0x0000000001000002UL});
    public static readonly BitSet FOLLOW_PARAMETER_in_parameter_specification4857 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_defining_identifier_list_in_parameter_specification4859 = new BitSet(new ulong[]{0x0000000000000800UL,0x0000400000004000UL});
    public static readonly BitSet FOLLOW_mode_in_parameter_specification4865 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_parameter_specification4871 = new BitSet(new ulong[]{0x0000000020000008UL});
    public static readonly BitSet FOLLOW_default_expression_in_parameter_specification4879 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_IN_in_mode4902 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_REF_in_mode4908 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_OUT_in_mode4927 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_handled_sequence_of_statements4956 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subprogram_specification_in_subprogram_body4992 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000008000000000UL});
    public static readonly BitSet FOLLOW_declarative_part_in_subprogram_body4999 = new BitSet(new ulong[]{0x0000000000018000UL,0x0000003000000000UL});
    public static readonly BitSet FOLLOW_handled_sequence_of_statements_in_subprogram_body5005 = new BitSet(new ulong[]{0x0004000400000002UL,0x0000000000040000UL});
    public static readonly BitSet FOLLOW_designator_in_subprogram_body5007 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_PROCEDURE_in_procedure_call_statement5138 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_direct_name_in_procedure_call_statement5144 = new BitSet(new ulong[]{0x0000000001000008UL});
    public static readonly BitSet FOLLOW_actual_parameter_part_in_procedure_call_statement5146 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_FUNCTION_in_function_call5211 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_direct_name_in_function_call5217 = new BitSet(new ulong[]{0x0000000001000008UL});
    public static readonly BitSet FOLLOW_actual_parameter_part_in_function_call5219 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_parameter_association_in_actual_parameter_part5265 = new BitSet(new ulong[]{0x0000000001000002UL});
    public static readonly BitSet FOLLOW_PARAMETER_in_parameter_association5282 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_explicit_actual_parameter_in_parameter_association5288 = new BitSet(new ulong[]{0x0004000400000008UL,0x0000000000040000UL});
    public static readonly BitSet FOLLOW_selector_name_in_parameter_association5296 = new BitSet(new ulong[]{0x0000000000000008UL});
    public static readonly BitSet FOLLOW_expression_in_explicit_actual_parameter5342 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_RETURN_in_return_statement5368 = new BitSet(new ulong[]{0x0000000000000004UL});
    public static readonly BitSet FOLLOW_expression_in_return_statement5376 = new BitSet(new ulong[]{0x0000000000000008UL});

}
}