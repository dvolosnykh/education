﻿using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using System.CodeDom;
using System.CodeDom.Compiler;
using Ada95;
using Antlr.Runtime;
using Antlr.Runtime.Tree;
using Antlr.StringTemplate;

using System.Runtime.InteropServices;

namespace lab5_8
{
	class CodeGenerator
	{
		private const string EXECUTABLE_NAME = "temp.exe";

		CompilerResults __results = null;

[ DllImport( "kernel32.dll" ) ]
private static extern bool AllocConsole();

[ DllImport( "kernel32.dll" ) ]
private static extern bool FreeConsole();

		public bool Build( string text, ref string output )
		{
			CodeDomProvider provider = CodeDomProvider.CreateProvider( "CSharp" );
			CompilerParameters options = new CompilerParameters( new string[] { "System.dll" }, EXECUTABLE_NAME );	// also works: new string[] { null }
			options.GenerateExecutable = true;
			options.GenerateInMemory = true;
			options.CompilerOptions = "/optimize";

			bool ok = Ada95ToCSharp( ref text, ref output );
if ( true )
{
	AllocConsole();
	Console.WriteLine( text );
	Console.ReadKey( true );
	FreeConsole();
}
			if ( ok )
			{
				__results = provider.CompileAssemblyFromSource( options, text );
				ok = !__results.Errors.HasErrors;

				if ( output != null )
				{
					StringBuilder sb = new StringBuilder();

					if ( ok )
					{
						sb.AppendLine( "Build succeeded." );
					}
					else
					{
						sb.AppendLine( "Errors encountered while building:" );
						sb.AppendLine();
						foreach ( CompilerError error in __results.Errors )
							sb.AppendLine( error.ToString() );
					}

					output = sb.ToString();
				}
			}

			return ( ok );
		}

		private bool Ada95ToCSharp( ref string text, ref string output )
		{
			ANTLRStringStream input = new ANTLRStringStream( text );
			Ada95Lexer lexer = new Ada95Lexer( input );
			CommonTokenStream tokens = new CommonTokenStream( lexer );
			Ada95Parser parser = new Ada95Parser( tokens );
			Ada95Parser.program_return parserResult = parser.program();

			//text = ( parserResult.Tree as CommonTree ).ToStringTree();
			bool ok = ( parser.NumberOfSyntaxErrors == 0 );
			//if ( false )
			if (  ok )
			{
				CommonTree tree = parserResult.Tree as CommonTree;

				CommonTreeNodeStream nodes = new CommonTreeNodeStream( tree );
				nodes.TokenStream = tokens;
				Ada95Walker walker = new Ada95Walker( nodes );

				using ( StreamReader templates = new StreamReader( new MemoryStream( Properties.Resources.CSharp ) ) )
				{
					walker.TemplateLib = new StringTemplateGroup( templates );
				}
				Ada95Walker.program_return walkerResult = walker.program();

				ok = ( walker.NumberOfSyntaxErrors == 0 );
				if ( ok )
				{
					text = walkerResult.Template.ToString();
				}
				else if ( output != null )
				{
					walker.Errors.Insert( 0, "Errors encountered during walking AST:\n" );
					output = walker.Errors.ToString();
				}
			}
			else if ( output != null )
			{
				parser.Errors.Insert( 0, "Errors encountered during parsing:\n" );
				output = parser.Errors.ToString();
			}

			return ( ok );
		}

		public Exception Run()
		{
			Debug.Assert( __results != null && !__results.Errors.HasErrors );

			System.Type type = __results.CompiledAssembly.GetType( "Ada95.Program" );
			Debug.Assert( type != null );

			BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.InvokeMethod;
			MethodInfo method = type.GetMethod( "Main", flags );
			Debug.Assert( method != null );

			Exception exception = null;
			try
			{
				method.Invoke( type, new object[] { null } );
			}
			catch ( Exception e )
			{
				exception = e.InnerException;
			}

			return ( exception );
		}
	}
}
