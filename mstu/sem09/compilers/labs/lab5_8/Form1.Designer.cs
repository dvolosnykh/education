﻿namespace lab5_8
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( Form1 ) );
			this.editorTextBox = new System.Windows.Forms.RichTextBox();
			this.menuMain = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.buildToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.buildProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.runProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.outputTextBox = new System.Windows.Forms.RichTextBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.toolStripLine = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripColumn = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
			this.menuMain.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.statusStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// editorTextBox
			// 
			this.editorTextBox.AcceptsTab = true;
			this.editorTextBox.DetectUrls = false;
			this.editorTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.editorTextBox.Font = new System.Drawing.Font( "Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.editorTextBox.Location = new System.Drawing.Point( 3, 3 );
			this.editorTextBox.Name = "editorTextBox";
			this.editorTextBox.Size = new System.Drawing.Size( 808, 318 );
			this.editorTextBox.TabIndex = 0;
			this.editorTextBox.Text = resources.GetString( "editorTextBox.Text" );
			this.editorTextBox.SelectionChanged += new System.EventHandler( this.editorTextBox_SelectionChanged );
			this.editorTextBox.TextChanged += new System.EventHandler( this.editorTextBox_TextChanged );
			// 
			// menuMain
			// 
			this.menuMain.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.buildToolStripMenuItem} );
			this.menuMain.Location = new System.Drawing.Point( 0, 0 );
			this.menuMain.Name = "menuMain";
			this.menuMain.Size = new System.Drawing.Size( 808, 24 );
			this.menuMain.TabIndex = 2;
			this.menuMain.Text = "menuMain";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripSeparator1,
            this.quitToolStripMenuItem} );
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size( 37, 20 );
			this.fileToolStripMenuItem.Text = "File";
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.ShortcutKeys = ( ( System.Windows.Forms.Keys )( ( System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N ) ) );
			this.newToolStripMenuItem.Size = new System.Drawing.Size( 146, 22 );
			this.newToolStripMenuItem.Text = "&New";
			this.newToolStripMenuItem.Click += new System.EventHandler( this.newToolStripMenuItem_Click );
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.ShortcutKeys = ( ( System.Windows.Forms.Keys )( ( System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O ) ) );
			this.openToolStripMenuItem.Size = new System.Drawing.Size( 146, 22 );
			this.openToolStripMenuItem.Text = "&Open";
			this.openToolStripMenuItem.Click += new System.EventHandler( this.openToolStripMenuItem_Click );
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.ShortcutKeys = ( ( System.Windows.Forms.Keys )( ( System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S ) ) );
			this.saveToolStripMenuItem.Size = new System.Drawing.Size( 146, 22 );
			this.saveToolStripMenuItem.Text = "&Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler( this.saveToolStripMenuItem_Click );
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size( 143, 6 );
			// 
			// quitToolStripMenuItem
			// 
			this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
			this.quitToolStripMenuItem.ShortcutKeys = ( ( System.Windows.Forms.Keys )( ( System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q ) ) );
			this.quitToolStripMenuItem.Size = new System.Drawing.Size( 146, 22 );
			this.quitToolStripMenuItem.Text = "&Quit";
			this.quitToolStripMenuItem.Click += new System.EventHandler( this.quitToolStripMenuItem_Click );
			// 
			// buildToolStripMenuItem
			// 
			this.buildToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.buildProgramToolStripMenuItem,
            this.runProgramToolStripMenuItem} );
			this.buildToolStripMenuItem.Name = "buildToolStripMenuItem";
			this.buildToolStripMenuItem.Size = new System.Drawing.Size( 46, 20 );
			this.buildToolStripMenuItem.Text = "Build";
			// 
			// buildProgramToolStripMenuItem
			// 
			this.buildProgramToolStripMenuItem.Name = "buildProgramToolStripMenuItem";
			this.buildProgramToolStripMenuItem.ShortcutKeys = ( ( System.Windows.Forms.Keys )( ( System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F6 ) ) );
			this.buildProgramToolStripMenuItem.Size = new System.Drawing.Size( 196, 22 );
			this.buildProgramToolStripMenuItem.Text = "Build program";
			this.buildProgramToolStripMenuItem.Click += new System.EventHandler( this.buildProgramToolStripMenuItem_Click );
			// 
			// runProgramToolStripMenuItem
			// 
			this.runProgramToolStripMenuItem.Name = "runProgramToolStripMenuItem";
			this.runProgramToolStripMenuItem.ShortcutKeys = ( ( System.Windows.Forms.Keys )( ( System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5 ) ) );
			this.runProgramToolStripMenuItem.Size = new System.Drawing.Size( 196, 22 );
			this.runProgramToolStripMenuItem.Text = "Run program";
			this.runProgramToolStripMenuItem.Click += new System.EventHandler( this.runProgramToolStripMenuItem_Click );
			// 
			// openFileDialog
			// 
			this.openFileDialog.FileName = "test.adb";
			this.openFileDialog.Filter = "Ada files (*.adb)|*.adb";
			this.openFileDialog.InitialDirectory = "..\\\\..\\\\";
			this.openFileDialog.Title = "Select Ada file";
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.Filter = "Ada files (*.adb)|*.adb";
			this.saveFileDialog.Title = "Specify filename where to save";
			// 
			// outputTextBox
			// 
			this.outputTextBox.BackColor = System.Drawing.SystemColors.Info;
			this.outputTextBox.DetectUrls = false;
			this.outputTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.outputTextBox.Font = new System.Drawing.Font( "Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( ( byte )( 204 ) ) );
			this.outputTextBox.Location = new System.Drawing.Point( 3, 327 );
			this.outputTextBox.Name = "outputTextBox";
			this.outputTextBox.ReadOnly = true;
			this.outputTextBox.Size = new System.Drawing.Size( 808, 129 );
			this.outputTextBox.TabIndex = 3;
			this.outputTextBox.TabStop = false;
			this.outputTextBox.Text = "";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.AutoSize = true;
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle() );
			this.tableLayoutPanel1.Controls.Add( this.editorTextBox, 0, 0 );
			this.tableLayoutPanel1.Controls.Add( this.outputTextBox, 0, 1 );
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point( 0, 24 );
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 70.58823F ) );
			this.tableLayoutPanel1.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 29.41176F ) );
			this.tableLayoutPanel1.Size = new System.Drawing.Size( 808, 459 );
			this.tableLayoutPanel1.TabIndex = 4;
			// 
			// statusStrip
			// 
			this.statusStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripLine,
            this.toolStripStatusLabel2,
            this.toolStripColumn} );
			this.statusStrip.Location = new System.Drawing.Point( 0, 461 );
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size( 808, 22 );
			this.statusStrip.TabIndex = 5;
			this.statusStrip.Text = "statusStrip";
			// 
			// toolStripLine
			// 
			this.toolStripLine.Name = "toolStripLine";
			this.toolStripLine.Size = new System.Drawing.Size( 0, 17 );
			// 
			// toolStripColumn
			// 
			this.toolStripColumn.Name = "toolStripColumn";
			this.toolStripColumn.Size = new System.Drawing.Size( 0, 17 );
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size( 32, 17 );
			this.toolStripStatusLabel1.Text = "Line:";
			// 
			// toolStripStatusLabel2
			// 
			this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
			this.toolStripStatusLabel2.Size = new System.Drawing.Size( 53, 17 );
			this.toolStripStatusLabel2.Text = "Column:";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 808, 483 );
			this.Controls.Add( this.statusStrip );
			this.Controls.Add( this.tableLayoutPanel1 );
			this.Controls.Add( this.menuMain );
			this.MainMenuStrip = this.menuMain;
			this.Name = "Form1";
			this.Text = "Minor Ada95 compiler";
			this.menuMain.ResumeLayout( false );
			this.menuMain.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout( false );
			this.statusStrip.ResumeLayout( false );
			this.statusStrip.PerformLayout();
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.RichTextBox editorTextBox;
		private System.Windows.Forms.MenuStrip menuMain;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem buildToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem buildProgramToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem runProgramToolStripMenuItem;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
		private System.Windows.Forms.RichTextBox outputTextBox;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripLine;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
		private System.Windows.Forms.ToolStripStatusLabel toolStripColumn;
	}
}

