tree grammar Ada95Walker;

options
{
	language = CSharp2;
	tokenVocab = Ada95;
	ASTLabelType = CommonTree;
	output = template;
}

scope SymbolTable
{
	SortedDictionary< string, string > Types;
	SortedDictionary< string, List< Function > > Functions;
	SortedDictionary< string, List< Procedure > > Procedures;
	SortedDictionary< string, Object > Objects;
}
scope Symbols
{
	SortedSet< string > IDs;
}
scope ActualParameters
{
	List< string > Types;
	List< string > Values;
}

@namespace { Ada95 }
@header
{
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Text;
}
@members
{
	// symbols related stuff.
	private bool IsType( string id )
	{
		bool found = false;
		for ( int i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Types.ContainsKey( id ) );

		return ( found );
	}
	
	private string GetType( string adaType )
	{
		adaType = adaType.ToLower();
		
		bool found = false;
		int i;
		for ( i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Types.ContainsKey( adaType ) );

		return ( found ? $SymbolTable[ i + 1 ]::Types[ adaType ] : null );
	}
	
	private bool IsFunction( string id )
	{
		bool found = false;
		for ( int i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Functions.ContainsKey( id ) );

		return ( found );
	}
	
	private List< Function > GetFunction( string id )
	{	
		bool found = false;
		int i;
		for ( i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Functions.ContainsKey( id ) );

		return ( found ? $SymbolTable[ i + 1 ]::Functions[ id ] : null );
	}
	
	private bool IsProcedure( string id )
	{
		bool found = false;
		for ( int i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Procedures.ContainsKey( id ) );

		return ( found );
	}
	
	private List< Procedure > GetProcedure( string id )
	{	
		bool found = false;
		int i;
		for ( i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Procedures.ContainsKey( id ) );

		return ( found ? $SymbolTable[ i + 1 ]::Procedures[ id ] : null );
	}
	
	private bool IsObject( string id )
	{
		bool found = false;
		for ( int i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Objects.ContainsKey( id ) );
			
		return ( found );
	}
	
	private Object GetObject( string id )
	{	
		bool found = false;
		int i;
		for ( i = $SymbolTable.Count - 1; !found && i >= 0; --i )
			found = ( $SymbolTable[ i ]::Objects.ContainsKey( id ) );

		return ( found ? $SymbolTable[ i + 1 ]::Objects[ id ] : null );
	}
	
	private bool IsString( string id )
	{
		return ( true );
	}
	
	private Function FindBest( List< Function > functions, List< string > argTypes )
	{
		bool found = false;
		int i;
		for ( i = 0; !found && i < functions.Count; ++i )
			found = __ArgTypesMatch( functions[ i ].Parameters, argTypes );
		
		return ( found ? functions[ i - 1 ] : null );
	}
	
	private Procedure FindBest( List< Procedure > procedures, List< string > argTypes )
	{
		bool found = false;
		int i;
		for ( i = 0; !found && i < procedures.Count; ++i )
			found = __ArgTypesMatch( procedures[ i ].Parameters, argTypes );
		
		return ( found ? procedures[ i - 1 ] : null );
	}
	
	private bool __ArgTypesMatch( List< Parameter > parameters, List< string > argTypes )
	{
		bool match = ( parameters.Count == argTypes.Count );
		for ( int i = 0; match && i < parameters.Count; ++i )
			match = parameters[ i ].ValueType == argTypes[ i ];
			
		return ( match );
	}
		
	// errors related stuff.
	public StringBuilder Errors = new StringBuilder();	
	
	public override void EmitErrorMessage( string message )
	{
		Errors.AppendLine( message );
	}
	
	public override string GetErrorMessage( RecognitionException re, string[] tokenNames )
	{
		return ( re is CompileException ? ( re as CompileException ).Message : base.GetErrorMessage( re, tokenNames ) );
	}
}


public program
	scope SymbolTable;
	@init
	{
		$SymbolTable::Types = new SortedDictionary< string, string >();
		$SymbolTable::Functions = new SortedDictionary< string, List< Function > >();
		$SymbolTable::Procedures = new SortedDictionary< string, List< Procedure > >();
		$SymbolTable::Objects = new SortedDictionary< string, Object >();
		
		// predefined types.
		$SymbolTable::Types.Add( "boolean", "bool" );
		$SymbolTable::Types.Add( "integer", "int" );
		$SymbolTable::Types.Add( "mod", "uint" );
		$SymbolTable::Types.Add( "string", "string" );
		
		// predefined functions and procedures.
		
		// Ada.Text_IO
		$SymbolTable::Procedures.Add
		( "Put", new List< Procedure >
			( new Procedure[]
				{
					new Procedure( "Put", new List< Parameter >( new Parameter[] { new Parameter( "", "int", "value", "" ) } ) ),
					new Procedure( "Put", new List< Parameter >( new Parameter[] { new Parameter( "", "uint", "value", "" ) } ) ),
					new Procedure( "Put", new List< Parameter >( new Parameter[] { new Parameter( "", "bool", "flag", "" ) } ) ),
					new Procedure( "Put", new List< Parameter >( new Parameter[] { new Parameter( "", "string", "str", "" ) } ) )
				}
			)
		);
		$SymbolTable::Procedures.Add
		( "Get", new List< Procedure >
			( new Procedure[]
				{
					new Procedure( "Get", new List< Parameter >( new Parameter[] { new Parameter( "out", "int", "value", "" ) } ) ),
					new Procedure( "Get", new List< Parameter >( new Parameter[] { new Parameter( "out", "uint", "value", "" ) } ) ),
					new Procedure( "Get", new List< Parameter >( new Parameter[] { new Parameter( "out", "bool", "flag", "" ) } ) ),
					new Procedure( "Get", new List< Parameter >( new Parameter[] { new Parameter( "out", "string", "str", "" ) } ) )
				}
			)
		);
		$SymbolTable::Procedures.Add( "New_Line", new List< Procedure >( new Procedure[] { new Procedure( "New_Line", new List< Parameter >() ) } ) );
	}
	:	d = declarative_part -> Program( declarativePart = { $d.st } ) ;

basic_declaration
	:
		typeDecl = type_declaration				-> { $typeDecl.st } |
		//subtype_declaration |
		objDecl = object_declaration			-> { $objDecl.st } |
		numDecl = number_declaration			-> { $numDecl.st } |
		subProgDecl = subprogram_declaration	-> { $subProgDecl.st }
	;
	
defining_identifier
	:	IDENTIFIER ;

type_declaration
	:	full_type_declaration ;

full_type_declaration
	scope
	{
		string typeName;
	}
	:
		^( TYPE id = defining_identifier { $full_type_declaration::typeName = $id.text; } type_definition )
	;

type_definition
	:
		enumeration_type_definition |
		integer_type_definition |
		//real_type_definition |
		array_type_definition |
		record_type_definition |
		//access_type_definition |
		derived_type_definition
	;

subtype_declaration
	:	^( SUBTYPE defining_identifier subtype_indication ) ;

subtype_mark
	:	IDENTIFIER ;

subtype_indication
	:	^( subtype_mark constraint? ) ;

constraint
	:
		scalar_constraint //|
		//composite_constraint
	;

scalar_constraint
	:
		range_constraint |
		digits_constraint
	;
/*
composite_constraint :
	index_constraint |
	discriminant_constraint ;
*/
number_declaration
	@init
	{
		string type;
	}
	:	^( CONSTANT defining_identifier_list expression[ out type ] ) ;

derived_type_definition
	:	^( NEW subtype_indication record_extension_part? ABSTRACT? ) ;


defining_identifier_list
	:	^( ID_LIST ( id = defining_identifier { $Symbols::IDs.Add( $id.text ); } )+ ) ;

object_declaration
	scope Symbols;
	@init
	{
		$Symbols::IDs = new SortedSet< string >();
		string type;
		string exprType;
	}
	:
		^( OBJECT defining_identifier_list subtype = subtype_indication ( e = expression[ out exprType ] )? )
			{
				type = GetType( $subtype.text );
				foreach ( string id in $Symbols::IDs )
					$SymbolTable::Objects.Add( id, new Object( id, type ) );
			}
			-> Object( variables = { $Symbols::IDs }, type = { type }, initExpr = { $e.st } ) |
		^( ARRAY_OBJECT defining_identifier_list arrayType = array_type_definition ( e = expression[ out exprType ] )? )
			-> { $arrayType.st }
			//-> ArrayObject( variables = { $Symbols::IDs }, type = { $arrayType.st }, initExpr = { $e.st } )
	;


range_constraint
	@init
	{
		Range r;
	}
	:	range[ out r ] ;

range[ out Range r ]
	@init
	{
		r = null;
		string exprType;
	}
	:
		//range_attribute_reference |
		^( RANGE
			a = simple_expression[ out exprType ] { if ( exprType != "int" ) throw new UnexpectedType( exprType, "int", input ); }
			b = simple_expression[ out exprType ] { if ( exprType != "int" ) throw new UnexpectedType( exprType, "int", input ); }
		) { r = new Range( $a.st.ToString(), $b.st.ToString() ); }
	;
enumeration_type_definition
	:	^( ENUMERATION enumeration_literal_specification+ ) ;

enumeration_literal_specification
	:
		defining_identifier |
		defining_character_literal
	;

defining_character_literal
	:	CHARACTER_LITERAL ;

integer_type_definition
	:
		signed_integer_type_definition |
		modular_type_definition
	;

signed_integer_type_definition
	@init
	{
		string temp;
	}
	:	^( RANGE a = simple_expression[ out temp ] b = simple_expression[ out temp ] ) ;//-> SignedInteger( typeName = { $full_type_declaration::typeName }, min = { $a.st }, max = { $b.st } ) ;

modular_type_definition
	@init
	{
		string exprType;
	}
	:	^( MOD expression[ out exprType ] ) ;

real_type_definition
	:
		floating_point_definition |
		fixed_point_definition
	;

floating_point_definition
	@init
	{
		string exprType;
	}
	:	^( DIGITS expression[ out exprType ] real_range_specification? ) ;

real_range_specification
	@init
	{
		string temp;
	}
	:	^( RANGE simple_expression[ out temp ] simple_expression[ out temp ] ) ;

fixed_point_definition
	:
		ordinary_fixed_point_definition |
		decimal_fixed_point_definition
	;

ordinary_fixed_point_definition
	@init
	{
		string exprType;
	}
	:	^( ORDINARY_FIXED expression[ out exprType ] real_range_specification ) ;

decimal_fixed_point_definition
	@init
	{
		string temp;
	}
	:	^( DECIMAL_FIXED expression[ out temp ] expression[ out temp ] real_range_specification? ) ;

digits_constraint
	@init
	{
		string exprType;
	}
	:	^( DIGITS expression[ out exprType ] range_constraint? ) ;	// static_ expression

array_type_definition
	:
		unconstrained_array_definition |
		c = constrained_array_definition	-> { $c.st }
	;

unconstrained_array_definition
	:	^( UNCONSTRAINED_ARRAY index_subtype_definition+ OF component_definition ) ;

index_subtype_definition
	:	subtype_mark RANGE BOX ;


component_definition
	:	ALIASED? subtype_indication ;

//index_constraint :  '(' discrete_range ( ',' discrete_range )* ')' ;

discrete_subtype_definition[ out Range dimension ]
	@init
	{
		dimension = null;
	}
	:
		//subtype_indication |
		range[ out dimension ]
	;

constrained_array_definition
	@init
	{
		List< Range > dimensions = new List< Range >();
		Range dimension;
	}
	:
		^( CONSTRAINED_ARRAY ( discrete_subtype_definition[ out dimension ] { dimensions.Add( dimension ); } )+ OF componentType = component_definition )
		{
			foreach ( string id in $Symbols::IDs )
				$SymbolTable::Objects.Add( id, new ArrayObject( id, dimensions, GetType( $componentType.text ) ) );
		}
		-> ConstrainedArray( variables = { $Symbols::IDs }, type = { GetType( $componentType.text ) }, dimensions = { dimensions } )
	;
	

discrete_range
	@init
	{
		Range r;
	}
	:
		subtype_indication |	// discrete_ subtype_indication
		range[ out r ]
	;

known_discriminant_part
	:	discriminant_specification+ ;

discriminant_specification
	scope Symbols;
	@init
	{
		$Symbols::IDs = new SortedSet< string >();
		string defExprType;
	}
	:
		^( DISCRIMINANT_SPECIFICATION defining_identifier_list subtype_mark default_expression[ out defExprType ]? )
		{
			foreach ( string id in $Symbols::IDs )
				$SymbolTable::Objects.Add( id, null );	// not objects! inside type declaration;
		}
	;

default_expression[ out string type ]
	@init
	{
		type = string.Empty;
	}
	:	expression[ out type ] ;
/*
discriminant_constraint :	'(' discriminant_association ( ',' discriminant_association )* ')' ;

discriminant_association :	( selector_name ( '|' selector_name )* '=>' )? expression ;	// discriminant_ selector_name
*/
record_type_definition
	:	^( RECORD ( ABSTRACT? TAGGED )? LIMITED? record_definition ) ;

record_definition
	:	component_list? ;

component_list
	:	^( COMPONENT_LIST component_item* variant_part? ) ;

component_item
	:	component_declaration ;

component_declaration
	scope Symbols;
	@init
	{
		$Symbols::IDs = new SortedSet< string >();
		string defExprType;
	}
	:
		^( COMPONENT defining_identifier_list component_definition default_expression[ out defExprType ]? )
		{
			foreach ( string id in $Symbols::IDs )
				$SymbolTable::Objects.Add( id, null );
		}
	;

variant_part
	:	^( CASE direct_name variant* other_variant? ) ;

variant
	:	^( WHEN discrete_choice_list component_list ) ;
	
other_variant
	:	^( OTHERS component_list ) ;

discrete_choice_list
	:	( c += discrete_choice )+ -> DiscreteChoiceList( choices = { $c } ) ;

discrete_choice
	@init
	{
		string exprType;
	}
	:
		e = expression[ out exprType ]
		{ if ( exprType != "int" ) throw new UnexpectedType( exprType, "int", input ); }
			-> DiscreteChoice( expr = { $e.st } ) |
		discrete_range
	;

record_extension_part
	:	^( WITH record_definition ) ;
/*
access_type_definition :
	access_to_object_definition |
	access_to_subprogram_definition ;

access_to_object_definition :	'access' general_access_modifier? subtype_indication ;

general_access_modifier :
	'all' |
	'constant' ;

access_to_subprogram_definition :
	'access' 'protected'? ( 'procedure' parameter_profile | 'function' parameter_and_result_profile ) ;
*/
access_definition
	:	ACCESS subtype_mark ;

declarative_part
	:	^( DECLARE ( items += declarative_item )* ) -> DeclarativePart( declarations = { $items } ) ;

declarative_item
	:
		^( ITEM i = basic_declarative_item )	-> { $i.st } |
		^( BODY b = body )						-> { $b.st }
	;

basic_declarative_item
	:	d = basic_declaration -> { $d.st } ;

body
	:	p = proper_body -> { $p.st } ;

proper_body
	:	s = subprogram_body -> { $s.st } ;

name[ out string type ]
	@init
	{
		type = string.Empty;
	}
	:
		f = function_call[ out type ]		-> { $f.st } |
		type_conversion |
		slice |
		i = indexed_component[ out type ]	-> { $i.st } |
		explicit_dereference |
		selected_component |
		^( OBJECT n = direct_name )	{ type = GetObject( $n.text ).ValueType; } -> Name( name = { $n.text } ) |
		attribute_reference |
	   	CHARACTER_LITERAL
	;

direct_name
	:
		IDENTIFIER |
		operator_symbol
	;


explicit_dereference
	:	^( EXPLICIT_DEREFERENCE direct_name ) ;
	
indexed_component[ out string type ]
	@init
	{
		string exprType;
		type = string.Empty;
		ArrayObject array;
	}
	:
		^( INDEXED_COMPONENT n = direct_name
		{
			array = GetObject( $n.text ) as ArrayObject;
			if ( array == null )
				throw new UnknownIndexedComponent( $n.text, input );
			type = array.ComponentType;
		}
		( e += expression[ out exprType ] )+ ) -> IndexedComponent( array = { array }, expressions = { $e } ) ;

slice
	:	^( SLICE direct_name discrete_range ) ;

selected_component
	:	^( SELECTED_COMPONENT direct_name selector_name ) ;

selector_name
	:
		IDENTIFIER |
		CHARACTER_LITERAL |
		operator_symbol
	;

attribute_reference
	:	^( ATTRIBUTE_REFERENCE direct_name attribute_designator ) ;

attribute_designator
	@init
	{
		string exprType;
	}
	:	IDENTIFIER expression[ out exprType ]? ;
//range_attribute_reference :	name APOSTROPHE range_attribute_designator ;

//range_attribute_designator :	'Range' ( '(' expression ')' )? ;
/*
aggregate :
	record_aggregate |
	extension_aggregate |
	array_aggregate ;

record_aggregate : '(' record_component_association_list ')' ;

record_component_association_list :
	record_component_association ( ',' record_component_association )* |
	NULL RECORD ;

record_component_association :	( component_choice_list '=>' )? expression ;

component_choice_list :
	selector_name ( '|' selector_name )* |	// component_ selector_name
	OTHERS ;

extension_aggregate :	'(' ancestor_part 'with' record_component_association_list ')' ;

ancestor_part :
	expression |
	subtype_mark ;

array_aggregate :
	positional_array_aggregate |
	named_array_aggregate ;

positional_array_aggregate :
	'(' expression ( ',' expression )+ ')' |
	'(' expression ( ',' expression )* ',' 'others' '=>' expression ')' ;

named_array_aggregate :	'(' array_component_association ( ',' array_component_association )* ')' ;

array_component_association :	discrete_choice_list '=>' expression ;
*/
expression[ out string type ]
	@init
	{
		type = string.Empty;
		string exprType;
	}
	:
		^( EXPRESSION
			(
				THEN
				(
					rList += relation[ out exprType ]
					{ if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); }
				)+ { type = "bool"; }	-> AndThen( relations = { $rList } ) |
				AND
				(
					rList += relation[ out exprType ]
					{ if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); }
				)+ { type = "bool"; }	-> And( relations = { $rList } ) |
				ELSE
				(
					rList += relation[ out exprType ]
					{ if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); }
				)+ { type = "bool"; }	-> OrElse( relations = { $rList } ) |
				OR
				(
					rList += relation[ out exprType ]
					{ if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); }
				)+ { type = "bool"; }	-> Or( relations = { $rList } ) |
				XOR
				(
					rList += relation[ out exprType ]
					{ if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); }
				)+ { type = "bool"; }	-> Xor( relations = { $rList } ) |
				r = relation[ out type ] -> { $r.st }
			)
		)
	;
	
relation[ out string type ]
	@init
	{
		string leftType;
		string rightType;
		type = string.Empty;
		Range range_;
	}
	:
		^( EQ lhs = simple_expression[ out leftType ] rhs = simple_expression[ out rightType ] )
		{
			if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
			type = "bool";
		}	-> Equal( left = { $lhs.st }, right = { $rhs.st } ) |
		^( NEQ lhs = simple_expression[ out leftType ] rhs = simple_expression[ out rightType ] )
		{
			if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
			type = "bool";
		}	-> NotEqual( left = { $lhs.st }, right = { $rhs.st } ) |
		^( LESS lhs = simple_expression[ out leftType ] rhs = simple_expression[ out rightType ] )
		{
			if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
			type = "bool";
		}	-> Less( left = { $lhs.st }, right = { $rhs.st } ) |
		^( LEQ lhs = simple_expression[ out leftType ] rhs = simple_expression[ out rightType ] )
		{
			if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
			type = "bool";
		}	-> LessOrEqual( left = { $lhs.st }, right = { $rhs.st } ) |
		^( GREATER lhs = simple_expression[ out leftType ] rhs = simple_expression[ out rightType ] )
		{
			if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
			type = "bool";
		}	-> Greater( left = { $lhs.st }, right = { $rhs.st } ) |
		^( GEQ lhs = simple_expression[ out leftType ] rhs = simple_expression[ out rightType ] )
		{
			if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input );
			type = "bool";
		}	-> GreaterOrEqual( left = { $lhs.st }, right = { $rhs.st } ) |
		^( IN_RANGE e = simple_expression[ out leftType ] r = range[ out range_ ] ( n = NOT )? )
		{
			if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input );
			type = "bool";
		}	-> InRange( expr = { $e.st }, range = { $r.st }, not = { $n.text } ) |
		^( IN_SUBTYPE e = simple_expression[ out leftType ] s = subtype_mark ( n = NOT )? )
		{
			if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input );
			type = "bool";
		}	-> InSubtype( expr = { $e.st }, subtype = { $s.st }, not = { $n.text } ) |
		e = simple_expression[ out type ] -> { $e.st }
	;

simple_expression[ out string type ]
	@init
	{
		string leftType;
		string rightType;
		type = string.Empty;
	}
	:
		^( PLUS
			lhs = simple_expression[ out leftType ]	{ if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); }
			rhs = term[ out rightType ] { if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); }
		) { type = "int"; }	->	BinaryPlus( left = { $lhs.st }, right = { $rhs.st } ) |
		^( MINUS
			lhs = simple_expression[ out leftType ]	{ if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); }
			rhs = term[ out rightType ] { if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); }
		) { type = "int"; }	->	BinaryMinus( left = { $lhs.st }, right = { $rhs.st } ) |
		^( CONCAT
			lhs = simple_expression[ out leftType ]	{ if ( leftType != "string" && leftType != "array" ) throw new UnexpectedType( leftType, "string/array", input ); }
			rhs = term[ out rightType ] { if ( rightType != leftType ) throw new UnexpectedType( rightType, leftType, input ); }
		) { type = "int"; }	->	{ leftType == "string" }?	StringConcat( left = { $lhs.st }, right = { $rhs.st } )
							->								ArrayConcat( left = { $lhs.st }, right = { $rhs.st } ) |
		PLUS t = term[ out type ]
		{
			if ( type != "int" ) throw new UnexpectedType( type, "int", input );
			type = "int";
		}	->	UnaryPlus( term = { $t.st } ) |
		MINUS t = term[ out type ]
		{
			if ( type != "int" ) throw new UnexpectedType( type, "int", input );
			type = "int";
		}	->	UnaryMinus( term = { $t.st } ) |
		t = term[ out type ]	->	{ $t.st }
	;

term[ out string type ]
	@init
	{
		string leftType;
		string rightType;
		type = string.Empty;
	}
	:
		^( MULT
			lhs = term[ out leftType ] { if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); }
			rhs = factor[ out rightType ] { if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); }
		) { type = "int"; }	-> Multiply( left = { $lhs.st }, right = { $rhs.st } ) |
		^( DIV
			lhs = term[ out leftType ] { if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); }
			rhs = factor[ out rightType ] { if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); }
		) { type = "int"; }	-> Divide( left = { $lhs.st }, right = { $rhs.st } ) |
		^( MOD
			lhs = term[ out leftType ] { if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); }
			rhs = factor[ out rightType ] { if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); }
		) { type = "int"; }	-> Modulo( left = { $lhs.st }, right = { $rhs.st } ) |
		^( REM
			lhs = term[ out leftType ] { if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); }
			rhs = factor[ out rightType ] { if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); }
		) { type = "int"; }	-> Reminder( left = { $lhs.st }, right = { $rhs.st } ) |
		f = factor[ out type ]	-> { $f.st }
	;

factor[ out string type ]
	@init
	{
		string leftType;
		string rightType;
		type = string.Empty;
	}
	:
		^( POW
			b = primary[ out leftType ] { if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); }
			i = primary[ out rightType ] { if ( rightType != "int" ) throw new UnexpectedType( rightType, "int", input ); }
		) { type = "int"; }	-> Pow( base = { $b.st }, index = { $i.st } ) |
		^( ABS
			v = primary[ out leftType ] { if ( leftType != "int" ) throw new UnexpectedType( leftType, "int", input ); }
		) { type = "int"; }	-> Abs( value = { $v.st } ) |
		^( NOT
			v = primary[ out leftType ] { if ( leftType != "bool" ) throw new UnexpectedType( leftType, "bool", input ); }
		) { type = "bool"; }	-> Not( value = { $v.st } ) |
		p = primary[ out type ]	-> { $p.st }
	;

primary[ out string type ]
	@init
	{
		type = string.Empty;
	}
	:
		num = NUMERIC_LITERAL { type = "int"; }		-> NumericLiteral( number = { $num.text.Replace( "_", "" ) } ) |
		NULL |
		str = STRING_LITERAL { type = "string"; }	-> StringLiteral( string = { $str.text } ) |
		//aggregate |
		n = name[ out type ]						-> { $n.st } |
		q = qualified_expression					-> { $q.st } |
	//	allocator |
		e = expression[ out type ]					-> Subexpression( expr = { $e.st } )
	;

type_conversion
	@init
	{
		string exprType;
	}
	:	^( TYPE direct_name expression[ out exprType ] ) ;

qualified_expression
	@init
	{
		string exprType;
	}
	:
		^( QUALIFIED_BY_EXPRESSION subtype_mark expression[ out exprType ] ) //|
		//^( QUALIFIED_BY_AGREGATE subtype_mark expression )
	;

//allocator : 'new' ( subtype_indication | qualified_expression ) ;

sequence_of_statements
	:	( s += statement )+ -> SequenceOfStatements( statements = { $s } ) ;

statement
	:
		^( SIMPLE_STATEMENT label* s = simple_statement )		-> { $s.st } |
		^( COMPOUND_STATEMENT label* c = compound_statement )	-> { $c.st }
	;

simple_statement
	:
		null_statement |
		a = assignment_statement		-> { $a.st } |
		e = exit_statement				-> { $e.st } |
		g = goto_statement				-> { $g.st } |
		p = procedure_call_statement	-> { $p.st } |
		r = return_statement			-> { $r.st }
	;


null_statement
	:	;
	
compound_statement
	:
		i = if_statement	-> { $i.st } |
		c = case_statement	-> { $c.st } |
		l = loop_statement	-> { $l.st } |
		b = block_statement	-> { $b.st }
	;

label
	:	^( LABEL statement_identifier ) ;

statement_identifier
	:	IDENTIFIER ;

assignment_statement
	@init
	{
		string exprType;
		string varType;
	}
	:
		^( ASSIGN var = name[ out varType ] e = expression[ out exprType ] )
		{ if ( exprType != varType ) throw new UnexpectedType( exprType, varType, input ); }
			-> Assign( variable = { $var.st }, expr = { $e.st } )
	;

if_statement
	:	^( IF ( c += condition s += sequence_of_statements )+ ( ELSE elSeq = sequence_of_statements )? ) -> If( conditions = { $c }, sequences = { $s }, elseSequence = { $elSeq.st } ) ;

condition
	@init
	{
		string exprType;
	}
	:
		e = expression[ out exprType ]
		{ if ( exprType != "bool" ) throw new UnexpectedType( exprType, "bool", input ); }
			-> { $e.st }
	;

case_statement
	@init
	{
		string exprType;
	}
	:
		^( CASE e = expression[ out exprType ]
		{ if ( exprType != "int" ) throw new UnexpectedType( exprType, "int", input ); }
			( c += case_statement_alternative )* ( o = other_case_statement_alternative )? )
			-> Case( expr = { $e.st }, alternatives = { $c }, other = { $o.st } )
	;

case_statement_alternative
	:	^( WHEN d = discrete_choice_list s = sequence_of_statements ) -> CaseAlternative( choices = { $d.st }, statements = { $s.st } ) ;
	
other_case_statement_alternative
	:	^( OTHERS s = sequence_of_statements ) -> CaseAlternative( statements = { $s.st } ) ;

loop_statement
	scope SymbolTable;
	@init
	{
		$SymbolTable::Types = new SortedDictionary< string, string >();
		$SymbolTable::Functions = new SortedDictionary< string, List< Function > >();
		$SymbolTable::Procedures = new SortedDictionary< string, List< Procedure > >();
		$SymbolTable::Objects = new SortedDictionary< string, Object >();
	}
	:
		^( LOOP statement_identifier? ( scheme = iteration_scheme )? s = handled_sequence_of_statements statement_identifier? )
			-> Loop( scheme = { $scheme.st }, statements = { $s.st } )
	;

iteration_scheme
	:
		^( WHILE c = condition )					-> While( condition = { $c.st } ) |
		^( FOR l = loop_parameter_specification )	-> { $l.st }
	;

loop_parameter_specification
	@init
	{
		Range range;
	}
	:
		^( IN i = defining_identifier ( r = REVERSE )? discrete_subtype_definition[ out range ] )
		{ $SymbolTable::Objects.Add( $i.text, new Object( $i.text, "int" ) ); }
			-> For( index = { $i.text }, range = { range }, reverse = { $r.text } )
	;

block_statement
	scope SymbolTable;
	@init
	{
		$SymbolTable::Types = new SortedDictionary< string, string >();
		$SymbolTable::Functions = new SortedDictionary< string, List< Function > >();
		$SymbolTable::Procedures = new SortedDictionary< string, List< Procedure > >();
		$SymbolTable::Objects = new SortedDictionary< string, Object >();
	}
	:	^( BLOCK statement_identifier? declarative_part? sequence_of_statements statement_identifier? ) ;

exit_statement
	:	^( EXIT statement_identifier? ( c = condition )? ) -> Exit( condition = { $c.st } );

goto_statement
	:	^( GOTO statement_identifier ) ;

subprogram_declaration
	:	subprogram_specification[ false ] ABSTRACT? ;

subprogram_specification[ bool withBody ]
	scope
	{
		List< Parameter > parameters;
		string returnType;
	}
	@init
	{
		$subprogram_specification::parameters = new List< Parameter >();
	}
	:
		^( PROCEDURE procID = defining_program_unit_name parameter_profile[ withBody ] )
			{
				int prev = $SymbolTable.Count - ( withBody ? 2 : 1 );
				if ( !$SymbolTable[ prev ]::Procedures.ContainsKey( $procID.text ) )
					$SymbolTable[ prev ]::Procedures.Add( $procID.text, new List< Procedure >() );
				Procedure procedure = new Procedure( $procID.text, $subprogram_specification::parameters );
				$SymbolTable[ prev ]::Procedures[ $procID.text ].Add( procedure );
				if ( withBody )
					$subprogram_body::routine = procedure;
			} |
		^( FUNCTION funcID = defining_designator parameter_and_result_profile[ withBody ] )
			{
				int prev = $SymbolTable.Count - ( withBody ? 2 : 1 );
				if ( !$SymbolTable[ prev ]::Functions.ContainsKey( $funcID.text ) )
					$SymbolTable[ prev ]::Functions.Add( $funcID.text, new List< Function >() );
				Function function = new Function( $subprogram_specification::returnType, $funcID.text, $subprogram_specification::parameters );
				$SymbolTable[ prev ]::Functions[ $funcID.text ].Add( function );
				if ( withBody )
					$subprogram_body::routine = function;
			}
	;

designator
	:
		IDENTIFIER |
		operator_symbol
	;

defining_designator
	:
		defining_program_unit_name |
		defining_operator_symbol
	;

defining_program_unit_name
	:	defining_identifier ;

operator_symbol
	:	STRING_LITERAL ;

defining_operator_symbol
	:	operator_symbol ;

parameter_profile[ bool withBody ]
	:	( f = formal_part[ withBody ] )? ;

parameter_and_result_profile[ bool withBody ]
	:	( f = formal_part[ withBody ] )? RETURN t = subtype_mark { $subprogram_specification::returnType = GetType( $t.text ); } ;

formal_part[ bool withBody ]
	:	parameter_specification[ withBody ]+ ;

parameter_specification[ bool withBody ]
	scope Symbols;
	@init
	{
		$Symbols::IDs = new SortedSet< string >();
		string defExprType;
	}
	:
		^( PARAMETER defining_identifier_list m = mode t = subtype_mark ( e = default_expression[ out defExprType ] )? )
		{
			foreach ( string id in $Symbols::IDs )
			{
				string paramType = GetType( $t.text );
				$subprogram_specification::parameters.Add( new Parameter( ( $m.st != null ? $m.st.ToString() : string.Empty ), paramType, id, ( $e.st != null ? $e.st.ToString() : null ) ) );
				if ( withBody )
					$SymbolTable::Objects.Add( id, new Object( id, paramType ) );
			}
		}
	;

mode
	:
		IN |
		REF	-> Mode( mode = { "ref" } ) |
		OUT	-> Mode( mode = { "out" } )
	;


handled_sequence_of_statements
	:	s = sequence_of_statements -> { $s.st } ;
	
subprogram_body
	scope
	{
		object routine;
	}
	scope SymbolTable;
	@init
	{
		$SymbolTable::Types = new SortedDictionary< string, string >();
		$SymbolTable::Functions = new SortedDictionary< string, List< Function > >();
		$SymbolTable::Procedures = new SortedDictionary< string, List< Procedure > >();
		$SymbolTable::Objects = new SortedDictionary< string, Object >();
	}
	:
		subprogram_specification[ true ] d = declarative_part s = handled_sequence_of_statements designator?
			->	{ $subprogram_body::routine is Function }?	SubprogramBody( returnType = { ( $subprogram_body::routine as Function ).ReturnType }, name = { ( $subprogram_body::routine as Function ).Name }, params = { ( $subprogram_body::routine as Function ).Parameters }, declarativePart = { $d.st }, statements = { $s.st } )
			->												SubprogramBody( returnType = { ( $subprogram_body::routine as Procedure ).ReturnType }, name = { ( $subprogram_body::routine as Procedure ).Name }, params = { ( $subprogram_body::routine as Procedure ).Parameters }, declarativePart = { $d.st }, statements = { $s.st } )
	;


procedure_call_statement
	scope ActualParameters;
	@init
	{
		$ActualParameters::Types = new List< string >();
		$ActualParameters::Values = new List< string >();
	
		List< Procedure > procedures;
		Procedure procedure;
		
		string[] modes;
	}
	:
		^( PROCEDURE id = direct_name actual_parameter_part? )
		{
			procedures = GetProcedure( $id.text );
			if ( procedures == null )
				throw new UnknownProcedure( $id.text, input );
			procedure = FindBest( procedures, $ActualParameters::Types );
		}
		-> ProcedureCall( proc = { procedure }, values = { $ActualParameters::Values } )
	;

function_call[ out string type ]
	scope ActualParameters;
	@init
	{
		$ActualParameters::Types = new List< string >();
		$ActualParameters::Values = new List< string >();
				
		List< Function > functions;
		Function function;
		
		type = string.Empty;
	}
	@after
	{
		type = function.ReturnType;
	}
	:
		^( FUNCTION id = direct_name actual_parameter_part? )
		{
			functions = GetFunction( $id.text );
			if ( functions == null )
				throw new UnknownFunction( $id.text, input );
			function = FindBest( functions, $ActualParameters::Types );
		}
		-> FunctionCall( func = { function }, values = { $ActualParameters::Values } )
	;

actual_parameter_part
	:	( p = parameter_association { $ActualParameters::Values.Add( $p.st.ToString() ); } )+ ;

parameter_association
	:	^( PARAMETER p = explicit_actual_parameter ( id = selector_name )? ) -> ParameterAssociation( param = { $p.st }, selector = { $id.st } ) ;

explicit_actual_parameter
	@init
	{
		string argType;
	}
	:	e = expression[ out argType ] { $ActualParameters::Types.Add( argType ); } -> { $e.st } ;

return_statement
	@init
	{
		string exprType;
	}
	:	^( RETURN ( e = expression[ out exprType ] )? ) -> ReturnStatement( expr = { $e.st } ) ;