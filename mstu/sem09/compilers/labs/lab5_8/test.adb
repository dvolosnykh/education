function Sqr( x : Integer ) return Integer is
begin
	if x = 1 then
		x := x * 1;
	elsif ( x = 2 ) then
		x := x * 2;
	elsif ( x = 3 ) then
		x := x * 3;
	else
		case x is
			when 4	=>	x := x * 4;
			when 5	=>	x := x * 5;
			when others	=>	x := x * x;
		end case;
	end if;

	return ( x );
end Sqr;

procedure Entry is
	a : Integer := 3;
	b : Integer := 4;
	v : array ( 1 .. 10 ) of Integer;
	j : Integer := 1;
	sum : Integer := 0;
begin
	Put( "Array contents: " );
	j := 1;
	loop
	exit when j > 10;
		v( j ) := j;
		Put( v( j ) );
		if j < 10 then
			Put( ", " );
		else
			New_Line;
		end if;
		j := j + 1;
	end loop;

	for i in reverse 1 .. 10 loop
		sum := sum + v( i );
	end loop;
	Put( "Sum of array: " );
	Put( sum );
	New_Line;

	Put( "Array contents: " );
	j := 1;
	while j <= 10 loop
		v( j ) := Sqr( v( j ) );
		Put( v( j ) );
		if j < 10 then
			Put( ", " );
		else
			New_Line;
		end if;
		j := j + 1;
	end loop;

	for i in reverse 1 .. 10 loop
		sum := sum + v( i );
	end loop;
	Put( "Sum of array: " );
	Put( sum );
	New_Line;
end Entry;