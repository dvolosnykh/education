// $ANTLR 3.2 Sep 23, 2009 12:02:23 Ada95.g 2009-12-23 07:03:03

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Text;


using System;
using Antlr.Runtime;
using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;

using IDictionary	= System.Collections.IDictionary;
using Hashtable 	= System.Collections.Hashtable;

using Antlr.Runtime.Tree;

namespace  Ada95 
{
public partial class Ada95Parser : Parser
{
    public static readonly string[] tokenNames = new string[] 
	{
        "<invalid>", 
		"<EOR>", 
		"<DOWN>", 
		"<UP>", 
		"ORDINARY_FIXED", 
		"DECIMAL_FIXED", 
		"ID_LIST", 
		"ARRAY_OBJECT", 
		"UNCONSTRAINED_ARRAY", 
		"CONSTRAINED_ARRAY", 
		"ENUMERATION", 
		"REF", 
		"ITEM", 
		"BODY", 
		"BLOCK", 
		"SIMPLE_STATEMENT", 
		"COMPOUND_STATEMENT", 
		"EXPLICIT_DEREFERENCE", 
		"INDEXED_COMPONENT", 
		"SLICE", 
		"SELECTED_COMPONENT", 
		"OBJECT", 
		"ATTRIBUTE_REFERENCE", 
		"COMPONENT", 
		"PARAMETER", 
		"DISCRIMINANT_SPECIFICATION", 
		"QUALIFIED_BY_EXPRESSION", 
		"QUALIFIED_BY_AGREGATE", 
		"COMPONENT_LIST", 
		"EXPRESSION", 
		"DISCRETE_RANGE", 
		"IN_RANGE", 
		"IN_SUBTYPE", 
		"LABEL", 
		"IDENTIFIER", 
		"TYPE", 
		"IS", 
		"SEMICOLON", 
		"SUBTYPE", 
		"COLON", 
		"CONSTANT", 
		"ASSIGN", 
		"ABSTRACT", 
		"NEW", 
		"ALIASED", 
		"COMMA", 
		"RANGE", 
		"DOTDOT", 
		"LPARANTHESIS", 
		"RPARANTHESIS", 
		"CHARACTER_LITERAL", 
		"MOD", 
		"DIGITS", 
		"DELTA", 
		"ARRAY", 
		"OF", 
		"BOX", 
		"TAGGED", 
		"LIMITED", 
		"RECORD", 
		"END", 
		"NULL", 
		"CASE", 
		"WHEN", 
		"ASSOCIATION", 
		"OTHERS", 
		"CHOICE", 
		"WITH", 
		"ACCESS", 
		"DOT", 
		"ALL", 
		"APOSTROPHE", 
		"AND", 
		"THEN", 
		"OR", 
		"ELSE", 
		"XOR", 
		"NOT", 
		"IN", 
		"POW", 
		"ABS", 
		"NUMERIC_LITERAL", 
		"STRING_LITERAL", 
		"EQ", 
		"NEQ", 
		"LESS", 
		"LEQ", 
		"GREATER", 
		"GEQ", 
		"PLUS", 
		"MINUS", 
		"CONCAT", 
		"MULT", 
		"DIV", 
		"REM", 
		"LLABELBRACKET", 
		"RLABELBRACKET", 
		"IF", 
		"ELSIF", 
		"LOOP", 
		"WHILE", 
		"FOR", 
		"REVERSE", 
		"DECLARE", 
		"BEGIN", 
		"EXIT", 
		"GOTO", 
		"PROCEDURE", 
		"FUNCTION", 
		"RETURN", 
		"OUT", 
		"IDENTIFIER_LETTER", 
		"DIGIT", 
		"DECIMAL_LITERAL", 
		"BASED_LITERAL", 
		"NUMERAL", 
		"EXPONENT", 
		"BASE", 
		"BASED_NUMERAL", 
		"EXTENDED_DIGIT", 
		"COMMENT", 
		"WS"
    };

    public const int FUNCTION = 108;
    public const int EXPONENT = 116;
    public const int WHILE = 100;
    public const int MOD = 51;
    public const int UNCONSTRAINED_ARRAY = 8;
    public const int CASE = 62;
    public const int NEW = 43;
    public const int NOT = 77;
    public const int IN_SUBTYPE = 32;
    public const int SUBTYPE = 38;
    public const int EOF = -1;
    public const int BASED_LITERAL = 114;
    public const int TYPE = 35;
    public const int ATTRIBUTE_REFERENCE = 22;
    public const int STRING_LITERAL = 82;
    public const int GREATER = 87;
    public const int POW = 79;
    public const int ENUMERATION = 10;
    public const int BEGIN = 104;
    public const int LOOP = 99;
    public const int PARAMETER = 24;
    public const int RETURN = 109;
    public const int LESS = 85;
    public const int EXPLICIT_DEREFERENCE = 17;
    public const int BASE = 117;
    public const int ALIASED = 44;
    public const int BODY = 13;
    public const int APOSTROPHE = 71;
    public const int GEQ = 88;
    public const int GOTO = 106;
    public const int EQ = 83;
    public const int COMMENT = 120;
    public const int RLABELBRACKET = 96;
    public const int ARRAY = 54;
    public const int EXIT = 105;
    public const int RECORD = 59;
    public const int CONSTRAINED_ARRAY = 9;
    public const int CONCAT = 91;
    public const int NULL = 61;
    public const int ELSE = 75;
    public const int CHARACTER_LITERAL = 50;
    public const int DELTA = 53;
    public const int SEMICOLON = 37;
    public const int INDEXED_COMPONENT = 18;
    public const int MULT = 92;
    public const int COMPONENT_LIST = 28;
    public const int OF = 55;
    public const int ABS = 80;
    public const int WS = 121;
    public const int OUT = 110;
    public const int EXTENDED_DIGIT = 119;
    public const int COMPOUND_STATEMENT = 16;
    public const int OR = 74;
    public const int CONSTANT = 40;
    public const int NUMERAL = 115;
    public const int ELSIF = 98;
    public const int REVERSE = 102;
    public const int END = 60;
    public const int DECIMAL_LITERAL = 113;
    public const int OTHERS = 65;
    public const int LIMITED = 58;
    public const int IDENTIFIER_LETTER = 111;
    public const int NUMERIC_LITERAL = 81;
    public const int DIGITS = 52;
    public const int FOR = 101;
    public const int DISCRIMINANT_SPECIFICATION = 25;
    public const int DOTDOT = 47;
    public const int ABSTRACT = 42;
    public const int QUALIFIED_BY_EXPRESSION = 26;
    public const int SLICE = 19;
    public const int ID_LIST = 6;
    public const int AND = 72;
    public const int IF = 97;
    public const int SELECTED_COMPONENT = 20;
    public const int BOX = 56;
    public const int ORDINARY_FIXED = 4;
    public const int THEN = 73;
    public const int IN = 78;
    public const int RPARANTHESIS = 49;
    public const int OBJECT = 21;
    public const int COMMA = 45;
    public const int IS = 36;
    public const int IDENTIFIER = 34;
    public const int DECIMAL_FIXED = 5;
    public const int ALL = 70;
    public const int ACCESS = 68;
    public const int BASED_NUMERAL = 118;
    public const int PLUS = 89;
    public const int DIGIT = 112;
    public const int IN_RANGE = 31;
    public const int DOT = 69;
    public const int COMPONENT = 23;
    public const int EXPRESSION = 29;
    public const int CHOICE = 66;
    public const int DISCRETE_RANGE = 30;
    public const int WITH = 67;
    public const int LLABELBRACKET = 95;
    public const int XOR = 76;
    public const int ITEM = 12;
    public const int SIMPLE_STATEMENT = 15;
    public const int RANGE = 46;
    public const int ARRAY_OBJECT = 7;
    public const int REM = 94;
    public const int MINUS = 90;
    public const int PROCEDURE = 107;
    public const int REF = 11;
    public const int COLON = 39;
    public const int LPARANTHESIS = 48;
    public const int NEQ = 84;
    public const int LABEL = 33;
    public const int TAGGED = 57;
    public const int WHEN = 63;
    public const int BLOCK = 14;
    public const int ASSIGN = 41;
    public const int DECLARE = 103;
    public const int QUALIFIED_BY_AGREGATE = 27;
    public const int DIV = 93;
    public const int ASSOCIATION = 64;
    public const int LEQ = 86;

    // delegates
    // delegators

    protected class Symbols_scope 
    {
        protected internal SortedSet< string > IDs;
    }
    protected Stack Symbols_stack = new Stack();
    protected class SymbolTable_scope 
    {
        protected internal SortedSet< string > Types;
        protected internal SortedSet< string > Functions;
        protected internal SortedSet< string > Procedures;
        protected internal SortedSet< string > Objects;
    }
    protected Stack SymbolTable_stack = new Stack();



        public Ada95Parser(ITokenStream input)
    		: this(input, new RecognizerSharedState()) {
        }

        public Ada95Parser(ITokenStream input, RecognizerSharedState state)
    		: base(input, state) {
            InitializeCyclicDFAs();
            this.state.ruleMemo = new Hashtable[135+1];
             
             
        }
        
    protected ITreeAdaptor adaptor = new CommonTreeAdaptor();

    public ITreeAdaptor TreeAdaptor
    {
        get { return this.adaptor; }
        set {
    	this.adaptor = value;
    	}
    }

    override public string[] TokenNames {
		get { return Ada95Parser.tokenNames; }
    }

    override public string GrammarFileName {
		get { return "Ada95.g"; }
    }


    	// symbols related stuff.
    	private bool IsType( string id )
    	{
    		bool found = false;
    		for ( int i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Types.Contains( id ) );

    		return ( found );
    	}
    	
    	private bool IsFunction( string id )
    	{
    		bool found = false;
    		for ( int i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Functions.Contains( id ) );

    		return ( found );
    	}
    	
    	private bool IsProcedure( string id )
    	{
    		bool found = false;
    		for ( int i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Procedures.Contains( id ) );

    		return ( found );
    	}
    	
    	private bool IsObject( string id )
    	{
    		bool found = false;
    		for ( int i = SymbolTable_stack.Count - 1; !found && i >= 0; --i )
    			found = ( ((SymbolTable_scope)SymbolTable_stack[ i ]).Objects.Contains( id ) );
    			
    		return ( found );
    	}
    		
    	// errors related stuff.
    	public StringBuilder Errors = new StringBuilder();
    		
    	public override void EmitErrorMessage( string message )
    	{
    		Errors.AppendLine( message );
    	}


    public class program_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "program"
    // Ada95.g:114:8: public program : declarative_part ;
    public Ada95Parser.program_return program() // throws RecognitionException [1]
    {   
        SymbolTable_stack.Push(new SymbolTable_scope());

        Ada95Parser.program_return retval = new Ada95Parser.program_return();
        retval.Start = input.LT(1);
        int program_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.declarative_part_return declarative_part1 = default(Ada95Parser.declarative_part_return);




        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Functions =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Procedures =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Objects =  new SortedSet< string >();
        	
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 1) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:123:2: ( declarative_part )
            // Ada95.g:123:4: declarative_part
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_declarative_part_in_program235);
            	declarative_part1 = declarative_part();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, declarative_part1.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 1, program_StartIndex); 
            }
            SymbolTable_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "program"

    public class basic_declaration_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "basic_declaration"
    // Ada95.g:125:1: basic_declaration : ( type_declaration | object_declaration | number_declaration | subprogram_declaration );
    public Ada95Parser.basic_declaration_return basic_declaration() // throws RecognitionException [1]
    {   
        Ada95Parser.basic_declaration_return retval = new Ada95Parser.basic_declaration_return();
        retval.Start = input.LT(1);
        int basic_declaration_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.type_declaration_return type_declaration2 = default(Ada95Parser.type_declaration_return);

        Ada95Parser.object_declaration_return object_declaration3 = default(Ada95Parser.object_declaration_return);

        Ada95Parser.number_declaration_return number_declaration4 = default(Ada95Parser.number_declaration_return);

        Ada95Parser.subprogram_declaration_return subprogram_declaration5 = default(Ada95Parser.subprogram_declaration_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 2) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:126:2: ( type_declaration | object_declaration | number_declaration | subprogram_declaration )
            int alt1 = 4;
            alt1 = dfa1.Predict(input);
            switch (alt1) 
            {
                case 1 :
                    // Ada95.g:127:3: type_declaration
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_type_declaration_in_basic_declaration247);
                    	type_declaration2 = type_declaration();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, type_declaration2.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:129:3: object_declaration
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_object_declaration_in_basic_declaration256);
                    	object_declaration3 = object_declaration();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, object_declaration3.Tree);

                    }
                    break;
                case 3 :
                    // Ada95.g:130:3: number_declaration
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_number_declaration_in_basic_declaration262);
                    	number_declaration4 = number_declaration();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, number_declaration4.Tree);

                    }
                    break;
                case 4 :
                    // Ada95.g:131:3: subprogram_declaration
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_subprogram_declaration_in_basic_declaration268);
                    	subprogram_declaration5 = subprogram_declaration();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subprogram_declaration5.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 2, basic_declaration_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "basic_declaration"

    public class defining_identifier_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "defining_identifier"
    // Ada95.g:134:1: defining_identifier : IDENTIFIER ;
    public Ada95Parser.defining_identifier_return defining_identifier() // throws RecognitionException [1]
    {   
        Ada95Parser.defining_identifier_return retval = new Ada95Parser.defining_identifier_return();
        retval.Start = input.LT(1);
        int defining_identifier_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IDENTIFIER6 = null;

        CommonTree IDENTIFIER6_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 3) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:135:2: ( IDENTIFIER )
            // Ada95.g:135:4: IDENTIFIER
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	IDENTIFIER6=(IToken)Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_defining_identifier279); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{IDENTIFIER6_tree = (CommonTree)adaptor.Create(IDENTIFIER6);
            		adaptor.AddChild(root_0, IDENTIFIER6_tree);
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 3, defining_identifier_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "defining_identifier"

    public class type_declaration_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "type_declaration"
    // Ada95.g:137:1: type_declaration : full_type_declaration ;
    public Ada95Parser.type_declaration_return type_declaration() // throws RecognitionException [1]
    {   
        Ada95Parser.type_declaration_return retval = new Ada95Parser.type_declaration_return();
        retval.Start = input.LT(1);
        int type_declaration_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.full_type_declaration_return full_type_declaration7 = default(Ada95Parser.full_type_declaration_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 4) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:138:2: ( full_type_declaration )
            // Ada95.g:138:4: full_type_declaration
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_full_type_declaration_in_type_declaration289);
            	full_type_declaration7 = full_type_declaration();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, full_type_declaration7.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 4, type_declaration_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "type_declaration"

    public class full_type_declaration_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "full_type_declaration"
    // Ada95.g:140:1: full_type_declaration : TYPE id= defining_identifier IS type_definition SEMICOLON ;
    public Ada95Parser.full_type_declaration_return full_type_declaration() // throws RecognitionException [1]
    {   
        Ada95Parser.full_type_declaration_return retval = new Ada95Parser.full_type_declaration_return();
        retval.Start = input.LT(1);
        int full_type_declaration_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken TYPE8 = null;
        IToken IS9 = null;
        IToken SEMICOLON11 = null;
        Ada95Parser.defining_identifier_return id = default(Ada95Parser.defining_identifier_return);

        Ada95Parser.type_definition_return type_definition10 = default(Ada95Parser.type_definition_return);


        CommonTree TYPE8_tree=null;
        CommonTree IS9_tree=null;
        CommonTree SEMICOLON11_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 5) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:141:2: ( TYPE id= defining_identifier IS type_definition SEMICOLON )
            // Ada95.g:142:3: TYPE id= defining_identifier IS type_definition SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	TYPE8=(IToken)Match(input,TYPE,FOLLOW_TYPE_in_full_type_declaration301); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{TYPE8_tree = (CommonTree)adaptor.Create(TYPE8);
            		root_0 = (CommonTree)adaptor.BecomeRoot(TYPE8_tree, root_0);
            	}
            	PushFollow(FOLLOW_defining_identifier_in_full_type_declaration308);
            	id = defining_identifier();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, id.Tree);
            	IS9=(IToken)Match(input,IS,FOLLOW_IS_in_full_type_declaration312); if (state.failed) return retval;
            	PushFollow(FOLLOW_type_definition_in_full_type_declaration315);
            	type_definition10 = type_definition();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, type_definition10.Tree);
            	SEMICOLON11=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_full_type_declaration317); if (state.failed) return retval;
            	if ( (state.backtracking==0) )
            	{

            	  		 	// TODO: discriminant
            	  		 	((SymbolTable_scope)SymbolTable_stack.Peek()).Types.Add( ((id != null) ? input.ToString((IToken)(id.Start),(IToken)(id.Stop)) : null) );
            	  		
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 5, full_type_declaration_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "full_type_declaration"

    public class type_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "type_definition"
    // Ada95.g:149:1: type_definition : ( enumeration_type_definition | integer_type_definition | real_type_definition | array_type_definition | record_type_definition | derived_type_definition );
    public Ada95Parser.type_definition_return type_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.type_definition_return retval = new Ada95Parser.type_definition_return();
        retval.Start = input.LT(1);
        int type_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.enumeration_type_definition_return enumeration_type_definition12 = default(Ada95Parser.enumeration_type_definition_return);

        Ada95Parser.integer_type_definition_return integer_type_definition13 = default(Ada95Parser.integer_type_definition_return);

        Ada95Parser.real_type_definition_return real_type_definition14 = default(Ada95Parser.real_type_definition_return);

        Ada95Parser.array_type_definition_return array_type_definition15 = default(Ada95Parser.array_type_definition_return);

        Ada95Parser.record_type_definition_return record_type_definition16 = default(Ada95Parser.record_type_definition_return);

        Ada95Parser.derived_type_definition_return derived_type_definition17 = default(Ada95Parser.derived_type_definition_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 6) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:150:2: ( enumeration_type_definition | integer_type_definition | real_type_definition | array_type_definition | record_type_definition | derived_type_definition )
            int alt2 = 6;
            switch ( input.LA(1) ) 
            {
            case LPARANTHESIS:
            	{
                alt2 = 1;
                }
                break;
            case RANGE:
            case MOD:
            	{
                alt2 = 2;
                }
                break;
            case DIGITS:
            case DELTA:
            	{
                alt2 = 3;
                }
                break;
            case ARRAY:
            	{
                alt2 = 4;
                }
                break;
            case ABSTRACT:
            	{
                int LA2_5 = input.LA(2);

                if ( (LA2_5 == TAGGED) )
                {
                    alt2 = 5;
                }
                else if ( (LA2_5 == NEW) )
                {
                    alt2 = 6;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d2s5 =
                        new NoViableAltException("", 2, 5, input);

                    throw nvae_d2s5;
                }
                }
                break;
            case TAGGED:
            case LIMITED:
            case RECORD:
            case NULL:
            	{
                alt2 = 5;
                }
                break;
            case NEW:
            	{
                alt2 = 6;
                }
                break;
            	default:
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    NoViableAltException nvae_d2s0 =
            	        new NoViableAltException("", 2, 0, input);

            	    throw nvae_d2s0;
            }

            switch (alt2) 
            {
                case 1 :
                    // Ada95.g:151:3: enumeration_type_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_enumeration_type_definition_in_type_definition335);
                    	enumeration_type_definition12 = enumeration_type_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, enumeration_type_definition12.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:152:3: integer_type_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_integer_type_definition_in_type_definition341);
                    	integer_type_definition13 = integer_type_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, integer_type_definition13.Tree);

                    }
                    break;
                case 3 :
                    // Ada95.g:153:3: real_type_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_real_type_definition_in_type_definition347);
                    	real_type_definition14 = real_type_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, real_type_definition14.Tree);

                    }
                    break;
                case 4 :
                    // Ada95.g:154:3: array_type_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_array_type_definition_in_type_definition353);
                    	array_type_definition15 = array_type_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, array_type_definition15.Tree);

                    }
                    break;
                case 5 :
                    // Ada95.g:155:3: record_type_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_record_type_definition_in_type_definition359);
                    	record_type_definition16 = record_type_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, record_type_definition16.Tree);

                    }
                    break;
                case 6 :
                    // Ada95.g:157:3: derived_type_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_derived_type_definition_in_type_definition368);
                    	derived_type_definition17 = derived_type_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, derived_type_definition17.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 6, type_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "type_definition"

    public class subtype_declaration_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "subtype_declaration"
    // Ada95.g:160:1: subtype_declaration : SUBTYPE defining_identifier IS subtype_indication SEMICOLON ;
    public Ada95Parser.subtype_declaration_return subtype_declaration() // throws RecognitionException [1]
    {   
        Ada95Parser.subtype_declaration_return retval = new Ada95Parser.subtype_declaration_return();
        retval.Start = input.LT(1);
        int subtype_declaration_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken SUBTYPE18 = null;
        IToken IS20 = null;
        IToken SEMICOLON22 = null;
        Ada95Parser.defining_identifier_return defining_identifier19 = default(Ada95Parser.defining_identifier_return);

        Ada95Parser.subtype_indication_return subtype_indication21 = default(Ada95Parser.subtype_indication_return);


        CommonTree SUBTYPE18_tree=null;
        CommonTree IS20_tree=null;
        CommonTree SEMICOLON22_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 7) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:161:2: ( SUBTYPE defining_identifier IS subtype_indication SEMICOLON )
            // Ada95.g:161:4: SUBTYPE defining_identifier IS subtype_indication SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	SUBTYPE18=(IToken)Match(input,SUBTYPE,FOLLOW_SUBTYPE_in_subtype_declaration379); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{SUBTYPE18_tree = (CommonTree)adaptor.Create(SUBTYPE18);
            		root_0 = (CommonTree)adaptor.BecomeRoot(SUBTYPE18_tree, root_0);
            	}
            	PushFollow(FOLLOW_defining_identifier_in_subtype_declaration382);
            	defining_identifier19 = defining_identifier();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, defining_identifier19.Tree);
            	IS20=(IToken)Match(input,IS,FOLLOW_IS_in_subtype_declaration384); if (state.failed) return retval;
            	PushFollow(FOLLOW_subtype_indication_in_subtype_declaration387);
            	subtype_indication21 = subtype_indication();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subtype_indication21.Tree);
            	SEMICOLON22=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_subtype_declaration389); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 7, subtype_declaration_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "subtype_declaration"

    public class subtype_mark_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "subtype_mark"
    // Ada95.g:163:1: subtype_mark : IDENTIFIER ;
    public Ada95Parser.subtype_mark_return subtype_mark() // throws RecognitionException [1]
    {   
        Ada95Parser.subtype_mark_return retval = new Ada95Parser.subtype_mark_return();
        retval.Start = input.LT(1);
        int subtype_mark_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IDENTIFIER23 = null;

        CommonTree IDENTIFIER23_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 8) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:164:2: ( IDENTIFIER )
            // Ada95.g:164:4: IDENTIFIER
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	IDENTIFIER23=(IToken)Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_subtype_mark400); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{IDENTIFIER23_tree = (CommonTree)adaptor.Create(IDENTIFIER23);
            		adaptor.AddChild(root_0, IDENTIFIER23_tree);
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 8, subtype_mark_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "subtype_mark"

    public class subtype_indication_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "subtype_indication"
    // Ada95.g:166:1: subtype_indication : subtype_mark ( constraint )? ;
    public Ada95Parser.subtype_indication_return subtype_indication() // throws RecognitionException [1]
    {   
        Ada95Parser.subtype_indication_return retval = new Ada95Parser.subtype_indication_return();
        retval.Start = input.LT(1);
        int subtype_indication_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.subtype_mark_return subtype_mark24 = default(Ada95Parser.subtype_mark_return);

        Ada95Parser.constraint_return constraint25 = default(Ada95Parser.constraint_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 9) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:167:2: ( subtype_mark ( constraint )? )
            // Ada95.g:167:4: subtype_mark ( constraint )?
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_subtype_mark_in_subtype_indication410);
            	subtype_mark24 = subtype_mark();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) root_0 = (CommonTree)adaptor.BecomeRoot(subtype_mark24.Tree, root_0);
            	// Ada95.g:167:18: ( constraint )?
            	int alt3 = 2;
            	int LA3_0 = input.LA(1);

            	if ( (LA3_0 == RANGE || LA3_0 == DIGITS) )
            	{
            	    alt3 = 1;
            	}
            	switch (alt3) 
            	{
            	    case 1 :
            	        // Ada95.g:167:18: constraint
            	        {
            	        	PushFollow(FOLLOW_constraint_in_subtype_indication413);
            	        	constraint25 = constraint();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, constraint25.Tree);

            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 9, subtype_indication_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "subtype_indication"

    public class constraint_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "constraint"
    // Ada95.g:169:1: constraint : scalar_constraint ;
    public Ada95Parser.constraint_return constraint() // throws RecognitionException [1]
    {   
        Ada95Parser.constraint_return retval = new Ada95Parser.constraint_return();
        retval.Start = input.LT(1);
        int constraint_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.scalar_constraint_return scalar_constraint26 = default(Ada95Parser.scalar_constraint_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 10) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:170:2: ( scalar_constraint )
            // Ada95.g:171:3: scalar_constraint
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_scalar_constraint_in_constraint426);
            	scalar_constraint26 = scalar_constraint();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, scalar_constraint26.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 10, constraint_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "constraint"

    public class scalar_constraint_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "scalar_constraint"
    // Ada95.g:175:1: scalar_constraint : ( range_constraint | digits_constraint );
    public Ada95Parser.scalar_constraint_return scalar_constraint() // throws RecognitionException [1]
    {   
        Ada95Parser.scalar_constraint_return retval = new Ada95Parser.scalar_constraint_return();
        retval.Start = input.LT(1);
        int scalar_constraint_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.range_constraint_return range_constraint27 = default(Ada95Parser.range_constraint_return);

        Ada95Parser.digits_constraint_return digits_constraint28 = default(Ada95Parser.digits_constraint_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 11) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:176:2: ( range_constraint | digits_constraint )
            int alt4 = 2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0 == RANGE) )
            {
                alt4 = 1;
            }
            else if ( (LA4_0 == DIGITS) )
            {
                alt4 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d4s0 =
                    new NoViableAltException("", 4, 0, input);

                throw nvae_d4s0;
            }
            switch (alt4) 
            {
                case 1 :
                    // Ada95.g:177:3: range_constraint
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_range_constraint_in_scalar_constraint443);
                    	range_constraint27 = range_constraint();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, range_constraint27.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:178:3: digits_constraint
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_digits_constraint_in_scalar_constraint449);
                    	digits_constraint28 = digits_constraint();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, digits_constraint28.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 11, scalar_constraint_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "scalar_constraint"

    public class number_declaration_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "number_declaration"
    // Ada95.g:185:1: number_declaration : defining_identifier_list COLON CONSTANT ASSIGN expression SEMICOLON ;
    public Ada95Parser.number_declaration_return number_declaration() // throws RecognitionException [1]
    {   
        Ada95Parser.number_declaration_return retval = new Ada95Parser.number_declaration_return();
        retval.Start = input.LT(1);
        int number_declaration_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken COLON30 = null;
        IToken CONSTANT31 = null;
        IToken ASSIGN32 = null;
        IToken SEMICOLON34 = null;
        Ada95Parser.defining_identifier_list_return defining_identifier_list29 = default(Ada95Parser.defining_identifier_list_return);

        Ada95Parser.expression_return expression33 = default(Ada95Parser.expression_return);


        CommonTree COLON30_tree=null;
        CommonTree CONSTANT31_tree=null;
        CommonTree ASSIGN32_tree=null;
        CommonTree SEMICOLON34_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 12) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:186:2: ( defining_identifier_list COLON CONSTANT ASSIGN expression SEMICOLON )
            // Ada95.g:186:4: defining_identifier_list COLON CONSTANT ASSIGN expression SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_defining_identifier_list_in_number_declaration462);
            	defining_identifier_list29 = defining_identifier_list();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, defining_identifier_list29.Tree);
            	COLON30=(IToken)Match(input,COLON,FOLLOW_COLON_in_number_declaration464); if (state.failed) return retval;
            	CONSTANT31=(IToken)Match(input,CONSTANT,FOLLOW_CONSTANT_in_number_declaration467); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{CONSTANT31_tree = (CommonTree)adaptor.Create(CONSTANT31);
            		root_0 = (CommonTree)adaptor.BecomeRoot(CONSTANT31_tree, root_0);
            	}
            	ASSIGN32=(IToken)Match(input,ASSIGN,FOLLOW_ASSIGN_in_number_declaration470); if (state.failed) return retval;
            	PushFollow(FOLLOW_expression_in_number_declaration473);
            	expression33 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression33.Tree);
            	SEMICOLON34=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_number_declaration475); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 12, number_declaration_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "number_declaration"

    public class derived_type_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "derived_type_definition"
    // Ada95.g:188:1: derived_type_definition : ( ABSTRACT )? NEW subtype_indication ( record_extension_part )? ;
    public Ada95Parser.derived_type_definition_return derived_type_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.derived_type_definition_return retval = new Ada95Parser.derived_type_definition_return();
        retval.Start = input.LT(1);
        int derived_type_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken ABSTRACT35 = null;
        IToken NEW36 = null;
        Ada95Parser.subtype_indication_return subtype_indication37 = default(Ada95Parser.subtype_indication_return);

        Ada95Parser.record_extension_part_return record_extension_part38 = default(Ada95Parser.record_extension_part_return);


        CommonTree ABSTRACT35_tree=null;
        CommonTree NEW36_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 13) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:189:2: ( ( ABSTRACT )? NEW subtype_indication ( record_extension_part )? )
            // Ada95.g:189:4: ( ABSTRACT )? NEW subtype_indication ( record_extension_part )?
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	// Ada95.g:189:4: ( ABSTRACT )?
            	int alt5 = 2;
            	int LA5_0 = input.LA(1);

            	if ( (LA5_0 == ABSTRACT) )
            	{
            	    alt5 = 1;
            	}
            	switch (alt5) 
            	{
            	    case 1 :
            	        // Ada95.g:189:4: ABSTRACT
            	        {
            	        	ABSTRACT35=(IToken)Match(input,ABSTRACT,FOLLOW_ABSTRACT_in_derived_type_definition486); if (state.failed) return retval;
            	        	if ( state.backtracking == 0 )
            	        	{ABSTRACT35_tree = (CommonTree)adaptor.Create(ABSTRACT35);
            	        		adaptor.AddChild(root_0, ABSTRACT35_tree);
            	        	}

            	        }
            	        break;

            	}

            	NEW36=(IToken)Match(input,NEW,FOLLOW_NEW_in_derived_type_definition489); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{NEW36_tree = (CommonTree)adaptor.Create(NEW36);
            		root_0 = (CommonTree)adaptor.BecomeRoot(NEW36_tree, root_0);
            	}
            	PushFollow(FOLLOW_subtype_indication_in_derived_type_definition492);
            	subtype_indication37 = subtype_indication();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subtype_indication37.Tree);
            	// Ada95.g:189:38: ( record_extension_part )?
            	int alt6 = 2;
            	int LA6_0 = input.LA(1);

            	if ( (LA6_0 == WITH) )
            	{
            	    alt6 = 1;
            	}
            	switch (alt6) 
            	{
            	    case 1 :
            	        // Ada95.g:189:38: record_extension_part
            	        {
            	        	PushFollow(FOLLOW_record_extension_part_in_derived_type_definition494);
            	        	record_extension_part38 = record_extension_part();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, record_extension_part38.Tree);

            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 13, derived_type_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "derived_type_definition"

    public class object_declaration_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "object_declaration"
    // Ada95.g:191:1: object_declaration : defining_identifier_list COLON ( ALIASED )? ( CONSTANT )? ( subtype_indication ( ASSIGN expression )? -> ^( OBJECT defining_identifier_list subtype_indication ( expression )? ) | array_type_definition ( ASSIGN expression )? -> ^( ARRAY_OBJECT defining_identifier_list array_type_definition ( expression )? ) ) SEMICOLON ;
    public Ada95Parser.object_declaration_return object_declaration() // throws RecognitionException [1]
    {   
        Symbols_stack.Push(new Symbols_scope());

        Ada95Parser.object_declaration_return retval = new Ada95Parser.object_declaration_return();
        retval.Start = input.LT(1);
        int object_declaration_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken COLON40 = null;
        IToken ALIASED41 = null;
        IToken CONSTANT42 = null;
        IToken ASSIGN44 = null;
        IToken ASSIGN47 = null;
        IToken SEMICOLON49 = null;
        Ada95Parser.defining_identifier_list_return defining_identifier_list39 = default(Ada95Parser.defining_identifier_list_return);

        Ada95Parser.subtype_indication_return subtype_indication43 = default(Ada95Parser.subtype_indication_return);

        Ada95Parser.expression_return expression45 = default(Ada95Parser.expression_return);

        Ada95Parser.array_type_definition_return array_type_definition46 = default(Ada95Parser.array_type_definition_return);

        Ada95Parser.expression_return expression48 = default(Ada95Parser.expression_return);


        CommonTree COLON40_tree=null;
        CommonTree ALIASED41_tree=null;
        CommonTree CONSTANT42_tree=null;
        CommonTree ASSIGN44_tree=null;
        CommonTree ASSIGN47_tree=null;
        CommonTree SEMICOLON49_tree=null;
        RewriteRuleTokenStream stream_COLON = new RewriteRuleTokenStream(adaptor,"token COLON");
        RewriteRuleTokenStream stream_CONSTANT = new RewriteRuleTokenStream(adaptor,"token CONSTANT");
        RewriteRuleTokenStream stream_ALIASED = new RewriteRuleTokenStream(adaptor,"token ALIASED");
        RewriteRuleTokenStream stream_SEMICOLON = new RewriteRuleTokenStream(adaptor,"token SEMICOLON");
        RewriteRuleTokenStream stream_ASSIGN = new RewriteRuleTokenStream(adaptor,"token ASSIGN");
        RewriteRuleSubtreeStream stream_expression = new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_subtype_indication = new RewriteRuleSubtreeStream(adaptor,"rule subtype_indication");
        RewriteRuleSubtreeStream stream_defining_identifier_list = new RewriteRuleSubtreeStream(adaptor,"rule defining_identifier_list");
        RewriteRuleSubtreeStream stream_array_type_definition = new RewriteRuleSubtreeStream(adaptor,"rule array_type_definition");

        		((Symbols_scope)Symbols_stack.Peek()).IDs =  new SortedSet< string >();
        	
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 14) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:197:2: ( defining_identifier_list COLON ( ALIASED )? ( CONSTANT )? ( subtype_indication ( ASSIGN expression )? -> ^( OBJECT defining_identifier_list subtype_indication ( expression )? ) | array_type_definition ( ASSIGN expression )? -> ^( ARRAY_OBJECT defining_identifier_list array_type_definition ( expression )? ) ) SEMICOLON )
            // Ada95.g:198:3: defining_identifier_list COLON ( ALIASED )? ( CONSTANT )? ( subtype_indication ( ASSIGN expression )? -> ^( OBJECT defining_identifier_list subtype_indication ( expression )? ) | array_type_definition ( ASSIGN expression )? -> ^( ARRAY_OBJECT defining_identifier_list array_type_definition ( expression )? ) ) SEMICOLON
            {
            	PushFollow(FOLLOW_defining_identifier_list_in_object_declaration520);
            	defining_identifier_list39 = defining_identifier_list();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_defining_identifier_list.Add(defining_identifier_list39.Tree);
            	COLON40=(IToken)Match(input,COLON,FOLLOW_COLON_in_object_declaration522); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_COLON.Add(COLON40);

            	// Ada95.g:198:34: ( ALIASED )?
            	int alt7 = 2;
            	int LA7_0 = input.LA(1);

            	if ( (LA7_0 == ALIASED) )
            	{
            	    alt7 = 1;
            	}
            	switch (alt7) 
            	{
            	    case 1 :
            	        // Ada95.g:198:34: ALIASED
            	        {
            	        	ALIASED41=(IToken)Match(input,ALIASED,FOLLOW_ALIASED_in_object_declaration524); if (state.failed) return retval; 
            	        	if ( (state.backtracking==0) ) stream_ALIASED.Add(ALIASED41);


            	        }
            	        break;

            	}

            	// Ada95.g:198:43: ( CONSTANT )?
            	int alt8 = 2;
            	int LA8_0 = input.LA(1);

            	if ( (LA8_0 == CONSTANT) )
            	{
            	    alt8 = 1;
            	}
            	switch (alt8) 
            	{
            	    case 1 :
            	        // Ada95.g:198:43: CONSTANT
            	        {
            	        	CONSTANT42=(IToken)Match(input,CONSTANT,FOLLOW_CONSTANT_in_object_declaration527); if (state.failed) return retval; 
            	        	if ( (state.backtracking==0) ) stream_CONSTANT.Add(CONSTANT42);


            	        }
            	        break;

            	}

            	// Ada95.g:199:3: ( subtype_indication ( ASSIGN expression )? -> ^( OBJECT defining_identifier_list subtype_indication ( expression )? ) | array_type_definition ( ASSIGN expression )? -> ^( ARRAY_OBJECT defining_identifier_list array_type_definition ( expression )? ) )
            	int alt11 = 2;
            	int LA11_0 = input.LA(1);

            	if ( (LA11_0 == IDENTIFIER) )
            	{
            	    alt11 = 1;
            	}
            	else if ( (LA11_0 == ARRAY) )
            	{
            	    alt11 = 2;
            	}
            	else 
            	{
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    NoViableAltException nvae_d11s0 =
            	        new NoViableAltException("", 11, 0, input);

            	    throw nvae_d11s0;
            	}
            	switch (alt11) 
            	{
            	    case 1 :
            	        // Ada95.g:200:4: subtype_indication ( ASSIGN expression )?
            	        {
            	        	PushFollow(FOLLOW_subtype_indication_in_object_declaration537);
            	        	subtype_indication43 = subtype_indication();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_subtype_indication.Add(subtype_indication43.Tree);
            	        	// Ada95.g:200:23: ( ASSIGN expression )?
            	        	int alt9 = 2;
            	        	int LA9_0 = input.LA(1);

            	        	if ( (LA9_0 == ASSIGN) )
            	        	{
            	        	    alt9 = 1;
            	        	}
            	        	switch (alt9) 
            	        	{
            	        	    case 1 :
            	        	        // Ada95.g:200:25: ASSIGN expression
            	        	        {
            	        	        	ASSIGN44=(IToken)Match(input,ASSIGN,FOLLOW_ASSIGN_in_object_declaration541); if (state.failed) return retval; 
            	        	        	if ( (state.backtracking==0) ) stream_ASSIGN.Add(ASSIGN44);

            	        	        	PushFollow(FOLLOW_expression_in_object_declaration543);
            	        	        	expression45 = expression();
            	        	        	state.followingStackPointer--;
            	        	        	if (state.failed) return retval;
            	        	        	if ( (state.backtracking==0) ) stream_expression.Add(expression45.Tree);

            	        	        }
            	        	        break;

            	        	}



            	        	// AST REWRITE
            	        	// elements:          expression, subtype_indication, defining_identifier_list
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 200:47: -> ^( OBJECT defining_identifier_list subtype_indication ( expression )? )
            	        	{
            	        	    // Ada95.g:200:50: ^( OBJECT defining_identifier_list subtype_indication ( expression )? )
            	        	    {
            	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(OBJECT, "OBJECT"), root_1);

            	        	    adaptor.AddChild(root_1, stream_defining_identifier_list.NextTree());
            	        	    adaptor.AddChild(root_1, stream_subtype_indication.NextTree());
            	        	    // Ada95.g:200:104: ( expression )?
            	        	    if ( stream_expression.HasNext() )
            	        	    {
            	        	        adaptor.AddChild(root_1, stream_expression.NextTree());

            	        	    }
            	        	    stream_expression.Reset();

            	        	    adaptor.AddChild(root_0, root_1);
            	        	    }

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;
            	    case 2 :
            	        // Ada95.g:201:4: array_type_definition ( ASSIGN expression )?
            	        {
            	        	PushFollow(FOLLOW_array_type_definition_in_object_declaration569);
            	        	array_type_definition46 = array_type_definition();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_array_type_definition.Add(array_type_definition46.Tree);
            	        	// Ada95.g:201:26: ( ASSIGN expression )?
            	        	int alt10 = 2;
            	        	int LA10_0 = input.LA(1);

            	        	if ( (LA10_0 == ASSIGN) )
            	        	{
            	        	    alt10 = 1;
            	        	}
            	        	switch (alt10) 
            	        	{
            	        	    case 1 :
            	        	        // Ada95.g:201:28: ASSIGN expression
            	        	        {
            	        	        	ASSIGN47=(IToken)Match(input,ASSIGN,FOLLOW_ASSIGN_in_object_declaration573); if (state.failed) return retval; 
            	        	        	if ( (state.backtracking==0) ) stream_ASSIGN.Add(ASSIGN47);

            	        	        	PushFollow(FOLLOW_expression_in_object_declaration575);
            	        	        	expression48 = expression();
            	        	        	state.followingStackPointer--;
            	        	        	if (state.failed) return retval;
            	        	        	if ( (state.backtracking==0) ) stream_expression.Add(expression48.Tree);

            	        	        }
            	        	        break;

            	        	}



            	        	// AST REWRITE
            	        	// elements:          defining_identifier_list, expression, array_type_definition
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 201:49: -> ^( ARRAY_OBJECT defining_identifier_list array_type_definition ( expression )? )
            	        	{
            	        	    // Ada95.g:201:52: ^( ARRAY_OBJECT defining_identifier_list array_type_definition ( expression )? )
            	        	    {
            	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(ARRAY_OBJECT, "ARRAY_OBJECT"), root_1);

            	        	    adaptor.AddChild(root_1, stream_defining_identifier_list.NextTree());
            	        	    adaptor.AddChild(root_1, stream_array_type_definition.NextTree());
            	        	    // Ada95.g:201:115: ( expression )?
            	        	    if ( stream_expression.HasNext() )
            	        	    {
            	        	        adaptor.AddChild(root_1, stream_expression.NextTree());

            	        	    }
            	        	    stream_expression.Reset();

            	        	    adaptor.AddChild(root_0, root_1);
            	        	    }

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;

            	}

            	SEMICOLON49=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_object_declaration599); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_SEMICOLON.Add(SEMICOLON49);

            	if ( (state.backtracking==0) )
            	{
            	   ((SymbolTable_scope)SymbolTable_stack.Peek()).Objects.UnionWith( ((Symbols_scope)Symbols_stack.Peek()).IDs ); 
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 14, object_declaration_StartIndex); 
            }
            Symbols_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "object_declaration"

    public class defining_identifier_list_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "defining_identifier_list"
    // Ada95.g:205:1: defining_identifier_list : id= defining_identifier ( COMMA id= defining_identifier )* -> ^( ID_LIST ( defining_identifier )+ ) ;
    public Ada95Parser.defining_identifier_list_return defining_identifier_list() // throws RecognitionException [1]
    {   
        Ada95Parser.defining_identifier_list_return retval = new Ada95Parser.defining_identifier_list_return();
        retval.Start = input.LT(1);
        int defining_identifier_list_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken COMMA50 = null;
        Ada95Parser.defining_identifier_return id = default(Ada95Parser.defining_identifier_return);


        CommonTree COMMA50_tree=null;
        RewriteRuleTokenStream stream_COMMA = new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleSubtreeStream stream_defining_identifier = new RewriteRuleSubtreeStream(adaptor,"rule defining_identifier");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 15) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:206:2: (id= defining_identifier ( COMMA id= defining_identifier )* -> ^( ID_LIST ( defining_identifier )+ ) )
            // Ada95.g:207:3: id= defining_identifier ( COMMA id= defining_identifier )*
            {
            	PushFollow(FOLLOW_defining_identifier_in_defining_identifier_list618);
            	id = defining_identifier();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_defining_identifier.Add(id.Tree);
            	if ( (state.backtracking==0) )
            	{
            	   ((Symbols_scope)Symbols_stack.Peek()).IDs.Add( ((id != null) ? input.ToString((IToken)(id.Start),(IToken)(id.Stop)) : null) ); 
            	}
            	// Ada95.g:207:63: ( COMMA id= defining_identifier )*
            	do 
            	{
            	    int alt12 = 2;
            	    int LA12_0 = input.LA(1);

            	    if ( (LA12_0 == COMMA) )
            	    {
            	        alt12 = 1;
            	    }


            	    switch (alt12) 
            		{
            			case 1 :
            			    // Ada95.g:207:65: COMMA id= defining_identifier
            			    {
            			    	COMMA50=(IToken)Match(input,COMMA,FOLLOW_COMMA_in_defining_identifier_list624); if (state.failed) return retval; 
            			    	if ( (state.backtracking==0) ) stream_COMMA.Add(COMMA50);

            			    	PushFollow(FOLLOW_defining_identifier_in_defining_identifier_list630);
            			    	id = defining_identifier();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( (state.backtracking==0) ) stream_defining_identifier.Add(id.Tree);
            			    	if ( (state.backtracking==0) )
            			    	{
            			    	   ((Symbols_scope)Symbols_stack.Peek()).IDs.Add( ((id != null) ? input.ToString((IToken)(id.Start),(IToken)(id.Stop)) : null) ); 
            			    	}

            			    }
            			    break;

            			default:
            			    goto loop12;
            	    }
            	} while (true);

            	loop12:
            		;	// Stops C# compiler whining that label 'loop12' has no statements



            	// AST REWRITE
            	// elements:          defining_identifier
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 208:4: -> ^( ID_LIST ( defining_identifier )+ )
            	{
            	    // Ada95.g:208:7: ^( ID_LIST ( defining_identifier )+ )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(ID_LIST, "ID_LIST"), root_1);

            	    if ( !(stream_defining_identifier.HasNext()) ) {
            	        throw new RewriteEarlyExitException();
            	    }
            	    while ( stream_defining_identifier.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_defining_identifier.NextTree());

            	    }
            	    stream_defining_identifier.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 15, defining_identifier_list_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "defining_identifier_list"

    public class range_constraint_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "range_constraint"
    // Ada95.g:211:1: range_constraint : RANGE range ;
    public Ada95Parser.range_constraint_return range_constraint() // throws RecognitionException [1]
    {   
        Ada95Parser.range_constraint_return retval = new Ada95Parser.range_constraint_return();
        retval.Start = input.LT(1);
        int range_constraint_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken RANGE51 = null;
        Ada95Parser.range_return range52 = default(Ada95Parser.range_return);


        CommonTree RANGE51_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 16) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:212:2: ( RANGE range )
            // Ada95.g:212:4: RANGE range
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	RANGE51=(IToken)Match(input,RANGE,FOLLOW_RANGE_in_range_constraint660); if (state.failed) return retval;
            	PushFollow(FOLLOW_range_in_range_constraint663);
            	range52 = range();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, range52.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 16, range_constraint_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "range_constraint"

    public class range_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "range"
    // Ada95.g:214:1: range : simple_expression DOTDOT simple_expression -> ^( RANGE simple_expression simple_expression ) ;
    public Ada95Parser.range_return range() // throws RecognitionException [1]
    {   
        Ada95Parser.range_return retval = new Ada95Parser.range_return();
        retval.Start = input.LT(1);
        int range_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken DOTDOT54 = null;
        Ada95Parser.simple_expression_return simple_expression53 = default(Ada95Parser.simple_expression_return);

        Ada95Parser.simple_expression_return simple_expression55 = default(Ada95Parser.simple_expression_return);


        CommonTree DOTDOT54_tree=null;
        RewriteRuleTokenStream stream_DOTDOT = new RewriteRuleTokenStream(adaptor,"token DOTDOT");
        RewriteRuleSubtreeStream stream_simple_expression = new RewriteRuleSubtreeStream(adaptor,"rule simple_expression");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 17) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:215:2: ( simple_expression DOTDOT simple_expression -> ^( RANGE simple_expression simple_expression ) )
            // Ada95.g:217:3: simple_expression DOTDOT simple_expression
            {
            	PushFollow(FOLLOW_simple_expression_in_range678);
            	simple_expression53 = simple_expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_simple_expression.Add(simple_expression53.Tree);
            	DOTDOT54=(IToken)Match(input,DOTDOT,FOLLOW_DOTDOT_in_range680); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_DOTDOT.Add(DOTDOT54);

            	PushFollow(FOLLOW_simple_expression_in_range682);
            	simple_expression55 = simple_expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_simple_expression.Add(simple_expression55.Tree);


            	// AST REWRITE
            	// elements:          simple_expression, simple_expression
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 217:46: -> ^( RANGE simple_expression simple_expression )
            	{
            	    // Ada95.g:217:49: ^( RANGE simple_expression simple_expression )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(RANGE, "RANGE"), root_1);

            	    adaptor.AddChild(root_1, stream_simple_expression.NextTree());
            	    adaptor.AddChild(root_1, stream_simple_expression.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 17, range_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "range"

    public class enumeration_type_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "enumeration_type_definition"
    // Ada95.g:220:1: enumeration_type_definition : LPARANTHESIS enumeration_literal_specification ( COMMA enumeration_literal_specification )* RPARANTHESIS -> ^( ENUMERATION ( enumeration_literal_specification )+ ) ;
    public Ada95Parser.enumeration_type_definition_return enumeration_type_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.enumeration_type_definition_return retval = new Ada95Parser.enumeration_type_definition_return();
        retval.Start = input.LT(1);
        int enumeration_type_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken LPARANTHESIS56 = null;
        IToken COMMA58 = null;
        IToken RPARANTHESIS60 = null;
        Ada95Parser.enumeration_literal_specification_return enumeration_literal_specification57 = default(Ada95Parser.enumeration_literal_specification_return);

        Ada95Parser.enumeration_literal_specification_return enumeration_literal_specification59 = default(Ada95Parser.enumeration_literal_specification_return);


        CommonTree LPARANTHESIS56_tree=null;
        CommonTree COMMA58_tree=null;
        CommonTree RPARANTHESIS60_tree=null;
        RewriteRuleTokenStream stream_LPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token LPARANTHESIS");
        RewriteRuleTokenStream stream_RPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token RPARANTHESIS");
        RewriteRuleTokenStream stream_COMMA = new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleSubtreeStream stream_enumeration_literal_specification = new RewriteRuleSubtreeStream(adaptor,"rule enumeration_literal_specification");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 18) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:221:2: ( LPARANTHESIS enumeration_literal_specification ( COMMA enumeration_literal_specification )* RPARANTHESIS -> ^( ENUMERATION ( enumeration_literal_specification )+ ) )
            // Ada95.g:221:4: LPARANTHESIS enumeration_literal_specification ( COMMA enumeration_literal_specification )* RPARANTHESIS
            {
            	LPARANTHESIS56=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_enumeration_type_definition705); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_LPARANTHESIS.Add(LPARANTHESIS56);

            	PushFollow(FOLLOW_enumeration_literal_specification_in_enumeration_type_definition707);
            	enumeration_literal_specification57 = enumeration_literal_specification();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_enumeration_literal_specification.Add(enumeration_literal_specification57.Tree);
            	// Ada95.g:221:51: ( COMMA enumeration_literal_specification )*
            	do 
            	{
            	    int alt13 = 2;
            	    int LA13_0 = input.LA(1);

            	    if ( (LA13_0 == COMMA) )
            	    {
            	        alt13 = 1;
            	    }


            	    switch (alt13) 
            		{
            			case 1 :
            			    // Ada95.g:221:53: COMMA enumeration_literal_specification
            			    {
            			    	COMMA58=(IToken)Match(input,COMMA,FOLLOW_COMMA_in_enumeration_type_definition711); if (state.failed) return retval; 
            			    	if ( (state.backtracking==0) ) stream_COMMA.Add(COMMA58);

            			    	PushFollow(FOLLOW_enumeration_literal_specification_in_enumeration_type_definition713);
            			    	enumeration_literal_specification59 = enumeration_literal_specification();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( (state.backtracking==0) ) stream_enumeration_literal_specification.Add(enumeration_literal_specification59.Tree);

            			    }
            			    break;

            			default:
            			    goto loop13;
            	    }
            	} while (true);

            	loop13:
            		;	// Stops C# compiler whining that label 'loop13' has no statements

            	RPARANTHESIS60=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_enumeration_type_definition718); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_RPARANTHESIS.Add(RPARANTHESIS60);



            	// AST REWRITE
            	// elements:          enumeration_literal_specification
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 221:109: -> ^( ENUMERATION ( enumeration_literal_specification )+ )
            	{
            	    // Ada95.g:221:112: ^( ENUMERATION ( enumeration_literal_specification )+ )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(ENUMERATION, "ENUMERATION"), root_1);

            	    if ( !(stream_enumeration_literal_specification.HasNext()) ) {
            	        throw new RewriteEarlyExitException();
            	    }
            	    while ( stream_enumeration_literal_specification.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_enumeration_literal_specification.NextTree());

            	    }
            	    stream_enumeration_literal_specification.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 18, enumeration_type_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "enumeration_type_definition"

    public class enumeration_literal_specification_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "enumeration_literal_specification"
    // Ada95.g:223:1: enumeration_literal_specification : ( defining_identifier | defining_character_literal );
    public Ada95Parser.enumeration_literal_specification_return enumeration_literal_specification() // throws RecognitionException [1]
    {   
        Ada95Parser.enumeration_literal_specification_return retval = new Ada95Parser.enumeration_literal_specification_return();
        retval.Start = input.LT(1);
        int enumeration_literal_specification_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.defining_identifier_return defining_identifier61 = default(Ada95Parser.defining_identifier_return);

        Ada95Parser.defining_character_literal_return defining_character_literal62 = default(Ada95Parser.defining_character_literal_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 19) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:224:2: ( defining_identifier | defining_character_literal )
            int alt14 = 2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0 == IDENTIFIER) )
            {
                alt14 = 1;
            }
            else if ( (LA14_0 == CHARACTER_LITERAL) )
            {
                alt14 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d14s0 =
                    new NoViableAltException("", 14, 0, input);

                throw nvae_d14s0;
            }
            switch (alt14) 
            {
                case 1 :
                    // Ada95.g:225:3: defining_identifier
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_defining_identifier_in_enumeration_literal_specification741);
                    	defining_identifier61 = defining_identifier();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, defining_identifier61.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:226:3: defining_character_literal
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_defining_character_literal_in_enumeration_literal_specification747);
                    	defining_character_literal62 = defining_character_literal();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, defining_character_literal62.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 19, enumeration_literal_specification_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "enumeration_literal_specification"

    public class defining_character_literal_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "defining_character_literal"
    // Ada95.g:229:1: defining_character_literal : CHARACTER_LITERAL ;
    public Ada95Parser.defining_character_literal_return defining_character_literal() // throws RecognitionException [1]
    {   
        Ada95Parser.defining_character_literal_return retval = new Ada95Parser.defining_character_literal_return();
        retval.Start = input.LT(1);
        int defining_character_literal_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken CHARACTER_LITERAL63 = null;

        CommonTree CHARACTER_LITERAL63_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 20) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:230:2: ( CHARACTER_LITERAL )
            // Ada95.g:230:4: CHARACTER_LITERAL
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	CHARACTER_LITERAL63=(IToken)Match(input,CHARACTER_LITERAL,FOLLOW_CHARACTER_LITERAL_in_defining_character_literal758); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{CHARACTER_LITERAL63_tree = (CommonTree)adaptor.Create(CHARACTER_LITERAL63);
            		adaptor.AddChild(root_0, CHARACTER_LITERAL63_tree);
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 20, defining_character_literal_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "defining_character_literal"

    public class integer_type_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "integer_type_definition"
    // Ada95.g:232:1: integer_type_definition : ( signed_integer_type_definition | modular_type_definition );
    public Ada95Parser.integer_type_definition_return integer_type_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.integer_type_definition_return retval = new Ada95Parser.integer_type_definition_return();
        retval.Start = input.LT(1);
        int integer_type_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.signed_integer_type_definition_return signed_integer_type_definition64 = default(Ada95Parser.signed_integer_type_definition_return);

        Ada95Parser.modular_type_definition_return modular_type_definition65 = default(Ada95Parser.modular_type_definition_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 21) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:233:2: ( signed_integer_type_definition | modular_type_definition )
            int alt15 = 2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0 == RANGE) )
            {
                alt15 = 1;
            }
            else if ( (LA15_0 == MOD) )
            {
                alt15 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d15s0 =
                    new NoViableAltException("", 15, 0, input);

                throw nvae_d15s0;
            }
            switch (alt15) 
            {
                case 1 :
                    // Ada95.g:234:3: signed_integer_type_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_signed_integer_type_definition_in_integer_type_definition770);
                    	signed_integer_type_definition64 = signed_integer_type_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, signed_integer_type_definition64.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:235:3: modular_type_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_modular_type_definition_in_integer_type_definition776);
                    	modular_type_definition65 = modular_type_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, modular_type_definition65.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 21, integer_type_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "integer_type_definition"

    public class signed_integer_type_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "signed_integer_type_definition"
    // Ada95.g:238:1: signed_integer_type_definition : RANGE simple_expression DOTDOT simple_expression ;
    public Ada95Parser.signed_integer_type_definition_return signed_integer_type_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.signed_integer_type_definition_return retval = new Ada95Parser.signed_integer_type_definition_return();
        retval.Start = input.LT(1);
        int signed_integer_type_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken RANGE66 = null;
        IToken DOTDOT68 = null;
        Ada95Parser.simple_expression_return simple_expression67 = default(Ada95Parser.simple_expression_return);

        Ada95Parser.simple_expression_return simple_expression69 = default(Ada95Parser.simple_expression_return);


        CommonTree RANGE66_tree=null;
        CommonTree DOTDOT68_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 22) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:239:2: ( RANGE simple_expression DOTDOT simple_expression )
            // Ada95.g:239:4: RANGE simple_expression DOTDOT simple_expression
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	RANGE66=(IToken)Match(input,RANGE,FOLLOW_RANGE_in_signed_integer_type_definition787); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{RANGE66_tree = (CommonTree)adaptor.Create(RANGE66);
            		root_0 = (CommonTree)adaptor.BecomeRoot(RANGE66_tree, root_0);
            	}
            	PushFollow(FOLLOW_simple_expression_in_signed_integer_type_definition790);
            	simple_expression67 = simple_expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, simple_expression67.Tree);
            	DOTDOT68=(IToken)Match(input,DOTDOT,FOLLOW_DOTDOT_in_signed_integer_type_definition792); if (state.failed) return retval;
            	PushFollow(FOLLOW_simple_expression_in_signed_integer_type_definition795);
            	simple_expression69 = simple_expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, simple_expression69.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 22, signed_integer_type_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "signed_integer_type_definition"

    public class modular_type_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "modular_type_definition"
    // Ada95.g:241:1: modular_type_definition : MOD expression ;
    public Ada95Parser.modular_type_definition_return modular_type_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.modular_type_definition_return retval = new Ada95Parser.modular_type_definition_return();
        retval.Start = input.LT(1);
        int modular_type_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken MOD70 = null;
        Ada95Parser.expression_return expression71 = default(Ada95Parser.expression_return);


        CommonTree MOD70_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 23) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:242:2: ( MOD expression )
            // Ada95.g:242:4: MOD expression
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	MOD70=(IToken)Match(input,MOD,FOLLOW_MOD_in_modular_type_definition805); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{MOD70_tree = (CommonTree)adaptor.Create(MOD70);
            		root_0 = (CommonTree)adaptor.BecomeRoot(MOD70_tree, root_0);
            	}
            	PushFollow(FOLLOW_expression_in_modular_type_definition808);
            	expression71 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression71.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 23, modular_type_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "modular_type_definition"

    public class real_type_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "real_type_definition"
    // Ada95.g:244:1: real_type_definition : ( floating_point_definition | fixed_point_definition );
    public Ada95Parser.real_type_definition_return real_type_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.real_type_definition_return retval = new Ada95Parser.real_type_definition_return();
        retval.Start = input.LT(1);
        int real_type_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.floating_point_definition_return floating_point_definition72 = default(Ada95Parser.floating_point_definition_return);

        Ada95Parser.fixed_point_definition_return fixed_point_definition73 = default(Ada95Parser.fixed_point_definition_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 24) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:245:2: ( floating_point_definition | fixed_point_definition )
            int alt16 = 2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0 == DIGITS) )
            {
                alt16 = 1;
            }
            else if ( (LA16_0 == DELTA) )
            {
                alt16 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d16s0 =
                    new NoViableAltException("", 16, 0, input);

                throw nvae_d16s0;
            }
            switch (alt16) 
            {
                case 1 :
                    // Ada95.g:246:3: floating_point_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_floating_point_definition_in_real_type_definition820);
                    	floating_point_definition72 = floating_point_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, floating_point_definition72.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:247:3: fixed_point_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_fixed_point_definition_in_real_type_definition826);
                    	fixed_point_definition73 = fixed_point_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, fixed_point_definition73.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 24, real_type_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "real_type_definition"

    public class floating_point_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "floating_point_definition"
    // Ada95.g:250:1: floating_point_definition : DIGITS expression ( real_range_specification )? ;
    public Ada95Parser.floating_point_definition_return floating_point_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.floating_point_definition_return retval = new Ada95Parser.floating_point_definition_return();
        retval.Start = input.LT(1);
        int floating_point_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken DIGITS74 = null;
        Ada95Parser.expression_return expression75 = default(Ada95Parser.expression_return);

        Ada95Parser.real_range_specification_return real_range_specification76 = default(Ada95Parser.real_range_specification_return);


        CommonTree DIGITS74_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 25) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:251:2: ( DIGITS expression ( real_range_specification )? )
            // Ada95.g:251:4: DIGITS expression ( real_range_specification )?
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	DIGITS74=(IToken)Match(input,DIGITS,FOLLOW_DIGITS_in_floating_point_definition837); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{DIGITS74_tree = (CommonTree)adaptor.Create(DIGITS74);
            		root_0 = (CommonTree)adaptor.BecomeRoot(DIGITS74_tree, root_0);
            	}
            	PushFollow(FOLLOW_expression_in_floating_point_definition840);
            	expression75 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression75.Tree);
            	// Ada95.g:251:23: ( real_range_specification )?
            	int alt17 = 2;
            	int LA17_0 = input.LA(1);

            	if ( (LA17_0 == RANGE) )
            	{
            	    alt17 = 1;
            	}
            	switch (alt17) 
            	{
            	    case 1 :
            	        // Ada95.g:251:23: real_range_specification
            	        {
            	        	PushFollow(FOLLOW_real_range_specification_in_floating_point_definition842);
            	        	real_range_specification76 = real_range_specification();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, real_range_specification76.Tree);

            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 25, floating_point_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "floating_point_definition"

    public class real_range_specification_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "real_range_specification"
    // Ada95.g:253:1: real_range_specification : RANGE simple_expression DOTDOT simple_expression ;
    public Ada95Parser.real_range_specification_return real_range_specification() // throws RecognitionException [1]
    {   
        Ada95Parser.real_range_specification_return retval = new Ada95Parser.real_range_specification_return();
        retval.Start = input.LT(1);
        int real_range_specification_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken RANGE77 = null;
        IToken DOTDOT79 = null;
        Ada95Parser.simple_expression_return simple_expression78 = default(Ada95Parser.simple_expression_return);

        Ada95Parser.simple_expression_return simple_expression80 = default(Ada95Parser.simple_expression_return);


        CommonTree RANGE77_tree=null;
        CommonTree DOTDOT79_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 26) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:254:2: ( RANGE simple_expression DOTDOT simple_expression )
            // Ada95.g:254:4: RANGE simple_expression DOTDOT simple_expression
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	RANGE77=(IToken)Match(input,RANGE,FOLLOW_RANGE_in_real_range_specification853); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{RANGE77_tree = (CommonTree)adaptor.Create(RANGE77);
            		root_0 = (CommonTree)adaptor.BecomeRoot(RANGE77_tree, root_0);
            	}
            	PushFollow(FOLLOW_simple_expression_in_real_range_specification856);
            	simple_expression78 = simple_expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, simple_expression78.Tree);
            	DOTDOT79=(IToken)Match(input,DOTDOT,FOLLOW_DOTDOT_in_real_range_specification858); if (state.failed) return retval;
            	PushFollow(FOLLOW_simple_expression_in_real_range_specification861);
            	simple_expression80 = simple_expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, simple_expression80.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 26, real_range_specification_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "real_range_specification"

    public class fixed_point_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "fixed_point_definition"
    // Ada95.g:256:1: fixed_point_definition : ( ( ordinary_fixed_point_definition )=> ordinary_fixed_point_definition | decimal_fixed_point_definition );
    public Ada95Parser.fixed_point_definition_return fixed_point_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.fixed_point_definition_return retval = new Ada95Parser.fixed_point_definition_return();
        retval.Start = input.LT(1);
        int fixed_point_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.ordinary_fixed_point_definition_return ordinary_fixed_point_definition81 = default(Ada95Parser.ordinary_fixed_point_definition_return);

        Ada95Parser.decimal_fixed_point_definition_return decimal_fixed_point_definition82 = default(Ada95Parser.decimal_fixed_point_definition_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 27) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:257:2: ( ( ordinary_fixed_point_definition )=> ordinary_fixed_point_definition | decimal_fixed_point_definition )
            int alt18 = 2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0 == DELTA) )
            {
                int LA18_1 = input.LA(2);

                if ( (synpred1_Ada95()) )
                {
                    alt18 = 1;
                }
                else if ( (true) )
                {
                    alt18 = 2;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d18s1 =
                        new NoViableAltException("", 18, 1, input);

                    throw nvae_d18s1;
                }
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d18s0 =
                    new NoViableAltException("", 18, 0, input);

                throw nvae_d18s0;
            }
            switch (alt18) 
            {
                case 1 :
                    // Ada95.g:258:3: ( ordinary_fixed_point_definition )=> ordinary_fixed_point_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_ordinary_fixed_point_definition_in_fixed_point_definition880);
                    	ordinary_fixed_point_definition81 = ordinary_fixed_point_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, ordinary_fixed_point_definition81.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:259:13: decimal_fixed_point_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_decimal_fixed_point_definition_in_fixed_point_definition896);
                    	decimal_fixed_point_definition82 = decimal_fixed_point_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, decimal_fixed_point_definition82.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 27, fixed_point_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "fixed_point_definition"

    public class ordinary_fixed_point_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "ordinary_fixed_point_definition"
    // Ada95.g:262:1: ordinary_fixed_point_definition : DELTA expression real_range_specification -> ^( ORDINARY_FIXED expression real_range_specification ) ;
    public Ada95Parser.ordinary_fixed_point_definition_return ordinary_fixed_point_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.ordinary_fixed_point_definition_return retval = new Ada95Parser.ordinary_fixed_point_definition_return();
        retval.Start = input.LT(1);
        int ordinary_fixed_point_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken DELTA83 = null;
        Ada95Parser.expression_return expression84 = default(Ada95Parser.expression_return);

        Ada95Parser.real_range_specification_return real_range_specification85 = default(Ada95Parser.real_range_specification_return);


        CommonTree DELTA83_tree=null;
        RewriteRuleTokenStream stream_DELTA = new RewriteRuleTokenStream(adaptor,"token DELTA");
        RewriteRuleSubtreeStream stream_expression = new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_real_range_specification = new RewriteRuleSubtreeStream(adaptor,"rule real_range_specification");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 28) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:263:2: ( DELTA expression real_range_specification -> ^( ORDINARY_FIXED expression real_range_specification ) )
            // Ada95.g:263:4: DELTA expression real_range_specification
            {
            	DELTA83=(IToken)Match(input,DELTA,FOLLOW_DELTA_in_ordinary_fixed_point_definition907); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_DELTA.Add(DELTA83);

            	PushFollow(FOLLOW_expression_in_ordinary_fixed_point_definition909);
            	expression84 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_expression.Add(expression84.Tree);
            	PushFollow(FOLLOW_real_range_specification_in_ordinary_fixed_point_definition911);
            	real_range_specification85 = real_range_specification();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_real_range_specification.Add(real_range_specification85.Tree);


            	// AST REWRITE
            	// elements:          expression, real_range_specification
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 263:46: -> ^( ORDINARY_FIXED expression real_range_specification )
            	{
            	    // Ada95.g:263:49: ^( ORDINARY_FIXED expression real_range_specification )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(ORDINARY_FIXED, "ORDINARY_FIXED"), root_1);

            	    adaptor.AddChild(root_1, stream_expression.NextTree());
            	    adaptor.AddChild(root_1, stream_real_range_specification.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 28, ordinary_fixed_point_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "ordinary_fixed_point_definition"

    public class decimal_fixed_point_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "decimal_fixed_point_definition"
    // Ada95.g:265:1: decimal_fixed_point_definition : DELTA expression DIGITS expression ( real_range_specification )? -> ^( DECIMAL_FIXED expression expression ( real_range_specification )? ) ;
    public Ada95Parser.decimal_fixed_point_definition_return decimal_fixed_point_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.decimal_fixed_point_definition_return retval = new Ada95Parser.decimal_fixed_point_definition_return();
        retval.Start = input.LT(1);
        int decimal_fixed_point_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken DELTA86 = null;
        IToken DIGITS88 = null;
        Ada95Parser.expression_return expression87 = default(Ada95Parser.expression_return);

        Ada95Parser.expression_return expression89 = default(Ada95Parser.expression_return);

        Ada95Parser.real_range_specification_return real_range_specification90 = default(Ada95Parser.real_range_specification_return);


        CommonTree DELTA86_tree=null;
        CommonTree DIGITS88_tree=null;
        RewriteRuleTokenStream stream_DELTA = new RewriteRuleTokenStream(adaptor,"token DELTA");
        RewriteRuleTokenStream stream_DIGITS = new RewriteRuleTokenStream(adaptor,"token DIGITS");
        RewriteRuleSubtreeStream stream_expression = new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_real_range_specification = new RewriteRuleSubtreeStream(adaptor,"rule real_range_specification");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 29) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:266:2: ( DELTA expression DIGITS expression ( real_range_specification )? -> ^( DECIMAL_FIXED expression expression ( real_range_specification )? ) )
            // Ada95.g:266:4: DELTA expression DIGITS expression ( real_range_specification )?
            {
            	DELTA86=(IToken)Match(input,DELTA,FOLLOW_DELTA_in_decimal_fixed_point_definition933); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_DELTA.Add(DELTA86);

            	PushFollow(FOLLOW_expression_in_decimal_fixed_point_definition935);
            	expression87 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_expression.Add(expression87.Tree);
            	DIGITS88=(IToken)Match(input,DIGITS,FOLLOW_DIGITS_in_decimal_fixed_point_definition937); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_DIGITS.Add(DIGITS88);

            	PushFollow(FOLLOW_expression_in_decimal_fixed_point_definition939);
            	expression89 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_expression.Add(expression89.Tree);
            	// Ada95.g:266:39: ( real_range_specification )?
            	int alt19 = 2;
            	int LA19_0 = input.LA(1);

            	if ( (LA19_0 == RANGE) )
            	{
            	    alt19 = 1;
            	}
            	switch (alt19) 
            	{
            	    case 1 :
            	        // Ada95.g:266:39: real_range_specification
            	        {
            	        	PushFollow(FOLLOW_real_range_specification_in_decimal_fixed_point_definition941);
            	        	real_range_specification90 = real_range_specification();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_real_range_specification.Add(real_range_specification90.Tree);

            	        }
            	        break;

            	}



            	// AST REWRITE
            	// elements:          expression, real_range_specification, expression
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 266:65: -> ^( DECIMAL_FIXED expression expression ( real_range_specification )? )
            	{
            	    // Ada95.g:266:68: ^( DECIMAL_FIXED expression expression ( real_range_specification )? )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(DECIMAL_FIXED, "DECIMAL_FIXED"), root_1);

            	    adaptor.AddChild(root_1, stream_expression.NextTree());
            	    adaptor.AddChild(root_1, stream_expression.NextTree());
            	    // Ada95.g:266:107: ( real_range_specification )?
            	    if ( stream_real_range_specification.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_real_range_specification.NextTree());

            	    }
            	    stream_real_range_specification.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 29, decimal_fixed_point_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "decimal_fixed_point_definition"

    public class digits_constraint_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "digits_constraint"
    // Ada95.g:268:1: digits_constraint : DIGITS expression ( range_constraint )? ;
    public Ada95Parser.digits_constraint_return digits_constraint() // throws RecognitionException [1]
    {   
        Ada95Parser.digits_constraint_return retval = new Ada95Parser.digits_constraint_return();
        retval.Start = input.LT(1);
        int digits_constraint_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken DIGITS91 = null;
        Ada95Parser.expression_return expression92 = default(Ada95Parser.expression_return);

        Ada95Parser.range_constraint_return range_constraint93 = default(Ada95Parser.range_constraint_return);


        CommonTree DIGITS91_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 30) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:269:2: ( DIGITS expression ( range_constraint )? )
            // Ada95.g:269:4: DIGITS expression ( range_constraint )?
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	DIGITS91=(IToken)Match(input,DIGITS,FOLLOW_DIGITS_in_digits_constraint967); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{DIGITS91_tree = (CommonTree)adaptor.Create(DIGITS91);
            		root_0 = (CommonTree)adaptor.BecomeRoot(DIGITS91_tree, root_0);
            	}
            	PushFollow(FOLLOW_expression_in_digits_constraint970);
            	expression92 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression92.Tree);
            	// Ada95.g:269:23: ( range_constraint )?
            	int alt20 = 2;
            	int LA20_0 = input.LA(1);

            	if ( (LA20_0 == RANGE) )
            	{
            	    alt20 = 1;
            	}
            	switch (alt20) 
            	{
            	    case 1 :
            	        // Ada95.g:269:23: range_constraint
            	        {
            	        	PushFollow(FOLLOW_range_constraint_in_digits_constraint972);
            	        	range_constraint93 = range_constraint();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, range_constraint93.Tree);

            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 30, digits_constraint_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "digits_constraint"

    public class array_type_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "array_type_definition"
    // Ada95.g:271:1: array_type_definition : ( unconstrained_array_definition | constrained_array_definition );
    public Ada95Parser.array_type_definition_return array_type_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.array_type_definition_return retval = new Ada95Parser.array_type_definition_return();
        retval.Start = input.LT(1);
        int array_type_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.unconstrained_array_definition_return unconstrained_array_definition94 = default(Ada95Parser.unconstrained_array_definition_return);

        Ada95Parser.constrained_array_definition_return constrained_array_definition95 = default(Ada95Parser.constrained_array_definition_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 31) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:272:2: ( unconstrained_array_definition | constrained_array_definition )
            int alt21 = 2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0 == ARRAY) )
            {
                int LA21_1 = input.LA(2);

                if ( (LA21_1 == LPARANTHESIS) )
                {
                    int LA21_2 = input.LA(3);

                    if ( (LA21_2 == IDENTIFIER) )
                    {
                        int LA21_3 = input.LA(4);

                        if ( (LA21_3 == COMMA || (LA21_3 >= DOTDOT && LA21_3 <= RPARANTHESIS) || (LA21_3 >= MOD && LA21_3 <= DIGITS) || LA21_3 == DOT || LA21_3 == APOSTROPHE || LA21_3 == POW || (LA21_3 >= PLUS && LA21_3 <= REM)) )
                        {
                            alt21 = 2;
                        }
                        else if ( (LA21_3 == RANGE) )
                        {
                            int LA21_5 = input.LA(5);

                            if ( (LA21_5 == BOX) )
                            {
                                alt21 = 1;
                            }
                            else if ( (LA21_5 == IDENTIFIER || LA21_5 == LPARANTHESIS || LA21_5 == CHARACTER_LITERAL || LA21_5 == NULL || LA21_5 == NOT || (LA21_5 >= ABS && LA21_5 <= STRING_LITERAL) || (LA21_5 >= PLUS && LA21_5 <= MINUS)) )
                            {
                                alt21 = 2;
                            }
                            else 
                            {
                                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                                NoViableAltException nvae_d21s5 =
                                    new NoViableAltException("", 21, 5, input);

                                throw nvae_d21s5;
                            }
                        }
                        else 
                        {
                            if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                            NoViableAltException nvae_d21s3 =
                                new NoViableAltException("", 21, 3, input);

                            throw nvae_d21s3;
                        }
                    }
                    else if ( (LA21_2 == LPARANTHESIS || LA21_2 == CHARACTER_LITERAL || LA21_2 == NULL || LA21_2 == NOT || (LA21_2 >= ABS && LA21_2 <= STRING_LITERAL) || (LA21_2 >= PLUS && LA21_2 <= MINUS)) )
                    {
                        alt21 = 2;
                    }
                    else 
                    {
                        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                        NoViableAltException nvae_d21s2 =
                            new NoViableAltException("", 21, 2, input);

                        throw nvae_d21s2;
                    }
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d21s1 =
                        new NoViableAltException("", 21, 1, input);

                    throw nvae_d21s1;
                }
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d21s0 =
                    new NoViableAltException("", 21, 0, input);

                throw nvae_d21s0;
            }
            switch (alt21) 
            {
                case 1 :
                    // Ada95.g:273:3: unconstrained_array_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_unconstrained_array_definition_in_array_type_definition985);
                    	unconstrained_array_definition94 = unconstrained_array_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, unconstrained_array_definition94.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:274:3: constrained_array_definition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_constrained_array_definition_in_array_type_definition991);
                    	constrained_array_definition95 = constrained_array_definition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, constrained_array_definition95.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 31, array_type_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "array_type_definition"

    public class unconstrained_array_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "unconstrained_array_definition"
    // Ada95.g:277:1: unconstrained_array_definition : ARRAY LPARANTHESIS index_subtype_definition ( COMMA index_subtype_definition )* RPARANTHESIS OF component_definition -> ^( UNCONSTRAINED_ARRAY ( index_subtype_definition )+ OF component_definition ) ;
    public Ada95Parser.unconstrained_array_definition_return unconstrained_array_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.unconstrained_array_definition_return retval = new Ada95Parser.unconstrained_array_definition_return();
        retval.Start = input.LT(1);
        int unconstrained_array_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken ARRAY96 = null;
        IToken LPARANTHESIS97 = null;
        IToken COMMA99 = null;
        IToken RPARANTHESIS101 = null;
        IToken OF102 = null;
        Ada95Parser.index_subtype_definition_return index_subtype_definition98 = default(Ada95Parser.index_subtype_definition_return);

        Ada95Parser.index_subtype_definition_return index_subtype_definition100 = default(Ada95Parser.index_subtype_definition_return);

        Ada95Parser.component_definition_return component_definition103 = default(Ada95Parser.component_definition_return);


        CommonTree ARRAY96_tree=null;
        CommonTree LPARANTHESIS97_tree=null;
        CommonTree COMMA99_tree=null;
        CommonTree RPARANTHESIS101_tree=null;
        CommonTree OF102_tree=null;
        RewriteRuleTokenStream stream_LPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token LPARANTHESIS");
        RewriteRuleTokenStream stream_RPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token RPARANTHESIS");
        RewriteRuleTokenStream stream_COMMA = new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleTokenStream stream_OF = new RewriteRuleTokenStream(adaptor,"token OF");
        RewriteRuleTokenStream stream_ARRAY = new RewriteRuleTokenStream(adaptor,"token ARRAY");
        RewriteRuleSubtreeStream stream_index_subtype_definition = new RewriteRuleSubtreeStream(adaptor,"rule index_subtype_definition");
        RewriteRuleSubtreeStream stream_component_definition = new RewriteRuleSubtreeStream(adaptor,"rule component_definition");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 32) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:278:2: ( ARRAY LPARANTHESIS index_subtype_definition ( COMMA index_subtype_definition )* RPARANTHESIS OF component_definition -> ^( UNCONSTRAINED_ARRAY ( index_subtype_definition )+ OF component_definition ) )
            // Ada95.g:279:3: ARRAY LPARANTHESIS index_subtype_definition ( COMMA index_subtype_definition )* RPARANTHESIS OF component_definition
            {
            	ARRAY96=(IToken)Match(input,ARRAY,FOLLOW_ARRAY_in_unconstrained_array_definition1004); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_ARRAY.Add(ARRAY96);

            	LPARANTHESIS97=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_unconstrained_array_definition1006); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_LPARANTHESIS.Add(LPARANTHESIS97);

            	PushFollow(FOLLOW_index_subtype_definition_in_unconstrained_array_definition1008);
            	index_subtype_definition98 = index_subtype_definition();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_index_subtype_definition.Add(index_subtype_definition98.Tree);
            	// Ada95.g:279:47: ( COMMA index_subtype_definition )*
            	do 
            	{
            	    int alt22 = 2;
            	    int LA22_0 = input.LA(1);

            	    if ( (LA22_0 == COMMA) )
            	    {
            	        alt22 = 1;
            	    }


            	    switch (alt22) 
            		{
            			case 1 :
            			    // Ada95.g:279:49: COMMA index_subtype_definition
            			    {
            			    	COMMA99=(IToken)Match(input,COMMA,FOLLOW_COMMA_in_unconstrained_array_definition1012); if (state.failed) return retval; 
            			    	if ( (state.backtracking==0) ) stream_COMMA.Add(COMMA99);

            			    	PushFollow(FOLLOW_index_subtype_definition_in_unconstrained_array_definition1014);
            			    	index_subtype_definition100 = index_subtype_definition();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( (state.backtracking==0) ) stream_index_subtype_definition.Add(index_subtype_definition100.Tree);

            			    }
            			    break;

            			default:
            			    goto loop22;
            	    }
            	} while (true);

            	loop22:
            		;	// Stops C# compiler whining that label 'loop22' has no statements

            	RPARANTHESIS101=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_unconstrained_array_definition1019); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_RPARANTHESIS.Add(RPARANTHESIS101);

            	OF102=(IToken)Match(input,OF,FOLLOW_OF_in_unconstrained_array_definition1021); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_OF.Add(OF102);

            	PushFollow(FOLLOW_component_definition_in_unconstrained_array_definition1023);
            	component_definition103 = component_definition();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_component_definition.Add(component_definition103.Tree);


            	// AST REWRITE
            	// elements:          OF, component_definition, index_subtype_definition
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 280:4: -> ^( UNCONSTRAINED_ARRAY ( index_subtype_definition )+ OF component_definition )
            	{
            	    // Ada95.g:280:7: ^( UNCONSTRAINED_ARRAY ( index_subtype_definition )+ OF component_definition )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(UNCONSTRAINED_ARRAY, "UNCONSTRAINED_ARRAY"), root_1);

            	    if ( !(stream_index_subtype_definition.HasNext()) ) {
            	        throw new RewriteEarlyExitException();
            	    }
            	    while ( stream_index_subtype_definition.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_index_subtype_definition.NextTree());

            	    }
            	    stream_index_subtype_definition.Reset();
            	    adaptor.AddChild(root_1, stream_OF.NextNode());
            	    adaptor.AddChild(root_1, stream_component_definition.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 32, unconstrained_array_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "unconstrained_array_definition"

    public class index_subtype_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "index_subtype_definition"
    // Ada95.g:283:1: index_subtype_definition : subtype_mark RANGE BOX ;
    public Ada95Parser.index_subtype_definition_return index_subtype_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.index_subtype_definition_return retval = new Ada95Parser.index_subtype_definition_return();
        retval.Start = input.LT(1);
        int index_subtype_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken RANGE105 = null;
        IToken BOX106 = null;
        Ada95Parser.subtype_mark_return subtype_mark104 = default(Ada95Parser.subtype_mark_return);


        CommonTree RANGE105_tree=null;
        CommonTree BOX106_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 33) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:284:2: ( subtype_mark RANGE BOX )
            // Ada95.g:284:4: subtype_mark RANGE BOX
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_subtype_mark_in_index_subtype_definition1052);
            	subtype_mark104 = subtype_mark();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subtype_mark104.Tree);
            	RANGE105=(IToken)Match(input,RANGE,FOLLOW_RANGE_in_index_subtype_definition1054); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{RANGE105_tree = (CommonTree)adaptor.Create(RANGE105);
            		adaptor.AddChild(root_0, RANGE105_tree);
            	}
            	BOX106=(IToken)Match(input,BOX,FOLLOW_BOX_in_index_subtype_definition1056); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{BOX106_tree = (CommonTree)adaptor.Create(BOX106);
            		adaptor.AddChild(root_0, BOX106_tree);
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 33, index_subtype_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "index_subtype_definition"

    public class constrained_array_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "constrained_array_definition"
    // Ada95.g:286:1: constrained_array_definition : ARRAY LPARANTHESIS discrete_subtype_definition ( COMMA discrete_subtype_definition )* RPARANTHESIS OF component_definition -> ^( CONSTRAINED_ARRAY ( discrete_subtype_definition )+ OF component_definition ) ;
    public Ada95Parser.constrained_array_definition_return constrained_array_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.constrained_array_definition_return retval = new Ada95Parser.constrained_array_definition_return();
        retval.Start = input.LT(1);
        int constrained_array_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken ARRAY107 = null;
        IToken LPARANTHESIS108 = null;
        IToken COMMA110 = null;
        IToken RPARANTHESIS112 = null;
        IToken OF113 = null;
        Ada95Parser.discrete_subtype_definition_return discrete_subtype_definition109 = default(Ada95Parser.discrete_subtype_definition_return);

        Ada95Parser.discrete_subtype_definition_return discrete_subtype_definition111 = default(Ada95Parser.discrete_subtype_definition_return);

        Ada95Parser.component_definition_return component_definition114 = default(Ada95Parser.component_definition_return);


        CommonTree ARRAY107_tree=null;
        CommonTree LPARANTHESIS108_tree=null;
        CommonTree COMMA110_tree=null;
        CommonTree RPARANTHESIS112_tree=null;
        CommonTree OF113_tree=null;
        RewriteRuleTokenStream stream_LPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token LPARANTHESIS");
        RewriteRuleTokenStream stream_RPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token RPARANTHESIS");
        RewriteRuleTokenStream stream_COMMA = new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleTokenStream stream_OF = new RewriteRuleTokenStream(adaptor,"token OF");
        RewriteRuleTokenStream stream_ARRAY = new RewriteRuleTokenStream(adaptor,"token ARRAY");
        RewriteRuleSubtreeStream stream_discrete_subtype_definition = new RewriteRuleSubtreeStream(adaptor,"rule discrete_subtype_definition");
        RewriteRuleSubtreeStream stream_component_definition = new RewriteRuleSubtreeStream(adaptor,"rule component_definition");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 34) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:287:2: ( ARRAY LPARANTHESIS discrete_subtype_definition ( COMMA discrete_subtype_definition )* RPARANTHESIS OF component_definition -> ^( CONSTRAINED_ARRAY ( discrete_subtype_definition )+ OF component_definition ) )
            // Ada95.g:288:3: ARRAY LPARANTHESIS discrete_subtype_definition ( COMMA discrete_subtype_definition )* RPARANTHESIS OF component_definition
            {
            	ARRAY107=(IToken)Match(input,ARRAY,FOLLOW_ARRAY_in_constrained_array_definition1068); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_ARRAY.Add(ARRAY107);

            	LPARANTHESIS108=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_constrained_array_definition1070); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_LPARANTHESIS.Add(LPARANTHESIS108);

            	PushFollow(FOLLOW_discrete_subtype_definition_in_constrained_array_definition1072);
            	discrete_subtype_definition109 = discrete_subtype_definition();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_discrete_subtype_definition.Add(discrete_subtype_definition109.Tree);
            	// Ada95.g:288:50: ( COMMA discrete_subtype_definition )*
            	do 
            	{
            	    int alt23 = 2;
            	    int LA23_0 = input.LA(1);

            	    if ( (LA23_0 == COMMA) )
            	    {
            	        alt23 = 1;
            	    }


            	    switch (alt23) 
            		{
            			case 1 :
            			    // Ada95.g:288:52: COMMA discrete_subtype_definition
            			    {
            			    	COMMA110=(IToken)Match(input,COMMA,FOLLOW_COMMA_in_constrained_array_definition1076); if (state.failed) return retval; 
            			    	if ( (state.backtracking==0) ) stream_COMMA.Add(COMMA110);

            			    	PushFollow(FOLLOW_discrete_subtype_definition_in_constrained_array_definition1078);
            			    	discrete_subtype_definition111 = discrete_subtype_definition();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( (state.backtracking==0) ) stream_discrete_subtype_definition.Add(discrete_subtype_definition111.Tree);

            			    }
            			    break;

            			default:
            			    goto loop23;
            	    }
            	} while (true);

            	loop23:
            		;	// Stops C# compiler whining that label 'loop23' has no statements

            	RPARANTHESIS112=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_constrained_array_definition1083); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_RPARANTHESIS.Add(RPARANTHESIS112);

            	OF113=(IToken)Match(input,OF,FOLLOW_OF_in_constrained_array_definition1085); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_OF.Add(OF113);

            	PushFollow(FOLLOW_component_definition_in_constrained_array_definition1087);
            	component_definition114 = component_definition();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_component_definition.Add(component_definition114.Tree);


            	// AST REWRITE
            	// elements:          component_definition, OF, discrete_subtype_definition
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 289:4: -> ^( CONSTRAINED_ARRAY ( discrete_subtype_definition )+ OF component_definition )
            	{
            	    // Ada95.g:289:7: ^( CONSTRAINED_ARRAY ( discrete_subtype_definition )+ OF component_definition )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(CONSTRAINED_ARRAY, "CONSTRAINED_ARRAY"), root_1);

            	    if ( !(stream_discrete_subtype_definition.HasNext()) ) {
            	        throw new RewriteEarlyExitException();
            	    }
            	    while ( stream_discrete_subtype_definition.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_discrete_subtype_definition.NextTree());

            	    }
            	    stream_discrete_subtype_definition.Reset();
            	    adaptor.AddChild(root_1, stream_OF.NextNode());
            	    adaptor.AddChild(root_1, stream_component_definition.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 34, constrained_array_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "constrained_array_definition"

    public class discrete_subtype_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "discrete_subtype_definition"
    // Ada95.g:292:1: discrete_subtype_definition : ( subtype_indication | range );
    public Ada95Parser.discrete_subtype_definition_return discrete_subtype_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.discrete_subtype_definition_return retval = new Ada95Parser.discrete_subtype_definition_return();
        retval.Start = input.LT(1);
        int discrete_subtype_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.subtype_indication_return subtype_indication115 = default(Ada95Parser.subtype_indication_return);

        Ada95Parser.range_return range116 = default(Ada95Parser.range_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 35) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:293:2: ( subtype_indication | range )
            int alt24 = 2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0 == IDENTIFIER) )
            {
                int LA24_1 = input.LA(2);

                if ( ((LA24_1 >= DOTDOT && LA24_1 <= LPARANTHESIS) || LA24_1 == MOD || LA24_1 == DOT || LA24_1 == APOSTROPHE || LA24_1 == POW || (LA24_1 >= PLUS && LA24_1 <= REM)) )
                {
                    alt24 = 2;
                }
                else if ( ((LA24_1 >= COMMA && LA24_1 <= RANGE) || LA24_1 == RPARANTHESIS || LA24_1 == DIGITS || LA24_1 == LOOP) )
                {
                    alt24 = 1;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d24s1 =
                        new NoViableAltException("", 24, 1, input);

                    throw nvae_d24s1;
                }
            }
            else if ( (LA24_0 == LPARANTHESIS || LA24_0 == CHARACTER_LITERAL || LA24_0 == NULL || LA24_0 == NOT || (LA24_0 >= ABS && LA24_0 <= STRING_LITERAL) || (LA24_0 >= PLUS && LA24_0 <= MINUS)) )
            {
                alt24 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d24s0 =
                    new NoViableAltException("", 24, 0, input);

                throw nvae_d24s0;
            }
            switch (alt24) 
            {
                case 1 :
                    // Ada95.g:294:3: subtype_indication
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_subtype_indication_in_discrete_subtype_definition1118);
                    	subtype_indication115 = subtype_indication();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subtype_indication115.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:295:3: range
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_range_in_discrete_subtype_definition1125);
                    	range116 = range();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, range116.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 35, discrete_subtype_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "discrete_subtype_definition"

    public class component_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "component_definition"
    // Ada95.g:298:1: component_definition : ( ALIASED )? subtype_indication ;
    public Ada95Parser.component_definition_return component_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.component_definition_return retval = new Ada95Parser.component_definition_return();
        retval.Start = input.LT(1);
        int component_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken ALIASED117 = null;
        Ada95Parser.subtype_indication_return subtype_indication118 = default(Ada95Parser.subtype_indication_return);


        CommonTree ALIASED117_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 36) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:299:2: ( ( ALIASED )? subtype_indication )
            // Ada95.g:299:4: ( ALIASED )? subtype_indication
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	// Ada95.g:299:4: ( ALIASED )?
            	int alt25 = 2;
            	int LA25_0 = input.LA(1);

            	if ( (LA25_0 == ALIASED) )
            	{
            	    alt25 = 1;
            	}
            	switch (alt25) 
            	{
            	    case 1 :
            	        // Ada95.g:299:4: ALIASED
            	        {
            	        	ALIASED117=(IToken)Match(input,ALIASED,FOLLOW_ALIASED_in_component_definition1136); if (state.failed) return retval;
            	        	if ( state.backtracking == 0 )
            	        	{ALIASED117_tree = (CommonTree)adaptor.Create(ALIASED117);
            	        		adaptor.AddChild(root_0, ALIASED117_tree);
            	        	}

            	        }
            	        break;

            	}

            	PushFollow(FOLLOW_subtype_indication_in_component_definition1139);
            	subtype_indication118 = subtype_indication();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subtype_indication118.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 36, component_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "component_definition"

    public class discrete_range_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "discrete_range"
    // Ada95.g:303:1: discrete_range : ({...}? => subtype_indication | range );
    public Ada95Parser.discrete_range_return discrete_range() // throws RecognitionException [1]
    {   
        Ada95Parser.discrete_range_return retval = new Ada95Parser.discrete_range_return();
        retval.Start = input.LT(1);
        int discrete_range_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.subtype_indication_return subtype_indication119 = default(Ada95Parser.subtype_indication_return);

        Ada95Parser.range_return range120 = default(Ada95Parser.range_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 37) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:304:2: ({...}? => subtype_indication | range )
            int alt26 = 2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0 == IDENTIFIER) )
            {
                int LA26_1 = input.LA(2);

                if ( ((LA26_1 >= DOTDOT && LA26_1 <= LPARANTHESIS) || LA26_1 == MOD || LA26_1 == DOT || LA26_1 == APOSTROPHE || LA26_1 == POW || (LA26_1 >= PLUS && LA26_1 <= REM)) )
                {
                    alt26 = 2;
                }
                else if ( (LA26_1 == EOF || LA26_1 == RANGE || LA26_1 == RPARANTHESIS || LA26_1 == DIGITS || LA26_1 == ASSOCIATION || LA26_1 == CHOICE) && (( IsType( input.LT( 1 ).Text ) )) )
                {
                    alt26 = 1;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d26s1 =
                        new NoViableAltException("", 26, 1, input);

                    throw nvae_d26s1;
                }
            }
            else if ( (LA26_0 == LPARANTHESIS || LA26_0 == CHARACTER_LITERAL || LA26_0 == NULL || LA26_0 == NOT || (LA26_0 >= ABS && LA26_0 <= STRING_LITERAL) || (LA26_0 >= PLUS && LA26_0 <= MINUS)) )
            {
                alt26 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d26s0 =
                    new NoViableAltException("", 26, 0, input);

                throw nvae_d26s0;
            }
            switch (alt26) 
            {
                case 1 :
                    // Ada95.g:305:3: {...}? => subtype_indication
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	if ( !(( IsType( input.LT( 1 ).Text ) )) ) 
                    	{
                    	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    	    throw new FailedPredicateException(input, "discrete_range", " IsType( input.LT( 1 ).Text ) ");
                    	}
                    	PushFollow(FOLLOW_subtype_indication_in_discrete_range1156);
                    	subtype_indication119 = subtype_indication();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subtype_indication119.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:306:12: range
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_range_in_discrete_range1172);
                    	range120 = range();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, range120.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 37, discrete_range_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "discrete_range"

    public class known_discriminant_part_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "known_discriminant_part"
    // Ada95.g:309:1: known_discriminant_part : LPARANTHESIS discriminant_specification ( SEMICOLON discriminant_specification )* RPARANTHESIS ;
    public Ada95Parser.known_discriminant_part_return known_discriminant_part() // throws RecognitionException [1]
    {   
        Ada95Parser.known_discriminant_part_return retval = new Ada95Parser.known_discriminant_part_return();
        retval.Start = input.LT(1);
        int known_discriminant_part_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken LPARANTHESIS121 = null;
        IToken SEMICOLON123 = null;
        IToken RPARANTHESIS125 = null;
        Ada95Parser.discriminant_specification_return discriminant_specification122 = default(Ada95Parser.discriminant_specification_return);

        Ada95Parser.discriminant_specification_return discriminant_specification124 = default(Ada95Parser.discriminant_specification_return);


        CommonTree LPARANTHESIS121_tree=null;
        CommonTree SEMICOLON123_tree=null;
        CommonTree RPARANTHESIS125_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 38) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:310:2: ( LPARANTHESIS discriminant_specification ( SEMICOLON discriminant_specification )* RPARANTHESIS )
            // Ada95.g:310:4: LPARANTHESIS discriminant_specification ( SEMICOLON discriminant_specification )* RPARANTHESIS
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	LPARANTHESIS121=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_known_discriminant_part1183); if (state.failed) return retval;
            	PushFollow(FOLLOW_discriminant_specification_in_known_discriminant_part1186);
            	discriminant_specification122 = discriminant_specification();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, discriminant_specification122.Tree);
            	// Ada95.g:310:45: ( SEMICOLON discriminant_specification )*
            	do 
            	{
            	    int alt27 = 2;
            	    int LA27_0 = input.LA(1);

            	    if ( (LA27_0 == SEMICOLON) )
            	    {
            	        alt27 = 1;
            	    }


            	    switch (alt27) 
            		{
            			case 1 :
            			    // Ada95.g:310:47: SEMICOLON discriminant_specification
            			    {
            			    	SEMICOLON123=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_known_discriminant_part1190); if (state.failed) return retval;
            			    	PushFollow(FOLLOW_discriminant_specification_in_known_discriminant_part1193);
            			    	discriminant_specification124 = discriminant_specification();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, discriminant_specification124.Tree);

            			    }
            			    break;

            			default:
            			    goto loop27;
            	    }
            	} while (true);

            	loop27:
            		;	// Stops C# compiler whining that label 'loop27' has no statements

            	RPARANTHESIS125=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_known_discriminant_part1197); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 38, known_discriminant_part_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "known_discriminant_part"

    public class discriminant_specification_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "discriminant_specification"
    // Ada95.g:312:1: discriminant_specification : defining_identifier_list COLON ( subtype_mark ) ( ASSIGN default_expression )? -> ^( DISCRIMINANT_SPECIFICATION defining_identifier_list subtype_mark ( default_expression )? ) ;
    public Ada95Parser.discriminant_specification_return discriminant_specification() // throws RecognitionException [1]
    {   
        Symbols_stack.Push(new Symbols_scope());

        Ada95Parser.discriminant_specification_return retval = new Ada95Parser.discriminant_specification_return();
        retval.Start = input.LT(1);
        int discriminant_specification_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken COLON127 = null;
        IToken ASSIGN129 = null;
        Ada95Parser.defining_identifier_list_return defining_identifier_list126 = default(Ada95Parser.defining_identifier_list_return);

        Ada95Parser.subtype_mark_return subtype_mark128 = default(Ada95Parser.subtype_mark_return);

        Ada95Parser.default_expression_return default_expression130 = default(Ada95Parser.default_expression_return);


        CommonTree COLON127_tree=null;
        CommonTree ASSIGN129_tree=null;
        RewriteRuleTokenStream stream_COLON = new RewriteRuleTokenStream(adaptor,"token COLON");
        RewriteRuleTokenStream stream_ASSIGN = new RewriteRuleTokenStream(adaptor,"token ASSIGN");
        RewriteRuleSubtreeStream stream_default_expression = new RewriteRuleSubtreeStream(adaptor,"rule default_expression");
        RewriteRuleSubtreeStream stream_defining_identifier_list = new RewriteRuleSubtreeStream(adaptor,"rule defining_identifier_list");
        RewriteRuleSubtreeStream stream_subtype_mark = new RewriteRuleSubtreeStream(adaptor,"rule subtype_mark");

        		((Symbols_scope)Symbols_stack.Peek()).IDs =  new SortedSet< string >();
        	
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 39) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:318:2: ( defining_identifier_list COLON ( subtype_mark ) ( ASSIGN default_expression )? -> ^( DISCRIMINANT_SPECIFICATION defining_identifier_list subtype_mark ( default_expression )? ) )
            // Ada95.g:319:3: defining_identifier_list COLON ( subtype_mark ) ( ASSIGN default_expression )?
            {
            	PushFollow(FOLLOW_defining_identifier_list_in_discriminant_specification1223);
            	defining_identifier_list126 = defining_identifier_list();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_defining_identifier_list.Add(defining_identifier_list126.Tree);
            	COLON127=(IToken)Match(input,COLON,FOLLOW_COLON_in_discriminant_specification1225); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_COLON.Add(COLON127);

            	// Ada95.g:319:34: ( subtype_mark )
            	// Ada95.g:319:36: subtype_mark
            	{
            		PushFollow(FOLLOW_subtype_mark_in_discriminant_specification1229);
            		subtype_mark128 = subtype_mark();
            		state.followingStackPointer--;
            		if (state.failed) return retval;
            		if ( (state.backtracking==0) ) stream_subtype_mark.Add(subtype_mark128.Tree);

            	}

            	// Ada95.g:319:75: ( ASSIGN default_expression )?
            	int alt28 = 2;
            	int LA28_0 = input.LA(1);

            	if ( (LA28_0 == ASSIGN) )
            	{
            	    alt28 = 1;
            	}
            	switch (alt28) 
            	{
            	    case 1 :
            	        // Ada95.g:319:77: ASSIGN default_expression
            	        {
            	        	ASSIGN129=(IToken)Match(input,ASSIGN,FOLLOW_ASSIGN_in_discriminant_specification1236); if (state.failed) return retval; 
            	        	if ( (state.backtracking==0) ) stream_ASSIGN.Add(ASSIGN129);

            	        	PushFollow(FOLLOW_default_expression_in_discriminant_specification1238);
            	        	default_expression130 = default_expression();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_default_expression.Add(default_expression130.Tree);

            	        }
            	        break;

            	}

            	if ( (state.backtracking==0) )
            	{
            	   ((SymbolTable_scope)SymbolTable_stack.Peek()).Objects.UnionWith( ((Symbols_scope)Symbols_stack.Peek()).IDs ); 
            	}


            	// AST REWRITE
            	// elements:          default_expression, subtype_mark, defining_identifier_list
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 321:4: -> ^( DISCRIMINANT_SPECIFICATION defining_identifier_list subtype_mark ( default_expression )? )
            	{
            	    // Ada95.g:321:7: ^( DISCRIMINANT_SPECIFICATION defining_identifier_list subtype_mark ( default_expression )? )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(DISCRIMINANT_SPECIFICATION, "DISCRIMINANT_SPECIFICATION"), root_1);

            	    adaptor.AddChild(root_1, stream_defining_identifier_list.NextTree());
            	    adaptor.AddChild(root_1, stream_subtype_mark.NextTree());
            	    // Ada95.g:321:75: ( default_expression )?
            	    if ( stream_default_expression.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_default_expression.NextTree());

            	    }
            	    stream_default_expression.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 39, discriminant_specification_StartIndex); 
            }
            Symbols_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "discriminant_specification"

    public class default_expression_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "default_expression"
    // Ada95.g:324:1: default_expression : expression ;
    public Ada95Parser.default_expression_return default_expression() // throws RecognitionException [1]
    {   
        Ada95Parser.default_expression_return retval = new Ada95Parser.default_expression_return();
        retval.Start = input.LT(1);
        int default_expression_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.expression_return expression131 = default(Ada95Parser.expression_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 40) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:325:2: ( expression )
            // Ada95.g:325:4: expression
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_expression_in_default_expression1275);
            	expression131 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression131.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 40, default_expression_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "default_expression"

    public class record_type_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "record_type_definition"
    // Ada95.g:331:1: record_type_definition : ( ( ABSTRACT )? TAGGED )? ( LIMITED )? record_definition -> ^( RECORD ( ( ABSTRACT )? TAGGED )? ( LIMITED )? record_definition ) ;
    public Ada95Parser.record_type_definition_return record_type_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.record_type_definition_return retval = new Ada95Parser.record_type_definition_return();
        retval.Start = input.LT(1);
        int record_type_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken ABSTRACT132 = null;
        IToken TAGGED133 = null;
        IToken LIMITED134 = null;
        Ada95Parser.record_definition_return record_definition135 = default(Ada95Parser.record_definition_return);


        CommonTree ABSTRACT132_tree=null;
        CommonTree TAGGED133_tree=null;
        CommonTree LIMITED134_tree=null;
        RewriteRuleTokenStream stream_ABSTRACT = new RewriteRuleTokenStream(adaptor,"token ABSTRACT");
        RewriteRuleTokenStream stream_LIMITED = new RewriteRuleTokenStream(adaptor,"token LIMITED");
        RewriteRuleTokenStream stream_TAGGED = new RewriteRuleTokenStream(adaptor,"token TAGGED");
        RewriteRuleSubtreeStream stream_record_definition = new RewriteRuleSubtreeStream(adaptor,"rule record_definition");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 41) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:332:2: ( ( ( ABSTRACT )? TAGGED )? ( LIMITED )? record_definition -> ^( RECORD ( ( ABSTRACT )? TAGGED )? ( LIMITED )? record_definition ) )
            // Ada95.g:332:4: ( ( ABSTRACT )? TAGGED )? ( LIMITED )? record_definition
            {
            	// Ada95.g:332:4: ( ( ABSTRACT )? TAGGED )?
            	int alt30 = 2;
            	int LA30_0 = input.LA(1);

            	if ( (LA30_0 == ABSTRACT || LA30_0 == TAGGED) )
            	{
            	    alt30 = 1;
            	}
            	switch (alt30) 
            	{
            	    case 1 :
            	        // Ada95.g:332:6: ( ABSTRACT )? TAGGED
            	        {
            	        	// Ada95.g:332:6: ( ABSTRACT )?
            	        	int alt29 = 2;
            	        	int LA29_0 = input.LA(1);

            	        	if ( (LA29_0 == ABSTRACT) )
            	        	{
            	        	    alt29 = 1;
            	        	}
            	        	switch (alt29) 
            	        	{
            	        	    case 1 :
            	        	        // Ada95.g:332:6: ABSTRACT
            	        	        {
            	        	        	ABSTRACT132=(IToken)Match(input,ABSTRACT,FOLLOW_ABSTRACT_in_record_type_definition1288); if (state.failed) return retval; 
            	        	        	if ( (state.backtracking==0) ) stream_ABSTRACT.Add(ABSTRACT132);


            	        	        }
            	        	        break;

            	        	}

            	        	TAGGED133=(IToken)Match(input,TAGGED,FOLLOW_TAGGED_in_record_type_definition1291); if (state.failed) return retval; 
            	        	if ( (state.backtracking==0) ) stream_TAGGED.Add(TAGGED133);


            	        }
            	        break;

            	}

            	// Ada95.g:332:26: ( LIMITED )?
            	int alt31 = 2;
            	int LA31_0 = input.LA(1);

            	if ( (LA31_0 == LIMITED) )
            	{
            	    alt31 = 1;
            	}
            	switch (alt31) 
            	{
            	    case 1 :
            	        // Ada95.g:332:26: LIMITED
            	        {
            	        	LIMITED134=(IToken)Match(input,LIMITED,FOLLOW_LIMITED_in_record_type_definition1296); if (state.failed) return retval; 
            	        	if ( (state.backtracking==0) ) stream_LIMITED.Add(LIMITED134);


            	        }
            	        break;

            	}

            	PushFollow(FOLLOW_record_definition_in_record_type_definition1299);
            	record_definition135 = record_definition();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_record_definition.Add(record_definition135.Tree);


            	// AST REWRITE
            	// elements:          record_definition, LIMITED, ABSTRACT, TAGGED
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 332:53: -> ^( RECORD ( ( ABSTRACT )? TAGGED )? ( LIMITED )? record_definition )
            	{
            	    // Ada95.g:332:56: ^( RECORD ( ( ABSTRACT )? TAGGED )? ( LIMITED )? record_definition )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(RECORD, "RECORD"), root_1);

            	    // Ada95.g:332:66: ( ( ABSTRACT )? TAGGED )?
            	    if ( stream_ABSTRACT.HasNext() || stream_TAGGED.HasNext() )
            	    {
            	        // Ada95.g:332:68: ( ABSTRACT )?
            	        if ( stream_ABSTRACT.HasNext() )
            	        {
            	            adaptor.AddChild(root_1, stream_ABSTRACT.NextNode());

            	        }
            	        stream_ABSTRACT.Reset();
            	        adaptor.AddChild(root_1, stream_TAGGED.NextNode());

            	    }
            	    stream_ABSTRACT.Reset();
            	    stream_TAGGED.Reset();
            	    // Ada95.g:332:88: ( LIMITED )?
            	    if ( stream_LIMITED.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_LIMITED.NextNode());

            	    }
            	    stream_LIMITED.Reset();
            	    adaptor.AddChild(root_1, stream_record_definition.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 41, record_type_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "record_type_definition"

    public class record_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "record_definition"
    // Ada95.g:334:1: record_definition : ( RECORD component_list END RECORD | NULL RECORD );
    public Ada95Parser.record_definition_return record_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.record_definition_return retval = new Ada95Parser.record_definition_return();
        retval.Start = input.LT(1);
        int record_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken RECORD136 = null;
        IToken END138 = null;
        IToken RECORD139 = null;
        IToken NULL140 = null;
        IToken RECORD141 = null;
        Ada95Parser.component_list_return component_list137 = default(Ada95Parser.component_list_return);


        CommonTree RECORD136_tree=null;
        CommonTree END138_tree=null;
        CommonTree RECORD139_tree=null;
        CommonTree NULL140_tree=null;
        CommonTree RECORD141_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 42) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:335:2: ( RECORD component_list END RECORD | NULL RECORD )
            int alt32 = 2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0 == RECORD) )
            {
                alt32 = 1;
            }
            else if ( (LA32_0 == NULL) )
            {
                alt32 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d32s0 =
                    new NoViableAltException("", 32, 0, input);

                throw nvae_d32s0;
            }
            switch (alt32) 
            {
                case 1 :
                    // Ada95.g:336:3: RECORD component_list END RECORD
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	RECORD136=(IToken)Match(input,RECORD,FOLLOW_RECORD_in_record_definition1334); if (state.failed) return retval;
                    	PushFollow(FOLLOW_component_list_in_record_definition1337);
                    	component_list137 = component_list();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, component_list137.Tree);
                    	END138=(IToken)Match(input,END,FOLLOW_END_in_record_definition1339); if (state.failed) return retval;
                    	RECORD139=(IToken)Match(input,RECORD,FOLLOW_RECORD_in_record_definition1342); if (state.failed) return retval;

                    }
                    break;
                case 2 :
                    // Ada95.g:337:3: NULL RECORD
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	NULL140=(IToken)Match(input,NULL,FOLLOW_NULL_in_record_definition1349); if (state.failed) return retval;
                    	RECORD141=(IToken)Match(input,RECORD,FOLLOW_RECORD_in_record_definition1352); if (state.failed) return retval;

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 42, record_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "record_definition"

    public class component_list_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "component_list"
    // Ada95.g:340:1: component_list : ( ( ( component_item )* variant_part )=> ( component_item )* variant_part -> ^( COMPONENT_LIST ( component_item )* variant_part ) | ( component_item )+ -> ^( COMPONENT_LIST ( component_item )+ ) | NULL SEMICOLON );
    public Ada95Parser.component_list_return component_list() // throws RecognitionException [1]
    {   
        Ada95Parser.component_list_return retval = new Ada95Parser.component_list_return();
        retval.Start = input.LT(1);
        int component_list_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken NULL145 = null;
        IToken SEMICOLON146 = null;
        Ada95Parser.component_item_return component_item142 = default(Ada95Parser.component_item_return);

        Ada95Parser.variant_part_return variant_part143 = default(Ada95Parser.variant_part_return);

        Ada95Parser.component_item_return component_item144 = default(Ada95Parser.component_item_return);


        CommonTree NULL145_tree=null;
        CommonTree SEMICOLON146_tree=null;
        RewriteRuleSubtreeStream stream_variant_part = new RewriteRuleSubtreeStream(adaptor,"rule variant_part");
        RewriteRuleSubtreeStream stream_component_item = new RewriteRuleSubtreeStream(adaptor,"rule component_item");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 43) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:341:2: ( ( ( component_item )* variant_part )=> ( component_item )* variant_part -> ^( COMPONENT_LIST ( component_item )* variant_part ) | ( component_item )+ -> ^( COMPONENT_LIST ( component_item )+ ) | NULL SEMICOLON )
            int alt35 = 3;
            int LA35_0 = input.LA(1);

            if ( (LA35_0 == IDENTIFIER) )
            {
                int LA35_1 = input.LA(2);

                if ( (synpred2_Ada95()) )
                {
                    alt35 = 1;
                }
                else if ( (true) )
                {
                    alt35 = 2;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d35s1 =
                        new NoViableAltException("", 35, 1, input);

                    throw nvae_d35s1;
                }
            }
            else if ( (LA35_0 == CASE) && (synpred2_Ada95()) )
            {
                alt35 = 1;
            }
            else if ( (LA35_0 == NULL) )
            {
                alt35 = 3;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d35s0 =
                    new NoViableAltException("", 35, 0, input);

                throw nvae_d35s0;
            }
            switch (alt35) 
            {
                case 1 :
                    // Ada95.g:342:3: ( ( component_item )* variant_part )=> ( component_item )* variant_part
                    {
                    	// Ada95.g:342:38: ( component_item )*
                    	do 
                    	{
                    	    int alt33 = 2;
                    	    int LA33_0 = input.LA(1);

                    	    if ( (LA33_0 == IDENTIFIER) )
                    	    {
                    	        alt33 = 1;
                    	    }


                    	    switch (alt33) 
                    		{
                    			case 1 :
                    			    // Ada95.g:342:38: component_item
                    			    {
                    			    	PushFollow(FOLLOW_component_item_in_component_list1376);
                    			    	component_item142 = component_item();
                    			    	state.followingStackPointer--;
                    			    	if (state.failed) return retval;
                    			    	if ( (state.backtracking==0) ) stream_component_item.Add(component_item142.Tree);

                    			    }
                    			    break;

                    			default:
                    			    goto loop33;
                    	    }
                    	} while (true);

                    	loop33:
                    		;	// Stops C# compiler whining that label 'loop33' has no statements

                    	PushFollow(FOLLOW_variant_part_in_component_list1379);
                    	variant_part143 = variant_part();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_variant_part.Add(variant_part143.Tree);


                    	// AST REWRITE
                    	// elements:          component_item, variant_part
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (CommonTree)adaptor.GetNilNode();
                    	// 342:67: -> ^( COMPONENT_LIST ( component_item )* variant_part )
                    	{
                    	    // Ada95.g:342:70: ^( COMPONENT_LIST ( component_item )* variant_part )
                    	    {
                    	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
                    	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(COMPONENT_LIST, "COMPONENT_LIST"), root_1);

                    	    // Ada95.g:342:88: ( component_item )*
                    	    while ( stream_component_item.HasNext() )
                    	    {
                    	        adaptor.AddChild(root_1, stream_component_item.NextTree());

                    	    }
                    	    stream_component_item.Reset();
                    	    adaptor.AddChild(root_1, stream_variant_part.NextTree());

                    	    adaptor.AddChild(root_0, root_1);
                    	    }

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;
                case 2 :
                    // Ada95.g:343:12: ( component_item )+
                    {
                    	// Ada95.g:343:12: ( component_item )+
                    	int cnt34 = 0;
                    	do 
                    	{
                    	    int alt34 = 2;
                    	    int LA34_0 = input.LA(1);

                    	    if ( (LA34_0 == IDENTIFIER) )
                    	    {
                    	        alt34 = 1;
                    	    }


                    	    switch (alt34) 
                    		{
                    			case 1 :
                    			    // Ada95.g:343:12: component_item
                    			    {
                    			    	PushFollow(FOLLOW_component_item_in_component_list1407);
                    			    	component_item144 = component_item();
                    			    	state.followingStackPointer--;
                    			    	if (state.failed) return retval;
                    			    	if ( (state.backtracking==0) ) stream_component_item.Add(component_item144.Tree);

                    			    }
                    			    break;

                    			default:
                    			    if ( cnt34 >= 1 ) goto loop34;
                    			    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    		            EarlyExitException eee34 =
                    		                new EarlyExitException(34, input);
                    		            throw eee34;
                    	    }
                    	    cnt34++;
                    	} while (true);

                    	loop34:
                    		;	// Stops C# compiler whining that label 'loop34' has no statements



                    	// AST REWRITE
                    	// elements:          component_item
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (CommonTree)adaptor.GetNilNode();
                    	// 343:32: -> ^( COMPONENT_LIST ( component_item )+ )
                    	{
                    	    // Ada95.g:343:35: ^( COMPONENT_LIST ( component_item )+ )
                    	    {
                    	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
                    	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(COMPONENT_LIST, "COMPONENT_LIST"), root_1);

                    	    if ( !(stream_component_item.HasNext()) ) {
                    	        throw new RewriteEarlyExitException();
                    	    }
                    	    while ( stream_component_item.HasNext() )
                    	    {
                    	        adaptor.AddChild(root_1, stream_component_item.NextTree());

                    	    }
                    	    stream_component_item.Reset();

                    	    adaptor.AddChild(root_0, root_1);
                    	    }

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;
                case 3 :
                    // Ada95.g:344:12: NULL SEMICOLON
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	NULL145=(IToken)Match(input,NULL,FOLLOW_NULL_in_component_list1438); if (state.failed) return retval;
                    	SEMICOLON146=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_component_list1441); if (state.failed) return retval;

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 43, component_list_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "component_list"

    public class component_item_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "component_item"
    // Ada95.g:347:1: component_item : component_declaration ;
    public Ada95Parser.component_item_return component_item() // throws RecognitionException [1]
    {   
        Ada95Parser.component_item_return retval = new Ada95Parser.component_item_return();
        retval.Start = input.LT(1);
        int component_item_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.component_declaration_return component_declaration147 = default(Ada95Parser.component_declaration_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 44) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:348:2: ( component_declaration )
            // Ada95.g:348:4: component_declaration
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_component_declaration_in_component_item1453);
            	component_declaration147 = component_declaration();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, component_declaration147.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 44, component_item_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "component_item"

    public class component_declaration_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "component_declaration"
    // Ada95.g:350:1: component_declaration : defining_identifier_list COLON component_definition ( ASSIGN default_expression )? SEMICOLON -> ^( COMPONENT defining_identifier_list component_definition ( default_expression )? ) ;
    public Ada95Parser.component_declaration_return component_declaration() // throws RecognitionException [1]
    {   
        Symbols_stack.Push(new Symbols_scope());

        Ada95Parser.component_declaration_return retval = new Ada95Parser.component_declaration_return();
        retval.Start = input.LT(1);
        int component_declaration_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken COLON149 = null;
        IToken ASSIGN151 = null;
        IToken SEMICOLON153 = null;
        Ada95Parser.defining_identifier_list_return defining_identifier_list148 = default(Ada95Parser.defining_identifier_list_return);

        Ada95Parser.component_definition_return component_definition150 = default(Ada95Parser.component_definition_return);

        Ada95Parser.default_expression_return default_expression152 = default(Ada95Parser.default_expression_return);


        CommonTree COLON149_tree=null;
        CommonTree ASSIGN151_tree=null;
        CommonTree SEMICOLON153_tree=null;
        RewriteRuleTokenStream stream_COLON = new RewriteRuleTokenStream(adaptor,"token COLON");
        RewriteRuleTokenStream stream_SEMICOLON = new RewriteRuleTokenStream(adaptor,"token SEMICOLON");
        RewriteRuleTokenStream stream_ASSIGN = new RewriteRuleTokenStream(adaptor,"token ASSIGN");
        RewriteRuleSubtreeStream stream_default_expression = new RewriteRuleSubtreeStream(adaptor,"rule default_expression");
        RewriteRuleSubtreeStream stream_component_definition = new RewriteRuleSubtreeStream(adaptor,"rule component_definition");
        RewriteRuleSubtreeStream stream_defining_identifier_list = new RewriteRuleSubtreeStream(adaptor,"rule defining_identifier_list");

        		((Symbols_scope)Symbols_stack.Peek()).IDs =  new SortedSet< string >();
        	
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 45) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:356:2: ( defining_identifier_list COLON component_definition ( ASSIGN default_expression )? SEMICOLON -> ^( COMPONENT defining_identifier_list component_definition ( default_expression )? ) )
            // Ada95.g:357:3: defining_identifier_list COLON component_definition ( ASSIGN default_expression )? SEMICOLON
            {
            	PushFollow(FOLLOW_defining_identifier_list_in_component_declaration1478);
            	defining_identifier_list148 = defining_identifier_list();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_defining_identifier_list.Add(defining_identifier_list148.Tree);
            	COLON149=(IToken)Match(input,COLON,FOLLOW_COLON_in_component_declaration1480); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_COLON.Add(COLON149);

            	PushFollow(FOLLOW_component_definition_in_component_declaration1482);
            	component_definition150 = component_definition();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_component_definition.Add(component_definition150.Tree);
            	// Ada95.g:357:55: ( ASSIGN default_expression )?
            	int alt36 = 2;
            	int LA36_0 = input.LA(1);

            	if ( (LA36_0 == ASSIGN) )
            	{
            	    alt36 = 1;
            	}
            	switch (alt36) 
            	{
            	    case 1 :
            	        // Ada95.g:357:57: ASSIGN default_expression
            	        {
            	        	ASSIGN151=(IToken)Match(input,ASSIGN,FOLLOW_ASSIGN_in_component_declaration1486); if (state.failed) return retval; 
            	        	if ( (state.backtracking==0) ) stream_ASSIGN.Add(ASSIGN151);

            	        	PushFollow(FOLLOW_default_expression_in_component_declaration1488);
            	        	default_expression152 = default_expression();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_default_expression.Add(default_expression152.Tree);

            	        }
            	        break;

            	}

            	SEMICOLON153=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_component_declaration1493); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_SEMICOLON.Add(SEMICOLON153);

            	if ( (state.backtracking==0) )
            	{
            	   ((SymbolTable_scope)SymbolTable_stack.Peek()).Objects.UnionWith( ((Symbols_scope)Symbols_stack.Peek()).IDs ); 
            	}


            	// AST REWRITE
            	// elements:          component_definition, defining_identifier_list, default_expression
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 359:4: -> ^( COMPONENT defining_identifier_list component_definition ( default_expression )? )
            	{
            	    // Ada95.g:359:7: ^( COMPONENT defining_identifier_list component_definition ( default_expression )? )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(COMPONENT, "COMPONENT"), root_1);

            	    adaptor.AddChild(root_1, stream_defining_identifier_list.NextTree());
            	    adaptor.AddChild(root_1, stream_component_definition.NextTree());
            	    // Ada95.g:359:66: ( default_expression )?
            	    if ( stream_default_expression.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_default_expression.NextTree());

            	    }
            	    stream_default_expression.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 45, component_declaration_StartIndex); 
            }
            Symbols_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "component_declaration"

    public class variant_part_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "variant_part"
    // Ada95.g:362:1: variant_part : CASE direct_name IS ( ( ( variant )* other_variant )=> ( variant )* other_variant | ( variant )+ ) END CASE SEMICOLON ;
    public Ada95Parser.variant_part_return variant_part() // throws RecognitionException [1]
    {   
        Ada95Parser.variant_part_return retval = new Ada95Parser.variant_part_return();
        retval.Start = input.LT(1);
        int variant_part_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken CASE154 = null;
        IToken IS156 = null;
        IToken END160 = null;
        IToken CASE161 = null;
        IToken SEMICOLON162 = null;
        Ada95Parser.direct_name_return direct_name155 = default(Ada95Parser.direct_name_return);

        Ada95Parser.variant_return variant157 = default(Ada95Parser.variant_return);

        Ada95Parser.other_variant_return other_variant158 = default(Ada95Parser.other_variant_return);

        Ada95Parser.variant_return variant159 = default(Ada95Parser.variant_return);


        CommonTree CASE154_tree=null;
        CommonTree IS156_tree=null;
        CommonTree END160_tree=null;
        CommonTree CASE161_tree=null;
        CommonTree SEMICOLON162_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 46) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:363:2: ( CASE direct_name IS ( ( ( variant )* other_variant )=> ( variant )* other_variant | ( variant )+ ) END CASE SEMICOLON )
            // Ada95.g:364:3: CASE direct_name IS ( ( ( variant )* other_variant )=> ( variant )* other_variant | ( variant )+ ) END CASE SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	CASE154=(IToken)Match(input,CASE,FOLLOW_CASE_in_variant_part1529); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{CASE154_tree = (CommonTree)adaptor.Create(CASE154);
            		root_0 = (CommonTree)adaptor.BecomeRoot(CASE154_tree, root_0);
            	}
            	PushFollow(FOLLOW_direct_name_in_variant_part1532);
            	direct_name155 = direct_name();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, direct_name155.Tree);
            	IS156=(IToken)Match(input,IS,FOLLOW_IS_in_variant_part1534); if (state.failed) return retval;
            	// Ada95.g:365:3: ( ( ( variant )* other_variant )=> ( variant )* other_variant | ( variant )+ )
            	int alt39 = 2;
            	int LA39_0 = input.LA(1);

            	if ( (LA39_0 == WHEN) )
            	{
            	    int LA39_1 = input.LA(2);

            	    if ( (synpred3_Ada95()) )
            	    {
            	        alt39 = 1;
            	    }
            	    else if ( (true) )
            	    {
            	        alt39 = 2;
            	    }
            	    else 
            	    {
            	        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        NoViableAltException nvae_d39s1 =
            	            new NoViableAltException("", 39, 1, input);

            	        throw nvae_d39s1;
            	    }
            	}
            	else 
            	{
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    NoViableAltException nvae_d39s0 =
            	        new NoViableAltException("", 39, 0, input);

            	    throw nvae_d39s0;
            	}
            	switch (alt39) 
            	{
            	    case 1 :
            	        // Ada95.g:366:4: ( ( variant )* other_variant )=> ( variant )* other_variant
            	        {
            	        	// Ada95.g:366:33: ( variant )*
            	        	do 
            	        	{
            	        	    int alt37 = 2;
            	        	    int LA37_0 = input.LA(1);

            	        	    if ( (LA37_0 == WHEN) )
            	        	    {
            	        	        int LA37_1 = input.LA(2);

            	        	        if ( (LA37_1 == IDENTIFIER || LA37_1 == LPARANTHESIS || LA37_1 == CHARACTER_LITERAL || LA37_1 == NULL || LA37_1 == NOT || (LA37_1 >= ABS && LA37_1 <= STRING_LITERAL) || (LA37_1 >= PLUS && LA37_1 <= MINUS)) )
            	        	        {
            	        	            alt37 = 1;
            	        	        }


            	        	    }


            	        	    switch (alt37) 
            	        		{
            	        			case 1 :
            	        			    // Ada95.g:366:33: variant
            	        			    {
            	        			    	PushFollow(FOLLOW_variant_in_variant_part1554);
            	        			    	variant157 = variant();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, variant157.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    goto loop37;
            	        	    }
            	        	} while (true);

            	        	loop37:
            	        		;	// Stops C# compiler whining that label 'loop37' has no statements

            	        	PushFollow(FOLLOW_other_variant_in_variant_part1557);
            	        	other_variant158 = other_variant();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, other_variant158.Tree);

            	        }
            	        break;
            	    case 2 :
            	        // Ada95.g:367:12: ( variant )+
            	        {
            	        	// Ada95.g:367:12: ( variant )+
            	        	int cnt38 = 0;
            	        	do 
            	        	{
            	        	    int alt38 = 2;
            	        	    int LA38_0 = input.LA(1);

            	        	    if ( (LA38_0 == WHEN) )
            	        	    {
            	        	        alt38 = 1;
            	        	    }


            	        	    switch (alt38) 
            	        		{
            	        			case 1 :
            	        			    // Ada95.g:367:12: variant
            	        			    {
            	        			    	PushFollow(FOLLOW_variant_in_variant_part1572);
            	        			    	variant159 = variant();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, variant159.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt38 >= 1 ) goto loop38;
            	        			    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        		            EarlyExitException eee38 =
            	        		                new EarlyExitException(38, input);
            	        		            throw eee38;
            	        	    }
            	        	    cnt38++;
            	        	} while (true);

            	        	loop38:
            	        		;	// Stops C# compiler whining that label 'loop38' has no statements


            	        }
            	        break;

            	}

            	END160=(IToken)Match(input,END,FOLLOW_END_in_variant_part1579); if (state.failed) return retval;
            	CASE161=(IToken)Match(input,CASE,FOLLOW_CASE_in_variant_part1582); if (state.failed) return retval;
            	SEMICOLON162=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_variant_part1585); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 46, variant_part_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "variant_part"

    public class variant_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "variant"
    // Ada95.g:371:1: variant : WHEN discrete_choice_list ASSOCIATION component_list ;
    public Ada95Parser.variant_return variant() // throws RecognitionException [1]
    {   
        Ada95Parser.variant_return retval = new Ada95Parser.variant_return();
        retval.Start = input.LT(1);
        int variant_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken WHEN163 = null;
        IToken ASSOCIATION165 = null;
        Ada95Parser.discrete_choice_list_return discrete_choice_list164 = default(Ada95Parser.discrete_choice_list_return);

        Ada95Parser.component_list_return component_list166 = default(Ada95Parser.component_list_return);


        CommonTree WHEN163_tree=null;
        CommonTree ASSOCIATION165_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 47) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:372:2: ( WHEN discrete_choice_list ASSOCIATION component_list )
            // Ada95.g:372:4: WHEN discrete_choice_list ASSOCIATION component_list
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	WHEN163=(IToken)Match(input,WHEN,FOLLOW_WHEN_in_variant1597); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{WHEN163_tree = (CommonTree)adaptor.Create(WHEN163);
            		root_0 = (CommonTree)adaptor.BecomeRoot(WHEN163_tree, root_0);
            	}
            	PushFollow(FOLLOW_discrete_choice_list_in_variant1600);
            	discrete_choice_list164 = discrete_choice_list();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, discrete_choice_list164.Tree);
            	ASSOCIATION165=(IToken)Match(input,ASSOCIATION,FOLLOW_ASSOCIATION_in_variant1602); if (state.failed) return retval;
            	PushFollow(FOLLOW_component_list_in_variant1605);
            	component_list166 = component_list();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, component_list166.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 47, variant_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "variant"

    public class other_variant_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "other_variant"
    // Ada95.g:374:1: other_variant : WHEN OTHERS ASSOCIATION component_list ;
    public Ada95Parser.other_variant_return other_variant() // throws RecognitionException [1]
    {   
        Ada95Parser.other_variant_return retval = new Ada95Parser.other_variant_return();
        retval.Start = input.LT(1);
        int other_variant_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken WHEN167 = null;
        IToken OTHERS168 = null;
        IToken ASSOCIATION169 = null;
        Ada95Parser.component_list_return component_list170 = default(Ada95Parser.component_list_return);


        CommonTree WHEN167_tree=null;
        CommonTree OTHERS168_tree=null;
        CommonTree ASSOCIATION169_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 48) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:375:2: ( WHEN OTHERS ASSOCIATION component_list )
            // Ada95.g:375:4: WHEN OTHERS ASSOCIATION component_list
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	WHEN167=(IToken)Match(input,WHEN,FOLLOW_WHEN_in_other_variant1616); if (state.failed) return retval;
            	OTHERS168=(IToken)Match(input,OTHERS,FOLLOW_OTHERS_in_other_variant1619); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{OTHERS168_tree = (CommonTree)adaptor.Create(OTHERS168);
            		root_0 = (CommonTree)adaptor.BecomeRoot(OTHERS168_tree, root_0);
            	}
            	ASSOCIATION169=(IToken)Match(input,ASSOCIATION,FOLLOW_ASSOCIATION_in_other_variant1622); if (state.failed) return retval;
            	PushFollow(FOLLOW_component_list_in_other_variant1625);
            	component_list170 = component_list();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, component_list170.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 48, other_variant_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "other_variant"

    public class discrete_choice_list_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "discrete_choice_list"
    // Ada95.g:377:1: discrete_choice_list : discrete_choice ( CHOICE discrete_choice )* ;
    public Ada95Parser.discrete_choice_list_return discrete_choice_list() // throws RecognitionException [1]
    {   
        Ada95Parser.discrete_choice_list_return retval = new Ada95Parser.discrete_choice_list_return();
        retval.Start = input.LT(1);
        int discrete_choice_list_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken CHOICE172 = null;
        Ada95Parser.discrete_choice_return discrete_choice171 = default(Ada95Parser.discrete_choice_return);

        Ada95Parser.discrete_choice_return discrete_choice173 = default(Ada95Parser.discrete_choice_return);


        CommonTree CHOICE172_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 49) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:378:2: ( discrete_choice ( CHOICE discrete_choice )* )
            // Ada95.g:378:4: discrete_choice ( CHOICE discrete_choice )*
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_discrete_choice_in_discrete_choice_list1635);
            	discrete_choice171 = discrete_choice();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, discrete_choice171.Tree);
            	// Ada95.g:378:20: ( CHOICE discrete_choice )*
            	do 
            	{
            	    int alt40 = 2;
            	    int LA40_0 = input.LA(1);

            	    if ( (LA40_0 == CHOICE) )
            	    {
            	        alt40 = 1;
            	    }


            	    switch (alt40) 
            		{
            			case 1 :
            			    // Ada95.g:378:22: CHOICE discrete_choice
            			    {
            			    	CHOICE172=(IToken)Match(input,CHOICE,FOLLOW_CHOICE_in_discrete_choice_list1639); if (state.failed) return retval;
            			    	PushFollow(FOLLOW_discrete_choice_in_discrete_choice_list1642);
            			    	discrete_choice173 = discrete_choice();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, discrete_choice173.Tree);

            			    }
            			    break;

            			default:
            			    goto loop40;
            	    }
            	} while (true);

            	loop40:
            		;	// Stops C# compiler whining that label 'loop40' has no statements


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 49, discrete_choice_list_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "discrete_choice_list"

    public class discrete_choice_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "discrete_choice"
    // Ada95.g:380:1: discrete_choice : ( ( discrete_range )=> discrete_range | expression );
    public Ada95Parser.discrete_choice_return discrete_choice() // throws RecognitionException [1]
    {   
        Ada95Parser.discrete_choice_return retval = new Ada95Parser.discrete_choice_return();
        retval.Start = input.LT(1);
        int discrete_choice_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.discrete_range_return discrete_range174 = default(Ada95Parser.discrete_range_return);

        Ada95Parser.expression_return expression175 = default(Ada95Parser.expression_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 50) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:381:2: ( ( discrete_range )=> discrete_range | expression )
            int alt41 = 2;
            alt41 = dfa41.Predict(input);
            switch (alt41) 
            {
                case 1 :
                    // Ada95.g:382:3: ( discrete_range )=> discrete_range
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_discrete_range_in_discrete_choice1664);
                    	discrete_range174 = discrete_range();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, discrete_range174.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:383:9: expression
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_expression_in_discrete_choice1676);
                    	expression175 = expression();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression175.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 50, discrete_choice_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "discrete_choice"

    public class record_extension_part_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "record_extension_part"
    // Ada95.g:386:1: record_extension_part : WITH record_definition ;
    public Ada95Parser.record_extension_part_return record_extension_part() // throws RecognitionException [1]
    {   
        Ada95Parser.record_extension_part_return retval = new Ada95Parser.record_extension_part_return();
        retval.Start = input.LT(1);
        int record_extension_part_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken WITH176 = null;
        Ada95Parser.record_definition_return record_definition177 = default(Ada95Parser.record_definition_return);


        CommonTree WITH176_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 51) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:387:2: ( WITH record_definition )
            // Ada95.g:387:4: WITH record_definition
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	WITH176=(IToken)Match(input,WITH,FOLLOW_WITH_in_record_extension_part1687); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{WITH176_tree = (CommonTree)adaptor.Create(WITH176);
            		root_0 = (CommonTree)adaptor.BecomeRoot(WITH176_tree, root_0);
            	}
            	PushFollow(FOLLOW_record_definition_in_record_extension_part1690);
            	record_definition177 = record_definition();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, record_definition177.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 51, record_extension_part_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "record_extension_part"

    public class access_definition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "access_definition"
    // Ada95.g:402:1: access_definition : ACCESS subtype_mark ;
    public Ada95Parser.access_definition_return access_definition() // throws RecognitionException [1]
    {   
        Ada95Parser.access_definition_return retval = new Ada95Parser.access_definition_return();
        retval.Start = input.LT(1);
        int access_definition_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken ACCESS178 = null;
        Ada95Parser.subtype_mark_return subtype_mark179 = default(Ada95Parser.subtype_mark_return);


        CommonTree ACCESS178_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 52) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:403:2: ( ACCESS subtype_mark )
            // Ada95.g:403:4: ACCESS subtype_mark
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	ACCESS178=(IToken)Match(input,ACCESS,FOLLOW_ACCESS_in_access_definition1701); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{ACCESS178_tree = (CommonTree)adaptor.Create(ACCESS178);
            		adaptor.AddChild(root_0, ACCESS178_tree);
            	}
            	PushFollow(FOLLOW_subtype_mark_in_access_definition1703);
            	subtype_mark179 = subtype_mark();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subtype_mark179.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 52, access_definition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "access_definition"

    public class declarative_part_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "declarative_part"
    // Ada95.g:405:1: declarative_part : ( declarative_item )* -> ^( DECLARE ( declarative_item )* ) ;
    public Ada95Parser.declarative_part_return declarative_part() // throws RecognitionException [1]
    {   
        Ada95Parser.declarative_part_return retval = new Ada95Parser.declarative_part_return();
        retval.Start = input.LT(1);
        int declarative_part_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.declarative_item_return declarative_item180 = default(Ada95Parser.declarative_item_return);


        RewriteRuleSubtreeStream stream_declarative_item = new RewriteRuleSubtreeStream(adaptor,"rule declarative_item");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 53) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:406:2: ( ( declarative_item )* -> ^( DECLARE ( declarative_item )* ) )
            // Ada95.g:406:4: ( declarative_item )*
            {
            	// Ada95.g:406:4: ( declarative_item )*
            	do 
            	{
            	    int alt42 = 2;
            	    int LA42_0 = input.LA(1);

            	    if ( ((LA42_0 >= IDENTIFIER && LA42_0 <= TYPE) || (LA42_0 >= PROCEDURE && LA42_0 <= FUNCTION)) )
            	    {
            	        alt42 = 1;
            	    }


            	    switch (alt42) 
            		{
            			case 1 :
            			    // Ada95.g:406:4: declarative_item
            			    {
            			    	PushFollow(FOLLOW_declarative_item_in_declarative_part1713);
            			    	declarative_item180 = declarative_item();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( (state.backtracking==0) ) stream_declarative_item.Add(declarative_item180.Tree);

            			    }
            			    break;

            			default:
            			    goto loop42;
            	    }
            	} while (true);

            	loop42:
            		;	// Stops C# compiler whining that label 'loop42' has no statements



            	// AST REWRITE
            	// elements:          declarative_item
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 406:22: -> ^( DECLARE ( declarative_item )* )
            	{
            	    // Ada95.g:406:25: ^( DECLARE ( declarative_item )* )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(DECLARE, "DECLARE"), root_1);

            	    // Ada95.g:406:36: ( declarative_item )*
            	    while ( stream_declarative_item.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_declarative_item.NextTree());

            	    }
            	    stream_declarative_item.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 53, declarative_part_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "declarative_part"

    public class declarative_item_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "declarative_item"
    // Ada95.g:408:1: declarative_item : ( ( basic_declarative_item )=> basic_declarative_item -> ^( ITEM basic_declarative_item ) | body -> ^( BODY body ) );
    public Ada95Parser.declarative_item_return declarative_item() // throws RecognitionException [1]
    {   
        Ada95Parser.declarative_item_return retval = new Ada95Parser.declarative_item_return();
        retval.Start = input.LT(1);
        int declarative_item_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.basic_declarative_item_return basic_declarative_item181 = default(Ada95Parser.basic_declarative_item_return);

        Ada95Parser.body_return body182 = default(Ada95Parser.body_return);


        RewriteRuleSubtreeStream stream_body = new RewriteRuleSubtreeStream(adaptor,"rule body");
        RewriteRuleSubtreeStream stream_basic_declarative_item = new RewriteRuleSubtreeStream(adaptor,"rule basic_declarative_item");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 54) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:409:2: ( ( basic_declarative_item )=> basic_declarative_item -> ^( ITEM basic_declarative_item ) | body -> ^( BODY body ) )
            int alt43 = 2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0 == TYPE) && (synpred5_Ada95()) )
            {
                alt43 = 1;
            }
            else if ( (LA43_0 == IDENTIFIER) && (synpred5_Ada95()) )
            {
                alt43 = 1;
            }
            else if ( (LA43_0 == PROCEDURE) )
            {
                int LA43_3 = input.LA(2);

                if ( (synpred5_Ada95()) )
                {
                    alt43 = 1;
                }
                else if ( (true) )
                {
                    alt43 = 2;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d43s3 =
                        new NoViableAltException("", 43, 3, input);

                    throw nvae_d43s3;
                }
            }
            else if ( (LA43_0 == FUNCTION) )
            {
                int LA43_4 = input.LA(2);

                if ( (synpred5_Ada95()) )
                {
                    alt43 = 1;
                }
                else if ( (true) )
                {
                    alt43 = 2;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d43s4 =
                        new NoViableAltException("", 43, 4, input);

                    throw nvae_d43s4;
                }
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d43s0 =
                    new NoViableAltException("", 43, 0, input);

                throw nvae_d43s0;
            }
            switch (alt43) 
            {
                case 1 :
                    // Ada95.g:410:3: ( basic_declarative_item )=> basic_declarative_item
                    {
                    	PushFollow(FOLLOW_basic_declarative_item_in_declarative_item1744);
                    	basic_declarative_item181 = basic_declarative_item();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_basic_declarative_item.Add(basic_declarative_item181.Tree);


                    	// AST REWRITE
                    	// elements:          basic_declarative_item
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (CommonTree)adaptor.GetNilNode();
                    	// 410:55: -> ^( ITEM basic_declarative_item )
                    	{
                    	    // Ada95.g:410:58: ^( ITEM basic_declarative_item )
                    	    {
                    	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
                    	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(ITEM, "ITEM"), root_1);

                    	    adaptor.AddChild(root_1, stream_basic_declarative_item.NextTree());

                    	    adaptor.AddChild(root_0, root_1);
                    	    }

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;
                case 2 :
                    // Ada95.g:411:11: body
                    {
                    	PushFollow(FOLLOW_body_in_declarative_item1768);
                    	body182 = body();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( (state.backtracking==0) ) stream_body.Add(body182.Tree);


                    	// AST REWRITE
                    	// elements:          body
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (CommonTree)adaptor.GetNilNode();
                    	// 411:20: -> ^( BODY body )
                    	{
                    	    // Ada95.g:411:23: ^( BODY body )
                    	    {
                    	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
                    	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(BODY, "BODY"), root_1);

                    	    adaptor.AddChild(root_1, stream_body.NextTree());

                    	    adaptor.AddChild(root_0, root_1);
                    	    }

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 54, declarative_item_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "declarative_item"

    public class basic_declarative_item_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "basic_declarative_item"
    // Ada95.g:414:1: basic_declarative_item : basic_declaration ;
    public Ada95Parser.basic_declarative_item_return basic_declarative_item() // throws RecognitionException [1]
    {   
        Ada95Parser.basic_declarative_item_return retval = new Ada95Parser.basic_declarative_item_return();
        retval.Start = input.LT(1);
        int basic_declarative_item_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.basic_declaration_return basic_declaration183 = default(Ada95Parser.basic_declaration_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 55) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:415:2: ( basic_declaration )
            // Ada95.g:415:4: basic_declaration
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_basic_declaration_in_basic_declarative_item1793);
            	basic_declaration183 = basic_declaration();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, basic_declaration183.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 55, basic_declarative_item_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "basic_declarative_item"

    public class body_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "body"
    // Ada95.g:417:1: body : proper_body ;
    public Ada95Parser.body_return body() // throws RecognitionException [1]
    {   
        Ada95Parser.body_return retval = new Ada95Parser.body_return();
        retval.Start = input.LT(1);
        int body_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.proper_body_return proper_body184 = default(Ada95Parser.proper_body_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 56) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:418:2: ( proper_body )
            // Ada95.g:418:4: proper_body
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_proper_body_in_body1803);
            	proper_body184 = proper_body();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, proper_body184.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 56, body_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "body"

    public class proper_body_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "proper_body"
    // Ada95.g:420:1: proper_body : subprogram_body ;
    public Ada95Parser.proper_body_return proper_body() // throws RecognitionException [1]
    {   
        Ada95Parser.proper_body_return retval = new Ada95Parser.proper_body_return();
        retval.Start = input.LT(1);
        int proper_body_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.subprogram_body_return subprogram_body185 = default(Ada95Parser.subprogram_body_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 57) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:421:2: ( subprogram_body )
            // Ada95.g:421:4: subprogram_body
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_subprogram_body_in_proper_body1813);
            	subprogram_body185 = subprogram_body();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subprogram_body185.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 57, proper_body_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "proper_body"

    public class name_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "name"
    // Ada95.g:423:1: name : ({...}? => function_call | {...}? => type_conversion | {...}? => ( options {backtrack=true; } : slice -> slice | indexed_component -> indexed_component | explicit_dereference -> explicit_dereference | selected_component -> selected_component | direct_name -> ^( OBJECT direct_name ) ) | attribute_reference | CHARACTER_LITERAL | ( options {backtrack=true; } : type_conversion -> type_conversion | indexed_component -> indexed_component | slice -> slice | explicit_dereference -> explicit_dereference | selected_component -> selected_component | function_call -> function_call ) );
    public Ada95Parser.name_return name() // throws RecognitionException [1]
    {   
        Ada95Parser.name_return retval = new Ada95Parser.name_return();
        retval.Start = input.LT(1);
        int name_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken CHARACTER_LITERAL194 = null;
        Ada95Parser.function_call_return function_call186 = default(Ada95Parser.function_call_return);

        Ada95Parser.type_conversion_return type_conversion187 = default(Ada95Parser.type_conversion_return);

        Ada95Parser.slice_return slice188 = default(Ada95Parser.slice_return);

        Ada95Parser.indexed_component_return indexed_component189 = default(Ada95Parser.indexed_component_return);

        Ada95Parser.explicit_dereference_return explicit_dereference190 = default(Ada95Parser.explicit_dereference_return);

        Ada95Parser.selected_component_return selected_component191 = default(Ada95Parser.selected_component_return);

        Ada95Parser.direct_name_return direct_name192 = default(Ada95Parser.direct_name_return);

        Ada95Parser.attribute_reference_return attribute_reference193 = default(Ada95Parser.attribute_reference_return);

        Ada95Parser.type_conversion_return type_conversion195 = default(Ada95Parser.type_conversion_return);

        Ada95Parser.indexed_component_return indexed_component196 = default(Ada95Parser.indexed_component_return);

        Ada95Parser.slice_return slice197 = default(Ada95Parser.slice_return);

        Ada95Parser.explicit_dereference_return explicit_dereference198 = default(Ada95Parser.explicit_dereference_return);

        Ada95Parser.selected_component_return selected_component199 = default(Ada95Parser.selected_component_return);

        Ada95Parser.function_call_return function_call200 = default(Ada95Parser.function_call_return);


        CommonTree CHARACTER_LITERAL194_tree=null;
        RewriteRuleSubtreeStream stream_explicit_dereference = new RewriteRuleSubtreeStream(adaptor,"rule explicit_dereference");
        RewriteRuleSubtreeStream stream_type_conversion = new RewriteRuleSubtreeStream(adaptor,"rule type_conversion");
        RewriteRuleSubtreeStream stream_function_call = new RewriteRuleSubtreeStream(adaptor,"rule function_call");
        RewriteRuleSubtreeStream stream_indexed_component = new RewriteRuleSubtreeStream(adaptor,"rule indexed_component");
        RewriteRuleSubtreeStream stream_selected_component = new RewriteRuleSubtreeStream(adaptor,"rule selected_component");
        RewriteRuleSubtreeStream stream_slice = new RewriteRuleSubtreeStream(adaptor,"rule slice");
        RewriteRuleSubtreeStream stream_direct_name = new RewriteRuleSubtreeStream(adaptor,"rule direct_name");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 58) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:424:2: ({...}? => function_call | {...}? => type_conversion | {...}? => ( options {backtrack=true; } : slice -> slice | indexed_component -> indexed_component | explicit_dereference -> explicit_dereference | selected_component -> selected_component | direct_name -> ^( OBJECT direct_name ) ) | attribute_reference | CHARACTER_LITERAL | ( options {backtrack=true; } : type_conversion -> type_conversion | indexed_component -> indexed_component | slice -> slice | explicit_dereference -> explicit_dereference | selected_component -> selected_component | function_call -> function_call ) )
            int alt46 = 6;
            int LA46_0 = input.LA(1);

            if ( (LA46_0 == IDENTIFIER) )
            {
                int LA46_1 = input.LA(2);

                if ( (LA46_1 == LPARANTHESIS) && (( IsType( input.LT( 1 ).Text ) )) )
                {
                    alt46 = 2;
                }
                else if ( (LA46_1 == APOSTROPHE) )
                {
                    alt46 = 4;
                }
                else if ( (( IsFunction( input.LT( 1 ).Text ) )) )
                {
                    alt46 = 1;
                }
                else if ( (( IsObject( input.LT( 1 ).Text ) )) )
                {
                    alt46 = 3;
                }
                else if ( (true) )
                {
                    alt46 = 6;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d46s1 =
                        new NoViableAltException("", 46, 1, input);

                    throw nvae_d46s1;
                }
            }
            else if ( (LA46_0 == CHARACTER_LITERAL) )
            {
                alt46 = 5;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d46s0 =
                    new NoViableAltException("", 46, 0, input);

                throw nvae_d46s0;
            }
            switch (alt46) 
            {
                case 1 :
                    // Ada95.g:425:3: {...}? => function_call
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	if ( !(( IsFunction( input.LT( 1 ).Text ) )) ) 
                    	{
                    	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    	    throw new FailedPredicateException(input, "name", " IsFunction( input.LT( 1 ).Text ) ");
                    	}
                    	PushFollow(FOLLOW_function_call_in_name1828);
                    	function_call186 = function_call();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, function_call186.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:426:3: {...}? => type_conversion
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	if ( !(( IsType( input.LT( 1 ).Text ) )) ) 
                    	{
                    	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    	    throw new FailedPredicateException(input, "name", " IsType( input.LT( 1 ).Text ) ");
                    	}
                    	PushFollow(FOLLOW_type_conversion_in_name1838);
                    	type_conversion187 = type_conversion();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, type_conversion187.Tree);

                    }
                    break;
                case 3 :
                    // Ada95.g:427:3: {...}? => ( options {backtrack=true; } : slice -> slice | indexed_component -> indexed_component | explicit_dereference -> explicit_dereference | selected_component -> selected_component | direct_name -> ^( OBJECT direct_name ) )
                    {
                    	if ( !(( IsObject( input.LT( 1 ).Text ) )) ) 
                    	{
                    	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    	    throw new FailedPredicateException(input, "name", " IsObject( input.LT( 1 ).Text ) ");
                    	}
                    	// Ada95.g:427:41: ( options {backtrack=true; } : slice -> slice | indexed_component -> indexed_component | explicit_dereference -> explicit_dereference | selected_component -> selected_component | direct_name -> ^( OBJECT direct_name ) )
                    	int alt44 = 5;
                    	int LA44_0 = input.LA(1);

                    	if ( (LA44_0 == IDENTIFIER) )
                    	{
                    	    int LA44_1 = input.LA(2);

                    	    if ( (synpred6_Ada95()) )
                    	    {
                    	        alt44 = 1;
                    	    }
                    	    else if ( (synpred7_Ada95()) )
                    	    {
                    	        alt44 = 2;
                    	    }
                    	    else if ( (synpred8_Ada95()) )
                    	    {
                    	        alt44 = 3;
                    	    }
                    	    else if ( (synpred9_Ada95()) )
                    	    {
                    	        alt44 = 4;
                    	    }
                    	    else if ( (true) )
                    	    {
                    	        alt44 = 5;
                    	    }
                    	    else 
                    	    {
                    	        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    	        NoViableAltException nvae_d44s1 =
                    	            new NoViableAltException("", 44, 1, input);

                    	        throw nvae_d44s1;
                    	    }
                    	}
                    	else 
                    	{
                    	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    	    NoViableAltException nvae_d44s0 =
                    	        new NoViableAltException("", 44, 0, input);

                    	    throw nvae_d44s0;
                    	}
                    	switch (alt44) 
                    	{
                    	    case 1 :
                    	        // Ada95.g:428:14: slice
                    	        {
                    	        	PushFollow(FOLLOW_slice_in_name1875);
                    	        	slice188 = slice();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( (state.backtracking==0) ) stream_slice.Add(slice188.Tree);


                    	        	// AST REWRITE
                    	        	// elements:          slice
                    	        	// token labels:      
                    	        	// rule labels:       retval
                    	        	// token list labels: 
                    	        	// rule list labels:  
                    	        	// wildcard labels: 
                    	        	if ( (state.backtracking==0) ) {
                    	        	retval.Tree = root_0;
                    	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	        	root_0 = (CommonTree)adaptor.GetNilNode();
                    	        	// 428:24: -> slice
                    	        	{
                    	        	    adaptor.AddChild(root_0, stream_slice.NextTree());

                    	        	}

                    	        	retval.Tree = root_0;retval.Tree = root_0;}
                    	        }
                    	        break;
                    	    case 2 :
                    	        // Ada95.g:429:14: indexed_component
                    	        {
                    	        	PushFollow(FOLLOW_indexed_component_in_name1900);
                    	        	indexed_component189 = indexed_component();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( (state.backtracking==0) ) stream_indexed_component.Add(indexed_component189.Tree);


                    	        	// AST REWRITE
                    	        	// elements:          indexed_component
                    	        	// token labels:      
                    	        	// rule labels:       retval
                    	        	// token list labels: 
                    	        	// rule list labels:  
                    	        	// wildcard labels: 
                    	        	if ( (state.backtracking==0) ) {
                    	        	retval.Tree = root_0;
                    	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	        	root_0 = (CommonTree)adaptor.GetNilNode();
                    	        	// 429:33: -> indexed_component
                    	        	{
                    	        	    adaptor.AddChild(root_0, stream_indexed_component.NextTree());

                    	        	}

                    	        	retval.Tree = root_0;retval.Tree = root_0;}
                    	        }
                    	        break;
                    	    case 3 :
                    	        // Ada95.g:430:14: explicit_dereference
                    	        {
                    	        	PushFollow(FOLLOW_explicit_dereference_in_name1922);
                    	        	explicit_dereference190 = explicit_dereference();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( (state.backtracking==0) ) stream_explicit_dereference.Add(explicit_dereference190.Tree);


                    	        	// AST REWRITE
                    	        	// elements:          explicit_dereference
                    	        	// token labels:      
                    	        	// rule labels:       retval
                    	        	// token list labels: 
                    	        	// rule list labels:  
                    	        	// wildcard labels: 
                    	        	if ( (state.backtracking==0) ) {
                    	        	retval.Tree = root_0;
                    	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	        	root_0 = (CommonTree)adaptor.GetNilNode();
                    	        	// 430:35: -> explicit_dereference
                    	        	{
                    	        	    adaptor.AddChild(root_0, stream_explicit_dereference.NextTree());

                    	        	}

                    	        	retval.Tree = root_0;retval.Tree = root_0;}
                    	        }
                    	        break;
                    	    case 4 :
                    	        // Ada95.g:431:14: selected_component
                    	        {
                    	        	PushFollow(FOLLOW_selected_component_in_name1943);
                    	        	selected_component191 = selected_component();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( (state.backtracking==0) ) stream_selected_component.Add(selected_component191.Tree);


                    	        	// AST REWRITE
                    	        	// elements:          selected_component
                    	        	// token labels:      
                    	        	// rule labels:       retval
                    	        	// token list labels: 
                    	        	// rule list labels:  
                    	        	// wildcard labels: 
                    	        	if ( (state.backtracking==0) ) {
                    	        	retval.Tree = root_0;
                    	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	        	root_0 = (CommonTree)adaptor.GetNilNode();
                    	        	// 431:34: -> selected_component
                    	        	{
                    	        	    adaptor.AddChild(root_0, stream_selected_component.NextTree());

                    	        	}

                    	        	retval.Tree = root_0;retval.Tree = root_0;}
                    	        }
                    	        break;
                    	    case 5 :
                    	        // Ada95.g:432:14: direct_name
                    	        {
                    	        	PushFollow(FOLLOW_direct_name_in_name1965);
                    	        	direct_name192 = direct_name();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( (state.backtracking==0) ) stream_direct_name.Add(direct_name192.Tree);


                    	        	// AST REWRITE
                    	        	// elements:          direct_name
                    	        	// token labels:      
                    	        	// rule labels:       retval
                    	        	// token list labels: 
                    	        	// rule list labels:  
                    	        	// wildcard labels: 
                    	        	if ( (state.backtracking==0) ) {
                    	        	retval.Tree = root_0;
                    	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	        	root_0 = (CommonTree)adaptor.GetNilNode();
                    	        	// 432:29: -> ^( OBJECT direct_name )
                    	        	{
                    	        	    // Ada95.g:432:32: ^( OBJECT direct_name )
                    	        	    {
                    	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
                    	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(OBJECT, "OBJECT"), root_1);

                    	        	    adaptor.AddChild(root_1, stream_direct_name.NextTree());

                    	        	    adaptor.AddChild(root_0, root_1);
                    	        	    }

                    	        	}

                    	        	retval.Tree = root_0;retval.Tree = root_0;}
                    	        }
                    	        break;

                    	}


                    }
                    break;
                case 4 :
                    // Ada95.g:434:13: attribute_reference
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_attribute_reference_in_name2008);
                    	attribute_reference193 = attribute_reference();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, attribute_reference193.Tree);

                    }
                    break;
                case 5 :
                    // Ada95.g:435:13: CHARACTER_LITERAL
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	CHARACTER_LITERAL194=(IToken)Match(input,CHARACTER_LITERAL,FOLLOW_CHARACTER_LITERAL_in_name2024); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{CHARACTER_LITERAL194_tree = (CommonTree)adaptor.Create(CHARACTER_LITERAL194);
                    		adaptor.AddChild(root_0, CHARACTER_LITERAL194_tree);
                    	}

                    }
                    break;
                case 6 :
                    // Ada95.g:437:3: ( options {backtrack=true; } : type_conversion -> type_conversion | indexed_component -> indexed_component | slice -> slice | explicit_dereference -> explicit_dereference | selected_component -> selected_component | function_call -> function_call )
                    {
                    	// Ada95.g:437:3: ( options {backtrack=true; } : type_conversion -> type_conversion | indexed_component -> indexed_component | slice -> slice | explicit_dereference -> explicit_dereference | selected_component -> selected_component | function_call -> function_call )
                    	int alt45 = 6;
                    	int LA45_0 = input.LA(1);

                    	if ( (LA45_0 == IDENTIFIER) )
                    	{
                    	    int LA45_1 = input.LA(2);

                    	    if ( (synpred10_Ada95()) )
                    	    {
                    	        alt45 = 1;
                    	    }
                    	    else if ( (synpred11_Ada95()) )
                    	    {
                    	        alt45 = 2;
                    	    }
                    	    else if ( (synpred12_Ada95()) )
                    	    {
                    	        alt45 = 3;
                    	    }
                    	    else if ( (synpred13_Ada95()) )
                    	    {
                    	        alt45 = 4;
                    	    }
                    	    else if ( (synpred14_Ada95()) )
                    	    {
                    	        alt45 = 5;
                    	    }
                    	    else if ( (true) )
                    	    {
                    	        alt45 = 6;
                    	    }
                    	    else 
                    	    {
                    	        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    	        NoViableAltException nvae_d45s1 =
                    	            new NoViableAltException("", 45, 1, input);

                    	        throw nvae_d45s1;
                    	    }
                    	}
                    	else 
                    	{
                    	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    	    NoViableAltException nvae_d45s0 =
                    	        new NoViableAltException("", 45, 0, input);

                    	    throw nvae_d45s0;
                    	}
                    	switch (alt45) 
                    	{
                    	    case 1 :
                    	        // Ada95.g:438:4: type_conversion
                    	        {
                    	        	PushFollow(FOLLOW_type_conversion_in_name2051);
                    	        	type_conversion195 = type_conversion();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( (state.backtracking==0) ) stream_type_conversion.Add(type_conversion195.Tree);


                    	        	// AST REWRITE
                    	        	// elements:          type_conversion
                    	        	// token labels:      
                    	        	// rule labels:       retval
                    	        	// token list labels: 
                    	        	// rule list labels:  
                    	        	// wildcard labels: 
                    	        	if ( (state.backtracking==0) ) {
                    	        	retval.Tree = root_0;
                    	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	        	root_0 = (CommonTree)adaptor.GetNilNode();
                    	        	// 438:22: -> type_conversion
                    	        	{
                    	        	    adaptor.AddChild(root_0, stream_type_conversion.NextTree());

                    	        	}

                    	        	retval.Tree = root_0;retval.Tree = root_0;}
                    	        }
                    	        break;
                    	    case 2 :
                    	        // Ada95.g:439:4: indexed_component
                    	        {
                    	        	PushFollow(FOLLOW_indexed_component_in_name2067);
                    	        	indexed_component196 = indexed_component();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( (state.backtracking==0) ) stream_indexed_component.Add(indexed_component196.Tree);


                    	        	// AST REWRITE
                    	        	// elements:          indexed_component
                    	        	// token labels:      
                    	        	// rule labels:       retval
                    	        	// token list labels: 
                    	        	// rule list labels:  
                    	        	// wildcard labels: 
                    	        	if ( (state.backtracking==0) ) {
                    	        	retval.Tree = root_0;
                    	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	        	root_0 = (CommonTree)adaptor.GetNilNode();
                    	        	// 439:23: -> indexed_component
                    	        	{
                    	        	    adaptor.AddChild(root_0, stream_indexed_component.NextTree());

                    	        	}

                    	        	retval.Tree = root_0;retval.Tree = root_0;}
                    	        }
                    	        break;
                    	    case 3 :
                    	        // Ada95.g:440:4: slice
                    	        {
                    	        	PushFollow(FOLLOW_slice_in_name2081);
                    	        	slice197 = slice();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( (state.backtracking==0) ) stream_slice.Add(slice197.Tree);


                    	        	// AST REWRITE
                    	        	// elements:          slice
                    	        	// token labels:      
                    	        	// rule labels:       retval
                    	        	// token list labels: 
                    	        	// rule list labels:  
                    	        	// wildcard labels: 
                    	        	if ( (state.backtracking==0) ) {
                    	        	retval.Tree = root_0;
                    	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	        	root_0 = (CommonTree)adaptor.GetNilNode();
                    	        	// 440:14: -> slice
                    	        	{
                    	        	    adaptor.AddChild(root_0, stream_slice.NextTree());

                    	        	}

                    	        	retval.Tree = root_0;retval.Tree = root_0;}
                    	        }
                    	        break;
                    	    case 4 :
                    	        // Ada95.g:441:4: explicit_dereference
                    	        {
                    	        	PushFollow(FOLLOW_explicit_dereference_in_name2101);
                    	        	explicit_dereference198 = explicit_dereference();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( (state.backtracking==0) ) stream_explicit_dereference.Add(explicit_dereference198.Tree);


                    	        	// AST REWRITE
                    	        	// elements:          explicit_dereference
                    	        	// token labels:      
                    	        	// rule labels:       retval
                    	        	// token list labels: 
                    	        	// rule list labels:  
                    	        	// wildcard labels: 
                    	        	if ( (state.backtracking==0) ) {
                    	        	retval.Tree = root_0;
                    	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	        	root_0 = (CommonTree)adaptor.GetNilNode();
                    	        	// 441:25: -> explicit_dereference
                    	        	{
                    	        	    adaptor.AddChild(root_0, stream_explicit_dereference.NextTree());

                    	        	}

                    	        	retval.Tree = root_0;retval.Tree = root_0;}
                    	        }
                    	        break;
                    	    case 5 :
                    	        // Ada95.g:442:4: selected_component
                    	        {
                    	        	PushFollow(FOLLOW_selected_component_in_name2113);
                    	        	selected_component199 = selected_component();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( (state.backtracking==0) ) stream_selected_component.Add(selected_component199.Tree);


                    	        	// AST REWRITE
                    	        	// elements:          selected_component
                    	        	// token labels:      
                    	        	// rule labels:       retval
                    	        	// token list labels: 
                    	        	// rule list labels:  
                    	        	// wildcard labels: 
                    	        	if ( (state.backtracking==0) ) {
                    	        	retval.Tree = root_0;
                    	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	        	root_0 = (CommonTree)adaptor.GetNilNode();
                    	        	// 442:24: -> selected_component
                    	        	{
                    	        	    adaptor.AddChild(root_0, stream_selected_component.NextTree());

                    	        	}

                    	        	retval.Tree = root_0;retval.Tree = root_0;}
                    	        }
                    	        break;
                    	    case 6 :
                    	        // Ada95.g:443:4: function_call
                    	        {
                    	        	PushFollow(FOLLOW_function_call_in_name2127);
                    	        	function_call200 = function_call();
                    	        	state.followingStackPointer--;
                    	        	if (state.failed) return retval;
                    	        	if ( (state.backtracking==0) ) stream_function_call.Add(function_call200.Tree);


                    	        	// AST REWRITE
                    	        	// elements:          function_call
                    	        	// token labels:      
                    	        	// rule labels:       retval
                    	        	// token list labels: 
                    	        	// rule list labels:  
                    	        	// wildcard labels: 
                    	        	if ( (state.backtracking==0) ) {
                    	        	retval.Tree = root_0;
                    	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	        	root_0 = (CommonTree)adaptor.GetNilNode();
                    	        	// 443:20: -> function_call
                    	        	{
                    	        	    adaptor.AddChild(root_0, stream_function_call.NextTree());

                    	        	}

                    	        	retval.Tree = root_0;retval.Tree = root_0;}
                    	        }
                    	        break;

                    	}


                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 58, name_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "name"

    public class direct_name_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "direct_name"
    // Ada95.g:447:1: direct_name : IDENTIFIER ;
    public Ada95Parser.direct_name_return direct_name() // throws RecognitionException [1]
    {   
        Ada95Parser.direct_name_return retval = new Ada95Parser.direct_name_return();
        retval.Start = input.LT(1);
        int direct_name_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IDENTIFIER201 = null;

        CommonTree IDENTIFIER201_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 59) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:448:2: ( IDENTIFIER )
            // Ada95.g:448:4: IDENTIFIER
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	IDENTIFIER201=(IToken)Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_direct_name2151); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{IDENTIFIER201_tree = (CommonTree)adaptor.Create(IDENTIFIER201);
            		adaptor.AddChild(root_0, IDENTIFIER201_tree);
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 59, direct_name_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "direct_name"

    public class explicit_dereference_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "explicit_dereference"
    // Ada95.g:450:1: explicit_dereference : direct_name DOT ALL -> ^( EXPLICIT_DEREFERENCE direct_name ) ;
    public Ada95Parser.explicit_dereference_return explicit_dereference() // throws RecognitionException [1]
    {   
        Ada95Parser.explicit_dereference_return retval = new Ada95Parser.explicit_dereference_return();
        retval.Start = input.LT(1);
        int explicit_dereference_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken DOT203 = null;
        IToken ALL204 = null;
        Ada95Parser.direct_name_return direct_name202 = default(Ada95Parser.direct_name_return);


        CommonTree DOT203_tree=null;
        CommonTree ALL204_tree=null;
        RewriteRuleTokenStream stream_DOT = new RewriteRuleTokenStream(adaptor,"token DOT");
        RewriteRuleTokenStream stream_ALL = new RewriteRuleTokenStream(adaptor,"token ALL");
        RewriteRuleSubtreeStream stream_direct_name = new RewriteRuleSubtreeStream(adaptor,"rule direct_name");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 60) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:451:2: ( direct_name DOT ALL -> ^( EXPLICIT_DEREFERENCE direct_name ) )
            // Ada95.g:451:4: direct_name DOT ALL
            {
            	PushFollow(FOLLOW_direct_name_in_explicit_dereference2161);
            	direct_name202 = direct_name();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_direct_name.Add(direct_name202.Tree);
            	DOT203=(IToken)Match(input,DOT,FOLLOW_DOT_in_explicit_dereference2163); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_DOT.Add(DOT203);

            	ALL204=(IToken)Match(input,ALL,FOLLOW_ALL_in_explicit_dereference2165); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_ALL.Add(ALL204);



            	// AST REWRITE
            	// elements:          direct_name
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 451:24: -> ^( EXPLICIT_DEREFERENCE direct_name )
            	{
            	    // Ada95.g:451:27: ^( EXPLICIT_DEREFERENCE direct_name )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(EXPLICIT_DEREFERENCE, "EXPLICIT_DEREFERENCE"), root_1);

            	    adaptor.AddChild(root_1, stream_direct_name.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 60, explicit_dereference_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "explicit_dereference"

    public class indexed_component_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "indexed_component"
    // Ada95.g:453:1: indexed_component : direct_name LPARANTHESIS expression ( COMMA expression )* RPARANTHESIS -> ^( INDEXED_COMPONENT direct_name ( expression )+ ) ;
    public Ada95Parser.indexed_component_return indexed_component() // throws RecognitionException [1]
    {   
        Ada95Parser.indexed_component_return retval = new Ada95Parser.indexed_component_return();
        retval.Start = input.LT(1);
        int indexed_component_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken LPARANTHESIS206 = null;
        IToken COMMA208 = null;
        IToken RPARANTHESIS210 = null;
        Ada95Parser.direct_name_return direct_name205 = default(Ada95Parser.direct_name_return);

        Ada95Parser.expression_return expression207 = default(Ada95Parser.expression_return);

        Ada95Parser.expression_return expression209 = default(Ada95Parser.expression_return);


        CommonTree LPARANTHESIS206_tree=null;
        CommonTree COMMA208_tree=null;
        CommonTree RPARANTHESIS210_tree=null;
        RewriteRuleTokenStream stream_LPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token LPARANTHESIS");
        RewriteRuleTokenStream stream_RPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token RPARANTHESIS");
        RewriteRuleTokenStream stream_COMMA = new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleSubtreeStream stream_expression = new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_direct_name = new RewriteRuleSubtreeStream(adaptor,"rule direct_name");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 61) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:454:2: ( direct_name LPARANTHESIS expression ( COMMA expression )* RPARANTHESIS -> ^( INDEXED_COMPONENT direct_name ( expression )+ ) )
            // Ada95.g:454:4: direct_name LPARANTHESIS expression ( COMMA expression )* RPARANTHESIS
            {
            	PushFollow(FOLLOW_direct_name_in_indexed_component2186);
            	direct_name205 = direct_name();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_direct_name.Add(direct_name205.Tree);
            	LPARANTHESIS206=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_indexed_component2188); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_LPARANTHESIS.Add(LPARANTHESIS206);

            	PushFollow(FOLLOW_expression_in_indexed_component2190);
            	expression207 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_expression.Add(expression207.Tree);
            	// Ada95.g:454:40: ( COMMA expression )*
            	do 
            	{
            	    int alt47 = 2;
            	    int LA47_0 = input.LA(1);

            	    if ( (LA47_0 == COMMA) )
            	    {
            	        alt47 = 1;
            	    }


            	    switch (alt47) 
            		{
            			case 1 :
            			    // Ada95.g:454:42: COMMA expression
            			    {
            			    	COMMA208=(IToken)Match(input,COMMA,FOLLOW_COMMA_in_indexed_component2194); if (state.failed) return retval; 
            			    	if ( (state.backtracking==0) ) stream_COMMA.Add(COMMA208);

            			    	PushFollow(FOLLOW_expression_in_indexed_component2196);
            			    	expression209 = expression();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( (state.backtracking==0) ) stream_expression.Add(expression209.Tree);

            			    }
            			    break;

            			default:
            			    goto loop47;
            	    }
            	} while (true);

            	loop47:
            		;	// Stops C# compiler whining that label 'loop47' has no statements

            	RPARANTHESIS210=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_indexed_component2201); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_RPARANTHESIS.Add(RPARANTHESIS210);



            	// AST REWRITE
            	// elements:          expression, direct_name
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 454:75: -> ^( INDEXED_COMPONENT direct_name ( expression )+ )
            	{
            	    // Ada95.g:454:78: ^( INDEXED_COMPONENT direct_name ( expression )+ )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(INDEXED_COMPONENT, "INDEXED_COMPONENT"), root_1);

            	    adaptor.AddChild(root_1, stream_direct_name.NextTree());
            	    if ( !(stream_expression.HasNext()) ) {
            	        throw new RewriteEarlyExitException();
            	    }
            	    while ( stream_expression.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_expression.NextTree());

            	    }
            	    stream_expression.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 61, indexed_component_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "indexed_component"

    public class slice_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "slice"
    // Ada95.g:456:1: slice : direct_name LPARANTHESIS discrete_range RPARANTHESIS -> ^( SLICE direct_name discrete_range ) ;
    public Ada95Parser.slice_return slice() // throws RecognitionException [1]
    {   
        Ada95Parser.slice_return retval = new Ada95Parser.slice_return();
        retval.Start = input.LT(1);
        int slice_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken LPARANTHESIS212 = null;
        IToken RPARANTHESIS214 = null;
        Ada95Parser.direct_name_return direct_name211 = default(Ada95Parser.direct_name_return);

        Ada95Parser.discrete_range_return discrete_range213 = default(Ada95Parser.discrete_range_return);


        CommonTree LPARANTHESIS212_tree=null;
        CommonTree RPARANTHESIS214_tree=null;
        RewriteRuleTokenStream stream_LPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token LPARANTHESIS");
        RewriteRuleTokenStream stream_RPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token RPARANTHESIS");
        RewriteRuleSubtreeStream stream_direct_name = new RewriteRuleSubtreeStream(adaptor,"rule direct_name");
        RewriteRuleSubtreeStream stream_discrete_range = new RewriteRuleSubtreeStream(adaptor,"rule discrete_range");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 62) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:457:2: ( direct_name LPARANTHESIS discrete_range RPARANTHESIS -> ^( SLICE direct_name discrete_range ) )
            // Ada95.g:457:4: direct_name LPARANTHESIS discrete_range RPARANTHESIS
            {
            	PushFollow(FOLLOW_direct_name_in_slice2224);
            	direct_name211 = direct_name();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_direct_name.Add(direct_name211.Tree);
            	LPARANTHESIS212=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_slice2226); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_LPARANTHESIS.Add(LPARANTHESIS212);

            	PushFollow(FOLLOW_discrete_range_in_slice2228);
            	discrete_range213 = discrete_range();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_discrete_range.Add(discrete_range213.Tree);
            	RPARANTHESIS214=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_slice2230); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_RPARANTHESIS.Add(RPARANTHESIS214);



            	// AST REWRITE
            	// elements:          discrete_range, direct_name
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 457:57: -> ^( SLICE direct_name discrete_range )
            	{
            	    // Ada95.g:457:60: ^( SLICE direct_name discrete_range )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(SLICE, "SLICE"), root_1);

            	    adaptor.AddChild(root_1, stream_direct_name.NextTree());
            	    adaptor.AddChild(root_1, stream_discrete_range.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 62, slice_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "slice"

    public class selected_component_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "selected_component"
    // Ada95.g:459:1: selected_component : direct_name DOT selector_name -> ^( SELECTED_COMPONENT direct_name selector_name ) ;
    public Ada95Parser.selected_component_return selected_component() // throws RecognitionException [1]
    {   
        Ada95Parser.selected_component_return retval = new Ada95Parser.selected_component_return();
        retval.Start = input.LT(1);
        int selected_component_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken DOT216 = null;
        Ada95Parser.direct_name_return direct_name215 = default(Ada95Parser.direct_name_return);

        Ada95Parser.selector_name_return selector_name217 = default(Ada95Parser.selector_name_return);


        CommonTree DOT216_tree=null;
        RewriteRuleTokenStream stream_DOT = new RewriteRuleTokenStream(adaptor,"token DOT");
        RewriteRuleSubtreeStream stream_selector_name = new RewriteRuleSubtreeStream(adaptor,"rule selector_name");
        RewriteRuleSubtreeStream stream_direct_name = new RewriteRuleSubtreeStream(adaptor,"rule direct_name");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 63) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:460:2: ( direct_name DOT selector_name -> ^( SELECTED_COMPONENT direct_name selector_name ) )
            // Ada95.g:460:4: direct_name DOT selector_name
            {
            	PushFollow(FOLLOW_direct_name_in_selected_component2252);
            	direct_name215 = direct_name();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_direct_name.Add(direct_name215.Tree);
            	DOT216=(IToken)Match(input,DOT,FOLLOW_DOT_in_selected_component2254); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_DOT.Add(DOT216);

            	PushFollow(FOLLOW_selector_name_in_selected_component2256);
            	selector_name217 = selector_name();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_selector_name.Add(selector_name217.Tree);


            	// AST REWRITE
            	// elements:          selector_name, direct_name
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 460:34: -> ^( SELECTED_COMPONENT direct_name selector_name )
            	{
            	    // Ada95.g:460:37: ^( SELECTED_COMPONENT direct_name selector_name )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(SELECTED_COMPONENT, "SELECTED_COMPONENT"), root_1);

            	    adaptor.AddChild(root_1, stream_direct_name.NextTree());
            	    adaptor.AddChild(root_1, stream_selector_name.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 63, selected_component_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "selected_component"

    public class selector_name_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "selector_name"
    // Ada95.g:462:1: selector_name : ( IDENTIFIER | CHARACTER_LITERAL | operator_symbol );
    public Ada95Parser.selector_name_return selector_name() // throws RecognitionException [1]
    {   
        Ada95Parser.selector_name_return retval = new Ada95Parser.selector_name_return();
        retval.Start = input.LT(1);
        int selector_name_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IDENTIFIER218 = null;
        IToken CHARACTER_LITERAL219 = null;
        Ada95Parser.operator_symbol_return operator_symbol220 = default(Ada95Parser.operator_symbol_return);


        CommonTree IDENTIFIER218_tree=null;
        CommonTree CHARACTER_LITERAL219_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 64) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:463:2: ( IDENTIFIER | CHARACTER_LITERAL | operator_symbol )
            int alt48 = 3;
            switch ( input.LA(1) ) 
            {
            case IDENTIFIER:
            	{
                alt48 = 1;
                }
                break;
            case CHARACTER_LITERAL:
            	{
                alt48 = 2;
                }
                break;
            case STRING_LITERAL:
            	{
                alt48 = 3;
                }
                break;
            	default:
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    NoViableAltException nvae_d48s0 =
            	        new NoViableAltException("", 48, 0, input);

            	    throw nvae_d48s0;
            }

            switch (alt48) 
            {
                case 1 :
                    // Ada95.g:464:3: IDENTIFIER
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	IDENTIFIER218=(IToken)Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_selector_name2280); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{IDENTIFIER218_tree = (CommonTree)adaptor.Create(IDENTIFIER218);
                    		adaptor.AddChild(root_0, IDENTIFIER218_tree);
                    	}

                    }
                    break;
                case 2 :
                    // Ada95.g:465:3: CHARACTER_LITERAL
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	CHARACTER_LITERAL219=(IToken)Match(input,CHARACTER_LITERAL,FOLLOW_CHARACTER_LITERAL_in_selector_name2286); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{CHARACTER_LITERAL219_tree = (CommonTree)adaptor.Create(CHARACTER_LITERAL219);
                    		adaptor.AddChild(root_0, CHARACTER_LITERAL219_tree);
                    	}

                    }
                    break;
                case 3 :
                    // Ada95.g:466:3: operator_symbol
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_operator_symbol_in_selector_name2292);
                    	operator_symbol220 = operator_symbol();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, operator_symbol220.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 64, selector_name_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "selector_name"

    public class attribute_reference_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "attribute_reference"
    // Ada95.g:469:1: attribute_reference : direct_name APOSTROPHE attribute_designator -> ^( ATTRIBUTE_REFERENCE direct_name attribute_designator ) ;
    public Ada95Parser.attribute_reference_return attribute_reference() // throws RecognitionException [1]
    {   
        Ada95Parser.attribute_reference_return retval = new Ada95Parser.attribute_reference_return();
        retval.Start = input.LT(1);
        int attribute_reference_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken APOSTROPHE222 = null;
        Ada95Parser.direct_name_return direct_name221 = default(Ada95Parser.direct_name_return);

        Ada95Parser.attribute_designator_return attribute_designator223 = default(Ada95Parser.attribute_designator_return);


        CommonTree APOSTROPHE222_tree=null;
        RewriteRuleTokenStream stream_APOSTROPHE = new RewriteRuleTokenStream(adaptor,"token APOSTROPHE");
        RewriteRuleSubtreeStream stream_attribute_designator = new RewriteRuleSubtreeStream(adaptor,"rule attribute_designator");
        RewriteRuleSubtreeStream stream_direct_name = new RewriteRuleSubtreeStream(adaptor,"rule direct_name");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 65) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:470:2: ( direct_name APOSTROPHE attribute_designator -> ^( ATTRIBUTE_REFERENCE direct_name attribute_designator ) )
            // Ada95.g:470:4: direct_name APOSTROPHE attribute_designator
            {
            	PushFollow(FOLLOW_direct_name_in_attribute_reference2303);
            	direct_name221 = direct_name();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_direct_name.Add(direct_name221.Tree);
            	APOSTROPHE222=(IToken)Match(input,APOSTROPHE,FOLLOW_APOSTROPHE_in_attribute_reference2305); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_APOSTROPHE.Add(APOSTROPHE222);

            	PushFollow(FOLLOW_attribute_designator_in_attribute_reference2307);
            	attribute_designator223 = attribute_designator();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_attribute_designator.Add(attribute_designator223.Tree);


            	// AST REWRITE
            	// elements:          direct_name, attribute_designator
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 470:48: -> ^( ATTRIBUTE_REFERENCE direct_name attribute_designator )
            	{
            	    // Ada95.g:470:51: ^( ATTRIBUTE_REFERENCE direct_name attribute_designator )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(ATTRIBUTE_REFERENCE, "ATTRIBUTE_REFERENCE"), root_1);

            	    adaptor.AddChild(root_1, stream_direct_name.NextTree());
            	    adaptor.AddChild(root_1, stream_attribute_designator.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 65, attribute_reference_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "attribute_reference"

    public class attribute_designator_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "attribute_designator"
    // Ada95.g:472:1: attribute_designator : IDENTIFIER ( LPARANTHESIS expression RPARANTHESIS )? ;
    public Ada95Parser.attribute_designator_return attribute_designator() // throws RecognitionException [1]
    {   
        Ada95Parser.attribute_designator_return retval = new Ada95Parser.attribute_designator_return();
        retval.Start = input.LT(1);
        int attribute_designator_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IDENTIFIER224 = null;
        IToken LPARANTHESIS225 = null;
        IToken RPARANTHESIS227 = null;
        Ada95Parser.expression_return expression226 = default(Ada95Parser.expression_return);


        CommonTree IDENTIFIER224_tree=null;
        CommonTree LPARANTHESIS225_tree=null;
        CommonTree RPARANTHESIS227_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 66) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:473:2: ( IDENTIFIER ( LPARANTHESIS expression RPARANTHESIS )? )
            // Ada95.g:473:4: IDENTIFIER ( LPARANTHESIS expression RPARANTHESIS )?
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	IDENTIFIER224=(IToken)Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_attribute_designator2329); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{IDENTIFIER224_tree = (CommonTree)adaptor.Create(IDENTIFIER224);
            		adaptor.AddChild(root_0, IDENTIFIER224_tree);
            	}
            	// Ada95.g:473:15: ( LPARANTHESIS expression RPARANTHESIS )?
            	int alt49 = 2;
            	int LA49_0 = input.LA(1);

            	if ( (LA49_0 == LPARANTHESIS) )
            	{
            	    alt49 = 1;
            	}
            	switch (alt49) 
            	{
            	    case 1 :
            	        // Ada95.g:473:17: LPARANTHESIS expression RPARANTHESIS
            	        {
            	        	LPARANTHESIS225=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_attribute_designator2333); if (state.failed) return retval;
            	        	PushFollow(FOLLOW_expression_in_attribute_designator2336);
            	        	expression226 = expression();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression226.Tree);
            	        	RPARANTHESIS227=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_attribute_designator2338); if (state.failed) return retval;

            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 66, attribute_designator_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "attribute_designator"

    public class expression_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "expression"
    // Ada95.g:515:1: expression : relation ( ( AND relation )+ -> ^( EXPRESSION AND ( relation )+ ) | ( AND THEN relation )+ -> ^( EXPRESSION THEN ( relation )+ ) | ( OR relation )+ -> ^( EXPRESSION OR ( relation )+ ) | ( OR ELSE relation )+ -> ^( EXPRESSION ELSE ( relation )+ ) | ( XOR relation )+ -> ^( EXPRESSION XOR ( relation )+ ) | -> ^( EXPRESSION relation ) ) ;
    public Ada95Parser.expression_return expression() // throws RecognitionException [1]
    {   
        Ada95Parser.expression_return retval = new Ada95Parser.expression_return();
        retval.Start = input.LT(1);
        int expression_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken AND229 = null;
        IToken AND231 = null;
        IToken THEN232 = null;
        IToken OR234 = null;
        IToken OR236 = null;
        IToken ELSE237 = null;
        IToken XOR239 = null;
        Ada95Parser.relation_return relation228 = default(Ada95Parser.relation_return);

        Ada95Parser.relation_return relation230 = default(Ada95Parser.relation_return);

        Ada95Parser.relation_return relation233 = default(Ada95Parser.relation_return);

        Ada95Parser.relation_return relation235 = default(Ada95Parser.relation_return);

        Ada95Parser.relation_return relation238 = default(Ada95Parser.relation_return);

        Ada95Parser.relation_return relation240 = default(Ada95Parser.relation_return);


        CommonTree AND229_tree=null;
        CommonTree AND231_tree=null;
        CommonTree THEN232_tree=null;
        CommonTree OR234_tree=null;
        CommonTree OR236_tree=null;
        CommonTree ELSE237_tree=null;
        CommonTree XOR239_tree=null;
        RewriteRuleTokenStream stream_XOR = new RewriteRuleTokenStream(adaptor,"token XOR");
        RewriteRuleTokenStream stream_THEN = new RewriteRuleTokenStream(adaptor,"token THEN");
        RewriteRuleTokenStream stream_AND = new RewriteRuleTokenStream(adaptor,"token AND");
        RewriteRuleTokenStream stream_OR = new RewriteRuleTokenStream(adaptor,"token OR");
        RewriteRuleTokenStream stream_ELSE = new RewriteRuleTokenStream(adaptor,"token ELSE");
        RewriteRuleSubtreeStream stream_relation = new RewriteRuleSubtreeStream(adaptor,"rule relation");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 67) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:516:2: ( relation ( ( AND relation )+ -> ^( EXPRESSION AND ( relation )+ ) | ( AND THEN relation )+ -> ^( EXPRESSION THEN ( relation )+ ) | ( OR relation )+ -> ^( EXPRESSION OR ( relation )+ ) | ( OR ELSE relation )+ -> ^( EXPRESSION ELSE ( relation )+ ) | ( XOR relation )+ -> ^( EXPRESSION XOR ( relation )+ ) | -> ^( EXPRESSION relation ) ) )
            // Ada95.g:517:3: relation ( ( AND relation )+ -> ^( EXPRESSION AND ( relation )+ ) | ( AND THEN relation )+ -> ^( EXPRESSION THEN ( relation )+ ) | ( OR relation )+ -> ^( EXPRESSION OR ( relation )+ ) | ( OR ELSE relation )+ -> ^( EXPRESSION ELSE ( relation )+ ) | ( XOR relation )+ -> ^( EXPRESSION XOR ( relation )+ ) | -> ^( EXPRESSION relation ) )
            {
            	PushFollow(FOLLOW_relation_in_expression2356);
            	relation228 = relation();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_relation.Add(relation228.Tree);
            	// Ada95.g:518:3: ( ( AND relation )+ -> ^( EXPRESSION AND ( relation )+ ) | ( AND THEN relation )+ -> ^( EXPRESSION THEN ( relation )+ ) | ( OR relation )+ -> ^( EXPRESSION OR ( relation )+ ) | ( OR ELSE relation )+ -> ^( EXPRESSION ELSE ( relation )+ ) | ( XOR relation )+ -> ^( EXPRESSION XOR ( relation )+ ) | -> ^( EXPRESSION relation ) )
            	int alt55 = 6;
            	switch ( input.LA(1) ) 
            	{
            	case AND:
            		{
            	    int LA55_1 = input.LA(2);

            	    if ( (LA55_1 == THEN) )
            	    {
            	        alt55 = 2;
            	    }
            	    else if ( (LA55_1 == IDENTIFIER || LA55_1 == LPARANTHESIS || LA55_1 == CHARACTER_LITERAL || LA55_1 == NULL || LA55_1 == NOT || (LA55_1 >= ABS && LA55_1 <= STRING_LITERAL) || (LA55_1 >= PLUS && LA55_1 <= MINUS)) )
            	    {
            	        alt55 = 1;
            	    }
            	    else 
            	    {
            	        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        NoViableAltException nvae_d55s1 =
            	            new NoViableAltException("", 55, 1, input);

            	        throw nvae_d55s1;
            	    }
            	    }
            	    break;
            	case OR:
            		{
            	    int LA55_2 = input.LA(2);

            	    if ( (LA55_2 == ELSE) )
            	    {
            	        alt55 = 4;
            	    }
            	    else if ( (LA55_2 == IDENTIFIER || LA55_2 == LPARANTHESIS || LA55_2 == CHARACTER_LITERAL || LA55_2 == NULL || LA55_2 == NOT || (LA55_2 >= ABS && LA55_2 <= STRING_LITERAL) || (LA55_2 >= PLUS && LA55_2 <= MINUS)) )
            	    {
            	        alt55 = 3;
            	    }
            	    else 
            	    {
            	        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        NoViableAltException nvae_d55s2 =
            	            new NoViableAltException("", 55, 2, input);

            	        throw nvae_d55s2;
            	    }
            	    }
            	    break;
            	case XOR:
            		{
            	    alt55 = 5;
            	    }
            	    break;
            	case EOF:
            	case IS:
            	case SEMICOLON:
            	case ASSIGN:
            	case COMMA:
            	case RANGE:
            	case RPARANTHESIS:
            	case DIGITS:
            	case ASSOCIATION:
            	case CHOICE:
            	case WITH:
            	case THEN:
            	case LOOP:
            		{
            	    alt55 = 6;
            	    }
            	    break;
            		default:
            		    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            		    NoViableAltException nvae_d55s0 =
            		        new NoViableAltException("", 55, 0, input);

            		    throw nvae_d55s0;
            	}

            	switch (alt55) 
            	{
            	    case 1 :
            	        // Ada95.g:519:4: ( AND relation )+
            	        {
            	        	// Ada95.g:519:4: ( AND relation )+
            	        	int cnt50 = 0;
            	        	do 
            	        	{
            	        	    int alt50 = 2;
            	        	    int LA50_0 = input.LA(1);

            	        	    if ( (LA50_0 == AND) )
            	        	    {
            	        	        alt50 = 1;
            	        	    }


            	        	    switch (alt50) 
            	        		{
            	        			case 1 :
            	        			    // Ada95.g:519:6: AND relation
            	        			    {
            	        			    	AND229=(IToken)Match(input,AND,FOLLOW_AND_in_expression2367); if (state.failed) return retval; 
            	        			    	if ( (state.backtracking==0) ) stream_AND.Add(AND229);

            	        			    	PushFollow(FOLLOW_relation_in_expression2369);
            	        			    	relation230 = relation();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( (state.backtracking==0) ) stream_relation.Add(relation230.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt50 >= 1 ) goto loop50;
            	        			    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        		            EarlyExitException eee50 =
            	        		                new EarlyExitException(50, input);
            	        		            throw eee50;
            	        	    }
            	        	    cnt50++;
            	        	} while (true);

            	        	loop50:
            	        		;	// Stops C# compiler whining that label 'loop50' has no statements



            	        	// AST REWRITE
            	        	// elements:          relation, AND
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 519:23: -> ^( EXPRESSION AND ( relation )+ )
            	        	{
            	        	    // Ada95.g:519:26: ^( EXPRESSION AND ( relation )+ )
            	        	    {
            	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(EXPRESSION, "EXPRESSION"), root_1);

            	        	    adaptor.AddChild(root_1, stream_AND.NextNode());
            	        	    if ( !(stream_relation.HasNext()) ) {
            	        	        throw new RewriteEarlyExitException();
            	        	    }
            	        	    while ( stream_relation.HasNext() )
            	        	    {
            	        	        adaptor.AddChild(root_1, stream_relation.NextTree());

            	        	    }
            	        	    stream_relation.Reset();

            	        	    adaptor.AddChild(root_0, root_1);
            	        	    }

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;
            	    case 2 :
            	        // Ada95.g:520:4: ( AND THEN relation )+
            	        {
            	        	// Ada95.g:520:4: ( AND THEN relation )+
            	        	int cnt51 = 0;
            	        	do 
            	        	{
            	        	    int alt51 = 2;
            	        	    int LA51_0 = input.LA(1);

            	        	    if ( (LA51_0 == AND) )
            	        	    {
            	        	        alt51 = 1;
            	        	    }


            	        	    switch (alt51) 
            	        		{
            	        			case 1 :
            	        			    // Ada95.g:520:6: AND THEN relation
            	        			    {
            	        			    	AND231=(IToken)Match(input,AND,FOLLOW_AND_in_expression2395); if (state.failed) return retval; 
            	        			    	if ( (state.backtracking==0) ) stream_AND.Add(AND231);

            	        			    	THEN232=(IToken)Match(input,THEN,FOLLOW_THEN_in_expression2397); if (state.failed) return retval; 
            	        			    	if ( (state.backtracking==0) ) stream_THEN.Add(THEN232);

            	        			    	PushFollow(FOLLOW_relation_in_expression2399);
            	        			    	relation233 = relation();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( (state.backtracking==0) ) stream_relation.Add(relation233.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt51 >= 1 ) goto loop51;
            	        			    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        		            EarlyExitException eee51 =
            	        		                new EarlyExitException(51, input);
            	        		            throw eee51;
            	        	    }
            	        	    cnt51++;
            	        	} while (true);

            	        	loop51:
            	        		;	// Stops C# compiler whining that label 'loop51' has no statements



            	        	// AST REWRITE
            	        	// elements:          THEN, relation
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 520:27: -> ^( EXPRESSION THEN ( relation )+ )
            	        	{
            	        	    // Ada95.g:520:30: ^( EXPRESSION THEN ( relation )+ )
            	        	    {
            	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(EXPRESSION, "EXPRESSION"), root_1);

            	        	    adaptor.AddChild(root_1, stream_THEN.NextNode());
            	        	    if ( !(stream_relation.HasNext()) ) {
            	        	        throw new RewriteEarlyExitException();
            	        	    }
            	        	    while ( stream_relation.HasNext() )
            	        	    {
            	        	        adaptor.AddChild(root_1, stream_relation.NextTree());

            	        	    }
            	        	    stream_relation.Reset();

            	        	    adaptor.AddChild(root_0, root_1);
            	        	    }

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;
            	    case 3 :
            	        // Ada95.g:521:4: ( OR relation )+
            	        {
            	        	// Ada95.g:521:4: ( OR relation )+
            	        	int cnt52 = 0;
            	        	do 
            	        	{
            	        	    int alt52 = 2;
            	        	    int LA52_0 = input.LA(1);

            	        	    if ( (LA52_0 == OR) )
            	        	    {
            	        	        alt52 = 1;
            	        	    }


            	        	    switch (alt52) 
            	        		{
            	        			case 1 :
            	        			    // Ada95.g:521:6: OR relation
            	        			    {
            	        			    	OR234=(IToken)Match(input,OR,FOLLOW_OR_in_expression2424); if (state.failed) return retval; 
            	        			    	if ( (state.backtracking==0) ) stream_OR.Add(OR234);

            	        			    	PushFollow(FOLLOW_relation_in_expression2426);
            	        			    	relation235 = relation();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( (state.backtracking==0) ) stream_relation.Add(relation235.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt52 >= 1 ) goto loop52;
            	        			    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        		            EarlyExitException eee52 =
            	        		                new EarlyExitException(52, input);
            	        		            throw eee52;
            	        	    }
            	        	    cnt52++;
            	        	} while (true);

            	        	loop52:
            	        		;	// Stops C# compiler whining that label 'loop52' has no statements



            	        	// AST REWRITE
            	        	// elements:          relation, OR
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 521:22: -> ^( EXPRESSION OR ( relation )+ )
            	        	{
            	        	    // Ada95.g:521:25: ^( EXPRESSION OR ( relation )+ )
            	        	    {
            	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(EXPRESSION, "EXPRESSION"), root_1);

            	        	    adaptor.AddChild(root_1, stream_OR.NextNode());
            	        	    if ( !(stream_relation.HasNext()) ) {
            	        	        throw new RewriteEarlyExitException();
            	        	    }
            	        	    while ( stream_relation.HasNext() )
            	        	    {
            	        	        adaptor.AddChild(root_1, stream_relation.NextTree());

            	        	    }
            	        	    stream_relation.Reset();

            	        	    adaptor.AddChild(root_0, root_1);
            	        	    }

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;
            	    case 4 :
            	        // Ada95.g:522:4: ( OR ELSE relation )+
            	        {
            	        	// Ada95.g:522:4: ( OR ELSE relation )+
            	        	int cnt53 = 0;
            	        	do 
            	        	{
            	        	    int alt53 = 2;
            	        	    int LA53_0 = input.LA(1);

            	        	    if ( (LA53_0 == OR) )
            	        	    {
            	        	        alt53 = 1;
            	        	    }


            	        	    switch (alt53) 
            	        		{
            	        			case 1 :
            	        			    // Ada95.g:522:6: OR ELSE relation
            	        			    {
            	        			    	OR236=(IToken)Match(input,OR,FOLLOW_OR_in_expression2455); if (state.failed) return retval; 
            	        			    	if ( (state.backtracking==0) ) stream_OR.Add(OR236);

            	        			    	ELSE237=(IToken)Match(input,ELSE,FOLLOW_ELSE_in_expression2457); if (state.failed) return retval; 
            	        			    	if ( (state.backtracking==0) ) stream_ELSE.Add(ELSE237);

            	        			    	PushFollow(FOLLOW_relation_in_expression2459);
            	        			    	relation238 = relation();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( (state.backtracking==0) ) stream_relation.Add(relation238.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt53 >= 1 ) goto loop53;
            	        			    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        		            EarlyExitException eee53 =
            	        		                new EarlyExitException(53, input);
            	        		            throw eee53;
            	        	    }
            	        	    cnt53++;
            	        	} while (true);

            	        	loop53:
            	        		;	// Stops C# compiler whining that label 'loop53' has no statements



            	        	// AST REWRITE
            	        	// elements:          ELSE, relation
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 522:26: -> ^( EXPRESSION ELSE ( relation )+ )
            	        	{
            	        	    // Ada95.g:522:29: ^( EXPRESSION ELSE ( relation )+ )
            	        	    {
            	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(EXPRESSION, "EXPRESSION"), root_1);

            	        	    adaptor.AddChild(root_1, stream_ELSE.NextNode());
            	        	    if ( !(stream_relation.HasNext()) ) {
            	        	        throw new RewriteEarlyExitException();
            	        	    }
            	        	    while ( stream_relation.HasNext() )
            	        	    {
            	        	        adaptor.AddChild(root_1, stream_relation.NextTree());

            	        	    }
            	        	    stream_relation.Reset();

            	        	    adaptor.AddChild(root_0, root_1);
            	        	    }

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;
            	    case 5 :
            	        // Ada95.g:523:4: ( XOR relation )+
            	        {
            	        	// Ada95.g:523:4: ( XOR relation )+
            	        	int cnt54 = 0;
            	        	do 
            	        	{
            	        	    int alt54 = 2;
            	        	    int LA54_0 = input.LA(1);

            	        	    if ( (LA54_0 == XOR) )
            	        	    {
            	        	        alt54 = 1;
            	        	    }


            	        	    switch (alt54) 
            	        		{
            	        			case 1 :
            	        			    // Ada95.g:523:6: XOR relation
            	        			    {
            	        			    	XOR239=(IToken)Match(input,XOR,FOLLOW_XOR_in_expression2484); if (state.failed) return retval; 
            	        			    	if ( (state.backtracking==0) ) stream_XOR.Add(XOR239);

            	        			    	PushFollow(FOLLOW_relation_in_expression2486);
            	        			    	relation240 = relation();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( (state.backtracking==0) ) stream_relation.Add(relation240.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt54 >= 1 ) goto loop54;
            	        			    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        		            EarlyExitException eee54 =
            	        		                new EarlyExitException(54, input);
            	        		            throw eee54;
            	        	    }
            	        	    cnt54++;
            	        	} while (true);

            	        	loop54:
            	        		;	// Stops C# compiler whining that label 'loop54' has no statements



            	        	// AST REWRITE
            	        	// elements:          XOR, relation
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 523:23: -> ^( EXPRESSION XOR ( relation )+ )
            	        	{
            	        	    // Ada95.g:523:26: ^( EXPRESSION XOR ( relation )+ )
            	        	    {
            	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(EXPRESSION, "EXPRESSION"), root_1);

            	        	    adaptor.AddChild(root_1, stream_XOR.NextNode());
            	        	    if ( !(stream_relation.HasNext()) ) {
            	        	        throw new RewriteEarlyExitException();
            	        	    }
            	        	    while ( stream_relation.HasNext() )
            	        	    {
            	        	        adaptor.AddChild(root_1, stream_relation.NextTree());

            	        	    }
            	        	    stream_relation.Reset();

            	        	    adaptor.AddChild(root_0, root_1);
            	        	    }

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;
            	    case 6 :
            	        // Ada95.g:524:17: 
            	        {

            	        	// AST REWRITE
            	        	// elements:          relation
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 524:17: -> ^( EXPRESSION relation )
            	        	{
            	        	    // Ada95.g:524:20: ^( EXPRESSION relation )
            	        	    {
            	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(EXPRESSION, "EXPRESSION"), root_1);

            	        	    adaptor.AddChild(root_1, stream_relation.NextTree());

            	        	    adaptor.AddChild(root_0, root_1);
            	        	    }

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 67, expression_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "expression"

    public class relation_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "relation"
    // Ada95.g:528:1: relation : simple_expression ( relational_operator simple_expression -> ^( relational_operator simple_expression simple_expression ) | ( NOT )? IN ( ( range )=> range -> ^( IN_RANGE simple_expression range ( NOT )? ) | subtype_mark -> ^( IN_SUBTYPE simple_expression subtype_mark ( NOT )? ) ) | -> simple_expression ) ;
    public Ada95Parser.relation_return relation() // throws RecognitionException [1]
    {   
        Ada95Parser.relation_return retval = new Ada95Parser.relation_return();
        retval.Start = input.LT(1);
        int relation_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken NOT244 = null;
        IToken IN245 = null;
        Ada95Parser.simple_expression_return simple_expression241 = default(Ada95Parser.simple_expression_return);

        Ada95Parser.relational_operator_return relational_operator242 = default(Ada95Parser.relational_operator_return);

        Ada95Parser.simple_expression_return simple_expression243 = default(Ada95Parser.simple_expression_return);

        Ada95Parser.range_return range246 = default(Ada95Parser.range_return);

        Ada95Parser.subtype_mark_return subtype_mark247 = default(Ada95Parser.subtype_mark_return);


        CommonTree NOT244_tree=null;
        CommonTree IN245_tree=null;
        RewriteRuleTokenStream stream_IN = new RewriteRuleTokenStream(adaptor,"token IN");
        RewriteRuleTokenStream stream_NOT = new RewriteRuleTokenStream(adaptor,"token NOT");
        RewriteRuleSubtreeStream stream_range = new RewriteRuleSubtreeStream(adaptor,"rule range");
        RewriteRuleSubtreeStream stream_simple_expression = new RewriteRuleSubtreeStream(adaptor,"rule simple_expression");
        RewriteRuleSubtreeStream stream_relational_operator = new RewriteRuleSubtreeStream(adaptor,"rule relational_operator");
        RewriteRuleSubtreeStream stream_subtype_mark = new RewriteRuleSubtreeStream(adaptor,"rule subtype_mark");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 68) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:529:2: ( simple_expression ( relational_operator simple_expression -> ^( relational_operator simple_expression simple_expression ) | ( NOT )? IN ( ( range )=> range -> ^( IN_RANGE simple_expression range ( NOT )? ) | subtype_mark -> ^( IN_SUBTYPE simple_expression subtype_mark ( NOT )? ) ) | -> simple_expression ) )
            // Ada95.g:530:3: simple_expression ( relational_operator simple_expression -> ^( relational_operator simple_expression simple_expression ) | ( NOT )? IN ( ( range )=> range -> ^( IN_RANGE simple_expression range ( NOT )? ) | subtype_mark -> ^( IN_SUBTYPE simple_expression subtype_mark ( NOT )? ) ) | -> simple_expression )
            {
            	PushFollow(FOLLOW_simple_expression_in_relation2540);
            	simple_expression241 = simple_expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_simple_expression.Add(simple_expression241.Tree);
            	// Ada95.g:531:3: ( relational_operator simple_expression -> ^( relational_operator simple_expression simple_expression ) | ( NOT )? IN ( ( range )=> range -> ^( IN_RANGE simple_expression range ( NOT )? ) | subtype_mark -> ^( IN_SUBTYPE simple_expression subtype_mark ( NOT )? ) ) | -> simple_expression )
            	int alt58 = 3;
            	switch ( input.LA(1) ) 
            	{
            	case EQ:
            	case NEQ:
            	case LESS:
            	case LEQ:
            	case GREATER:
            	case GEQ:
            		{
            	    alt58 = 1;
            	    }
            	    break;
            	case NOT:
            	case IN:
            		{
            	    alt58 = 2;
            	    }
            	    break;
            	case EOF:
            	case IS:
            	case SEMICOLON:
            	case ASSIGN:
            	case COMMA:
            	case RANGE:
            	case RPARANTHESIS:
            	case DIGITS:
            	case ASSOCIATION:
            	case CHOICE:
            	case WITH:
            	case AND:
            	case THEN:
            	case OR:
            	case XOR:
            	case LOOP:
            		{
            	    alt58 = 3;
            	    }
            	    break;
            		default:
            		    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            		    NoViableAltException nvae_d58s0 =
            		        new NoViableAltException("", 58, 0, input);

            		    throw nvae_d58s0;
            	}

            	switch (alt58) 
            	{
            	    case 1 :
            	        // Ada95.g:532:4: relational_operator simple_expression
            	        {
            	        	PushFollow(FOLLOW_relational_operator_in_relation2549);
            	        	relational_operator242 = relational_operator();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_relational_operator.Add(relational_operator242.Tree);
            	        	PushFollow(FOLLOW_simple_expression_in_relation2551);
            	        	simple_expression243 = simple_expression();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_simple_expression.Add(simple_expression243.Tree);


            	        	// AST REWRITE
            	        	// elements:          simple_expression, relational_operator, simple_expression
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 532:42: -> ^( relational_operator simple_expression simple_expression )
            	        	{
            	        	    // Ada95.g:532:45: ^( relational_operator simple_expression simple_expression )
            	        	    {
            	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	    root_1 = (CommonTree)adaptor.BecomeRoot(stream_relational_operator.NextNode(), root_1);

            	        	    adaptor.AddChild(root_1, stream_simple_expression.NextTree());
            	        	    adaptor.AddChild(root_1, stream_simple_expression.NextTree());

            	        	    adaptor.AddChild(root_0, root_1);
            	        	    }

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;
            	    case 2 :
            	        // Ada95.g:533:4: ( NOT )? IN ( ( range )=> range -> ^( IN_RANGE simple_expression range ( NOT )? ) | subtype_mark -> ^( IN_SUBTYPE simple_expression subtype_mark ( NOT )? ) )
            	        {
            	        	// Ada95.g:533:4: ( NOT )?
            	        	int alt56 = 2;
            	        	int LA56_0 = input.LA(1);

            	        	if ( (LA56_0 == NOT) )
            	        	{
            	        	    alt56 = 1;
            	        	}
            	        	switch (alt56) 
            	        	{
            	        	    case 1 :
            	        	        // Ada95.g:533:4: NOT
            	        	        {
            	        	        	NOT244=(IToken)Match(input,NOT,FOLLOW_NOT_in_relation2570); if (state.failed) return retval; 
            	        	        	if ( (state.backtracking==0) ) stream_NOT.Add(NOT244);


            	        	        }
            	        	        break;

            	        	}

            	        	IN245=(IToken)Match(input,IN,FOLLOW_IN_in_relation2573); if (state.failed) return retval; 
            	        	if ( (state.backtracking==0) ) stream_IN.Add(IN245);

            	        	// Ada95.g:534:4: ( ( range )=> range -> ^( IN_RANGE simple_expression range ( NOT )? ) | subtype_mark -> ^( IN_SUBTYPE simple_expression subtype_mark ( NOT )? ) )
            	        	int alt57 = 2;
            	        	alt57 = dfa57.Predict(input);
            	        	switch (alt57) 
            	        	{
            	        	    case 1 :
            	        	        // Ada95.g:535:5: ( range )=> range
            	        	        {
            	        	        	PushFollow(FOLLOW_range_in_relation2591);
            	        	        	range246 = range();
            	        	        	state.followingStackPointer--;
            	        	        	if (state.failed) return retval;
            	        	        	if ( (state.backtracking==0) ) stream_range.Add(range246.Tree);


            	        	        	// AST REWRITE
            	        	        	// elements:          range, simple_expression, NOT
            	        	        	// token labels:      
            	        	        	// rule labels:       retval
            	        	        	// token list labels: 
            	        	        	// rule list labels:  
            	        	        	// wildcard labels: 
            	        	        	if ( (state.backtracking==0) ) {
            	        	        	retval.Tree = root_0;
            	        	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	        	// 535:25: -> ^( IN_RANGE simple_expression range ( NOT )? )
            	        	        	{
            	        	        	    // Ada95.g:535:28: ^( IN_RANGE simple_expression range ( NOT )? )
            	        	        	    {
            	        	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(IN_RANGE, "IN_RANGE"), root_1);

            	        	        	    adaptor.AddChild(root_1, stream_simple_expression.NextTree());
            	        	        	    adaptor.AddChild(root_1, stream_range.NextTree());
            	        	        	    // Ada95.g:535:64: ( NOT )?
            	        	        	    if ( stream_NOT.HasNext() )
            	        	        	    {
            	        	        	        adaptor.AddChild(root_1, stream_NOT.NextNode());

            	        	        	    }
            	        	        	    stream_NOT.Reset();

            	        	        	    adaptor.AddChild(root_0, root_1);
            	        	        	    }

            	        	        	}

            	        	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        	        }
            	        	        break;
            	        	    case 2 :
            	        	        // Ada95.g:536:8: subtype_mark
            	        	        {
            	        	        	PushFollow(FOLLOW_subtype_mark_in_relation2619);
            	        	        	subtype_mark247 = subtype_mark();
            	        	        	state.followingStackPointer--;
            	        	        	if (state.failed) return retval;
            	        	        	if ( (state.backtracking==0) ) stream_subtype_mark.Add(subtype_mark247.Tree);


            	        	        	// AST REWRITE
            	        	        	// elements:          subtype_mark, NOT, simple_expression
            	        	        	// token labels:      
            	        	        	// rule labels:       retval
            	        	        	// token list labels: 
            	        	        	// rule list labels:  
            	        	        	// wildcard labels: 
            	        	        	if ( (state.backtracking==0) ) {
            	        	        	retval.Tree = root_0;
            	        	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	        	// 536:21: -> ^( IN_SUBTYPE simple_expression subtype_mark ( NOT )? )
            	        	        	{
            	        	        	    // Ada95.g:536:24: ^( IN_SUBTYPE simple_expression subtype_mark ( NOT )? )
            	        	        	    {
            	        	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(IN_SUBTYPE, "IN_SUBTYPE"), root_1);

            	        	        	    adaptor.AddChild(root_1, stream_simple_expression.NextTree());
            	        	        	    adaptor.AddChild(root_1, stream_subtype_mark.NextTree());
            	        	        	    // Ada95.g:536:69: ( NOT )?
            	        	        	    if ( stream_NOT.HasNext() )
            	        	        	    {
            	        	        	        adaptor.AddChild(root_1, stream_NOT.NextNode());

            	        	        	    }
            	        	        	    stream_NOT.Reset();

            	        	        	    adaptor.AddChild(root_0, root_1);
            	        	        	    }

            	        	        	}

            	        	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        	        }
            	        	        break;

            	        	}


            	        }
            	        break;
            	    case 3 :
            	        // Ada95.g:538:14: 
            	        {

            	        	// AST REWRITE
            	        	// elements:          simple_expression
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 538:14: -> simple_expression
            	        	{
            	        	    adaptor.AddChild(root_0, stream_simple_expression.NextTree());

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 68, relation_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "relation"

    public class simple_expression_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "simple_expression"
    // Ada95.g:542:1: simple_expression : ( unary_adding_operator )? term ( binary_adding_operator term )* ;
    public Ada95Parser.simple_expression_return simple_expression() // throws RecognitionException [1]
    {   
        Ada95Parser.simple_expression_return retval = new Ada95Parser.simple_expression_return();
        retval.Start = input.LT(1);
        int simple_expression_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.unary_adding_operator_return unary_adding_operator248 = default(Ada95Parser.unary_adding_operator_return);

        Ada95Parser.term_return term249 = default(Ada95Parser.term_return);

        Ada95Parser.binary_adding_operator_return binary_adding_operator250 = default(Ada95Parser.binary_adding_operator_return);

        Ada95Parser.term_return term251 = default(Ada95Parser.term_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 69) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:543:2: ( ( unary_adding_operator )? term ( binary_adding_operator term )* )
            // Ada95.g:543:4: ( unary_adding_operator )? term ( binary_adding_operator term )*
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	// Ada95.g:543:4: ( unary_adding_operator )?
            	int alt59 = 2;
            	int LA59_0 = input.LA(1);

            	if ( ((LA59_0 >= PLUS && LA59_0 <= MINUS)) )
            	{
            	    alt59 = 1;
            	}
            	switch (alt59) 
            	{
            	    case 1 :
            	        // Ada95.g:543:4: unary_adding_operator
            	        {
            	        	PushFollow(FOLLOW_unary_adding_operator_in_simple_expression2665);
            	        	unary_adding_operator248 = unary_adding_operator();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, unary_adding_operator248.Tree);

            	        }
            	        break;

            	}

            	PushFollow(FOLLOW_term_in_simple_expression2668);
            	term249 = term();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term249.Tree);
            	// Ada95.g:543:32: ( binary_adding_operator term )*
            	do 
            	{
            	    int alt60 = 2;
            	    int LA60_0 = input.LA(1);

            	    if ( ((LA60_0 >= PLUS && LA60_0 <= CONCAT)) )
            	    {
            	        alt60 = 1;
            	    }


            	    switch (alt60) 
            		{
            			case 1 :
            			    // Ada95.g:543:34: binary_adding_operator term
            			    {
            			    	PushFollow(FOLLOW_binary_adding_operator_in_simple_expression2672);
            			    	binary_adding_operator250 = binary_adding_operator();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) root_0 = (CommonTree)adaptor.BecomeRoot(binary_adding_operator250.Tree, root_0);
            			    	PushFollow(FOLLOW_term_in_simple_expression2675);
            			    	term251 = term();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, term251.Tree);

            			    }
            			    break;

            			default:
            			    goto loop60;
            	    }
            	} while (true);

            	loop60:
            		;	// Stops C# compiler whining that label 'loop60' has no statements


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 69, simple_expression_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "simple_expression"

    public class term_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "term"
    // Ada95.g:545:1: term : factor ( multiplying_operator factor )* ;
    public Ada95Parser.term_return term() // throws RecognitionException [1]
    {   
        Ada95Parser.term_return retval = new Ada95Parser.term_return();
        retval.Start = input.LT(1);
        int term_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.factor_return factor252 = default(Ada95Parser.factor_return);

        Ada95Parser.multiplying_operator_return multiplying_operator253 = default(Ada95Parser.multiplying_operator_return);

        Ada95Parser.factor_return factor254 = default(Ada95Parser.factor_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 70) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:546:2: ( factor ( multiplying_operator factor )* )
            // Ada95.g:546:4: factor ( multiplying_operator factor )*
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_factor_in_term2688);
            	factor252 = factor();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, factor252.Tree);
            	// Ada95.g:546:11: ( multiplying_operator factor )*
            	do 
            	{
            	    int alt61 = 2;
            	    int LA61_0 = input.LA(1);

            	    if ( (LA61_0 == MOD || (LA61_0 >= MULT && LA61_0 <= REM)) )
            	    {
            	        alt61 = 1;
            	    }


            	    switch (alt61) 
            		{
            			case 1 :
            			    // Ada95.g:546:13: multiplying_operator factor
            			    {
            			    	PushFollow(FOLLOW_multiplying_operator_in_term2692);
            			    	multiplying_operator253 = multiplying_operator();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) root_0 = (CommonTree)adaptor.BecomeRoot(multiplying_operator253.Tree, root_0);
            			    	PushFollow(FOLLOW_factor_in_term2695);
            			    	factor254 = factor();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, factor254.Tree);

            			    }
            			    break;

            			default:
            			    goto loop61;
            	    }
            	} while (true);

            	loop61:
            		;	// Stops C# compiler whining that label 'loop61' has no statements


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 70, term_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "term"

    public class factor_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "factor"
    // Ada95.g:548:1: factor : ( ( primary POW )=> primary POW primary | primary | ABS primary | NOT primary );
    public Ada95Parser.factor_return factor() // throws RecognitionException [1]
    {   
        Ada95Parser.factor_return retval = new Ada95Parser.factor_return();
        retval.Start = input.LT(1);
        int factor_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken POW256 = null;
        IToken ABS259 = null;
        IToken NOT261 = null;
        Ada95Parser.primary_return primary255 = default(Ada95Parser.primary_return);

        Ada95Parser.primary_return primary257 = default(Ada95Parser.primary_return);

        Ada95Parser.primary_return primary258 = default(Ada95Parser.primary_return);

        Ada95Parser.primary_return primary260 = default(Ada95Parser.primary_return);

        Ada95Parser.primary_return primary262 = default(Ada95Parser.primary_return);


        CommonTree POW256_tree=null;
        CommonTree ABS259_tree=null;
        CommonTree NOT261_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 71) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:549:2: ( ( primary POW )=> primary POW primary | primary | ABS primary | NOT primary )
            int alt62 = 4;
            alt62 = dfa62.Predict(input);
            switch (alt62) 
            {
                case 1 :
                    // Ada95.g:550:3: ( primary POW )=> primary POW primary
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_primary_in_factor2719);
                    	primary255 = primary();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, primary255.Tree);
                    	POW256=(IToken)Match(input,POW,FOLLOW_POW_in_factor2721); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{POW256_tree = (CommonTree)adaptor.Create(POW256);
                    		root_0 = (CommonTree)adaptor.BecomeRoot(POW256_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_primary_in_factor2724);
                    	primary257 = primary();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, primary257.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:551:8: primary
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_primary_in_factor2735);
                    	primary258 = primary();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, primary258.Tree);

                    }
                    break;
                case 3 :
                    // Ada95.g:552:8: ABS primary
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	ABS259=(IToken)Match(input,ABS,FOLLOW_ABS_in_factor2746); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{ABS259_tree = (CommonTree)adaptor.Create(ABS259);
                    		root_0 = (CommonTree)adaptor.BecomeRoot(ABS259_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_primary_in_factor2749);
                    	primary260 = primary();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, primary260.Tree);

                    }
                    break;
                case 4 :
                    // Ada95.g:553:8: NOT primary
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	NOT261=(IToken)Match(input,NOT,FOLLOW_NOT_in_factor2760); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{NOT261_tree = (CommonTree)adaptor.Create(NOT261);
                    		root_0 = (CommonTree)adaptor.BecomeRoot(NOT261_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_primary_in_factor2763);
                    	primary262 = primary();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, primary262.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 71, factor_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "factor"

    public class primary_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "primary"
    // Ada95.g:556:1: primary : ( NUMERIC_LITERAL | NULL | STRING_LITERAL | name | qualified_expression | LPARANTHESIS expression RPARANTHESIS );
    public Ada95Parser.primary_return primary() // throws RecognitionException [1]
    {   
        Ada95Parser.primary_return retval = new Ada95Parser.primary_return();
        retval.Start = input.LT(1);
        int primary_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken NUMERIC_LITERAL263 = null;
        IToken NULL264 = null;
        IToken STRING_LITERAL265 = null;
        IToken LPARANTHESIS268 = null;
        IToken RPARANTHESIS270 = null;
        Ada95Parser.name_return name266 = default(Ada95Parser.name_return);

        Ada95Parser.qualified_expression_return qualified_expression267 = default(Ada95Parser.qualified_expression_return);

        Ada95Parser.expression_return expression269 = default(Ada95Parser.expression_return);


        CommonTree NUMERIC_LITERAL263_tree=null;
        CommonTree NULL264_tree=null;
        CommonTree STRING_LITERAL265_tree=null;
        CommonTree LPARANTHESIS268_tree=null;
        CommonTree RPARANTHESIS270_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 72) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:557:2: ( NUMERIC_LITERAL | NULL | STRING_LITERAL | name | qualified_expression | LPARANTHESIS expression RPARANTHESIS )
            int alt63 = 6;
            switch ( input.LA(1) ) 
            {
            case NUMERIC_LITERAL:
            	{
                alt63 = 1;
                }
                break;
            case NULL:
            	{
                alt63 = 2;
                }
                break;
            case STRING_LITERAL:
            	{
                alt63 = 3;
                }
                break;
            case IDENTIFIER:
            	{
                int LA63_4 = input.LA(2);

                if ( (LA63_4 == EOF || (LA63_4 >= IS && LA63_4 <= SEMICOLON) || LA63_4 == ASSIGN || (LA63_4 >= COMMA && LA63_4 <= RPARANTHESIS) || (LA63_4 >= MOD && LA63_4 <= DIGITS) || LA63_4 == ASSOCIATION || (LA63_4 >= CHOICE && LA63_4 <= WITH) || LA63_4 == DOT || (LA63_4 >= AND && LA63_4 <= OR) || (LA63_4 >= XOR && LA63_4 <= POW) || (LA63_4 >= EQ && LA63_4 <= REM) || LA63_4 == LOOP) )
                {
                    alt63 = 4;
                }
                else if ( (LA63_4 == APOSTROPHE) )
                {
                    int LA63_7 = input.LA(3);

                    if ( (LA63_7 == IDENTIFIER) )
                    {
                        alt63 = 4;
                    }
                    else if ( (LA63_7 == LPARANTHESIS) )
                    {
                        alt63 = 5;
                    }
                    else 
                    {
                        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                        NoViableAltException nvae_d63s7 =
                            new NoViableAltException("", 63, 7, input);

                        throw nvae_d63s7;
                    }
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d63s4 =
                        new NoViableAltException("", 63, 4, input);

                    throw nvae_d63s4;
                }
                }
                break;
            case CHARACTER_LITERAL:
            	{
                alt63 = 4;
                }
                break;
            case LPARANTHESIS:
            	{
                alt63 = 6;
                }
                break;
            	default:
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    NoViableAltException nvae_d63s0 =
            	        new NoViableAltException("", 63, 0, input);

            	    throw nvae_d63s0;
            }

            switch (alt63) 
            {
                case 1 :
                    // Ada95.g:558:3: NUMERIC_LITERAL
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	NUMERIC_LITERAL263=(IToken)Match(input,NUMERIC_LITERAL,FOLLOW_NUMERIC_LITERAL_in_primary2776); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{NUMERIC_LITERAL263_tree = (CommonTree)adaptor.Create(NUMERIC_LITERAL263);
                    		adaptor.AddChild(root_0, NUMERIC_LITERAL263_tree);
                    	}

                    }
                    break;
                case 2 :
                    // Ada95.g:559:3: NULL
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	NULL264=(IToken)Match(input,NULL,FOLLOW_NULL_in_primary2782); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{NULL264_tree = (CommonTree)adaptor.Create(NULL264);
                    		adaptor.AddChild(root_0, NULL264_tree);
                    	}

                    }
                    break;
                case 3 :
                    // Ada95.g:560:3: STRING_LITERAL
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	STRING_LITERAL265=(IToken)Match(input,STRING_LITERAL,FOLLOW_STRING_LITERAL_in_primary2788); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{STRING_LITERAL265_tree = (CommonTree)adaptor.Create(STRING_LITERAL265);
                    		adaptor.AddChild(root_0, STRING_LITERAL265_tree);
                    	}

                    }
                    break;
                case 4 :
                    // Ada95.g:562:3: name
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_name_in_primary2797);
                    	name266 = name();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, name266.Tree);

                    }
                    break;
                case 5 :
                    // Ada95.g:563:3: qualified_expression
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_qualified_expression_in_primary2803);
                    	qualified_expression267 = qualified_expression();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, qualified_expression267.Tree);

                    }
                    break;
                case 6 :
                    // Ada95.g:565:3: LPARANTHESIS expression RPARANTHESIS
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	LPARANTHESIS268=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_primary2811); if (state.failed) return retval;
                    	PushFollow(FOLLOW_expression_in_primary2814);
                    	expression269 = expression();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression269.Tree);
                    	RPARANTHESIS270=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_primary2816); if (state.failed) return retval;

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 72, primary_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "primary"

    public class relational_operator_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "relational_operator"
    // Ada95.g:568:1: relational_operator : ( EQ | NEQ | LESS | LEQ | GREATER | GEQ );
    public Ada95Parser.relational_operator_return relational_operator() // throws RecognitionException [1]
    {   
        Ada95Parser.relational_operator_return retval = new Ada95Parser.relational_operator_return();
        retval.Start = input.LT(1);
        int relational_operator_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken set271 = null;

        CommonTree set271_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 73) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:569:2: ( EQ | NEQ | LESS | LEQ | GREATER | GEQ )
            // Ada95.g:
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	set271 = (IToken)input.LT(1);
            	if ( (input.LA(1) >= EQ && input.LA(1) <= GEQ) ) 
            	{
            	    input.Consume();
            	    if ( state.backtracking == 0 ) adaptor.AddChild(root_0, (CommonTree)adaptor.Create(set271));
            	    state.errorRecovery = false;state.failed = false;
            	}
            	else 
            	{
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    throw mse;
            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 73, relational_operator_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "relational_operator"

    public class binary_adding_operator_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "binary_adding_operator"
    // Ada95.g:571:1: binary_adding_operator : ( PLUS | MINUS | CONCAT );
    public Ada95Parser.binary_adding_operator_return binary_adding_operator() // throws RecognitionException [1]
    {   
        Ada95Parser.binary_adding_operator_return retval = new Ada95Parser.binary_adding_operator_return();
        retval.Start = input.LT(1);
        int binary_adding_operator_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken set272 = null;

        CommonTree set272_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 74) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:572:2: ( PLUS | MINUS | CONCAT )
            // Ada95.g:
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	set272 = (IToken)input.LT(1);
            	if ( (input.LA(1) >= PLUS && input.LA(1) <= CONCAT) ) 
            	{
            	    input.Consume();
            	    if ( state.backtracking == 0 ) adaptor.AddChild(root_0, (CommonTree)adaptor.Create(set272));
            	    state.errorRecovery = false;state.failed = false;
            	}
            	else 
            	{
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    throw mse;
            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 74, binary_adding_operator_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "binary_adding_operator"

    public class unary_adding_operator_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "unary_adding_operator"
    // Ada95.g:574:1: unary_adding_operator : ( PLUS | MINUS );
    public Ada95Parser.unary_adding_operator_return unary_adding_operator() // throws RecognitionException [1]
    {   
        Ada95Parser.unary_adding_operator_return retval = new Ada95Parser.unary_adding_operator_return();
        retval.Start = input.LT(1);
        int unary_adding_operator_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken set273 = null;

        CommonTree set273_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 75) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:575:2: ( PLUS | MINUS )
            // Ada95.g:
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	set273 = (IToken)input.LT(1);
            	if ( (input.LA(1) >= PLUS && input.LA(1) <= MINUS) ) 
            	{
            	    input.Consume();
            	    if ( state.backtracking == 0 ) adaptor.AddChild(root_0, (CommonTree)adaptor.Create(set273));
            	    state.errorRecovery = false;state.failed = false;
            	}
            	else 
            	{
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    throw mse;
            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 75, unary_adding_operator_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "unary_adding_operator"

    public class multiplying_operator_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "multiplying_operator"
    // Ada95.g:577:1: multiplying_operator : ( MULT | DIV | MOD | REM );
    public Ada95Parser.multiplying_operator_return multiplying_operator() // throws RecognitionException [1]
    {   
        Ada95Parser.multiplying_operator_return retval = new Ada95Parser.multiplying_operator_return();
        retval.Start = input.LT(1);
        int multiplying_operator_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken set274 = null;

        CommonTree set274_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 76) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:578:2: ( MULT | DIV | MOD | REM )
            // Ada95.g:
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	set274 = (IToken)input.LT(1);
            	if ( input.LA(1) == MOD || (input.LA(1) >= MULT && input.LA(1) <= REM) ) 
            	{
            	    input.Consume();
            	    if ( state.backtracking == 0 ) adaptor.AddChild(root_0, (CommonTree)adaptor.Create(set274));
            	    state.errorRecovery = false;state.failed = false;
            	}
            	else 
            	{
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    throw mse;
            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 76, multiplying_operator_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "multiplying_operator"

    public class type_conversion_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "type_conversion"
    // Ada95.g:580:1: type_conversion : direct_name LPARANTHESIS expression RPARANTHESIS -> ^( TYPE direct_name expression ) ;
    public Ada95Parser.type_conversion_return type_conversion() // throws RecognitionException [1]
    {   
        Ada95Parser.type_conversion_return retval = new Ada95Parser.type_conversion_return();
        retval.Start = input.LT(1);
        int type_conversion_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken LPARANTHESIS276 = null;
        IToken RPARANTHESIS278 = null;
        Ada95Parser.direct_name_return direct_name275 = default(Ada95Parser.direct_name_return);

        Ada95Parser.expression_return expression277 = default(Ada95Parser.expression_return);


        CommonTree LPARANTHESIS276_tree=null;
        CommonTree RPARANTHESIS278_tree=null;
        RewriteRuleTokenStream stream_LPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token LPARANTHESIS");
        RewriteRuleTokenStream stream_RPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token RPARANTHESIS");
        RewriteRuleSubtreeStream stream_expression = new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_direct_name = new RewriteRuleSubtreeStream(adaptor,"rule direct_name");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 77) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:581:2: ( direct_name LPARANTHESIS expression RPARANTHESIS -> ^( TYPE direct_name expression ) )
            // Ada95.g:581:4: direct_name LPARANTHESIS expression RPARANTHESIS
            {
            	PushFollow(FOLLOW_direct_name_in_type_conversion2913);
            	direct_name275 = direct_name();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_direct_name.Add(direct_name275.Tree);
            	LPARANTHESIS276=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_type_conversion2915); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_LPARANTHESIS.Add(LPARANTHESIS276);

            	PushFollow(FOLLOW_expression_in_type_conversion2917);
            	expression277 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_expression.Add(expression277.Tree);
            	RPARANTHESIS278=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_type_conversion2919); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_RPARANTHESIS.Add(RPARANTHESIS278);



            	// AST REWRITE
            	// elements:          direct_name, expression
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 581:53: -> ^( TYPE direct_name expression )
            	{
            	    // Ada95.g:581:56: ^( TYPE direct_name expression )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(TYPE, "TYPE"), root_1);

            	    adaptor.AddChild(root_1, stream_direct_name.NextTree());
            	    adaptor.AddChild(root_1, stream_expression.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 77, type_conversion_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "type_conversion"

    public class qualified_expression_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "qualified_expression"
    // Ada95.g:583:1: qualified_expression : subtype_mark APOSTROPHE ( LPARANTHESIS expression RPARANTHESIS -> ^( QUALIFIED_BY_EXPRESSION subtype_mark expression ) ) ;
    public Ada95Parser.qualified_expression_return qualified_expression() // throws RecognitionException [1]
    {   
        Ada95Parser.qualified_expression_return retval = new Ada95Parser.qualified_expression_return();
        retval.Start = input.LT(1);
        int qualified_expression_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken APOSTROPHE280 = null;
        IToken LPARANTHESIS281 = null;
        IToken RPARANTHESIS283 = null;
        Ada95Parser.subtype_mark_return subtype_mark279 = default(Ada95Parser.subtype_mark_return);

        Ada95Parser.expression_return expression282 = default(Ada95Parser.expression_return);


        CommonTree APOSTROPHE280_tree=null;
        CommonTree LPARANTHESIS281_tree=null;
        CommonTree RPARANTHESIS283_tree=null;
        RewriteRuleTokenStream stream_LPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token LPARANTHESIS");
        RewriteRuleTokenStream stream_APOSTROPHE = new RewriteRuleTokenStream(adaptor,"token APOSTROPHE");
        RewriteRuleTokenStream stream_RPARANTHESIS = new RewriteRuleTokenStream(adaptor,"token RPARANTHESIS");
        RewriteRuleSubtreeStream stream_expression = new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_subtype_mark = new RewriteRuleSubtreeStream(adaptor,"rule subtype_mark");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 78) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:584:2: ( subtype_mark APOSTROPHE ( LPARANTHESIS expression RPARANTHESIS -> ^( QUALIFIED_BY_EXPRESSION subtype_mark expression ) ) )
            // Ada95.g:585:3: subtype_mark APOSTROPHE ( LPARANTHESIS expression RPARANTHESIS -> ^( QUALIFIED_BY_EXPRESSION subtype_mark expression ) )
            {
            	PushFollow(FOLLOW_subtype_mark_in_qualified_expression2943);
            	subtype_mark279 = subtype_mark();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_subtype_mark.Add(subtype_mark279.Tree);
            	APOSTROPHE280=(IToken)Match(input,APOSTROPHE,FOLLOW_APOSTROPHE_in_qualified_expression2945); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_APOSTROPHE.Add(APOSTROPHE280);

            	// Ada95.g:586:3: ( LPARANTHESIS expression RPARANTHESIS -> ^( QUALIFIED_BY_EXPRESSION subtype_mark expression ) )
            	// Ada95.g:587:4: LPARANTHESIS expression RPARANTHESIS
            	{
            		LPARANTHESIS281=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_qualified_expression2954); if (state.failed) return retval; 
            		if ( (state.backtracking==0) ) stream_LPARANTHESIS.Add(LPARANTHESIS281);

            		PushFollow(FOLLOW_expression_in_qualified_expression2956);
            		expression282 = expression();
            		state.followingStackPointer--;
            		if (state.failed) return retval;
            		if ( (state.backtracking==0) ) stream_expression.Add(expression282.Tree);
            		RPARANTHESIS283=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_qualified_expression2958); if (state.failed) return retval; 
            		if ( (state.backtracking==0) ) stream_RPARANTHESIS.Add(RPARANTHESIS283);



            		// AST REWRITE
            		// elements:          subtype_mark, expression
            		// token labels:      
            		// rule labels:       retval
            		// token list labels: 
            		// rule list labels:  
            		// wildcard labels: 
            		if ( (state.backtracking==0) ) {
            		retval.Tree = root_0;
            		RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            		root_0 = (CommonTree)adaptor.GetNilNode();
            		// 587:41: -> ^( QUALIFIED_BY_EXPRESSION subtype_mark expression )
            		{
            		    // Ada95.g:587:44: ^( QUALIFIED_BY_EXPRESSION subtype_mark expression )
            		    {
            		    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            		    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(QUALIFIED_BY_EXPRESSION, "QUALIFIED_BY_EXPRESSION"), root_1);

            		    adaptor.AddChild(root_1, stream_subtype_mark.NextTree());
            		    adaptor.AddChild(root_1, stream_expression.NextTree());

            		    adaptor.AddChild(root_0, root_1);
            		    }

            		}

            		retval.Tree = root_0;retval.Tree = root_0;}
            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 78, qualified_expression_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "qualified_expression"

    public class sequence_of_statements_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "sequence_of_statements"
    // Ada95.g:594:1: sequence_of_statements : ( statement )+ ;
    public Ada95Parser.sequence_of_statements_return sequence_of_statements() // throws RecognitionException [1]
    {   
        Ada95Parser.sequence_of_statements_return retval = new Ada95Parser.sequence_of_statements_return();
        retval.Start = input.LT(1);
        int sequence_of_statements_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.statement_return statement284 = default(Ada95Parser.statement_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 79) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:595:2: ( ( statement )+ )
            // Ada95.g:595:4: ( statement )+
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	// Ada95.g:595:4: ( statement )+
            	int cnt64 = 0;
            	do 
            	{
            	    int alt64 = 2;
            	    int LA64_0 = input.LA(1);

            	    if ( (LA64_0 == IDENTIFIER || LA64_0 == CHARACTER_LITERAL || (LA64_0 >= NULL && LA64_0 <= CASE) || LA64_0 == LLABELBRACKET || LA64_0 == IF || (LA64_0 >= LOOP && LA64_0 <= FOR) || (LA64_0 >= DECLARE && LA64_0 <= GOTO) || LA64_0 == RETURN) )
            	    {
            	        alt64 = 1;
            	    }


            	    switch (alt64) 
            		{
            			case 1 :
            			    // Ada95.g:595:4: statement
            			    {
            			    	PushFollow(FOLLOW_statement_in_sequence_of_statements2992);
            			    	statement284 = statement();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, statement284.Tree);

            			    }
            			    break;

            			default:
            			    if ( cnt64 >= 1 ) goto loop64;
            			    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            		            EarlyExitException eee64 =
            		                new EarlyExitException(64, input);
            		            throw eee64;
            	    }
            	    cnt64++;
            	} while (true);

            	loop64:
            		;	// Stops C# compiler whining that label 'loop64' has no statements


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 79, sequence_of_statements_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "sequence_of_statements"

    public class statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "statement"
    // Ada95.g:597:1: statement : ( label )* ( simple_statement -> ^( SIMPLE_STATEMENT ( label )* simple_statement ) | compound_statement -> ^( COMPOUND_STATEMENT ( label )* compound_statement ) ) ;
    public Ada95Parser.statement_return statement() // throws RecognitionException [1]
    {   
        Ada95Parser.statement_return retval = new Ada95Parser.statement_return();
        retval.Start = input.LT(1);
        int statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.label_return label285 = default(Ada95Parser.label_return);

        Ada95Parser.simple_statement_return simple_statement286 = default(Ada95Parser.simple_statement_return);

        Ada95Parser.compound_statement_return compound_statement287 = default(Ada95Parser.compound_statement_return);


        RewriteRuleSubtreeStream stream_label = new RewriteRuleSubtreeStream(adaptor,"rule label");
        RewriteRuleSubtreeStream stream_simple_statement = new RewriteRuleSubtreeStream(adaptor,"rule simple_statement");
        RewriteRuleSubtreeStream stream_compound_statement = new RewriteRuleSubtreeStream(adaptor,"rule compound_statement");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 80) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:598:2: ( ( label )* ( simple_statement -> ^( SIMPLE_STATEMENT ( label )* simple_statement ) | compound_statement -> ^( COMPOUND_STATEMENT ( label )* compound_statement ) ) )
            // Ada95.g:599:3: ( label )* ( simple_statement -> ^( SIMPLE_STATEMENT ( label )* simple_statement ) | compound_statement -> ^( COMPOUND_STATEMENT ( label )* compound_statement ) )
            {
            	// Ada95.g:599:3: ( label )*
            	do 
            	{
            	    int alt65 = 2;
            	    int LA65_0 = input.LA(1);

            	    if ( (LA65_0 == LLABELBRACKET) )
            	    {
            	        alt65 = 1;
            	    }


            	    switch (alt65) 
            		{
            			case 1 :
            			    // Ada95.g:599:3: label
            			    {
            			    	PushFollow(FOLLOW_label_in_statement3005);
            			    	label285 = label();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( (state.backtracking==0) ) stream_label.Add(label285.Tree);

            			    }
            			    break;

            			default:
            			    goto loop65;
            	    }
            	} while (true);

            	loop65:
            		;	// Stops C# compiler whining that label 'loop65' has no statements

            	// Ada95.g:600:3: ( simple_statement -> ^( SIMPLE_STATEMENT ( label )* simple_statement ) | compound_statement -> ^( COMPOUND_STATEMENT ( label )* compound_statement ) )
            	int alt66 = 2;
            	switch ( input.LA(1) ) 
            	{
            	case IDENTIFIER:
            		{
            	    int LA66_1 = input.LA(2);

            	    if ( (LA66_1 == SEMICOLON || LA66_1 == ASSIGN || LA66_1 == LPARANTHESIS || LA66_1 == DOT || LA66_1 == APOSTROPHE) )
            	    {
            	        alt66 = 1;
            	    }
            	    else if ( (LA66_1 == COLON) )
            	    {
            	        alt66 = 2;
            	    }
            	    else 
            	    {
            	        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        NoViableAltException nvae_d66s1 =
            	            new NoViableAltException("", 66, 1, input);

            	        throw nvae_d66s1;
            	    }
            	    }
            	    break;
            	case CHARACTER_LITERAL:
            	case NULL:
            	case EXIT:
            	case GOTO:
            	case RETURN:
            		{
            	    alt66 = 1;
            	    }
            	    break;
            	case CASE:
            	case IF:
            	case LOOP:
            	case WHILE:
            	case FOR:
            	case DECLARE:
            	case BEGIN:
            		{
            	    alt66 = 2;
            	    }
            	    break;
            		default:
            		    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            		    NoViableAltException nvae_d66s0 =
            		        new NoViableAltException("", 66, 0, input);

            		    throw nvae_d66s0;
            	}

            	switch (alt66) 
            	{
            	    case 1 :
            	        // Ada95.g:601:4: simple_statement
            	        {
            	        	PushFollow(FOLLOW_simple_statement_in_statement3015);
            	        	simple_statement286 = simple_statement();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_simple_statement.Add(simple_statement286.Tree);


            	        	// AST REWRITE
            	        	// elements:          label, simple_statement
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 601:21: -> ^( SIMPLE_STATEMENT ( label )* simple_statement )
            	        	{
            	        	    // Ada95.g:601:24: ^( SIMPLE_STATEMENT ( label )* simple_statement )
            	        	    {
            	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(SIMPLE_STATEMENT, "SIMPLE_STATEMENT"), root_1);

            	        	    // Ada95.g:601:44: ( label )*
            	        	    while ( stream_label.HasNext() )
            	        	    {
            	        	        adaptor.AddChild(root_1, stream_label.NextTree());

            	        	    }
            	        	    stream_label.Reset();
            	        	    adaptor.AddChild(root_1, stream_simple_statement.NextTree());

            	        	    adaptor.AddChild(root_0, root_1);
            	        	    }

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;
            	    case 2 :
            	        // Ada95.g:602:4: compound_statement
            	        {
            	        	PushFollow(FOLLOW_compound_statement_in_statement3035);
            	        	compound_statement287 = compound_statement();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_compound_statement.Add(compound_statement287.Tree);


            	        	// AST REWRITE
            	        	// elements:          compound_statement, label
            	        	// token labels:      
            	        	// rule labels:       retval
            	        	// token list labels: 
            	        	// rule list labels:  
            	        	// wildcard labels: 
            	        	if ( (state.backtracking==0) ) {
            	        	retval.Tree = root_0;
            	        	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	        	root_0 = (CommonTree)adaptor.GetNilNode();
            	        	// 602:23: -> ^( COMPOUND_STATEMENT ( label )* compound_statement )
            	        	{
            	        	    // Ada95.g:602:26: ^( COMPOUND_STATEMENT ( label )* compound_statement )
            	        	    {
            	        	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(COMPOUND_STATEMENT, "COMPOUND_STATEMENT"), root_1);

            	        	    // Ada95.g:602:48: ( label )*
            	        	    while ( stream_label.HasNext() )
            	        	    {
            	        	        adaptor.AddChild(root_1, stream_label.NextTree());

            	        	    }
            	        	    stream_label.Reset();
            	        	    adaptor.AddChild(root_1, stream_compound_statement.NextTree());

            	        	    adaptor.AddChild(root_0, root_1);
            	        	    }

            	        	}

            	        	retval.Tree = root_0;retval.Tree = root_0;}
            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 80, statement_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "statement"

    public class simple_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "simple_statement"
    // Ada95.g:606:1: simple_statement : ( ( assignment_statement )=> assignment_statement | procedure_call_statement | return_statement | exit_statement | goto_statement | null_statement );
    public Ada95Parser.simple_statement_return simple_statement() // throws RecognitionException [1]
    {   
        Ada95Parser.simple_statement_return retval = new Ada95Parser.simple_statement_return();
        retval.Start = input.LT(1);
        int simple_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.assignment_statement_return assignment_statement288 = default(Ada95Parser.assignment_statement_return);

        Ada95Parser.procedure_call_statement_return procedure_call_statement289 = default(Ada95Parser.procedure_call_statement_return);

        Ada95Parser.return_statement_return return_statement290 = default(Ada95Parser.return_statement_return);

        Ada95Parser.exit_statement_return exit_statement291 = default(Ada95Parser.exit_statement_return);

        Ada95Parser.goto_statement_return goto_statement292 = default(Ada95Parser.goto_statement_return);

        Ada95Parser.null_statement_return null_statement293 = default(Ada95Parser.null_statement_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 81) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:607:2: ( ( assignment_statement )=> assignment_statement | procedure_call_statement | return_statement | exit_statement | goto_statement | null_statement )
            int alt67 = 6;
            int LA67_0 = input.LA(1);

            if ( (LA67_0 == IDENTIFIER) )
            {
                int LA67_1 = input.LA(2);

                if ( (((synpred17_Ada95() && ( IsFunction( input.LT( 1 ).Text ) ))|| (synpred17_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred17_Ada95() && ( IsType( input.LT( 1 ).Text ) ))|| synpred17_Ada95())) )
                {
                    alt67 = 1;
                }
                else if ( (true) )
                {
                    alt67 = 2;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d67s1 =
                        new NoViableAltException("", 67, 1, input);

                    throw nvae_d67s1;
                }
            }
            else if ( (LA67_0 == CHARACTER_LITERAL) && (synpred17_Ada95()) )
            {
                alt67 = 1;
            }
            else if ( (LA67_0 == RETURN) )
            {
                alt67 = 3;
            }
            else if ( (LA67_0 == EXIT) )
            {
                alt67 = 4;
            }
            else if ( (LA67_0 == GOTO) )
            {
                alt67 = 5;
            }
            else if ( (LA67_0 == NULL) )
            {
                alt67 = 6;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d67s0 =
                    new NoViableAltException("", 67, 0, input);

                throw nvae_d67s0;
            }
            switch (alt67) 
            {
                case 1 :
                    // Ada95.g:608:3: ( assignment_statement )=> assignment_statement
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_assignment_statement_in_simple_statement3072);
                    	assignment_statement288 = assignment_statement();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, assignment_statement288.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:609:10: procedure_call_statement
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_procedure_call_statement_in_simple_statement3085);
                    	procedure_call_statement289 = procedure_call_statement();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, procedure_call_statement289.Tree);

                    }
                    break;
                case 3 :
                    // Ada95.g:610:10: return_statement
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_return_statement_in_simple_statement3098);
                    	return_statement290 = return_statement();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, return_statement290.Tree);

                    }
                    break;
                case 4 :
                    // Ada95.g:611:10: exit_statement
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_exit_statement_in_simple_statement3111);
                    	exit_statement291 = exit_statement();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, exit_statement291.Tree);

                    }
                    break;
                case 5 :
                    // Ada95.g:612:10: goto_statement
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_goto_statement_in_simple_statement3124);
                    	goto_statement292 = goto_statement();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, goto_statement292.Tree);

                    }
                    break;
                case 6 :
                    // Ada95.g:613:10: null_statement
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_null_statement_in_simple_statement3137);
                    	null_statement293 = null_statement();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, null_statement293.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 81, simple_statement_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "simple_statement"

    public class compound_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "compound_statement"
    // Ada95.g:616:1: compound_statement : ( if_statement | case_statement | loop_statement | block_statement );
    public Ada95Parser.compound_statement_return compound_statement() // throws RecognitionException [1]
    {   
        Ada95Parser.compound_statement_return retval = new Ada95Parser.compound_statement_return();
        retval.Start = input.LT(1);
        int compound_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.if_statement_return if_statement294 = default(Ada95Parser.if_statement_return);

        Ada95Parser.case_statement_return case_statement295 = default(Ada95Parser.case_statement_return);

        Ada95Parser.loop_statement_return loop_statement296 = default(Ada95Parser.loop_statement_return);

        Ada95Parser.block_statement_return block_statement297 = default(Ada95Parser.block_statement_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 82) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:617:2: ( if_statement | case_statement | loop_statement | block_statement )
            int alt68 = 4;
            switch ( input.LA(1) ) 
            {
            case IF:
            	{
                alt68 = 1;
                }
                break;
            case CASE:
            	{
                alt68 = 2;
                }
                break;
            case IDENTIFIER:
            	{
                int LA68_3 = input.LA(2);

                if ( (LA68_3 == COLON) )
                {
                    int LA68_6 = input.LA(3);

                    if ( ((LA68_6 >= LOOP && LA68_6 <= FOR)) )
                    {
                        alt68 = 3;
                    }
                    else if ( ((LA68_6 >= DECLARE && LA68_6 <= BEGIN)) )
                    {
                        alt68 = 4;
                    }
                    else 
                    {
                        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                        NoViableAltException nvae_d68s6 =
                            new NoViableAltException("", 68, 6, input);

                        throw nvae_d68s6;
                    }
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d68s3 =
                        new NoViableAltException("", 68, 3, input);

                    throw nvae_d68s3;
                }
                }
                break;
            case LOOP:
            case WHILE:
            case FOR:
            	{
                alt68 = 3;
                }
                break;
            case DECLARE:
            case BEGIN:
            	{
                alt68 = 4;
                }
                break;
            	default:
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    NoViableAltException nvae_d68s0 =
            	        new NoViableAltException("", 68, 0, input);

            	    throw nvae_d68s0;
            }

            switch (alt68) 
            {
                case 1 :
                    // Ada95.g:618:3: if_statement
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_if_statement_in_compound_statement3150);
                    	if_statement294 = if_statement();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, if_statement294.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:619:3: case_statement
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_case_statement_in_compound_statement3156);
                    	case_statement295 = case_statement();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, case_statement295.Tree);

                    }
                    break;
                case 3 :
                    // Ada95.g:620:3: loop_statement
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_loop_statement_in_compound_statement3162);
                    	loop_statement296 = loop_statement();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, loop_statement296.Tree);

                    }
                    break;
                case 4 :
                    // Ada95.g:621:3: block_statement
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_block_statement_in_compound_statement3168);
                    	block_statement297 = block_statement();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, block_statement297.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 82, compound_statement_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "compound_statement"

    public class null_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "null_statement"
    // Ada95.g:624:1: null_statement : NULL SEMICOLON ;
    public Ada95Parser.null_statement_return null_statement() // throws RecognitionException [1]
    {   
        Ada95Parser.null_statement_return retval = new Ada95Parser.null_statement_return();
        retval.Start = input.LT(1);
        int null_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken NULL298 = null;
        IToken SEMICOLON299 = null;

        CommonTree NULL298_tree=null;
        CommonTree SEMICOLON299_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 83) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:625:2: ( NULL SEMICOLON )
            // Ada95.g:625:4: NULL SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	NULL298=(IToken)Match(input,NULL,FOLLOW_NULL_in_null_statement3179); if (state.failed) return retval;
            	SEMICOLON299=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_null_statement3182); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 83, null_statement_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "null_statement"

    public class label_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "label"
    // Ada95.g:627:1: label : LLABELBRACKET statement_identifier RLABELBRACKET -> ^( LABEL statement_identifier ) ;
    public Ada95Parser.label_return label() // throws RecognitionException [1]
    {   
        Ada95Parser.label_return retval = new Ada95Parser.label_return();
        retval.Start = input.LT(1);
        int label_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken LLABELBRACKET300 = null;
        IToken RLABELBRACKET302 = null;
        Ada95Parser.statement_identifier_return statement_identifier301 = default(Ada95Parser.statement_identifier_return);


        CommonTree LLABELBRACKET300_tree=null;
        CommonTree RLABELBRACKET302_tree=null;
        RewriteRuleTokenStream stream_RLABELBRACKET = new RewriteRuleTokenStream(adaptor,"token RLABELBRACKET");
        RewriteRuleTokenStream stream_LLABELBRACKET = new RewriteRuleTokenStream(adaptor,"token LLABELBRACKET");
        RewriteRuleSubtreeStream stream_statement_identifier = new RewriteRuleSubtreeStream(adaptor,"rule statement_identifier");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 84) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:628:2: ( LLABELBRACKET statement_identifier RLABELBRACKET -> ^( LABEL statement_identifier ) )
            // Ada95.g:628:4: LLABELBRACKET statement_identifier RLABELBRACKET
            {
            	LLABELBRACKET300=(IToken)Match(input,LLABELBRACKET,FOLLOW_LLABELBRACKET_in_label3193); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_LLABELBRACKET.Add(LLABELBRACKET300);

            	PushFollow(FOLLOW_statement_identifier_in_label3195);
            	statement_identifier301 = statement_identifier();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_statement_identifier.Add(statement_identifier301.Tree);
            	RLABELBRACKET302=(IToken)Match(input,RLABELBRACKET,FOLLOW_RLABELBRACKET_in_label3197); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_RLABELBRACKET.Add(RLABELBRACKET302);



            	// AST REWRITE
            	// elements:          statement_identifier
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 628:53: -> ^( LABEL statement_identifier )
            	{
            	    // Ada95.g:628:56: ^( LABEL statement_identifier )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(LABEL, "LABEL"), root_1);

            	    adaptor.AddChild(root_1, stream_statement_identifier.NextTree());

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 84, label_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "label"

    public class statement_identifier_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "statement_identifier"
    // Ada95.g:630:1: statement_identifier : IDENTIFIER ;
    public Ada95Parser.statement_identifier_return statement_identifier() // throws RecognitionException [1]
    {   
        Ada95Parser.statement_identifier_return retval = new Ada95Parser.statement_identifier_return();
        retval.Start = input.LT(1);
        int statement_identifier_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IDENTIFIER303 = null;

        CommonTree IDENTIFIER303_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 85) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:631:2: ( IDENTIFIER )
            // Ada95.g:631:4: IDENTIFIER
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	IDENTIFIER303=(IToken)Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_statement_identifier3217); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{IDENTIFIER303_tree = (CommonTree)adaptor.Create(IDENTIFIER303);
            		adaptor.AddChild(root_0, IDENTIFIER303_tree);
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 85, statement_identifier_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "statement_identifier"

    public class assignment_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "assignment_statement"
    // Ada95.g:633:1: assignment_statement : name ASSIGN expression SEMICOLON ;
    public Ada95Parser.assignment_statement_return assignment_statement() // throws RecognitionException [1]
    {   
        Ada95Parser.assignment_statement_return retval = new Ada95Parser.assignment_statement_return();
        retval.Start = input.LT(1);
        int assignment_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken ASSIGN305 = null;
        IToken SEMICOLON307 = null;
        Ada95Parser.name_return name304 = default(Ada95Parser.name_return);

        Ada95Parser.expression_return expression306 = default(Ada95Parser.expression_return);


        CommonTree ASSIGN305_tree=null;
        CommonTree SEMICOLON307_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 86) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:634:2: ( name ASSIGN expression SEMICOLON )
            // Ada95.g:634:4: name ASSIGN expression SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_name_in_assignment_statement3227);
            	name304 = name();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, name304.Tree);
            	ASSIGN305=(IToken)Match(input,ASSIGN,FOLLOW_ASSIGN_in_assignment_statement3229); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{ASSIGN305_tree = (CommonTree)adaptor.Create(ASSIGN305);
            		root_0 = (CommonTree)adaptor.BecomeRoot(ASSIGN305_tree, root_0);
            	}
            	PushFollow(FOLLOW_expression_in_assignment_statement3232);
            	expression306 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression306.Tree);
            	SEMICOLON307=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_assignment_statement3234); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 86, assignment_statement_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "assignment_statement"

    public class if_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "if_statement"
    // Ada95.g:636:1: if_statement : IF condition THEN sequence_of_statements ( ELSIF condition THEN sequence_of_statements )* ( ELSE sequence_of_statements )? END IF SEMICOLON ;
    public Ada95Parser.if_statement_return if_statement() // throws RecognitionException [1]
    {   
        Ada95Parser.if_statement_return retval = new Ada95Parser.if_statement_return();
        retval.Start = input.LT(1);
        int if_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IF308 = null;
        IToken THEN310 = null;
        IToken ELSIF312 = null;
        IToken THEN314 = null;
        IToken ELSE316 = null;
        IToken END318 = null;
        IToken IF319 = null;
        IToken SEMICOLON320 = null;
        Ada95Parser.condition_return condition309 = default(Ada95Parser.condition_return);

        Ada95Parser.sequence_of_statements_return sequence_of_statements311 = default(Ada95Parser.sequence_of_statements_return);

        Ada95Parser.condition_return condition313 = default(Ada95Parser.condition_return);

        Ada95Parser.sequence_of_statements_return sequence_of_statements315 = default(Ada95Parser.sequence_of_statements_return);

        Ada95Parser.sequence_of_statements_return sequence_of_statements317 = default(Ada95Parser.sequence_of_statements_return);


        CommonTree IF308_tree=null;
        CommonTree THEN310_tree=null;
        CommonTree ELSIF312_tree=null;
        CommonTree THEN314_tree=null;
        CommonTree ELSE316_tree=null;
        CommonTree END318_tree=null;
        CommonTree IF319_tree=null;
        CommonTree SEMICOLON320_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 87) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:637:2: ( IF condition THEN sequence_of_statements ( ELSIF condition THEN sequence_of_statements )* ( ELSE sequence_of_statements )? END IF SEMICOLON )
            // Ada95.g:637:4: IF condition THEN sequence_of_statements ( ELSIF condition THEN sequence_of_statements )* ( ELSE sequence_of_statements )? END IF SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	IF308=(IToken)Match(input,IF,FOLLOW_IF_in_if_statement3245); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{IF308_tree = (CommonTree)adaptor.Create(IF308);
            		root_0 = (CommonTree)adaptor.BecomeRoot(IF308_tree, root_0);
            	}
            	PushFollow(FOLLOW_condition_in_if_statement3248);
            	condition309 = condition();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, condition309.Tree);
            	THEN310=(IToken)Match(input,THEN,FOLLOW_THEN_in_if_statement3250); if (state.failed) return retval;
            	PushFollow(FOLLOW_sequence_of_statements_in_if_statement3253);
            	sequence_of_statements311 = sequence_of_statements();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, sequence_of_statements311.Tree);
            	// Ada95.g:637:47: ( ELSIF condition THEN sequence_of_statements )*
            	do 
            	{
            	    int alt69 = 2;
            	    int LA69_0 = input.LA(1);

            	    if ( (LA69_0 == ELSIF) )
            	    {
            	        alt69 = 1;
            	    }


            	    switch (alt69) 
            		{
            			case 1 :
            			    // Ada95.g:637:49: ELSIF condition THEN sequence_of_statements
            			    {
            			    	ELSIF312=(IToken)Match(input,ELSIF,FOLLOW_ELSIF_in_if_statement3257); if (state.failed) return retval;
            			    	PushFollow(FOLLOW_condition_in_if_statement3260);
            			    	condition313 = condition();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, condition313.Tree);
            			    	THEN314=(IToken)Match(input,THEN,FOLLOW_THEN_in_if_statement3262); if (state.failed) return retval;
            			    	PushFollow(FOLLOW_sequence_of_statements_in_if_statement3265);
            			    	sequence_of_statements315 = sequence_of_statements();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, sequence_of_statements315.Tree);

            			    }
            			    break;

            			default:
            			    goto loop69;
            	    }
            	} while (true);

            	loop69:
            		;	// Stops C# compiler whining that label 'loop69' has no statements

            	// Ada95.g:637:98: ( ELSE sequence_of_statements )?
            	int alt70 = 2;
            	int LA70_0 = input.LA(1);

            	if ( (LA70_0 == ELSE) )
            	{
            	    alt70 = 1;
            	}
            	switch (alt70) 
            	{
            	    case 1 :
            	        // Ada95.g:637:100: ELSE sequence_of_statements
            	        {
            	        	ELSE316=(IToken)Match(input,ELSE,FOLLOW_ELSE_in_if_statement3272); if (state.failed) return retval;
            	        	if ( state.backtracking == 0 )
            	        	{ELSE316_tree = (CommonTree)adaptor.Create(ELSE316);
            	        		adaptor.AddChild(root_0, ELSE316_tree);
            	        	}
            	        	PushFollow(FOLLOW_sequence_of_statements_in_if_statement3274);
            	        	sequence_of_statements317 = sequence_of_statements();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, sequence_of_statements317.Tree);

            	        }
            	        break;

            	}

            	END318=(IToken)Match(input,END,FOLLOW_END_in_if_statement3279); if (state.failed) return retval;
            	IF319=(IToken)Match(input,IF,FOLLOW_IF_in_if_statement3282); if (state.failed) return retval;
            	SEMICOLON320=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_if_statement3285); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 87, if_statement_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "if_statement"

    public class condition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "condition"
    // Ada95.g:639:1: condition : expression ;
    public Ada95Parser.condition_return condition() // throws RecognitionException [1]
    {   
        Ada95Parser.condition_return retval = new Ada95Parser.condition_return();
        retval.Start = input.LT(1);
        int condition_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.expression_return expression321 = default(Ada95Parser.expression_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 88) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:640:2: ( expression )
            // Ada95.g:640:4: expression
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_expression_in_condition3296);
            	expression321 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression321.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 88, condition_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "condition"

    public class case_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "case_statement"
    // Ada95.g:642:1: case_statement : CASE expression IS ( ( ( case_statement_alternative )* other_case_statement_alternative )=> ( case_statement_alternative )* other_case_statement_alternative | ( case_statement_alternative )+ ) END CASE SEMICOLON ;
    public Ada95Parser.case_statement_return case_statement() // throws RecognitionException [1]
    {   
        Ada95Parser.case_statement_return retval = new Ada95Parser.case_statement_return();
        retval.Start = input.LT(1);
        int case_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken CASE322 = null;
        IToken IS324 = null;
        IToken END328 = null;
        IToken CASE329 = null;
        IToken SEMICOLON330 = null;
        Ada95Parser.expression_return expression323 = default(Ada95Parser.expression_return);

        Ada95Parser.case_statement_alternative_return case_statement_alternative325 = default(Ada95Parser.case_statement_alternative_return);

        Ada95Parser.other_case_statement_alternative_return other_case_statement_alternative326 = default(Ada95Parser.other_case_statement_alternative_return);

        Ada95Parser.case_statement_alternative_return case_statement_alternative327 = default(Ada95Parser.case_statement_alternative_return);


        CommonTree CASE322_tree=null;
        CommonTree IS324_tree=null;
        CommonTree END328_tree=null;
        CommonTree CASE329_tree=null;
        CommonTree SEMICOLON330_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 89) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:643:2: ( CASE expression IS ( ( ( case_statement_alternative )* other_case_statement_alternative )=> ( case_statement_alternative )* other_case_statement_alternative | ( case_statement_alternative )+ ) END CASE SEMICOLON )
            // Ada95.g:644:3: CASE expression IS ( ( ( case_statement_alternative )* other_case_statement_alternative )=> ( case_statement_alternative )* other_case_statement_alternative | ( case_statement_alternative )+ ) END CASE SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	CASE322=(IToken)Match(input,CASE,FOLLOW_CASE_in_case_statement3308); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{CASE322_tree = (CommonTree)adaptor.Create(CASE322);
            		root_0 = (CommonTree)adaptor.BecomeRoot(CASE322_tree, root_0);
            	}
            	PushFollow(FOLLOW_expression_in_case_statement3311);
            	expression323 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression323.Tree);
            	IS324=(IToken)Match(input,IS,FOLLOW_IS_in_case_statement3313); if (state.failed) return retval;
            	// Ada95.g:645:3: ( ( ( case_statement_alternative )* other_case_statement_alternative )=> ( case_statement_alternative )* other_case_statement_alternative | ( case_statement_alternative )+ )
            	int alt73 = 2;
            	int LA73_0 = input.LA(1);

            	if ( (LA73_0 == WHEN) )
            	{
            	    int LA73_1 = input.LA(2);

            	    if ( (synpred18_Ada95()) )
            	    {
            	        alt73 = 1;
            	    }
            	    else if ( (true) )
            	    {
            	        alt73 = 2;
            	    }
            	    else 
            	    {
            	        if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        NoViableAltException nvae_d73s1 =
            	            new NoViableAltException("", 73, 1, input);

            	        throw nvae_d73s1;
            	    }
            	}
            	else 
            	{
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    NoViableAltException nvae_d73s0 =
            	        new NoViableAltException("", 73, 0, input);

            	    throw nvae_d73s0;
            	}
            	switch (alt73) 
            	{
            	    case 1 :
            	        // Ada95.g:646:4: ( ( case_statement_alternative )* other_case_statement_alternative )=> ( case_statement_alternative )* other_case_statement_alternative
            	        {
            	        	// Ada95.g:646:71: ( case_statement_alternative )*
            	        	do 
            	        	{
            	        	    int alt71 = 2;
            	        	    int LA71_0 = input.LA(1);

            	        	    if ( (LA71_0 == WHEN) )
            	        	    {
            	        	        int LA71_1 = input.LA(2);

            	        	        if ( (LA71_1 == IDENTIFIER || LA71_1 == LPARANTHESIS || LA71_1 == CHARACTER_LITERAL || LA71_1 == NULL || LA71_1 == NOT || (LA71_1 >= ABS && LA71_1 <= STRING_LITERAL) || (LA71_1 >= PLUS && LA71_1 <= MINUS)) )
            	        	        {
            	        	            alt71 = 1;
            	        	        }


            	        	    }


            	        	    switch (alt71) 
            	        		{
            	        			case 1 :
            	        			    // Ada95.g:646:71: case_statement_alternative
            	        			    {
            	        			    	PushFollow(FOLLOW_case_statement_alternative_in_case_statement3333);
            	        			    	case_statement_alternative325 = case_statement_alternative();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, case_statement_alternative325.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    goto loop71;
            	        	    }
            	        	} while (true);

            	        	loop71:
            	        		;	// Stops C# compiler whining that label 'loop71' has no statements

            	        	PushFollow(FOLLOW_other_case_statement_alternative_in_case_statement3336);
            	        	other_case_statement_alternative326 = other_case_statement_alternative();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, other_case_statement_alternative326.Tree);

            	        }
            	        break;
            	    case 2 :
            	        // Ada95.g:647:21: ( case_statement_alternative )+
            	        {
            	        	// Ada95.g:647:21: ( case_statement_alternative )+
            	        	int cnt72 = 0;
            	        	do 
            	        	{
            	        	    int alt72 = 2;
            	        	    int LA72_0 = input.LA(1);

            	        	    if ( (LA72_0 == WHEN) )
            	        	    {
            	        	        alt72 = 1;
            	        	    }


            	        	    switch (alt72) 
            	        		{
            	        			case 1 :
            	        			    // Ada95.g:647:21: case_statement_alternative
            	        			    {
            	        			    	PushFollow(FOLLOW_case_statement_alternative_in_case_statement3360);
            	        			    	case_statement_alternative327 = case_statement_alternative();
            	        			    	state.followingStackPointer--;
            	        			    	if (state.failed) return retval;
            	        			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, case_statement_alternative327.Tree);

            	        			    }
            	        			    break;

            	        			default:
            	        			    if ( cnt72 >= 1 ) goto loop72;
            	        			    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	        		            EarlyExitException eee72 =
            	        		                new EarlyExitException(72, input);
            	        		            throw eee72;
            	        	    }
            	        	    cnt72++;
            	        	} while (true);

            	        	loop72:
            	        		;	// Stops C# compiler whining that label 'loop72' has no statements


            	        }
            	        break;

            	}

            	END328=(IToken)Match(input,END,FOLLOW_END_in_case_statement3367); if (state.failed) return retval;
            	CASE329=(IToken)Match(input,CASE,FOLLOW_CASE_in_case_statement3370); if (state.failed) return retval;
            	SEMICOLON330=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_case_statement3373); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 89, case_statement_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "case_statement"

    public class case_statement_alternative_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "case_statement_alternative"
    // Ada95.g:651:1: case_statement_alternative : WHEN discrete_choice_list ASSOCIATION sequence_of_statements ;
    public Ada95Parser.case_statement_alternative_return case_statement_alternative() // throws RecognitionException [1]
    {   
        Ada95Parser.case_statement_alternative_return retval = new Ada95Parser.case_statement_alternative_return();
        retval.Start = input.LT(1);
        int case_statement_alternative_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken WHEN331 = null;
        IToken ASSOCIATION333 = null;
        Ada95Parser.discrete_choice_list_return discrete_choice_list332 = default(Ada95Parser.discrete_choice_list_return);

        Ada95Parser.sequence_of_statements_return sequence_of_statements334 = default(Ada95Parser.sequence_of_statements_return);


        CommonTree WHEN331_tree=null;
        CommonTree ASSOCIATION333_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 90) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:652:2: ( WHEN discrete_choice_list ASSOCIATION sequence_of_statements )
            // Ada95.g:652:4: WHEN discrete_choice_list ASSOCIATION sequence_of_statements
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	WHEN331=(IToken)Match(input,WHEN,FOLLOW_WHEN_in_case_statement_alternative3385); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{WHEN331_tree = (CommonTree)adaptor.Create(WHEN331);
            		root_0 = (CommonTree)adaptor.BecomeRoot(WHEN331_tree, root_0);
            	}
            	PushFollow(FOLLOW_discrete_choice_list_in_case_statement_alternative3388);
            	discrete_choice_list332 = discrete_choice_list();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, discrete_choice_list332.Tree);
            	ASSOCIATION333=(IToken)Match(input,ASSOCIATION,FOLLOW_ASSOCIATION_in_case_statement_alternative3390); if (state.failed) return retval;
            	PushFollow(FOLLOW_sequence_of_statements_in_case_statement_alternative3393);
            	sequence_of_statements334 = sequence_of_statements();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, sequence_of_statements334.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 90, case_statement_alternative_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "case_statement_alternative"

    public class other_case_statement_alternative_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "other_case_statement_alternative"
    // Ada95.g:654:1: other_case_statement_alternative : WHEN OTHERS ASSOCIATION sequence_of_statements ;
    public Ada95Parser.other_case_statement_alternative_return other_case_statement_alternative() // throws RecognitionException [1]
    {   
        Ada95Parser.other_case_statement_alternative_return retval = new Ada95Parser.other_case_statement_alternative_return();
        retval.Start = input.LT(1);
        int other_case_statement_alternative_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken WHEN335 = null;
        IToken OTHERS336 = null;
        IToken ASSOCIATION337 = null;
        Ada95Parser.sequence_of_statements_return sequence_of_statements338 = default(Ada95Parser.sequence_of_statements_return);


        CommonTree WHEN335_tree=null;
        CommonTree OTHERS336_tree=null;
        CommonTree ASSOCIATION337_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 91) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:655:2: ( WHEN OTHERS ASSOCIATION sequence_of_statements )
            // Ada95.g:655:4: WHEN OTHERS ASSOCIATION sequence_of_statements
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	WHEN335=(IToken)Match(input,WHEN,FOLLOW_WHEN_in_other_case_statement_alternative3403); if (state.failed) return retval;
            	OTHERS336=(IToken)Match(input,OTHERS,FOLLOW_OTHERS_in_other_case_statement_alternative3406); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{OTHERS336_tree = (CommonTree)adaptor.Create(OTHERS336);
            		root_0 = (CommonTree)adaptor.BecomeRoot(OTHERS336_tree, root_0);
            	}
            	ASSOCIATION337=(IToken)Match(input,ASSOCIATION,FOLLOW_ASSOCIATION_in_other_case_statement_alternative3409); if (state.failed) return retval;
            	PushFollow(FOLLOW_sequence_of_statements_in_other_case_statement_alternative3412);
            	sequence_of_statements338 = sequence_of_statements();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, sequence_of_statements338.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 91, other_case_statement_alternative_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "other_case_statement_alternative"

    public class loop_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "loop_statement"
    // Ada95.g:657:1: loop_statement : ( statement_identifier COLON )? ( iteration_scheme )? LOOP handled_sequence_of_statements END LOOP ( statement_identifier )? SEMICOLON ;
    public Ada95Parser.loop_statement_return loop_statement() // throws RecognitionException [1]
    {   
        SymbolTable_stack.Push(new SymbolTable_scope());

        Ada95Parser.loop_statement_return retval = new Ada95Parser.loop_statement_return();
        retval.Start = input.LT(1);
        int loop_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken COLON340 = null;
        IToken LOOP342 = null;
        IToken END344 = null;
        IToken LOOP345 = null;
        IToken SEMICOLON347 = null;
        Ada95Parser.statement_identifier_return statement_identifier339 = default(Ada95Parser.statement_identifier_return);

        Ada95Parser.iteration_scheme_return iteration_scheme341 = default(Ada95Parser.iteration_scheme_return);

        Ada95Parser.handled_sequence_of_statements_return handled_sequence_of_statements343 = default(Ada95Parser.handled_sequence_of_statements_return);

        Ada95Parser.statement_identifier_return statement_identifier346 = default(Ada95Parser.statement_identifier_return);


        CommonTree COLON340_tree=null;
        CommonTree LOOP342_tree=null;
        CommonTree END344_tree=null;
        CommonTree LOOP345_tree=null;
        CommonTree SEMICOLON347_tree=null;


        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Functions =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Procedures =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Objects =  new SortedSet< string >();
        	
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 92) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:666:2: ( ( statement_identifier COLON )? ( iteration_scheme )? LOOP handled_sequence_of_statements END LOOP ( statement_identifier )? SEMICOLON )
            // Ada95.g:666:4: ( statement_identifier COLON )? ( iteration_scheme )? LOOP handled_sequence_of_statements END LOOP ( statement_identifier )? SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	// Ada95.g:666:4: ( statement_identifier COLON )?
            	int alt74 = 2;
            	int LA74_0 = input.LA(1);

            	if ( (LA74_0 == IDENTIFIER) )
            	{
            	    alt74 = 1;
            	}
            	switch (alt74) 
            	{
            	    case 1 :
            	        // Ada95.g:666:6: statement_identifier COLON
            	        {
            	        	PushFollow(FOLLOW_statement_identifier_in_loop_statement3437);
            	        	statement_identifier339 = statement_identifier();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, statement_identifier339.Tree);
            	        	COLON340=(IToken)Match(input,COLON,FOLLOW_COLON_in_loop_statement3439); if (state.failed) return retval;

            	        }
            	        break;

            	}

            	// Ada95.g:666:37: ( iteration_scheme )?
            	int alt75 = 2;
            	int LA75_0 = input.LA(1);

            	if ( ((LA75_0 >= WHILE && LA75_0 <= FOR)) )
            	{
            	    alt75 = 1;
            	}
            	switch (alt75) 
            	{
            	    case 1 :
            	        // Ada95.g:666:37: iteration_scheme
            	        {
            	        	PushFollow(FOLLOW_iteration_scheme_in_loop_statement3445);
            	        	iteration_scheme341 = iteration_scheme();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, iteration_scheme341.Tree);

            	        }
            	        break;

            	}

            	LOOP342=(IToken)Match(input,LOOP,FOLLOW_LOOP_in_loop_statement3448); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{LOOP342_tree = (CommonTree)adaptor.Create(LOOP342);
            		root_0 = (CommonTree)adaptor.BecomeRoot(LOOP342_tree, root_0);
            	}
            	PushFollow(FOLLOW_handled_sequence_of_statements_in_loop_statement3451);
            	handled_sequence_of_statements343 = handled_sequence_of_statements();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, handled_sequence_of_statements343.Tree);
            	END344=(IToken)Match(input,END,FOLLOW_END_in_loop_statement3453); if (state.failed) return retval;
            	LOOP345=(IToken)Match(input,LOOP,FOLLOW_LOOP_in_loop_statement3456); if (state.failed) return retval;
            	// Ada95.g:666:103: ( statement_identifier )?
            	int alt76 = 2;
            	int LA76_0 = input.LA(1);

            	if ( (LA76_0 == IDENTIFIER) )
            	{
            	    alt76 = 1;
            	}
            	switch (alt76) 
            	{
            	    case 1 :
            	        // Ada95.g:666:103: statement_identifier
            	        {
            	        	PushFollow(FOLLOW_statement_identifier_in_loop_statement3459);
            	        	statement_identifier346 = statement_identifier();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, statement_identifier346.Tree);

            	        }
            	        break;

            	}

            	SEMICOLON347=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_loop_statement3462); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 92, loop_statement_StartIndex); 
            }
            SymbolTable_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "loop_statement"

    public class iteration_scheme_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "iteration_scheme"
    // Ada95.g:668:1: iteration_scheme : ( WHILE condition | FOR loop_parameter_specification );
    public Ada95Parser.iteration_scheme_return iteration_scheme() // throws RecognitionException [1]
    {   
        Ada95Parser.iteration_scheme_return retval = new Ada95Parser.iteration_scheme_return();
        retval.Start = input.LT(1);
        int iteration_scheme_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken WHILE348 = null;
        IToken FOR350 = null;
        Ada95Parser.condition_return condition349 = default(Ada95Parser.condition_return);

        Ada95Parser.loop_parameter_specification_return loop_parameter_specification351 = default(Ada95Parser.loop_parameter_specification_return);


        CommonTree WHILE348_tree=null;
        CommonTree FOR350_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 93) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:669:2: ( WHILE condition | FOR loop_parameter_specification )
            int alt77 = 2;
            int LA77_0 = input.LA(1);

            if ( (LA77_0 == WHILE) )
            {
                alt77 = 1;
            }
            else if ( (LA77_0 == FOR) )
            {
                alt77 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d77s0 =
                    new NoViableAltException("", 77, 0, input);

                throw nvae_d77s0;
            }
            switch (alt77) 
            {
                case 1 :
                    // Ada95.g:670:3: WHILE condition
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	WHILE348=(IToken)Match(input,WHILE,FOLLOW_WHILE_in_iteration_scheme3475); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{WHILE348_tree = (CommonTree)adaptor.Create(WHILE348);
                    		root_0 = (CommonTree)adaptor.BecomeRoot(WHILE348_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_condition_in_iteration_scheme3478);
                    	condition349 = condition();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, condition349.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:671:3: FOR loop_parameter_specification
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	FOR350=(IToken)Match(input,FOR,FOLLOW_FOR_in_iteration_scheme3484); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{FOR350_tree = (CommonTree)adaptor.Create(FOR350);
                    		root_0 = (CommonTree)adaptor.BecomeRoot(FOR350_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_loop_parameter_specification_in_iteration_scheme3487);
                    	loop_parameter_specification351 = loop_parameter_specification();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, loop_parameter_specification351.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 93, iteration_scheme_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "iteration_scheme"

    public class loop_parameter_specification_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "loop_parameter_specification"
    // Ada95.g:674:1: loop_parameter_specification : i= defining_identifier IN ( REVERSE )? discrete_subtype_definition ;
    public Ada95Parser.loop_parameter_specification_return loop_parameter_specification() // throws RecognitionException [1]
    {   
        Ada95Parser.loop_parameter_specification_return retval = new Ada95Parser.loop_parameter_specification_return();
        retval.Start = input.LT(1);
        int loop_parameter_specification_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IN352 = null;
        IToken REVERSE353 = null;
        Ada95Parser.defining_identifier_return i = default(Ada95Parser.defining_identifier_return);

        Ada95Parser.discrete_subtype_definition_return discrete_subtype_definition354 = default(Ada95Parser.discrete_subtype_definition_return);


        CommonTree IN352_tree=null;
        CommonTree REVERSE353_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 94) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:675:2: (i= defining_identifier IN ( REVERSE )? discrete_subtype_definition )
            // Ada95.g:676:3: i= defining_identifier IN ( REVERSE )? discrete_subtype_definition
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_defining_identifier_in_loop_parameter_specification3504);
            	i = defining_identifier();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, i.Tree);
            	if ( (state.backtracking==0) )
            	{
            	   ((SymbolTable_scope)SymbolTable_stack.Peek()).Objects.Add( ((i != null) ? input.ToString((IToken)(i.Start),(IToken)(i.Stop)) : null) ); 
            	}
            	IN352=(IToken)Match(input,IN,FOLLOW_IN_in_loop_parameter_specification3512); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{IN352_tree = (CommonTree)adaptor.Create(IN352);
            		root_0 = (CommonTree)adaptor.BecomeRoot(IN352_tree, root_0);
            	}
            	// Ada95.g:678:7: ( REVERSE )?
            	int alt78 = 2;
            	int LA78_0 = input.LA(1);

            	if ( (LA78_0 == REVERSE) )
            	{
            	    alt78 = 1;
            	}
            	switch (alt78) 
            	{
            	    case 1 :
            	        // Ada95.g:678:7: REVERSE
            	        {
            	        	REVERSE353=(IToken)Match(input,REVERSE,FOLLOW_REVERSE_in_loop_parameter_specification3515); if (state.failed) return retval;
            	        	if ( state.backtracking == 0 )
            	        	{REVERSE353_tree = (CommonTree)adaptor.Create(REVERSE353);
            	        		adaptor.AddChild(root_0, REVERSE353_tree);
            	        	}

            	        }
            	        break;

            	}

            	PushFollow(FOLLOW_discrete_subtype_definition_in_loop_parameter_specification3518);
            	discrete_subtype_definition354 = discrete_subtype_definition();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, discrete_subtype_definition354.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 94, loop_parameter_specification_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "loop_parameter_specification"

    public class block_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "block_statement"
    // Ada95.g:681:1: block_statement : ( statement_identifier COLON )? ( DECLARE declarative_part )? BEGIN sequence_of_statements END ( statement_identifier )? SEMICOLON -> ^( BLOCK ( statement_identifier )? ( declarative_part )? sequence_of_statements ( statement_identifier )? ) ;
    public Ada95Parser.block_statement_return block_statement() // throws RecognitionException [1]
    {   
        SymbolTable_stack.Push(new SymbolTable_scope());

        Ada95Parser.block_statement_return retval = new Ada95Parser.block_statement_return();
        retval.Start = input.LT(1);
        int block_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken COLON356 = null;
        IToken DECLARE357 = null;
        IToken BEGIN359 = null;
        IToken END361 = null;
        IToken SEMICOLON363 = null;
        Ada95Parser.statement_identifier_return statement_identifier355 = default(Ada95Parser.statement_identifier_return);

        Ada95Parser.declarative_part_return declarative_part358 = default(Ada95Parser.declarative_part_return);

        Ada95Parser.sequence_of_statements_return sequence_of_statements360 = default(Ada95Parser.sequence_of_statements_return);

        Ada95Parser.statement_identifier_return statement_identifier362 = default(Ada95Parser.statement_identifier_return);


        CommonTree COLON356_tree=null;
        CommonTree DECLARE357_tree=null;
        CommonTree BEGIN359_tree=null;
        CommonTree END361_tree=null;
        CommonTree SEMICOLON363_tree=null;
        RewriteRuleTokenStream stream_COLON = new RewriteRuleTokenStream(adaptor,"token COLON");
        RewriteRuleTokenStream stream_DECLARE = new RewriteRuleTokenStream(adaptor,"token DECLARE");
        RewriteRuleTokenStream stream_SEMICOLON = new RewriteRuleTokenStream(adaptor,"token SEMICOLON");
        RewriteRuleTokenStream stream_END = new RewriteRuleTokenStream(adaptor,"token END");
        RewriteRuleTokenStream stream_BEGIN = new RewriteRuleTokenStream(adaptor,"token BEGIN");
        RewriteRuleSubtreeStream stream_declarative_part = new RewriteRuleSubtreeStream(adaptor,"rule declarative_part");
        RewriteRuleSubtreeStream stream_statement_identifier = new RewriteRuleSubtreeStream(adaptor,"rule statement_identifier");
        RewriteRuleSubtreeStream stream_sequence_of_statements = new RewriteRuleSubtreeStream(adaptor,"rule sequence_of_statements");

        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Functions =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Procedures =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Objects =  new SortedSet< string >();
        	
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 95) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:690:2: ( ( statement_identifier COLON )? ( DECLARE declarative_part )? BEGIN sequence_of_statements END ( statement_identifier )? SEMICOLON -> ^( BLOCK ( statement_identifier )? ( declarative_part )? sequence_of_statements ( statement_identifier )? ) )
            // Ada95.g:691:3: ( statement_identifier COLON )? ( DECLARE declarative_part )? BEGIN sequence_of_statements END ( statement_identifier )? SEMICOLON
            {
            	// Ada95.g:691:3: ( statement_identifier COLON )?
            	int alt79 = 2;
            	int LA79_0 = input.LA(1);

            	if ( (LA79_0 == IDENTIFIER) )
            	{
            	    alt79 = 1;
            	}
            	switch (alt79) 
            	{
            	    case 1 :
            	        // Ada95.g:691:5: statement_identifier COLON
            	        {
            	        	PushFollow(FOLLOW_statement_identifier_in_block_statement3546);
            	        	statement_identifier355 = statement_identifier();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_statement_identifier.Add(statement_identifier355.Tree);
            	        	COLON356=(IToken)Match(input,COLON,FOLLOW_COLON_in_block_statement3548); if (state.failed) return retval; 
            	        	if ( (state.backtracking==0) ) stream_COLON.Add(COLON356);


            	        }
            	        break;

            	}

            	// Ada95.g:691:35: ( DECLARE declarative_part )?
            	int alt80 = 2;
            	int LA80_0 = input.LA(1);

            	if ( (LA80_0 == DECLARE) )
            	{
            	    alt80 = 1;
            	}
            	switch (alt80) 
            	{
            	    case 1 :
            	        // Ada95.g:691:37: DECLARE declarative_part
            	        {
            	        	DECLARE357=(IToken)Match(input,DECLARE,FOLLOW_DECLARE_in_block_statement3555); if (state.failed) return retval; 
            	        	if ( (state.backtracking==0) ) stream_DECLARE.Add(DECLARE357);

            	        	PushFollow(FOLLOW_declarative_part_in_block_statement3557);
            	        	declarative_part358 = declarative_part();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_declarative_part.Add(declarative_part358.Tree);

            	        }
            	        break;

            	}

            	BEGIN359=(IToken)Match(input,BEGIN,FOLLOW_BEGIN_in_block_statement3562); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_BEGIN.Add(BEGIN359);

            	PushFollow(FOLLOW_sequence_of_statements_in_block_statement3564);
            	sequence_of_statements360 = sequence_of_statements();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_sequence_of_statements.Add(sequence_of_statements360.Tree);
            	END361=(IToken)Match(input,END,FOLLOW_END_in_block_statement3566); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_END.Add(END361);

            	// Ada95.g:691:98: ( statement_identifier )?
            	int alt81 = 2;
            	int LA81_0 = input.LA(1);

            	if ( (LA81_0 == IDENTIFIER) )
            	{
            	    alt81 = 1;
            	}
            	switch (alt81) 
            	{
            	    case 1 :
            	        // Ada95.g:691:98: statement_identifier
            	        {
            	        	PushFollow(FOLLOW_statement_identifier_in_block_statement3568);
            	        	statement_identifier362 = statement_identifier();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_statement_identifier.Add(statement_identifier362.Tree);

            	        }
            	        break;

            	}

            	SEMICOLON363=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_block_statement3571); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_SEMICOLON.Add(SEMICOLON363);



            	// AST REWRITE
            	// elements:          statement_identifier, declarative_part, sequence_of_statements, statement_identifier
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 692:4: -> ^( BLOCK ( statement_identifier )? ( declarative_part )? sequence_of_statements ( statement_identifier )? )
            	{
            	    // Ada95.g:692:7: ^( BLOCK ( statement_identifier )? ( declarative_part )? sequence_of_statements ( statement_identifier )? )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(BLOCK, "BLOCK"), root_1);

            	    // Ada95.g:692:16: ( statement_identifier )?
            	    if ( stream_statement_identifier.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_statement_identifier.NextTree());

            	    }
            	    stream_statement_identifier.Reset();
            	    // Ada95.g:692:38: ( declarative_part )?
            	    if ( stream_declarative_part.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_declarative_part.NextTree());

            	    }
            	    stream_declarative_part.Reset();
            	    adaptor.AddChild(root_1, stream_sequence_of_statements.NextTree());
            	    // Ada95.g:692:79: ( statement_identifier )?
            	    if ( stream_statement_identifier.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_statement_identifier.NextTree());

            	    }
            	    stream_statement_identifier.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 95, block_statement_StartIndex); 
            }
            SymbolTable_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "block_statement"

    public class exit_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "exit_statement"
    // Ada95.g:695:1: exit_statement : EXIT ( statement_identifier )? ( WHEN condition )? SEMICOLON ;
    public Ada95Parser.exit_statement_return exit_statement() // throws RecognitionException [1]
    {   
        Ada95Parser.exit_statement_return retval = new Ada95Parser.exit_statement_return();
        retval.Start = input.LT(1);
        int exit_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken EXIT364 = null;
        IToken WHEN366 = null;
        IToken SEMICOLON368 = null;
        Ada95Parser.statement_identifier_return statement_identifier365 = default(Ada95Parser.statement_identifier_return);

        Ada95Parser.condition_return condition367 = default(Ada95Parser.condition_return);


        CommonTree EXIT364_tree=null;
        CommonTree WHEN366_tree=null;
        CommonTree SEMICOLON368_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 96) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:696:2: ( EXIT ( statement_identifier )? ( WHEN condition )? SEMICOLON )
            // Ada95.g:696:4: EXIT ( statement_identifier )? ( WHEN condition )? SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	EXIT364=(IToken)Match(input,EXIT,FOLLOW_EXIT_in_exit_statement3604); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{EXIT364_tree = (CommonTree)adaptor.Create(EXIT364);
            		root_0 = (CommonTree)adaptor.BecomeRoot(EXIT364_tree, root_0);
            	}
            	// Ada95.g:696:10: ( statement_identifier )?
            	int alt82 = 2;
            	int LA82_0 = input.LA(1);

            	if ( (LA82_0 == IDENTIFIER) )
            	{
            	    alt82 = 1;
            	}
            	switch (alt82) 
            	{
            	    case 1 :
            	        // Ada95.g:696:10: statement_identifier
            	        {
            	        	PushFollow(FOLLOW_statement_identifier_in_exit_statement3607);
            	        	statement_identifier365 = statement_identifier();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, statement_identifier365.Tree);

            	        }
            	        break;

            	}

            	// Ada95.g:696:32: ( WHEN condition )?
            	int alt83 = 2;
            	int LA83_0 = input.LA(1);

            	if ( (LA83_0 == WHEN) )
            	{
            	    alt83 = 1;
            	}
            	switch (alt83) 
            	{
            	    case 1 :
            	        // Ada95.g:696:34: WHEN condition
            	        {
            	        	WHEN366=(IToken)Match(input,WHEN,FOLLOW_WHEN_in_exit_statement3612); if (state.failed) return retval;
            	        	PushFollow(FOLLOW_condition_in_exit_statement3615);
            	        	condition367 = condition();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, condition367.Tree);

            	        }
            	        break;

            	}

            	SEMICOLON368=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_exit_statement3620); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 96, exit_statement_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "exit_statement"

    public class goto_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "goto_statement"
    // Ada95.g:698:1: goto_statement : GOTO statement_identifier SEMICOLON ;
    public Ada95Parser.goto_statement_return goto_statement() // throws RecognitionException [1]
    {   
        Ada95Parser.goto_statement_return retval = new Ada95Parser.goto_statement_return();
        retval.Start = input.LT(1);
        int goto_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken GOTO369 = null;
        IToken SEMICOLON371 = null;
        Ada95Parser.statement_identifier_return statement_identifier370 = default(Ada95Parser.statement_identifier_return);


        CommonTree GOTO369_tree=null;
        CommonTree SEMICOLON371_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 97) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:699:2: ( GOTO statement_identifier SEMICOLON )
            // Ada95.g:699:4: GOTO statement_identifier SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	GOTO369=(IToken)Match(input,GOTO,FOLLOW_GOTO_in_goto_statement3631); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{GOTO369_tree = (CommonTree)adaptor.Create(GOTO369);
            		root_0 = (CommonTree)adaptor.BecomeRoot(GOTO369_tree, root_0);
            	}
            	PushFollow(FOLLOW_statement_identifier_in_goto_statement3634);
            	statement_identifier370 = statement_identifier();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, statement_identifier370.Tree);
            	SEMICOLON371=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_goto_statement3636); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 97, goto_statement_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "goto_statement"

    public class subprogram_declaration_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "subprogram_declaration"
    // Ada95.g:701:1: subprogram_declaration : subprogram_specification[ false ] ( IS ABSTRACT )? SEMICOLON ;
    public Ada95Parser.subprogram_declaration_return subprogram_declaration() // throws RecognitionException [1]
    {   
        Ada95Parser.subprogram_declaration_return retval = new Ada95Parser.subprogram_declaration_return();
        retval.Start = input.LT(1);
        int subprogram_declaration_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IS373 = null;
        IToken ABSTRACT374 = null;
        IToken SEMICOLON375 = null;
        Ada95Parser.subprogram_specification_return subprogram_specification372 = default(Ada95Parser.subprogram_specification_return);


        CommonTree IS373_tree=null;
        CommonTree ABSTRACT374_tree=null;
        CommonTree SEMICOLON375_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 98) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:702:2: ( subprogram_specification[ false ] ( IS ABSTRACT )? SEMICOLON )
            // Ada95.g:702:4: subprogram_specification[ false ] ( IS ABSTRACT )? SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_subprogram_specification_in_subprogram_declaration3647);
            	subprogram_specification372 = subprogram_specification(false);
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subprogram_specification372.Tree);
            	// Ada95.g:702:38: ( IS ABSTRACT )?
            	int alt84 = 2;
            	int LA84_0 = input.LA(1);

            	if ( (LA84_0 == IS) )
            	{
            	    alt84 = 1;
            	}
            	switch (alt84) 
            	{
            	    case 1 :
            	        // Ada95.g:702:40: IS ABSTRACT
            	        {
            	        	IS373=(IToken)Match(input,IS,FOLLOW_IS_in_subprogram_declaration3652); if (state.failed) return retval;
            	        	ABSTRACT374=(IToken)Match(input,ABSTRACT,FOLLOW_ABSTRACT_in_subprogram_declaration3655); if (state.failed) return retval;
            	        	if ( state.backtracking == 0 )
            	        	{ABSTRACT374_tree = (CommonTree)adaptor.Create(ABSTRACT374);
            	        		adaptor.AddChild(root_0, ABSTRACT374_tree);
            	        	}

            	        }
            	        break;

            	}

            	SEMICOLON375=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_subprogram_declaration3660); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 98, subprogram_declaration_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "subprogram_declaration"

    public class subprogram_specification_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "subprogram_specification"
    // Ada95.g:704:1: subprogram_specification[ bool withBody ] : ( PROCEDURE procID= defining_program_unit_name parameter_profile[ withBody ] | FUNCTION funcID= defining_designator parameter_and_result_profile[ withBody ] );
    public Ada95Parser.subprogram_specification_return subprogram_specification(bool withBody) // throws RecognitionException [1]
    {   
        Ada95Parser.subprogram_specification_return retval = new Ada95Parser.subprogram_specification_return();
        retval.Start = input.LT(1);
        int subprogram_specification_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken PROCEDURE376 = null;
        IToken FUNCTION378 = null;
        Ada95Parser.defining_program_unit_name_return procID = default(Ada95Parser.defining_program_unit_name_return);

        Ada95Parser.defining_designator_return funcID = default(Ada95Parser.defining_designator_return);

        Ada95Parser.parameter_profile_return parameter_profile377 = default(Ada95Parser.parameter_profile_return);

        Ada95Parser.parameter_and_result_profile_return parameter_and_result_profile379 = default(Ada95Parser.parameter_and_result_profile_return);


        CommonTree PROCEDURE376_tree=null;
        CommonTree FUNCTION378_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 99) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:705:2: ( PROCEDURE procID= defining_program_unit_name parameter_profile[ withBody ] | FUNCTION funcID= defining_designator parameter_and_result_profile[ withBody ] )
            int alt85 = 2;
            int LA85_0 = input.LA(1);

            if ( (LA85_0 == PROCEDURE) )
            {
                alt85 = 1;
            }
            else if ( (LA85_0 == FUNCTION) )
            {
                alt85 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d85s0 =
                    new NoViableAltException("", 85, 0, input);

                throw nvae_d85s0;
            }
            switch (alt85) 
            {
                case 1 :
                    // Ada95.g:706:3: PROCEDURE procID= defining_program_unit_name parameter_profile[ withBody ]
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PROCEDURE376=(IToken)Match(input,PROCEDURE,FOLLOW_PROCEDURE_in_subprogram_specification3674); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{PROCEDURE376_tree = (CommonTree)adaptor.Create(PROCEDURE376);
                    		root_0 = (CommonTree)adaptor.BecomeRoot(PROCEDURE376_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_defining_program_unit_name_in_subprogram_specification3681);
                    	procID = defining_program_unit_name();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, procID.Tree);
                    	PushFollow(FOLLOW_parameter_profile_in_subprogram_specification3684);
                    	parameter_profile377 = parameter_profile(withBody);
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, parameter_profile377.Tree);
                    	if ( (state.backtracking==0) )
                    	{

                    	  			int prev = SymbolTable_stack.Count - ( withBody ? 2 : 1 );
                    	  			((SymbolTable_scope)SymbolTable_stack[ prev ]).Procedures.Add( ((procID != null) ? input.ToString((IToken)(procID.Start),(IToken)(procID.Stop)) : null) );
                    	  		
                    	}

                    }
                    break;
                case 2 :
                    // Ada95.g:711:3: FUNCTION funcID= defining_designator parameter_and_result_profile[ withBody ]
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	FUNCTION378=(IToken)Match(input,FUNCTION,FOLLOW_FUNCTION_in_subprogram_specification3695); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{FUNCTION378_tree = (CommonTree)adaptor.Create(FUNCTION378);
                    		root_0 = (CommonTree)adaptor.BecomeRoot(FUNCTION378_tree, root_0);
                    	}
                    	PushFollow(FOLLOW_defining_designator_in_subprogram_specification3702);
                    	funcID = defining_designator();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, funcID.Tree);
                    	PushFollow(FOLLOW_parameter_and_result_profile_in_subprogram_specification3704);
                    	parameter_and_result_profile379 = parameter_and_result_profile(withBody);
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, parameter_and_result_profile379.Tree);
                    	if ( (state.backtracking==0) )
                    	{

                    	  			int prev = SymbolTable_stack.Count - ( withBody ? 2 : 1 );
                    	  			((SymbolTable_scope)SymbolTable_stack[ prev ]).Functions.Add( ((funcID != null) ? input.ToString((IToken)(funcID.Start),(IToken)(funcID.Stop)) : null) );
                    	  		
                    	}

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 99, subprogram_specification_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "subprogram_specification"

    public class designator_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "designator"
    // Ada95.g:718:1: designator : ( IDENTIFIER | operator_symbol );
    public Ada95Parser.designator_return designator() // throws RecognitionException [1]
    {   
        Ada95Parser.designator_return retval = new Ada95Parser.designator_return();
        retval.Start = input.LT(1);
        int designator_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IDENTIFIER380 = null;
        Ada95Parser.operator_symbol_return operator_symbol381 = default(Ada95Parser.operator_symbol_return);


        CommonTree IDENTIFIER380_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 100) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:719:2: ( IDENTIFIER | operator_symbol )
            int alt86 = 2;
            int LA86_0 = input.LA(1);

            if ( (LA86_0 == IDENTIFIER) )
            {
                alt86 = 1;
            }
            else if ( (LA86_0 == STRING_LITERAL) )
            {
                alt86 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d86s0 =
                    new NoViableAltException("", 86, 0, input);

                throw nvae_d86s0;
            }
            switch (alt86) 
            {
                case 1 :
                    // Ada95.g:720:3: IDENTIFIER
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	IDENTIFIER380=(IToken)Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_designator3722); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{IDENTIFIER380_tree = (CommonTree)adaptor.Create(IDENTIFIER380);
                    		adaptor.AddChild(root_0, IDENTIFIER380_tree);
                    	}

                    }
                    break;
                case 2 :
                    // Ada95.g:721:3: operator_symbol
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_operator_symbol_in_designator3728);
                    	operator_symbol381 = operator_symbol();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, operator_symbol381.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 100, designator_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "designator"

    public class defining_designator_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "defining_designator"
    // Ada95.g:724:1: defining_designator : ( defining_program_unit_name | defining_operator_symbol );
    public Ada95Parser.defining_designator_return defining_designator() // throws RecognitionException [1]
    {   
        Ada95Parser.defining_designator_return retval = new Ada95Parser.defining_designator_return();
        retval.Start = input.LT(1);
        int defining_designator_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.defining_program_unit_name_return defining_program_unit_name382 = default(Ada95Parser.defining_program_unit_name_return);

        Ada95Parser.defining_operator_symbol_return defining_operator_symbol383 = default(Ada95Parser.defining_operator_symbol_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 101) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:725:2: ( defining_program_unit_name | defining_operator_symbol )
            int alt87 = 2;
            int LA87_0 = input.LA(1);

            if ( (LA87_0 == IDENTIFIER) )
            {
                alt87 = 1;
            }
            else if ( (LA87_0 == STRING_LITERAL) )
            {
                alt87 = 2;
            }
            else 
            {
                if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                NoViableAltException nvae_d87s0 =
                    new NoViableAltException("", 87, 0, input);

                throw nvae_d87s0;
            }
            switch (alt87) 
            {
                case 1 :
                    // Ada95.g:726:3: defining_program_unit_name
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_defining_program_unit_name_in_defining_designator3741);
                    	defining_program_unit_name382 = defining_program_unit_name();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, defining_program_unit_name382.Tree);

                    }
                    break;
                case 2 :
                    // Ada95.g:727:3: defining_operator_symbol
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_defining_operator_symbol_in_defining_designator3747);
                    	defining_operator_symbol383 = defining_operator_symbol();
                    	state.followingStackPointer--;
                    	if (state.failed) return retval;
                    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, defining_operator_symbol383.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 101, defining_designator_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "defining_designator"

    public class defining_program_unit_name_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "defining_program_unit_name"
    // Ada95.g:730:1: defining_program_unit_name : defining_identifier ;
    public Ada95Parser.defining_program_unit_name_return defining_program_unit_name() // throws RecognitionException [1]
    {   
        Ada95Parser.defining_program_unit_name_return retval = new Ada95Parser.defining_program_unit_name_return();
        retval.Start = input.LT(1);
        int defining_program_unit_name_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.defining_identifier_return defining_identifier384 = default(Ada95Parser.defining_identifier_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 102) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:731:2: ( defining_identifier )
            // Ada95.g:731:4: defining_identifier
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_defining_identifier_in_defining_program_unit_name3758);
            	defining_identifier384 = defining_identifier();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, defining_identifier384.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 102, defining_program_unit_name_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "defining_program_unit_name"

    public class operator_symbol_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "operator_symbol"
    // Ada95.g:733:1: operator_symbol : STRING_LITERAL ;
    public Ada95Parser.operator_symbol_return operator_symbol() // throws RecognitionException [1]
    {   
        Ada95Parser.operator_symbol_return retval = new Ada95Parser.operator_symbol_return();
        retval.Start = input.LT(1);
        int operator_symbol_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken STRING_LITERAL385 = null;

        CommonTree STRING_LITERAL385_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 103) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:734:2: ( STRING_LITERAL )
            // Ada95.g:734:4: STRING_LITERAL
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	STRING_LITERAL385=(IToken)Match(input,STRING_LITERAL,FOLLOW_STRING_LITERAL_in_operator_symbol3768); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{STRING_LITERAL385_tree = (CommonTree)adaptor.Create(STRING_LITERAL385);
            		adaptor.AddChild(root_0, STRING_LITERAL385_tree);
            	}

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 103, operator_symbol_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "operator_symbol"

    public class defining_operator_symbol_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "defining_operator_symbol"
    // Ada95.g:736:1: defining_operator_symbol : operator_symbol ;
    public Ada95Parser.defining_operator_symbol_return defining_operator_symbol() // throws RecognitionException [1]
    {   
        Ada95Parser.defining_operator_symbol_return retval = new Ada95Parser.defining_operator_symbol_return();
        retval.Start = input.LT(1);
        int defining_operator_symbol_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.operator_symbol_return operator_symbol386 = default(Ada95Parser.operator_symbol_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 104) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:737:2: ( operator_symbol )
            // Ada95.g:737:4: operator_symbol
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_operator_symbol_in_defining_operator_symbol3778);
            	operator_symbol386 = operator_symbol();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, operator_symbol386.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 104, defining_operator_symbol_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "defining_operator_symbol"

    public class parameter_profile_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "parameter_profile"
    // Ada95.g:739:1: parameter_profile[ bool withBody ] : ( formal_part[ withBody ] )? ;
    public Ada95Parser.parameter_profile_return parameter_profile(bool withBody) // throws RecognitionException [1]
    {   
        Ada95Parser.parameter_profile_return retval = new Ada95Parser.parameter_profile_return();
        retval.Start = input.LT(1);
        int parameter_profile_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.formal_part_return formal_part387 = default(Ada95Parser.formal_part_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 105) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:740:2: ( ( formal_part[ withBody ] )? )
            // Ada95.g:740:4: ( formal_part[ withBody ] )?
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	// Ada95.g:740:4: ( formal_part[ withBody ] )?
            	int alt88 = 2;
            	int LA88_0 = input.LA(1);

            	if ( (LA88_0 == LPARANTHESIS) )
            	{
            	    alt88 = 1;
            	}
            	switch (alt88) 
            	{
            	    case 1 :
            	        // Ada95.g:740:4: formal_part[ withBody ]
            	        {
            	        	PushFollow(FOLLOW_formal_part_in_parameter_profile3789);
            	        	formal_part387 = formal_part(withBody);
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, formal_part387.Tree);

            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 105, parameter_profile_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "parameter_profile"

    public class parameter_and_result_profile_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "parameter_and_result_profile"
    // Ada95.g:742:1: parameter_and_result_profile[ bool withBody ] : ( formal_part[ withBody ] )? RETURN subtype_mark ;
    public Ada95Parser.parameter_and_result_profile_return parameter_and_result_profile(bool withBody) // throws RecognitionException [1]
    {   
        Ada95Parser.parameter_and_result_profile_return retval = new Ada95Parser.parameter_and_result_profile_return();
        retval.Start = input.LT(1);
        int parameter_and_result_profile_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken RETURN389 = null;
        Ada95Parser.formal_part_return formal_part388 = default(Ada95Parser.formal_part_return);

        Ada95Parser.subtype_mark_return subtype_mark390 = default(Ada95Parser.subtype_mark_return);


        CommonTree RETURN389_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 106) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:743:2: ( ( formal_part[ withBody ] )? RETURN subtype_mark )
            // Ada95.g:743:4: ( formal_part[ withBody ] )? RETURN subtype_mark
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	// Ada95.g:743:4: ( formal_part[ withBody ] )?
            	int alt89 = 2;
            	int LA89_0 = input.LA(1);

            	if ( (LA89_0 == LPARANTHESIS) )
            	{
            	    alt89 = 1;
            	}
            	switch (alt89) 
            	{
            	    case 1 :
            	        // Ada95.g:743:4: formal_part[ withBody ]
            	        {
            	        	PushFollow(FOLLOW_formal_part_in_parameter_and_result_profile3802);
            	        	formal_part388 = formal_part(withBody);
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, formal_part388.Tree);

            	        }
            	        break;

            	}

            	RETURN389=(IToken)Match(input,RETURN,FOLLOW_RETURN_in_parameter_and_result_profile3806); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{RETURN389_tree = (CommonTree)adaptor.Create(RETURN389);
            		adaptor.AddChild(root_0, RETURN389_tree);
            	}
            	PushFollow(FOLLOW_subtype_mark_in_parameter_and_result_profile3808);
            	subtype_mark390 = subtype_mark();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subtype_mark390.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 106, parameter_and_result_profile_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "parameter_and_result_profile"

    public class formal_part_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "formal_part"
    // Ada95.g:745:1: formal_part[ bool withBody ] : LPARANTHESIS parameter_specification[ withBody ] ( SEMICOLON parameter_specification[ withBody ] )* RPARANTHESIS ;
    public Ada95Parser.formal_part_return formal_part(bool withBody) // throws RecognitionException [1]
    {   
        Ada95Parser.formal_part_return retval = new Ada95Parser.formal_part_return();
        retval.Start = input.LT(1);
        int formal_part_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken LPARANTHESIS391 = null;
        IToken SEMICOLON393 = null;
        IToken RPARANTHESIS395 = null;
        Ada95Parser.parameter_specification_return parameter_specification392 = default(Ada95Parser.parameter_specification_return);

        Ada95Parser.parameter_specification_return parameter_specification394 = default(Ada95Parser.parameter_specification_return);


        CommonTree LPARANTHESIS391_tree=null;
        CommonTree SEMICOLON393_tree=null;
        CommonTree RPARANTHESIS395_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 107) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:746:2: ( LPARANTHESIS parameter_specification[ withBody ] ( SEMICOLON parameter_specification[ withBody ] )* RPARANTHESIS )
            // Ada95.g:746:4: LPARANTHESIS parameter_specification[ withBody ] ( SEMICOLON parameter_specification[ withBody ] )* RPARANTHESIS
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	LPARANTHESIS391=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_formal_part3819); if (state.failed) return retval;
            	PushFollow(FOLLOW_parameter_specification_in_formal_part3822);
            	parameter_specification392 = parameter_specification(withBody);
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, parameter_specification392.Tree);
            	// Ada95.g:746:54: ( SEMICOLON parameter_specification[ withBody ] )*
            	do 
            	{
            	    int alt90 = 2;
            	    int LA90_0 = input.LA(1);

            	    if ( (LA90_0 == SEMICOLON) )
            	    {
            	        alt90 = 1;
            	    }


            	    switch (alt90) 
            		{
            			case 1 :
            			    // Ada95.g:746:56: SEMICOLON parameter_specification[ withBody ]
            			    {
            			    	SEMICOLON393=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_formal_part3827); if (state.failed) return retval;
            			    	PushFollow(FOLLOW_parameter_specification_in_formal_part3830);
            			    	parameter_specification394 = parameter_specification(withBody);
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, parameter_specification394.Tree);

            			    }
            			    break;

            			default:
            			    goto loop90;
            	    }
            	} while (true);

            	loop90:
            		;	// Stops C# compiler whining that label 'loop90' has no statements

            	RPARANTHESIS395=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_formal_part3836); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 107, formal_part_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "formal_part"

    public class parameter_specification_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "parameter_specification"
    // Ada95.g:748:1: parameter_specification[ bool withBody ] : defining_identifier_list COLON ( mode subtype_mark ) ( ASSIGN default_expression )? -> ( ^( PARAMETER defining_identifier_list mode subtype_mark ( default_expression )? ) )+ ;
    public Ada95Parser.parameter_specification_return parameter_specification(bool withBody) // throws RecognitionException [1]
    {   
        Symbols_stack.Push(new Symbols_scope());

        Ada95Parser.parameter_specification_return retval = new Ada95Parser.parameter_specification_return();
        retval.Start = input.LT(1);
        int parameter_specification_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken COLON397 = null;
        IToken ASSIGN400 = null;
        Ada95Parser.defining_identifier_list_return defining_identifier_list396 = default(Ada95Parser.defining_identifier_list_return);

        Ada95Parser.mode_return mode398 = default(Ada95Parser.mode_return);

        Ada95Parser.subtype_mark_return subtype_mark399 = default(Ada95Parser.subtype_mark_return);

        Ada95Parser.default_expression_return default_expression401 = default(Ada95Parser.default_expression_return);


        CommonTree COLON397_tree=null;
        CommonTree ASSIGN400_tree=null;
        RewriteRuleTokenStream stream_COLON = new RewriteRuleTokenStream(adaptor,"token COLON");
        RewriteRuleTokenStream stream_ASSIGN = new RewriteRuleTokenStream(adaptor,"token ASSIGN");
        RewriteRuleSubtreeStream stream_default_expression = new RewriteRuleSubtreeStream(adaptor,"rule default_expression");
        RewriteRuleSubtreeStream stream_defining_identifier_list = new RewriteRuleSubtreeStream(adaptor,"rule defining_identifier_list");
        RewriteRuleSubtreeStream stream_subtype_mark = new RewriteRuleSubtreeStream(adaptor,"rule subtype_mark");
        RewriteRuleSubtreeStream stream_mode = new RewriteRuleSubtreeStream(adaptor,"rule mode");

        		((Symbols_scope)Symbols_stack.Peek()).IDs =  new SortedSet< string >();
        	
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 108) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:754:2: ( defining_identifier_list COLON ( mode subtype_mark ) ( ASSIGN default_expression )? -> ( ^( PARAMETER defining_identifier_list mode subtype_mark ( default_expression )? ) )+ )
            // Ada95.g:755:3: defining_identifier_list COLON ( mode subtype_mark ) ( ASSIGN default_expression )?
            {
            	PushFollow(FOLLOW_defining_identifier_list_in_parameter_specification3863);
            	defining_identifier_list396 = defining_identifier_list();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_defining_identifier_list.Add(defining_identifier_list396.Tree);
            	COLON397=(IToken)Match(input,COLON,FOLLOW_COLON_in_parameter_specification3865); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_COLON.Add(COLON397);

            	// Ada95.g:755:34: ( mode subtype_mark )
            	// Ada95.g:755:36: mode subtype_mark
            	{
            		PushFollow(FOLLOW_mode_in_parameter_specification3869);
            		mode398 = mode();
            		state.followingStackPointer--;
            		if (state.failed) return retval;
            		if ( (state.backtracking==0) ) stream_mode.Add(mode398.Tree);
            		PushFollow(FOLLOW_subtype_mark_in_parameter_specification3871);
            		subtype_mark399 = subtype_mark();
            		state.followingStackPointer--;
            		if (state.failed) return retval;
            		if ( (state.backtracking==0) ) stream_subtype_mark.Add(subtype_mark399.Tree);

            	}

            	// Ada95.g:755:80: ( ASSIGN default_expression )?
            	int alt91 = 2;
            	int LA91_0 = input.LA(1);

            	if ( (LA91_0 == ASSIGN) )
            	{
            	    alt91 = 1;
            	}
            	switch (alt91) 
            	{
            	    case 1 :
            	        // Ada95.g:755:82: ASSIGN default_expression
            	        {
            	        	ASSIGN400=(IToken)Match(input,ASSIGN,FOLLOW_ASSIGN_in_parameter_specification3879); if (state.failed) return retval; 
            	        	if ( (state.backtracking==0) ) stream_ASSIGN.Add(ASSIGN400);

            	        	PushFollow(FOLLOW_default_expression_in_parameter_specification3881);
            	        	default_expression401 = default_expression();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_default_expression.Add(default_expression401.Tree);

            	        }
            	        break;

            	}

            	if ( (state.backtracking==0) )
            	{
            	   if ( withBody ) ((SymbolTable_scope)SymbolTable_stack.Peek()).Objects.UnionWith( ((Symbols_scope)Symbols_stack.Peek()).IDs ); 
            	}


            	// AST REWRITE
            	// elements:          mode, subtype_mark, defining_identifier_list, default_expression
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 757:4: -> ( ^( PARAMETER defining_identifier_list mode subtype_mark ( default_expression )? ) )+
            	{
            	    if ( !(stream_mode.HasNext() || stream_subtype_mark.HasNext() || stream_defining_identifier_list.HasNext()) ) {
            	        throw new RewriteEarlyExitException();
            	    }
            	    while ( stream_mode.HasNext() || stream_subtype_mark.HasNext() || stream_defining_identifier_list.HasNext() )
            	    {
            	        // Ada95.g:757:7: ^( PARAMETER defining_identifier_list mode subtype_mark ( default_expression )? )
            	        {
            	        CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	        root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(PARAMETER, "PARAMETER"), root_1);

            	        adaptor.AddChild(root_1, stream_defining_identifier_list.NextTree());
            	        adaptor.AddChild(root_1, stream_mode.NextTree());
            	        adaptor.AddChild(root_1, stream_subtype_mark.NextTree());
            	        // Ada95.g:757:63: ( default_expression )?
            	        if ( stream_default_expression.HasNext() )
            	        {
            	            adaptor.AddChild(root_1, stream_default_expression.NextTree());

            	        }
            	        stream_default_expression.Reset();

            	        adaptor.AddChild(root_0, root_1);
            	        }

            	    }
            	    stream_mode.Reset();
            	    stream_subtype_mark.Reset();
            	    stream_defining_identifier_list.Reset();

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 108, parameter_specification_StartIndex); 
            }
            Symbols_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "parameter_specification"

    public class mode_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "mode"
    // Ada95.g:760:1: mode : ( -> IN | IN | IN OUT -> REF | OUT );
    public Ada95Parser.mode_return mode() // throws RecognitionException [1]
    {   
        Ada95Parser.mode_return retval = new Ada95Parser.mode_return();
        retval.Start = input.LT(1);
        int mode_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IN402 = null;
        IToken IN403 = null;
        IToken OUT404 = null;
        IToken OUT405 = null;

        CommonTree IN402_tree=null;
        CommonTree IN403_tree=null;
        CommonTree OUT404_tree=null;
        CommonTree OUT405_tree=null;
        RewriteRuleTokenStream stream_IN = new RewriteRuleTokenStream(adaptor,"token IN");
        RewriteRuleTokenStream stream_OUT = new RewriteRuleTokenStream(adaptor,"token OUT");

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 109) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:761:2: ( -> IN | IN | IN OUT -> REF | OUT )
            int alt92 = 4;
            switch ( input.LA(1) ) 
            {
            case IDENTIFIER:
            	{
                alt92 = 1;
                }
                break;
            case IN:
            	{
                int LA92_2 = input.LA(2);

                if ( (LA92_2 == OUT) )
                {
                    alt92 = 3;
                }
                else if ( (LA92_2 == IDENTIFIER) )
                {
                    alt92 = 2;
                }
                else 
                {
                    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
                    NoViableAltException nvae_d92s2 =
                        new NoViableAltException("", 92, 2, input);

                    throw nvae_d92s2;
                }
                }
                break;
            case OUT:
            	{
                alt92 = 4;
                }
                break;
            	default:
            	    if ( state.backtracking > 0 ) {state.failed = true; return retval;}
            	    NoViableAltException nvae_d92s0 =
            	        new NoViableAltException("", 92, 0, input);

            	    throw nvae_d92s0;
            }

            switch (alt92) 
            {
                case 1 :
                    // Ada95.g:762:13: 
                    {

                    	// AST REWRITE
                    	// elements:          
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (CommonTree)adaptor.GetNilNode();
                    	// 762:13: -> IN
                    	{
                    	    adaptor.AddChild(root_0, (CommonTree)adaptor.Create(IN, "IN"));

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;
                case 2 :
                    // Ada95.g:763:3: IN
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	IN402=(IToken)Match(input,IN,FOLLOW_IN_in_mode3933); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{IN402_tree = (CommonTree)adaptor.Create(IN402);
                    		adaptor.AddChild(root_0, IN402_tree);
                    	}

                    }
                    break;
                case 3 :
                    // Ada95.g:764:3: IN OUT
                    {
                    	IN403=(IToken)Match(input,IN,FOLLOW_IN_in_mode3939); if (state.failed) return retval; 
                    	if ( (state.backtracking==0) ) stream_IN.Add(IN403);

                    	OUT404=(IToken)Match(input,OUT,FOLLOW_OUT_in_mode3941); if (state.failed) return retval; 
                    	if ( (state.backtracking==0) ) stream_OUT.Add(OUT404);



                    	// AST REWRITE
                    	// elements:          
                    	// token labels:      
                    	// rule labels:       retval
                    	// token list labels: 
                    	// rule list labels:  
                    	// wildcard labels: 
                    	if ( (state.backtracking==0) ) {
                    	retval.Tree = root_0;
                    	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

                    	root_0 = (CommonTree)adaptor.GetNilNode();
                    	// 764:11: -> REF
                    	{
                    	    adaptor.AddChild(root_0, (CommonTree)adaptor.Create(REF, "REF"));

                    	}

                    	retval.Tree = root_0;retval.Tree = root_0;}
                    }
                    break;
                case 4 :
                    // Ada95.g:765:3: OUT
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	OUT405=(IToken)Match(input,OUT,FOLLOW_OUT_in_mode3952); if (state.failed) return retval;
                    	if ( state.backtracking == 0 )
                    	{OUT405_tree = (CommonTree)adaptor.Create(OUT405);
                    		adaptor.AddChild(root_0, OUT405_tree);
                    	}

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 109, mode_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "mode"

    public class subprogram_body_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "subprogram_body"
    // Ada95.g:768:1: subprogram_body : subprogram_specification[ true ] IS declarative_part BEGIN handled_sequence_of_statements END ( designator )? SEMICOLON ;
    public Ada95Parser.subprogram_body_return subprogram_body() // throws RecognitionException [1]
    {   
        SymbolTable_stack.Push(new SymbolTable_scope());

        Ada95Parser.subprogram_body_return retval = new Ada95Parser.subprogram_body_return();
        retval.Start = input.LT(1);
        int subprogram_body_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken IS407 = null;
        IToken BEGIN409 = null;
        IToken END411 = null;
        IToken SEMICOLON413 = null;
        Ada95Parser.subprogram_specification_return subprogram_specification406 = default(Ada95Parser.subprogram_specification_return);

        Ada95Parser.declarative_part_return declarative_part408 = default(Ada95Parser.declarative_part_return);

        Ada95Parser.handled_sequence_of_statements_return handled_sequence_of_statements410 = default(Ada95Parser.handled_sequence_of_statements_return);

        Ada95Parser.designator_return designator412 = default(Ada95Parser.designator_return);


        CommonTree IS407_tree=null;
        CommonTree BEGIN409_tree=null;
        CommonTree END411_tree=null;
        CommonTree SEMICOLON413_tree=null;


        		((SymbolTable_scope)SymbolTable_stack.Peek()).Types =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Functions =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Procedures =  new SortedSet< string >();
        		((SymbolTable_scope)SymbolTable_stack.Peek()).Objects =  new SortedSet< string >();
        	
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 110) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:777:2: ( subprogram_specification[ true ] IS declarative_part BEGIN handled_sequence_of_statements END ( designator )? SEMICOLON )
            // Ada95.g:777:4: subprogram_specification[ true ] IS declarative_part BEGIN handled_sequence_of_statements END ( designator )? SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_subprogram_specification_in_subprogram_body3976);
            	subprogram_specification406 = subprogram_specification(true);
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, subprogram_specification406.Tree);
            	IS407=(IToken)Match(input,IS,FOLLOW_IS_in_subprogram_body3979); if (state.failed) return retval;
            	PushFollow(FOLLOW_declarative_part_in_subprogram_body3982);
            	declarative_part408 = declarative_part();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, declarative_part408.Tree);
            	BEGIN409=(IToken)Match(input,BEGIN,FOLLOW_BEGIN_in_subprogram_body3984); if (state.failed) return retval;
            	PushFollow(FOLLOW_handled_sequence_of_statements_in_subprogram_body3987);
            	handled_sequence_of_statements410 = handled_sequence_of_statements();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, handled_sequence_of_statements410.Tree);
            	END411=(IToken)Match(input,END,FOLLOW_END_in_subprogram_body3989); if (state.failed) return retval;
            	// Ada95.g:777:101: ( designator )?
            	int alt93 = 2;
            	int LA93_0 = input.LA(1);

            	if ( (LA93_0 == IDENTIFIER || LA93_0 == STRING_LITERAL) )
            	{
            	    alt93 = 1;
            	}
            	switch (alt93) 
            	{
            	    case 1 :
            	        // Ada95.g:777:101: designator
            	        {
            	        	PushFollow(FOLLOW_designator_in_subprogram_body3992);
            	        	designator412 = designator();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, designator412.Tree);

            	        }
            	        break;

            	}

            	SEMICOLON413=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_subprogram_body3995); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 110, subprogram_body_StartIndex); 
            }
            SymbolTable_stack.Pop();

        }
        return retval;
    }
    // $ANTLR end "subprogram_body"

    public class handled_sequence_of_statements_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "handled_sequence_of_statements"
    // Ada95.g:779:1: handled_sequence_of_statements : sequence_of_statements ;
    public Ada95Parser.handled_sequence_of_statements_return handled_sequence_of_statements() // throws RecognitionException [1]
    {   
        Ada95Parser.handled_sequence_of_statements_return retval = new Ada95Parser.handled_sequence_of_statements_return();
        retval.Start = input.LT(1);
        int handled_sequence_of_statements_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.sequence_of_statements_return sequence_of_statements414 = default(Ada95Parser.sequence_of_statements_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 111) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:780:2: ( sequence_of_statements )
            // Ada95.g:780:4: sequence_of_statements
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_sequence_of_statements_in_handled_sequence_of_statements4006);
            	sequence_of_statements414 = sequence_of_statements();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, sequence_of_statements414.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 111, handled_sequence_of_statements_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "handled_sequence_of_statements"

    public class procedure_call_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "procedure_call_statement"
    // Ada95.g:782:1: procedure_call_statement : direct_name ( actual_parameter_part )? SEMICOLON -> ^( PROCEDURE direct_name ( actual_parameter_part )? ) ;
    public Ada95Parser.procedure_call_statement_return procedure_call_statement() // throws RecognitionException [1]
    {   
        Ada95Parser.procedure_call_statement_return retval = new Ada95Parser.procedure_call_statement_return();
        retval.Start = input.LT(1);
        int procedure_call_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken SEMICOLON417 = null;
        Ada95Parser.direct_name_return direct_name415 = default(Ada95Parser.direct_name_return);

        Ada95Parser.actual_parameter_part_return actual_parameter_part416 = default(Ada95Parser.actual_parameter_part_return);


        CommonTree SEMICOLON417_tree=null;
        RewriteRuleTokenStream stream_SEMICOLON = new RewriteRuleTokenStream(adaptor,"token SEMICOLON");
        RewriteRuleSubtreeStream stream_actual_parameter_part = new RewriteRuleSubtreeStream(adaptor,"rule actual_parameter_part");
        RewriteRuleSubtreeStream stream_direct_name = new RewriteRuleSubtreeStream(adaptor,"rule direct_name");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 112) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:783:2: ( direct_name ( actual_parameter_part )? SEMICOLON -> ^( PROCEDURE direct_name ( actual_parameter_part )? ) )
            // Ada95.g:783:4: direct_name ( actual_parameter_part )? SEMICOLON
            {
            	PushFollow(FOLLOW_direct_name_in_procedure_call_statement4016);
            	direct_name415 = direct_name();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_direct_name.Add(direct_name415.Tree);
            	// Ada95.g:783:16: ( actual_parameter_part )?
            	int alt94 = 2;
            	int LA94_0 = input.LA(1);

            	if ( (LA94_0 == LPARANTHESIS) )
            	{
            	    alt94 = 1;
            	}
            	switch (alt94) 
            	{
            	    case 1 :
            	        // Ada95.g:783:16: actual_parameter_part
            	        {
            	        	PushFollow(FOLLOW_actual_parameter_part_in_procedure_call_statement4018);
            	        	actual_parameter_part416 = actual_parameter_part();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_actual_parameter_part.Add(actual_parameter_part416.Tree);

            	        }
            	        break;

            	}

            	SEMICOLON417=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_procedure_call_statement4021); if (state.failed) return retval; 
            	if ( (state.backtracking==0) ) stream_SEMICOLON.Add(SEMICOLON417);



            	// AST REWRITE
            	// elements:          direct_name, actual_parameter_part
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 783:49: -> ^( PROCEDURE direct_name ( actual_parameter_part )? )
            	{
            	    // Ada95.g:783:52: ^( PROCEDURE direct_name ( actual_parameter_part )? )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(PROCEDURE, "PROCEDURE"), root_1);

            	    adaptor.AddChild(root_1, stream_direct_name.NextTree());
            	    // Ada95.g:783:77: ( actual_parameter_part )?
            	    if ( stream_actual_parameter_part.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_actual_parameter_part.NextTree());

            	    }
            	    stream_actual_parameter_part.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 112, procedure_call_statement_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "procedure_call_statement"

    public class function_call_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "function_call"
    // Ada95.g:785:1: function_call : direct_name ( actual_parameter_part )? -> ^( FUNCTION direct_name ( actual_parameter_part )? ) ;
    public Ada95Parser.function_call_return function_call() // throws RecognitionException [1]
    {   
        Ada95Parser.function_call_return retval = new Ada95Parser.function_call_return();
        retval.Start = input.LT(1);
        int function_call_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.direct_name_return direct_name418 = default(Ada95Parser.direct_name_return);

        Ada95Parser.actual_parameter_part_return actual_parameter_part419 = default(Ada95Parser.actual_parameter_part_return);


        RewriteRuleSubtreeStream stream_actual_parameter_part = new RewriteRuleSubtreeStream(adaptor,"rule actual_parameter_part");
        RewriteRuleSubtreeStream stream_direct_name = new RewriteRuleSubtreeStream(adaptor,"rule direct_name");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 113) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:786:2: ( direct_name ( actual_parameter_part )? -> ^( FUNCTION direct_name ( actual_parameter_part )? ) )
            // Ada95.g:786:4: direct_name ( actual_parameter_part )?
            {
            	PushFollow(FOLLOW_direct_name_in_function_call4045);
            	direct_name418 = direct_name();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_direct_name.Add(direct_name418.Tree);
            	// Ada95.g:786:16: ( actual_parameter_part )?
            	int alt95 = 2;
            	int LA95_0 = input.LA(1);

            	if ( (LA95_0 == LPARANTHESIS) )
            	{
            	    alt95 = 1;
            	}
            	switch (alt95) 
            	{
            	    case 1 :
            	        // Ada95.g:786:16: actual_parameter_part
            	        {
            	        	PushFollow(FOLLOW_actual_parameter_part_in_function_call4047);
            	        	actual_parameter_part419 = actual_parameter_part();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_actual_parameter_part.Add(actual_parameter_part419.Tree);

            	        }
            	        break;

            	}



            	// AST REWRITE
            	// elements:          direct_name, actual_parameter_part
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 786:39: -> ^( FUNCTION direct_name ( actual_parameter_part )? )
            	{
            	    // Ada95.g:786:42: ^( FUNCTION direct_name ( actual_parameter_part )? )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(FUNCTION, "FUNCTION"), root_1);

            	    adaptor.AddChild(root_1, stream_direct_name.NextTree());
            	    // Ada95.g:786:66: ( actual_parameter_part )?
            	    if ( stream_actual_parameter_part.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_actual_parameter_part.NextTree());

            	    }
            	    stream_actual_parameter_part.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 113, function_call_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "function_call"

    public class actual_parameter_part_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "actual_parameter_part"
    // Ada95.g:788:1: actual_parameter_part : LPARANTHESIS parameter_association ( COMMA parameter_association )* RPARANTHESIS ;
    public Ada95Parser.actual_parameter_part_return actual_parameter_part() // throws RecognitionException [1]
    {   
        Ada95Parser.actual_parameter_part_return retval = new Ada95Parser.actual_parameter_part_return();
        retval.Start = input.LT(1);
        int actual_parameter_part_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken LPARANTHESIS420 = null;
        IToken COMMA422 = null;
        IToken RPARANTHESIS424 = null;
        Ada95Parser.parameter_association_return parameter_association421 = default(Ada95Parser.parameter_association_return);

        Ada95Parser.parameter_association_return parameter_association423 = default(Ada95Parser.parameter_association_return);


        CommonTree LPARANTHESIS420_tree=null;
        CommonTree COMMA422_tree=null;
        CommonTree RPARANTHESIS424_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 114) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:789:2: ( LPARANTHESIS parameter_association ( COMMA parameter_association )* RPARANTHESIS )
            // Ada95.g:789:4: LPARANTHESIS parameter_association ( COMMA parameter_association )* RPARANTHESIS
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	LPARANTHESIS420=(IToken)Match(input,LPARANTHESIS,FOLLOW_LPARANTHESIS_in_actual_parameter_part4071); if (state.failed) return retval;
            	PushFollow(FOLLOW_parameter_association_in_actual_parameter_part4074);
            	parameter_association421 = parameter_association();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, parameter_association421.Tree);
            	// Ada95.g:789:40: ( COMMA parameter_association )*
            	do 
            	{
            	    int alt96 = 2;
            	    int LA96_0 = input.LA(1);

            	    if ( (LA96_0 == COMMA) )
            	    {
            	        alt96 = 1;
            	    }


            	    switch (alt96) 
            		{
            			case 1 :
            			    // Ada95.g:789:42: COMMA parameter_association
            			    {
            			    	COMMA422=(IToken)Match(input,COMMA,FOLLOW_COMMA_in_actual_parameter_part4078); if (state.failed) return retval;
            			    	PushFollow(FOLLOW_parameter_association_in_actual_parameter_part4081);
            			    	parameter_association423 = parameter_association();
            			    	state.followingStackPointer--;
            			    	if (state.failed) return retval;
            			    	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, parameter_association423.Tree);

            			    }
            			    break;

            			default:
            			    goto loop96;
            	    }
            	} while (true);

            	loop96:
            		;	// Stops C# compiler whining that label 'loop96' has no statements

            	RPARANTHESIS424=(IToken)Match(input,RPARANTHESIS,FOLLOW_RPARANTHESIS_in_actual_parameter_part4086); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 114, actual_parameter_part_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "actual_parameter_part"

    public class parameter_association_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "parameter_association"
    // Ada95.g:791:1: parameter_association : ( selector_name ASSOCIATION )? explicit_actual_parameter -> ^( PARAMETER explicit_actual_parameter ( selector_name )? ) ;
    public Ada95Parser.parameter_association_return parameter_association() // throws RecognitionException [1]
    {   
        Ada95Parser.parameter_association_return retval = new Ada95Parser.parameter_association_return();
        retval.Start = input.LT(1);
        int parameter_association_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken ASSOCIATION426 = null;
        Ada95Parser.selector_name_return selector_name425 = default(Ada95Parser.selector_name_return);

        Ada95Parser.explicit_actual_parameter_return explicit_actual_parameter427 = default(Ada95Parser.explicit_actual_parameter_return);


        CommonTree ASSOCIATION426_tree=null;
        RewriteRuleTokenStream stream_ASSOCIATION = new RewriteRuleTokenStream(adaptor,"token ASSOCIATION");
        RewriteRuleSubtreeStream stream_selector_name = new RewriteRuleSubtreeStream(adaptor,"rule selector_name");
        RewriteRuleSubtreeStream stream_explicit_actual_parameter = new RewriteRuleSubtreeStream(adaptor,"rule explicit_actual_parameter");
        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 115) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:792:2: ( ( selector_name ASSOCIATION )? explicit_actual_parameter -> ^( PARAMETER explicit_actual_parameter ( selector_name )? ) )
            // Ada95.g:792:4: ( selector_name ASSOCIATION )? explicit_actual_parameter
            {
            	// Ada95.g:792:4: ( selector_name ASSOCIATION )?
            	int alt97 = 2;
            	switch ( input.LA(1) ) 
            	{
            	    case IDENTIFIER:
            	    	{
            	        int LA97_1 = input.LA(2);

            	        if ( (LA97_1 == ASSOCIATION) )
            	        {
            	            alt97 = 1;
            	        }
            	        }
            	        break;
            	    case CHARACTER_LITERAL:
            	    	{
            	        int LA97_2 = input.LA(2);

            	        if ( (LA97_2 == ASSOCIATION) )
            	        {
            	            alt97 = 1;
            	        }
            	        }
            	        break;
            	    case STRING_LITERAL:
            	    	{
            	        int LA97_3 = input.LA(2);

            	        if ( (LA97_3 == ASSOCIATION) )
            	        {
            	            alt97 = 1;
            	        }
            	        }
            	        break;
            	}

            	switch (alt97) 
            	{
            	    case 1 :
            	        // Ada95.g:792:6: selector_name ASSOCIATION
            	        {
            	        	PushFollow(FOLLOW_selector_name_in_parameter_association4099);
            	        	selector_name425 = selector_name();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( (state.backtracking==0) ) stream_selector_name.Add(selector_name425.Tree);
            	        	ASSOCIATION426=(IToken)Match(input,ASSOCIATION,FOLLOW_ASSOCIATION_in_parameter_association4101); if (state.failed) return retval; 
            	        	if ( (state.backtracking==0) ) stream_ASSOCIATION.Add(ASSOCIATION426);


            	        }
            	        break;

            	}

            	PushFollow(FOLLOW_explicit_actual_parameter_in_parameter_association4106);
            	explicit_actual_parameter427 = explicit_actual_parameter();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( (state.backtracking==0) ) stream_explicit_actual_parameter.Add(explicit_actual_parameter427.Tree);


            	// AST REWRITE
            	// elements:          explicit_actual_parameter, selector_name
            	// token labels:      
            	// rule labels:       retval
            	// token list labels: 
            	// rule list labels:  
            	// wildcard labels: 
            	if ( (state.backtracking==0) ) {
            	retval.Tree = root_0;
            	RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval!=null ? retval.Tree : null);

            	root_0 = (CommonTree)adaptor.GetNilNode();
            	// 792:61: -> ^( PARAMETER explicit_actual_parameter ( selector_name )? )
            	{
            	    // Ada95.g:792:64: ^( PARAMETER explicit_actual_parameter ( selector_name )? )
            	    {
            	    CommonTree root_1 = (CommonTree)adaptor.GetNilNode();
            	    root_1 = (CommonTree)adaptor.BecomeRoot((CommonTree)adaptor.Create(PARAMETER, "PARAMETER"), root_1);

            	    adaptor.AddChild(root_1, stream_explicit_actual_parameter.NextTree());
            	    // Ada95.g:792:103: ( selector_name )?
            	    if ( stream_selector_name.HasNext() )
            	    {
            	        adaptor.AddChild(root_1, stream_selector_name.NextTree());

            	    }
            	    stream_selector_name.Reset();

            	    adaptor.AddChild(root_0, root_1);
            	    }

            	}

            	retval.Tree = root_0;retval.Tree = root_0;}
            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 115, parameter_association_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "parameter_association"

    public class explicit_actual_parameter_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "explicit_actual_parameter"
    // Ada95.g:794:1: explicit_actual_parameter : expression ;
    public Ada95Parser.explicit_actual_parameter_return explicit_actual_parameter() // throws RecognitionException [1]
    {   
        Ada95Parser.explicit_actual_parameter_return retval = new Ada95Parser.explicit_actual_parameter_return();
        retval.Start = input.LT(1);
        int explicit_actual_parameter_StartIndex = input.Index();
        CommonTree root_0 = null;

        Ada95Parser.expression_return expression428 = default(Ada95Parser.expression_return);



        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 116) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:795:2: ( expression )
            // Ada95.g:795:4: expression
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_expression_in_explicit_actual_parameter4129);
            	expression428 = expression();
            	state.followingStackPointer--;
            	if (state.failed) return retval;
            	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression428.Tree);

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 116, explicit_actual_parameter_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "explicit_actual_parameter"

    public class return_statement_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "return_statement"
    // Ada95.g:797:1: return_statement : RETURN ( expression )? SEMICOLON ;
    public Ada95Parser.return_statement_return return_statement() // throws RecognitionException [1]
    {   
        Ada95Parser.return_statement_return retval = new Ada95Parser.return_statement_return();
        retval.Start = input.LT(1);
        int return_statement_StartIndex = input.Index();
        CommonTree root_0 = null;

        IToken RETURN429 = null;
        IToken SEMICOLON431 = null;
        Ada95Parser.expression_return expression430 = default(Ada95Parser.expression_return);


        CommonTree RETURN429_tree=null;
        CommonTree SEMICOLON431_tree=null;

        try 
    	{
    	    if ( (state.backtracking > 0) && AlreadyParsedRule(input, 117) ) 
    	    {
    	    	return retval; 
    	    }
            // Ada95.g:798:2: ( RETURN ( expression )? SEMICOLON )
            // Ada95.g:798:4: RETURN ( expression )? SEMICOLON
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	RETURN429=(IToken)Match(input,RETURN,FOLLOW_RETURN_in_return_statement4139); if (state.failed) return retval;
            	if ( state.backtracking == 0 )
            	{RETURN429_tree = (CommonTree)adaptor.Create(RETURN429);
            		root_0 = (CommonTree)adaptor.BecomeRoot(RETURN429_tree, root_0);
            	}
            	// Ada95.g:798:12: ( expression )?
            	int alt98 = 2;
            	int LA98_0 = input.LA(1);

            	if ( (LA98_0 == IDENTIFIER || LA98_0 == LPARANTHESIS || LA98_0 == CHARACTER_LITERAL || LA98_0 == NULL || LA98_0 == NOT || (LA98_0 >= ABS && LA98_0 <= STRING_LITERAL) || (LA98_0 >= PLUS && LA98_0 <= MINUS)) )
            	{
            	    alt98 = 1;
            	}
            	switch (alt98) 
            	{
            	    case 1 :
            	        // Ada95.g:798:12: expression
            	        {
            	        	PushFollow(FOLLOW_expression_in_return_statement4142);
            	        	expression430 = expression();
            	        	state.followingStackPointer--;
            	        	if (state.failed) return retval;
            	        	if ( state.backtracking == 0 ) adaptor.AddChild(root_0, expression430.Tree);

            	        }
            	        break;

            	}

            	SEMICOLON431=(IToken)Match(input,SEMICOLON,FOLLOW_SEMICOLON_in_return_statement4145); if (state.failed) return retval;

            }

            retval.Stop = input.LT(-1);

            if ( (state.backtracking==0) )
            {	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);}
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
            if ( state.backtracking > 0 ) 
            {
            	Memoize(input, 117, return_statement_StartIndex); 
            }
        }
        return retval;
    }
    // $ANTLR end "return_statement"

    // $ANTLR start "synpred1_Ada95"
    public void synpred1_Ada95_fragment() {
        // Ada95.g:258:3: ( ordinary_fixed_point_definition )
        // Ada95.g:258:5: ordinary_fixed_point_definition
        {
        	PushFollow(FOLLOW_ordinary_fixed_point_definition_in_synpred1_Ada95875);
        	ordinary_fixed_point_definition();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred1_Ada95"

    // $ANTLR start "synpred2_Ada95"
    public void synpred2_Ada95_fragment() {
        // Ada95.g:342:3: ( ( component_item )* variant_part )
        // Ada95.g:342:5: ( component_item )* variant_part
        {
        	// Ada95.g:342:5: ( component_item )*
        	do 
        	{
        	    int alt99 = 2;
        	    int LA99_0 = input.LA(1);

        	    if ( (LA99_0 == IDENTIFIER) )
        	    {
        	        alt99 = 1;
        	    }


        	    switch (alt99) 
        		{
        			case 1 :
        			    // Ada95.g:342:5: component_item
        			    {
        			    	PushFollow(FOLLOW_component_item_in_synpred2_Ada951368);
        			    	component_item();
        			    	state.followingStackPointer--;
        			    	if (state.failed) return ;

        			    }
        			    break;

        			default:
        			    goto loop99;
        	    }
        	} while (true);

        	loop99:
        		;	// Stops C# compiler whining that label 'loop99' has no statements

        	PushFollow(FOLLOW_variant_part_in_synpred2_Ada951371);
        	variant_part();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred2_Ada95"

    // $ANTLR start "synpred3_Ada95"
    public void synpred3_Ada95_fragment() {
        // Ada95.g:366:4: ( ( variant )* other_variant )
        // Ada95.g:366:6: ( variant )* other_variant
        {
        	// Ada95.g:366:6: ( variant )*
        	do 
        	{
        	    int alt100 = 2;
        	    int LA100_0 = input.LA(1);

        	    if ( (LA100_0 == WHEN) )
        	    {
        	        int LA100_1 = input.LA(2);

        	        if ( (LA100_1 == IDENTIFIER || LA100_1 == LPARANTHESIS || LA100_1 == CHARACTER_LITERAL || LA100_1 == NULL || LA100_1 == NOT || (LA100_1 >= ABS && LA100_1 <= STRING_LITERAL) || (LA100_1 >= PLUS && LA100_1 <= MINUS)) )
        	        {
        	            alt100 = 1;
        	        }


        	    }


        	    switch (alt100) 
        		{
        			case 1 :
        			    // Ada95.g:366:6: variant
        			    {
        			    	PushFollow(FOLLOW_variant_in_synpred3_Ada951546);
        			    	variant();
        			    	state.followingStackPointer--;
        			    	if (state.failed) return ;

        			    }
        			    break;

        			default:
        			    goto loop100;
        	    }
        	} while (true);

        	loop100:
        		;	// Stops C# compiler whining that label 'loop100' has no statements

        	PushFollow(FOLLOW_other_variant_in_synpred3_Ada951549);
        	other_variant();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred3_Ada95"

    // $ANTLR start "synpred4_Ada95"
    public void synpred4_Ada95_fragment() {
        // Ada95.g:382:3: ( discrete_range )
        // Ada95.g:382:5: discrete_range
        {
        	PushFollow(FOLLOW_discrete_range_in_synpred4_Ada951659);
        	discrete_range();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred4_Ada95"

    // $ANTLR start "synpred5_Ada95"
    public void synpred5_Ada95_fragment() {
        // Ada95.g:410:3: ( basic_declarative_item )
        // Ada95.g:410:5: basic_declarative_item
        {
        	PushFollow(FOLLOW_basic_declarative_item_in_synpred5_Ada951739);
        	basic_declarative_item();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred5_Ada95"

    // $ANTLR start "synpred6_Ada95"
    public void synpred6_Ada95_fragment() {
        // Ada95.g:428:14: ( slice )
        // Ada95.g:428:14: slice
        {
        	PushFollow(FOLLOW_slice_in_synpred6_Ada951875);
        	slice();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred6_Ada95"

    // $ANTLR start "synpred7_Ada95"
    public void synpred7_Ada95_fragment() {
        // Ada95.g:429:14: ( indexed_component )
        // Ada95.g:429:14: indexed_component
        {
        	PushFollow(FOLLOW_indexed_component_in_synpred7_Ada951900);
        	indexed_component();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred7_Ada95"

    // $ANTLR start "synpred8_Ada95"
    public void synpred8_Ada95_fragment() {
        // Ada95.g:430:14: ( explicit_dereference )
        // Ada95.g:430:14: explicit_dereference
        {
        	PushFollow(FOLLOW_explicit_dereference_in_synpred8_Ada951922);
        	explicit_dereference();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred8_Ada95"

    // $ANTLR start "synpred9_Ada95"
    public void synpred9_Ada95_fragment() {
        // Ada95.g:431:14: ( selected_component )
        // Ada95.g:431:14: selected_component
        {
        	PushFollow(FOLLOW_selected_component_in_synpred9_Ada951943);
        	selected_component();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred9_Ada95"

    // $ANTLR start "synpred10_Ada95"
    public void synpred10_Ada95_fragment() {
        // Ada95.g:438:4: ( type_conversion )
        // Ada95.g:438:4: type_conversion
        {
        	PushFollow(FOLLOW_type_conversion_in_synpred10_Ada952051);
        	type_conversion();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred10_Ada95"

    // $ANTLR start "synpred11_Ada95"
    public void synpred11_Ada95_fragment() {
        // Ada95.g:439:4: ( indexed_component )
        // Ada95.g:439:4: indexed_component
        {
        	PushFollow(FOLLOW_indexed_component_in_synpred11_Ada952067);
        	indexed_component();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred11_Ada95"

    // $ANTLR start "synpred12_Ada95"
    public void synpred12_Ada95_fragment() {
        // Ada95.g:440:4: ( slice )
        // Ada95.g:440:4: slice
        {
        	PushFollow(FOLLOW_slice_in_synpred12_Ada952081);
        	slice();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred12_Ada95"

    // $ANTLR start "synpred13_Ada95"
    public void synpred13_Ada95_fragment() {
        // Ada95.g:441:4: ( explicit_dereference )
        // Ada95.g:441:4: explicit_dereference
        {
        	PushFollow(FOLLOW_explicit_dereference_in_synpred13_Ada952101);
        	explicit_dereference();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred13_Ada95"

    // $ANTLR start "synpred14_Ada95"
    public void synpred14_Ada95_fragment() {
        // Ada95.g:442:4: ( selected_component )
        // Ada95.g:442:4: selected_component
        {
        	PushFollow(FOLLOW_selected_component_in_synpred14_Ada952113);
        	selected_component();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred14_Ada95"

    // $ANTLR start "synpred15_Ada95"
    public void synpred15_Ada95_fragment() {
        // Ada95.g:535:5: ( range )
        // Ada95.g:535:7: range
        {
        	PushFollow(FOLLOW_range_in_synpred15_Ada952586);
        	range();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred15_Ada95"

    // $ANTLR start "synpred16_Ada95"
    public void synpred16_Ada95_fragment() {
        // Ada95.g:550:3: ( primary POW )
        // Ada95.g:550:5: primary POW
        {
        	PushFollow(FOLLOW_primary_in_synpred16_Ada952712);
        	primary();
        	state.followingStackPointer--;
        	if (state.failed) return ;
        	Match(input,POW,FOLLOW_POW_in_synpred16_Ada952714); if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred16_Ada95"

    // $ANTLR start "synpred17_Ada95"
    public void synpred17_Ada95_fragment() {
        // Ada95.g:608:3: ( assignment_statement )
        // Ada95.g:608:5: assignment_statement
        {
        	PushFollow(FOLLOW_assignment_statement_in_synpred17_Ada953067);
        	assignment_statement();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred17_Ada95"

    // $ANTLR start "synpred18_Ada95"
    public void synpred18_Ada95_fragment() {
        // Ada95.g:646:4: ( ( case_statement_alternative )* other_case_statement_alternative )
        // Ada95.g:646:6: ( case_statement_alternative )* other_case_statement_alternative
        {
        	// Ada95.g:646:6: ( case_statement_alternative )*
        	do 
        	{
        	    int alt101 = 2;
        	    int LA101_0 = input.LA(1);

        	    if ( (LA101_0 == WHEN) )
        	    {
        	        int LA101_1 = input.LA(2);

        	        if ( (LA101_1 == IDENTIFIER || LA101_1 == LPARANTHESIS || LA101_1 == CHARACTER_LITERAL || LA101_1 == NULL || LA101_1 == NOT || (LA101_1 >= ABS && LA101_1 <= STRING_LITERAL) || (LA101_1 >= PLUS && LA101_1 <= MINUS)) )
        	        {
        	            alt101 = 1;
        	        }


        	    }


        	    switch (alt101) 
        		{
        			case 1 :
        			    // Ada95.g:646:6: case_statement_alternative
        			    {
        			    	PushFollow(FOLLOW_case_statement_alternative_in_synpred18_Ada953325);
        			    	case_statement_alternative();
        			    	state.followingStackPointer--;
        			    	if (state.failed) return ;

        			    }
        			    break;

        			default:
        			    goto loop101;
        	    }
        	} while (true);

        	loop101:
        		;	// Stops C# compiler whining that label 'loop101' has no statements

        	PushFollow(FOLLOW_other_case_statement_alternative_in_synpred18_Ada953328);
        	other_case_statement_alternative();
        	state.followingStackPointer--;
        	if (state.failed) return ;

        }
    }
    // $ANTLR end "synpred18_Ada95"

    // Delegated rules

   	public bool synpred2_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred2_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred14_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred14_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred7_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred7_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred16_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred16_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred8_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred8_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred17_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred17_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred4_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred4_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred5_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred5_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred6_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred6_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred18_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred18_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred13_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred13_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred3_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred3_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred12_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred12_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred1_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred1_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred10_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred10_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred15_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred15_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred9_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred9_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}
   	public bool synpred11_Ada95() 
   	{
   	    state.backtracking++;
   	    int start = input.Mark();
   	    try 
   	    {
   	        synpred11_Ada95_fragment(); // can never throw exception
   	    }
   	    catch (RecognitionException re) 
   	    {
   	        Console.Error.WriteLine("impossible: "+re);
   	    }
   	    bool success = !state.failed;
   	    input.Rewind(start);
   	    state.backtracking--;
   	    state.failed = false;
   	    return success;
   	}


   	protected DFA1 dfa1;
   	protected DFA41 dfa41;
   	protected DFA57 dfa57;
   	protected DFA62 dfa62;
	private void InitializeCyclicDFAs()
	{
    	this.dfa1 = new DFA1(this);
    	this.dfa41 = new DFA41(this);
    	this.dfa57 = new DFA57(this);
    	this.dfa62 = new DFA62(this);
	    this.dfa41.specialStateTransitionHandler = new DFA.SpecialStateTransitionHandler(DFA41_SpecialStateTransition);
	    this.dfa57.specialStateTransitionHandler = new DFA.SpecialStateTransitionHandler(DFA57_SpecialStateTransition);
	    this.dfa62.specialStateTransitionHandler = new DFA.SpecialStateTransitionHandler(DFA62_SpecialStateTransition);
	}

    const string DFA1_eotS =
        "\x0a\uffff";
    const string DFA1_eofS =
        "\x0a\uffff";
    const string DFA1_minS =
        "\x01\x22\x01\uffff\x01\x27\x01\uffff\x02\x22\x01\x27\x01\x22\x02"+
        "\uffff";
    const string DFA1_maxS =
        "\x01\x6c\x01\uffff\x01\x2d\x01\uffff\x01\x22\x01\x36\x01\x2d\x01"+
        "\x36\x02\uffff";
    const string DFA1_acceptS =
        "\x01\uffff\x01\x01\x01\uffff\x01\x04\x04\uffff\x01\x02\x01\x03";
    const string DFA1_specialS =
        "\x0a\uffff}>";
    static readonly string[] DFA1_transitionS = {
            "\x01\x02\x01\x01\x47\uffff\x02\x03",
            "",
            "\x01\x05\x05\uffff\x01\x04",
            "",
            "\x01\x06",
            "\x01\x08\x05\uffff\x01\x07\x03\uffff\x01\x08\x09\uffff\x01"+
            "\x08",
            "\x01\x05\x05\uffff\x01\x04",
            "\x01\x08\x06\uffff\x01\x09\x0c\uffff\x01\x08",
            "",
            ""
    };

    static readonly short[] DFA1_eot = DFA.UnpackEncodedString(DFA1_eotS);
    static readonly short[] DFA1_eof = DFA.UnpackEncodedString(DFA1_eofS);
    static readonly char[] DFA1_min = DFA.UnpackEncodedStringToUnsignedChars(DFA1_minS);
    static readonly char[] DFA1_max = DFA.UnpackEncodedStringToUnsignedChars(DFA1_maxS);
    static readonly short[] DFA1_accept = DFA.UnpackEncodedString(DFA1_acceptS);
    static readonly short[] DFA1_special = DFA.UnpackEncodedString(DFA1_specialS);
    static readonly short[][] DFA1_transition = DFA.UnpackEncodedStringArray(DFA1_transitionS);

    protected class DFA1 : DFA
    {
        public DFA1(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 1;
            this.eot = DFA1_eot;
            this.eof = DFA1_eof;
            this.min = DFA1_min;
            this.max = DFA1_max;
            this.accept = DFA1_accept;
            this.special = DFA1_special;
            this.transition = DFA1_transition;

        }

        override public string Description
        {
            get { return "125:1: basic_declaration : ( type_declaration | object_declaration | number_declaration | subprogram_declaration );"; }
        }

    }

    const string DFA41_eotS =
        "\x0c\uffff";
    const string DFA41_eofS =
        "\x0c\uffff";
    const string DFA41_minS =
        "\x01\x22\x09\x00\x02\uffff";
    const string DFA41_maxS =
        "\x01\x5a\x09\x00\x02\uffff";
    const string DFA41_acceptS =
        "\x0a\uffff\x01\x01\x01\x02";
    const string DFA41_specialS =
        "\x01\uffff\x01\x00\x01\x01\x01\x02\x01\x03\x01\x04\x01\x05\x01"+
        "\x06\x01\x07\x01\x08\x02\uffff}>";
    static readonly string[] DFA41_transitionS = {
            "\x01\x01\x0d\uffff\x01\x07\x01\uffff\x01\x06\x0a\uffff\x01"+
            "\x04\x0f\uffff\x01\x09\x02\uffff\x01\x08\x01\x03\x01\x05\x06"+
            "\uffff\x02\x02",
            "\x01\uffff",
            "\x01\uffff",
            "\x01\uffff",
            "\x01\uffff",
            "\x01\uffff",
            "\x01\uffff",
            "\x01\uffff",
            "\x01\uffff",
            "\x01\uffff",
            "",
            ""
    };

    static readonly short[] DFA41_eot = DFA.UnpackEncodedString(DFA41_eotS);
    static readonly short[] DFA41_eof = DFA.UnpackEncodedString(DFA41_eofS);
    static readonly char[] DFA41_min = DFA.UnpackEncodedStringToUnsignedChars(DFA41_minS);
    static readonly char[] DFA41_max = DFA.UnpackEncodedStringToUnsignedChars(DFA41_maxS);
    static readonly short[] DFA41_accept = DFA.UnpackEncodedString(DFA41_acceptS);
    static readonly short[] DFA41_special = DFA.UnpackEncodedString(DFA41_specialS);
    static readonly short[][] DFA41_transition = DFA.UnpackEncodedStringArray(DFA41_transitionS);

    protected class DFA41 : DFA
    {
        public DFA41(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 41;
            this.eot = DFA41_eot;
            this.eof = DFA41_eof;
            this.min = DFA41_min;
            this.max = DFA41_max;
            this.accept = DFA41_accept;
            this.special = DFA41_special;
            this.transition = DFA41_transition;

        }

        override public string Description
        {
            get { return "380:1: discrete_choice : ( ( discrete_range )=> discrete_range | expression );"; }
        }

    }


    protected internal int DFA41_SpecialStateTransition(DFA dfa, int s, IIntStream _input) //throws NoViableAltException
    {
            ITokenStream input = (ITokenStream)_input;
    	int _s = s;
        switch ( s )
        {
               	case 0 : 
                   	int LA41_1 = input.LA(1);

                   	 
                   	int index41_1 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( ((synpred4_Ada95()|| (synpred4_Ada95() && ( IsType( input.LT( 1 ).Text ) ))|| (synpred4_Ada95() && ( IsType( input.LT( 1 ).Text ) ))|| (synpred4_Ada95() && ( IsFunction( input.LT( 1 ).Text ) ))|| (synpred4_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred4_Ada95() && ( IsFunction( input.LT( 1 ).Text ) ))|| (synpred4_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred4_Ada95() && ( IsType( input.LT( 1 ).Text ) )))) ) { s = 10; }

                   	else if ( (true) ) { s = 11; }

                   	 
                   	input.Seek(index41_1);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 1 : 
                   	int LA41_2 = input.LA(1);

                   	 
                   	int index41_2 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred4_Ada95()) ) { s = 10; }

                   	else if ( (true) ) { s = 11; }

                   	 
                   	input.Seek(index41_2);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 2 : 
                   	int LA41_3 = input.LA(1);

                   	 
                   	int index41_3 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred4_Ada95()) ) { s = 10; }

                   	else if ( (true) ) { s = 11; }

                   	 
                   	input.Seek(index41_3);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 3 : 
                   	int LA41_4 = input.LA(1);

                   	 
                   	int index41_4 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred4_Ada95()) ) { s = 10; }

                   	else if ( (true) ) { s = 11; }

                   	 
                   	input.Seek(index41_4);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 4 : 
                   	int LA41_5 = input.LA(1);

                   	 
                   	int index41_5 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred4_Ada95()) ) { s = 10; }

                   	else if ( (true) ) { s = 11; }

                   	 
                   	input.Seek(index41_5);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 5 : 
                   	int LA41_6 = input.LA(1);

                   	 
                   	int index41_6 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred4_Ada95()) ) { s = 10; }

                   	else if ( (true) ) { s = 11; }

                   	 
                   	input.Seek(index41_6);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 6 : 
                   	int LA41_7 = input.LA(1);

                   	 
                   	int index41_7 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred4_Ada95()) ) { s = 10; }

                   	else if ( (true) ) { s = 11; }

                   	 
                   	input.Seek(index41_7);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 7 : 
                   	int LA41_8 = input.LA(1);

                   	 
                   	int index41_8 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred4_Ada95()) ) { s = 10; }

                   	else if ( (true) ) { s = 11; }

                   	 
                   	input.Seek(index41_8);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 8 : 
                   	int LA41_9 = input.LA(1);

                   	 
                   	int index41_9 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred4_Ada95()) ) { s = 10; }

                   	else if ( (true) ) { s = 11; }

                   	 
                   	input.Seek(index41_9);
                   	if ( s >= 0 ) return s;
                   	break;
        }
        if (state.backtracking > 0) {state.failed = true; return -1;}
        NoViableAltException nvae41 =
            new NoViableAltException(dfa.Description, 41, _s, input);
        dfa.Error(nvae41);
        throw nvae41;
    }
    const string DFA57_eotS =
        "\x12\uffff";
    const string DFA57_eofS =
        "\x05\uffff\x01\x11\x0c\uffff";
    const string DFA57_minS =
        "\x01\x22\x04\uffff\x01\x24\x0c\uffff";
    const string DFA57_maxS =
        "\x01\x5a\x04\uffff\x01\x63\x0c\uffff";
    const string DFA57_acceptS =
        "\x01\uffff\x04\x01\x01\uffff\x0b\x01\x01\x02";
    const string DFA57_specialS =
        "\x01\x00\x04\uffff\x01\x01\x0c\uffff}>";
    static readonly string[] DFA57_transitionS = {
            "\x01\x05\x0d\uffff\x01\x07\x01\uffff\x01\x06\x0a\uffff\x01"+
            "\x03\x0f\uffff\x01\x09\x02\uffff\x01\x08\x01\x02\x01\x04\x06"+
            "\uffff\x02\x01",
            "",
            "",
            "",
            "",
            "\x02\x11\x03\uffff\x01\x11\x03\uffff\x02\x11\x01\x10\x01\x0a"+
            "\x01\x11\x01\uffff\x01\x0e\x01\x11\x0b\uffff\x01\x11\x01\uffff"+
            "\x02\x11\x01\uffff\x01\x0b\x01\uffff\x01\x0d\x03\x11\x01\uffff"+
            "\x01\x11\x02\uffff\x01\x0c\x09\uffff\x03\x0f\x03\x0e\x04\uffff"+
            "\x01\x11",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static readonly short[] DFA57_eot = DFA.UnpackEncodedString(DFA57_eotS);
    static readonly short[] DFA57_eof = DFA.UnpackEncodedString(DFA57_eofS);
    static readonly char[] DFA57_min = DFA.UnpackEncodedStringToUnsignedChars(DFA57_minS);
    static readonly char[] DFA57_max = DFA.UnpackEncodedStringToUnsignedChars(DFA57_maxS);
    static readonly short[] DFA57_accept = DFA.UnpackEncodedString(DFA57_acceptS);
    static readonly short[] DFA57_special = DFA.UnpackEncodedString(DFA57_specialS);
    static readonly short[][] DFA57_transition = DFA.UnpackEncodedStringArray(DFA57_transitionS);

    protected class DFA57 : DFA
    {
        public DFA57(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 57;
            this.eot = DFA57_eot;
            this.eof = DFA57_eof;
            this.min = DFA57_min;
            this.max = DFA57_max;
            this.accept = DFA57_accept;
            this.special = DFA57_special;
            this.transition = DFA57_transition;

        }

        override public string Description
        {
            get { return "534:4: ( ( range )=> range -> ^( IN_RANGE simple_expression range ( NOT )? ) | subtype_mark -> ^( IN_SUBTYPE simple_expression subtype_mark ( NOT )? ) )"; }
        }

    }


    protected internal int DFA57_SpecialStateTransition(DFA dfa, int s, IIntStream _input) //throws NoViableAltException
    {
            ITokenStream input = (ITokenStream)_input;
    	int _s = s;
        switch ( s )
        {
               	case 0 : 
                   	int LA57_0 = input.LA(1);

                   	 
                   	int index57_0 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( ((LA57_0 >= PLUS && LA57_0 <= MINUS)) && (synpred15_Ada95()) ) { s = 1; }

                   	else if ( (LA57_0 == NUMERIC_LITERAL) && (synpred15_Ada95()) ) { s = 2; }

                   	else if ( (LA57_0 == NULL) && (synpred15_Ada95()) ) { s = 3; }

                   	else if ( (LA57_0 == STRING_LITERAL) && (synpred15_Ada95()) ) { s = 4; }

                   	else if ( (LA57_0 == IDENTIFIER) ) { s = 5; }

                   	else if ( (LA57_0 == CHARACTER_LITERAL) && (synpred15_Ada95()) ) { s = 6; }

                   	else if ( (LA57_0 == LPARANTHESIS) && (synpred15_Ada95()) ) { s = 7; }

                   	else if ( (LA57_0 == ABS) && (synpred15_Ada95()) ) { s = 8; }

                   	else if ( (LA57_0 == NOT) && (synpred15_Ada95()) ) { s = 9; }

                   	 
                   	input.Seek(index57_0);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 1 : 
                   	int LA57_5 = input.LA(1);

                   	 
                   	int index57_5 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (LA57_5 == LPARANTHESIS) && (((synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsFunction( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsType( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsType( input.LT( 1 ).Text ) ))|| synpred15_Ada95()|| (synpred15_Ada95() && ( IsFunction( input.LT( 1 ).Text ) )))) ) { s = 10; }

                   	else if ( (LA57_5 == DOT) && (((synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| synpred15_Ada95()|| (synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) )))) ) { s = 11; }

                   	else if ( (LA57_5 == POW) && (((synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsFunction( input.LT( 1 ).Text ) ))|| synpred15_Ada95())) ) { s = 12; }

                   	else if ( (LA57_5 == APOSTROPHE) && (synpred15_Ada95()) ) { s = 13; }

                   	else if ( (LA57_5 == MOD || (LA57_5 >= MULT && LA57_5 <= REM)) && (((synpred15_Ada95() && ( IsFunction( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| synpred15_Ada95())) ) { s = 14; }

                   	else if ( ((LA57_5 >= PLUS && LA57_5 <= CONCAT)) && (((synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsFunction( input.LT( 1 ).Text ) ))|| synpred15_Ada95())) ) { s = 15; }

                   	else if ( (LA57_5 == DOTDOT) && (((synpred15_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| (synpred15_Ada95() && ( IsFunction( input.LT( 1 ).Text ) ))|| synpred15_Ada95())) ) { s = 16; }

                   	else if ( (LA57_5 == EOF || (LA57_5 >= IS && LA57_5 <= SEMICOLON) || LA57_5 == ASSIGN || (LA57_5 >= COMMA && LA57_5 <= RANGE) || LA57_5 == RPARANTHESIS || LA57_5 == DIGITS || LA57_5 == ASSOCIATION || (LA57_5 >= CHOICE && LA57_5 <= WITH) || (LA57_5 >= AND && LA57_5 <= OR) || LA57_5 == XOR || LA57_5 == LOOP) ) { s = 17; }

                   	 
                   	input.Seek(index57_5);
                   	if ( s >= 0 ) return s;
                   	break;
        }
        if (state.backtracking > 0) {state.failed = true; return -1;}
        NoViableAltException nvae57 =
            new NoViableAltException(dfa.Description, 57, _s, input);
        dfa.Error(nvae57);
        throw nvae57;
    }
    const string DFA62_eotS =
        "\x0b\uffff";
    const string DFA62_eofS =
        "\x0b\uffff";
    const string DFA62_minS =
        "\x01\x22\x06\x00\x04\uffff";
    const string DFA62_maxS =
        "\x01\x52\x06\x00\x04\uffff";
    const string DFA62_acceptS =
        "\x07\uffff\x01\x03\x01\x04\x01\x01\x01\x02";
    const string DFA62_specialS =
        "\x01\uffff\x01\x00\x01\x01\x01\x02\x01\x03\x01\x04\x01\x05\x04"+
        "\uffff}>";
    static readonly string[] DFA62_transitionS = {
            "\x01\x04\x0d\uffff\x01\x06\x01\uffff\x01\x05\x0a\uffff\x01"+
            "\x02\x0f\uffff\x01\x08\x02\uffff\x01\x07\x01\x01\x01\x03",
            "\x01\uffff",
            "\x01\uffff",
            "\x01\uffff",
            "\x01\uffff",
            "\x01\uffff",
            "\x01\uffff",
            "",
            "",
            "",
            ""
    };

    static readonly short[] DFA62_eot = DFA.UnpackEncodedString(DFA62_eotS);
    static readonly short[] DFA62_eof = DFA.UnpackEncodedString(DFA62_eofS);
    static readonly char[] DFA62_min = DFA.UnpackEncodedStringToUnsignedChars(DFA62_minS);
    static readonly char[] DFA62_max = DFA.UnpackEncodedStringToUnsignedChars(DFA62_maxS);
    static readonly short[] DFA62_accept = DFA.UnpackEncodedString(DFA62_acceptS);
    static readonly short[] DFA62_special = DFA.UnpackEncodedString(DFA62_specialS);
    static readonly short[][] DFA62_transition = DFA.UnpackEncodedStringArray(DFA62_transitionS);

    protected class DFA62 : DFA
    {
        public DFA62(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 62;
            this.eot = DFA62_eot;
            this.eof = DFA62_eof;
            this.min = DFA62_min;
            this.max = DFA62_max;
            this.accept = DFA62_accept;
            this.special = DFA62_special;
            this.transition = DFA62_transition;

        }

        override public string Description
        {
            get { return "548:1: factor : ( ( primary POW )=> primary POW primary | primary | ABS primary | NOT primary );"; }
        }

    }


    protected internal int DFA62_SpecialStateTransition(DFA dfa, int s, IIntStream _input) //throws NoViableAltException
    {
            ITokenStream input = (ITokenStream)_input;
    	int _s = s;
        switch ( s )
        {
               	case 0 : 
                   	int LA62_1 = input.LA(1);

                   	 
                   	int index62_1 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred16_Ada95()) ) { s = 9; }

                   	else if ( (true) ) { s = 10; }

                   	 
                   	input.Seek(index62_1);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 1 : 
                   	int LA62_2 = input.LA(1);

                   	 
                   	int index62_2 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred16_Ada95()) ) { s = 9; }

                   	else if ( (true) ) { s = 10; }

                   	 
                   	input.Seek(index62_2);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 2 : 
                   	int LA62_3 = input.LA(1);

                   	 
                   	int index62_3 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred16_Ada95()) ) { s = 9; }

                   	else if ( (true) ) { s = 10; }

                   	 
                   	input.Seek(index62_3);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 3 : 
                   	int LA62_4 = input.LA(1);

                   	 
                   	int index62_4 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (((synpred16_Ada95() && ( IsType( input.LT( 1 ).Text ) ))|| (synpred16_Ada95() && ( IsFunction( input.LT( 1 ).Text ) ))|| (synpred16_Ada95() && ( IsObject( input.LT( 1 ).Text ) ))|| synpred16_Ada95())) ) { s = 9; }

                   	else if ( (true) ) { s = 10; }

                   	 
                   	input.Seek(index62_4);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 4 : 
                   	int LA62_5 = input.LA(1);

                   	 
                   	int index62_5 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred16_Ada95()) ) { s = 9; }

                   	else if ( (true) ) { s = 10; }

                   	 
                   	input.Seek(index62_5);
                   	if ( s >= 0 ) return s;
                   	break;
               	case 5 : 
                   	int LA62_6 = input.LA(1);

                   	 
                   	int index62_6 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( (synpred16_Ada95()) ) { s = 9; }

                   	else if ( (true) ) { s = 10; }

                   	 
                   	input.Seek(index62_6);
                   	if ( s >= 0 ) return s;
                   	break;
        }
        if (state.backtracking > 0) {state.failed = true; return -1;}
        NoViableAltException nvae62 =
            new NoViableAltException(dfa.Description, 62, _s, input);
        dfa.Error(nvae62);
        throw nvae62;
    }
 

    public static readonly BitSet FOLLOW_declarative_part_in_program235 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_type_declaration_in_basic_declaration247 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_object_declaration_in_basic_declaration256 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_number_declaration_in_basic_declaration262 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subprogram_declaration_in_basic_declaration268 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_defining_identifier279 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_full_type_declaration_in_type_declaration289 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_TYPE_in_full_type_declaration301 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_full_type_declaration308 = new BitSet(new ulong[]{0x0000001000000000UL});
    public static readonly BitSet FOLLOW_IS_in_full_type_declaration312 = new BitSet(new ulong[]{0x2E794C0000000000UL});
    public static readonly BitSet FOLLOW_type_definition_in_full_type_declaration315 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_full_type_declaration317 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_enumeration_type_definition_in_type_definition335 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_integer_type_definition_in_type_definition341 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_real_type_definition_in_type_definition347 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_array_type_definition_in_type_definition353 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_record_type_definition_in_type_definition359 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_derived_type_definition_in_type_definition368 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_SUBTYPE_in_subtype_declaration379 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_subtype_declaration382 = new BitSet(new ulong[]{0x0000001000000000UL});
    public static readonly BitSet FOLLOW_IS_in_subtype_declaration384 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_indication_in_subtype_declaration387 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_subtype_declaration389 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_subtype_mark400 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_subtype_indication410 = new BitSet(new ulong[]{0x0010400000000002UL});
    public static readonly BitSet FOLLOW_constraint_in_subtype_indication413 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_scalar_constraint_in_constraint426 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_range_constraint_in_scalar_constraint443 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_digits_constraint_in_scalar_constraint449 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_identifier_list_in_number_declaration462 = new BitSet(new ulong[]{0x0000008000000000UL});
    public static readonly BitSet FOLLOW_COLON_in_number_declaration464 = new BitSet(new ulong[]{0x0000010000000000UL});
    public static readonly BitSet FOLLOW_CONSTANT_in_number_declaration467 = new BitSet(new ulong[]{0x0000020000000000UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_number_declaration470 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_number_declaration473 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_number_declaration475 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ABSTRACT_in_derived_type_definition486 = new BitSet(new ulong[]{0x0000080000000000UL});
    public static readonly BitSet FOLLOW_NEW_in_derived_type_definition489 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_indication_in_derived_type_definition492 = new BitSet(new ulong[]{0x0000000000000002UL,0x0000000000000008UL});
    public static readonly BitSet FOLLOW_record_extension_part_in_derived_type_definition494 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_identifier_list_in_object_declaration520 = new BitSet(new ulong[]{0x0000008000000000UL});
    public static readonly BitSet FOLLOW_COLON_in_object_declaration522 = new BitSet(new ulong[]{0x0040110400000000UL});
    public static readonly BitSet FOLLOW_ALIASED_in_object_declaration524 = new BitSet(new ulong[]{0x0040010400000000UL});
    public static readonly BitSet FOLLOW_CONSTANT_in_object_declaration527 = new BitSet(new ulong[]{0x0040000400000000UL});
    public static readonly BitSet FOLLOW_subtype_indication_in_object_declaration537 = new BitSet(new ulong[]{0x0000022000000000UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_object_declaration541 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_object_declaration543 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_array_type_definition_in_object_declaration569 = new BitSet(new ulong[]{0x0000022000000000UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_object_declaration573 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_object_declaration575 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_object_declaration599 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_defining_identifier_list618 = new BitSet(new ulong[]{0x0000200000000002UL});
    public static readonly BitSet FOLLOW_COMMA_in_defining_identifier_list624 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_defining_identifier_list630 = new BitSet(new ulong[]{0x0000200000000002UL});
    public static readonly BitSet FOLLOW_RANGE_in_range_constraint660 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_range_in_range_constraint663 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_simple_expression_in_range678 = new BitSet(new ulong[]{0x0000800000000000UL});
    public static readonly BitSet FOLLOW_DOTDOT_in_range680 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_range682 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_enumeration_type_definition705 = new BitSet(new ulong[]{0x0004000400000000UL});
    public static readonly BitSet FOLLOW_enumeration_literal_specification_in_enumeration_type_definition707 = new BitSet(new ulong[]{0x0002200000000000UL});
    public static readonly BitSet FOLLOW_COMMA_in_enumeration_type_definition711 = new BitSet(new ulong[]{0x0004000400000000UL});
    public static readonly BitSet FOLLOW_enumeration_literal_specification_in_enumeration_type_definition713 = new BitSet(new ulong[]{0x0002200000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_enumeration_type_definition718 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_enumeration_literal_specification741 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_character_literal_in_enumeration_literal_specification747 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CHARACTER_LITERAL_in_defining_character_literal758 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_signed_integer_type_definition_in_integer_type_definition770 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_modular_type_definition_in_integer_type_definition776 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_RANGE_in_signed_integer_type_definition787 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_signed_integer_type_definition790 = new BitSet(new ulong[]{0x0000800000000000UL});
    public static readonly BitSet FOLLOW_DOTDOT_in_signed_integer_type_definition792 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_signed_integer_type_definition795 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_MOD_in_modular_type_definition805 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_modular_type_definition808 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_floating_point_definition_in_real_type_definition820 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_fixed_point_definition_in_real_type_definition826 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_DIGITS_in_floating_point_definition837 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_floating_point_definition840 = new BitSet(new ulong[]{0x0000400000000002UL});
    public static readonly BitSet FOLLOW_real_range_specification_in_floating_point_definition842 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_RANGE_in_real_range_specification853 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_real_range_specification856 = new BitSet(new ulong[]{0x0000800000000000UL});
    public static readonly BitSet FOLLOW_DOTDOT_in_real_range_specification858 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_real_range_specification861 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ordinary_fixed_point_definition_in_fixed_point_definition880 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_decimal_fixed_point_definition_in_fixed_point_definition896 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_DELTA_in_ordinary_fixed_point_definition907 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_ordinary_fixed_point_definition909 = new BitSet(new ulong[]{0x0000400000000000UL});
    public static readonly BitSet FOLLOW_real_range_specification_in_ordinary_fixed_point_definition911 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_DELTA_in_decimal_fixed_point_definition933 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_decimal_fixed_point_definition935 = new BitSet(new ulong[]{0x0010000000000000UL});
    public static readonly BitSet FOLLOW_DIGITS_in_decimal_fixed_point_definition937 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_decimal_fixed_point_definition939 = new BitSet(new ulong[]{0x0000400000000002UL});
    public static readonly BitSet FOLLOW_real_range_specification_in_decimal_fixed_point_definition941 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_DIGITS_in_digits_constraint967 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_digits_constraint970 = new BitSet(new ulong[]{0x0000400000000002UL});
    public static readonly BitSet FOLLOW_range_constraint_in_digits_constraint972 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_unconstrained_array_definition_in_array_type_definition985 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_constrained_array_definition_in_array_type_definition991 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ARRAY_in_unconstrained_array_definition1004 = new BitSet(new ulong[]{0x0001000000000000UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_unconstrained_array_definition1006 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_index_subtype_definition_in_unconstrained_array_definition1008 = new BitSet(new ulong[]{0x0002200000000000UL});
    public static readonly BitSet FOLLOW_COMMA_in_unconstrained_array_definition1012 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_index_subtype_definition_in_unconstrained_array_definition1014 = new BitSet(new ulong[]{0x0002200000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_unconstrained_array_definition1019 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_OF_in_unconstrained_array_definition1021 = new BitSet(new ulong[]{0x0000100400000000UL});
    public static readonly BitSet FOLLOW_component_definition_in_unconstrained_array_definition1023 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_index_subtype_definition1052 = new BitSet(new ulong[]{0x0000400000000000UL});
    public static readonly BitSet FOLLOW_RANGE_in_index_subtype_definition1054 = new BitSet(new ulong[]{0x0100000000000000UL});
    public static readonly BitSet FOLLOW_BOX_in_index_subtype_definition1056 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ARRAY_in_constrained_array_definition1068 = new BitSet(new ulong[]{0x0001000000000000UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_constrained_array_definition1070 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_discrete_subtype_definition_in_constrained_array_definition1072 = new BitSet(new ulong[]{0x0002200000000000UL});
    public static readonly BitSet FOLLOW_COMMA_in_constrained_array_definition1076 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_discrete_subtype_definition_in_constrained_array_definition1078 = new BitSet(new ulong[]{0x0002200000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_constrained_array_definition1083 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_OF_in_constrained_array_definition1085 = new BitSet(new ulong[]{0x0000100400000000UL});
    public static readonly BitSet FOLLOW_component_definition_in_constrained_array_definition1087 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subtype_indication_in_discrete_subtype_definition1118 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_range_in_discrete_subtype_definition1125 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ALIASED_in_component_definition1136 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_indication_in_component_definition1139 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subtype_indication_in_discrete_range1156 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_range_in_discrete_range1172 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_known_discriminant_part1183 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_discriminant_specification_in_known_discriminant_part1186 = new BitSet(new ulong[]{0x0002002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_known_discriminant_part1190 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_discriminant_specification_in_known_discriminant_part1193 = new BitSet(new ulong[]{0x0002002000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_known_discriminant_part1197 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_identifier_list_in_discriminant_specification1223 = new BitSet(new ulong[]{0x0000008000000000UL});
    public static readonly BitSet FOLLOW_COLON_in_discriminant_specification1225 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_discriminant_specification1229 = new BitSet(new ulong[]{0x0000020000000002UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_discriminant_specification1236 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_default_expression_in_discriminant_specification1238 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expression_in_default_expression1275 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ABSTRACT_in_record_type_definition1288 = new BitSet(new ulong[]{0x0200000000000000UL});
    public static readonly BitSet FOLLOW_TAGGED_in_record_type_definition1291 = new BitSet(new ulong[]{0x2E00040000000000UL});
    public static readonly BitSet FOLLOW_LIMITED_in_record_type_definition1296 = new BitSet(new ulong[]{0x2E00040000000000UL});
    public static readonly BitSet FOLLOW_record_definition_in_record_type_definition1299 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_RECORD_in_record_definition1334 = new BitSet(new ulong[]{0x6000000400000000UL});
    public static readonly BitSet FOLLOW_component_list_in_record_definition1337 = new BitSet(new ulong[]{0x1000000000000000UL});
    public static readonly BitSet FOLLOW_END_in_record_definition1339 = new BitSet(new ulong[]{0x0800000000000000UL});
    public static readonly BitSet FOLLOW_RECORD_in_record_definition1342 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NULL_in_record_definition1349 = new BitSet(new ulong[]{0x0800000000000000UL});
    public static readonly BitSet FOLLOW_RECORD_in_record_definition1352 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_component_item_in_component_list1376 = new BitSet(new ulong[]{0x4000000400000000UL});
    public static readonly BitSet FOLLOW_variant_part_in_component_list1379 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_component_item_in_component_list1407 = new BitSet(new ulong[]{0x0000000400000002UL});
    public static readonly BitSet FOLLOW_NULL_in_component_list1438 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_component_list1441 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_component_declaration_in_component_item1453 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_identifier_list_in_component_declaration1478 = new BitSet(new ulong[]{0x0000008000000000UL});
    public static readonly BitSet FOLLOW_COLON_in_component_declaration1480 = new BitSet(new ulong[]{0x0000100400000000UL});
    public static readonly BitSet FOLLOW_component_definition_in_component_declaration1482 = new BitSet(new ulong[]{0x0000022000000000UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_component_declaration1486 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_default_expression_in_component_declaration1488 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_component_declaration1493 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CASE_in_variant_part1529 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_direct_name_in_variant_part1532 = new BitSet(new ulong[]{0x0000001000000000UL});
    public static readonly BitSet FOLLOW_IS_in_variant_part1534 = new BitSet(new ulong[]{0x8000000000000000UL});
    public static readonly BitSet FOLLOW_variant_in_variant_part1554 = new BitSet(new ulong[]{0x8000000000000000UL});
    public static readonly BitSet FOLLOW_other_variant_in_variant_part1557 = new BitSet(new ulong[]{0x1000000000000000UL});
    public static readonly BitSet FOLLOW_variant_in_variant_part1572 = new BitSet(new ulong[]{0x9000000000000000UL});
    public static readonly BitSet FOLLOW_END_in_variant_part1579 = new BitSet(new ulong[]{0x4000000000000000UL});
    public static readonly BitSet FOLLOW_CASE_in_variant_part1582 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_variant_part1585 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_WHEN_in_variant1597 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_discrete_choice_list_in_variant1600 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000001UL});
    public static readonly BitSet FOLLOW_ASSOCIATION_in_variant1602 = new BitSet(new ulong[]{0x6000000400000000UL});
    public static readonly BitSet FOLLOW_component_list_in_variant1605 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_WHEN_in_other_variant1616 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000002UL});
    public static readonly BitSet FOLLOW_OTHERS_in_other_variant1619 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000001UL});
    public static readonly BitSet FOLLOW_ASSOCIATION_in_other_variant1622 = new BitSet(new ulong[]{0x6000000400000000UL});
    public static readonly BitSet FOLLOW_component_list_in_other_variant1625 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_discrete_choice_in_discrete_choice_list1635 = new BitSet(new ulong[]{0x0000000000000002UL,0x0000000000000004UL});
    public static readonly BitSet FOLLOW_CHOICE_in_discrete_choice_list1639 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_discrete_choice_in_discrete_choice_list1642 = new BitSet(new ulong[]{0x0000000000000002UL,0x0000000000000004UL});
    public static readonly BitSet FOLLOW_discrete_range_in_discrete_choice1664 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expression_in_discrete_choice1676 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_WITH_in_record_extension_part1687 = new BitSet(new ulong[]{0x2E00040000000000UL});
    public static readonly BitSet FOLLOW_record_definition_in_record_extension_part1690 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ACCESS_in_access_definition1701 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_access_definition1703 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_declarative_item_in_declarative_part1713 = new BitSet(new ulong[]{0x0000000C00000002UL,0x0000180000000000UL});
    public static readonly BitSet FOLLOW_basic_declarative_item_in_declarative_item1744 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_body_in_declarative_item1768 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_basic_declaration_in_basic_declarative_item1793 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_proper_body_in_body1803 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subprogram_body_in_proper_body1813 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_function_call_in_name1828 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_type_conversion_in_name1838 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_slice_in_name1875 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_indexed_component_in_name1900 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_explicit_dereference_in_name1922 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_selected_component_in_name1943 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_direct_name_in_name1965 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_attribute_reference_in_name2008 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CHARACTER_LITERAL_in_name2024 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_type_conversion_in_name2051 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_indexed_component_in_name2067 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_slice_in_name2081 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_explicit_dereference_in_name2101 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_selected_component_in_name2113 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_function_call_in_name2127 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_direct_name2151 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_direct_name_in_explicit_dereference2161 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000020UL});
    public static readonly BitSet FOLLOW_DOT_in_explicit_dereference2163 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000040UL});
    public static readonly BitSet FOLLOW_ALL_in_explicit_dereference2165 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_direct_name_in_indexed_component2186 = new BitSet(new ulong[]{0x0001000000000000UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_indexed_component2188 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_indexed_component2190 = new BitSet(new ulong[]{0x0002200000000000UL});
    public static readonly BitSet FOLLOW_COMMA_in_indexed_component2194 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_indexed_component2196 = new BitSet(new ulong[]{0x0002200000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_indexed_component2201 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_direct_name_in_slice2224 = new BitSet(new ulong[]{0x0001000000000000UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_slice2226 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_discrete_range_in_slice2228 = new BitSet(new ulong[]{0x0002000000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_slice2230 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_direct_name_in_selected_component2252 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000020UL});
    public static readonly BitSet FOLLOW_DOT_in_selected_component2254 = new BitSet(new ulong[]{0x0004000400000000UL,0x0000000000040000UL});
    public static readonly BitSet FOLLOW_selector_name_in_selected_component2256 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_selector_name2280 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CHARACTER_LITERAL_in_selector_name2286 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_operator_symbol_in_selector_name2292 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_direct_name_in_attribute_reference2303 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000080UL});
    public static readonly BitSet FOLLOW_APOSTROPHE_in_attribute_reference2305 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_attribute_designator_in_attribute_reference2307 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_attribute_designator2329 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_attribute_designator2333 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_attribute_designator2336 = new BitSet(new ulong[]{0x0002000000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_attribute_designator2338 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_relation_in_expression2356 = new BitSet(new ulong[]{0x0000000000000002UL,0x0000000000001500UL});
    public static readonly BitSet FOLLOW_AND_in_expression2367 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_relation_in_expression2369 = new BitSet(new ulong[]{0x0000000000000002UL,0x0000000000000100UL});
    public static readonly BitSet FOLLOW_AND_in_expression2395 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000200UL});
    public static readonly BitSet FOLLOW_THEN_in_expression2397 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_relation_in_expression2399 = new BitSet(new ulong[]{0x0000000000000002UL,0x0000000000000100UL});
    public static readonly BitSet FOLLOW_OR_in_expression2424 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_relation_in_expression2426 = new BitSet(new ulong[]{0x0000000000000002UL,0x0000000000000400UL});
    public static readonly BitSet FOLLOW_OR_in_expression2455 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000800UL});
    public static readonly BitSet FOLLOW_ELSE_in_expression2457 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_relation_in_expression2459 = new BitSet(new ulong[]{0x0000000000000002UL,0x0000000000000400UL});
    public static readonly BitSet FOLLOW_XOR_in_expression2484 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_relation_in_expression2486 = new BitSet(new ulong[]{0x0000000000000002UL,0x0000000000001000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2540 = new BitSet(new ulong[]{0x0000000000000002UL,0x0000000001F86000UL});
    public static readonly BitSet FOLLOW_relational_operator_in_relation2549 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_simple_expression_in_relation2551 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NOT_in_relation2570 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000004000UL});
    public static readonly BitSet FOLLOW_IN_in_relation2573 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_range_in_relation2591 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_relation2619 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_unary_adding_operator_in_simple_expression2665 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_term_in_simple_expression2668 = new BitSet(new ulong[]{0x0000000000000002UL,0x000000000E000000UL});
    public static readonly BitSet FOLLOW_binary_adding_operator_in_simple_expression2672 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_term_in_simple_expression2675 = new BitSet(new ulong[]{0x0000000000000002UL,0x000000000E000000UL});
    public static readonly BitSet FOLLOW_factor_in_term2688 = new BitSet(new ulong[]{0x0008000000000002UL,0x0000000070000000UL});
    public static readonly BitSet FOLLOW_multiplying_operator_in_term2692 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_factor_in_term2695 = new BitSet(new ulong[]{0x0008000000000002UL,0x0000000070000000UL});
    public static readonly BitSet FOLLOW_primary_in_factor2719 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000008000UL});
    public static readonly BitSet FOLLOW_POW_in_factor2721 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000000060000UL});
    public static readonly BitSet FOLLOW_primary_in_factor2724 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_primary_in_factor2735 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ABS_in_factor2746 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000000060000UL});
    public static readonly BitSet FOLLOW_primary_in_factor2749 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NOT_in_factor2760 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000000060000UL});
    public static readonly BitSet FOLLOW_primary_in_factor2763 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NUMERIC_LITERAL_in_primary2776 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NULL_in_primary2782 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_STRING_LITERAL_in_primary2788 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_name_in_primary2797 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_qualified_expression_in_primary2803 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_primary2811 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_primary2814 = new BitSet(new ulong[]{0x0002000000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_primary2816 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_set_in_relational_operator0 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_set_in_binary_adding_operator0 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_set_in_unary_adding_operator0 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_set_in_multiplying_operator0 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_direct_name_in_type_conversion2913 = new BitSet(new ulong[]{0x0001000000000000UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_type_conversion2915 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_type_conversion2917 = new BitSet(new ulong[]{0x0002000000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_type_conversion2919 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_qualified_expression2943 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000080UL});
    public static readonly BitSet FOLLOW_APOSTROPHE_in_qualified_expression2945 = new BitSet(new ulong[]{0x0001000000000000UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_qualified_expression2954 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_qualified_expression2956 = new BitSet(new ulong[]{0x0002000000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_qualified_expression2958 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_statement_in_sequence_of_statements2992 = new BitSet(new ulong[]{0x6004000400000002UL,0x000027BA80000000UL});
    public static readonly BitSet FOLLOW_label_in_statement3005 = new BitSet(new ulong[]{0x6004000400000000UL,0x000027BA80000000UL});
    public static readonly BitSet FOLLOW_simple_statement_in_statement3015 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_compound_statement_in_statement3035 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_assignment_statement_in_simple_statement3072 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_procedure_call_statement_in_simple_statement3085 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_return_statement_in_simple_statement3098 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_exit_statement_in_simple_statement3111 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_goto_statement_in_simple_statement3124 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_null_statement_in_simple_statement3137 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_if_statement_in_compound_statement3150 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_case_statement_in_compound_statement3156 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_loop_statement_in_compound_statement3162 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_block_statement_in_compound_statement3168 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NULL_in_null_statement3179 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_null_statement3182 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_LLABELBRACKET_in_label3193 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_label3195 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000100000000UL});
    public static readonly BitSet FOLLOW_RLABELBRACKET_in_label3197 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_statement_identifier3217 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_name_in_assignment_statement3227 = new BitSet(new ulong[]{0x0000020000000000UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_assignment_statement3229 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_assignment_statement3232 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_assignment_statement3234 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IF_in_if_statement3245 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_condition_in_if_statement3248 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000200UL});
    public static readonly BitSet FOLLOW_THEN_in_if_statement3250 = new BitSet(new ulong[]{0x6004000400000000UL,0x000027BA80000000UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_if_statement3253 = new BitSet(new ulong[]{0x1000000000000000UL,0x0000000400000800UL});
    public static readonly BitSet FOLLOW_ELSIF_in_if_statement3257 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_condition_in_if_statement3260 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000200UL});
    public static readonly BitSet FOLLOW_THEN_in_if_statement3262 = new BitSet(new ulong[]{0x6004000400000000UL,0x000027BA80000000UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_if_statement3265 = new BitSet(new ulong[]{0x1000000000000000UL,0x0000000400000800UL});
    public static readonly BitSet FOLLOW_ELSE_in_if_statement3272 = new BitSet(new ulong[]{0x6004000400000000UL,0x000027BA80000000UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_if_statement3274 = new BitSet(new ulong[]{0x1000000000000000UL});
    public static readonly BitSet FOLLOW_END_in_if_statement3279 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000200000000UL});
    public static readonly BitSet FOLLOW_IF_in_if_statement3282 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_if_statement3285 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expression_in_condition3296 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CASE_in_case_statement3308 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_case_statement3311 = new BitSet(new ulong[]{0x0000001000000000UL});
    public static readonly BitSet FOLLOW_IS_in_case_statement3313 = new BitSet(new ulong[]{0x8000000000000000UL});
    public static readonly BitSet FOLLOW_case_statement_alternative_in_case_statement3333 = new BitSet(new ulong[]{0x8000000000000000UL});
    public static readonly BitSet FOLLOW_other_case_statement_alternative_in_case_statement3336 = new BitSet(new ulong[]{0x1000000000000000UL});
    public static readonly BitSet FOLLOW_case_statement_alternative_in_case_statement3360 = new BitSet(new ulong[]{0x9000000000000000UL});
    public static readonly BitSet FOLLOW_END_in_case_statement3367 = new BitSet(new ulong[]{0x4000000000000000UL});
    public static readonly BitSet FOLLOW_CASE_in_case_statement3370 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_case_statement3373 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_WHEN_in_case_statement_alternative3385 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_discrete_choice_list_in_case_statement_alternative3388 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000001UL});
    public static readonly BitSet FOLLOW_ASSOCIATION_in_case_statement_alternative3390 = new BitSet(new ulong[]{0x6004000400000000UL,0x000027BA80000000UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_case_statement_alternative3393 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_WHEN_in_other_case_statement_alternative3403 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000002UL});
    public static readonly BitSet FOLLOW_OTHERS_in_other_case_statement_alternative3406 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000001UL});
    public static readonly BitSet FOLLOW_ASSOCIATION_in_other_case_statement_alternative3409 = new BitSet(new ulong[]{0x6004000400000000UL,0x000027BA80000000UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_other_case_statement_alternative3412 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_loop_statement3437 = new BitSet(new ulong[]{0x0000008000000000UL});
    public static readonly BitSet FOLLOW_COLON_in_loop_statement3439 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000003800000000UL});
    public static readonly BitSet FOLLOW_iteration_scheme_in_loop_statement3445 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000800000000UL});
    public static readonly BitSet FOLLOW_LOOP_in_loop_statement3448 = new BitSet(new ulong[]{0x6004000400000000UL,0x000027BA80000000UL});
    public static readonly BitSet FOLLOW_handled_sequence_of_statements_in_loop_statement3451 = new BitSet(new ulong[]{0x1000000000000000UL});
    public static readonly BitSet FOLLOW_END_in_loop_statement3453 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000800000000UL});
    public static readonly BitSet FOLLOW_LOOP_in_loop_statement3456 = new BitSet(new ulong[]{0x0000002400000000UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_loop_statement3459 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_loop_statement3462 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_WHILE_in_iteration_scheme3475 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_condition_in_iteration_scheme3478 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_FOR_in_iteration_scheme3484 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_loop_parameter_specification_in_iteration_scheme3487 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_loop_parameter_specification3504 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000004000UL});
    public static readonly BitSet FOLLOW_IN_in_loop_parameter_specification3512 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000004006072000UL});
    public static readonly BitSet FOLLOW_REVERSE_in_loop_parameter_specification3515 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_discrete_subtype_definition_in_loop_parameter_specification3518 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_block_statement3546 = new BitSet(new ulong[]{0x0000008000000000UL});
    public static readonly BitSet FOLLOW_COLON_in_block_statement3548 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000018000000000UL});
    public static readonly BitSet FOLLOW_DECLARE_in_block_statement3555 = new BitSet(new ulong[]{0x0000000C00000000UL,0x0000180000000000UL});
    public static readonly BitSet FOLLOW_declarative_part_in_block_statement3557 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000010000000000UL});
    public static readonly BitSet FOLLOW_BEGIN_in_block_statement3562 = new BitSet(new ulong[]{0x6004000400000000UL,0x000027BA80000000UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_block_statement3564 = new BitSet(new ulong[]{0x1000000000000000UL});
    public static readonly BitSet FOLLOW_END_in_block_statement3566 = new BitSet(new ulong[]{0x0000002400000000UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_block_statement3568 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_block_statement3571 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_EXIT_in_exit_statement3604 = new BitSet(new ulong[]{0x8000002400000000UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_exit_statement3607 = new BitSet(new ulong[]{0x8000002000000000UL});
    public static readonly BitSet FOLLOW_WHEN_in_exit_statement3612 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_condition_in_exit_statement3615 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_exit_statement3620 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_GOTO_in_goto_statement3631 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_statement_identifier_in_goto_statement3634 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_goto_statement3636 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subprogram_specification_in_subprogram_declaration3647 = new BitSet(new ulong[]{0x0000003000000000UL});
    public static readonly BitSet FOLLOW_IS_in_subprogram_declaration3652 = new BitSet(new ulong[]{0x0000040000000000UL});
    public static readonly BitSet FOLLOW_ABSTRACT_in_subprogram_declaration3655 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_subprogram_declaration3660 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_PROCEDURE_in_subprogram_specification3674 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_defining_program_unit_name_in_subprogram_specification3681 = new BitSet(new ulong[]{0x0001000000000000UL});
    public static readonly BitSet FOLLOW_parameter_profile_in_subprogram_specification3684 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_FUNCTION_in_subprogram_specification3695 = new BitSet(new ulong[]{0x0004000400000000UL,0x0000000000040000UL});
    public static readonly BitSet FOLLOW_defining_designator_in_subprogram_specification3702 = new BitSet(new ulong[]{0x0001000000000000UL,0x0000200000000000UL});
    public static readonly BitSet FOLLOW_parameter_and_result_profile_in_subprogram_specification3704 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_designator3722 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_operator_symbol_in_designator3728 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_program_unit_name_in_defining_designator3741 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_operator_symbol_in_defining_designator3747 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_identifier_in_defining_program_unit_name3758 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_STRING_LITERAL_in_operator_symbol3768 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_operator_symbol_in_defining_operator_symbol3778 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_formal_part_in_parameter_profile3789 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_formal_part_in_parameter_and_result_profile3802 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000200000000000UL});
    public static readonly BitSet FOLLOW_RETURN_in_parameter_and_result_profile3806 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_parameter_and_result_profile3808 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_formal_part3819 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_parameter_specification_in_formal_part3822 = new BitSet(new ulong[]{0x0002002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_formal_part3827 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_parameter_specification_in_formal_part3830 = new BitSet(new ulong[]{0x0002002000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_formal_part3836 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_defining_identifier_list_in_parameter_specification3863 = new BitSet(new ulong[]{0x0000008000000000UL});
    public static readonly BitSet FOLLOW_COLON_in_parameter_specification3865 = new BitSet(new ulong[]{0x0000000400000000UL,0x0000400000004000UL});
    public static readonly BitSet FOLLOW_mode_in_parameter_specification3869 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_subtype_mark_in_parameter_specification3871 = new BitSet(new ulong[]{0x0000020000000002UL});
    public static readonly BitSet FOLLOW_ASSIGN_in_parameter_specification3879 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_default_expression_in_parameter_specification3881 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IN_in_mode3933 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IN_in_mode3939 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000400000000000UL});
    public static readonly BitSet FOLLOW_OUT_in_mode3941 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_OUT_in_mode3952 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_subprogram_specification_in_subprogram_body3976 = new BitSet(new ulong[]{0x0000001000000000UL});
    public static readonly BitSet FOLLOW_IS_in_subprogram_body3979 = new BitSet(new ulong[]{0x0000000C00000000UL,0x0000180000000000UL});
    public static readonly BitSet FOLLOW_declarative_part_in_subprogram_body3982 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000010000000000UL});
    public static readonly BitSet FOLLOW_BEGIN_in_subprogram_body3984 = new BitSet(new ulong[]{0x6004000400000000UL,0x000027BA80000000UL});
    public static readonly BitSet FOLLOW_handled_sequence_of_statements_in_subprogram_body3987 = new BitSet(new ulong[]{0x1000000000000000UL});
    public static readonly BitSet FOLLOW_END_in_subprogram_body3989 = new BitSet(new ulong[]{0x0004002400000000UL,0x0000000000040000UL});
    public static readonly BitSet FOLLOW_designator_in_subprogram_body3992 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_subprogram_body3995 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_sequence_of_statements_in_handled_sequence_of_statements4006 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_direct_name_in_procedure_call_statement4016 = new BitSet(new ulong[]{0x0001002000000000UL});
    public static readonly BitSet FOLLOW_actual_parameter_part_in_procedure_call_statement4018 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_procedure_call_statement4021 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_direct_name_in_function_call4045 = new BitSet(new ulong[]{0x0001000000000002UL});
    public static readonly BitSet FOLLOW_actual_parameter_part_in_function_call4047 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_LPARANTHESIS_in_actual_parameter_part4071 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_parameter_association_in_actual_parameter_part4074 = new BitSet(new ulong[]{0x0002200000000000UL});
    public static readonly BitSet FOLLOW_COMMA_in_actual_parameter_part4078 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_parameter_association_in_actual_parameter_part4081 = new BitSet(new ulong[]{0x0002200000000000UL});
    public static readonly BitSet FOLLOW_RPARANTHESIS_in_actual_parameter_part4086 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_selector_name_in_parameter_association4099 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000000001UL});
    public static readonly BitSet FOLLOW_ASSOCIATION_in_parameter_association4101 = new BitSet(new ulong[]{0x2005000400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_explicit_actual_parameter_in_parameter_association4106 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expression_in_explicit_actual_parameter4129 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_RETURN_in_return_statement4139 = new BitSet(new ulong[]{0x2005002400000000UL,0x0000000006072000UL});
    public static readonly BitSet FOLLOW_expression_in_return_statement4142 = new BitSet(new ulong[]{0x0000002000000000UL});
    public static readonly BitSet FOLLOW_SEMICOLON_in_return_statement4145 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ordinary_fixed_point_definition_in_synpred1_Ada95875 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_component_item_in_synpred2_Ada951368 = new BitSet(new ulong[]{0x4000000400000000UL});
    public static readonly BitSet FOLLOW_variant_part_in_synpred2_Ada951371 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_variant_in_synpred3_Ada951546 = new BitSet(new ulong[]{0x8000000000000000UL});
    public static readonly BitSet FOLLOW_other_variant_in_synpred3_Ada951549 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_discrete_range_in_synpred4_Ada951659 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_basic_declarative_item_in_synpred5_Ada951739 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_slice_in_synpred6_Ada951875 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_indexed_component_in_synpred7_Ada951900 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_explicit_dereference_in_synpred8_Ada951922 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_selected_component_in_synpred9_Ada951943 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_type_conversion_in_synpred10_Ada952051 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_indexed_component_in_synpred11_Ada952067 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_slice_in_synpred12_Ada952081 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_explicit_dereference_in_synpred13_Ada952101 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_selected_component_in_synpred14_Ada952113 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_range_in_synpred15_Ada952586 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_primary_in_synpred16_Ada952712 = new BitSet(new ulong[]{0x0000000000000000UL,0x0000000000008000UL});
    public static readonly BitSet FOLLOW_POW_in_synpred16_Ada952714 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_assignment_statement_in_synpred17_Ada953067 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_case_statement_alternative_in_synpred18_Ada953325 = new BitSet(new ulong[]{0x8000000000000000UL});
    public static readonly BitSet FOLLOW_other_case_statement_alternative_in_synpred18_Ada953328 = new BitSet(new ulong[]{0x0000000000000002UL});

}
}