// $ANTLR 3.2 Sep 23, 2009 12:02:23 Ada95.g 2009-12-23 07:03:04

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


using System;
using Antlr.Runtime;
using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;


namespace  Ada95 
{
public partial class Ada95Lexer : Lexer {
    public const int FUNCTION = 108;
    public const int EXPONENT = 116;
    public const int WHILE = 100;
    public const int MOD = 51;
    public const int UNCONSTRAINED_ARRAY = 8;
    public const int CASE = 62;
    public const int NEW = 43;
    public const int NOT = 77;
    public const int IN_SUBTYPE = 32;
    public const int SUBTYPE = 38;
    public const int EOF = -1;
    public const int BASED_LITERAL = 114;
    public const int TYPE = 35;
    public const int ATTRIBUTE_REFERENCE = 22;
    public const int STRING_LITERAL = 82;
    public const int GREATER = 87;
    public const int POW = 79;
    public const int ENUMERATION = 10;
    public const int LOOP = 99;
    public const int BEGIN = 104;
    public const int PARAMETER = 24;
    public const int LESS = 85;
    public const int RETURN = 109;
    public const int EXPLICIT_DEREFERENCE = 17;
    public const int BASE = 117;
    public const int ALIASED = 44;
    public const int BODY = 13;
    public const int GEQ = 88;
    public const int APOSTROPHE = 71;
    public const int EQ = 83;
    public const int GOTO = 106;
    public const int RLABELBRACKET = 96;
    public const int COMMENT = 120;
    public const int ARRAY = 54;
    public const int EXIT = 105;
    public const int RECORD = 59;
    public const int CONSTRAINED_ARRAY = 9;
    public const int CONCAT = 91;
    public const int NULL = 61;
    public const int ELSE = 75;
    public const int CHARACTER_LITERAL = 50;
    public const int DELTA = 53;
    public const int SEMICOLON = 37;
    public const int INDEXED_COMPONENT = 18;
    public const int MULT = 92;
    public const int COMPONENT_LIST = 28;
    public const int OF = 55;
    public const int ABS = 80;
    public const int WS = 121;
    public const int OUT = 110;
    public const int EXTENDED_DIGIT = 119;
    public const int COMPOUND_STATEMENT = 16;
    public const int OR = 74;
    public const int CONSTANT = 40;
    public const int NUMERAL = 115;
    public const int ELSIF = 98;
    public const int REVERSE = 102;
    public const int END = 60;
    public const int DECIMAL_LITERAL = 113;
    public const int OTHERS = 65;
    public const int LIMITED = 58;
    public const int IDENTIFIER_LETTER = 111;
    public const int NUMERIC_LITERAL = 81;
    public const int DIGITS = 52;
    public const int FOR = 101;
    public const int DISCRIMINANT_SPECIFICATION = 25;
    public const int DOTDOT = 47;
    public const int ABSTRACT = 42;
    public const int QUALIFIED_BY_EXPRESSION = 26;
    public const int SLICE = 19;
    public const int ID_LIST = 6;
    public const int AND = 72;
    public const int IF = 97;
    public const int SELECTED_COMPONENT = 20;
    public const int BOX = 56;
    public const int ORDINARY_FIXED = 4;
    public const int THEN = 73;
    public const int IN = 78;
    public const int RPARANTHESIS = 49;
    public const int OBJECT = 21;
    public const int COMMA = 45;
    public const int IS = 36;
    public const int IDENTIFIER = 34;
    public const int DECIMAL_FIXED = 5;
    public const int ALL = 70;
    public const int ACCESS = 68;
    public const int BASED_NUMERAL = 118;
    public const int PLUS = 89;
    public const int DIGIT = 112;
    public const int IN_RANGE = 31;
    public const int DOT = 69;
    public const int COMPONENT = 23;
    public const int EXPRESSION = 29;
    public const int CHOICE = 66;
    public const int DISCRETE_RANGE = 30;
    public const int WITH = 67;
    public const int LLABELBRACKET = 95;
    public const int XOR = 76;
    public const int ITEM = 12;
    public const int SIMPLE_STATEMENT = 15;
    public const int RANGE = 46;
    public const int ARRAY_OBJECT = 7;
    public const int MINUS = 90;
    public const int REM = 94;
    public const int PROCEDURE = 107;
    public const int REF = 11;
    public const int COLON = 39;
    public const int LPARANTHESIS = 48;
    public const int NEQ = 84;
    public const int LABEL = 33;
    public const int WHEN = 63;
    public const int TAGGED = 57;
    public const int BLOCK = 14;
    public const int ASSIGN = 41;
    public const int DECLARE = 103;
    public const int QUALIFIED_BY_AGREGATE = 27;
    public const int DIV = 93;
    public const int ASSOCIATION = 64;
    public const int LEQ = 86;

    // delegates
    // delegators

    public Ada95Lexer() 
    {
		InitializeCyclicDFAs();
    }
    public Ada95Lexer(ICharStream input)
		: this(input, null) {
    }
    public Ada95Lexer(ICharStream input, RecognizerSharedState state)
		: base(input, state) {
		InitializeCyclicDFAs(); 

    }
    
    override public string GrammarFileName
    {
    	get { return "Ada95.g";} 
    }

    // $ANTLR start "DELTA"
    public void mDELTA() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = DELTA;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:801:9: ( 'delta' )
            // Ada95.g:801:11: 'delta'
            {
            	Match("delta"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "DELTA"

    // $ANTLR start "WITH"
    public void mWITH() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = WITH;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:802:8: ( 'with' )
            // Ada95.g:802:10: 'with'
            {
            	Match("with"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "WITH"

    // $ANTLR start "NEW"
    public void mNEW() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NEW;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:803:8: ( 'new' )
            // Ada95.g:803:10: 'new'
            {
            	Match("new"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NEW"

    // $ANTLR start "ARRAY"
    public void mARRAY() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ARRAY;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:804:9: ( 'array' )
            // Ada95.g:804:11: 'array'
            {
            	Match("array"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ARRAY"

    // $ANTLR start "OF"
    public void mOF() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = OF;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:805:7: ( 'of' )
            // Ada95.g:805:9: 'of'
            {
            	Match("of"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "OF"

    // $ANTLR start "CONSTANT"
    public void mCONSTANT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = CONSTANT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:806:11: ( 'constant' )
            // Ada95.g:806:13: 'constant'
            {
            	Match("constant"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "CONSTANT"

    // $ANTLR start "ALIASED"
    public void mALIASED() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ALIASED;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:807:11: ( 'aliased' )
            // Ada95.g:807:13: 'aliased'
            {
            	Match("aliased"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ALIASED"

    // $ANTLR start "IF"
    public void mIF() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = IF;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:808:7: ( 'if' )
            // Ada95.g:808:9: 'if'
            {
            	Match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "IF"

    // $ANTLR start "ELSIF"
    public void mELSIF() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ELSIF;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:809:9: ( 'elsif' )
            // Ada95.g:809:11: 'elsif'
            {
            	Match("elsif"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ELSIF"

    // $ANTLR start "WHILE"
    public void mWHILE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = WHILE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:810:9: ( 'while' )
            // Ada95.g:810:11: 'while'
            {
            	Match("while"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "WHILE"

    // $ANTLR start "FOR"
    public void mFOR() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = FOR;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:811:8: ( 'for' )
            // Ada95.g:811:10: 'for'
            {
            	Match("for"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "FOR"

    // $ANTLR start "LOOP"
    public void mLOOP() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LOOP;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:812:8: ( 'loop' )
            // Ada95.g:812:10: 'loop'
            {
            	Match("loop"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LOOP"

    // $ANTLR start "REVERSE"
    public void mREVERSE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = REVERSE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:813:11: ( 'reverse' )
            // Ada95.g:813:13: 'reverse'
            {
            	Match("reverse"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "REVERSE"

    // $ANTLR start "GOTO"
    public void mGOTO() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = GOTO;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:814:8: ( 'goto' )
            // Ada95.g:814:10: 'goto'
            {
            	Match("goto"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "GOTO"

    // $ANTLR start "EXIT"
    public void mEXIT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = EXIT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:815:8: ( 'exit' )
            // Ada95.g:815:10: 'exit'
            {
            	Match("exit"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "EXIT"

    // $ANTLR start "DECLARE"
    public void mDECLARE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = DECLARE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:816:11: ( 'declare' )
            // Ada95.g:816:13: 'declare'
            {
            	Match("declare"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "DECLARE"

    // $ANTLR start "PROCEDURE"
    public void mPROCEDURE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = PROCEDURE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:817:12: ( 'procedure' )
            // Ada95.g:817:14: 'procedure'
            {
            	Match("procedure"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "PROCEDURE"

    // $ANTLR start "FUNCTION"
    public void mFUNCTION() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = FUNCTION;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:818:11: ( 'function' )
            // Ada95.g:818:13: 'function'
            {
            	Match("function"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "FUNCTION"

    // $ANTLR start "ABSTRACT"
    public void mABSTRACT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ABSTRACT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:819:11: ( 'abstract' )
            // Ada95.g:819:13: 'abstract'
            {
            	Match("abstract"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ABSTRACT"

    // $ANTLR start "TAGGED"
    public void mTAGGED() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = TAGGED;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:820:10: ( 'tagged' )
            // Ada95.g:820:12: 'tagged'
            {
            	Match("tagged"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "TAGGED"

    // $ANTLR start "LIMITED"
    public void mLIMITED() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LIMITED;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:821:11: ( 'limited' )
            // Ada95.g:821:13: 'limited'
            {
            	Match("limited"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LIMITED"

    // $ANTLR start "ACCESS"
    public void mACCESS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ACCESS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:822:10: ( 'access' )
            // Ada95.g:822:12: 'access'
            {
            	Match("access"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ACCESS"

    // $ANTLR start "ALL"
    public void mALL() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ALL;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:823:8: ( 'all' )
            // Ada95.g:823:10: 'all'
            {
            	Match("all"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ALL"

    // $ANTLR start "RETURN"
    public void mRETURN() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = RETURN;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:824:10: ( 'return' )
            // Ada95.g:824:12: 'return'
            {
            	Match("return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "RETURN"

    // $ANTLR start "DIGITS"
    public void mDIGITS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = DIGITS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:825:10: ( 'digits' )
            // Ada95.g:825:12: 'digits'
            {
            	Match("digits"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "DIGITS"

    // $ANTLR start "RANGE"
    public void mRANGE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = RANGE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:826:9: ( 'range' )
            // Ada95.g:826:11: 'range'
            {
            	Match("range"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "RANGE"

    // $ANTLR start "TYPE"
    public void mTYPE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = TYPE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:827:8: ( 'type' )
            // Ada95.g:827:10: 'type'
            {
            	Match("type"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "TYPE"

    // $ANTLR start "SUBTYPE"
    public void mSUBTYPE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = SUBTYPE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:828:11: ( 'subtype' )
            // Ada95.g:828:13: 'subtype'
            {
            	Match("subtype"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "SUBTYPE"

    // $ANTLR start "IS"
    public void mIS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = IS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:829:7: ( 'is' )
            // Ada95.g:829:9: 'is'
            {
            	Match("is"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "IS"

    // $ANTLR start "RECORD"
    public void mRECORD() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = RECORD;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:830:10: ( 'record' )
            // Ada95.g:830:12: 'record'
            {
            	Match("record"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "RECORD"

    // $ANTLR start "CASE"
    public void mCASE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = CASE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:831:8: ( 'case' )
            // Ada95.g:831:10: 'case'
            {
            	Match("case"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "CASE"

    // $ANTLR start "WHEN"
    public void mWHEN() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = WHEN;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:832:8: ( 'when' )
            // Ada95.g:832:10: 'when'
            {
            	Match("when"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "WHEN"

    // $ANTLR start "OTHERS"
    public void mOTHERS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = OTHERS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:833:10: ( 'others' )
            // Ada95.g:833:12: 'others'
            {
            	Match("others"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "OTHERS"

    // $ANTLR start "BEGIN"
    public void mBEGIN() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = BEGIN;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:834:9: ( 'begin' )
            // Ada95.g:834:11: 'begin'
            {
            	Match("begin"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "BEGIN"

    // $ANTLR start "END"
    public void mEND() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = END;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:835:8: ( 'end' )
            // Ada95.g:835:10: 'end'
            {
            	Match("end"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "END"

    // $ANTLR start "NULL"
    public void mNULL() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NULL;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:836:8: ( 'null' )
            // Ada95.g:836:10: 'null'
            {
            	Match("null"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NULL"

    // $ANTLR start "IN"
    public void mIN() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = IN;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:837:7: ( 'in' )
            // Ada95.g:837:9: 'in'
            {
            	Match("in"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "IN"

    // $ANTLR start "OUT"
    public void mOUT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = OUT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:838:8: ( 'out' )
            // Ada95.g:838:10: 'out'
            {
            	Match("out"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "OUT"

    // $ANTLR start "NOT"
    public void mNOT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NOT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:839:8: ( 'not' )
            // Ada95.g:839:10: 'not'
            {
            	Match("not"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NOT"

    // $ANTLR start "AND"
    public void mAND() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = AND;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:840:8: ( 'and' )
            // Ada95.g:840:10: 'and'
            {
            	Match("and"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "OR"
    public void mOR() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = OR;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:841:7: ( 'or' )
            // Ada95.g:841:9: 'or'
            {
            	Match("or"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "XOR"
    public void mXOR() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = XOR;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:842:8: ( 'xor' )
            // Ada95.g:842:10: 'xor'
            {
            	Match("xor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "XOR"

    // $ANTLR start "THEN"
    public void mTHEN() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = THEN;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:843:8: ( 'then' )
            // Ada95.g:843:10: 'then'
            {
            	Match("then"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "THEN"

    // $ANTLR start "ELSE"
    public void mELSE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ELSE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:844:8: ( 'else' )
            // Ada95.g:844:10: 'else'
            {
            	Match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ELSE"

    // $ANTLR start "CHOICE"
    public void mCHOICE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = CHOICE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:845:10: ( '|' )
            // Ada95.g:845:12: '|'
            {
            	Match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "CHOICE"

    // $ANTLR start "POW"
    public void mPOW() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = POW;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:846:8: ( '**' )
            // Ada95.g:846:10: '**'
            {
            	Match("**"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "POW"

    // $ANTLR start "MULT"
    public void mMULT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = MULT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:847:8: ( '*' )
            // Ada95.g:847:10: '*'
            {
            	Match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "MULT"

    // $ANTLR start "DIV"
    public void mDIV() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = DIV;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:848:8: ( '/' )
            // Ada95.g:848:10: '/'
            {
            	Match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "DIV"

    // $ANTLR start "ABS"
    public void mABS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ABS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:849:8: ( 'abs' )
            // Ada95.g:849:10: 'abs'
            {
            	Match("abs"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ABS"

    // $ANTLR start "MOD"
    public void mMOD() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = MOD;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:850:8: ( 'mod' )
            // Ada95.g:850:10: 'mod'
            {
            	Match("mod"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "MOD"

    // $ANTLR start "REM"
    public void mREM() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = REM;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:851:8: ( 'rem' )
            // Ada95.g:851:10: 'rem'
            {
            	Match("rem"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "REM"

    // $ANTLR start "LLABELBRACKET"
    public void mLLABELBRACKET() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LLABELBRACKET;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:852:15: ( '<<' )
            // Ada95.g:852:17: '<<'
            {
            	Match("<<"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LLABELBRACKET"

    // $ANTLR start "RLABELBRACKET"
    public void mRLABELBRACKET() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = RLABELBRACKET;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:853:15: ( '>>' )
            // Ada95.g:853:17: '>>'
            {
            	Match(">>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "RLABELBRACKET"

    // $ANTLR start "SEMICOLON"
    public void mSEMICOLON() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = SEMICOLON;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:854:12: ( ';' )
            // Ada95.g:854:14: ';'
            {
            	Match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "SEMICOLON"

    // $ANTLR start "ASSOCIATION"
    public void mASSOCIATION() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ASSOCIATION;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:855:14: ( '=>' )
            // Ada95.g:855:16: '=>'
            {
            	Match("=>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ASSOCIATION"

    // $ANTLR start "COLON"
    public void mCOLON() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = COLON;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:856:9: ( ':' )
            // Ada95.g:856:11: ':'
            {
            	Match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "COLON"

    // $ANTLR start "COMMA"
    public void mCOMMA() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = COMMA;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:857:9: ( ',' )
            // Ada95.g:857:11: ','
            {
            	Match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "COMMA"

    // $ANTLR start "APOSTROPHE"
    public void mAPOSTROPHE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = APOSTROPHE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:858:13: ( '\\'' )
            // Ada95.g:858:15: '\\''
            {
            	Match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "APOSTROPHE"

    // $ANTLR start "DOT"
    public void mDOT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = DOT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:859:8: ( '.' )
            // Ada95.g:859:10: '.'
            {
            	Match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "DOT"

    // $ANTLR start "DOTDOT"
    public void mDOTDOT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = DOTDOT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:860:10: ( '..' )
            // Ada95.g:860:12: '..'
            {
            	Match(".."); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "DOTDOT"

    // $ANTLR start "ASSIGN"
    public void mASSIGN() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = ASSIGN;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:861:10: ( ':=' )
            // Ada95.g:861:12: ':='
            {
            	Match(":="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ASSIGN"

    // $ANTLR start "LPARANTHESIS"
    public void mLPARANTHESIS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LPARANTHESIS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:862:14: ( '(' )
            // Ada95.g:862:16: '('
            {
            	Match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LPARANTHESIS"

    // $ANTLR start "RPARANTHESIS"
    public void mRPARANTHESIS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = RPARANTHESIS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:863:14: ( ')' )
            // Ada95.g:863:16: ')'
            {
            	Match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "RPARANTHESIS"

    // $ANTLR start "BOX"
    public void mBOX() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = BOX;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:864:8: ( '<>' )
            // Ada95.g:864:10: '<>'
            {
            	Match("<>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "BOX"

    // $ANTLR start "EQ"
    public void mEQ() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = EQ;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:865:7: ( '=' )
            // Ada95.g:865:9: '='
            {
            	Match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "EQ"

    // $ANTLR start "NEQ"
    public void mNEQ() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NEQ;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:866:8: ( '/=' )
            // Ada95.g:866:10: '/='
            {
            	Match("/="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NEQ"

    // $ANTLR start "LESS"
    public void mLESS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LESS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:867:8: ( '<' )
            // Ada95.g:867:10: '<'
            {
            	Match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LESS"

    // $ANTLR start "LEQ"
    public void mLEQ() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LEQ;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:868:8: ( '<=' )
            // Ada95.g:868:10: '<='
            {
            	Match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LEQ"

    // $ANTLR start "GREATER"
    public void mGREATER() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = GREATER;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:869:11: ( '>' )
            // Ada95.g:869:13: '>'
            {
            	Match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "GREATER"

    // $ANTLR start "GEQ"
    public void mGEQ() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = GEQ;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:870:8: ( '>=' )
            // Ada95.g:870:10: '>='
            {
            	Match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "GEQ"

    // $ANTLR start "PLUS"
    public void mPLUS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = PLUS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:871:8: ( '+' )
            // Ada95.g:871:10: '+'
            {
            	Match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "PLUS"

    // $ANTLR start "MINUS"
    public void mMINUS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = MINUS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:872:9: ( '-' )
            // Ada95.g:872:11: '-'
            {
            	Match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "MINUS"

    // $ANTLR start "CONCAT"
    public void mCONCAT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = CONCAT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:873:10: ( '&' )
            // Ada95.g:873:12: '&'
            {
            	Match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "CONCAT"

    // $ANTLR start "IDENTIFIER"
    public void mIDENTIFIER() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = IDENTIFIER;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:877:2: ( IDENTIFIER_LETTER ( ( '_' )? ( IDENTIFIER_LETTER | DIGIT ) )* )
            // Ada95.g:877:4: IDENTIFIER_LETTER ( ( '_' )? ( IDENTIFIER_LETTER | DIGIT ) )*
            {
            	mIDENTIFIER_LETTER(); 
            	// Ada95.g:877:22: ( ( '_' )? ( IDENTIFIER_LETTER | DIGIT ) )*
            	do 
            	{
            	    int alt2 = 2;
            	    int LA2_0 = input.LA(1);

            	    if ( ((LA2_0 >= '0' && LA2_0 <= '9') || (LA2_0 >= 'A' && LA2_0 <= 'Z') || LA2_0 == '_' || (LA2_0 >= 'a' && LA2_0 <= 'z')) )
            	    {
            	        alt2 = 1;
            	    }


            	    switch (alt2) 
            		{
            			case 1 :
            			    // Ada95.g:877:24: ( '_' )? ( IDENTIFIER_LETTER | DIGIT )
            			    {
            			    	// Ada95.g:877:24: ( '_' )?
            			    	int alt1 = 2;
            			    	int LA1_0 = input.LA(1);

            			    	if ( (LA1_0 == '_') )
            			    	{
            			    	    alt1 = 1;
            			    	}
            			    	switch (alt1) 
            			    	{
            			    	    case 1 :
            			    	        // Ada95.g:877:24: '_'
            			    	        {
            			    	        	Match('_'); 

            			    	        }
            			    	        break;

            			    	}

            			    	if ( (input.LA(1) >= '0' && input.LA(1) <= '9') || (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) 
            			    	{
            			    	    input.Consume();

            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    Recover(mse);
            			    	    throw mse;}


            			    }
            			    break;

            			default:
            			    goto loop2;
            	    }
            	} while (true);

            	loop2:
            		;	// Stops C# compiler whining that label 'loop2' has no statements


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "IDENTIFIER"

    // $ANTLR start "NUMERIC_LITERAL"
    public void mNUMERIC_LITERAL() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NUMERIC_LITERAL;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:880:2: ( DECIMAL_LITERAL | BASED_LITERAL )
            int alt3 = 2;
            alt3 = dfa3.Predict(input);
            switch (alt3) 
            {
                case 1 :
                    // Ada95.g:880:4: DECIMAL_LITERAL
                    {
                    	mDECIMAL_LITERAL(); 

                    }
                    break;
                case 2 :
                    // Ada95.g:880:22: BASED_LITERAL
                    {
                    	mBASED_LITERAL(); 

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NUMERIC_LITERAL"

    // $ANTLR start "DECIMAL_LITERAL"
    public void mDECIMAL_LITERAL() // throws RecognitionException [2]
    {
    		try
    		{
            // Ada95.g:884:2: ( NUMERAL ( '.' NUMERAL )? ( EXPONENT )? )
            // Ada95.g:884:4: NUMERAL ( '.' NUMERAL )? ( EXPONENT )?
            {
            	mNUMERAL(); 
            	// Ada95.g:884:12: ( '.' NUMERAL )?
            	int alt4 = 2;
            	int LA4_0 = input.LA(1);

            	if ( (LA4_0 == '.') )
            	{
            	    alt4 = 1;
            	}
            	switch (alt4) 
            	{
            	    case 1 :
            	        // Ada95.g:884:14: '.' NUMERAL
            	        {
            	        	Match('.'); 
            	        	mNUMERAL(); 

            	        }
            	        break;

            	}

            	// Ada95.g:884:29: ( EXPONENT )?
            	int alt5 = 2;
            	int LA5_0 = input.LA(1);

            	if ( (LA5_0 == 'E' || LA5_0 == 'e') )
            	{
            	    alt5 = 1;
            	}
            	switch (alt5) 
            	{
            	    case 1 :
            	        // Ada95.g:884:29: EXPONENT
            	        {
            	        	mEXPONENT(); 

            	        }
            	        break;

            	}


            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "DECIMAL_LITERAL"

    // $ANTLR start "BASED_LITERAL"
    public void mBASED_LITERAL() // throws RecognitionException [2]
    {
    		try
    		{
            // Ada95.g:888:2: ( BASE '#' BASED_NUMERAL ( '.' BASED_NUMERAL )? '#' ( EXPONENT )? )
            // Ada95.g:888:4: BASE '#' BASED_NUMERAL ( '.' BASED_NUMERAL )? '#' ( EXPONENT )?
            {
            	mBASE(); 
            	Match('#'); 
            	mBASED_NUMERAL(); 
            	// Ada95.g:888:26: ( '.' BASED_NUMERAL )?
            	int alt6 = 2;
            	int LA6_0 = input.LA(1);

            	if ( (LA6_0 == '.') )
            	{
            	    alt6 = 1;
            	}
            	switch (alt6) 
            	{
            	    case 1 :
            	        // Ada95.g:888:28: '.' BASED_NUMERAL
            	        {
            	        	Match('.'); 
            	        	mBASED_NUMERAL(); 

            	        }
            	        break;

            	}

            	Match('#'); 
            	// Ada95.g:888:53: ( EXPONENT )?
            	int alt7 = 2;
            	int LA7_0 = input.LA(1);

            	if ( (LA7_0 == 'E' || LA7_0 == 'e') )
            	{
            	    alt7 = 1;
            	}
            	switch (alt7) 
            	{
            	    case 1 :
            	        // Ada95.g:888:53: EXPONENT
            	        {
            	        	mEXPONENT(); 

            	        }
            	        break;

            	}


            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "BASED_LITERAL"

    // $ANTLR start "EXPONENT"
    public void mEXPONENT() // throws RecognitionException [2]
    {
    		try
    		{
            // Ada95.g:892:2: ( ( 'e' | 'E' ) ( '+' | '-' )? NUMERAL )
            // Ada95.g:892:4: ( 'e' | 'E' ) ( '+' | '-' )? NUMERAL
            {
            	if ( input.LA(1) == 'E' || input.LA(1) == 'e' ) 
            	{
            	    input.Consume();

            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    Recover(mse);
            	    throw mse;}

            	// Ada95.g:892:18: ( '+' | '-' )?
            	int alt8 = 2;
            	int LA8_0 = input.LA(1);

            	if ( (LA8_0 == '+' || LA8_0 == '-') )
            	{
            	    alt8 = 1;
            	}
            	switch (alt8) 
            	{
            	    case 1 :
            	        // Ada95.g:
            	        {
            	        	if ( input.LA(1) == '+' || input.LA(1) == '-' ) 
            	        	{
            	        	    input.Consume();

            	        	}
            	        	else 
            	        	{
            	        	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	        	    Recover(mse);
            	        	    throw mse;}


            	        }
            	        break;

            	}

            	mNUMERAL(); 

            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "EXPONENT"

    // $ANTLR start "NUMERAL"
    public void mNUMERAL() // throws RecognitionException [2]
    {
    		try
    		{
            // Ada95.g:896:2: ( DIGIT ( ( '_' )? DIGIT )* )
            // Ada95.g:896:4: DIGIT ( ( '_' )? DIGIT )*
            {
            	mDIGIT(); 
            	// Ada95.g:896:10: ( ( '_' )? DIGIT )*
            	do 
            	{
            	    int alt10 = 2;
            	    int LA10_0 = input.LA(1);

            	    if ( ((LA10_0 >= '0' && LA10_0 <= '9') || LA10_0 == '_') )
            	    {
            	        alt10 = 1;
            	    }


            	    switch (alt10) 
            		{
            			case 1 :
            			    // Ada95.g:896:12: ( '_' )? DIGIT
            			    {
            			    	// Ada95.g:896:12: ( '_' )?
            			    	int alt9 = 2;
            			    	int LA9_0 = input.LA(1);

            			    	if ( (LA9_0 == '_') )
            			    	{
            			    	    alt9 = 1;
            			    	}
            			    	switch (alt9) 
            			    	{
            			    	    case 1 :
            			    	        // Ada95.g:896:12: '_'
            			    	        {
            			    	        	Match('_'); 

            			    	        }
            			    	        break;

            			    	}

            			    	mDIGIT(); 

            			    }
            			    break;

            			default:
            			    goto loop10;
            	    }
            	} while (true);

            	loop10:
            		;	// Stops C# compiler whining that label 'loop10' has no statements


            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "NUMERAL"

    // $ANTLR start "DIGIT"
    public void mDIGIT() // throws RecognitionException [2]
    {
    		try
    		{
            // Ada95.g:900:2: ( '0' .. '9' )
            // Ada95.g:900:4: '0' .. '9'
            {
            	MatchRange('0','9'); 

            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "DIGIT"

    // $ANTLR start "BASE"
    public void mBASE() // throws RecognitionException [2]
    {
    		try
    		{
            // Ada95.g:904:2: ( NUMERAL )
            // Ada95.g:904:4: NUMERAL
            {
            	mNUMERAL(); 

            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "BASE"

    // $ANTLR start "BASED_NUMERAL"
    public void mBASED_NUMERAL() // throws RecognitionException [2]
    {
    		try
    		{
            // Ada95.g:908:2: ( EXTENDED_DIGIT ( ( '_' )? EXTENDED_DIGIT )* )
            // Ada95.g:908:4: EXTENDED_DIGIT ( ( '_' )? EXTENDED_DIGIT )*
            {
            	mEXTENDED_DIGIT(); 
            	// Ada95.g:908:19: ( ( '_' )? EXTENDED_DIGIT )*
            	do 
            	{
            	    int alt12 = 2;
            	    int LA12_0 = input.LA(1);

            	    if ( ((LA12_0 >= '0' && LA12_0 <= '9') || (LA12_0 >= 'A' && LA12_0 <= 'F') || LA12_0 == '_' || (LA12_0 >= 'a' && LA12_0 <= 'f')) )
            	    {
            	        alt12 = 1;
            	    }


            	    switch (alt12) 
            		{
            			case 1 :
            			    // Ada95.g:908:21: ( '_' )? EXTENDED_DIGIT
            			    {
            			    	// Ada95.g:908:21: ( '_' )?
            			    	int alt11 = 2;
            			    	int LA11_0 = input.LA(1);

            			    	if ( (LA11_0 == '_') )
            			    	{
            			    	    alt11 = 1;
            			    	}
            			    	switch (alt11) 
            			    	{
            			    	    case 1 :
            			    	        // Ada95.g:908:21: '_'
            			    	        {
            			    	        	Match('_'); 

            			    	        }
            			    	        break;

            			    	}

            			    	mEXTENDED_DIGIT(); 

            			    }
            			    break;

            			default:
            			    goto loop12;
            	    }
            	} while (true);

            	loop12:
            		;	// Stops C# compiler whining that label 'loop12' has no statements


            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "BASED_NUMERAL"

    // $ANTLR start "EXTENDED_DIGIT"
    public void mEXTENDED_DIGIT() // throws RecognitionException [2]
    {
    		try
    		{
            // Ada95.g:912:2: ( DIGIT | 'a' .. 'f' | 'A' .. 'F' )
            // Ada95.g:
            {
            	if ( (input.LA(1) >= '0' && input.LA(1) <= '9') || (input.LA(1) >= 'A' && input.LA(1) <= 'F') || (input.LA(1) >= 'a' && input.LA(1) <= 'f') ) 
            	{
            	    input.Consume();

            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    Recover(mse);
            	    throw mse;}


            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "EXTENDED_DIGIT"

    // $ANTLR start "STRING_LITERAL"
    public void mSTRING_LITERAL() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = STRING_LITERAL;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:915:2: ( '\"' ( '\"\"' | ~ '\"' )* '\"' )
            // Ada95.g:915:4: '\"' ( '\"\"' | ~ '\"' )* '\"'
            {
            	Match('\"'); 
            	// Ada95.g:915:8: ( '\"\"' | ~ '\"' )*
            	do 
            	{
            	    int alt13 = 3;
            	    int LA13_0 = input.LA(1);

            	    if ( (LA13_0 == '\"') )
            	    {
            	        int LA13_1 = input.LA(2);

            	        if ( (LA13_1 == '\"') )
            	        {
            	            alt13 = 1;
            	        }


            	    }
            	    else if ( ((LA13_0 >= '\u0000' && LA13_0 <= '!') || (LA13_0 >= '#' && LA13_0 <= '\uFFFF')) )
            	    {
            	        alt13 = 2;
            	    }


            	    switch (alt13) 
            		{
            			case 1 :
            			    // Ada95.g:915:10: '\"\"'
            			    {
            			    	Match("\"\""); 


            			    }
            			    break;
            			case 2 :
            			    // Ada95.g:915:17: ~ '\"'
            			    {
            			    	if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!') || (input.LA(1) >= '#' && input.LA(1) <= '\uFFFF') ) 
            			    	{
            			    	    input.Consume();

            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    Recover(mse);
            			    	    throw mse;}


            			    }
            			    break;

            			default:
            			    goto loop13;
            	    }
            	} while (true);

            	loop13:
            		;	// Stops C# compiler whining that label 'loop13' has no statements

            	Match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "STRING_LITERAL"

    // $ANTLR start "CHARACTER_LITERAL"
    public void mCHARACTER_LITERAL() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = CHARACTER_LITERAL;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:918:2: ({...}? => '\\'' ( '\\'\\'' | ~ '\\'' ) '\\'' )
            // Ada95.g:918:4: {...}? => '\\'' ( '\\'\\'' | ~ '\\'' ) '\\''
            {
            	if ( !(( input.LA( 3 ) == '\'' )) ) 
            	{
            	    throw new FailedPredicateException(input, "CHARACTER_LITERAL", " input.LA( 3 ) == '\\'' ");
            	}
            	Match('\''); 
            	// Ada95.g:918:38: ( '\\'\\'' | ~ '\\'' )
            	int alt14 = 2;
            	int LA14_0 = input.LA(1);

            	if ( (LA14_0 == '\'') )
            	{
            	    alt14 = 1;
            	}
            	else if ( ((LA14_0 >= '\u0000' && LA14_0 <= '&') || (LA14_0 >= '(' && LA14_0 <= '\uFFFF')) )
            	{
            	    alt14 = 2;
            	}
            	else 
            	{
            	    NoViableAltException nvae_d14s0 =
            	        new NoViableAltException("", 14, 0, input);

            	    throw nvae_d14s0;
            	}
            	switch (alt14) 
            	{
            	    case 1 :
            	        // Ada95.g:918:40: '\\'\\''
            	        {
            	        	Match("''"); 


            	        }
            	        break;
            	    case 2 :
            	        // Ada95.g:918:49: ~ '\\''
            	        {
            	        	if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '&') || (input.LA(1) >= '(' && input.LA(1) <= '\uFFFF') ) 
            	        	{
            	        	    input.Consume();

            	        	}
            	        	else 
            	        	{
            	        	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	        	    Recover(mse);
            	        	    throw mse;}


            	        }
            	        break;

            	}

            	Match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "CHARACTER_LITERAL"

    // $ANTLR start "IDENTIFIER_LETTER"
    public void mIDENTIFIER_LETTER() // throws RecognitionException [2]
    {
    		try
    		{
            // Ada95.g:922:2: ( 'a' .. 'z' | 'A' .. 'Z' )
            // Ada95.g:
            {
            	if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) 
            	{
            	    input.Consume();

            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    Recover(mse);
            	    throw mse;}


            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "IDENTIFIER_LETTER"

    // $ANTLR start "COMMENT"
    public void mCOMMENT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = COMMENT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:925:2: ( '--' (~ ( '\\n' | '\\r' ) )* ( '\\r\\n' | '\\r' | '\\n' )? )
            // Ada95.g:925:4: '--' (~ ( '\\n' | '\\r' ) )* ( '\\r\\n' | '\\r' | '\\n' )?
            {
            	Match("--"); 

            	// Ada95.g:925:9: (~ ( '\\n' | '\\r' ) )*
            	do 
            	{
            	    int alt15 = 2;
            	    int LA15_0 = input.LA(1);

            	    if ( ((LA15_0 >= '\u0000' && LA15_0 <= '\t') || (LA15_0 >= '\u000B' && LA15_0 <= '\f') || (LA15_0 >= '\u000E' && LA15_0 <= '\uFFFF')) )
            	    {
            	        alt15 = 1;
            	    }


            	    switch (alt15) 
            		{
            			case 1 :
            			    // Ada95.g:925:9: ~ ( '\\n' | '\\r' )
            			    {
            			    	if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t') || (input.LA(1) >= '\u000B' && input.LA(1) <= '\f') || (input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) 
            			    	{
            			    	    input.Consume();

            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    Recover(mse);
            			    	    throw mse;}


            			    }
            			    break;

            			default:
            			    goto loop15;
            	    }
            	} while (true);

            	loop15:
            		;	// Stops C# compiler whining that label 'loop15' has no statements

            	// Ada95.g:925:27: ( '\\r\\n' | '\\r' | '\\n' )?
            	int alt16 = 4;
            	int LA16_0 = input.LA(1);

            	if ( (LA16_0 == '\r') )
            	{
            	    int LA16_1 = input.LA(2);

            	    if ( (LA16_1 == '\n') )
            	    {
            	        alt16 = 1;
            	    }
            	}
            	else if ( (LA16_0 == '\n') )
            	{
            	    alt16 = 3;
            	}
            	switch (alt16) 
            	{
            	    case 1 :
            	        // Ada95.g:925:29: '\\r\\n'
            	        {
            	        	Match("\r\n"); 


            	        }
            	        break;
            	    case 2 :
            	        // Ada95.g:925:38: '\\r'
            	        {
            	        	Match('\r'); 

            	        }
            	        break;
            	    case 3 :
            	        // Ada95.g:925:45: '\\n'
            	        {
            	        	Match('\n'); 

            	        }
            	        break;

            	}

            	 _channel = HIDDEN; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "COMMENT"

    // $ANTLR start "WS"
    public void mWS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = WS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ada95.g:928:2: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // Ada95.g:928:4: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            	// Ada95.g:928:4: ( ' ' | '\\t' | '\\r' | '\\n' )+
            	int cnt17 = 0;
            	do 
            	{
            	    int alt17 = 2;
            	    int LA17_0 = input.LA(1);

            	    if ( ((LA17_0 >= '\t' && LA17_0 <= '\n') || LA17_0 == '\r' || LA17_0 == ' ') )
            	    {
            	        alt17 = 1;
            	    }


            	    switch (alt17) 
            		{
            			case 1 :
            			    // Ada95.g:
            			    {
            			    	if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n') || input.LA(1) == '\r' || input.LA(1) == ' ' ) 
            			    	{
            			    	    input.Consume();

            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    Recover(mse);
            			    	    throw mse;}


            			    }
            			    break;

            			default:
            			    if ( cnt17 >= 1 ) goto loop17;
            		            EarlyExitException eee17 =
            		                new EarlyExitException(17, input);
            		            throw eee17;
            	    }
            	    cnt17++;
            	} while (true);

            	loop17:
            		;	// Stops C# compiler whining that label 'loop17' has no statements

            	 _channel = HIDDEN; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "WS"

    override public void mTokens() // throws RecognitionException 
    {
        // Ada95.g:1:8: ( DELTA | WITH | NEW | ARRAY | OF | CONSTANT | ALIASED | IF | ELSIF | WHILE | FOR | LOOP | REVERSE | GOTO | EXIT | DECLARE | PROCEDURE | FUNCTION | ABSTRACT | TAGGED | LIMITED | ACCESS | ALL | RETURN | DIGITS | RANGE | TYPE | SUBTYPE | IS | RECORD | CASE | WHEN | OTHERS | BEGIN | END | NULL | IN | OUT | NOT | AND | OR | XOR | THEN | ELSE | CHOICE | POW | MULT | DIV | ABS | MOD | REM | LLABELBRACKET | RLABELBRACKET | SEMICOLON | ASSOCIATION | COLON | COMMA | APOSTROPHE | DOT | DOTDOT | ASSIGN | LPARANTHESIS | RPARANTHESIS | BOX | EQ | NEQ | LESS | LEQ | GREATER | GEQ | PLUS | MINUS | CONCAT | IDENTIFIER | NUMERIC_LITERAL | STRING_LITERAL | CHARACTER_LITERAL | COMMENT | WS )
        int alt18 = 79;
        alt18 = dfa18.Predict(input);
        switch (alt18) 
        {
            case 1 :
                // Ada95.g:1:10: DELTA
                {
                	mDELTA(); 

                }
                break;
            case 2 :
                // Ada95.g:1:16: WITH
                {
                	mWITH(); 

                }
                break;
            case 3 :
                // Ada95.g:1:21: NEW
                {
                	mNEW(); 

                }
                break;
            case 4 :
                // Ada95.g:1:25: ARRAY
                {
                	mARRAY(); 

                }
                break;
            case 5 :
                // Ada95.g:1:31: OF
                {
                	mOF(); 

                }
                break;
            case 6 :
                // Ada95.g:1:34: CONSTANT
                {
                	mCONSTANT(); 

                }
                break;
            case 7 :
                // Ada95.g:1:43: ALIASED
                {
                	mALIASED(); 

                }
                break;
            case 8 :
                // Ada95.g:1:51: IF
                {
                	mIF(); 

                }
                break;
            case 9 :
                // Ada95.g:1:54: ELSIF
                {
                	mELSIF(); 

                }
                break;
            case 10 :
                // Ada95.g:1:60: WHILE
                {
                	mWHILE(); 

                }
                break;
            case 11 :
                // Ada95.g:1:66: FOR
                {
                	mFOR(); 

                }
                break;
            case 12 :
                // Ada95.g:1:70: LOOP
                {
                	mLOOP(); 

                }
                break;
            case 13 :
                // Ada95.g:1:75: REVERSE
                {
                	mREVERSE(); 

                }
                break;
            case 14 :
                // Ada95.g:1:83: GOTO
                {
                	mGOTO(); 

                }
                break;
            case 15 :
                // Ada95.g:1:88: EXIT
                {
                	mEXIT(); 

                }
                break;
            case 16 :
                // Ada95.g:1:93: DECLARE
                {
                	mDECLARE(); 

                }
                break;
            case 17 :
                // Ada95.g:1:101: PROCEDURE
                {
                	mPROCEDURE(); 

                }
                break;
            case 18 :
                // Ada95.g:1:111: FUNCTION
                {
                	mFUNCTION(); 

                }
                break;
            case 19 :
                // Ada95.g:1:120: ABSTRACT
                {
                	mABSTRACT(); 

                }
                break;
            case 20 :
                // Ada95.g:1:129: TAGGED
                {
                	mTAGGED(); 

                }
                break;
            case 21 :
                // Ada95.g:1:136: LIMITED
                {
                	mLIMITED(); 

                }
                break;
            case 22 :
                // Ada95.g:1:144: ACCESS
                {
                	mACCESS(); 

                }
                break;
            case 23 :
                // Ada95.g:1:151: ALL
                {
                	mALL(); 

                }
                break;
            case 24 :
                // Ada95.g:1:155: RETURN
                {
                	mRETURN(); 

                }
                break;
            case 25 :
                // Ada95.g:1:162: DIGITS
                {
                	mDIGITS(); 

                }
                break;
            case 26 :
                // Ada95.g:1:169: RANGE
                {
                	mRANGE(); 

                }
                break;
            case 27 :
                // Ada95.g:1:175: TYPE
                {
                	mTYPE(); 

                }
                break;
            case 28 :
                // Ada95.g:1:180: SUBTYPE
                {
                	mSUBTYPE(); 

                }
                break;
            case 29 :
                // Ada95.g:1:188: IS
                {
                	mIS(); 

                }
                break;
            case 30 :
                // Ada95.g:1:191: RECORD
                {
                	mRECORD(); 

                }
                break;
            case 31 :
                // Ada95.g:1:198: CASE
                {
                	mCASE(); 

                }
                break;
            case 32 :
                // Ada95.g:1:203: WHEN
                {
                	mWHEN(); 

                }
                break;
            case 33 :
                // Ada95.g:1:208: OTHERS
                {
                	mOTHERS(); 

                }
                break;
            case 34 :
                // Ada95.g:1:215: BEGIN
                {
                	mBEGIN(); 

                }
                break;
            case 35 :
                // Ada95.g:1:221: END
                {
                	mEND(); 

                }
                break;
            case 36 :
                // Ada95.g:1:225: NULL
                {
                	mNULL(); 

                }
                break;
            case 37 :
                // Ada95.g:1:230: IN
                {
                	mIN(); 

                }
                break;
            case 38 :
                // Ada95.g:1:233: OUT
                {
                	mOUT(); 

                }
                break;
            case 39 :
                // Ada95.g:1:237: NOT
                {
                	mNOT(); 

                }
                break;
            case 40 :
                // Ada95.g:1:241: AND
                {
                	mAND(); 

                }
                break;
            case 41 :
                // Ada95.g:1:245: OR
                {
                	mOR(); 

                }
                break;
            case 42 :
                // Ada95.g:1:248: XOR
                {
                	mXOR(); 

                }
                break;
            case 43 :
                // Ada95.g:1:252: THEN
                {
                	mTHEN(); 

                }
                break;
            case 44 :
                // Ada95.g:1:257: ELSE
                {
                	mELSE(); 

                }
                break;
            case 45 :
                // Ada95.g:1:262: CHOICE
                {
                	mCHOICE(); 

                }
                break;
            case 46 :
                // Ada95.g:1:269: POW
                {
                	mPOW(); 

                }
                break;
            case 47 :
                // Ada95.g:1:273: MULT
                {
                	mMULT(); 

                }
                break;
            case 48 :
                // Ada95.g:1:278: DIV
                {
                	mDIV(); 

                }
                break;
            case 49 :
                // Ada95.g:1:282: ABS
                {
                	mABS(); 

                }
                break;
            case 50 :
                // Ada95.g:1:286: MOD
                {
                	mMOD(); 

                }
                break;
            case 51 :
                // Ada95.g:1:290: REM
                {
                	mREM(); 

                }
                break;
            case 52 :
                // Ada95.g:1:294: LLABELBRACKET
                {
                	mLLABELBRACKET(); 

                }
                break;
            case 53 :
                // Ada95.g:1:308: RLABELBRACKET
                {
                	mRLABELBRACKET(); 

                }
                break;
            case 54 :
                // Ada95.g:1:322: SEMICOLON
                {
                	mSEMICOLON(); 

                }
                break;
            case 55 :
                // Ada95.g:1:332: ASSOCIATION
                {
                	mASSOCIATION(); 

                }
                break;
            case 56 :
                // Ada95.g:1:344: COLON
                {
                	mCOLON(); 

                }
                break;
            case 57 :
                // Ada95.g:1:350: COMMA
                {
                	mCOMMA(); 

                }
                break;
            case 58 :
                // Ada95.g:1:356: APOSTROPHE
                {
                	mAPOSTROPHE(); 

                }
                break;
            case 59 :
                // Ada95.g:1:367: DOT
                {
                	mDOT(); 

                }
                break;
            case 60 :
                // Ada95.g:1:371: DOTDOT
                {
                	mDOTDOT(); 

                }
                break;
            case 61 :
                // Ada95.g:1:378: ASSIGN
                {
                	mASSIGN(); 

                }
                break;
            case 62 :
                // Ada95.g:1:385: LPARANTHESIS
                {
                	mLPARANTHESIS(); 

                }
                break;
            case 63 :
                // Ada95.g:1:398: RPARANTHESIS
                {
                	mRPARANTHESIS(); 

                }
                break;
            case 64 :
                // Ada95.g:1:411: BOX
                {
                	mBOX(); 

                }
                break;
            case 65 :
                // Ada95.g:1:415: EQ
                {
                	mEQ(); 

                }
                break;
            case 66 :
                // Ada95.g:1:418: NEQ
                {
                	mNEQ(); 

                }
                break;
            case 67 :
                // Ada95.g:1:422: LESS
                {
                	mLESS(); 

                }
                break;
            case 68 :
                // Ada95.g:1:427: LEQ
                {
                	mLEQ(); 

                }
                break;
            case 69 :
                // Ada95.g:1:431: GREATER
                {
                	mGREATER(); 

                }
                break;
            case 70 :
                // Ada95.g:1:439: GEQ
                {
                	mGEQ(); 

                }
                break;
            case 71 :
                // Ada95.g:1:443: PLUS
                {
                	mPLUS(); 

                }
                break;
            case 72 :
                // Ada95.g:1:448: MINUS
                {
                	mMINUS(); 

                }
                break;
            case 73 :
                // Ada95.g:1:454: CONCAT
                {
                	mCONCAT(); 

                }
                break;
            case 74 :
                // Ada95.g:1:461: IDENTIFIER
                {
                	mIDENTIFIER(); 

                }
                break;
            case 75 :
                // Ada95.g:1:472: NUMERIC_LITERAL
                {
                	mNUMERIC_LITERAL(); 

                }
                break;
            case 76 :
                // Ada95.g:1:488: STRING_LITERAL
                {
                	mSTRING_LITERAL(); 

                }
                break;
            case 77 :
                // Ada95.g:1:503: CHARACTER_LITERAL
                {
                	mCHARACTER_LITERAL(); 

                }
                break;
            case 78 :
                // Ada95.g:1:521: COMMENT
                {
                	mCOMMENT(); 

                }
                break;
            case 79 :
                // Ada95.g:1:529: WS
                {
                	mWS(); 

                }
                break;

        }

    }


    protected DFA3 dfa3;
    protected DFA18 dfa18;
	private void InitializeCyclicDFAs()
	{
	    this.dfa3 = new DFA3(this);
	    this.dfa18 = new DFA18(this);
	    this.dfa18.specialStateTransitionHandler = new DFA.SpecialStateTransitionHandler(DFA18_SpecialStateTransition);
	}

    const string DFA3_eotS =
        "\x01\uffff\x01\x02\x02\uffff\x01\x02\x01\uffff";
    const string DFA3_eofS =
        "\x06\uffff";
    const string DFA3_minS =
        "\x01\x30\x01\x23\x01\uffff\x01\x30\x01\x23\x01\uffff";
    const string DFA3_maxS =
        "\x01\x39\x01\x5f\x01\uffff\x01\x39\x01\x5f\x01\uffff";
    const string DFA3_acceptS =
        "\x02\uffff\x01\x01\x02\uffff\x01\x02";
    const string DFA3_specialS =
        "\x06\uffff}>";
    static readonly string[] DFA3_transitionS = {
            "\x0a\x01",
            "\x01\x05\x0c\uffff\x0a\x04\x25\uffff\x01\x03",
            "",
            "\x0a\x04",
            "\x01\x05\x0c\uffff\x0a\x04\x25\uffff\x01\x03",
            ""
    };

    static readonly short[] DFA3_eot = DFA.UnpackEncodedString(DFA3_eotS);
    static readonly short[] DFA3_eof = DFA.UnpackEncodedString(DFA3_eofS);
    static readonly char[] DFA3_min = DFA.UnpackEncodedStringToUnsignedChars(DFA3_minS);
    static readonly char[] DFA3_max = DFA.UnpackEncodedStringToUnsignedChars(DFA3_maxS);
    static readonly short[] DFA3_accept = DFA.UnpackEncodedString(DFA3_acceptS);
    static readonly short[] DFA3_special = DFA.UnpackEncodedString(DFA3_specialS);
    static readonly short[][] DFA3_transition = DFA.UnpackEncodedStringArray(DFA3_transitionS);

    protected class DFA3 : DFA
    {
        public DFA3(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = DFA3_eot;
            this.eof = DFA3_eof;
            this.min = DFA3_min;
            this.max = DFA3_max;
            this.accept = DFA3_accept;
            this.special = DFA3_special;
            this.transition = DFA3_transition;

        }

        override public string Description
        {
            get { return "879:1: NUMERIC_LITERAL : ( DECIMAL_LITERAL | BASED_LITERAL );"; }
        }

    }

    const string DFA18_eotS =
        "\x01\uffff\x11\x23\x01\uffff\x01\x4e\x01\x50\x01\x23\x01\x55\x01"+
        "\x58\x01\uffff\x01\x5a\x01\x5c\x01\uffff\x01\x5d\x01\x60\x03\uffff"+
        "\x01\x62\x05\uffff\x0c\x23\x01\x72\x02\x23\x01\x75\x02\x23\x01\x78"+
        "\x01\x79\x01\x7a\x11\x23\x04\uffff\x01\x23\x11\uffff\x06\x23\x01"+
        "\u0096\x01\x23\x01\u0098\x02\x23\x01\u009b\x01\u009d\x01\x23\x01"+
        "\u009f\x01\uffff\x01\x23\x01\u00a1\x01\uffff\x02\x23\x03\uffff\x02"+
        "\x23\x01\u00a7\x01\u00a8\x06\x23\x01\u00af\x08\x23\x01\u00b8\x01"+
        "\u00b9\x03\x23\x01\u00bd\x01\x23\x01\u00bf\x01\uffff\x01\u00c0\x01"+
        "\uffff\x02\x23\x01\uffff\x01\x23\x01\uffff\x01\x23\x01\uffff\x01"+
        "\x23\x01\uffff\x01\x23\x01\u00c7\x01\x23\x01\u00c9\x01\u00ca\x02"+
        "\uffff\x01\x23\x01\u00cc\x04\x23\x01\uffff\x01\x23\x01\u00d2\x02"+
        "\x23\x01\u00d5\x01\u00d6\x02\x23\x02\uffff\x01\u00d9\x02\x23\x01"+
        "\uffff\x01\u00dc\x02\uffff\x01\u00dd\x05\x23\x01\uffff\x01\u00e3"+
        "\x02\uffff\x01\x23\x01\uffff\x04\x23\x01\u00e9\x01\uffff\x02\x23"+
        "\x02\uffff\x01\x23\x01\u00ed\x01\uffff\x01\x23\x01\u00ef\x02\uffff"+
        "\x02\x23\x01\u00f2\x01\u00f3\x01\x23\x01\uffff\x03\x23\x01\u00f8"+
        "\x01\u00f9\x01\uffff\x01\x23\x01\u00fb\x01\x23\x01\uffff\x01\u00fd"+
        "\x01\uffff\x01\u00fe\x01\x23\x02\uffff\x02\x23\x01\u0102\x01\u0103"+
        "\x02\uffff\x01\x23\x01\uffff\x01\u0105\x02\uffff\x01\u0106\x01\u0107"+
        "\x01\u0108\x02\uffff\x01\x23\x04\uffff\x01\u010a\x01\uffff";
    const string DFA18_eofS =
        "\u010b\uffff";
    const string DFA18_minS =
        "\x01\x09\x01\x65\x01\x68\x01\x65\x01\x62\x01\x66\x01\x61\x01\x66"+
        "\x01\x6c\x01\x6f\x01\x69\x01\x61\x01\x6f\x01\x72\x01\x61\x01\x75"+
        "\x01\x65\x01\x6f\x01\uffff\x01\x2a\x01\x3d\x01\x6f\x01\x3c\x01\x3d"+
        "\x01\uffff\x01\x3e\x01\x3d\x01\uffff\x01\x00\x01\x2e\x03\uffff\x01"+
        "\x2d\x05\uffff\x01\x63\x01\x67\x01\x74\x01\x65\x01\x77\x01\x6c\x01"+
        "\x74\x01\x72\x01\x69\x01\x73\x01\x63\x01\x64\x01\x30\x01\x68\x01"+
        "\x74\x01\x30\x01\x6e\x01\x73\x03\x30\x01\x73\x01\x69\x01\x64\x01"+
        "\x72\x01\x6e\x01\x6f\x01\x6d\x01\x63\x01\x6e\x01\x74\x01\x6f\x01"+
        "\x67\x01\x70\x01\x65\x01\x62\x01\x67\x01\x72\x04\uffff\x01\x64\x11"+
        "\uffff\x01\x74\x01\x6c\x01\x69\x01\x68\x01\x6c\x01\x6e\x01\x30\x01"+
        "\x6c\x01\x30\x02\x61\x02\x30\x01\x65\x01\x30\x01\uffff\x01\x65\x01"+
        "\x30\x01\uffff\x01\x73\x01\x65\x03\uffff\x01\x65\x01\x74\x02\x30"+
        "\x01\x63\x01\x70\x01\x69\x01\x65\x01\x75\x01\x6f\x01\x30\x01\x67"+
        "\x01\x6f\x01\x63\x01\x67\x01\x65\x01\x6e\x01\x74\x01\x69\x02\x30"+
        "\x02\x61\x01\x74\x01\x30\x01\x65\x01\x30\x01\uffff\x01\x30\x01\uffff"+
        "\x01\x79\x01\x73\x01\uffff\x01\x72\x01\uffff\x01\x73\x01\uffff\x01"+
        "\x72\x01\uffff\x01\x74\x01\x30\x01\x66\x02\x30\x02\uffff\x01\x74"+
        "\x01\x30\x01\x74\x03\x72\x01\uffff\x01\x65\x01\x30\x02\x65\x02\x30"+
        "\x01\x79\x01\x6e\x02\uffff\x01\x30\x01\x72\x01\x73\x01\uffff\x01"+
        "\x30\x02\uffff\x01\x30\x01\x65\x01\x61\x02\x73\x01\x61\x01\uffff"+
        "\x01\x30\x02\uffff\x01\x69\x01\uffff\x01\x65\x01\x73\x01\x6e\x01"+
        "\x64\x01\x30\x01\uffff\x02\x64\x02\uffff\x01\x70\x01\x30\x01\uffff"+
        "\x01\x65\x01\x30\x02\uffff\x01\x64\x01\x63\x02\x30\x01\x6e\x01\uffff"+
        "\x01\x6f\x01\x64\x01\x65\x02\x30\x01\uffff\x01\x75\x01\x30\x01\x65"+
        "\x01\uffff\x01\x30\x01\uffff\x01\x30\x01\x74\x02\uffff\x01\x74\x01"+
        "\x6e\x02\x30\x02\uffff\x01\x72\x01\uffff\x01\x30\x02\uffff\x03\x30"+
        "\x02\uffff\x01\x65\x04\uffff\x01\x30\x01\uffff";
    const string DFA18_maxS =
        "\x01\x7c\x02\x69\x01\x75\x01\x72\x01\x75\x01\x6f\x01\x73\x01\x78"+
        "\x01\x75\x01\x6f\x01\x65\x01\x6f\x01\x72\x01\x79\x01\x75\x01\x65"+
        "\x01\x6f\x01\uffff\x01\x2a\x01\x3d\x01\x6f\x02\x3e\x01\uffff\x01"+
        "\x3e\x01\x3d\x01\uffff\x01\uffff\x01\x2e\x03\uffff\x01\x2d\x05\uffff"+
        "\x01\x6c\x01\x67\x01\x74\x01\x69\x01\x77\x01\x6c\x01\x74\x01\x72"+
        "\x01\x6c\x01\x73\x01\x63\x01\x64\x01\x7a\x01\x68\x01\x74\x01\x7a"+
        "\x01\x6e\x01\x73\x03\x7a\x01\x73\x01\x69\x01\x64\x01\x72\x01\x6e"+
        "\x01\x6f\x01\x6d\x01\x76\x01\x6e\x01\x74\x01\x6f\x01\x67\x01\x70"+
        "\x01\x65\x01\x62\x01\x67\x01\x72\x04\uffff\x01\x64\x11\uffff\x01"+
        "\x74\x01\x6c\x01\x69\x01\x68\x01\x6c\x01\x6e\x01\x7a\x01\x6c\x01"+
        "\x7a\x02\x61\x02\x7a\x01\x65\x01\x7a\x01\uffff\x01\x65\x01\x7a\x01"+
        "\uffff\x01\x73\x01\x65\x03\uffff\x01\x69\x01\x74\x02\x7a\x01\x63"+
        "\x01\x70\x01\x69\x01\x65\x01\x75\x01\x6f\x01\x7a\x01\x67\x01\x6f"+
        "\x01\x63\x01\x67\x01\x65\x01\x6e\x01\x74\x01\x69\x02\x7a\x02\x61"+
        "\x01\x74\x01\x7a\x01\x65\x01\x7a\x01\uffff\x01\x7a\x01\uffff\x01"+
        "\x79\x01\x73\x01\uffff\x01\x72\x01\uffff\x01\x73\x01\uffff\x01\x72"+
        "\x01\uffff\x01\x74\x01\x7a\x01\x66\x02\x7a\x02\uffff\x01\x74\x01"+
        "\x7a\x01\x74\x03\x72\x01\uffff\x01\x65\x01\x7a\x02\x65\x02\x7a\x01"+
        "\x79\x01\x6e\x02\uffff\x01\x7a\x01\x72\x01\x73\x01\uffff\x01\x7a"+
        "\x02\uffff\x01\x7a\x01\x65\x01\x61\x02\x73\x01\x61\x01\uffff\x01"+
        "\x7a\x02\uffff\x01\x69\x01\uffff\x01\x65\x01\x73\x01\x6e\x01\x64"+
        "\x01\x7a\x01\uffff\x02\x64\x02\uffff\x01\x70\x01\x7a\x01\uffff\x01"+
        "\x65\x01\x7a\x02\uffff\x01\x64\x01\x63\x02\x7a\x01\x6e\x01\uffff"+
        "\x01\x6f\x01\x64\x01\x65\x02\x7a\x01\uffff\x01\x75\x01\x7a\x01\x65"+
        "\x01\uffff\x01\x7a\x01\uffff\x01\x7a\x01\x74\x02\uffff\x01\x74\x01"+
        "\x6e\x02\x7a\x02\uffff\x01\x72\x01\uffff\x01\x7a\x02\uffff\x03\x7a"+
        "\x02\uffff\x01\x65\x04\uffff\x01\x7a\x01\uffff";
    const string DFA18_acceptS =
        "\x12\uffff\x01\x2d\x05\uffff\x01\x36\x02\uffff\x01\x39\x02\uffff"+
        "\x01\x3e\x01\x3f\x01\x47\x01\uffff\x01\x49\x01\x4a\x01\x4b\x01\x4c"+
        "\x01\x4f\x26\uffff\x01\x2e\x01\x2f\x01\x42\x01\x30\x01\uffff\x01"+
        "\x34\x01\x40\x01\x44\x01\x43\x01\x35\x01\x46\x01\x45\x01\x37\x01"+
        "\x41\x01\x3d\x01\x38\x01\x3a\x01\x4d\x01\x3c\x01\x3b\x01\x4e\x01"+
        "\x48\x0f\uffff\x01\x05\x02\uffff\x01\x29\x02\uffff\x01\x08\x01\x1d"+
        "\x01\x25\x1b\uffff\x01\x03\x01\uffff\x01\x27\x02\uffff\x01\x17\x01"+
        "\uffff\x01\x31\x01\uffff\x01\x28\x01\uffff\x01\x26\x05\uffff\x01"+
        "\x23\x01\x0b\x06\uffff\x01\x33\x08\uffff\x01\x2a\x01\x32\x03\uffff"+
        "\x01\x02\x01\uffff\x01\x20\x01\x24\x06\uffff\x01\x1f\x01\uffff\x01"+
        "\x2c\x01\x0f\x01\uffff\x01\x0c\x05\uffff\x01\x0e\x02\uffff\x01\x1b"+
        "\x01\x2b\x02\uffff\x01\x01\x02\uffff\x01\x0a\x01\x04\x05\uffff\x01"+
        "\x09\x05\uffff\x01\x1a\x03\uffff\x01\x22\x01\uffff\x01\x19\x02\uffff"+
        "\x01\x16\x01\x21\x04\uffff\x01\x18\x01\x1e\x01\uffff\x01\x14\x01"+
        "\uffff\x01\x10\x01\x07\x03\uffff\x01\x15\x01\x0d\x01\uffff\x01\x1c"+
        "\x01\x13\x01\x06\x01\x12\x01\uffff\x01\x11";
    const string DFA18_specialS =
        "\x1c\uffff\x01\x00\u00ee\uffff}>";
    static readonly string[] DFA18_transitionS = {
            "\x02\x26\x02\uffff\x01\x26\x12\uffff\x01\x26\x01\uffff\x01"+
            "\x25\x03\uffff\x01\x22\x01\x1c\x01\x1e\x01\x1f\x01\x13\x01\x20"+
            "\x01\x1b\x01\x21\x01\x1d\x01\x14\x0a\x24\x01\x1a\x01\x18\x01"+
            "\x16\x01\x19\x01\x17\x02\uffff\x1a\x23\x06\uffff\x01\x04\x01"+
            "\x10\x01\x06\x01\x01\x01\x08\x01\x09\x01\x0c\x01\x23\x01\x07"+
            "\x02\x23\x01\x0a\x01\x15\x01\x03\x01\x05\x01\x0d\x01\x23\x01"+
            "\x0b\x01\x0f\x01\x0e\x02\x23\x01\x02\x01\x11\x02\x23\x01\uffff"+
            "\x01\x12",
            "\x01\x27\x03\uffff\x01\x28",
            "\x01\x2a\x01\x29",
            "\x01\x2b\x09\uffff\x01\x2d\x05\uffff\x01\x2c",
            "\x01\x30\x01\x31\x08\uffff\x01\x2f\x01\uffff\x01\x32\x03\uffff"+
            "\x01\x2e",
            "\x01\x33\x0b\uffff\x01\x36\x01\uffff\x01\x34\x01\x35",
            "\x01\x38\x0d\uffff\x01\x37",
            "\x01\x39\x07\uffff\x01\x3b\x04\uffff\x01\x3a",
            "\x01\x3c\x01\uffff\x01\x3e\x09\uffff\x01\x3d",
            "\x01\x3f\x05\uffff\x01\x40",
            "\x01\x42\x05\uffff\x01\x41",
            "\x01\x44\x03\uffff\x01\x43",
            "\x01\x45",
            "\x01\x46",
            "\x01\x47\x06\uffff\x01\x49\x10\uffff\x01\x48",
            "\x01\x4a",
            "\x01\x4b",
            "\x01\x4c",
            "",
            "\x01\x4d",
            "\x01\x4f",
            "\x01\x51",
            "\x01\x52\x01\x54\x01\x53",
            "\x01\x57\x01\x56",
            "",
            "\x01\x59",
            "\x01\x5b",
            "",
            "\x00\x5e",
            "\x01\x5f",
            "",
            "",
            "",
            "\x01\x61",
            "",
            "",
            "",
            "",
            "",
            "\x01\x64\x08\uffff\x01\x63",
            "\x01\x65",
            "\x01\x66",
            "\x01\x68\x03\uffff\x01\x67",
            "\x01\x69",
            "\x01\x6a",
            "\x01\x6b",
            "\x01\x6c",
            "\x01\x6d\x02\uffff\x01\x6e",
            "\x01\x6f",
            "\x01\x70",
            "\x01\x71",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\x73",
            "\x01\x74",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\x76",
            "\x01\x77",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\x7b",
            "\x01\x7c",
            "\x01\x7d",
            "\x01\x7e",
            "\x01\x7f",
            "\x01\u0080",
            "\x01\u0081",
            "\x01\u0084\x09\uffff\x01\u0085\x06\uffff\x01\u0083\x01\uffff"+
            "\x01\u0082",
            "\x01\u0086",
            "\x01\u0087",
            "\x01\u0088",
            "\x01\u0089",
            "\x01\u008a",
            "\x01\u008b",
            "\x01\u008c",
            "\x01\u008d",
            "\x01\u008e",
            "",
            "",
            "",
            "",
            "\x01\u008f",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\x01\u0090",
            "\x01\u0091",
            "\x01\u0092",
            "\x01\u0093",
            "\x01\u0094",
            "\x01\u0095",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u0097",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u0099",
            "\x01\u009a",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x13"+
            "\x23\x01\u009c\x06\x23",
            "\x01\u009e",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "\x01\u00a0",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "\x01\u00a2",
            "\x01\u00a3",
            "",
            "",
            "",
            "\x01\u00a5\x03\uffff\x01\u00a4",
            "\x01\u00a6",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00a9",
            "\x01\u00aa",
            "\x01\u00ab",
            "\x01\u00ac",
            "\x01\u00ad",
            "\x01\u00ae",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00b0",
            "\x01\u00b1",
            "\x01\u00b2",
            "\x01\u00b3",
            "\x01\u00b4",
            "\x01\u00b5",
            "\x01\u00b6",
            "\x01\u00b7",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00ba",
            "\x01\u00bb",
            "\x01\u00bc",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00be",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "\x01\u00c1",
            "\x01\u00c2",
            "",
            "\x01\u00c3",
            "",
            "\x01\u00c4",
            "",
            "\x01\u00c5",
            "",
            "\x01\u00c6",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00c8",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "",
            "\x01\u00cb",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00cd",
            "\x01\u00ce",
            "\x01\u00cf",
            "\x01\u00d0",
            "",
            "\x01\u00d1",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00d3",
            "\x01\u00d4",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00d7",
            "\x01\u00d8",
            "",
            "",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00da",
            "\x01\u00db",
            "",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00de",
            "\x01\u00df",
            "\x01\u00e0",
            "\x01\u00e1",
            "\x01\u00e2",
            "",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "",
            "\x01\u00e4",
            "",
            "\x01\u00e5",
            "\x01\u00e6",
            "\x01\u00e7",
            "\x01\u00e8",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "\x01\u00ea",
            "\x01\u00eb",
            "",
            "",
            "\x01\u00ec",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "\x01\u00ee",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "",
            "\x01\u00f0",
            "\x01\u00f1",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00f4",
            "",
            "\x01\u00f5",
            "\x01\u00f6",
            "\x01\u00f7",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "\x01\u00fa",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00fc",
            "",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x01\u00ff",
            "",
            "",
            "\x01\u0100",
            "\x01\u0101",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "",
            "\x01\u0104",
            "",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            "",
            "",
            "\x01\u0109",
            "",
            "",
            "",
            "",
            "\x0a\x23\x07\uffff\x1a\x23\x04\uffff\x01\x23\x01\uffff\x1a"+
            "\x23",
            ""
    };

    static readonly short[] DFA18_eot = DFA.UnpackEncodedString(DFA18_eotS);
    static readonly short[] DFA18_eof = DFA.UnpackEncodedString(DFA18_eofS);
    static readonly char[] DFA18_min = DFA.UnpackEncodedStringToUnsignedChars(DFA18_minS);
    static readonly char[] DFA18_max = DFA.UnpackEncodedStringToUnsignedChars(DFA18_maxS);
    static readonly short[] DFA18_accept = DFA.UnpackEncodedString(DFA18_acceptS);
    static readonly short[] DFA18_special = DFA.UnpackEncodedString(DFA18_specialS);
    static readonly short[][] DFA18_transition = DFA.UnpackEncodedStringArray(DFA18_transitionS);

    protected class DFA18 : DFA
    {
        public DFA18(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 18;
            this.eot = DFA18_eot;
            this.eof = DFA18_eof;
            this.min = DFA18_min;
            this.max = DFA18_max;
            this.accept = DFA18_accept;
            this.special = DFA18_special;
            this.transition = DFA18_transition;

        }

        override public string Description
        {
            get { return "1:1: Tokens : ( DELTA | WITH | NEW | ARRAY | OF | CONSTANT | ALIASED | IF | ELSIF | WHILE | FOR | LOOP | REVERSE | GOTO | EXIT | DECLARE | PROCEDURE | FUNCTION | ABSTRACT | TAGGED | LIMITED | ACCESS | ALL | RETURN | DIGITS | RANGE | TYPE | SUBTYPE | IS | RECORD | CASE | WHEN | OTHERS | BEGIN | END | NULL | IN | OUT | NOT | AND | OR | XOR | THEN | ELSE | CHOICE | POW | MULT | DIV | ABS | MOD | REM | LLABELBRACKET | RLABELBRACKET | SEMICOLON | ASSOCIATION | COLON | COMMA | APOSTROPHE | DOT | DOTDOT | ASSIGN | LPARANTHESIS | RPARANTHESIS | BOX | EQ | NEQ | LESS | LEQ | GREATER | GEQ | PLUS | MINUS | CONCAT | IDENTIFIER | NUMERIC_LITERAL | STRING_LITERAL | CHARACTER_LITERAL | COMMENT | WS );"; }
        }

    }


    protected internal int DFA18_SpecialStateTransition(DFA dfa, int s, IIntStream _input) //throws NoViableAltException
    {
            IIntStream input = _input;
    	int _s = s;
        switch ( s )
        {
               	case 0 : 
                   	int LA18_28 = input.LA(1);

                   	 
                   	int index18_28 = input.Index();
                   	input.Rewind();
                   	s = -1;
                   	if ( ((LA18_28 >= '\u0000' && LA18_28 <= '\uFFFF')) && (( input.LA( 3 ) == '\'' )) ) { s = 94; }

                   	else s = 93;

                   	 
                   	input.Seek(index18_28);
                   	if ( s >= 0 ) return s;
                   	break;
        }
        NoViableAltException nvae18 =
            new NoViableAltException(dfa.Description, 18, _s, input);
        dfa.Error(nvae18);
        throw nvae18;
    }
 
    
}
}