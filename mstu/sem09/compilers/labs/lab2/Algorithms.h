#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include <functional>

// find any occurance of x or where it could be inserted.
// to find first occurance delete '*high != x' in for-loop.
template< class RandIter, class Type, class BinaryLessPredicate >
RandIter BinarySearch( const RandIter start, const RandIter end, const Type & x, BinaryLessPredicate less = std::less< Type >() )
{
	RandIter pos = start;
	if ( end == start )
		pos = start;
	else if ( less( x, *start ) )
		pos = start;
	else if ( less( *( end - 1 ), x ) )
		pos = end;
	else
	{
		RandIter low = start, high = end - 1;

		for ( ; *high != x && low < high; )
		{
			const RandIter mid = start + ( ( ( low - start ) + ( high - start ) ) >> 1 );
			( less( *mid, x ) ? low++ : high ) = mid;
		}

		pos = high;
	}

	return ( pos );
}

#endif