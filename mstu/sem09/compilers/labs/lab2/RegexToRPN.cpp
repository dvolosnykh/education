#include "RegexToRPN.h"


const bool RegexToRPN::operator()( std::vector< char > * const rpn, const std::string & regex )
{
	rpn->resize( 0 );

	bool ok = __WorkOnSubexpressionStart();
	for ( size_t i = 0; ok && i < regex.length(); ++i )
	{
		const char & cur = regex[ i ];

		const bool curIsOperand = Regex::IsOperand( cur );
		const bool operandBegining = ( curIsOperand || cur == Regex::OPERATOR_SUBEXPRESSION_START );
		const bool implicitOperator = ( operandBegining && __prevWasOperand );

		if ( implicitOperator )
			ok = __WorkOnOperator( rpn, Regex::OPERATOR_CONCATENATE );

		switch ( cur )
		{
		case Regex::OPERATOR_SUBEXPRESSION_START:
			ok = __WorkOnSubexpressionStart();
			break;

		case Regex::OPERATOR_SUBEXPRESSION_END:
			ok = __WorkOnSubexpressionEnd( rpn );
			break;

		default:
			ok = ( curIsOperand ? __WorkOnOperand( rpn, cur ) : __WorkOnOperator( rpn, cur ) );
			break;
		}
	}

	ok = ok && __WorkOnSubexpressionEnd( rpn, true );

	return ( ok );
}

const bool RegexToRPN::__WorkOnOperand( std::vector< char > * const rpn, const char cur )
{
	rpn->push_back( cur );
	__emptyExpr = false;
	__prevWasOperand = true;

	return ( true );
}

const bool RegexToRPN::__WorkOnSubexpressionStart()
{
	__bottoms.push( __s.size() );
	__emptyExpr = true;
	__prevWasOperand = false;

	return ( true );
}

const bool RegexToRPN::__WorkOnSubexpressionEnd( std::vector< char > * const rpn, const bool wholeExpressionEnded )
{
	bool ok = ( __s.size() == __bottoms.top() || __prevWasOperand );
	if ( ok )
	{
		for ( ; __s.size() > __bottoms.top(); __s.pop() )
			rpn->push_back( __s.top() );
		__bottoms.pop();
		
		ok = ( wholeExpressionEnded == __bottoms.empty() );
		if ( ok && !wholeExpressionEnded )
		{
			if ( __emptyExpr )
			{
				rpn->push_back( Regex::Symbols::EMPTY );
				__emptyExpr = false;
			}

			__prevWasOperand = true;
		}
	}

	return ( ok );
}

const bool RegexToRPN::__WorkOnOperator( std::vector< char > * const rpn, const char op )
{
	bool ok = __prevWasOperand;
	if ( ok )
	{
		const int priority = Regex::Priority( op );
		ok = ( priority >= 0 );
		if ( ok )
		{
			for ( ; __s.size() > __bottoms.top() && Regex::Priority( __s.top() ) >= priority; __s.pop() )
				rpn->push_back( __s.top() );
			__s.push( op );

			__prevWasOperand = Regex::IsUnaryPostOperator( op );
		}
	}

	return ( ok );
}
