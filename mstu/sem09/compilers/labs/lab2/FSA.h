#ifndef FSA_H
#define FSA_H

#include <string>
#include <vector>
#include <stack>
#include <list>
#include <set>
#include <utility>

#include "Regex.h"
#include "Graph.h"

class FSA
{
private:
	typedef std::set< size_t > state_t;
	struct ends_t { size_t start, final; };

private:
	Graph< Regex::Symbols > __graph;

public:
	FSA( const std::string & regex )
		: __graph( Regex::Alphabet ) { ConstructByRegex( regex ); }

	const bool ConstructByRegex( const std::string & regex );
	void Determine();
	void Minimize();
	const bool CorrectString( const std::string & str ) const;
	void Print( std::ostream & out ) const { __graph.Print( out ); }

private:
	void __ConstructByRPN( const std::vector< char > & rpn );
	void __ConstructOnBinaryOperator( std::stack< ends_t > * const s, const char token );
	const ends_t __ConstructOnAlternate( const std::pair< ends_t, ends_t > & operands );
	const ends_t __ConstructOnConcatenate(const std::pair< ends_t, ends_t > & operands );
	void __ConstructOnUnaryPostOperator( std::stack< ends_t > * const s, const char token );
	const ends_t __ConstructOnPositiveClosure( const ends_t & operand );
	const ends_t __ConstructOnClosure( const ends_t & operand );
	void __ConstructOnOperand( std::stack< ends_t > * const s, const char token );

	const bool __AreIndistinguishable( size_t u, size_t v, const size_t symbolIndex, const std::list< state_t > & partitions ) const;
};

#endif