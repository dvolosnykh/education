#include "Regex.h"

#include <algorithm>
#include <ctype.h>

Regex::Symbols Regex::Alphabet;

#define TEST

void Regex::Symbols::Initialize()
{
#ifdef TEST
	__labels.push_back( 'a' );
	__labels.push_back( 'b' );
#else
	for ( int symbol = std::numeric_limits< unsigned char >::min(); symbol <= std::numeric_limits< unsigned char >::max(); ++symbol )
		if ( isalpha( symbol ) || isdigit( symbol ) )
			__labels.push_back( symbol );
#endif
}

const bool Regex::Symbols::Contains( const char symbol ) const
{
	const size_t index = GetIndex( symbol );
	return ( index < __labels.size() && __labels[ index ] == symbol );
}

const size_t Regex::Symbols::GetIndex( const char & symbol ) const
{
	switch ( symbol )
	{
	case Symbols::EMPTY:	return ( __labels.size() );
	default:				return ( BinarySearch( __labels.begin(), __labels.end(), symbol, std::less< char >() ) - __labels.begin() );
	}
}

char & Regex::Symbols::operator [] ( const size_t index )
{
	return const_cast< char & >( static_cast< const Symbols & >( *this )[ index ] );
}

const char & Regex::Symbols::operator [] ( const size_t index ) const
{
	return ( index < __labels.size() ? __labels[ index ] : Symbols::EMPTY );
}

const bool Regex::IsOperand( const char token )
{
	switch ( token )
	{
	case Symbols::ANY:
	case Symbols::EMPTY:
		return ( true );
	default:
		return Alphabet.Contains( token );
	}
}

const bool Regex::IsBinaryOperator( const char token )
{
	switch ( token )
	{
	case OPERATOR_ALTERNATE:
	case OPERATOR_CONCATENATE:
		return ( true );
	default:
		return ( false );
	};
}

const bool Regex::IsUnaryPostOperator( const char token )
{
	switch ( token )
	{
	case OPERATOR_CLOSURE:
	case OPERATOR_POSITIVE_CLOSURE:
	case OPERATOR_OPTIONAL:
		return ( true );
	default:
		return ( false );
	};
}

const int Regex::Priority( const char token )
{
	switch ( token )
	{
	case OPERATOR_SUBEXPRESSION_START:	return ( PRIORITY_SUBEXPRESSION_START );
	case OPERATOR_ALTERNATE:			return ( PRIORITY_ALTERNATE );
	case OPERATOR_CONCATENATE:			return ( PRIORITY_CONCATENATE );
	case OPERATOR_CLOSURE:				return ( PRIORITY_CLOSURE );
	case OPERATOR_POSITIVE_CLOSURE:		return ( PRIORITY_POSITIVE_CLOSURE );
	case OPERATOR_OPTIONAL:				return ( PRIORITY_OPTIONAL );
	case OPERATOR_SUBEXPRESSION_END:	return ( PRIORITY_SUBEXPRESSION_END );
	default:							return ( -1 );
	}
}
