#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <set>
#include <stack>

#include "Algorithms.h"

template< typename T >
class LabelCollection
{
public:
	typedef typename T LabelType;

protected:
	std::vector< T > __labels;

public:
	const size_t Size() const { return ( __labels.size() ); }
	virtual void Initialize() = 0;
	const bool Contains( const T & symbol ) const;
	virtual const size_t GetIndex( const T & symbol ) const;
	virtual T & operator [] ( const size_t index ) { return ( __labels[ index ] ); }
	virtual const T & operator [] ( const size_t index ) const { return ( __labels[ index ] ); }
};

template< typename T >
const bool LabelCollection< T >::Contains( const T & label ) const
{
	const size_t index = GetIndex( label );
	return ( index < __labels.size() && __labels[ index ] == label );
}

template< typename T >
const size_t LabelCollection< T >::GetIndex( const T & label ) const
{
	return ( BinarySearch( __labels.begin(), __labels.end(), label, std::less< T >() ) - __labels.begin() );
}


template< typename Labels >
class Graph
{
private:
	std::vector< std::vector< std::set< size_t > > > __v;
	size_t __startState;
	std::set< size_t > __finalStates;
	const Labels & __labels;

public:
	Graph( const Labels & labels, const size_t statesCount = 0 )
		: __v( statesCount )
		, __labels( labels )
	{
		for ( size_t i = 0; i < __v.size(); ++i )
			__v[ i ].resize( __labels.Size() + 1 );
	}

public:
	Graph & operator = ( Graph & g )
	{
		__v = g.__v;
		__startState = g.__startState;
		__finalStates = g.__finalStates;

		return ( *this );
	}

	void Clear() { __v.clear(); }
	const size_t NewVertex();
	const size_t GetVertexCount() const { return ( __v.size() ); }

	std::set< size_t > operator()( const std::set< size_t > & fromSet, const size_t labelIndex ) const;
	std::set< size_t > & operator()( const size_t from, const typename Labels::LabelType & label );
	const std::set< size_t > & operator()( const size_t from, const typename Labels::LabelType & label ) const;
	std::set< size_t > & operator()( const size_t from, const size_t labelIndex );
	const std::set< size_t > & operator()( const size_t from, const size_t labelIndex ) const;

	void Closure( std::set< size_t > * const vertices, const typename Labels::LabelType & label ) const;

	void SetStartState( const size_t startState ) { __startState = startState; }
	const size_t GetStartState() const { return ( __startState ); }

	void ClearFinalStates() { __finalStates.clear(); }
	void AddFinalState( const size_t finalState ) { __finalStates.insert( finalState ); }
	const std::set< size_t > & GetFinalStates() const { return ( __finalStates ); }
	const std::set< size_t > GetNotFinalStates() const;

	void Print( std::ostream & out ) const;
};

template< typename Labels >
const size_t Graph< Labels >::NewVertex()
{
	__v.resize( __v.size() + 1 );
	__v.back().resize( __labels.Size() + 1 );

	return ( __v.size() - 1 );
}

template< typename Labels >
std::set< size_t > Graph< Labels >::operator()( const std::set< size_t > & fromSet, const size_t labelIndex ) const
{
	std::set< size_t > arrivalsUnion;

	for ( std::set< size_t >::const_iterator from = fromSet.begin(); from != fromSet.end(); ++from )
	{
		const std::set< size_t > & arrival = ( *this )( *from, labelIndex );
		arrivalsUnion.insert( arrival.begin(), arrival.end() );
	}

	return ( arrivalsUnion );
}

template< typename Labels >
std::set< size_t > & Graph< Labels >::operator()( const size_t from, const typename Labels::LabelType & label )
{
	return ( *this )( from, __labels.GetIndex( label ) );
}

template< typename Labels >
const std::set< size_t > & Graph< Labels >::operator()( const size_t from, const typename Labels::LabelType & label ) const
{
	return ( *this )( from, __labels.GetIndex( label ) );
}

template< typename Labels >
std::set< size_t > & Graph< Labels >::operator()( const size_t from, const size_t labelIndex )
{
	return const_cast< std::set< size_t > & >( static_cast< const Graph & >( *this )( from, labelIndex ) );
}

template< typename Labels >
const std::set< size_t > & Graph< Labels >::operator()( const size_t from, const size_t labelIndex ) const
{
	return ( __v[ from ][ labelIndex ] );
}

template< typename Labels >
void Graph< Labels >::Closure( std::set< size_t > * const vertices, const typename Labels::LabelType & label  ) const
{
	std::stack< size_t > reachedStates;
	for ( std::set< size_t >::const_iterator v = vertices->begin(); v != vertices->end(); ++v )
		reachedStates.push( *v );

	const size_t labelIndex = __labels.GetIndex( label );

	for ( ; !reachedStates.empty(); )
	{
		const size_t from = reachedStates.top();	reachedStates.pop();
		for ( size_t to = 0; to < __v[ from ].size(); ++to )
		{
			const std::set< size_t > & arrival = ( *this )( from, labelIndex );

			for ( std::set< size_t >::const_iterator u = arrival.begin(); u != arrival.end(); ++u )
				if ( vertices->find( *u ) == vertices->end() )
				{
					vertices->insert( *u );
					reachedStates.push( *u );
				}
		}
	}
}

template< typename Labels >
const std::set< size_t > Graph< Labels >::GetNotFinalStates() const
{
	std::set< size_t > allStates;
	for ( size_t i = 0; i < __v.size(); ++i )
		allStates.insert( allStates.end(), i );

	std::vector< size_t > difference( allStates.size() - __finalStates.size() );
	std::set_difference( allStates.begin(), allStates.end(), __finalStates.begin(), __finalStates.end(), difference.begin() );

	std::set< size_t > notFinalStates;
	for ( size_t i = 0; i < difference.size(); ++i )
		notFinalStates.insert( notFinalStates.end(), difference[ i ] );

	return ( notFinalStates );
}

template< typename Labels >
void Graph< Labels >::Print( std::ostream & out ) const
{
	out << "Start state:" << std::endl
		<< '\t' << __startState << std::endl;
	
	out << "Final states:" << std::endl
		<< "\t{ ";
	for ( std::set< size_t >::const_iterator finalState = __finalStates.begin(); finalState != __finalStates.end(); )
	{
		out << *finalState;
		out << ( ++finalState != __finalStates.end() ? ", " : " }\n" );
	}

	out << "Transition table:" << std::endl;
	for ( size_t fromIndex = 0; fromIndex < __v.size(); ++fromIndex )
		for ( size_t labelIndex = 0; labelIndex < __v[ fromIndex ].size(); ++labelIndex )
		{
			const std::set< size_t > & toSet = __v[ fromIndex ][ labelIndex ];

			if ( !toSet.empty() )
			{
				out << "\t( " << fromIndex << "\t, " << __labels[ labelIndex ] << " )\t->\t{ ";
				for ( std::set< size_t >::const_iterator toIndex = toSet.begin(); toIndex != toSet.end(); )
				{
					out << *toIndex;
					out << ( ++toIndex != toSet.end() ? ", " : " }\n" );
				}
			}
		}
}

#endif