#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>

#include "Regex.h"
#include "FSA.h"


int main()
{
	//freopen( "input.txt", "rt", stdin );

	Regex::Initialize();
	
	std::string regex;
	std::cin >> regex;

	FSA fsa( regex );
	std::cout << "Non-deterministic State Automata:" << std::endl;
	fsa.Print( std::cout );

	fsa.Determine();
	std::cout << std::endl << "Deterministic State Automata:" << std::endl;
	fsa.Print( std::cout );

	fsa.Minimize();
	std::cout << std::endl << "Minimalistic State Automata:" << std::endl;
	fsa.Print( std::cout );

	for ( std::boolalpha( std::cout ); !std::cin.eof(); )
	{
		std::string str;
		std::cin >> str;
		std::cout << fsa.CorrectString( str ) << std::endl;
	}

	return ( EXIT_SUCCESS );
}