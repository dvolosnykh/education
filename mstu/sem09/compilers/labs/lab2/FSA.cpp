#include "FSA.h"

#include <algorithm>
#include <queue>

#include "RegexToRPN.h"

const bool FSA::ConstructByRegex( const std::string & regex )
{
	RegexToRPN regexToRPN;
	std::vector< char > rpn;
	const bool correct = regexToRPN( &rpn, regex );
	if ( correct )
	{
		__graph.Clear();
		__ConstructByRPN( rpn );
	}

	return ( correct );
}

void FSA::Determine()
{
	state_t start;
	start.insert( __graph.GetStartState() );
	__graph.Closure( &start, Regex::Symbols::EMPTY );

	std::vector< state_t > states;
	std::stack< size_t > newStatesIndices;
	Graph< Regex::Symbols > newGraph( Regex::Alphabet );

	states.push_back( start );
	newStatesIndices.push( states.size() - 1 );
	newGraph.NewVertex();
	newGraph.SetStartState( newStatesIndices.top() );
	
	for ( ; !newStatesIndices.empty(); )
	{
		const size_t fromStateIndex = newStatesIndices.top();	newStatesIndices.pop();
		
		for ( size_t symbolIndex = 0; symbolIndex < Regex::Alphabet.Size(); ++symbolIndex )
		{
			state_t & toState = __graph( states[ fromStateIndex ], symbolIndex );

			if ( !toState.empty() )
			{
				__graph.Closure( &toState, ( char )Regex::Symbols::EMPTY );

				const size_t toStateIndex = std::find( states.begin(), states.end(), toState ) - states.begin();

				if ( toStateIndex == states.size() )
				{
					states.push_back( toState );
					newStatesIndices.push( states.size() - 1 );
					newGraph.NewVertex();
				}

				newGraph( fromStateIndex, symbolIndex ).insert( toStateIndex );
			}
		}
	}

	const std::set< size_t > & finalStates = __graph.GetFinalStates();
	for ( size_t i = 0; i < states.size(); ++i )
		if ( std::find_first_of( states[ i ].begin(), states[ i ].end(), finalStates.begin(), finalStates.end() ) != states[ i ].end() )
			newGraph.AddFinalState( i );

	__graph = newGraph;
}

void FSA::Minimize()
{
	std::list< state_t > partitions[ 2 ];

	size_t cur = 0;
	partitions[ cur ].push_back( __graph.GetFinalStates() );
	partitions[ cur ].push_back( __graph.GetNotFinalStates() );

	for ( bool enclosed = false; !enclosed; cur = 1 - cur )
	{
		std::list< state_t > & fromList = partitions[ cur ], & toList = partitions[ 1 - cur ];

		for ( std::list< state_t >::const_iterator from = fromList.begin(); from != fromList.end(); ++from )
		{
			state_t::const_iterator v = from->begin();

			toList.push_back( state_t() );
			std::list< state_t >::iterator toSublistBegin = --toList.end();
			toSublistBegin->insert( *v );

			for ( ; ++v != from->end(); )
			{
				std::list< state_t >::iterator to = toSublistBegin;
				for ( ; ; )
				{
					size_t symbolIndex = 0;
					for ( ; symbolIndex < Regex::Alphabet.Size() && __AreIndistinguishable( *v, *to->begin(), symbolIndex, fromList ); ++symbolIndex );

				if ( symbolIndex == Regex::Alphabet.Size() || ++to == toList.end() ) break;
				}

				if ( to == toList.end() )
				{
					toList.push_back( state_t() );
					to = --toList.rbegin().base();
				}

				to->insert( *v );
			}
		}

		enclosed = ( fromList.size() == toList.size() );

		fromList.clear();
	}

	const std::list< state_t > & states = partitions[ cur ];
	Graph< Regex::Symbols > newGraph( Regex::Alphabet, states.size() );

	const size_t & oldStartStateIndex = __graph.GetStartState();
	size_t startStateIndex = 0;
	for ( std::list< state_t >::const_iterator state = states.begin(); state != states.end() && state->find( oldStartStateIndex ) == state->end(); ++state, ++startStateIndex );
	newGraph.SetStartState( startStateIndex );

	const std::set< size_t > & oldFinalStates = __graph.GetFinalStates();
	size_t finalStateIndex = 0;
	for ( std::list< state_t >::const_iterator state = states.begin(); state != states.end(); ++state, ++finalStateIndex )
		if ( std::find_first_of( state->begin(), state->end(), oldFinalStates.begin(), oldFinalStates.end() ) != state->end() )
			newGraph.AddFinalState( finalStateIndex );

	size_t fromStateIndex = 0;
	for ( std::list< state_t >::const_iterator fromState = states.begin(); fromState != states.end(); ++fromState, ++fromStateIndex )
	{
		const size_t oldFromStateIndex = *fromState->begin();

		for ( size_t symbolIndex = 0; symbolIndex < Regex::Alphabet.Size(); ++symbolIndex )
		{
			const char symbol = Regex::Alphabet[ symbolIndex ];
			const std::set< size_t > & oldFromStateAccessorList = __graph( oldFromStateIndex, symbol );
			if ( !oldFromStateAccessorList.empty() )
			{
				const size_t oldToStateIndex = *oldFromStateAccessorList.begin();
				size_t toStateIndex = 0;
				for ( std::list< state_t >::const_iterator toState = states.begin(); toState != states.end() && toState->find( oldToStateIndex ) == toState->end(); ++toState, ++toStateIndex );

				newGraph( fromStateIndex, symbol ).insert( toStateIndex );
			}
		}
	}

	__graph = newGraph;
}

const bool FSA::CorrectString( const std::string & str ) const
{
	size_t state = __graph.GetStartState();

	bool quit = false;
	size_t i = 0;
	for ( ; !quit && i < str.length() && Regex::Alphabet.Contains( str[ i ] ); ++i )
	{
		const std::set< size_t > & toStates = __graph( state, str[ i ] );
		quit = toStates.empty();
		if ( !quit )
			state = *toStates.begin();
	}

	return ( i == str.length() && __graph.GetFinalStates().find( state ) != __graph.GetFinalStates().end() );
}

void FSA::__ConstructByRPN( const std::vector< char > & rpn )
{
	std::stack< ends_t > s;
	for ( size_t i = 0; i < rpn.size(); ++i )
	{
		const char & token = rpn[ i ];

		if ( Regex::IsOperand( token ) )
			__ConstructOnOperand( &s, token );
		else if ( Regex::IsUnaryPostOperator( token ) )
			__ConstructOnUnaryPostOperator( &s, token );
		else if ( Regex::IsBinaryOperator( token ) )
			__ConstructOnBinaryOperator( &s, token );
	}

	const ends_t result = s.top();	s.pop();
	__graph.SetStartState( result.start );
	__graph.ClearFinalStates();
	__graph.AddFinalState( result.final );
}

void FSA::__ConstructOnBinaryOperator( std::stack< ends_t > * const s, const char token )
{
	std::pair< ends_t, ends_t > operands;
	operands.second = s->top(); s->pop();
	operands.first = s->top(); s->pop();

	ends_t result;
	switch ( token )
	{
	case Regex::OPERATOR_ALTERNATE:		result = __ConstructOnAlternate( operands );	break;
	case Regex::OPERATOR_CONCATENATE:	result = __ConstructOnConcatenate( operands );	break;
	}

	s->push( result );
}

const FSA::ends_t FSA::__ConstructOnAlternate( const std::pair< ends_t, ends_t > & operands )
{
	const ends_t result = { __graph.NewVertex(), __graph.NewVertex() };

	__graph( result.start, ( char )Regex::Symbols::EMPTY ).insert( operands.first.start );
	__graph( result.start, ( char )Regex::Symbols::EMPTY ).insert( operands.second.start );
	__graph( operands.first.final, ( char )Regex::Symbols::EMPTY ).insert( result.final );
	__graph( operands.second.final, ( char )Regex::Symbols::EMPTY ).insert( result.final );

	return ( result );
}

const FSA::ends_t FSA::__ConstructOnConcatenate(const std::pair< ends_t, ends_t > & operands )
{
	const ends_t result = { operands.first.start, operands.second.final };

	__graph( operands.first.final, ( char )Regex::Symbols::EMPTY ).insert( operands.second.start );

	return ( result );
}

void FSA::__ConstructOnUnaryPostOperator( std::stack< ends_t > * const s, const char token )
{
	const ends_t operand = s->top(); s->pop();

	ends_t result;
	switch ( token )
	{
	case Regex::OPERATOR_POSITIVE_CLOSURE:	result = __ConstructOnPositiveClosure( operand );	break;
	case Regex::OPERATOR_CLOSURE:			result = __ConstructOnClosure( operand );			break;
	};
	
	s->push( result );
}

const FSA::ends_t FSA::__ConstructOnPositiveClosure( const ends_t & operand )
{
	const ends_t result = { operand.start, __graph.NewVertex() };
	
	__graph( operand.final, ( char )Regex::Symbols::EMPTY ).insert( operand.start );
	__graph( operand.final, ( char )Regex::Symbols::EMPTY ).insert( result.final );
	
	return ( result );
}

const FSA::ends_t FSA::__ConstructOnClosure( const ends_t & operand )
{
	const ends_t result = { __graph.NewVertex(), __graph.NewVertex() };
	
	__graph( result.start, ( char )Regex::Symbols::EMPTY ).insert( operand.start );
	__graph( operand.final, ( char )Regex::Symbols::EMPTY ).insert( operand.start );
	__graph( operand.final, ( char )Regex::Symbols::EMPTY ).insert( result.final );
	__graph( result.start, ( char )Regex::Symbols::EMPTY ).insert( result.final );

	return ( result );
}

void FSA::__ConstructOnOperand( std::stack< ends_t > * const s, const char token )
{
	const ends_t result = { __graph.NewVertex(), __graph.NewVertex() };

	if ( token == Regex::Symbols::ANY )
	{
		for ( size_t symbolIndex = 0; symbolIndex < Regex::Alphabet.Size(); ++symbolIndex )
			__graph( result.start, symbolIndex ).insert( result.final );
	}
	else
		__graph( result.start, token ).insert( result.final );

	s->push( result );
}

const bool FSA::__AreIndistinguishable( size_t u, size_t v, const size_t symbolIndex, const std::list< state_t > & partitions ) const
{
	const std::set< size_t > & uAccessorList = __graph( u, symbolIndex );
	const std::set< size_t > & vAccessorList = __graph( v, symbolIndex );

	bool indistinguishable = ( uAccessorList.empty() && vAccessorList.empty() );

	if ( !indistinguishable && !uAccessorList.empty() && !vAccessorList.empty() )
	{
		u = *uAccessorList.begin();
		v = *vAccessorList.begin();

		bool uIsPresent, vIsPresent;
		bool found = false;
		for ( std::list< state_t >::const_iterator p = partitions.begin(); !found && p != partitions.end(); ++p )
		{
			uIsPresent = ( p->find( u ) != p->end() );
			vIsPresent = ( p->find( v ) != p->end() );
			found = ( uIsPresent || vIsPresent );
		}

		indistinguishable = ( uIsPresent == vIsPresent );
	}

	return ( indistinguishable );
}