#ifndef REGEX_H
#define REGEX_H

#include <vector>
#include <limits>

#include "Graph.h"

class Regex
{
public:
	class Symbols : public LabelCollection< char >
	{
	public:
		enum { EMPTY = '_', ANY = '.' };

	public:
		virtual void Initialize();
		const bool Contains( const char symbol ) const;
		virtual const size_t GetIndex( const char & symbol ) const;
		virtual char & operator [] ( const size_t index );
		virtual const char & operator [] ( const size_t index ) const;
	};

	enum OPERATORS
	{
		OPERATOR_SUBEXPRESSION_START = '(',
		OPERATOR_ALTERNATE = '|',
		OPERATOR_CONCATENATE = '&',
		OPERATOR_CLOSURE = '*',
		OPERATOR_POSITIVE_CLOSURE = '+',
		OPERATOR_OPTIONAL = '?',
		OPERATOR_SUBEXPRESSION_END = ')'
	};

	enum PRIORITIES
	{
		PRIORITY_SUBEXPRESSION_START,
		PRIORITY_ALTERNATE,
		PRIORITY_CONCATENATE,
		PRIORITY_CLOSURE,
		PRIORITY_POSITIVE_CLOSURE = PRIORITY_CLOSURE,
		PRIORITY_OPTIONAL = PRIORITY_CLOSURE,
		PRIORITY_SUBEXPRESSION_END
	};

public:
	static Symbols Alphabet;

public:
	static void Initialize() { Alphabet.Initialize(); }

	static const bool IsOperand( const char token );
	static const bool IsBinaryOperator( const char token );
	static const bool IsUnaryPostOperator( const char token );

	static const int Priority( const char token );

private:
	Regex( const Regex & );
	~Regex();
	Regex & operator = ( const Regex & );
};

#endif