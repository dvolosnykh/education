#ifndef REGEXTORPN_H
#define REGEXTORPN_H

#include <stack>
#include <vector>
#include <string>

#include "Regex.h"

class RegexToRPN
{
private:
	std::stack< char > __s;
	std::stack< size_t > __bottoms;
	bool __emptyExpr, __prevWasOperand;

public:
	const bool operator()( std::vector< char > * const rpn, const std::string & regex );

private:
	const bool __WorkOnOperand( std::vector< char > * const rpn, const char cur );
	const bool __WorkOnSubexpressionStart();
	const bool __WorkOnSubexpressionEnd( std::vector< char > * const rpn, const bool wholeExpressionEnded = false );
	const bool __WorkOnOperator( std::vector< char > * const rpn, const char op );
};

#endif