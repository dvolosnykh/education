#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

const std::string PARANTHESIS_OPEN = "(", PARANTHESIS_CLOSE = ")";
const std::string PARAMETER_ASSOCIATION = "=>";
const char DECIMAL_SEPARATOR = '.';
const char UNDERSCORE = '_';
const char MINUS = '-', PLUS = '+';
const char BASE = '#';
const char EXPONENT = 'E';

std::vector< std::string > LogicalOperators;
std::vector< std::string > RelationalOperators;
std::vector< std::string > BinaryAdditiveOperators;
std::vector< std::string > UnaryAdditiveOperators;
std::vector< std::string > MultiplicativeOperators;
std::vector< std::string > BinaryHighestPriorityOperators;
std::vector< std::string > UnaryHighestPriorityOperators;
std::vector< std::string > CommaOperator;

typedef const bool ( * ReadLexem )( const std::string & text, size_t * const index );

std::vector< ReadLexem > PrimaryAlternatives;
std::vector< ReadLexem > ExplicitActualParameterAlternatives;


const bool ReadExpression( const std::string & text, size_t * const index );

inline
void SkipWhitespaces( const std::string & text, size_t * const index )
{
	while ( *index < text.length() && isspace( text[ *index ] ) )
		++*index;
}

inline
const bool ReadSymbol( const char desiredSymbol, const std::string & text, size_t * const index )
{
	const bool ok = ( text[ *index ] == desiredSymbol );
	if ( ok )
		++*index;

	return ( ok );
}

inline
const bool ReadPrefix( const std::string & prefix, const std::string & text, size_t * const index )
{
	SkipWhitespaces( text, index );
	const bool ok = ( *index + prefix.length() < text.length() && std::equal( prefix.begin(), prefix.end(), text.begin() + *index ) );
	if ( ok )
		*index += prefix.length();

	return ( ok );
}

inline
const bool ReadSeparator( const std::string & text, size_t * const index, const std::vector< std::string > & separators )
{
	size_t i = 0;
	for ( ; i < separators.size() && !ReadPrefix( separators[ i ], text, index ); ++i )
		continue;

	return ( i < separators.size() );
}

inline
const bool ReadList( const std::string & text, size_t * const index, const ReadLexem ReadListElement, const std::vector< std::string > & separators )
{
	bool ok = ReadListElement( text, index );

	while ( ok && ReadSeparator( text, index, separators ) )
		ok = ReadListElement( text, index );

	return ( ok );
}

inline
const bool ReadAlternative( const std::string & text, size_t * const index, std::vector< ReadLexem > & alternatives )
{
	bool matched = false;
	for ( size_t i = 0; i < alternatives.size() && !matched; ++i )
	{
		const size_t saveIndex = *index;
		matched = alternatives[ i ]( text, index );
		if ( !matched )
			*index = saveIndex;
	}

	return ( matched );
}

inline
const bool ReadSubexpression( const std::string & text, size_t * const index )
{
	bool ok = true;
	
	if ( ok )
		ok = ReadPrefix( PARANTHESIS_OPEN, text, index );

	if ( ok )
		ok = ReadExpression( text, index );

	if ( ok )
		ok = ReadPrefix( PARANTHESIS_CLOSE, text, index );

	return ( ok );
}

inline
const bool ReadNumeral( const std::string & text, size_t * const index )
{
	bool ok = ( isdigit( text[ *index ] ) != 0 );

	if ( ok )
	{
		do
		{
			++*index;
			ReadSymbol( UNDERSCORE, text, index );
		}
		while ( isdigit( text[ *index ] ) );
	}

	return ( true );
}

inline
const bool ReadExponent( const std::string & text, size_t * const index )
{
	bool ok = ReadSymbol( EXPONENT, text, index );

	if ( ok )
	{
		const bool wasMinus = ReadSymbol( MINUS, text, index );
		if ( !wasMinus )
			ReadSymbol( PLUS, text, index );

		ok = ReadNumeral( text, index );
	}

	return ( ok );
}

inline
const bool ReadNumericLiteral( const std::string & text, size_t * const index )
{
	SkipWhitespaces( text, index );
	bool ok = ReadNumeral( text, index );

	if ( ok )
	{
		const bool wasBase = ReadSymbol( BASE, text, index );
		if ( wasBase )
			ok = ReadNumeral( text, index );

		if ( ok )
		{
			const bool wasDecimalSeparator = ReadSymbol( DECIMAL_SEPARATOR, text, index );
			if ( wasDecimalSeparator )
				ok = ReadNumeral( text, index );
		}

		if ( wasBase )
			ok = ReadSymbol( BASE, text, index );
	}

	if ( ok )
		ReadExponent( text, index );

	return ( ok );
}

inline
const bool ReadIdentifier( const std::string & text, size_t * const index )
{
	SkipWhitespaces( text, index );

	bool ok = ( isalpha( text[ *index ] ) != 0 );

	if ( ok )
	{
		do
		{
			if ( text[ ++*index ] == UNDERSCORE )
				++*index;
		}
		while ( isalnum( text[ *index ] ) );
	}

	return ( ok );
}

inline
const bool ReadTypeConversion( const std::string & text, size_t * const index )
{
	bool ok = ReadIdentifier( text, index );
	if ( ok )
		ok = ReadSubexpression( text, index );

	return ( ok );
}

inline
const bool ReadExplicitActualParameter( const std::string & text, size_t * const index )
{
	return ReadAlternative( text, index, ExplicitActualParameterAlternatives );
}

inline
const bool ReadParameterAssociation( const std::string & text, size_t * const index )
{
	bool ok = true;
	const bool wasId = ReadIdentifier( text, index );
	if ( wasId )
		ok = ReadPrefix( PARAMETER_ASSOCIATION, text, index );

	if ( ok )
		ok = ReadExplicitActualParameter( text, index );

	return ( ok );
}

inline
const bool ActualParametersPart( const std::string & text, size_t * const index )
{
	bool ok = true;
	
	if ( ok )
		ok = ReadPrefix( PARANTHESIS_OPEN, text, index );

	if ( ok )
		ok = ReadList( text, index, &ReadParameterAssociation, CommaOperator );

	if ( ok )
		ok = ReadPrefix( PARANTHESIS_CLOSE, text, index );

	return ( ok );
}

inline
const bool ReadFunctionCall( const std::string & text, size_t * const index )
{
	bool ok = ReadIdentifier( text, index );
	if ( ok )
		ok = ActualParametersPart( text, index );

	return ( ok );
}

inline
const bool ReadPrimary( const std::string & text, size_t * const index )
{
	//bool ok = ReadIdentifier( text, index );
	//if ( ok )
	//	;
	return ReadAlternative( text, index, PrimaryAlternatives );
	//return ( true );
}

inline
const bool ReadFactor( const std::string & text, size_t * const index )
{
	const bool wasUnary = ReadSeparator( text, index, UnaryHighestPriorityOperators );

	bool ok = ReadPrimary( text, index );

	if ( ok && !wasUnary )
	{
		const bool wasBinary = ReadSeparator( text, index, BinaryHighestPriorityOperators );

		if ( wasBinary )
			ok = ReadPrimary( text, index );
	}

	return ( ok );
}

inline
const bool ReadTerm( const std::string & text, size_t * const index )
{
	return ReadList( text, index, ReadFactor, MultiplicativeOperators );
}

inline
const bool ReadSimpleExpression( const std::string & text, size_t * const index )
{
	ReadSeparator( text, index, UnaryAdditiveOperators );
	return ReadList( text, index, ReadTerm, BinaryAdditiveOperators );
}

inline
const bool ReadRelation( const std::string & text, size_t * const index )
{
	return ReadList( text, index, ReadSimpleExpression, RelationalOperators );
}

inline
const bool ReadExpression( const std::string & text, size_t * const index )
{
	return ReadList( text, index, ReadRelation, LogicalOperators );
}

inline
const std::string Trim( const std::string & str, const std::string & characters = " \r\n" )
{
	const size_t begin = str.find_first_not_of( characters );
	const size_t end = ( begin != std::string::npos ? str.find_last_not_of( characters ) : std::string::npos );
	return ( begin != std::string::npos ? str.substr( begin, end - begin + 1 ) : std::string() );
}

inline
const bool ReadExpression( std::string text )
{
	text = Trim( text );

	size_t index = 0;
	const bool ok = ReadExpression( text, &index );
	std::cout << index << '\t' << text.length() << std::endl;
	return ( ok && index == text.length() );
}

void Initialize()
{
	const char * const logical[] = { "and", "or", "xor" };
	const size_t logicalCount = sizeof( logical ) / sizeof( logical[ 0 ] );
	LogicalOperators.insert( LogicalOperators.begin(), &logical[ 0 ], &logical[ 0 ] + logicalCount );

	const char * const relational[] = { "=", "/=", "<", "<=", ">", ">=" };
	const size_t relationalCount = sizeof( relational ) / sizeof( relational[ 0 ] );
	RelationalOperators.insert( RelationalOperators.begin(), &relational[ 0 ], &relational[ 0 ] + relationalCount );

	const char * const binaryAdditive[] = { "+", "-", "&" };
	const size_t binaryAdditiveCount = sizeof( binaryAdditive ) / sizeof( binaryAdditive[ 0 ] );
	BinaryAdditiveOperators.insert( BinaryAdditiveOperators.begin(), &binaryAdditive[ 0 ], &binaryAdditive[ 0 ] + binaryAdditiveCount );

	const char * const unaryAdditive[] = { "+", "-" };
	const size_t unaryAdditiveCount = sizeof( unaryAdditive ) / sizeof( unaryAdditive[ 0 ] );
	UnaryAdditiveOperators.insert( UnaryAdditiveOperators.begin(), &unaryAdditive[ 0 ], &unaryAdditive[ 0 ] + unaryAdditiveCount );

	const char * const multiplicative[] = { "*", "/", "mod", "rem" };
	const size_t multiplicativeCount = sizeof( multiplicative ) / sizeof( multiplicative[ 0 ] );
	MultiplicativeOperators.insert( MultiplicativeOperators.begin(), &multiplicative[ 0 ], &multiplicative[ 0 ] + multiplicativeCount );

	const char * const binaryHighestPriority[] = { "abs", "not" };
	const size_t binaryHighestPriorityCount = sizeof( binaryHighestPriority ) / sizeof( binaryHighestPriority[ 0 ] );
	BinaryHighestPriorityOperators.insert( BinaryHighestPriorityOperators.begin(), &binaryHighestPriority[ 0 ], &binaryHighestPriority[ 0 ] + binaryHighestPriorityCount );

	const char * const unaryHighestPriority[] = { "**" };
	const size_t unaryHighestPriorityCount = sizeof( unaryHighestPriority ) / sizeof( unaryHighestPriority[ 0 ] );
	UnaryHighestPriorityOperators.insert( UnaryHighestPriorityOperators.begin(), &unaryHighestPriority[ 0 ], &unaryHighestPriority[ 0 ] + unaryHighestPriorityCount );

	CommaOperator.push_back( "," );

	PrimaryAlternatives.push_back( &ReadIdentifier );
	PrimaryAlternatives.push_back( &ReadNumericLiteral );
	PrimaryAlternatives.push_back( &ReadTypeConversion );
	PrimaryAlternatives.push_back( &ReadFunctionCall );
	PrimaryAlternatives.push_back( &ReadSubexpression );

	ExplicitActualParameterAlternatives.push_back( &ReadExpression );
	ExplicitActualParameterAlternatives.push_back( &ReadTypeConversion );
}

int main()
{
	freopen( "input.txt", "rt", stdin );

	Initialize();
	std::string text;
	std::getline( std::cin, text, '\0' );
	std::cout << std::boolalpha << ReadExpression( text ) << std::endl;

	return ( EXIT_SUCCESS );
}