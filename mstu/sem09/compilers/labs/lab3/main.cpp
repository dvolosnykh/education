#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "Grammar.h"

int main()
{
	try
	{
		std::ifstream fin;
		std::ofstream fout;

		Grammar grammar;

		fin.open( "InitialGrammar.txt" );
		fin >> grammar;
		fin.close(), fin.clear();

		grammar.DeleteLeftRecursion();

		fout.open( "TransformedGrammar.txt" );
		fout << grammar;
		fout.close(), fout.clear();

		fin.open( "Test.adb" );
		std::string text;
		std::getline( fin, text, '\0' );
		fin.close(), fin.clear();

		fout.open( "output.txt" );
		fout << std::boolalpha << grammar.Accepts( text, fout );
		fout.close(), fout.clear(); 
	}
	catch ( const std::bad_exception & ex )
	{
		std::cout << ex.what() << std::endl;
	}

	return ( EXIT_SUCCESS );
}