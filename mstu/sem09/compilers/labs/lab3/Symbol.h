#ifndef SYMBOL_H
#define SYMBOL_H

#include <iostream>
#include <string>
#include <memory>
#include <assert.h>

class Symbol;
class Grammar;
class Configuration;

typedef std::tr1::shared_ptr< Symbol > SymbolPtr;

class Symbol
{
public:
	virtual void WriteSimple( std::ostream & out ) const = 0;
	virtual const bool Try( Configuration * const configuraion, const Grammar & grammar ) const = 0;
};

class Terminal : public Symbol
{
public:
	char Literal;

public:
	Terminal() {}
	Terminal( const char literal )
		: Literal( literal ) {}

public:
	virtual void WriteSimple( std::ostream & out ) const;
	virtual const bool Try( Configuration * const configuraion, const Grammar & grammar ) const;
};

class Nonterminal : public Symbol
{
public:
	size_t RuleIndex;

public:
	Nonterminal() {}
	Nonterminal( const size_t ruleIndex )
		: RuleIndex( ruleIndex ) {}

public:
	virtual void WriteSimple( std::ostream & out ) const;
	virtual const bool Try( Configuration * const configuraion, const Grammar & grammar ) const;
};

class Whitespace : public Symbol
{
public:
	virtual void WriteSimple( std::ostream & out ) const;
	virtual const bool Try( Configuration * const configuraion, const Grammar & grammar ) const;
};

class ChosenAlternative : public Symbol
{
public:
	SymbolPtr Nonterminal;
	size_t AlternativeIndex;
	size_t SavedPosition;

public:
	ChosenAlternative( const SymbolPtr & nonterminal, const size_t alternativeIndex, const size_t savedPosition )
		: Nonterminal( nonterminal )
		, AlternativeIndex( alternativeIndex )
		, SavedPosition( savedPosition ) {}

public:
	virtual void WriteSimple( std::ostream & out ) const;
	virtual const bool Try( Configuration * const configuraion, const Grammar & grammar ) const { assert( false ); return ( false ); }

	const size_t GetRuleIndex() const { return ( ( ( ::Nonterminal * )Nonterminal.get() )->RuleIndex ); }
};

#endif