#include <ctype.h>
#include <assert.h>

#include "Common.h"


void SkipWhitespaces( std::istream & in )
{
	while ( in.good() && isspace( in.peek() ) )
		in.get();
}

void SkipWhitespaces( const std::string & str, size_t * const pos )
{
	while ( *pos < str.length() && isspace( str[ *pos ] ) )
		++*pos;
}

const std::string ReadLiteral( std::istream & in )
{
	SkipWhitespaces( in );

	std::string literal;
	switch ( in.peek() )
	{
	case STRING_LITERAL:
		literal = ReadStringLiteral( in );
		break;

	case CHARACTER_LITERAL:
		literal.push_back( ReadCharacterLiteral( in ) );
		break;

	default:
		assert( false );
		break;
	}

	return ( literal );
}

void WriteLiteral( const std::string & literal, std::ostream & out )
{
	const char & boundarySymbol = ( literal.length() > 1 ? STRING_LITERAL : CHARACTER_LITERAL );
	out << boundarySymbol;
	for ( size_t i = 0; i < literal.length(); ++i )
		WriteSingleChar( literal[ i ], out, boundarySymbol );
	out << boundarySymbol;
}

const std::string ReadStringLiteral( std::istream & in )
{
	std::string literal;
	int prev = in.get();
	assert( prev == STRING_LITERAL );
	while ( true )
	{
		bool composite;
		int cur = ReadSingleChar( in, &composite );

	if ( in.fail() || cur == STRING_LITERAL && !composite ) break;

		literal.push_back( cur );
		prev = cur;
	}

	return ( literal );
}

const char ReadCharacterLiteral( std::istream & in )
{
	int c = in.get();
	assert( c == CHARACTER_LITERAL );

	const char literal = ReadSingleChar( in );

	c = in.get();
	assert( c == CHARACTER_LITERAL );

	return ( literal );
}

const std::string ReadNonterminal( std::istream & in )
{
	if ( !isalpha( in.peek() ) )
		throw std::bad_exception( "Nonterminal should start with letter." );

	std::string nonterminal;
	while ( !in.fail() && ( isalnum( in.peek() ) || in.peek() == '_' ) )
		nonterminal.push_back( in.get() );

	return ( nonterminal );
}

const char ReadSingleChar( std::istream & in, bool * const composite )
{
	int c = in.get();
	if ( composite != NULL )
		*composite = ( c == ESCAPE );

	if ( c == ESCAPE )
	{
		c = in.get();
		switch ( c )
		{
		case 'n':	c = '\n';	break;
		case 'r':	c = '\r';	break;
		case 't':	c = '\t';	break;
		}
	}

	return ( c );
}

void WriteSingleChar( const char c, std::ostream & out, const char boundarySymbol )
{
	if ( c == '\n' || c == '\r' || c == '\t' || c == boundarySymbol )
		out << ESCAPE;

	out << c;
}