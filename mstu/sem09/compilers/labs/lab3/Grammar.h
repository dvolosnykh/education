#ifndef GRAMMAR_H
#define GRAMMAR_H

#include <vector>
#include <set>
#include <memory>

#include "Rule.h"


class Grammar
{
public:
	std::string SINGLE_LINE_COMMENT_BEGIN;
	std::string SINGLE_LINE_COMMENT_END;

public:
	std::vector< RulePtr > Rules;
	std::set< std::string > Keywords;

public:
	void DeleteLeftRecursion();
	const bool Accepts( std::string text, std::ostream & out ) const;

	const bool ReadRule( std::istream & in );
	void WriteRule( const size_t ruleIndex, std::ostream & out ) const;

	const size_t GetProductionIndex( const std::string & newNonterminal );

private:
	const std::string __DeleteComments( const std::string & text ) const;
};

std::istream & operator >> ( std::istream & in, Grammar & grammar );
std::ostream & operator << ( std::ostream & out, const Grammar & grammar );

enum STATES { STATE_NORMAL = 'q', STATE_ROLLBACK = 'r', STATE_TERMINATED = 't' };

class Configuration
{
public:
	STATES State;
	const std::string & Input;
	size_t Position;
	std::list< SymbolPtr > Alpha;
	std::list< SymbolPtr > Beta;

public:
	Configuration( const std::string & input )
		: State( STATE_NORMAL )
		, Input( input )
		, Position( 0 )
		, Alpha()
		, Beta( 1, SymbolPtr( new Nonterminal( 0 ) ) ) {}
};

std::ostream & operator << ( std::ostream & out, const Configuration & configuration );

#endif