#include <assert.h>

#include "Rule.h"
#include "Grammar.h"


const bool Alternative::__IsRecursive( const size_t ruleIndex, const bool left ) const
{
	bool isNonRecursive = this->empty();
	if ( !isNonRecursive )
	{
		const Nonterminal * const nonterminal = dynamic_cast< Nonterminal * >( ( left ? this->front() : this->back() ).get() );
		isNonRecursive = ( nonterminal == NULL );
		if ( !isNonRecursive )
			isNonRecursive = ( nonterminal->RuleIndex != ruleIndex );
	}

	return ( !isNonRecursive );
}

const size_t Rule::FindNonRecursiveAlternative( const Grammar & grammar ) const
{
	const std::vector< AlternativePtr > & alternatives = grammar.Rules[ Index ]->Alternatives;

	size_t nonRecursiveIndex = 0;
	for ( ; nonRecursiveIndex < alternatives.size() && alternatives[ nonRecursiveIndex ]->IsLeftRecursive( Index ); ++nonRecursiveIndex )
		continue;
	assert( nonRecursiveIndex < alternatives.size() );

	return ( nonRecursiveIndex );
}