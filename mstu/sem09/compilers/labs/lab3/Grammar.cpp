#include "Common.h"
#include "Grammar.h"

#include <algorithm>
#include <functional>
#include <assert.h>
#include <ctype.h>
#include <limits>


static
const std::string GenerateNewNonterminalName()
{
	static unsigned index = 0;
	static char indexStr[ 256 ];
	return ( std::string( "New" ) + itoa( ++index, indexStr, 10 ) );
}

void Grammar::DeleteLeftRecursion()
{
	const size_t rulesInitialCount = Rules.size();
	for ( size_t ruleIndex = 0; ruleIndex < rulesInitialCount; ++ruleIndex )
	{
		Rule & rule = *Rules[ ruleIndex ];
		assert( !rule.Alternatives.empty() );

		const size_t nonRecursiveIndex = rule.FindNonRecursiveAlternative( *this );
		Alternative & nonRecursive = *rule.Alternatives[ nonRecursiveIndex ];

		for ( size_t alternativeIndex = 0; alternativeIndex < rule.Alternatives.size(); )
		{
			AlternativePtr & alternative = rule.Alternatives[ alternativeIndex ];
			if ( alternative->IsLeftRecursive( ruleIndex ) )
			{
				alternative->pop_front();

				const size_t newNonterminalIndex = GetProductionIndex( GenerateNewNonterminalName() );
				const SymbolPtr header( new Nonterminal( newNonterminalIndex ) );

				const bool withWhitespace = ( dynamic_cast< Whitespace * >( alternative->front().get() ) != NULL );
				if ( withWhitespace )
					nonRecursive.push_back( SymbolPtr( new Whitespace() ) );
				nonRecursive.push_back( SymbolPtr( new Nonterminal( newNonterminalIndex ) ) );

				Rule & newRule = *Rules[ newNonterminalIndex ];
				if ( withWhitespace )
					alternative->push_back( SymbolPtr( new Whitespace() ) );
				alternative->push_back( SymbolPtr( new Nonterminal( newNonterminalIndex ) ) );
				newRule.Alternatives.push_back( alternative );

				rule.Alternatives.erase( rule.Alternatives.begin() + alternativeIndex );

				newRule.Alternatives.push_back( AlternativePtr( new Alternative() ) );
			}
			else
				++alternativeIndex;
		}
	}
}

const bool Grammar::Accepts( std::string text, std::ostream & out ) const
{
	text = __DeleteComments( text );
	text = Trim( text );
	Configuration configuration( text );

	out << configuration << std::endl;
	bool finished = false;
	while ( !finished )
	{
		const SymbolPtr frontSymbol = configuration.Beta.front();
		finished = !frontSymbol->Try( &configuration, *this );

		out << configuration << std::endl;
	}

	if ( configuration.Beta.empty() && configuration.Position == configuration.Input.length() )
	{
		configuration.State = STATE_TERMINATED;
		out << configuration << std::endl;
	}

	return ( configuration.State == STATE_TERMINATED );
}


const bool Grammar::ReadRule( std::istream & in )
{
	SkipWhitespaces( in );

	bool ok;
	std::string header;
	try
	{
		header = ReadNonterminal( in );
		ok = in.good();
	}
	catch ( std::bad_exception & ex )
	{
		in >> header;
		if ( header != SECTION_RULES_END ) throw;
		ok = false;
	}

	if ( ok )
	{
		const size_t ruleIndex = GetProductionIndex( header );
		Rule & rule = *Rules[ ruleIndex ];

		std::string splitter;
		in >> splitter;
		assert( splitter == RULE_PARTS_SPLITTER );

		do
		{
			AlternativePtr alternative( new Alternative() );
			while ( true )
			{
				SkipWhitespaces( in );

			if ( in.peek() == RIGHT_PARTS_SPLITTER || in.peek() == END_RULE ) break;

				if ( in.peek() == CONCATENATE )
				{
					in.get();
					SkipWhitespaces( in );
				}
				else if ( !alternative->empty() )
					alternative->push_back( SymbolPtr( new Whitespace() ) );

				switch ( in.peek() )
				{
				case STRING_LITERAL:
				case CHARACTER_LITERAL:
					{
						const std::string literal = ReadLiteral( in );
						for ( size_t i = 0; i < literal.length(); ++i )
							alternative->push_back( SymbolPtr( new Terminal( literal[ i ] ) ) );
					}
					break;

				default:
					{
						const std::string nonterminalName = ReadNonterminal( in );
						const size_t nonterminalIndex = GetProductionIndex( nonterminalName );
						alternative->push_back( SymbolPtr( new Nonterminal( nonterminalIndex ) ) );
					}
					break;
				}
			}

			rule.Alternatives.push_back( alternative );
		}
		while ( in.get() != END_RULE );
	}

	return ( ok );
}

void Grammar::WriteRule( const size_t ruleIndex, std::ostream & out ) const
{
	const Rule & rule = *Rules[ ruleIndex ];

	out << ruleIndex + 1 << ":\t" << rule.Header << ' ' << RULE_PARTS_SPLITTER << std::endl;

	for ( size_t alternativeIndex = 0; alternativeIndex < rule.Alternatives.size(); ++alternativeIndex )
	{
		const Alternative & alternative = *rule.Alternatives[ alternativeIndex ];
		out << "\t\t\t";
		if ( !alternative.empty() )
		{
			for ( Alternative::const_iterator symbol = alternative.begin(); symbol != alternative.end(); )
				if ( dynamic_cast< Whitespace * >( symbol->get() ) == NULL ) 
				{
					if ( dynamic_cast< Terminal * >( symbol->get() ) != NULL )
					{
						std::string terminal;
						for ( ; symbol != alternative.end() && dynamic_cast< Terminal * >( symbol->get() ) != NULL; ++symbol )
							terminal.push_back( ( ( Terminal * ) symbol->get() )->Literal );

						WriteLiteral( terminal, out );
					}
					else // if ( dynamic_cast< Nonterminal * >( symbol->get() ) != NULL )
					{
						const Nonterminal * const nonterminal = ( Nonterminal * )symbol->get();
						const std::string & name = Rules[ nonterminal->RuleIndex ]->Header;
						out << name;

						++symbol;
					}

					if ( symbol != alternative.end() )
						out << ( dynamic_cast< Whitespace * >( symbol->get() ) != NULL ? " " : " + " );
				}
				else
					++symbol;
		}

		if ( alternativeIndex + 1 < rule.Alternatives.size() )
			out << ' ' << RIGHT_PARTS_SPLITTER << std::endl;
		else
			out << " ;";
	}
}

const size_t Grammar::GetProductionIndex( const std::string & header )
{
	struct HeaderIsEqualTo : public std::binary_function< RulePtr, std::string, bool >
	{
		const bool operator() ( const RulePtr & rule, const std::string & header ) const { return ( rule->Header == header ); }
	};

	const std::vector< RulePtr >::const_iterator iter = std::find_if( Rules.begin(), Rules.end(), std::bind2nd( HeaderIsEqualTo(), header ) );
	size_t index;
	if ( iter == Rules.end() )
	{
		index = Rules.size();
		Rules.push_back( RulePtr( new Rule( header, index ) ) );
	}
	else
		index = iter - Rules.begin();

	return ( index );
}

const std::string Grammar::__DeleteComments( const std::string & text ) const
{
	std::string newText;
	for ( size_t i = 0; i < text.length(); )
	{
		while ( i + SINGLE_LINE_COMMENT_BEGIN.length() < text.length() && !std::equal( SINGLE_LINE_COMMENT_BEGIN.begin(), SINGLE_LINE_COMMENT_BEGIN.end(), text.begin() + i ) )
			newText.push_back( text[ i++ ] );

		if ( i + SINGLE_LINE_COMMENT_BEGIN.length() < text.length() )
		{
			i += SINGLE_LINE_COMMENT_BEGIN.length();
			while ( i + SINGLE_LINE_COMMENT_END.length() < text.length() && !std::equal( SINGLE_LINE_COMMENT_END.begin(), SINGLE_LINE_COMMENT_END.end(), text.begin() + i ) )
				++i;
			i += SINGLE_LINE_COMMENT_END.length();
		}
		else
		{
			while ( i < text.length() )
				newText.push_back( text[ i++ ] );
		}
	}

	return ( newText );
}

std::istream & operator >> ( std::istream & in, Grammar & grammar )
{
	while ( true )
	{
		std::string section;
		in >> section;

	if ( in.eof() ) break;

		if ( section == SECTION_RULES_BEGIN )
		{
			while ( grammar.ReadRule( in ) )
				continue;
		}
		else if ( section == SECTION_KEYWORDS_BEGIN )
		{
			while ( true )
			{
				std::string keyword;
				in >> keyword;
				std::transform( keyword.begin(), keyword.end(), keyword.begin(), tolower );

			if ( in.fail() || keyword == SECTION_KEYWORDS_END ) break;

				grammar.Keywords.insert( keyword );
			}
		}
		else if ( section == SECTION_COMMENTS_BEGIN )
		{
			grammar.SINGLE_LINE_COMMENT_BEGIN = ReadLiteral( in );
			grammar.SINGLE_LINE_COMMENT_END = ReadLiteral( in );
		}
		else
			assert( false );
	}

	return ( in );
}

std::ostream & operator << ( std::ostream & out, const Grammar & grammar )
{
	out << SECTION_RULES_BEGIN << std::endl << std::endl;
	for ( size_t ruleIndex = 0; ruleIndex < grammar.Rules.size(); ++ruleIndex )
	{
		grammar.WriteRule( ruleIndex, out );
		out << std::endl;
	}
	out << std::endl << SECTION_RULES_END << std::endl << std::endl;

	out << SECTION_KEYWORDS_BEGIN << std::endl << std::endl;
	for ( std::set< std::string >::const_iterator keyword = grammar.Keywords.begin(); keyword != grammar.Keywords.end(); ++keyword )
		out << *keyword << std::endl;
	out << std::endl << SECTION_KEYWORDS_END << std::endl << std::endl;

	out << SECTION_COMMENTS_BEGIN << std::endl << std::endl;
	out << grammar.SINGLE_LINE_COMMENT_BEGIN << ' ' << grammar.SINGLE_LINE_COMMENT_END << std::endl;
	out << std::endl << SECTION_COMMENTS_END << std::endl << std::endl;

	return ( out );
}

static
std::ostream & operator << ( std::ostream & out, const std::list< SymbolPtr > & symbolsList )
{
	for ( std::list< SymbolPtr >::const_iterator symbol = symbolsList.begin(); symbol != symbolsList.end(); )
	{
		( *symbol )->WriteSimple( out );

		if ( ++symbol != symbolsList.end() )
			out << ' ';
	}

	return ( out );
}

std::ostream & operator << ( std::ostream & out, const Configuration & configuration )
{
	return ( out << "( " << ( char )configuration.State << ", "
		<< configuration.Position << ", "
		<< configuration.Alpha << ", "
		<< configuration.Beta << " )" );
}