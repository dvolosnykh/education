#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <string>
#include <ctype.h>


const std::string RULE_PARTS_SPLITTER = "::=";
const std::string SECTION_RULES_BEGIN = "[rules.begin]";
const std::string SECTION_RULES_END = "[rules.end]";
const std::string SECTION_KEYWORDS_BEGIN = "[keywords.begin]";
const std::string SECTION_KEYWORDS_END = "[keywords.end]";
const std::string SECTION_COMMENTS_BEGIN = "[comments.begin]";
const std::string SECTION_COMMENTS_END = "[comments.end]";
const char RIGHT_PARTS_SPLITTER = '|';
const char CONCATENATE = '+';
const char STRING_LITERAL = '"';
const char CHARACTER_LITERAL = '\'';
const char ESCAPE = '\\';
const char END_RULE = ';';
const char RULE_PREFIX = 'R';


void SkipWhitespaces( std::istream & in );
void SkipWhitespaces( const std::string & str, size_t * const pos );
const std::string ReadLiteral( std::istream & in );
void WriteLiteral( const std::string & literal, std::ostream & out );

const std::string ReadStringLiteral( std::istream & in );
const char ReadCharacterLiteral( std::istream & in );

const std::string ReadNonterminal( std::istream & in );

const char ReadSingleChar( std::istream & in, bool * const composite );
inline
const char ReadSingleChar( std::istream & in ) { return ReadSingleChar( in, NULL ); }
void WriteSingleChar( const char c, std::ostream & out, const char boundarySymbol );

inline
const std::string Trim( const std::string & str, const std::string & characters = " \r\n" )
{
	const size_t begin = str.find_first_not_of( characters );
	const size_t end = ( begin != std::string::npos ? str.find_last_not_of( characters ) : std::string::npos );
	return ( begin != std::string::npos ? str.substr( begin, end - begin + 1 ) : std::string() );
}

#endif