#include <assert.h>
#include <ctype.h>
#include <algorithm>

#include "Common.h"
#include "Symbol.h"
#include "Grammar.h"

void Terminal::WriteSimple( std::ostream & out ) const
{
	out << Literal;
}

const bool Terminal::Try( Configuration * configuration, const Grammar & grammar ) const
{
	size_t & pos = configuration->Position;
	const std::string & text = configuration->Input;

	bool goOn;
	const bool match = ( pos < text.length() && Literal == text[ pos ] );
	if ( match )
	{
		++pos;
		configuration->Alpha.push_back( configuration->Beta.front() );
		configuration->Beta.pop_front();
		goOn = true;
	}
	else
	{
		bool noAlternatives;
		ChosenAlternative * choice;
		do
		{
			while ( dynamic_cast< ChosenAlternative * >( configuration->Alpha.back().get() ) == NULL )
			{
				configuration->Beta.push_front( configuration->Alpha.back() );
				configuration->Alpha.pop_back();
			}

			choice = ( ChosenAlternative * )configuration->Alpha.back().get();

			const Rule & rule = *grammar.Rules[ choice->GetRuleIndex() ];
			const Alternative & alternative = *rule.Alternatives[ choice->AlternativeIndex ];
			for ( size_t i = 0; i < alternative.size(); ++i )
				configuration->Beta.pop_front();

			noAlternatives = ( ++choice->AlternativeIndex == rule.Alternatives.size() );
			if ( noAlternatives )
			{
				configuration->Beta.push_front( choice->Nonterminal );
				configuration->Alpha.pop_back();
			}
		}
		while ( !configuration->Alpha.empty() && noAlternatives );

		if ( !configuration->Alpha.empty() )
		{
			pos = choice->SavedPosition;
			const Rule & rule = *grammar.Rules[ choice->GetRuleIndex() ];
			const Alternative & alternative = *rule.Alternatives[ choice->AlternativeIndex ];
			configuration->Beta.insert( configuration->Beta.begin(), alternative.begin(), alternative.end() );
		}

		goOn = !configuration->Beta.empty();
	}

	return ( goOn );
}

void Nonterminal::WriteSimple( std::ostream & out ) const
{
	out << RULE_PREFIX << RuleIndex + 1;
}

const bool Nonterminal::Try( Configuration * const configuration, const Grammar & grammar ) const
{
	configuration->Alpha.push_back( SymbolPtr( new ChosenAlternative( configuration->Beta.front(), 0, configuration->Position ) ) );

	configuration->Beta.pop_front();
	const ChosenAlternative * const choice = ( ChosenAlternative * )configuration->Alpha.back().get();
	const Rule & rule = *grammar.Rules[ choice->GetRuleIndex() ];
	const Alternative & alternative = *rule.Alternatives[ choice->AlternativeIndex ];
	configuration->Beta.insert( configuration->Beta.begin(), alternative.begin(), alternative.end() );

	return ( true );
}

void Whitespace::WriteSimple( std::ostream & out ) const
{
	out << ' ';
}

static
const bool __LastLexemWasKeyWord( const Configuration & configuration, const Grammar& grammar )
{
	const size_t & pos = configuration.Position;
	const std::string & text = configuration.Input;

	size_t start = pos;
	while ( start > 0 && !isspace( text[ start - 1 ] ) )
		--start;

	std::string lexem = text.substr( start, pos - start );
	std::transform( lexem.begin(), lexem.end(), lexem.begin(), tolower );

	std::list< SymbolPtr >::const_reverse_iterator symbol = configuration.Alpha.rbegin();
	bool equal = true;
	for ( std::string::const_reverse_iterator c = lexem.rbegin(); c != lexem.rend() && equal; ++c, ++symbol )
	{
		const Terminal * const terminal = dynamic_cast< Terminal * >( symbol->get() );
		equal = ( terminal != NULL && terminal->Literal == *c );
	}

	return ( equal );
}

const bool Whitespace::Try( Configuration * const configuration, const Grammar & grammar ) const
{
	size_t & pos = configuration->Position;
	const std::string & text = configuration->Input;

	

	SkipWhitespaces( configuration->Input, &configuration->Position );

	configuration->Alpha.push_back( configuration->Beta.front() );
	configuration->Beta.pop_front();

	return ( true );
}

void ChosenAlternative::WriteSimple( std::ostream & out ) const
{
	Nonterminal->WriteSimple( out );
	out << '.' << AlternativeIndex + 1;
}