#ifndef RULE_H
#define RULE_H

#include <list>
#include <vector>
#include <string>
#include <memory>

#include "Symbol.h"

class Grammar;

class Alternative : public std::list< SymbolPtr >
{
public:
	const bool IsLeftRecursive( const size_t ruleIndex ) const { return __IsRecursive( ruleIndex, true ); }
	const bool IsRightRecursive( const size_t ruleIndex ) const { return __IsRecursive( ruleIndex, false ); }

private:
	const bool __IsRecursive( const size_t ruleIndex, const bool left ) const;
};

typedef std::tr1::shared_ptr< Alternative > AlternativePtr;

class Rule
{
public:
	std::string Header;
	std::vector< AlternativePtr > Alternatives;
	size_t Index;

public:
	Rule( const std::string & header, const size_t index )
		: Header( header )
		, Index( index ) {}

public:
	const size_t FindNonRecursiveAlternative( const Grammar & grammar ) const;
};

typedef std::tr1::shared_ptr< Rule > RulePtr;

#endif