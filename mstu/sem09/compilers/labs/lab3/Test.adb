------------------
-- SumOfSquares --
------------------

subtype Number is Long_Float;
type FloatVector is array( Positive range <> ) of Number;

function SumOfSquares( v : in FloatVector; first, last : in Positive ) return Number
is
	sum : Number := 0.0;
begin
	for i in first .. last loop
		sum := sum + v( i ) ** 2;
	end loop;

	return sum;
end SumOfSquares;

-------------------------------------------
-- GetLongestIncreasingSubsequenceLength --
-------------------------------------------

--type IntVector is array( Positive range <> ) of Positive;

--function MaxElement( v : in IntVector; size : in Positive ) return Positive;
--function Max( a, b : in Positive ) return Positive;

--function GetLongestIncreasingSubsequenceLength( sequence : in FloatVector; size : in Positive ) return Positive
--is
--	length : array( 0 .. size - 1 ) of Positive;
--begin
--	for i in 0 .. size - 1 loop
--		length( i ) := 1;
--	end loop;

--	for last in 0 .. size - 1 loop
--		for previous in 0 .. last - 1 loop
--			if sequence( previous ) < sequence( last ) then
--				length( last ) = Max( length( last ), length( previous ) + 1 );
--			end if;
--		end loop;
--	end loop;
	
--	return MaxElement( length, size );
--end GetLongestIncreasingSubsequenceLength;

--function MaxElement( v : in IntVector; size : in Positive ) return Positive
--is
--	maximum : Positive := v( 0 );
--begin
--	for i in 1 .. size - 1 loop
--		maximum := Max( v( i ), maximum );
--	end loop;

--	return maximum;
--end MaxElement;

--function Max( a, b : in Positive ) return Positive
--is
--begin
--	if a > b then
--		return a;
--	else
--		return b;
--	end if;
--end Max;