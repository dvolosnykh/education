#include <stdlib.h>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <tuple>
#include <assert.h>

using std::vector;
using std::cin;
using std::cout;
using std::endl;

#include "Algorithms.h"

//#define MANUAL

typedef char symbol_t;

class Rule
{
public:
	static const std::string Splitter;
	vector< symbol_t > Right;
	symbol_t Left;
};

const std::string Rule::Splitter = "->";

class Grammar
{
public:
	vector< symbol_t > Nonterminals;
	vector< symbol_t > Terminals;
	vector< Rule > Rules;
	symbol_t StartSymbol;

public:
	void DeleteChainRules()
	{
		vector< Rule > chainRules;
		__ExtractChainRules( &chainRules );

		vector< Rule > newRules;
		for ( vector< symbol_t >::const_iterator pn = Nonterminals.begin(); pn != Nonterminals.end(); ++pn )
		{
			const size_t nonterminalIndex = __GetNonterminalIndex( *pn );

			vector< size_t > closureIndices;
			__BuildClosure( &closureIndices, nonterminalIndex, chainRules );

			__AddNewRules( &newRules, closureIndices, nonterminalIndex );
		}

		Rules.insert( Rules.end(), newRules.begin(), newRules.end() );
	}

private:
	const size_t __GetNonterminalIndex( const symbol_t nonterminal ) const
	{
		vector< symbol_t >::const_iterator pos = BinarySearch( Nonterminals.begin(), Nonterminals.end(), nonterminal, std::less< symbol_t >() );
		return ( pos - Nonterminals.begin() );
	}

	const bool __IsChainRule( const size_t index ) const
	{
		return ( Rules[ index ].Right.size() == 1 && __IsNonterminal( Rules[ index ].Right.front() ) );
	}

	const bool __IsNonterminal( const symbol_t symbol ) const
	{
		return std::binary_search( Nonterminals.begin(), Nonterminals.end(), symbol );
	};

	void __ExtractChainRules( vector< Rule > * const chainRules )
	{
		for ( size_t i = Rules.size(); i-- > 0;  )
			if ( __IsChainRule( i ) )
			{
				chainRules->push_back( Rules[ i ] );
				Rules.erase( Rules.begin() + i );
			}
	}

	void __BuildClosure( vector< size_t > * const closureIndices, const size_t startNonterminalIndex, const vector< Rule > & chainRules ) const
	{
		closureIndices->push_back( startNonterminalIndex );

		for ( size_t oldSize = 0; closureIndices->size() > oldSize; )
		{
			vector< size_t > closureIncIndices;
			__BuildClosureInc( &closureIncIndices, oldSize, *closureIndices, chainRules );

			oldSize = closureIndices->size();
			__UnionClosureAndInc( closureIndices, closureIncIndices );
		}
	}

	void __BuildClosureInc( vector< size_t > * const inc, const size_t oldSize, const vector< size_t > & closure, const vector< Rule > & chainRules ) const
	{
		for ( size_t ni = oldSize; ni < closure.size(); ++ni )
			for ( vector< Rule >::const_iterator r = chainRules.begin(); r != chainRules.end(); ++r )
				if ( __GetNonterminalIndex( r->Left ) == closure[ ni ] )
					inc->push_back( __GetNonterminalIndex( r->Right.front() ) );

		std::sort( inc->begin(), inc->end() );
	}

	void __UnionClosureAndInc( vector< size_t > * const closure, const vector< size_t > & inc ) const
	{
		vector< size_t > result( closure->size() + inc.size() );
		const vector< size_t >::const_iterator firstUnneeded = std::set_union( closure->begin(), closure->end(), inc.begin(), inc.end(), result.begin() );
		result.erase( firstUnneeded, result.end() );
		closure->swap( result );
	}

	void __AddNewRules( vector< Rule > * const newRules, const vector< size_t > & closure, const size_t nonterminalIndex  ) const
	{
		for ( size_t ri = 0; ri < Rules.size(); ++ri )
		{
			const size_t curRuleLeftIndex = __GetNonterminalIndex( Rules[ ri ].Left );
			if ( curRuleLeftIndex != nonterminalIndex )
			{
				const vector< size_t >::const_iterator index = BinarySearch( closure.begin(), closure.end(), curRuleLeftIndex, std::less< size_t >() );
				const bool reachable = ( index != closure.end() && *index == curRuleLeftIndex );
				if ( reachable )
				{
					Rule newRule;
					newRule.Left = Nonterminals[ nonterminalIndex ];
					newRule.Right = Rules[ ri ].Right;
					newRules->push_back( newRule );
				}
			}
		}
	}
};

void SkipWhitespaces( const bool stopAtEOL = false )
{
	int c;
	do
		c = cin.get();
	while ( !cin.eof() && ( c == ' ' || !stopAtEOL && c == '\n' ) );

	if ( !cin.eof() )
		cin.unget();
}

const symbol_t ReadSymbol( const bool stopAtEOL = false )
{
	SkipWhitespaces( stopAtEOL );
	return getchar();
}

void WriteSymbol( const symbol_t symbol )
{
	putchar( symbol );
	putchar( ' ' );
}

void ReadSetOfSymbols( vector< symbol_t > * s )
{
	size_t count;
	cin >> count;
	s->reserve( count );
	for ( size_t i = 0; i < count; ++i )
		s->push_back( ReadSymbol() );
}

void WriteSetOfSymbols( const vector< symbol_t > & s )
{
	for ( size_t i = 0; i < s.size(); ++i )
		WriteSymbol( s[ i ] );
	putchar( '\n' );
}

// skip '->'
void ReadRuleSplitter()
{
	std::string splitter;
	cin >> splitter;
	assert( splitter == Rule::Splitter );
}

void WriteRuleSplitter()
{
	cout << Rule::Splitter << ' ';
}

void ReadRule( Rule * const r )
{
	r->Left = ReadSymbol();
	ReadRuleSplitter();

	for ( ; ; )
	{
		const symbol_t symbol = ReadSymbol( true );

	if ( symbol == '\n' ) break;

		r->Right.push_back( symbol );
	}
}

void WriteRule( const Rule & r )
{
	WriteSymbol( r.Left );
	WriteRuleSplitter();
	WriteSetOfSymbols( r.Right );
}

void ReadRules( vector< Rule > * const r )
{
	size_t count;
	cin >> count;
	r->resize( count );
	for ( size_t i = 0; i < count; ++i )
		ReadRule( &( *r )[ i ] );
}

void WriteRules( const vector< Rule > & r )
{
	for ( size_t i = 0; i < r.size(); ++i )
		WriteRule( r[ i ] );
}

void ReadGrammar( Grammar * const g )
{
	ReadSetOfSymbols( &g->Nonterminals );
	std::sort( g->Nonterminals.begin(), g->Nonterminals.end() );

	ReadSetOfSymbols( &g->Terminals );
	std::sort( g->Terminals.begin(), g->Terminals.end() );

	ReadRules( &g->Rules );

	g->StartSymbol = ReadSymbol();
}

void WriteGrammar( const Grammar & g )
{
	cout << "Nonterminals:" << endl;
	WriteSetOfSymbols( g.Nonterminals );
	cout << "Terminals:" << endl;
	WriteSetOfSymbols( g.Terminals );
	cout << "Rules:" << endl;
	WriteRules( g.Rules );
	cout << "Start symbol:" << endl;
	WriteSymbol( g.StartSymbol );
	cout << endl;
}

int main()
{
#ifndef MANUAL
	freopen( "input.txt", "rt", stdin );
	//freopen( "output.txt", "wt", stdout );
#endif

	for ( ; ; )
	{
		SkipWhitespaces();

	if ( cin.eof() ) break;

		Grammar g;
		ReadGrammar( &g );
		g.DeleteChainRules();
		WriteGrammar( g );
	}

	return ( EXIT_SUCCESS );
}
