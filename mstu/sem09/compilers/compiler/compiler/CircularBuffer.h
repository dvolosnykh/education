#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H

#include <array>

template< typename T, std::size_t length, const Traits & Read >
class CircularBuffer
{
private:
	std::tr1::array< T, length > __buffer;
	std::size_t __read, __write;

public:
	CircularBuffer();

public:
	static std::size_t NextIndex( const std::size_t cur, const std::size_t count ) { return ( cur + 1 < count ? cur + 1 : 0 ); }
	static std::size_t PrevIndex( const std::size_t cur, const std::size_t count ) { return ( cur > 0 ? cur - 1 : count - 1 ); }
};

template< typename T, std::size_t length, const Traits & Read >
CircularBuffer::CircularBuffer()
	: __read( 0 )
	, __write( 0 )
{
	for ( ; Read( __buffer[ __write ] ); )
	{
		__write = NextIndex( __write, 
	}
}

#endif