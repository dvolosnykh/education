#include <stdlib.h>

#include "Lexer.h"


int main()
{
	for ( Lexer lexer; ; )
	{
		const Token * const t = lexer.Scan();

	if ( t == NULL ) break;

		t->ToString();
	}

	return ( EXIT_SUCCESS );
}