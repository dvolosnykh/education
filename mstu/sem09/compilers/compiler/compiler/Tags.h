#ifndef TAGS_H
#define TAGS_H

#include <limits>

enum TAG
{
	SPACE = ' ',
	TAB = '\t',
	NEWLINE = '\n',
	ZERO = '0', ONE = '1', TWO = '2', THREE = '3', FOUR = '4', FIVE = '5', SIX = '6', SEVEN = '7', EIGHT = '8', NINE = '9',
	DIVIDE = '/', MULTIPLY = '*',
	COMMENT_PREFIX = '/', COMMENT_SINGLELINE = '/', COMMENT_MULTILINE = '*',
	NUMBER = UCHAR_MAX + 1,
	ID,
	TRUE,
	FALSE
};


#endif