#ifndef LEXER_H
#define LEXER_H

#include <stdlib.h>
#include <map>

#include "Tokens.h"
#include "CircularBuffer.h"


class Lexer
{
private:
	typedef std::map< const std::string, const Word * > Dictionary;

public:
	std::size_t Line;

private:
	LookageadBuffer
	Dictionary __words;
	CircularBuffer< int, 2 > __lookahead;

public:
	Lexer();
	~Lexer();

public:
	const Token * const Scan();

private:
	void __SkipWhitespaces();
	const Number * const __ReadNumber();
	const Word * const __ReadWord();
	void __ReadSinglelineComment();
	void __ReadMultilineComment();

	void __ReserveWord( const Word * const w ) { __words[ w->Lexeme ] = w; }
	const Word * const __FindWordByKey( const std::string & key ) const
	{
		Dictionary::const_iterator i =__words.find( key );
		return ( i != __words.end() ? i->second : NULL );
	}
};

#endif