#include "Lexer.h"

#include <ctype.h>



LookaheadBuffer::LookaheadBuffer()
	: __readIndex( 0 )
	, __writeIndex( 0 )
{
	for ( ; ; )
	{
		__buffer[ __write ] = getchar();
	}
}

Lexer::Lexer()
	: Line( 1 )
	, __peekChar( getchar() )
{
	__ReserveWord( new Word( TRUE, "true" ) );
	__ReserveWord( new Word( FALSE, "false" ) );
}

Lexer::~Lexer()
{
	for ( Dictionary::iterator i = __words.begin(); i != __words.end(); i++ )
		delete i->second;
}

const Token * const Lexer::Scan()
{
	__SkipWhitespaces();

	const Token * t = NULL;

	if ( __peekChar != EOF )
	{
		if ( isdigit( __peekChar ) )
			t = __ReadNumber();
		else if ( isalpha( __peekChar ) )
			t = __ReadWord();
		else if ( __peekChar == COMMENT_PREFIX )
		{
			switch ( __peekChar = getchar() )
			{
			case COMMENT_SINGLELINE:	__ReadSinglelineComment();	break;
			case COMMENT_MULTILINE:		__ReadMultilineComment();	break;
			}

			t = new Token( DIVIDE );
		}
		else
		{
			t = new Token( TAG( __peekChar ) );
			__peekChar = getchar();
		}
	}

	return ( t );
}

void Lexer::__SkipWhitespaces()
{
	for ( ; ; __peekChar = getchar() )
	{
		if ( __peekChar == NEWLINE )
			Line++;
		else if ( __peekChar != SPACE && __peekChar != TAB )
			break;
	}
}

const Number * const Lexer::__ReadNumber()
{
	unsigned value = __peekChar - ZERO;
	for ( ; isdigit( __peekChar = getchar() ); )
		value = 10 * value + ( __peekChar - ZERO );

	return new Number( value );
}

const Word * const Lexer::__ReadWord()
{
	std::string s( 1, __peekChar );
	for ( ; isalnum( __peekChar = getchar() ); )
		s.append( 1, __peekChar );

	const Word * w = __FindWordByKey( s );
	if ( w == NULL )
	{
		w = new Word( ID, s );
		__ReserveWord( w );
	}

	return ( w );
}

void Lexer::__ReadSinglelineComment()
{
	for ( ; getchar() != '\n'; );
	__peekChar = getchar();
}

void Lexer::__ReadMultilineComment()
{
	for ( __peekChar = getchar(); ; )
	{
		for ( ; getchar() != COMMENT_MULTILINE; );

		if ( getchar() == )
		{
			__peekChar = getchar();
			if ( __peekChar == COMMENT_PREFIX )
				break;
		}
	}

	__peekChar = getchar();
}