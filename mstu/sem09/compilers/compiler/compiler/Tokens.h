#ifndef TOKENS_H
#define TOKENS_H


#include <stdio.h>
#include <string>

#include "Tags.h"


class Token
{
public:
	const TAG Tag;

public:
	Token( const TAG tag )
		: Tag( tag ) {}

public:
	virtual void ToString() const
	{
		printf( "%c\n", Tag );
	}
};

class Number : public Token
{
public:
	const unsigned Value;

public:
	Number( const unsigned value )
		: Token( NUMBER )
		, Value( value ) {}

public:
	virtual void ToString() const
	{
		printf( "%u\n", Value );
	}
};

class Word : public Token
{
public:
	const std::string Lexeme;

public:
	Word( const TAG tag, const std::string & lexeme )
		: Token( tag )
		, Lexeme( lexeme ) {}

public:
	virtual void ToString() const
	{
		printf( "%s\n", Lexeme.c_str() );
	}
};

#endif