/*
	Dmitry F. Volosnykh, IU7-92M
	Variant 5
*/

#define MANUAL_INPUT

#include <stdlib.h>
#include <iostream>
#include <vector>
#include <stack>
#include <set>
#include <string>
#include <map>
#include <bitset>
#include <sstream>
#include <assert.h>

typedef std::vector< std::string > source_t;

struct result_t
{
	bool Resolved;	// shows whether program was successfully resolved, i.e. either successful termination or discovered infinite loop.
	size_t TicksCount;	// processor ticks used by program.
};

class Interpreter	// executes programs.
{
private:
	enum { REGISTERS_COUNT = 32 };
	enum COMMAND { AND, OR, XOR, NOT, MOV, SET, RANDOM, JMP, JZ, STOP };	// command set.

	typedef std::vector< size_t > args_t;
	typedef std::pair< COMMAND, args_t > instruction_t;
	typedef std::map< std::string, COMMAND > commandsDictionary_t;
	typedef std::set< unsigned long > instructionHistory_t;
	typedef std::vector< instructionHistory_t > history_t;

public:
	enum STATE { EXECUTES, TERMINATED, CYCLED };	// possible states.

	struct info_t
	{
		STATE State;	// the state of the interpreter.
		size_t TicksCount;	// processor ticks used by interpreter while executing program.
		std::vector< size_t > Indices;	// indices of the undefined registers that are about to be used.

		info_t( const STATE state, const size_t ticksCount )
			: State( state )
			, TicksCount( ticksCount )
		{}
	};

	typedef std::vector< instruction_t > code_t;

	struct unit_t	// represents compiled program with some helping information.
	{
		code_t Instructions;	// compiled instructions of the program.
		std::set< size_t > UsedRegisters;	// the set of registers that are effectively used.
	};

public:
	info_t Info;

private:
	static bool __staticsInitialized;	// ensures single initialization of the command set.
	static commandsDictionary_t __commands;	// command set.

	const unit_t & __unit;
	std::bitset< REGISTERS_COUNT > __register;	// registers of the interpreter.
	std::bitset< REGISTERS_COUNT > __initialized;	// "register has been initialized" flags.
	size_t __index;	// index of the processed instruction.
	history_t __history;	// memoization of the interpreter's context in order to figure out infinite loops.
	
public:
	Interpreter( const unit_t & unit );

public:
	void TryExecute();
	static const unit_t Compile( const source_t & source );

	const bool GetRegister( const size_t index ) const { return ( __register[ index ] ); }
	void SetRegister( const size_t index, const bool value ) { __register[ index ] = value, __initialized[ index ] = true; }

private:
	const std::vector< size_t > __GetAmbiguousRegistersIndices() const;
	const bool __CheckIfCycled( const size_t index );
	const bool __IsUsed( const size_t index ) const { return ( __unit.UsedRegisters.find( index ) != __unit.UsedRegisters.end() ); }
	static const COMMAND __GetCommandByName( const std::string name ) { return ( __commands.find( name )->second ); }
	static void __FollowDependencies( std::set< size_t > * const used, const std::vector< std::set< size_t > > & dependency );
	static void __Initialize();
};

bool Interpreter::__staticsInitialized = false;
Interpreter::commandsDictionary_t Interpreter::__commands;

Interpreter::Interpreter( const unit_t & unit )
	: Info( EXECUTES, 0 )
	, __unit( unit )
	, __index( 0 )
	, __history( unit.Instructions.size() )
{
	__Initialize();
}

// try to execute whole program.
void Interpreter::TryExecute()
{
	bool ambiguity = false;	// shows whether ambiguity was encounter due to use of undefined registers.
	while ( Info.State == EXECUTES && !ambiguity )	// loop while instructions are normaly executed without any ambiguities.
	{
		const instruction_t & instruction = __unit.Instructions[ __index ];
		const COMMAND & command = instruction.first;	// command code.
		const args_t & args = instruction.second;	// arguments of the command.

		Info.Indices.clear();	// there are no referenced undefined registers yet.

		const size_t curIndex = __index;	// need to save index of the current instruction due to possible change during command processing.

		bool processed = false;	// shows whether current instruction has been processed.
		switch ( command )
		{
		case AND:
		case OR:
		case XOR:
			{
				const size_t & first = args[ 0 ], & second = args[ 1 ];

				if ( __IsUsed( first ) )
				{
					ambiguity = !( __initialized[ first ] && __initialized[ second ] );
					if ( !ambiguity )
					{
						switch ( command )
						{
						case AND:	__register[ first ] = ( __register[ first ] && __register[ second ] );	break;
						case OR:	__register[ first ] = ( __register[ first ] || __register[ second ] );	break;
						case XOR:	__register[ first ] = ( __register[ first ] != __register[ second ] );	break;
						}

						processed = true;
					}
					else
						Info.Indices = __GetAmbiguousRegistersIndices();
				}
				else
					processed = true;

				if ( processed )
					++__index;
			}
			break;

		case NOT:
			{
				const size_t & first = args[ 0 ];
				
				if ( __IsUsed( first ) )
				{
					ambiguity = !__initialized[ first ];
					if ( !ambiguity )
					{
						__register[ first ] = !__register[ first ];
						processed = true;
					}
					else
						Info.Indices = __GetAmbiguousRegistersIndices();
				}
				else
					processed = true;

				if ( processed )
					++__index;
			}
			break;

		case MOV:
			{
				const size_t & first = args[ 0 ], & second = args[ 1 ];

				if ( __IsUsed( first ) )
				{
					ambiguity = !__initialized[ second ];
					if ( !ambiguity )
					{
						__initialized[ first ] = true;
						__register[ first ] = __register[ second ];
						processed = true;
					}
					else
						Info.Indices = __GetAmbiguousRegistersIndices();
				}
				else
					processed = true;

				if ( processed )
					++__index;

			}
			break;

		case SET:
			{
				const size_t & first = args[ 0 ];

				__initialized[ first ] = true;
				__register[ first ] = ( args[ 1 ] != 0 );
				processed = true;
				++__index;
			}
			break;

		case RANDOM:
			{
				const size_t & first = args[ 0 ];

				if ( __IsUsed( first ) )
				{
					ambiguity = true;
					Info.Indices = __GetAmbiguousRegistersIndices();
				}

				processed = true;
				++__index;
			}
			break;

		case JMP:
			{
				const size_t & first = args[ 0 ];

				processed = true;
				__index = first;
			}
			break;

		case JZ:
			{
				const size_t & first = args[ 0 ], & second = args[ 1 ];

				ambiguity = !__initialized[ second ];
				if ( !ambiguity )
				{
					if ( !__register[ second ] )
						__index = first;
					else
						++__index;

					processed = true;
				}
				else
					Info.Indices = __GetAmbiguousRegistersIndices();
			}
			break;

		case STOP:
			{
				Info.State = TERMINATED;
				processed = true;
			}
			break;
		}

		if ( processed )
		{
			++Info.TicksCount;

			if ( __CheckIfCycled( curIndex ) )
				Info.State = CYCLED;
		}
	}
}

// compile source text of the porgram.
const Interpreter::unit_t Interpreter::Compile( const source_t & source )
{
	__Initialize();

	unit_t unit;
	unit.Instructions.resize( source.size() );

	std::vector< std::set< size_t > > dependency( REGISTERS_COUNT );	// represents registers definition dependency graph, i.e. what registers should be figured out before the one.

	for ( size_t lineIndex = 0; lineIndex < source.size(); ++lineIndex )
	{
		instruction_t & instruction = unit.Instructions[ lineIndex ];	// current instruction.
		COMMAND & command = instruction.first;	// command code.
		args_t & args = instruction.second;	// arguments of the command.

		// read instruction text, using stream.
		std::stringstream sin( source[ lineIndex ] );

		// read command name.
		std::string commandName;
		sin >> commandName;

		// get it's code.
		command = __GetCommandByName( commandName );

		// read arguments, resolving dependencies of the referenced registers.
		switch ( command )
		{
		case AND:
		case OR:
		case XOR:
		case MOV:
			args.resize( 2 );
			sin >> args[ 0 ] >> args[ 1 ];
			
			// first argument depends on the value of the second one.
			dependency[ args[ 0 ] ].insert( args[ 1 ] );
			break;

		case SET:
			args.resize( 2 );
			sin >> args[ 0 ] >> args[ 1 ];
			break;

		case NOT:
		case RANDOM:
		case JMP:
			args.resize( 1 );
			sin >> args[ 0 ];
			break;

		case JZ:
			args.resize( 2 );
			sin >> args[ 0 ] >> args[ 1 ];

			// registers referenced in JZ-command are used directly.
			unit.UsedRegisters.insert( args[ 1 ] );
			break;

		case STOP:
			break;
		}

		assert( !sin.fail() );
	}

	__FollowDependencies( &unit.UsedRegisters, dependency );

	return ( unit );
}

// returns vector of undefined registers referenced as arguments of the current instruction.
const std::vector< size_t > Interpreter::__GetAmbiguousRegistersIndices() const
{
	std::vector< size_t > indices;

	const instruction_t & instruction = __unit.Instructions[ __index ];
	const COMMAND & command = instruction.first;	// command code.
	const args_t & args = instruction.second;	// arguments of the command.

	switch ( command )
	{
	case AND:
	case OR:
	case XOR:
		{
			if ( !__initialized[ args[ 0 ] ] )
				indices.push_back( args[ 0 ] );

			if ( !__initialized[ args[ 1 ] ] )
				indices.push_back( args[ 1 ] );
		}
		break;

	case NOT:
		{
			if ( !__initialized[ args[ 0 ] ] )
				indices.push_back( args[ 0 ] );
		}
		break;

	case MOV:
	case JZ:
		{
			if ( !__initialized[ args[ 1 ] ] )
				indices.push_back( args[ 1 ] );
		}
		break;

	case RANDOM:
		{
			indices.push_back( args[ 0 ] );
		}
		break;
	}

	return ( indices );
}

// look past back into the instruction's history: check if this instruction was executed with the same interpreter's context.
const bool Interpreter::__CheckIfCycled( const size_t index )
{
	instructionHistory_t & instructionHistory = __history[ index ];
	const std::pair< instructionHistory_t::iterator, bool > instructionResult = instructionHistory.insert( __register.to_ulong() );
	return ( !instructionResult.second );
}

// find the dependency-closure of used registers.
void Interpreter::__FollowDependencies( std::set< size_t > * const used, const std::vector< std::set< size_t > > & dependency )
{
	std::stack< size_t > reached;	// set of newly reached vertices of the dependency graph.

	// all directly used registers are newly reached vertices at this point.
	for ( std::set< size_t >::const_iterator u = used->begin(); u != used->end(); ++u )
		reached.push( *u );

	while ( !reached.empty() )	// loop while there are still newly reached vertices.
	{
		// get another newly reached vertex.
		const size_t u = reached.top();	reached.pop();

		const std::set< size_t > & dependsOn = dependency[ u ];	// it's dependency list.

		// add all newly reached vertices.
		for ( std::set< size_t >::const_iterator d = dependsOn.begin(); d != dependsOn.end(); ++d )
		{
			const std::pair< std::set< size_t >::iterator, bool > result = used->insert( *d );
			if ( result.second )
				reached.push( *d );
		}
	}
}

// increment vector of bools as if it was the set of bits of some integral type.
std::vector< bool > & operator ++ ( std::vector< bool > & v )
{
	bool carry = true;
	for ( size_t i = 0; carry && i < v.size(); ++i )
	{
		carry = ( carry && v[ i ] );
		v[ i ] = !v[ i ];
	}

	return ( v );
}

// test if any bit is set.
const bool Any( const std::vector< bool > & v )
{
	size_t i = 0;
	while ( i < v.size() && !v[ i ] )
		++i;

	return ( i < v.size() );
}

// preform the intialization.
void Interpreter::__Initialize()
{
	if ( !__staticsInitialized )
	{
		__commands[ "AND" ] = AND;
		__commands[ "OR" ] = OR;
		__commands[ "XOR" ] = XOR;
		__commands[ "NOT" ] = NOT;
		__commands[ "MOV" ] = MOV;
		__commands[ "SET" ] = SET;
		__commands[ "RANDOM" ] = RANDOM;
		__commands[ "JMP" ] = JMP;
		__commands[ "JZ" ] = JZ;
		__commands[ "STOP" ] = STOP;

		__staticsInitialized = true;
	}
}


class Analyzer	// analyzes programs.
{
private:
	std::stack< Interpreter > __interpreters;	// set of pending interpreters.

public:
	const result_t Analyze( const source_t & source );
};

// analyze passed source text.
const result_t Analyzer::Analyze( const source_t & source )
{
	const Interpreter::unit_t unit = Interpreter::Compile( source );

	result_t result = { false, 0 };

	__interpreters.push( Interpreter( unit ) );
	while ( !__interpreters.empty() )
	{
		// try executing another interpreter.
		Interpreter interpreter = __interpreters.top();
		__interpreters.pop();

		interpreter.TryExecute();
		Interpreter::info_t & info = interpreter.Info;

		const bool nondeterminism = ( info.State == Interpreter::EXECUTES );	// if interpreter appears to be executed then ambiguity was encountered due to use of undefined registers.
		if ( !nondeterminism )
		{
			if ( info.State == Interpreter::TERMINATED )	// terminated in finite time.
			{
				bool save = !result.Resolved;	// save results if there were no successful terminations till now.
				if ( !save )
					save = ( info.TicksCount < result.TicksCount );	// save results if last executed interpreter terminated in a shorter time.
				
				if ( save )
				{
					result.Resolved = true;
					result.TicksCount = info.TicksCount;
				}
			}
		}
		else
		{
			// clone interpretator in ambiguate state, setting all possible compinations of undefined registers.
			std::vector< bool > values( info.Indices.size() );

			do
			{
				for ( size_t i = 0; i < info.Indices.size(); ++i )
					interpreter.SetRegister( info.Indices[ i ], values[ i ] );

				__interpreters.push( interpreter );
			}
			while( Any( ++values ) );
		}
	}
	
	return ( result );
}


void Input( source_t * const source )
{
	size_t count;
	std::cin >> count;

	source->resize( count );

	std::getline( std::cin, source->front() ); // dummy reading of end-of-line left after reading number of instructions.
	for ( size_t i = 0; i < count; ++i )
		std::getline( std::cin, ( *source )[ i ] );

	assert( !std::cin.fail() );
}

void Output( const result_t & result )
{
	if ( result.Resolved )
		std::cout << result.TicksCount << std::endl;
	else
		std::cout << "HANGS" << std::endl;
}

int main()
{
#ifndef MANUAL_INPUT
	freopen( "input.txt", "rt", stdin );
#endif

	size_t testsCount;
	std::cin >> testsCount;

	source_t source;
	Analyzer analyzer;
	for ( size_t testIndex = 0 ; testIndex < testsCount; ++testIndex )
	{
		Input( &source );
		const result_t result = analyzer.Analyze( source );
		Output( result );
	}

	return ( EXIT_SUCCESS );
}