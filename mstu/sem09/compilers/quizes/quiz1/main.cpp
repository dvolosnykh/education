//#define MANUAL_INPUT


#include <stdlib.h>
#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <ctype.h>

typedef std::vector< std::string > program_t;


class Array
{
private:
	std::map< int, int > __array;
	const size_t __length;

public:
	Array( const size_t length )
		: __length( length ) {}

public:
	const bool Set( const int index, const int value );
	const bool Get( const int index, int * value ) const;

private:
	const bool __InRange( const int index ) const { return ( 0 <= index && index < __length ); }
};

const bool Array::Set( const int index, const int value )
{
	const bool ok = __InRange( index );

	if ( ok )
		__array[ index ] = value;

	return ( ok );
}

const bool Array::Get( const int index, int * value ) const
{
	const std::map< int, int >::const_iterator element = __array.find( index );
	const bool initialized = ( element != __array.end() );

	if ( initialized )
		*value = element->second;

	return ( initialized );
}

class Compiler
{
private:
	enum { ACCESS_BEGIN = '[', ACCESS_END = ']', ASSIGN = '=' };
	typedef char id_t;
	struct access_info_t
	{
		id_t Name;
		int Expression;
		bool ExpressionIsConstant;
	};

private:
	std::map< id_t, Array > __symbolTable;

public:
	const size_t Compile( const program_t & program );

private:
	void __Reset() { __symbolTable.clear(); }

	const bool __Access( access_info_t * const info, const std::string & line, size_t * const symbolIndex );
	const bool __ArrayName( id_t * const name, const std::string & line, size_t * const symbolIndex );
	const bool __Expression( int * const value, bool * const isConstant, const std::string & line, size_t * const symbolIndex );
	const bool __Constant( int * const value, const std::string & line, size_t * const symbolIndex );
};

const size_t Compiler::Compile( const program_t & program )
{
	__Reset();

	size_t bugLine = 0;

	for ( size_t lineIndex = 0; lineIndex < program.size() && bugLine == 0; ++lineIndex )
	{
		const std::string & line = program[ lineIndex ];
		size_t symbolIndex = 0;

		access_info_t info;
		bool ok = __Access( &info, line, &symbolIndex );
		bool assignment = false, declaration = false;
		if ( ok )
		{
			if ( symbolIndex < line.length() )
				assignment = ( line[ symbolIndex++ ] == ASSIGN );
			else
				declaration = info.ExpressionIsConstant;

			ok = ( assignment || declaration );
		}

		if ( ok )
		{
			if ( assignment )
			{
				int value;
				ok = __Expression( &value, NULL, line, &symbolIndex );
				if ( ok && symbolIndex == line.length() )
				{
					const std::map< id_t, Array >::iterator array = __symbolTable.find( info.Name );
					ok = ( array != __symbolTable.end() && array->second.Set( info.Expression, value ) );
				}
			}
			else // if ( declaration )
			{
				ok = ( __symbolTable.find( info.Name ) == __symbolTable.end() );
				if ( ok )
					__symbolTable.insert( std::make_pair( info.Name, Array( info.Expression ) ) );
			}
		}

		if ( !ok )
			bugLine = lineIndex + 1;
	}

	return ( bugLine );
}

const bool Compiler::__Access( access_info_t * const info, const std::string & line, size_t * const symbolIndex )
{
	bool ok = __ArrayName( &info->Name, line, symbolIndex );

	if ( ok )
		ok = ( line[ ( *symbolIndex )++ ] == ACCESS_BEGIN );

	if ( ok )
		ok = __Expression( &info->Expression, &info->ExpressionIsConstant, line, symbolIndex );

	if ( ok )
		ok = ( line[ ( *symbolIndex )++ ] == ACCESS_END );

	return ( ok );
}

const bool Compiler::__ArrayName( id_t * const name, const std::string & line, size_t * const symbolIndex )
{
	const bool ok = ( isalpha( line[ *symbolIndex ] ) != 0 );
	if ( ok )
		*name = line[ ( *symbolIndex )++ ];

	return ( ok );
}

const bool Compiler::__Expression( int * const value, bool * const isConstant, const std::string & line, size_t * const symbolIndex )
{
	bool ok = __Constant( value, line, symbolIndex );
	if ( isConstant != NULL )
		*isConstant = ok;

	if ( !ok )
	{
		access_info_t info;
		ok = __Access( &info, line, symbolIndex );
		if ( ok )
		{
			const std::map< id_t, Array >::const_iterator array = __symbolTable.find( info.Name );
			ok = ( array != __symbolTable.end() && array->second.Get( info.Expression, value ) );
		}
	}

	return ( ok );
}

const bool Compiler::__Constant( int * const value, const std::string & line, size_t * const symbolIndex )
{
	const bool ok = ( isdigit( line[ *symbolIndex ] ) != 0 );

	if ( ok )
	{
		*value = 0;
		do
		{
			*value *= 10;
			*value += line[ ( *symbolIndex )++ ] - '0';
		}
		while ( *symbolIndex < line.length() && isdigit( line[ *symbolIndex ] ) );
	}

	return ( ok );
}

const bool Input( program_t * const program )
{
	static const std::string END = ".";

	program->clear();

	while ( true )
	{
		std::string line;
		std::getline( std::cin, line );

	if ( line == END ) break;

		program->push_back( line );
	}

	return ( program->size() > 0 );
}

void Output( const size_t bugLine )
{
	std::cout << bugLine << std::endl;
}

int main()
{
#ifndef MANUAL_INPUT
	freopen( "input.txt", "rt", stdin );
#endif

	Compiler compiler;
	program_t program;
	while ( Input( &program ) )
	{
		const size_t bugLine = compiler.Compile( program );
		Output( bugLine );
	}
	
	return ( EXIT_SUCCESS );
}