-module( dfa ).
-export( [ start_link / 3, start / 3, stop / 1, send_event / 2, send_message / 2 ] ).

-behaviour( gen_fsm ).
-export( [ init / 1, handle_event / 3, handle_sync_event / 4, handle_info / 3,
	terminate / 3, code_change / 4 ] ).

-export( [ behaviour_info / 1, states_auto / 1, initial_state_auto / 1,
	accept_states_auto / 1, alphabet_auto / 1, is_accept_state / 1 ] ).

%
% dfa interface.
%

start_link( Id, Logic, Data ) ->
	gen_fsm:start_link( { local, Id }, ?MODULE, { Logic, Data }, [] ).

start( Id, Logic, Data ) ->
	gen_fsm:start( { local, Id }, ?MODULE, { Logic, Data }, [] ).

stop( Id ) ->
	gen_fsm:send_all_state_event( Id, { external, stop } ).

send_event( Id, Event ) ->
	gen_fsm:send_all_state_event( Id, Event ).

send_message( Id, Message ) ->
	gen_fsm:send_all_state_event( Id, { message, Message } ).

%
% gen_fsm behaviour.
%

init( { Logic, Data } ) ->
	prepare( Logic ),
	InitialStateData = Logic:init( Data ),
	% enter initial state.
	InitialState = get( { ?MODULE, initial_state } ),
	Logic = get( { ?MODULE, logic } ),
	EnterStateData = Logic:enter( InitialState, noevent, InitialStateData ),
	{ ok, InitialState, EnterStateData }.

terminate( Reason, State, StateData ) ->
	% leave last entered state.
	Logic = get( { ?MODULE, logic } ),
	_LeaveStateData = Logic:leave( State, stop, StateData ),
	Logic:terminate( Reason, State, StateData ).

% DFA received some message.
handle_event( { message, Message }, State, StateData ) ->
	Logic = get( { ?MODULE, logic } ),
	Logic:handle_message( Message, State, StateData ),
	{ next_state, State, StateData };
% DFA is requested to stop.
handle_event( { external, stop }, _State, StateData ) ->
	{ stop, normal, StateData };
% DFA received event.
handle_event( Event, State, StateData ) when is_atom( Event ) ->
	{ NextState, NewStateData } = transfer( State, Event, StateData ),
	{ next_state, NextState, NewStateData }.

% Ignore undefined synchronous events.
handle_sync_event( _Event, _From, State, StateData ) ->
	{ reply, ok, State, StateData }.

% Handle exit-messages from linked processes.
handle_info( { 'EXIT', Pid, Reason }, State, StateData ) ->
	Logic = get( { ?MODULE, logic } ),
	Logic:handle_exit( Pid, Reason ),
	{ next_state, State, StateData };
% Ignore undefined info messages.
handle_info( _Info, State, StateData ) ->
	{ next_state, State, StateData }.

% Ignore undefined code changes.
code_change( _OldVersion, State, StateData, _Extra ) ->
	{ ok, State, StateData }.

% Helping stuff.

prepare( Logic ) ->
	process_flag( trap_exit, true ),
	put( { ?MODULE, logic }, Logic ),
	% dfa-defining stuff.
	put( { ?MODULE, states }, Logic:states() ),
	put( { ?MODULE, initial_state }, Logic:initial_state() ),
	put( { ?MODULE, accept_states }, Logic:accept_states() ),
	put( { ?MODULE, alphabet }, Logic:alphabet() ),
	put( { ?MODULE, transition_function }, dict:from_list( Logic:transition_function() ) ).

transfer( State, Event, StateData ) ->
	Result = dict:find( { State, Event }, get( { ?MODULE, transition_function } ) ),
	case Result of
		{ ok, NextState } ->
			Logic = get( { ?MODULE, logic } ),
			LeaveStateData = Logic:leave( State, Event, StateData ),
			EnterStateData = Logic:enter( NextState, Event, LeaveStateData ),
			{ NextState, EnterStateData };
		error ->
			{ State, StateData }
	end.

%
% Define dfa behaviour.
%

% States.
%% @spec	states() -> set( state() ) | auto.

% Initial state.
%% @spec	initial_state() -> state() | auto.

% Accept states.
%% @spec	accept_states() -> set( state() ) | auto.

% Alphabet.
%% @spec	alphabet() -> set( symbol() ) | auto.

% Transition-state function.
%% @spec	transition_function() -> list( tuple( tuple( state(), symbol() ), state() ) ).

% Symantic actions when leaving the current state on some event.
%% @spec	leave( state(), event(), data() ) -> data().

% Symantic actions when entering the next state on some event.
%% @spec	enter( state(), event(), data() ) -> data().

behaviour_info( callbacks ) ->
	[ { init, 1 }, { terminate, 3 }, { handle_message, 3 }, { handle_exit, 2 },
		{ enter, 3 }, { leave, 3 }, { states, 0 }, { initial_state, 0 },
		{ accept_states, 0 }, { alphabet, 0 }, { transition_function, 0 } ];
behaviour_info( _Info ) -> % ignore unexpected info-messages.
	undefined.

% Tools for building dfa elements automaticly.

% states_auto returns union of from-states and to-states.
states_auto( Logic ) ->
	Transitions = Logic:transition_function(),
	FromStates = get_from_states( Transitions ),
	ToStates = get_to_states( Transitions ),
	_States = sets:union( FromStates, ToStates ).

% initial_state_auto returns first from-state.
initial_state_auto( Logic ) ->
	[ First | _ ] = Logic:transition_function(),
	{ { InitialState, _ }, _ } = First,
	InitialState.

% accept_states_auto returns subtraction of to-states and from-states.
accept_states_auto( Logic ) ->
	Transitions = Logic:transition_function(),
	FromStates = get_from_states( Transitions ),
	ToStates = get_to_states( Transitions ),
	_AcceptStates = sets:subtract( ToStates, FromStates ).

% alphabet_auto returns set of all symbols.
alphabet_auto( Logic ) ->
	Transitions = Logic:transition_function(),
	_Alphabet = get_symbols( Transitions ).

% get_from_states returns set of used from-states.
get_from_states( Transitions ) ->
	sets:from_list( [ FromState || { { FromState, _Symbol }, _ToState } <- Transitions ] ).

% get_symbols returns set of used symbols.
get_symbols( Transitions ) ->
	sets:from_list( [ Symbol || { { _FromState, Symbol }, _ToState } <- Transitions ] ).

% get_to_states returns set of used to-states.
get_to_states( Transitions ) ->
	sets:from_list( [ ToState || { { _FromState, _Symbol }, ToState } <- Transitions ] ).

is_accept_state( State ) ->
	sets:is_element( State, get( { ?MODULE, accept_states } ) ).

%
% Utility for building dot-file.
%

%erl_to_dot( Logic ) ->
	

