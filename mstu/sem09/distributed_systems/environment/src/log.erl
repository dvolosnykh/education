-module( log ).
-export( [ write / 3 ] ).

write( Id, Format, Arguments ) ->
	io:format( "~p: " ++ Format ++ "\n", [ Id ] ++ Arguments ).
