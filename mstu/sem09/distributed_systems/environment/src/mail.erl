-module( mail ).
-export( [ start_retriever / 4, stop_retriever / 1, retrieve_all / 3, send / 6,
	parse / 1, make_message / 4 ] ).

-include( "settings.hrl" ).

%
% Retriever-wise functions.
%

start_retriever
(
	User = { _Login, _Password },
	Server = { _Address, _Port },
	{ Callback = { _Module, _Function }, Data },
	Interval
) ->
	{ ok, Timer } = timer:apply_interval( Interval, ?MODULE, retrieve_all, [ User, Server, { Callback, Data } ] ),
	Timer.

stop_retriever( Timer ) ->
	timer:cancel( Timer ).

% Retrival.

-ifndef( TRANSPORT_FAKE ).

retrieve_all( _User = { Login, Password }, _Server = { _Address, Port }, { Callback, Data } ) ->
	ConnectResult = epop_client:connect( Login, Password, [ { port, Port } ] ),
	case ConnectResult of
		{ ok, SessionKey } ->
			StatResult = epop_client:stat( SessionKey ),
			case StatResult of
				{ ok, { Count, _SizeTotal } } ->
					Messages = retrieve( SessionKey, Count ),
					lists:foreach( fun( Message ) -> Callback( Message, Data ) end, Messages );
				{ error, Reason } ->
					log:write( node(), "POP3 command '~p' failed with reason ~p.", [ stat, Reason ] )
			end;
		{ error, Reason } ->
			log:write( node(), "POP3 command '~p' failed with reason ~p.", [ connect, Reason ] );
		_Unknown -> void
	end.

retrieve( SessionKey, Count ) ->
	retrieve( SessionKey, 0, Count, [] ).

retrieve( SessionKey, RetrievedCount, TotalCount, Messages ) when RetrievedCount < TotalCount ->
	Number = RetrievedCount + 1,
	case epop_client:retrieve( SessionKey, Number ) of
		{ ok, Message } ->
			_DeleteResult = epop_client:delete( SessionKey, Number ),
			retrieve( SessionKey, RetrievedCount + 1, TotalCount, [ Message | Messages ] );
		{ error, Reason } ->
			log:write( node(), "POP3 command '~p' failed with reason ~p.", [ retrieve, Reason ] )
	end;
retrieve( SessionKey, _Number, _Count, Messages ) ->
	_QuitResult = epop_client:quit( SessionKey ),
	lists:reverse( Messages ).

-else.

retrieve_all( _User = { Login, _Password }, _Server, { Callback, Data } ) ->
	Messages = rpc:call( ?MANAGER_NODE, manager_db, get_mail, [ Login ] ),
	lists:foreach( fun( Message ) -> Callback( Message, Data ) end, Messages ).

-endif.

%
% Send mail message.
%

-ifndef( TRANSPORT_FAKE ).

send( From, To, Subject, Body, _Server = { Address, Port }, Password ) ->
	Message = make_message( From, To, Subject, Body ),
	{ ok, Pid } = smtp_fsm:start( Address, Port ),
	smtp_fsm:ehlo( Pid ),
	smtp_fsm:login( Pid, From, Password ),
	smtp_fsm:sendemail( Pid, From, To, Message ),
	smtp_fsm:close( Pid ).

-else.

send( From, To, Subject, Body, _Server, Password ) ->
	Message = mail:make_message( From, To, Subject, Body ),
	rpc:call( ?MANAGER_NODE, manager_db, save_mail, [ To, Message ] ).

-endif.

%
% Message related stuff.
%

parse( Message ) ->
	{ message, Headers, Body } = epop_message:parse( Message ),
	From = get_header_value( "From", Headers ),
	To = get_header_value( "To", Headers ),
	Subject = get_header_value( "Subject", Headers ),
	{ From, To, Subject, Body }.

get_header_value( Key, Headers ) ->
	Result = epop_message:find_header( Headers, Key ),
	case Result of
		{ ok, Value }		->	Value;
		{ error, _Error }	->	undefined
	end.

make_message( From, To, Subject, Body ) ->
	lists:concat
	( [
		"From: ", From, "\r\n",
		"To: ", To, "\r\n",
		"Subject: ", Subject, "\r\n",
		"\r\n",
		Body
	] ).
