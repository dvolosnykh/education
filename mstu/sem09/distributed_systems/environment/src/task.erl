-module( task ).
-export( [ execute / 1, cancel / 1, send_mail / 2 ] ).

-behaviour( protocol ).
-export( [ init / 1, terminate / 2, handle_message / 3, handle_exit / 2,
	start_subtask / 2, polling / 1, reserving / 1, obtaining_visas / 1,
	confirming / 1, succeeded / 1, failed / 1, cancelled / 1 ] ).

-include( "settings.hrl" ).

%
% task interface.
%

execute( TaskId ) ->
	protocol:start( TaskId, { TaskId } ).

%resume( Id,

cancel( TaskId ) ->
	protocol:send_message( TaskId, cancel ).

send_mail( TaskId, Message ) ->
	protocol:send_message( TaskId, { mail, Message } ).

%
% protocol behaviour.
%

init( { TaskId } ) ->
	prepare( TaskId ),
	log:write( node(), "~p: Started.", [ get( { ?MODULE, id } ) ] ).

terminate( Reason, Subtask ) ->
	log:write( node(), "~p: Stopped in subtask ~p due to reason ~p.",
		[ get( { ?MODULE, id } ), Subtask, Reason ] ).

% Subtask completed.
handle_message( { subtask, Result }, _Subtask, Final ) ->
	put( { ?MODULE, subtask }, undefined ),
	case Final of
		false	->	protocol:send_event( get( { ?MODULE, id } ), Result );
		true	->	protocol:stop( get( { ?MODULE, id } ) )
	end;
% Task received mail.
handle_message( { mail, Message }, _Subtask, _Final ) ->
	SubtaskPid = get( { ?MODULE, subtask } ),
	case is_subtask_alive( SubtaskPid ) of
		true	->	SubtaskPid ! { mail, Message };
		false	->	void
	end;
% Task is requested for cancellation.
handle_message( cancel, _Subtask, Final ) ->
	case Final of
		false ->
			SubtaskPid = get( { ?MODULE, subtask } ),
			case is_subtask_alive( SubtaskPid ) of
				true	->	exit( SubtaskPid, kill );
				false	->	void
			end,
			protocol:send_event( get( { ?MODULE, id } ), cancelled );
		true -> ignore
	end;
% Ignore undefined requests.
handle_message( _Message, _State, _Final ) ->
	ignore.

handle_exit( Pid, Reason ) ->
	case get( { ?MODULE, subtask } ) of
		Pid ->
			case Reason of
				normal -> void;
				Reason -> exit( Reason )
			end;
		_Other -> ignore
	end.

start_subtask( Subtask, _Final ) ->
	SubtaskPid = spawn_link( ?MODULE, Subtask, [ get( { ?MODULE, id } ) ] ),
	put( { ?MODULE, subtask }, SubtaskPid ).

% Helping stuff.

prepare( TaskId ) ->
	put( { ?MODULE, id }, TaskId ).

is_subtask_alive( SubtaskPid ) ->
	SubtaskPid =/= undefined andalso is_process_alive( SubtaskPid ).

%
% protocol states.
%

% polling.

polling( TaskId ) ->
	agency_db:set_task_state( TaskId, polling, undefined ),
	TryNumber = agency_db:increase_try_number( TaskId ),
	log:write( node(), "~p: try number #~p.", [ TaskId, TryNumber ] ),
	SubtaskResult =
		case TryNumber =< ?RETRY_TIMES of
			true	->	try_polling( TaskId );
			false	->
				log:write( node(), "~p: failing due to reaching limit of try times.", [ TaskId ] ),
				failed
		end,
	agency_db:set_task_state( TaskId, polling, SubtaskResult ),
	log:write( node(), "~p: Polling ~p.", [ TaskId, SubtaskResult ] ),
	protocol:send_message( TaskId, { subtask, SubtaskResult } ).

try_polling( TaskId ) ->
	log:write( node(), "~p: Polling known hotels.", [ TaskId ] ),
	Results = query_hotels(),
	log:write( node(), "~p: Results: ~p.", [ TaskId, Results ] ),
	case Results =/= [] of
		true ->
			{ BestId, MaxCapacity } = find_best( Results ),
			PlacesCount = agency_db:get_task_request( TaskId ),
			case MaxCapacity >= PlacesCount of
				true ->
					agency_db:save_target_hotel( TaskId, BestId ),
					succeeded;
				false ->
					log:write( node(), "~p: None of the hotels has enough places (~p).", [ TaskId, PlacesCount ] ),
					retry
			end;
		false -> retry
	end.

query_hotels() ->
	QueryHotel =
		fun( _Hotel = { Id, Rpc }, Results ) ->
			Result = transport:rpc_call( Rpc, { query_free_places, [] } ),
			case Result of
				{ ok, Capacity }	->	[ { Id, Capacity } | Results ];
				_Other				->	Results
			end
		end,
	_Results = lists:foldl( QueryHotel, [], agency_db:get_known_hotels( rpc ) ).

find_best( Results ) ->
	FindBest =
		fun( Result = { _, Capacity }, BestResult = { _, MaxCapacity } ) ->
			case Capacity > MaxCapacity of
				true	->	Result;
				false	->	BestResult
			end
		end,
	_BestResult = lists:foldl( FindBest, lists:nth( 1, Results ), Results ).

% reserving.

reserving( TaskId ) ->
	log:write( node(), "~p: Reserving places in the chosen hotel.", [ TaskId ] ),
	PlacesNumber = agency_db:get_task_request( TaskId ),
	{ AgencyMailBox, Password } = agency_db:get_mail(),
	HotelMailBox = agency_db:get_target_hotel_mail( TaskId ),
	Body = json:encode( PlacesNumber ),
	MailResult = transport:sync_send_mail( AgencyMailBox, HotelMailBox, ?SUBJECT_RESERVE, Body, ?SMTP_SERVER, Password ),
	SubtaskResult =
		case MailResult of
			{ ok, ReplyReserveBody } ->
				case json:parse( ReplyReserveBody ) of
					[ <<"affirmative">>, RequestId ] ->
						agency_db:save_request_id( TaskId, RequestId ),
						succeeded;
					<<"negative">>	->	failed;
					_Undefined		->	failed
				end;
			{ error, timeout } -> failed
		end,
	log:write( node(), "~p: Reserving ~p.", [ TaskId, SubtaskResult ] ),
	protocol:send_message( TaskId, { subtask, SubtaskResult } ).

% obtaining_visas.

obtaining_visas( TaskId ) ->
	log:write( node(), "~p: Obtaining visas from the single known embassy.", [ TaskId ] ),
	{ AgencyMailBox, Password } = agency_db:get_mail(),
	[ { _, EmbassyMailBox } | _ ] = agency_db:get_known_embassies( mailbox ),
	PlacesNumber = agency_db:get_task_request( TaskId ),
	ObtainVisa =
		fun( Index ) ->
			Body = json:encode( Index ),
			MailResult = transport:sync_send_mail( AgencyMailBox, EmbassyMailBox, ?SUBJECT_OBTAIN_VISA, Body, ?SMTP_SERVER, Password ),
			case MailResult of
				{ ok, ReplyBody } ->
					case json:parse( ReplyBody ) of
						<<"affirmative">>	->	true;
						<<"negative">>		->	false;
						_Undefined			->	false
					end;
				{ error, timeout } -> false
			end
		end,
	Results = lists:map( ObtainVisa, lists:seq( 1, PlacesNumber ) ),
	SubtaskResult =
		case lists:all( fun( Result ) -> Result end, Results ) of
			true	->	succeeded;
			false	->	failed
		end,
	log:write( node(), "~p: Obtaining visas ~p.", [ TaskId, SubtaskResult ] ),
	protocol:send_message( TaskId, { subtask, SubtaskResult } ).

% confirming

confirming( TaskId ) ->
	log:write( node(), "~p: Confirming reservation in the chosen hotel.", [ TaskId ] ),
	RequestId = agency_db:get_request_id( TaskId ),
	{ AgencyMailBox, Password } = agency_db:get_mail(),
	HotelMailBox = agency_db:get_target_hotel_mail( TaskId ),
	Body = json:encode( RequestId ),
	MailResult = transport:sync_send_mail( AgencyMailBox, HotelMailBox, ?SUBJECT_CONFIRM, Body, ?SMTP_SERVER, Password ),
	SubtaskResult =
		case MailResult of
			{ ok, ReplyBody } ->
				case json:parse( ReplyBody ) of
					<<"affirmative">>	->	succeeded;
					<<"negative">>		->	failed;
					_Undefined			->	failed
				end;
			{ error, timeout } -> failed
		end,
	log:write( node(), "~p: Confirming ~p.", [ TaskId, SubtaskResult ] ),
	protocol:send_message( TaskId, { subtask, SubtaskResult } ).

% succeeded.

succeeded( TaskId ) ->
	log:write( node(), "~p: Notifying about success.", [ TaskId ] ),
	HotelMailBox = agency_db:get_target_hotel_mail( TaskId ),
	RequestId = agency_db:get_request_id( TaskId ),
	Reply = { HotelMailBox, RequestId },
	agency:send_message( { task_stopped, TaskId, { succeeded, Reply } } ),
	protocol:send_message( TaskId, { subtask, succeeded } ).

% failed.

failed( TaskId ) ->
	log:write( node(), "~p: Notifying about failure.", [ TaskId ] ),
	agency:send_message( { task_stopped, TaskId, failed } ),
	protocol:send_message( TaskId, { subtask, succeeded } ).

% cancelled.

cancelled( TaskId ) ->
	log:write( node(), "~p: Notifying about cancellation.", [ TaskId ] ),
	agency:send_message( { task_stopped, TaskId, cancelled } ),
	protocol:send_message( TaskId, { subtask, succeeded } ).
