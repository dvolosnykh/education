-module( manager_db ).
-export( [ start / 0, stop / 0, get_mail / 1, save_mail / 2, get_rpc_server / 1,
	get_transport_settings / 1, create / 1, delete / 0 ] ).

-record( mailbox, { login, letters = queue:new() } ).
-record( rpc_server, { address, node } ).

-include( "settings.hrl" ).
-include_lib( "stdlib/include/qlc.hrl" ).

start() ->
	mnesia:start(),
	mnesia:wait_for_tables( [ mailbox, rpc_server ], 2000 ).

stop() ->
	mnesia:stop().

get_mail( Login ) ->
	Retrieve =
		fun() ->
			[ Entry ] = mnesia:read( { mailbox, Login } ),
			mnesia:write( Entry#mailbox{ letters = queue:new() } ),
			queue:to_list( Entry#mailbox.letters )
		end,
	_Messages = transaction( Retrieve ).

save_mail( Login, Message ) ->
	UpdateBox =
		fun() ->
			[ Entry ] = mnesia:read( { mailbox, Login } ),
			Letters = Entry#mailbox.letters,
			mnesia:write( Entry#mailbox{ letters = queue:in( Message, Letters ) } )
		end,
	transaction( UpdateBox ).

get_rpc_server( Server ) ->
	GetRpcServer =
		fun() ->
			[ Entry ] = mnesia:read( { rpc_server, Server } ),
			Entry#rpc_server.node
		end,
	transaction( GetRpcServer ).

get_transport_settings( Node ) ->
	Query = qlc:q( [ X#rpc_server.address || X <- mnesia:table( rpc_server ), X#rpc_server.node =:= Node ] ),
	Result = transaction( fun() -> qlc:e( Query ) end ),
	case Result of
		[ RpcServer ] -> RpcServer;
		Undefined ->
			log:write( node(), "Transport settings: ~p.", [ Undefined ] ),
			Undefined
	end.

%
% Creation.
%

create( NodesInfo ) ->
	mnesia:create_schema( [ node() ] ),
	mnesia:start(),
	mnesia:create_table( mailbox, [ { attributes, record_info( fields, mailbox ) },
		{ disc_copies, [ node() ] } ] ),
	mnesia:create_table( rpc_server, [ { attributes, record_info( fields, rpc_server ) },
		{ disc_copies, [ node() ] } ] ),
	mnesia:wait_for_tables( [ mailbox, rpc_server ], 2000 ),
	register_systems( NodesInfo ),
	mnesia:stop().

register_systems( NodesInfo ) ->
	lists:foreach( fun register_system / 1, NodesInfo ),
	register_manager().

register_system( _NodeInfo = { Node, _Name, _Type, SettingsFile } ) ->
	Result = file:read_file( SettingsFile ),
	case Result of
		{ ok, Binary } ->
			{ struct, List } = json:parse( Binary ),
			Dictionary = dict:from_list( List ),
			[ MailBox, _Password ] = dict:fetch( <<"mail">>, Dictionary ),
			[ Address, Port ] = dict:fetch( <<"rpc">>, Dictionary ),
			RegisterSystem =
				fun() ->
					mnesia:write( #mailbox{ login = binary_to_list( MailBox ) } ),
					mnesia:write( #rpc_server{ address = { binary_to_list( Address ), Port }, node = Node } )
				end,
			transaction( RegisterSystem );
		{ error, Reason } ->
			log:write( node(), "Failed to read settings due to reason: ~p.", [ Reason ] ),
			{ error, Reason }
	end.

register_manager() ->
	RegisterManager =
		fun() ->
			mnesia:write( #rpc_server{ address = { "localhost", 4577 }, node = node() } )
		end,
	transaction( RegisterManager ).

%
% Deletion.
%

delete() ->
	mnesia:delete_schema( [ node() ] ).

%
% Utilities.
%

transaction( T ) ->
	Result = mnesia:transaction( T ),
	case Result of
		{ atomic, Value }	->	Value;
		{ aborted, Reason } ->
			log:write( ?MODULE, "TRANSACTION ABORTED: ~p.", [ Reason ] ),
			error
	end.
