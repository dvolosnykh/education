-module( remote ).
-export( [ start_server / 2, stop_server / 0, call / 2, handle_rpc / 2 ] ).

-include( "settings.hrl" ).

%
% Server-wise functions.
%

-ifndef( TRANSPORT_FAKE ).

start_server( Port, { Callback = { _Module, _Function }, Data } ) ->
	{ ok, Pid } = xmlrpc:start_link( Port, ?RPC_MAX_SESSIONS, ?RPC_TIMEOUT,
		{ ?MODULE, handle_rpc }, { Callback, Data } ),
	register( rpc_server, Pid ).

stop_server() ->
	xmlrpc:stop( whereis( rpc_server ) ).

-else.

start_server( _Port, { Callback = { _Module, _Function }, Data } ) ->
	register( rpc_server, spawn_link( fun() -> main_loop( { Callback, Data } ) end ) ).

stop_server() ->
	whereis( rpc_server ) ! { stop, self() },
	receive
		{ Pid, Reply } -> Reply
	end.

-endif.

% Helping stuff.

-ifndef( TRANSPORT_FAKE ).

handle_rpc( { Callback, Data }, { call, Function, Arguments } ) ->
	Result = Callback( { Function, Arguments }, Data ),
	{ false, { response, [ Result ] } }.

-else.

main_loop( { Callback, Data } ) ->
	receive
		{ rpc_call, From, Call } ->
			Result = handle_rpc( { Callback, Data }, Call ),
			From ! { rpc_reply, self(), Result },
			main_loop( { Callback, Data } );
		{ stop, From } ->
			From ! { self(), stop };
		_Unknown -> % ingnore undefined messages.
			main_loop( { Callback, Data } )
	end.

handle_rpc( { Callback, Data }, Call ) ->
	_Result = Callback( Call, Data ).

-endif.

%
% Making rpc-call.
%

-ifndef( TRANSPORT_FAKE ).

call( _Server = { Address, Port }, { Function, Arguments } ) ->
	RpcResult = xmlrpc:call( Address, Port, "/", { call, Function, Arguments } ),
	case RpcResult of
		{ ok, { response, [ Result ] } }	->	{ ok, Result };
		{ error, Reason }					->	{ error, Reason }
	end.

-else.

call( Server, Call = { _Function, _Arguments } ) ->
	Node = rpc:call( ?MANAGER_NODE, manager_db, get_rpc_server, [ Server ] ),
	From = self(),
	RpcCall =
		fun() ->
			ServerPid = whereis( rpc_server ),
			ServerPid ! { rpc_call, From, Call }
		end,
	spawn_link( Node, RpcCall ),
	receive
		{ rpc_reply, _ServerPid, Result } -> { ok, Result }
	after
		?RPC_TIMEOUT -> { error, timeout }
	end.

-endif.
