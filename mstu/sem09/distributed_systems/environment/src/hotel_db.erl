-module( hotel_db ).
-export( [ start / 0, stop / 0, create / 1, delete / 0 ] ).

-export( [ get_mail / 0, get_transport_settings / 0, how_many_free / 0,
	get_cancel_period / 0 ] ).
-export( [ reserve_places / 1, cancel_unconfirmed_requests / 0,
	confirm_request / 1 ] ).

-record( hotel, { id, mail, rpc, capacity, free = undefined, cancel_period } ).
-record( request, { id, places_number, confirmed = false, time } ).

-include_lib( "stdlib/include/qlc.hrl" ).

%
% agency_db behaviour.
%

start() ->
	mnesia:start(),
	mnesia:wait_for_tables( [ hotel, request ], 2000 ).

stop() ->
	mnesia:stop().

create( JsonSettings ) ->
	mnesia:create_schema( [ node() ] ),
	mnesia:start(),
	mnesia:create_table( hotel, [ { attributes, record_info( fields, hotel ) },
		{ disc_copies, [ node() ] } ] ),
	mnesia:create_table( request, [ { attributes, record_info( fields, request ) },
		{ disc_copies, [ node() ] } ] ),
	mnesia:wait_for_tables( [ hotel, request ], 2000 ),
	Settings = unpack_settings( JsonSettings ),
	save_settings( Settings ),
	initialize_free_places(),
	mnesia:stop().

initialize_free_places() ->
	CheckFreePlaces =
		fun() ->
			[ Entry ] = mnesia:read( { hotel, node() } ),
			case Entry#hotel.free =:= undefined of
				true	->	mnesia:write( Entry#hotel{ free = Entry#hotel.capacity } );
				false	->	void
			end
		end,
	transaction( CheckFreePlaces ).

delete() ->
	mnesia:delete_schema( [ node() ] ).

% Hotel-wise functions.

get_mail() ->
	GetMailBox =
		fun() ->
			[ Entry ] = mnesia:read( { hotel, node() } ),
			Entry#hotel.mail
		end,
	transaction( GetMailBox ).

get_transport_settings() ->
	GetTransportSettings =
		fun() ->
			[ Entry ] = mnesia:read( { hotel, node() } ),
			{ Entry#hotel.mail, Entry#hotel.rpc }
		end,
	transaction( GetTransportSettings ).

how_many_free() ->
	HowManyFree =
		fun() ->
			[ Entry ] = mnesia:read( { hotel, node() } ),
			Entry#hotel.free
		end,
	transaction( HowManyFree ).

get_cancel_period() ->
	GetCancelPeriod =
		fun() ->
			[ Entry ] = mnesia:read( { hotel, node() } ),
			Entry#hotel.cancel_period
		end,
	transaction( GetCancelPeriod ).

% Request-wise functions.

reserve_places( Number ) ->
	ReservePlaces =
		fun() ->
			RequestId = generate_request_id(),
			mnesia:write( #request{ id = RequestId, places_number = Number,
				time = calendar:local_time() } ),
			[ Entry ] = mnesia:read( { hotel, node() } ),
			mnesia:write( Entry#hotel{ free = Entry#hotel.free - Number } ),
			RequestId
		end,
	transaction( ReservePlaces ).

cancel_unconfirmed_requests() ->
	CancelPeriod = get_cancel_period(),
	CancelUnconfirmedRequests =
		fun() ->
			QueryUnconfirmed = qlc:q
			( [
				{ X#request.id, X#request.places_number } || X <- mnesia:table( request ),
				is_too_old( X#request.time, CancelPeriod ), not X#request.confirmed
			] ),
			OldRequests = qlc:e( QueryUnconfirmed ),
			CancelRequest =
				fun( { Id, PlacesNumber }, TotalPlacesNumber ) ->
					mnesia:delete( { request, Id } ),
					TotalPlacesNumber + PlacesNumber
				end,
			UnconfirmedPlaces = lists:foldl( CancelRequest, 0, OldRequests ),
			[ HotelEntry ] = mnesia:read( { hotel, node() } ),
			mnesia:write( HotelEntry#hotel{ free = HotelEntry#hotel.free + UnconfirmedPlaces } )
		end,
	transaction( CancelUnconfirmedRequests ).

confirm_request( RequestId ) ->
	ConfirmRequest =
		fun() ->
			List = mnesia:read( { request, RequestId } ),
			IsStillReserved = ( List =/= [] ),
			case IsStillReserved of
				true ->
					[ Entry ] = List,
					mnesia:write( Entry#request{ confirmed = true } );
				false -> void
			end,
			IsStillReserved
		end,
	transaction( ConfirmRequest ).

% Helping stuff.

is_too_old( ReserveTime, CancelPeriod ) ->
	calendar:datetime_to_gregorian_seconds( calendar:local_time() ) -
		calendar:datetime_to_gregorian_seconds( ReserveTime ) >= CancelPeriod.

generate_request_id() ->
	calendar:datetime_to_gregorian_seconds( calendar:local_time() ).

%
% Creation helping stuff.
%

unpack_settings( JsonBinary ) ->
	{ struct, List } = json:parse( JsonBinary ),
	Dictionary = dict:from_list( List ),
	[ MailBox, Password ] = dict:fetch( <<"mail">>, Dictionary ),
	[ Address, Port ] = dict:fetch( <<"rpc">>, Dictionary ),
	Capacity = dict:fetch( <<"capacity">>, Dictionary ),
	CancelPeriod = dict:fetch( <<"cancel_period">>, Dictionary ),
	MailUser = { binary_to_list( MailBox ), binary_to_list( Password ) },
	RpcServer = { binary_to_list( Address ), Port },
	{ MailUser, RpcServer, Capacity, CancelPeriod }.

save_settings( _Settings = { MailUser, RpcServer, Capacity, CancelPeriod } ) ->
	SaveHotel =
		fun() ->
			Record = #hotel{ id = node(), mail = MailUser, rpc = RpcServer,
				capacity = Capacity, cancel_period = CancelPeriod },
			mnesia:write( Record )
		end,
	transaction( SaveHotel ).
%
% Utilities.
%

transaction( T ) ->
	Result = mnesia:transaction( T ),
	case Result of
		{ atomic, Value }	->	Value;
		{ aborted, Reason } ->
			log:write( ?MODULE, "TRANSACTION ABORTED: ~p.", [ Reason ] ),
			error
	end.
