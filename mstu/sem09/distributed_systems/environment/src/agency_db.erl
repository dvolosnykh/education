-module( agency_db ).
-export( [ start / 0, stop / 0, create / 1, delete / 0 ] ).

-export( [ get_mail / 0, get_transport_settings / 0, get_known_hotels / 1,
	get_known_embassies / 1, enqueue_task / 1, dequeue_task / 0, drop_task / 1,
	is_queue_empty / 0, get_ui_rpc_server / 0 ] ).
-export( [ set_current_task / 1, get_current_task / 0, save_task_request / 2,
	get_task_request / 1, delete_task_request / 1, increase_try_number / 1,
	save_target_hotel / 2, get_target_hotel_mail / 1, set_task_state / 3,
	save_request_id / 2, get_request_id / 1 ] ).

-record( agency, { id, mail, rpc, current_task = undefined, queue = queue:new(), ui_rpc } ).
-record( task, { id, places_count, state, state_data, target_hotel,
	target_hotel_request_id = undefined, try_number = 0 } ).
-record( hotel, { id, mailbox, rpc } ).
-record( embassy, { id, mailbox, rpc } ).

-include_lib( "stdlib/include/qlc.hrl" ).

%
% agency_db behaviour.
%

start() ->
	mnesia:start(),
	mnesia:wait_for_tables( [ agency, task, hotel, embassy ], 2000 ).

stop() ->
	mnesia:stop().

create( JsonSettings ) ->
	mnesia:create_schema( [ node() ] ),
	mnesia:start(),
	mnesia:create_table( agency, [ { attributes, record_info( fields, agency ) },
		{ disc_copies, [ node() ] } ] ),
	mnesia:create_table( task, [ { attributes, record_info( fields, task ) },
		{ disc_copies, [ node() ] } ] ),
	mnesia:create_table( hotel, [ { attributes, record_info( fields, hotel ) },
		{ disc_copies, [ node() ] } ] ),
	mnesia:create_table( embassy, [ { attributes, record_info( fields, embassy ) },
		{ disc_copies, [ node() ] } ] ),
	mnesia:wait_for_tables( [ agency, task, hotel, embassy ], 2000 ),
	Settings = unpack_settings( JsonSettings ),
	save_settings( Settings ),
	mnesia:stop().

delete() ->
	mnesia:delete_schema( [ node() ] ).


% Agency-wise functions.

get_mail() ->
	GetMailBox =
		fun() ->
			[ Entry ] = mnesia:read( { agency, node() } ),
			Entry#agency.mail
		end,
	transaction( GetMailBox ).

get_transport_settings() ->
	GetTransportSettings =
		fun() ->
			[ Entry ] = mnesia:read( { agency, node() } ),
			{ Entry#agency.mail, Entry#agency.rpc }
		end,
	transaction( GetTransportSettings ).

get_known_hotels( mailbox ) ->
	Query = qlc:q( [ { X#hotel.id, X#hotel.mailbox } || X <- mnesia:table( hotel ) ] ),
	transaction( fun() -> qlc:e( Query ) end );
get_known_hotels( rpc ) ->
	Query = qlc:q( [ { X#hotel.id, X#hotel.rpc } || X <- mnesia:table( hotel ) ] ),
	transaction( fun() -> qlc:e( Query ) end ).

get_known_embassies( mailbox ) ->
	Query = qlc:q( [ { X#embassy.id, X#embassy.mailbox } || X <- mnesia:table( embassy ) ] ),
	transaction( fun() -> qlc:e( Query ) end );
get_known_embassies( rpc ) ->
	Query = qlc:q( [ { X#embassy.id, X#embassy.rpc } || X <- mnesia:table( embassy ) ] ),
	transaction( fun() -> qlc:e( Query ) end ).

enqueue_task( TaskId ) ->
	EnqueueTask =
		fun() ->
			[ Entry ] = mnesia:read( { agency, node() } ),
			OldQueue = Entry#agency.queue,
			NewQueue = queue:in( TaskId, OldQueue ),
			mnesia:write( Entry#agency{ queue = NewQueue } )
		end,
	transaction( EnqueueTask ).

dequeue_task() ->
	DequeueTask =
		fun() ->
			[ Entry ] = mnesia:read( { agency, node() } ),
			OldQueue = Entry#agency.queue,
			case queue:out( OldQueue ) of
				{ { value, TaskId }, NewQueue } ->
					mnesia:write( Entry#agency{ queue = NewQueue } ),
					TaskId;
				{ empty, _OldQueue } -> empty
			end
		end,
	transaction( DequeueTask ).

drop_task( TaskId ) ->
	DropTask =
		fun() ->
			[ Entry ] = mnesia:read( { agency, node() } ),
			OldQueue = Entry#agency.queue,
			NewQueue = queue:filter( fun( Id ) -> Id =/= TaskId end, OldQueue ),
			mnesia:write( Entry#agency{ queue = NewQueue } )
		end,
	transaction( DropTask ).

is_queue_empty() ->
	IsQueueEmpty =
		fun() ->
			[ Entry ] = mnesia:read( { agency, node() } ),
			Queue = Entry#agency.queue,
			queue:is_empty( Queue )
		end,
	transaction( IsQueueEmpty ).

get_ui_rpc_server() ->
	GetUiRpcServer =
		fun() ->
			[ Entry ] = mnesia:read( { agency, node() } ),
			Entry#agency.ui_rpc
		end,
	transaction( GetUiRpcServer ).

% Task-wise functions.

set_current_task( TaskId ) ->
	SetCurrentTask =
		fun() ->
			[ Entry ] = mnesia:read( { agency, node() } ),
			mnesia:write( Entry#agency{ current_task = TaskId } )
		end,
	transaction( SetCurrentTask ).

get_current_task() ->
	GetCurrentTask =
		fun() ->
			[ Entry ] = mnesia:read( { agency, node() } ),
			Entry#agency.current_task
		end,
	transaction( GetCurrentTask ).

save_task_request( TaskId, _Request = PlacesCount ) ->
	SaveTask =
		fun() ->
			mnesia:write( #task{ id = TaskId, places_count = PlacesCount } )
		end,
	transaction( SaveTask ).

get_task_request( TaskId ) ->
	GetTaskRequest =
		fun() ->
			[ Entry ] = mnesia:read( { task, TaskId } ),
			Entry#task.places_count
		end,
	transaction( GetTaskRequest ).

delete_task_request( TaskId ) ->
	transaction( fun() -> mnesia:delete( { task, TaskId } ) end ).

increase_try_number( TaskId ) ->
	IncreaseTryNumber =
		fun() ->
			[ Entry ] = mnesia:read( { task, TaskId } ),
			N = Entry#task.try_number + 1,
			mnesia:write( Entry#task{ try_number = N } ),
			N
		end,
	transaction( IncreaseTryNumber ).

save_target_hotel( TaskId, HotelId ) ->
	SaveTargetHotel =
		fun() ->
			[ Entry ] = mnesia:read( { task, TaskId } ),
			mnesia:write( Entry#task{ target_hotel = HotelId } )
		end,
	transaction( SaveTargetHotel ).

get_target_hotel_mail( TaskId ) ->
	GetTargetHotelMail =
		fun() ->
			[ TaskEntry ] = mnesia:read( { task, TaskId } ),
			HotelId = TaskEntry#task.target_hotel,
			[ HotelEntry ] = mnesia:read( { hotel, HotelId } ),
			HotelEntry#hotel.mailbox
		end,
	transaction( GetTargetHotelMail ).

set_task_state( TaskId, State, StateData ) ->
	SetTaskState =
		fun() ->
			[ Entry ] = mnesia:read( { task, TaskId } ),
			mnesia:write( Entry#task{ state = State, state_data = StateData } )
		end,
	transaction( SetTaskState ).

save_request_id( TaskId, RequestId ) ->
	SaveRequestId =
		fun() ->
			[ Entry ] = mnesia:read( { task, TaskId } ),
			mnesia:write( Entry#task{ target_hotel_request_id = RequestId } )
		end,
	transaction( SaveRequestId ).

get_request_id( TaskId ) ->
	GetRequestId =
		fun() ->
			[ Entry ] = mnesia:read( { task, TaskId } ),
			Entry#task.target_hotel_request_id
		end,
	transaction( GetRequestId ).

%
% Creation helping stuff.
%

unpack_settings( JsonBinary ) ->
	{ struct, List } = json:parse( JsonBinary ),
	Dictionary = dict:from_list( List ),
	[ MailBox, Password ] = dict:fetch( <<"mail">>, Dictionary ),
	[ RpcAddress, RpcPort ] = dict:fetch( <<"rpc">>, Dictionary ),
	[ UiRpcAddress, UiRpcPort ] = dict:fetch( <<"ui_rpc">>, Dictionary ),
	JsonHotels = dict:fetch( <<"known_hotels">>, Dictionary ),
	JsonEmbassies = dict:fetch( <<"known_embassies">>, Dictionary ),
	MailUser = { binary_to_list( MailBox ), binary_to_list( Password ) },
	RpcServer = { binary_to_list( RpcAddress ), RpcPort },
	UiRpcServer = { binary_to_list( UiRpcAddress ), UiRpcPort },
	Hotels = unpack_systems( JsonHotels ),
	Embassies = unpack_systems( JsonEmbassies ),
	{ MailUser, RpcServer, UiRpcServer, Hotels, Embassies }.

unpack_systems( JsonSystems ) ->
	Unpack =
		fun( JsonSystem, PrevSystems ) ->
			{ struct, List } = JsonSystem,
			Dictionary = dict:from_list( List ),
			[ RpcAddress, RpcPort ] = dict:fetch( <<"rpc">>, Dictionary ),
			MailBox = dict:fetch( <<"mailbox">>, Dictionary ),
			System = { length( PrevSystems ), binary_to_list( MailBox ), { binary_to_list( RpcAddress ), RpcPort } },
			[ System | PrevSystems ]
		end,
	lists:foldl( Unpack, [], JsonSystems ).

save_settings( _Settings = { MailUser, RpcServer, UiRpcServer, Hotels, Embassies } ) ->
	SaveAgency =
		fun() ->
			mnesia:write( #agency{ id = node(), mail = MailUser, rpc = RpcServer, ui_rpc = UiRpcServer } )
		end,
	transaction( SaveAgency ),
	register_systems( hotel, Hotels ),
	register_systems( embassy, Embassies ).

register_systems( hotel, Hotels ) ->
	RegisterHotels =
		fun() ->
			RegisterHotel =
				fun( _Hotel = { Index, MailBox, RpcServer } ) ->
					mnesia:write( #hotel{ id = Index, mailbox = MailBox, rpc = RpcServer } )
				end,
			lists:foreach( RegisterHotel, Hotels )
		end,
	transaction( RegisterHotels );
register_systems( embassy, Embassies ) ->
	RegisterEmbassies =
		fun() ->
			RegisterEmbassy =
				fun( _Embassy = { Index, MailBox, RpcServer } ) ->
					mnesia:write( #embassy{ id = Index, mailbox = MailBox, rpc = RpcServer } )
				end,
			lists:foreach( RegisterEmbassy, Embassies )
		end,
	transaction( RegisterEmbassies ).

%
% Utilities.
%

transaction( T ) ->
	Result = mnesia:transaction( T ),
	case Result of
		{ atomic, Value }	->	Value;
		{ aborted, Reason } ->
			log:write( ?MODULE, "TRANSACTION ABORTED: ~p.", [ Reason ] ),
			error
	end.
