-module( embassy_db ).
-export( [ start / 0, stop / 0, create / 1, delete / 0 ] ).

-export( [ get_mail / 0, get_transport_settings / 0, get_kindness / 0 ] ).

-record( embassy, { id, mail, rpc, kindness } ).

-include_lib( "stdlib/include/qlc.hrl" ).

%
% embassy_db behaviour.
%

start() ->
	mnesia:start(),
	mnesia:wait_for_tables( [ embassy ], 2000 ).

stop() ->
	mnesia:stop().

create( JsonSettings ) ->
	mnesia:create_schema( [ node() ] ),
	mnesia:start(),
	mnesia:create_table( embassy, [ { attributes, record_info( fields, embassy ) },
		{ disc_copies, [ node() ] } ] ),
	mnesia:wait_for_tables( [ embassy ], 2000 ),
	Settings = unpack_settings( JsonSettings ),
	save_settings( Settings ),
	mnesia:stop().

delete() ->
	mnesia:delete_schema( [ node() ] ).

% Embassy-wise functions.

get_mail() ->
	GetMailBox =
		fun() ->
			[ Entry ] = mnesia:read( { embassy, node() } ),
			Entry#embassy.mail
		end,
	transaction( GetMailBox ).

get_transport_settings() ->
	GetTransportSettings =
		fun() ->
			[ Entry ] = mnesia:read( { embassy, node() } ),
			{ Entry#embassy.mail, Entry#embassy.rpc }
		end,
	transaction( GetTransportSettings ).

get_kindness() ->
	GetKindness =
		fun() ->
			[ Entry ] = mnesia:read( { embassy, node() } ),
			Entry#embassy.kindness
		end,
	transaction( GetKindness ).

%
% Creation helping stuff.
%

unpack_settings( JsonBinary ) ->
	{ struct, List } = json:parse( JsonBinary ),
	Dictionary = dict:from_list( List ),
	[ MailBox, Password ] = dict:fetch( <<"mail">>, Dictionary ),
	[ Address, Port ] = dict:fetch( <<"rpc">>, Dictionary ),
	Kindness = dict:fetch( <<"kindness">>, Dictionary ),
	MailUser = { binary_to_list( MailBox ), binary_to_list( Password ) },
	RpcServer = { binary_to_list( Address ), Port },
	{ MailUser, RpcServer, Kindness }.

save_settings( _Settings = { MailUser, RpcServer, Kindness } ) ->
	SaveEmbassy =
		fun() ->
			Record = #embassy{ id = node(), mail = MailUser, rpc = RpcServer,
				kindness = Kindness },
			mnesia:write( Record )
		end,
	transaction( SaveEmbassy ).
%
% Utilities.
%

transaction( T ) ->
	Result = mnesia:transaction( T ),
	case Result of
		{ atomic, Value }	->	Value;
		{ aborted, Reason } ->
			log:write( ?MODULE, "TRANSACTION ABORTED: ~p.", [ Reason ] ),
			error
	end.
