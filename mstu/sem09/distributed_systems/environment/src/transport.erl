-module( transport ).
-export( [ start / 3, stop / 0, rpc_call / 2, send_mail / 6, sync_send_mail / 6,
	parse_mail / 1, make_mail / 4 ] ).
-export( [ handle_mail / 2, handle_rpc / 2 ] ).

-include( "settings.hrl" ).

%
% transport interface.
%

start( Owner, MailUser, RpcPort ) ->
	case MailUser of % start mail retriever.
		false ->
			put( { ?MODULE, mail }, false );
		User ->
			Retriever = mail:start_retriever( User, ?POP3_SERVER,
				{ { ?MODULE, handle_mail }, Owner }, ?MAIL_RETRIEVE_INTERVAL ),
			put( { ?MODULE, mail }, Retriever ),
			{ Box, _Password } = User,
			log:write( node(), "Started mail-retriever from ~p every ~p ms.", [ Box, ?MAIL_RETRIEVE_INTERVAL ] )
	end,
	case RpcPort of % start rpc server.
		false -> void;
		Port ->
			remote:start_server( Port, { { ?MODULE, handle_rpc }, Owner } ),
			log:write( node(), "Started RPC-server on port ~p.", [ Port ] )
	end.

stop() ->
	case whereis( rpc_server ) =/= undefined of % stop rpc server.
		false -> void;
		true ->
			remote:stop_server(),
			log:write( node(), "Stopped RPC-server.", [] )
	end,
	case get( { ?MODULE, mail } ) of % stop mail retriever.
		false -> void;
		Retriever ->
			mail:stop_retriever( Retriever ),
			log:write( node(), "Stopped mail-retriever.", [] )
	end.

rpc_call( Server, Call = { _Function, _Arguments } ) ->
	remote:call( Server, Call ).

send_mail( From, To, Subject, Body, Server, Password ) ->
	mail:send( From, To, Subject, Body, Server, Password ).

%
% Mail delivery stuff.
%

sync_send_mail( From, To, Subject, Body, Server, Password ) ->
	flush(),
	mail:send( From, To, Subject, Body, Server, Password ),
	Result = wait_for_mail( From, To, Subject ),
	flush(),
	Result.

wait_for_mail( From, To, Subject ) ->
	receive
		{ mail, ReplyMessage } ->
			{ _, _, _, ReplyBody } = parse_mail( ReplyMessage ),
			{ ok, ReplyBody }
	after
		?MAIL_TIMEOUT ->
			{ error, timeout }
	end.

% Flush message buffer.

flush() ->
	Marker = { marker, make_ref() },
	self() ! Marker,
	flush( Marker ).

flush( Marker ) ->
	receive
		Marker	->	ok;
		_Any	->	flush( Marker )
	end.

%
% Helping stuff.
%

handle_mail( Message, Owner ) ->
%	log:write( node(), "~p Received mail: ~p.", [ Owner, Message ] ),
	gen_server:cast( Owner, { mail, Message } ).

handle_rpc( Call, Owner ) ->
%	log:write( node(), "~p Received RPC-call: ~p.", [ Owner, Call ] ),
	whereis( Owner ) ! { test },
	gen_server:call( Owner, { rpc, Call } ).

%
% Message-wise functions.
%

parse_mail( Message ) ->
	mail:parse( Message ).

make_mail( From, To, Subject, Body ) ->
	mail:make_message( From, To, Subject, Body ).
