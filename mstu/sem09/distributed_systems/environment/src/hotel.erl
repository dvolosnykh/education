-module( hotel ).
-behaviour( system ).
-export( [ start / 0, stop / 0, initialize / 1, deinitialize / 0 ] ).

-behaviour( gen_server ).
-export( [ init / 1, handle_call / 3, handle_cast / 2, handle_info / 2,
	terminate / 2, code_change / 3 ] ).

-include( "settings.hrl" ).

%
% system behaviour.
%

start() ->
	{ ok, Pid } = gen_server:start( { local, ?MODULE }, ?MODULE, nodata, [] ),
	Pid.

stop() ->
	gen_server:call( ?MODULE, stop ).

initialize( [ SettingsFile ] ) ->
	Result = file:read_file( SettingsFile ),
	case Result of
		{ ok, Binary } ->
			hotel_db:create( Binary );
		{ error, Reason } ->
			log:write( node(), "Failed to read settings due to reason: ~p.", [ Reason ] )
	end.

deinitialize() ->
	hotel_db:delete().

%
% gen_server behaviour.
%

init( _Data ) ->
	prepare(),
	log:write( node(), "Started.", [] ),
	hotel_db:start(),
	{ MailUser, { _RpcAddress, RpcPort } } = hotel_db:get_transport_settings(),
	transport:start( ?MODULE, MailUser, RpcPort ),
	Timer = start_cancel_timer(),
	put( timer, Timer ),
	{ ok, listening }.

terminate( Reason, State ) ->
	stop_cancel_timer( get( timer ) ),
	transport:stop(),
	hotel_db:stop(),
	log:write( node(), "Stopped in state ~p due to reason ~p.", [ State, Reason ] ).

% Hotel received rpc-call.
handle_call( { rpc, Call }, _From, listening ) ->
	Result =
		case Call of
			{ query_free_places, [] }	->	query_free_places();
			_Undefined					->	undefined
		end,
	{ reply, Result, listening };
handle_call( stop, _From, listening ) ->
	{ stop, normal, ok, stopped };
handle_call( _Request, _From, State ) -> % ignore undefined call-requests.
	{ reply, undefined, State }.

% Hotel received mail.
handle_cast( { mail, Message }, listening ) ->
	{ From, To, Subject, Body } = transport:parse_mail( Message ),
	case Subject of
		?SUBJECT_RESERVE	->	reserve( From, Subject, Body );
		?SUBJECT_CONFIRM	->	confirm( From, Subject, Body );
		_Undefined			->	ignore
	end,
	{ noreply, listening };
handle_cast( _Request, State ) -> % ignore undefined cast-requests.
	{ noreply, State }.

handle_info( _Info, State ) -> % ignore undefined info-messages.
	{ noreply, State }.

code_change( _OldVersion, State, _Extra ) -> % ignore undefined code changes.
	{ ok, State }.

% Helping stuff.

prepare() ->
	process_flag( trap_exit, true ).

query_free_places() ->
	free_places().

reserve( From, Subject, Body ) ->
	PlacesNumber = json:parse( Body ),
	Result = try_reserve( PlacesNumber ),
	ReplyBody = json:encode( Result ),
	{ HotelMailBox, Password } = hotel_db:get_mail(),
	transport:send_mail( HotelMailBox, From, ?SUBJECT_REPLY( Subject ), ReplyBody, ?SMTP_SERVER, Password ).

confirm( From, Subject, Body ) ->
	RequestId = json:parse( Body ),
	Result = try_confirm( RequestId ),
	ReplyBody = json:encode( Result ),
	{ HotelMailBox, Password } = hotel_db:get_mail(),
	transport:send_mail( HotelMailBox, From, ?SUBJECT_REPLY( Subject ), ReplyBody, ?SMTP_SERVER, Password ).

%
% Core logic.
%

free_places() ->
	timer:sleep( 1000 ),
	hotel_db:how_many_free().

try_reserve( PlacesNumber ) ->
	timer:sleep( 1000 ),
	FreePlacesNumber = hotel_db:how_many_free(),
	case prolog_logic( PlacesNumber, FreePlacesNumber ) of
		true ->
			RequestId = hotel_db:reserve_places( PlacesNumber ),
			[ <<"affirmative">>, RequestId ];
		false -> <<"negative">>
	end.

try_confirm( RequestId ) ->
	timer:sleep( 1000 ),
	case hotel_db:confirm_request( RequestId ) of
		true	->	<<"affirmative">>;
		false	->	<<"negative">>
	end.

% Prolog logic.

prolog_logic( PlacesNumber, FreePlacesNumber ) ->
	Command = lists:concat( [
		"swipl -q -s ../src/solution.pl -t 'enough_places(",
		integer_to_list( PlacesNumber ),
		", ",
		integer_to_list( FreePlacesNumber ),
		")'"
	] ),
	Port = open_port( { spawn, Command }, [ exit_status, stream, in ] ),
	receive
		{ Port, { exit_status, Status } }	->	Status
	end,
	case Status of
		0	->	true;
		1	->	false
	end.

%
% Timer for cancellation of unconfirmed requests.
%

start_cancel_timer() ->
	Interval = hotel_db:get_cancel_period(),
	{ ok, Timer } = timer:apply_interval( Interval, hotel_db, cancel_unconfirmed_requests, [] ),
	Timer.

stop_cancel_timer( Timer ) ->
	timer:cancel( Timer ).
