-module( protocol ).
-export( [ start / 2, stop / 1, send_event / 2, send_message / 2 ] ).

-behaviour( dfa ).
-export( [ init / 1, terminate / 3, handle_message / 3, handle_exit / 2,
	states / 0, initial_state / 0, accept_states / 0, alphabet / 0,
	transition_function / 0, enter / 3, leave / 3 ] ).

-export( [ behaviour_info / 1 ] ).

%
% protocol interface.
%

start( Id, Data ) -> dfa:start_link( Id, ?MODULE, Data ).
stop( Id ) -> dfa:stop( Id ).
send_event( Id, Event ) -> dfa:send_event( Id, Event ).
send_message( Id, Message ) -> dfa:send_message( Id, Message ).

%
% dfa behaviour.
%

init( Data ) ->
	task:init( Data ),
	no_state_data.

terminate( Reason, State, _StateData ) ->
	task:terminate( Reason, State ).

handle_message( Message, State, _StateData ) ->
	task:handle_message( Message, _Subtask = State,
		dfa:is_accept_state( State ) ).

handle_exit( Pid, Reason ) ->
	task:handle_exit( Pid, Reason ).

enter( State, _Event, StateData ) ->
	task:start_subtask( _Subtask = State, dfa:is_accept_state( State ) ),
	StateData.

leave( _State, _Event, StateData ) ->
	StateData.

states() -> dfa:states_auto( ?MODULE ).
initial_state() -> dfa:initial_state_auto( ?MODULE ).
accept_states() -> dfa:accept_states_auto( ?MODULE ).
alphabet() -> dfa:alphabet_auto( ?MODULE ).
transition_function() ->
	[
		{ { polling, succeeded }, reserving },
		{ { polling, cancelled }, cancelled },
		{ { polling, retry }, polling },
		{ { polling, failed }, failed },
		{ { reserving, succeeded }, obtaining_visas },
		{ { reserving, failed }, polling },
		{ { reserving, cancelled }, cancelled },
		{ { obtaining_visas, succeeded }, confirming },
		{ { obtaining_visas, failed }, failed },
		{ { obtaining_visas, cancelled }, cancelled },
		{ { confirming, succeeded }, succeeded },
		{ { confirming, failed }, polling },
		{ { confirming, cancelled }, cancelled }
	].

%
% Define protocol behaviour.
%

behaviour_info( callbacks ) ->
	ProtocolCallbacks = [ { init, 1 }, { terminate, 2 },
		{ handle_message, 3 }, { handle_exit, 2 },
		{ start_subtask, 2 } ],
	StatesFunctions = [ { State, 1 } || State <- sets:to_list( states() ) ],
	_Callbacks = ProtocolCallbacks ++ StatesFunctions;
behaviour_info( _Info ) -> % ignore unexpected info-messages.
	undefined.
