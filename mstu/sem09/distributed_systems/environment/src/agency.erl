-module( agency ).
-behaviour( system ).
-export( [ start / 0, stop / 0, initialize / 1, deinitialize / 0 ] ).

-export( [ send_message / 1 ] ).

-behaviour( gen_server ).
-export( [ init / 1, handle_call / 3, handle_cast / 2, handle_info / 2,
	terminate / 2, code_change / 3 ] ).

-include( "settings.hrl" ).

%
% agency interface.
%

send_message( Request ) ->
	gen_server:cast( ?MODULE, Request ).

%
% system behaviour.
%

start() ->
	{ ok, Pid } = gen_server:start( { local, ?MODULE }, ?MODULE, nodata, [] ),
	Pid.

stop() ->
	gen_server:call( ?MODULE, stop ).

initialize( [ SettingsFile ] ) ->
	Result = file:read_file( SettingsFile ),
	case Result of
		{ ok, Binary } ->
			agency_db:create( Binary );
		{ error, Reason } ->
			log:write( node(), "Failed to read settings due to reason: ~p.", [ Reason ] )
	end.

deinitialize() ->
	agency_db:delete().

%
% gen_server behaviour.
%

init( _Data ) ->
	prepare(),
	log:write( node(), "Started.", [] ),
	agency_db:start(),
	{ MailUser, { _RpcAddress, RpcPort } } = agency_db:get_transport_settings(),
	transport:start( ?MODULE, MailUser, RpcPort ),
	{ ok, listening }.

terminate( Reason, State ) ->
	transport:stop(),
	agency_db:stop(),
	log:write( node(), "Stopped in state ~p due to reason ~p.", [ State, Reason ] ).

% Agency received rpc-call.
handle_call( { rpc, Call }, _From, listening ) ->
	case Call of
		{ request, [ UserId, Request ] } ->
			TaskId = build_task_id( UserId ),
			% cancel any pending or executing task for the current user.
			case is_task_executing( TaskId ) of
				true	->	task:cancel( TaskId );
				false	->	agency_db:drop_task( TaskId )
			end,
			agency_db:save_task_request( TaskId, Request ),
			% enqueue or execute requested task.
			case is_task_executing( TaskId ) orelse not agency_db:is_queue_empty() of
				true	->	agency_db:enqueue_task( TaskId );
				false	->
					agency_db:set_current_task( TaskId ),
					{ ok, _TaskPid } = task:execute( TaskId )
			end;
		{ cancel, [ UserId ] } ->
			TaskId = build_task_id( UserId ),
			case is_task_executing( TaskId ) of
				true	->	task:cancel( TaskId );
				false	->	void
			end
	end,
	{ reply, "ok", listening };
% Stop agency server.
handle_call( stop, _From, listening ) ->
	TaskId = agency_db:get_current_task(),
	case is_task_executing( TaskId ) of
		true	->	kill_task( TaskId );
		false	->	void
	end,
	{ stop, normal, ok, stopped };
% Ignore undefined call-requests.
handle_call( _Request, _From, State ) ->
	{ reply, undefined, State }.

% Agency received mail.
handle_cast( { mail, Message }, listening ) ->
	TaskId = agency_db:get_current_task(),
	case is_task_executing( TaskId ) of
		true	->	task:send_mail( TaskId, Message );
		false	->	ignore
	end,
	{ noreply, listening };
% Task stopped.
handle_cast( { task_stopped, TaskId, TaskResult }, listening ) ->
	agency_db:set_current_task( undefined ),
	% start next pending task, if there is any.
	case not agency_db:is_queue_empty() of
		true ->
			NewTaskId = agency_db:dequeue_task(),
			{ ok, _TaskPid } = task:execute( NewTaskId ),
			agency_db:set_current_task( NewTaskId );
		false -> void
	end,
	UserId = parse_task_id( TaskId ),
	UiRpcServer = agency_db:get_ui_rpc_server(),
	transport:rpc_call( UiRpcServer, { request_processed, [ UserId, TaskResult ] } ),
	agency_db:delete_task_request( TaskId ),
	{ noreply, listening };
% Ignore undefined cast-requests.
handle_cast( _Request, State ) ->
	{ noreply, State }.

% Ignore undefined info-messages.
handle_info( _Info, State ) ->
	{ noreply, State }.

% Ignore undefined code changes.
code_change( _OldVersion, State, _Extra ) ->
	{ ok, State }.

% Helping stuff.

prepare() ->
	process_flag( trap_exit, true ).

%
% Task-wise functions.
%

kill_task( TaskId ) ->
	case whereis( TaskId ) of
		undefined	->	void;
		TaskPid		->	exit( TaskPid, kill )
	end.

is_task_executing( TaskId ) ->
	whereis( TaskId ) =/= undefined.

build_task_id( UserId ) ->
	list_to_atom( UserId ++ "_" ++ atom_to_list( task ) ).

parse_task_id( TaskId ) ->
	S = atom_to_list( TaskId ),
	Index = string:chr( S, $_ ),
	_UserId = string:substr( S, 1, Index - 1 ).
