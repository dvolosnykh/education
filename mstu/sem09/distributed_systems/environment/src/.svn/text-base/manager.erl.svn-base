-module( manager ).
-export( [ start / 1, initialize / 1, deinitialize / 1 ] ).

-behaviour( gen_server ).
-export( [ init / 1, handle_call / 3, handle_cast / 2, handle_info / 2,
	terminate / 2, code_change / 3 ] ).

-include( "settings.hrl" ).

%
% manager interface.
%

start( [ Test, SettingsDir, Ebins ] ) ->
	{ ok, Pid } = gen_server:start_link( { local, ?MODULE }, ?MODULE,
		{ self(), SettingsDir, Ebins }, [] ),
	gen_server:cast( ?MODULE, { run_test, Test } ),
	receive
		{ Pid, stopped } -> stopped
	end.

%
% gen_server behaviour.
%

init( { ParentPid, SettingsDir, Ebins } ) ->
	prepare( ParentPid ),
	log:write( node(), "Started.", [] ),
	manager_db:start(),
	Systems = get_systems_info( SettingsDir ),
	NodesInfo = start_systems_nodes( Systems, Ebins, start ),
	put( nodes_info, NodesInfo ),
	{ _Address, Port } = manager_db:get_transport_settings( node() ),
	transport:start( ?MODULE, false, Port ),
	{ ok, listening }.

terminate( Reason, State ) ->
	transport:stop(),
	stop_systems_nodes( get( nodes_info ), stop ),
	manager_db:stop(),
	get( parent ) ! { self(), stopped },
	log:write( node(), "Stopped in state ~p due to reason ~p.", [ State, Reason ] ).

% Manager received rpc-call.
handle_call( { rpc, Call = { Function, Arguments } }, _From, listening ) ->
	{ request_processed, [ UserId, RequestResult ] } = Call,
	case RequestResult of
		{ succeeded, Reply } ->
			log:write( node(), "Request for user ~p succeeded with result: ~p.", [ UserId, Reply ] );
		failed ->
			log:write( node(), "Request for user ~p failed.", [ UserId ] );
		cancelled ->
			log:write( node(), "Request for user ~p cancelled.", [ UserId ] )
	end,
	gen_server:cast( ?MODULE, stop ),
	{ reply, ok, listening };
% Ignore undefined call-requests.
handle_call( _Request, _From, State ) ->
	{ reply, undefined, State }.

% Run test.
handle_cast( { run_test, Test }, listening ) ->
	file:script( Test ),
	{ noreply, listening };
% Stop.
handle_cast( stop, listening ) ->
	{ stop, normal, stopped };
% User requested task cancellation.
handle_cast( _Request, State ) ->
	{ noreply, State }.

% Ignore undefined info-messages.
handle_info( _Info, State ) ->
	{ noreply, State }.

% Ignore undefined code changes.
code_change( _OldVersion, State, _Extra ) ->
	{ ok, State }.

% Helping stuff.

prepare( ParentPid ) ->
	process_flag( trap_exit, true ),
	put( parent, ParentPid ).

%
% Initialization.
%

initialize( [ SettingsDir, Ebins ] ) ->
	Systems = get_systems_info( SettingsDir ),
	NodesInfo = start_systems_nodes( Systems, Ebins, initialize ),
	manager_db:create( NodesInfo ),
	stop_systems_nodes( NodesInfo, none ).

%
% Deinitialization.
%

deinitialize( [ SettingsDir, Ebins ] ) ->
	Systems = get_systems_info( SettingsDir ),
	NodesInfo = start_systems_nodes( Systems, Ebins, deinitialize ),
	stop_systems_nodes( NodesInfo, none ),
	manager_db:delete().

%
% Helping stuff.
%

start_systems_nodes( Systems, Ebins, Action ) ->
	Arguments = "-pa " ++ Ebins,
	StartNode =
		fun( _System = { Name, Type, File } ) ->
			log:write( node(), "Starting node for system ~p.", [ Name ] ),
			{ ok, Node } = slave:start_link( net_adm:localhost(), Name, Arguments ),
			case Action of
				start ->
					log:write( node(), "Launching system ~p.", [ Name ] ),
					rpc:call( Node, Type, start, [] );
				initialize ->
					log:write( node(), "Initializing system ~p.", [ Name ] ),
					rpc:call( Node, Type, initialize, [ [ File ] ] );
				deinitialize ->
					log:write( node(), "Deinitializing system ~p.", [ Name ] ),
					rpc:call( Node, Type, deinitialize, [] );
				none -> void
			end,
			{ Node, Name, Type, File }
		end,
	NodesInfo = lists:map( StartNode, Systems ).

stop_systems_nodes( NodesInfo, Action ) ->
	StopNode =
		fun( _NodeInfo = { Node, Name, Type, _File } ) ->
			case Action of
				stop ->
					log:write( node(), "Stopping system ~p.", [ Name ] ),
					rpc:call( Node, Type, stop, [] );
				none -> void
			end,
			slave:stop( Node )
		end,
	lists:foreach( StopNode, NodesInfo ).

get_systems_info( SettingsDir ) ->
	Filenames = filelib:wildcard( filename:join( SettingsDir, "*.json" ) ),
	Parse =
		fun( Filename ) ->
			Name = filename:basename( Filename, ".json" ),
			[ Type | _ ] = string:tokens( Name, "_" ),
			{ list_to_atom( Name ), list_to_atom( Type ), Filename }
		end,
	_Systems = lists:map( Parse, Filenames ).
