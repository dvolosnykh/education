%%% File    : json.erl
%%% Author  : Ivan Korotkov <twee@tweedle-dee.org>
%%% Description : JSON parsing (via json_parser) and encoding.
%%% Created : 9 Nov 2009 by Ivan Korotkov <twee@tweedle-dee.org>

-module(json).

-export([parse/1, encode/1, encode_flat/1]).

%%% JSON->Erlang structure mapping
%%%
%%% JSON                               Erlang
%%%
%%% Number ("1", "1.0")                number (1, 1.0)
%%% String ("abc")                     binary (<<"abc">>)
%%% Array ("[1,2,3]")                  list ([1, 2, 3])
%%% Object ({"a": 1, "b": "c"})        tuple ({struct, [{<<"a">>, 1}, {<<"b">>, <<"c">>}]}
%%% Special ("null", "true", "false")  atom (null, true, false)

%%% Front-end to json_parser with DOM interface.

%% @spec parse(Input) -> Output
%% where Input = binary() | list()
%%       Output = term()
parse(List) when is_list(List) -> 
	parse(list_to_binary(List));
parse(Bin) when is_binary(Bin) ->
	{ok, Output, _} = json_parser:event_stream_parser(Bin, ok, fun dom_construct/2),
	Output.

dom_construct(startDocument, _) ->
	start;
dom_construct(startObject, Stack) ->
	[[]| Stack];
dom_construct(startArray, Stack) ->
	[[]| Stack];
dom_construct({key, _} = Event, Stack) ->
	[Event|Stack];
dom_construct({value, Value}, start) ->
	{value, Value};
dom_construct({value, Value}, [{key, Key}, List | T]) ->
	[[{Key, Value} | List] | T];
dom_construct({value, Value}, [List | T]) ->
	[[Value | List] | T];
dom_construct(endObject, [List | T]) ->
	dom_construct({value, {struct, lists:reverse(List)}}, T);
dom_construct(endArray, [List | T]) ->
	dom_construct({value, lists:reverse(List)}, T);
dom_construct(endDocument, {value, R}) ->
	R.

encode_flat(V) -> 
	lists:flatten(encode(V)).

%%% Reverse mapping
%%%
%%% Erlang                                    JSON
%%%
%%% Number (1, 1.0)                           number ("1", "1.0")
%%% Binary (<<"abc">>>)                       string ("abc")
%%% List ([1, 2, 3])                          array ("[1,2,3]")
%%% Tuple ({struct, [{key1, "value1"}, 
%%%                  {<<"some key">>, 2}]}    object ({"key1": "value1", "some key": 2})  
%%% (Keys may be atoms, lists or binaries, translated to strings)
%%% Atom (null, true, false)                  special ("null", "true", "false")

%% @spec encode(Input) -> Output
%% where Input = term()
%%       Output = iolist()
encode(null) -> "null";
encode(true) -> "true";
encode(false) -> "false";
encode({struct, O}) -> encode_object(O);
encode(I) when is_integer(I) -> integer_to_list(I);
encode(F) when is_float(F) -> float_to_list(F);
encode(L) when is_list(L) -> encode_array(L);
encode(L) when is_tuple(L) -> encode_array(L);
encode(B) when is_binary(B) -> encode_string(B).

encode_array([H|T]) -> ["[", encode(H), [[", ", encode(El)] || El <- T], "]"];
encode_array([])    -> "[]".

encode_object([H|T]) -> ["{", encode_kv(H), [[", ", encode_kv(KV)] || KV <- T], "}"];
encode_object([])    -> "{}".

encode_kv({Key, Value}) -> [encode_key(Key), " : ", encode(Value)].

encode_key(Key) when is_list(Key); is_binary(Key) -> encode_string(Key);
encode_key(Key) when is_atom(Key) -> encode_string(atom_to_list(Key)).

encode_string(Bin) when is_binary(Bin) -> encode_string(utf8_decode(Bin));
encode_string(List) -> ["\"", [encode_cp(CP) || CP <- List], "\""].

encode_cp($\") -> "\\\"";
encode_cp($\\) -> "\\\\";
encode_cp($/)  -> "\\/";
encode_cp($\b) -> "\\b";
encode_cp($\f) -> "\\f";
encode_cp($\n) -> "\\n";
encode_cp($\r) -> "\\r";
encode_cp($\t) -> "\\t";
encode_cp(CP) when CP >= 32, CP < 128 -> CP;
encode_cp(CP) -> 
	D4 = CP rem 16 + $0,
	R4 = CP div 16,
	D3 = R4 rem 16 + $0,
	R3 = R4 div 16,
	D2 = R3 rem 16 + $0,
	R2 = R3 div 16,
	D1 = R2 rem 16 + $0,
	[$\\, $u, D1, D2, D3, D4].

utf8_decode(Bin) ->
	binary_to_characters(Bin, []).

binary_to_characters(<<Char/utf8, Rest/binary>>, List) ->
	binary_to_characters(Rest, [Char | List]);
binary_to_characters(<<_Invalid:8, Rest/binary>>, List) ->
	binary_to_characters(Rest, List);
binary_to_characters(<<>>, List) ->
	lists:reverse(List).
