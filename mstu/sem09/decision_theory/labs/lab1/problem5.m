function problem5
    [ M, A, learningSamplesCount, examSamplesCount ] = InitialData;
    learningSample = GenerateSample( M, A, learningSamplesCount );

	Mest = EstimateAverages( learningSample )
	Aest = EstimateCovariation( learningSample, M )

    examSample = GenerateSample( M, A, examSamplesCount );
    for i = 1 : 3
        if ( i == 2 ), mu = Mest; else mu = M; end
        if ( i == 3 ), sigma = Aest; else sigma = A; end;

        result = Classify( mu, sigma, examSample );
        mistakesPercentage = CalculateStats( result )
        pause;
    end
end

function [ M, A, learningSamplesCount, examSamplesCount ] = InitialData
    coordsCount = 5;
    learningSamplesCount = 20;
    examSamplesCount = 1000;
    
    M1 = zeros( 1, coordsCount ); 
    B1 = zeros( coordsCount );
    for i = 1 : coordsCount
        for j = 1 : coordsCount
            B1( i, j ) = 2 / ( 1 + abs( i - j ) );
        end
    end
    A1 = B1 * B1';
    
    M2 = 5 * ones( 1, coordsCount );
    B2 = 2 * eye( coordsCount );
    A2 = B2 * B2';
    
    M( :, :, 1 ) = M1;
    M( :, :, 2 ) = M2;
    A( :, :, 1 ) = A1;
    A( :, :, 2 ) = A2;
end

function sample = GenerateSample( M, A, samplesCount )
    sample( :, :, 1 ) = mvnrnd( M( :, :, 1 ), A( :, :, 1 ), samplesCount );
    sample( :, :, 2 ) = mvnrnd( M( :, :, 2 ), A( :, :, 2 ), samplesCount );
end

function Mest = EstimateAverages( sample )
    [ ~, coordsCount, classCount ] = size( sample );
	Mest = zeros( 1, coordsCount, classCount );
	
	for class = 1 : classCount
        [ Mest( :, :, class ), ~ ] = normfit( sample( :, :, class ) );
	end
end

function Aest = EstimateCovariation( sample, M )
    [ sampleLength, coordsCount, classCount ] = size( sample );
    
    Aest = zeros( coordsCount, coordsCount, classCount );
    
    for class = 1 : classCount
        deviation = zeros( size( sample( :, :, class ) ) );
        for i = 1 : sampleLength
            deviation( i, : ) = sample( i, :, class ) - M( :, :, class );
        end

        for i = 1 : coordsCount
            for j = i : coordsCount
                Aest( i, j, class ) = sum( deviation( :, i ) .* deviation( :, j ) ) / sampleLength;
                Aest( j, i, class ) = Aest( i, j, class );
            end 
        end
        
        Aest( :, :, class )
    end
    
    Aaverage = zeros( coordsCount, coordsCount );
    for class = 1 : classCount
        Aaverage = Aaverage + Aest( :, :, class );
    end
    Aaverage = Aaverage / classCount;
    
    for class = 1 : classCount
        Aest( :, :, class ) = Aaverage;
    end
end

function result = Classify( M, A, sample )
    [ sampleLength, ~, classCount ] = size( sample );
    result = zeros( sampleLength, 1, classCount );
    
    for class = 1 : classCount
        for i = 1 : sampleLength
            result( i, 1, class ) = Discriminator( M, A, sample( i, :, class ) );
        end
    end
    
    plot( 1 : sampleLength, ones( 1, sampleLength ), 'ob' ); hold on
    plot( 1 : sampleLength, result( :, :, 1 ), 'xr' );
    plot( sampleLength + 1: 2 * sampleLength, 2 * ones( 1, sampleLength ), 'ob' );
	plot( sampleLength + 1: 2 * sampleLength, result( :, :, 2 ), 'xr' ); hold off
end

function mistakesPercentage = CalculateStats( result )
    [ sampleLength, ~, classCount ] = size( result );
    
    mistakesCount = zeros( 1, classCount );
    for class = 1 : classCount
        mistakesCount( class ) = sum( result( :, :, class ) ~= class );
    end
    mistakesPercentage = ( mistakesCount ./ sampleLength ) * 100;
    totalPercentage = ( sum( mistakesCount ) / ( classCount * sampleLength ) ) * 100;
    mistakesPercentage = [ mistakesPercentage, totalPercentage ];
end

function class = Discriminator( M, A, x )
    M1 = M( :, :, 1 );
    A1 = A( :, :, 1 );
    M2 = M( :, :, 2 );
    A2 = A( :, :, 2 );
    
    value = log( det( A1 ) / det( A2 ) ) + ( x - M1 ) / A1 * ( x - M1 )' - ( x - M2 )/ A2 * ( x - M2 )';
    if ( value > 0 )
        class = 2;
    else
        class = 1;
    end
end