function lab2
    visualize = true;
    [ transactions, r ] = InitialData;
    [ ownership, profit ] = CLOPE( transactions, r, visualize );
    disp( '================================' );
    ownership
    profit
end

function [ ownership, profit ] = CLOPE( transactions, r, visualize )
    clusters = struct( 'Area', {}, 'Width', {}, 'Items', {}, 'Occurence', {}, 'TransactionsCount', {} );
    ownership = Inf * ones( size( transactions ) );
    profit = 0.0;
    while ( true )
        [ newOwnership, clusters, profit ] = MaximizeProfit( clusters, ownership, profit, transactions, r );
        
    if ( newOwnership == ownership ), break; end
    
        ownership = newOwnership;
        
        if ( visualize )
            Visualize( ownership, profit, clusters );
        end
    end
end

function [ ownership, clusters, profit ] = MaximizeProfit( clusters, ownership, profit, transactions, r )
    for transactionIndex = 1 : length( transactions )
        % find the best target cluster.
        % find profit increase when adding to a new cluster.
        newCluster = struct( 'Area', 0, 'Width', 0, 'Items', [], 'Occurence', [], 'TransactionsCount', 0 );
        maxProfitDelta = ProfitDeltaOnAdding( newCluster( 1 ), transactions( transactionIndex ), r );
        targetClusterIndex = length( clusters ) + 1;
        % find profit change when adding to one of the existing clusters.
        sourceClusterIndex = ownership( transactionIndex );
        for clusterIndex = 1 : length( clusters )
            if ( clusterIndex ~= sourceClusterIndex )
                profitDelta = ProfitDeltaOnAdding( clusters( clusterIndex ), transactions( transactionIndex ), r );
            else
                profitDelta = ProfitDeltaOnRemoving( clusters( clusterIndex ), transactions( transactionIndex ), r );
            end

            if ( profitDelta > maxProfitDelta )
                targetClusterIndex = clusterIndex;
                maxProfitDelta = profitDelta;
            end
        end
        % if target cluster is a new one, add it to the partition.
        if ( targetClusterIndex > length( clusters ) )
            clusters = [ clusters, newCluster ];
        end

        if ( targetClusterIndex ~= sourceClusterIndex )
            if ( sourceClusterIndex ~= Inf )
                clusters( sourceClusterIndex ) = RemoveTransactionFromCluster( clusters( sourceClusterIndex ), transactions( transactionIndex ) );
                if ( clusters( sourceClusterIndex ).TransactionsCount == 0 ) % remove cluster if it became empty.
                    clusters = clusters( [ 1 : sourceClusterIndex - 1, sourceClusterIndex + 1 : length( clusters ) ] );
                    
                    % refine cluster indices;
                    mask = ( ownership > sourceClusterIndex );
                    ownership( mask ) = ownership( mask ) - 1;
                    if ( targetClusterIndex > sourceClusterIndex )
                        targetClusterIndex = targetClusterIndex - 1;
                    end
                end
            end
            
            clusters( targetClusterIndex ) = AddTransactionToCluster( clusters( targetClusterIndex ), transactions( transactionIndex ) );
            ownership( transactionIndex ) = targetClusterIndex;
            profit = profit + maxProfitDelta;
        end
    end
end

function delta = ProfitDeltaOnAdding( cluster, transaction, r )
    newArea = cluster.Area + length( transaction.Items );
    newWidth = length( unique( [ cluster.Items, transaction.Items ] ) );

    if ( cluster.TransactionsCount > 0 )
        delta = newArea * ( cluster.TransactionsCount + 1 ) / ( newWidth ^ r ) - ...
            cluster.Area * cluster.TransactionsCount / ( cluster.Width ^ r );
    else
        delta = newArea * ( cluster.TransactionsCount + 1 ) / ( newWidth ^ r );
    end
end

function delta = ProfitDeltaOnRemoving( cluster, transaction, r )
    if ( cluster.TransactionsCount > 1 )
        newArea = cluster.Area - length( transaction.Items );
        mask = ( ismember( cluster.Items, transaction.Items ) & cluster.Occurence == 1 );
        newWidth = cluster.Width - sum( mask );

        delta = cluster.Area * cluster.TransactionsCount / ( cluster.Width ^ r ) - ...
            newArea * ( cluster.TransactionsCount - 1 ) / ( newWidth ^ r );
    else
        delta = cluster.Area * cluster.TransactionsCount / ( cluster.Width ^ r );
    end
end

function cluster = AddTransactionToCluster( cluster, transaction )
    cluster.TransactionsCount = cluster.TransactionsCount + 1;
    cluster.Area = cluster.Area + length( transaction.Items );
    cluster.Items = unique( [ cluster.Items, transaction.Items ], 'first' );
    mask = ismember( cluster.Items, transaction.Items );
    cluster.Occurence = [ cluster.Occurence, zeros( 1, length( cluster.Items ) - cluster.Width ) ];
    cluster.Occurence( mask ) = cluster.Occurence( mask ) + 1;
    cluster.Width = length( cluster.Items );
end

function cluster = RemoveTransactionFromCluster( cluster, transaction )
    cluster.TransactionsCount = cluster.TransactionsCount - 1;
    cluster.Area = cluster.Area - length( transaction.Items );
    mask = ismember( cluster.Items, transaction.Items );
    cluster.Occurence( mask ) = cluster.Occurence( mask ) - 1;
    mask = ( cluster.Occurence ~= 0 );
    cluster.Items = cluster.Items( mask );
    cluster.Occurence = cluster.Occurence( mask );
    cluster.Width = sum( mask );
end

function [ transactions, r ] = InitialData
    transactionMaxLength = 3;
    categoriesCount = 3;
    transactionsCount = 10;
    r = 2;
    %{
    transactions = struct( [] );
    transactions( 1 ).Items = 'ab';
    transactions( 2 ).Items = 'abc';
    transactions( 3 ).Items = 'acd';
    transactions( 4 ).Items = 'de';
    transactions( 5 ).Items = 'def';
    %}
    %%{
    for i = 1 : transactionsCount
        transactionLength = randi( transactionMaxLength );
        transactions( i ).Items = char( 'a' - 1 + unique( randi( categoriesCount, 1, transactionLength ) ) );
        disp( transactions( i ).Items );
    end
    %%}
end

function Visualize( ownership, profit, clusters )
    fprintf( '----------------------------------------------\n' );
    fprintf( '%s\t%s\n', num2str( ownership ), num2str( profit ) );
    
    x = 0;
    for cluster = clusters
        for i = 1 : length( cluster.Items )
            h = cluster.Occurence( i );
            X = repmat( [ x + i, x + i + 1, x + i + 1, x + i ], 1, h );
            Y = repmat( [ 0, 0, 1, 1 ], 1, h ) + reshape( repmat( ( 0 : h - 1 ), 4, 1 ), 1, [] );
            fill( X, Y, 'b' ); hold on
        end
        x = x + length( cluster.Items ) + 1;
    end
    hold off
    pause;
end