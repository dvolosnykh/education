function lab3
    [ beings, survivalRatio, mutationProbability, generationsCount ] = InitialData;
    lively = Genetic( beings, survivalRatio, mutationProbability, generationsCount )
end

function lively = Genetic( beings, survivalRatio, mutationProbability, generationsCount )
    generationNum = 1;
    while ( generationNum <= generationsCount )
        disp( '---------------------------------------------------------------' );
        disp( beings );
        pause;
        
        fitness = Fitness( beings );
        distribution = cumsum( fitness / sum( fitness ) );
        
        % detect deads.
        [ ~, sortOrder ] = sort( fitness );
        deadCount = round( ( 1 - survivalRatio ) * size( beings, 2 ) );
        deadIndices = sortOrder( 1 : deadCount );
        
        % crossover.
        nextGeneration = beings;
        for deadIndex = deadIndices
            parentsIndices = [ find( rand( 1 ) < distribution, 1 ), find( rand( 1 ) < distribution, 1 ) ];
            nextGeneration( :, deadIndex ) = Crossover( beings( :, parentsIndices ) );
        end
        beings = nextGeneration;
        
        % mutation.
        for i = 1 : length( beings )
            beings( :, i ) = Mutation( beings( :, i ), mutationProbability );
        end
        
        generationNum = generationNum + 1;
    end
    
    fitness = Fitness( beings );
    [ ~, livelies ] = max( fitness );
    lively = beings( :, livelies( 1 ) );
end

function child = Crossover( parents )
    splitIndex = randi( size( parents, 1 ) - 1 );
    motherIndex = randi( 2 );
    fatherIndex = 3 - motherIndex;
    child = [ parents( 1 : splitIndex, motherIndex ); parents( splitIndex + 1 : end, fatherIndex ) ];
end

function being = Mutation( being, probability )
    mutation = binornd( ones( size( being, 1 ), 1 ), probability );
    mutation( mutation ~= 0 ) = 2 * randi( 1, [ sum( mutation ), 1 ] ) - 1;
    being = being + mutation;
end

function fitness = Fitness( beings )
    lengths = sqrt( sum( beings .^ 2 ) );
    lengths = lengths / max( lengths );
    fitness = exp( - lengths );
end

function [ beings, survivalRatio, mutationProbability, generationsCount ] = InitialData
    beingsCount = 20;
    genesCount = 2;
    survivalRatio = 0.5;
    mutationProbability = 0.2;
    generationsCount = 100;

    beings = randi( 500, [ genesCount, beingsCount ] );
end