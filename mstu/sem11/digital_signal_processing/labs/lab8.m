function lab8
    dt = 0.05;
    dk = 5;
    epsilon_r = 2;
    epsilon_v = 0.05;

    A = 2;
    T = 2;

    t = [-2 * T : dt : 2 * T];

    % Gauss signal.
    mean = 0;
    deviation = 0.5 * T;
    original = gaussSignal(t, mean, deviation, A);

    % Uniform noise.
    jumpsCount = 6;
    a = -0.25 * T;
    b = 0.25 * T;
    noise = uniformNoise(t, a, b, jumpsCount);

    deformed = original + noise;

    show(t, deformed, dk, @smooth_v, epsilon_v, @mean, 'Ev', 'MEAN', 1, 1);
    show(t, deformed, dk, @smooth_v, epsilon_v, @median, 'Ev', 'MEDIAN', 1, 2);
    show(t, deformed, dk, @smooth_r, epsilon_r, @mean, 'Er', 'MEAN', 2, 1);
    show(t, deformed, dk, @smooth_r, epsilon_r, @median, 'Er', 'MEDIAN', 2, 2);
end

function show(t, deformed, dk, smooth, epsilon, smoothFunc, methodName, averageName, row, column)
    smoothed = smooth(deformed, dk, epsilon, smoothFunc);

    subplot(2, 2, (row - 1) * 2 + column);
    plot(t, deformed, 'b', t, smoothed, 'r');
    grid on;
    legend('deformed', 'smoothed');
    title([methodName, ' (SMTH = ', averageName, ')']);
end

function smoothed = smooth_v(deformed, dk, epsilon_v, smoothFunc)
    smoothed = zeros(size(deformed));
    for k = 1 : length(deformed);
        S = false(size(deformed));
        lower = max(k - dk, 1);
        upper = min(k + dk, length(deformed));
        S(lower : upper) = true;

        if (noiseDetected_v(deformed(k), epsilon_v, smoothFunc(deformed(S))))
            S(k) = false;
            smoothed(k) = smoothFunc(deformed(S));
        else
            smoothed(k) = deformed(k);
        end
    end

    function decision = noiseDetected_v(value, epsilon_v, average)
        decision = (abs(value - average) > epsilon_v);
    end
end

function smoothed = smooth_r(deformed, dk, epsilon_r, smoothFunc)
    smoothed = zeros(size(deformed));
    for k = 1 : length(deformed);
        S = false(size(deformed));
        lower = max(k - dk, 1);
        upper = min(k + dk, length(deformed));
        S(lower : upper) = true;

        if (noiseDetected_r(deformed(S), k - lower + 1, epsilon_r))
            S(k) = false;
            smoothed(k) = smoothFunc(deformed(S));
        else
            smoothed(k) = deformed(k);
        end
    end

    function decision = noiseDetected_r(deformed, k, epsilon_r)
        [dummy, ranked] = sort(deformed);
        rank = find(ranked == k);
        decision = (abs(rank - (length(ranked) + 1) / 2) > epsilon_r);
    end
end
