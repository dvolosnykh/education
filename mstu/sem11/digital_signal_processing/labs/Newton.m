function [ xRoot, yRoot, iterationNum, callsCount, xEval ] = Newton( f, eps, xCur )
    iterationsMax = 100;
    
    iterationNum = 0;
    yCur = feval( f, xCur );
    callsCount = 1;
    xEval = xCur;
    h = eps * 0.1;
    while ( true )
        iterationNum = iterationNum + 1;
        
        y = feval( f, xCur + h );
        callsCount = callsCount + 1;
        xEval = [ xEval, xCur + h ];
        
	df = (y - yCur) / h;
        if ( df == 0 )
            %disp( 'Infinite Newton step!' );
            break;
        end
        step = -yCur / df;
        while ( true )
            xNext = xCur + step;
            yNext = feval( f, xNext );
            callsCount = callsCount + 1;
            xEval = [ xEval, xNext ];
            
        if ( abs(yNext) < abs(yCur) || abs( step ) < eps ), break; end
        
            step = step / 2;
        end

        xCur = xNext;
        yCur = yNext;
        
    if ( abs( step ) < eps || iterationNum > iterationsMax ), break; end
    end
    
    if ( iterationNum > iterationsMax )
        disp( 'Iterations limit reached!' );
    end
    
    xRoot = xCur;
    yRoot = yCur;
end
