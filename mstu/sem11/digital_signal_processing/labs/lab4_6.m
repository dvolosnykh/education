function lab4_6
    dt = 0.05;
    cut = 0.4;
    order = 2;
    
    A = 2;
    T = 2;
    
    t = [-2 * T : dt : 2 * T];


    % Gauss signal.
    mean = 0;
    deviation = 0.5 * T;
    u = gaussSignal(t, mean, deviation, A);

    % Uniform noise.
    jumpsCount = 6;
    a = -0.25 * T;
    b = 0.25 * T;
    e = uniformNoise(t, a, b, jumpsCount);
    show(t, u + e, e, 'Uniform', 1, cut, order);

    % Gauss noise.
    noise_m = 0 * T;
    noise_d = 0.05 * T;
    e = gaussNoise(t, noise_m, noise_d);
    show(t, u + e, e, 'Gauss', 2, cut, order);
end

function show(t, u, e, noiseName, column, cut, order)
    % Butterworth IIR.
    dt = t(2) - t(1);
    Fmax = 1 / dt;
    Fnyq = Fmax / 2;
    Fcut = cut * Fnyq;

    [b, a] = butter(2 * order, Fcut / (pi * Fnyq), 'high');
    fu_high = filtfilt(b, a, u);
    [b, a] = butter(2 * order, Fcut / (pi * Fnyq), 'low');
    fu_low = filtfilt(b, a, u);
    
    subplot(3, 2, column);
    plot(t, u, 'b', t, fu_low, 'r', t, fu_high, 'm');
    legend('Original', 'Low Freq', 'High Freq');
    title([noiseName, ' noise (Butterworth IIR)']);
    grid on;

    % Butterworth FIR.
    fu_high = butterworth_filter(u, Fcut, order, 'high');
    fu_low = butterworth_filter(u, Fcut, order, 'low');

    subplot(3, 2, column + 2);
    plot(t, u, 'b', t, fu_low, 'r', t, fu_high, 'm');
    legend('Original', 'Low Freq', 'High Freq');
    title([noiseName, ' noise (Butterworth FIR)']);
    grid on;

    % Wiener.
    fu_high = wiener_filter(u, e, 'high');
    fu_low = wiener_filter(u, e, 'low');

    subplot(3, 2, column + 4);
    plot(t, u, 'b', t, fu_low, 'r', t, fu_high, 'm');
    legend('Original', 'Low Freq', 'High Freq');
    title([noiseName, ' noise (Wiener)']);
    grid on;
end

function u = butterworth_filter(u, Fcut, order, type)
    ft_u = fft(u);

    if strcmp(type, 'high')
        r = ft_u ./ Fcut;
    elseif strcmp(type, 'low')
        r = Fcut ./ ft_u;
    end

    H = sqrt(abs(1 ./ (1 + r .^ (2 * order))));
    u = ifft(ft_u .* H);
end

function u = wiener_filter(u, e, type)
    if strcmp(type, 'high')
       e = u - e;
    end

    ft_u = fft(u);
    ft_e = fft(e);

    H = 1 - (ft_e ./ ft_u) .^ 2;
    u = ifft(ft_u .* H);
end
