function lab2
    dt = 0.05;
    
    A = 2;
    T = 2;
    
    t = [-2 * T : dt : 2 * T];

    % Rectangular signal.
    u = rectangularSignal(t, -T, T, A);
    show(t, u, 'Rectangular', 1);
    
    % Gauss signal.
    mean = 0;
    deviation = 0.5 * T;
    u = gaussSignal(t, mean, deviation, A);
    show(t, u, 'Gauss', 2);
end

function show(t, u, signalName, column)
    N = length(u);
    
    % Original.
    subplot(3, 2, column);
    plot(t, u);
    title([signalName, ' signal']);
    
    % DFT.
    tic;
    ud = dft(u);
    dft_time = toc;
    ud2 = dft(u .* ((-1) .^ [0 : N - 1]));
    subplot(3, 2, column + 2);
    plot(t, abs(ud), 'g', t, abs(ud2), 'r');
    title(sprintf('DFT of %s signal (%f sec.)', signalName, dft_time));
    
    % FFT.
    tic;
    uf = fft(u);
    fft_time = toc;
    uf2 = fft(u .* ((-1) .^ [0 : N - 1]));
    subplot(3, 2, column + 4);
    plot(t, abs(uf), 'g', t, abs(uf2), 'r');
    title(sprintf('FFT of %s signal (%f sec.)', signalName, fft_time));
end

function u = dft(u)
    N = length(u);
    indices = [0 : N - 1];
    prod_matrix = repmat(indices, N, 1) .* repmat(indices', 1, N);
    u = sum(repmat(u', 1, N) .* exp((2 * pi * i / N) * prod_matrix));
end
