function noise = uniformNoise(t, jumps_a, jumps_b, jumpsCount)
    N = length(t);

    jumpsIndices = round(N * rand(1, jumpsCount));
    jumpsIndices(jumpsIndices < 1) = 1;
    jumpsIndices(jumpsIndices > N) = N;

    noise = zeros(1, N);
    noise(jumpsIndices) = jumps_a + (jumps_b - jumps_a) * rand(1, jumpsCount);
end
