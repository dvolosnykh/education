function noise = gaussNoise(t, m, d)
    noise = m + d * randn(size(t));
end
