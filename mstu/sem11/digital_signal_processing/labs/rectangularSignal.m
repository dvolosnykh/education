function u = rectangularSignal(t, a, b, A)
    u = A * ((a <= t) & (t <= b));
end
