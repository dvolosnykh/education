function lab3
    dt = 0.5;
    
    A = 2;
    T = 2;
    
    t = [-2 * T : dt : 2 * T];

    % Rectangular signal.
    u = rectangularSignal(t, -T, T, A);
    show(t, u, 'Rectangular', 1);
    
    % Gauss signal.
    mean = 0;
    deviation = 0.5 * T;
    u = gaussSignal(t, mean, deviation, A);
    show(t, u, 'Gauss', 2);
end

function show(t, u, signalName, column)
    N = length(u);
    
    % Original.
    subplot(2, 2, column);
    plot(t, u);
    title([signalName, ' signal']);
    
    % Convolution.
    uc = convolution(u, u);
    subplot(2, 2, column + 2);
    plot(abs(uc), 'r');
    title(sprintf('Convolution of %s signal', signalName));
end

function u = convolution(u, H)
    u = fft([u, zeros(size(u))]);
    H = fft([H, zeros(size(H))]);
    u = ifft(u .* H);
end
