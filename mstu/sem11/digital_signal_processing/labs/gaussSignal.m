function u = gaussSignal(t, m, d, A)
    u = A * exp(- (t - m) .^ 2 / d ^ 2);
end
