function lab1
    dts = 0.2;
    
    dtr = 0.01;
    A = 2;
    T = 2;
    
    ts = [-2 * T : dts : 2 * T];
    tr = [-2 * T : dtr : 2 * T];
    
    % Rectangular signal.
    u = rectangularSignal(ts, -T, T, A);
    uf = rectangularSignal(tr, -T, T, A);
    subplot(2, 1, 1);
    show(tr, uf, ts, u, 'Rectangular');
    
    % Gauss signal.
    mean = 0;
    deviation = 0.05 * T;
    u = gaussSignal(ts, mean, deviation, A);
    uf = gaussSignal(tr, mean, deviation, A);
    subplot(2, 1, 2);
    show(tr, uf, ts, u, 'Gauss');
end

function show(tr, uf, ts, u, signalName)
    ur = restore(ts, u, tr);
    plot(tr, uf, 'k', ts, u, '*b', tr, ur, 'r');
    title([signalName, ' signal reconstruction']);
    legend('original', 'sampled', 'restored');
end

function u = restore(ts, u, tr)
    delta = ts(2) - ts(1);
    t_diff = repmat(tr, length(ts), 1) - repmat(ts', 1, length(tr));
    u = sum(repmat(u', 1, length(tr)) .* sinc(t_diff / delta));
end

