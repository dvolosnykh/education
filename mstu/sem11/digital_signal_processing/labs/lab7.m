function lab7
	dx = 0.1;
	A = 3;
	delta = 0.05;
	epsilon = 0.06;
	alphaPrecision = 10 ^ -6;

	x = [-A : dx : A];

	u1 = initialSignal(x, delta);
	u2 = deformedSignal(x, epsilon);

	u1ft = fft(u1);
	u2ft = fft(u2);

	rhoFunc = @(alpha)(evaluateRho(x, abs(u1ft), delta, abs(u2ft), epsilon, alpha));
	alpha = evaluateAlpha(rhoFunc, length(x) - 1, alphaPrecision);
	H = transferFunction(x, u1ft, u2ft, alpha);
	u = ifft(u2ft .* H');

	subplot(2, 2, 2);
	plot(x, u1, 'b', x, u2, 'g', x, u, 'r');
	title('Signals');
	legend('Original', 'Deformed');
	grid on;

	subplot(2, 2, 4);
	plot(x, H, 'r');
	title('Transfer function');
	grid on;

	subplot(2, 2, 3);
	plot(x, abs(fft(u1 .* ((-1) .^ [0 : length(u1) - 1]))), 'm');
	title('FT of original signal');
	grid on;
end

function H = transferFunction(x, u1ft, u2ft, alpha)
	dx = x(2) - x(1);
	T = x(end) - x(1);
	N = length(x) - 1;

	km = [0 : N]' * [0 : N];
	numerator = exp(2 * pi * i * km / N) .* repmat(conj(u2ft) .* u2ft, N + 1, 1);
	H = (dx / N) * sum(numerator ./ repmat(denominator(abs(u2ft), dx, T, N, alpha), N + 1, 1), 2);
end

function alphaRoot = evaluateAlpha(rhoFunc, N, precision)
	alpha0 = 1.0;
	[alphaRoot, rhoRoot, dummy, dummy, dummy] = Newton(rhoFunc, precision, alpha0);

	alphaMax = N / 50;
	da = alphaMax / 100;
	alpha = [0.01 : da : alphaMax]';
	rho = rhoFunc(alpha);

	subplot(2, 2, 1);
	plot(alpha, rho, 'b', alphaRoot, rhoRoot, 'ro');
	title('Rho(alpha)');
	legend('Rho', 'alpha_0', 'location', 'northwest');
	grid on;
end

function rho = evaluateRho(x, u1ft, delta, u2ft, epsilon, alpha)
	if (size(alpha, 2) > 1)
		alpha = alpha';
	end

	betta = evaluateBetta(x, u1ft, u2ft, alpha);
	gamma = evaluateGamma(x, u1ft, u2ft, alpha);
	rho = betta - (delta + epsilon * sqrt(gamma)) .^ 2;

	function betta = evaluateBetta(x, u1ft, u2ft, alpha)
		dx = x(2) - x(1);
		T = x(end) - x(1);
		N = length(x) - 1;

		numerator = ((alpha * u1ft) .^ 2) .* repmat(helper_pi_m_T(N, T), size(alpha));
		betta = (dx / N) * sum(numerator ./ denominator(u2ft, dx, T, N, alpha) .^ 2, 2);
	end

	function gamma = evaluateGamma(x, u1ft, u2ft, alpha)
		dx = x(2) - x(1);
		T = x(end) - x(1);
		N = length(x) - 1;

		numerator = repmat((u1ft .* u2ft) .^ 2 .* helper_pi_m_T(N, T), size(alpha));
		gamma = (dx / N) * sum(numerator ./ denominator(u2ft, dx, T, N, alpha) .^ 2, 2);
	end
end

function d = denominator(u2ft, dx, T, N, alpha)
	d = repmat((u2ft * dx) .^ 2, size(alpha)) + alpha * helper_pi_m_T(N, T);
end

function t = helper_pi_m_T(N, T)
	m = [0 : N];
	t = 1 + (2 * pi * m / T) .^ 2;
end

function u = initialSignal(x, delta)
	e = noise(delta, size(x));
	u = exp(- (x .^ 2) / 2) + e;
end

function u = deformedSignal(x, epsilon)
	e = noise(epsilon, size(x));
	u = exp(- (x .^ 2)) + e;

function e = noise(m, dim)
	e = -m + rand(dim) * (2 * m);
end
