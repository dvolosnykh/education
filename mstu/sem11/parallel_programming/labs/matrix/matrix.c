#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include <time.h>
#include <assert.h>


enum node_t {
	SENDER = 0,
	RECEIVER = 1
};

#define TAG_MATRIX 123
#define SIZE 10
#define UPPER_LIMIT 100
#define MAX_STR_LEN 255


void generate_matrix(int a[][SIZE]) {
	int i;
	for (i = 0; i < SIZE; ++i) {
		int j;
		for (j = 0; j < SIZE; ++j) {
			a[i][j] = rand() % UPPER_LIMIT;
		}
	}
}

void print_matrix(int a[][SIZE], const int rank) {
	char buffer[(SIZE + 1) * (MAX_STR_LEN + 1)];

	int count = 0;
	int row;
	count += sprintf(buffer, "rank = %d:\n", rank);
	for (row = 0; row < SIZE; ++row) {
		int column;
		for (column = 0; column < SIZE; ++column) {
			count += sprintf(buffer + count, "%4d", a[row][column]);
		}
		count += sprintf(buffer + count, "\n");
	}
	printf(buffer);
}

void send_matrix(const MPI_Datatype matrix_t) {
	int a[SIZE][SIZE];
	generate_matrix(a);
	print_matrix(a, SENDER);
	MPI_Send(a, 1, matrix_t, RECEIVER, TAG_MATRIX, MPI_COMM_WORLD);
}

void receive_matrix() {
	int a[SIZE][SIZE];
	MPI_Status status;
	MPI_Recv(a, SIZE * SIZE, MPI_INT, SENDER, TAG_MATRIX, MPI_COMM_WORLD, &status);
	print_matrix(a, RECEIVER);
}

void transpose_matrix(const int rank) {
	switch (rank) {
	case SENDER:
	{
		MPI_Datatype column_t, matrix_t;
		MPI_Type_vector(SIZE, 1, SIZE, MPI_INT, &column_t);
		MPI_Type_hvector(SIZE, 1, sizeof(int), column_t, &matrix_t);
		MPI_Type_commit(&column_t);
		MPI_Type_commit(&matrix_t);

		send_matrix(matrix_t);

		MPI_Type_free(&matrix_t);
		MPI_Type_free(&column_t);
	}	break;
	case RECEIVER:
		receive_matrix();
		break;
	default:
		assert(0);
	}
}

int main(int argc, char** argv) {
	srand(time(NULL));

	MPI_Init(&argc, &argv);

	int size, rank;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	transpose_matrix(rank);

	MPI_Finalize();

	return (EXIT_SUCCESS);
}
