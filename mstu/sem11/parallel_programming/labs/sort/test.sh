#!/bin/bash

if [[ $# -ne 1 ]]; then 
        echo "[ERROR] Name of the job is not specified." 
        exit 1 
fi 

name=$1

if ! mpicc -lm -fopenmp $name.c -o $name; then exit 1; fi

rm stdout.* &>/dev/null
rm -rf data &>/dev/null; mkdir data
llcancel -u $(whoami) &>/dev/null

for nodes in {1..8}; do
	for length in 10000 100000 1000000 10000000; do
		sed -i "s/# @ node = [0-9][0-9]*/# @ node = $nodes/g" $name.job
		sed -i "s/length=[0-9][0-9]*/length=$length/g" $name.job

		while true; do
			while true; do
				echo "Submitting with length=${length} to $nodes nodes."
				task=$(llsubmit $name.job | sed -n 's/.*"mgmt\.nodes\.\([0-9][0-9]*\)".*/\1/p')
				echo "Task $task"

			if ./wait.sh && ! grep -iq failed stdout.$task; then break; fi

				llcancel -u $(whoami) $task &>/dev/null
				rm stdout.$task stderr.$task &>/dev/null
			done

		if [[ -f stderr.$task ]]; then exit 1; fi

		if ! grep -iq killed stdout.$task && ./parse.awk stdout.$task >>data/$length.data; then break; fi

			rm stdout.$task &>/dev/null
		done
	done
done
