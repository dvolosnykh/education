#!/bin/bash

if [[ $# -lt 1 ]]; then
	echo "[ERROR] Name of the job is not specified."
	exit 1
fi

name=$1 cleanup=${2:-true}

if [[ $cleanup == true ]]; then
	rm std*
fi

mpicc $name.c -o $name && \
	llsubmit $name.job
