#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <memory.h>
#include <string.h>
#include <assert.h>

//#define DEBUG
//#define DEBUG_STEP
//#define DEBUG_ARRAY
//#define DEBUG_STOPPED
//#define DEBUG_CALC
//#define DEBUG_BANDWIDTH
//#define DEBUG_SEQ_SORT

#define DEFAULT_LENGTH      100000000

#define UPPER_LIMIT 100
#define FIELD_WIDTH 4

enum tags {TAG_INITIAL, TAG_EXCHANGE, TAG_PASS};

enum nodes {HOST = 0, ROOT = 1};


//
// Simple math functions.
//

int min(const int a, const int b) {
    return (a < b ? a : b);
}

int max(const int a, const int b) {
    return (a > b ? a : b);
}

int ceil_pow2(const int n) {
    if (n == 0) return (-1);

    int p = 0;
    while ((1 << p) < n)
        ++p;
    return (p);
}

int floor_pow2(const int n) {
    int p = 0;
    while ((1 << p) <= n)
        ++p;
    return (--p);
}


//
// Tree's properties.
//

#define HEIGHT(size) ceil_pow2(size)
#define DEPTH(rank) ((rank) != HOST ? floor_pow2(rank) : 0)
#define IS_ODD(n) ((n) & 0x1)
#define IS_MAIN_CHILD(rank) !IS_ODD(rank)
#define TEAMMATE(rank) ((rank) + 1 - (IS_ODD(rank) << 1))
#define MAIN_CHILD(rank) ((rank) << 1)
#define PARENT(rank) ((rank) >> 1)

#define LEVEL_BASE(level) (1 << (level))
#define LEVEL_INDEX(rank, level) ((rank) - LEVEL_BASE(level))

#define LEVEL_NODES_COUNT_COMPLETE(depth, level) (1 << (level) - (depth))
#define FIRST_LEVEL_NODE(rank, depth, level, height) \
    (LEVEL_BASE(level) + LEVEL_NODES_COUNT_COMPLETE(depth, level) * LEVEL_INDEX(rank, depth))

#define FULL_BLOCK_DEFAULT_LENGTH(length, size) (((length) - 1) / (size) + 1)
#define TOTAL_FULL_COUNT(length, size) (((length) - 1) % (size) + 1)

int LEVEL_NODES_COUNT(const int first_node, const int nodes_count_complete,
                      const int nodes_count) {
    return min(max(0, nodes_count - first_node), nodes_count_complete);
}

int LEVEL_BANDWIDTH(const int rank, const int size, const int depth,
                    const int level, const int height,
                    const unsigned total_full_count, const unsigned full_block_length) {
    const int first_level_node = FIRST_LEVEL_NODE(rank, depth, level, height);
    const int level_nodes_count_complete = LEVEL_NODES_COUNT_COMPLETE(depth, level);
    const int full_level_nodes_count = LEVEL_NODES_COUNT(first_level_node, level_nodes_count_complete, total_full_count);
    const int level_nodes_count = LEVEL_NODES_COUNT(first_level_node, level_nodes_count_complete, size);
    return  (level_nodes_count * (full_block_length - 1) + full_level_nodes_count);
}

int BANDWIDTH(const int rank, const int size, const int depth, const int height,
              const int total_full_count, const int full_block_length,
              const int length) {
    const int teammate = TEAMMATE(rank);

    const int hack = 1;
    if (!hack) {
        const int self_bottom_nodes_bandwidth = LEVEL_BANDWIDTH(rank, size, depth, height - 1, height, total_full_count, full_block_length);
        const int teammate_bottom_nodes_bandwidth =
            (teammate != HOST ?
                LEVEL_BANDWIDTH(teammate, size, depth, height - 1, height, total_full_count, full_block_length) :
                length - LEVEL_BANDWIDTH(rank, size, depth, height - 1, height, total_full_count, full_block_length));
        const int bottom_nodes_bandwidth = self_bottom_nodes_bandwidth + teammate_bottom_nodes_bandwidth;

        const int self_pre_bottom_nodes_bandwidth = LEVEL_BANDWIDTH(rank, size, depth, height - 2, height, total_full_count, full_block_length);
        const int teammate_pre_bottom_nodes_bandwidth =
            (teammate != HOST ?
                LEVEL_BANDWIDTH(teammate, size, depth, height - 2, height, total_full_count, full_block_length) :
                teammate_bottom_nodes_bandwidth - LEVEL_BANDWIDTH(rank, size, depth, height - 2, height, total_full_count, full_block_length));
        const int pre_bottom_nodes_bandwidth = self_pre_bottom_nodes_bandwidth + teammate_pre_bottom_nodes_bandwidth;

        return max(pre_bottom_nodes_bandwidth, bottom_nodes_bandwidth);
    } else {
        const int bottom_nodes_bandwidth =
            LEVEL_BANDWIDTH(rank, size, depth, height - 1, height, total_full_count, full_block_length) +
                LEVEL_BANDWIDTH(teammate, size, depth, height - 1, height, total_full_count, full_block_length);
        const int pre_bottom_nodes_bandwidth =
            LEVEL_BANDWIDTH(rank, size, depth, height - 2, height, total_full_count, full_block_length) +
                LEVEL_BANDWIDTH(teammate, size, depth, height - 2, height, total_full_count, full_block_length);

        return max(pre_bottom_nodes_bandwidth, bottom_nodes_bandwidth);
    }
}


//
// Basic functions.
//

void generate_array(int a[], const unsigned length) {
    unsigned i;
    for (i = 0; i < length; ++i) {
        a[i] = rand() % UPPER_LIMIT;
    }
}

void print_array(const char prefix[], const int a[], const unsigned length) {
    char buffer[length * (FIELD_WIDTH + 1) + strlen(prefix) + 100];

    int offset = 0;
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    offset += sprintf(buffer + offset, "rank:%4d --- ", rank);
    offset += sprintf(buffer + offset, "%s(%u):\t", prefix, length);
    unsigned i;
    for (i = 0; i < length; ++i) {
        offset += sprintf(buffer + offset, "%4d", a[i]);
        if ((i + 1) % 20 == 0) {
            offset += sprintf(buffer + offset, "\n");
        }
    }
    offset += sprintf(buffer + offset, "\n");
    puts(buffer);
}

unsigned validate_order(const int a[], const unsigned length,
                      int (*comparator)(const void*, const void*)) {
    unsigned i = 0;
    while (i + 1 < length && comparator(a + i, a + i + 1) <= 0)
        ++i;
    return (i);
}

int order(const void* const left, const void* const right) {
    return (*(const int*)left - *(const int*)right);
}

void merge_front(int d[], const unsigned d_length,
                 const int a[], const unsigned a_length,
                 const int b[], const unsigned b_length) {
    unsigned a_i = 0;
    unsigned b_i = 0;
    unsigned d_i = 0;
    while (d_i < d_length) {
        if (a_i < a_length && b_i < b_length) {
            if (order(a + a_i, b + b_i) < 0) {
                d[d_i] = a[a_i++];
            } else {
                d[d_i] = b[b_i++];
            }
        } else if (a_i < a_length) {
            d[d_i] = a[a_i++];
        } else if (b_i < b_length) {
            d[d_i] = b[b_i++];
        } else {
            assert(0);
        }

        ++d_i;
    }
}

void merge_back(int d[], const unsigned d_length,
                const int a[], const unsigned a_length,
                const int b[], const unsigned b_length) {
    int a_i = a_length - 1;
    int b_i = b_length - 1;
    int d_i = d_length - 1;
    while (d_i >= 0) {
        if (a_i >= 0 && b_i >= 0) {
            if (order(a + a_i, b + b_i) >= 0) {
                d[d_i] = a[a_i--];
            } else {
                d[d_i] = b[b_i--];
            }
        } else if (a_i >= 0) {
            d[d_i] = a[a_i--];
        } else if (b_i >= 0) {
            d[d_i] = b[b_i--];
        } else {
            assert(0);
        }

        --d_i;
    }
}

void worker_loop(int storage[], unsigned a_length, const unsigned full_block_length,
                 const unsigned total_full_count, const unsigned bandwidth,
                 const int depth, const int height, const int rank, const int size) {
    // Calculate various usefull stuff.
    const int teammate = TEAMMATE(rank);
    const int bottom_nodes_count_complete = LEVEL_NODES_COUNT_COMPLETE(depth, height - 1);
    const int bottom_nodes_count = LEVEL_NODES_COUNT(FIRST_LEVEL_NODE(rank, depth, height - 1, height), bottom_nodes_count_complete, size);
    const int teammate_bottom_nodes_count = LEVEL_NODES_COUNT(FIRST_LEVEL_NODE(teammate, depth, height - 1, height), bottom_nodes_count_complete, size);

    // Allocate array for result of merging.
    const unsigned result_length = (IS_MAIN_CHILD(rank) ? bandwidth / 2 : bandwidth - bandwidth / 2);
    int *const result = malloc(result_length * sizeof(*result));
    assert(result != NULL);

    // Set up pointers.
    int *const a = storage;
    int *b = storage + a_length;

    // Sort initial block.
    qsort(a, a_length, sizeof(*a), order);

    // Exchange sorted initial blocks with teammate.
    unsigned b_length = 0;
    if (teammate < size) {
        MPI_Status status;
        MPI_Sendrecv(a, a_length, MPI_INT, teammate, TAG_EXCHANGE,
                     b, full_block_length, MPI_INT, teammate, TAG_EXCHANGE,
                     MPI_COMM_WORLD, &status);
        int received;
        MPI_Get_count(&status, MPI_INT, &received);
        b_length = received;
    }

    int step = 1;
    const int step_count = height - depth + (rank == HOST);
    while (1) {
#ifdef DEBUG_STEP
        printf("rank:%4d --- STEP %4d\n", rank, step);
#endif

        // Merge a and b to result.
        int *send;
        unsigned send_length;
        if (b_length == 0) {
            send = a;
            send_length = a_length;
        } else if (IS_MAIN_CHILD(rank)) {
            send = result;
            send_length = (a_length + b_length) / 2;
            merge_front(send, send_length, a, a_length, b, b_length);
        } else {
            send = result;
            send_length = a_length + b_length - (a_length + b_length) / 2;
            merge_back(send, send_length, a, a_length, b, b_length);
        }
#ifdef DEBUG
        printf("rank:%4d --- Merged\n", rank);
#endif

#ifdef DEBUG_ARRAY
        print_array("a:    ", a, a_length);
        print_array("b:    ", b, b_length);
        print_array("send: ", send, send_length);
#endif

        //
        // Perform send-receiver cycle.
        //
        const unsigned expected_length = (1 << step) * full_block_length;

        b = storage + LEVEL_BANDWIDTH(rank, size, depth, depth + step, height, total_full_count, full_block_length);
        a_length = 0;
        b_length = 0;

        int receiver = PARENT(rank);
#ifdef DEBUG
        printf("rank:%4d --- About to send to parent (%d).\n", rank, receiver);
#endif
        switch (rank) {
        case HOST:  // Send to self.
            memcpy(a + a_length, send, send_length * sizeof(*send));
            a_length += send_length;
            break;
        case ROOT:  // Exchange with HOST.
            MPI_Send(send, send_length, MPI_INT, receiver, TAG_EXCHANGE, MPI_COMM_WORLD);

            if (step < step_count) {
                MPI_Status status;
                MPI_Recv(b + b_length, expected_length - b_length, MPI_INT, receiver, TAG_EXCHANGE, MPI_COMM_WORLD, &status);
                int received;
                MPI_Get_count(&status, MPI_INT, &received);
                b_length += received;
            }
            break;
        default:    // Send to higher team.
            MPI_Send(send, send_length, MPI_INT, receiver, TAG_PASS, MPI_COMM_WORLD);
        }
#ifdef DEBUG
        printf("rank:%4d --- Sent to parent (%d).\n", rank, receiver);
#endif

        receiver = TEAMMATE(receiver);
#ifdef DEBUG
        printf("rank:%4d --- About to send to parent's teammate (%d).\n", rank, receiver);
#endif
        switch (rank) {
        case HOST:  // Exchange with ROOT.
            if (step < step_count) {
                MPI_Status status;
                MPI_Recv(a + a_length, expected_length - a_length, MPI_INT, receiver, TAG_EXCHANGE, MPI_COMM_WORLD, &status);
                int received;
                MPI_Get_count(&status, MPI_INT, &received);
                a_length += received;

                if (step < step_count - 1) {
                    MPI_Send(send, send_length, MPI_INT, receiver, TAG_EXCHANGE, MPI_COMM_WORLD);
                }
            }
            break;
        case ROOT:  // Send to self.
            if (step < step_count) {
                memcpy(b + b_length, send, send_length * sizeof(*send));
                b_length += send_length;
            }
            break;
        default:    // Send to higher team.
            if (step < step_count || LEVEL_NODES_COUNT(FIRST_LEVEL_NODE(receiver, depth - 1, height - 1, height), LEVEL_NODES_COUNT_COMPLETE(depth - 1, height - 1), size) > 0) {
                MPI_Send(send, send_length, MPI_INT, receiver, TAG_PASS, MPI_COMM_WORLD);
            }
        }
#ifdef DEBUG
        printf("rank:%4d --- Sent to parent's teammate (%d).\n", rank, receiver);
#endif

    if (++step > step_count || step == step_count && bottom_nodes_count == 0) break;

        // Collect array a.
        int sender = MAIN_CHILD(rank);
        if (rank != HOST) {
            if (step < step_count || bottom_nodes_count > 0) {
                MPI_Status status;
                MPI_Recv(a + a_length, expected_length - a_length, MPI_INT, sender, TAG_PASS, MPI_COMM_WORLD, &status);
                int received;
                MPI_Get_count(&status, MPI_INT, &received);
                a_length += received;
            }
        }
#ifdef DEBUG
        printf("rank:%4d --- Received from main child.\n", rank);
#endif

        sender = TEAMMATE(sender);
        if (rank != HOST) {
            if (step < step_count || bottom_nodes_count > bottom_nodes_count_complete / 2) {
                MPI_Status status;
                MPI_Recv(a + a_length, expected_length - a_length, MPI_INT, sender, TAG_PASS, MPI_COMM_WORLD, &status);
                int received;
                MPI_Get_count(&status, MPI_INT, &received);
                a_length += received;
            }
        }
#ifdef DEBUG
        printf("rank:%4d --- Received from main child's teammate.\n", rank);
#endif

        // Collect array b.
        if (rank != ROOT && (rank != HOST || step < step_count) && a_length > 0) {
            sender = MAIN_CHILD(teammate);
            if (step < step_count - (rank == HOST) || teammate_bottom_nodes_count > 0) {
                MPI_Status status;
                MPI_Recv(b + b_length, expected_length - b_length, MPI_INT, sender, TAG_PASS, MPI_COMM_WORLD, &status);
                int received;
                MPI_Get_count(&status, MPI_INT, &received);
                b_length += received;
            }
#ifdef DEBUG
            printf("rank:%4d --- Received from teammate's main child.\n", rank);
#endif

            sender = TEAMMATE(sender);
            if (step < step_count - (rank == HOST) || teammate_bottom_nodes_count > bottom_nodes_count_complete / 2) {
                MPI_Status status;
                MPI_Recv(b + b_length, expected_length - b_length, MPI_INT, sender, TAG_PASS, MPI_COMM_WORLD, &status);
                int received;
                MPI_Get_count(&status, MPI_INT, &received);
                b_length += received;
            }
#ifdef DEBUG
            printf("rank:%4d --- Received from teammate's main child's teammate.\n", rank);
#endif
        }
    }
#ifdef DEBUG_STOPPED
    printf("rank:%4d --- STOPPED\n", rank);
#endif
#ifdef DEBUG_ARRAY
    print_array("a:    ", a, a_length);
    print_array("b:    ", b, b_length);
#endif

    free(result);
}

void node_entry(const int rank, const int size) {
    // Receive the length of the array to be worked on.
    unsigned worked_length;
    MPI_Bcast(&worked_length, 1, MPI_UNSIGNED, HOST, MPI_COMM_WORLD);

    if (worked_length > 0) {
        // Calculate helping stuff.
        const unsigned total_full_count = TOTAL_FULL_COUNT(worked_length, size);
        const unsigned full_block_length = FULL_BLOCK_DEFAULT_LENGTH(worked_length, size);

        // Calculate bandwidth of self and teammate.
        const int height = HEIGHT(size);
        const int depth = DEPTH(rank);
        const unsigned bandwidth = BANDWIDTH(rank, size, depth, height, total_full_count, full_block_length, worked_length);
#ifdef DEBUG_BANDWIDTH
        printf("rank:%4d\tbandwidth:%4u\n", rank, bandwidth);
#endif

        // Allocate space for storing sorted parts that are to be merged.
        int *const storage = malloc(bandwidth * sizeof(*storage));
        assert(storage != NULL);

        // Receive initial block.
        MPI_Status status;
        MPI_Recv(storage, full_block_length, MPI_INT, MPI_ANY_SOURCE, TAG_INITIAL, MPI_COMM_WORLD, &status);
        int received;
        MPI_Get_count(&status, MPI_INT, &received);
        const unsigned block_length = received;

        // Main working loop.
        worker_loop(storage, block_length, full_block_length, total_full_count,
                    bandwidth, depth, height, rank, size);

        // Clean up.
        free(storage);
    }
}

void merge_sort(int a[], const unsigned length, const int rank, const int size) {
    int parallel = (size > 1);
    if (parallel) {
        parallel = (length > size);

        // Make everybody know the length of the array to be worked on.
        unsigned worked_length = (parallel ? length : 0);
        MPI_Bcast(&worked_length, 1, MPI_UNSIGNED, HOST, MPI_COMM_WORLD);
    }

    if (parallel) {
        // Calculate helping stuff.
        const unsigned total_full_count = TOTAL_FULL_COUNT(length, size);
        const unsigned full_block_length = FULL_BLOCK_DEFAULT_LENGTH(length, size);

        // Send blocks.
        MPI_Request request[size - 1];
        unsigned offset = full_block_length;
        int node;
        for (node = 1; node < size; ++node) {
            const unsigned block_length = full_block_length - (node >= total_full_count);
            MPI_Isend(a + offset, block_length, MPI_INT, node, TAG_INITIAL, MPI_COMM_WORLD, &request[node - 1]);
            offset += block_length;
        }

        // Calculate bandwidth of self and teammate.
        const int height = HEIGHT(size);
        const int depth = DEPTH(rank);
        const unsigned bandwidth = BANDWIDTH(rank, size, depth, height, total_full_count, full_block_length, length);
#ifdef DEBUG_BANDWIDTH
        printf("rank:%4d\tbandwidth:%4u\n", rank, bandwidth);
#endif

        // Wait when sending is completed.
        MPI_Status status;
        for (node = 1; node < size; ++node) {
            MPI_Wait(&request[node - 1], &status);
        }

        // Main working loop.
        worker_loop(a, full_block_length, full_block_length, total_full_count,
                    bandwidth, depth, height, rank, size);
    } else {
        qsort(a, length, sizeof(*a), order);
    }
}

void host_entry(const int rank, const int size, const unsigned length,
                const int do_sequential_sort) {
    // Allocate space for data to be processed.
    int *const a = malloc(length * sizeof(*a));
    assert(a != NULL);

    // Pretend reading data from somewhere.
    generate_array(a, length);

    // Duplicate data for sequential sorting.
    int *const b = (do_sequential_sort ? malloc(length * sizeof(*b)) : NULL);
    if (do_sequential_sort) {
        assert(b != NULL);
        memcpy(b, a, length * sizeof(*a));
    }

    // Sort. Parallel, then sequential.
    const double startTime = MPI_Wtime();
    merge_sort(a, length, rank, size);
    const double parallelTime = MPI_Wtime() - startTime;
    if (do_sequential_sort) {
        qsort(b, length, sizeof(*b), order);
    }
    const double sequentialTime = MPI_Wtime() - startTime - parallelTime;

    printf("size: %d\n", size);
    printf("length: %u\n\n", length);

    // Validate results.
    const unsigned last = validate_order(a, length, order);
    int passed = (last == length - 1);
    printf("Validation of order:\t%s\n", passed ? "PASSED" : "FAILED");
    if (passed && do_sequential_sort) {
        passed = (memcmp(a, b, length * sizeof(*a)) == 0);
        printf("Validation of equality:\t%s\n", passed ? "PASSED" : "FAILED");
#ifdef DEBUG_ARRAY
        if (!passed) {
            print_array("seq: ", b, length);
        }
#endif
    }
    printf("\n");

    // Report time measurements.
    if (passed) {
        printf("%25s: %10.6f\n", "Parallel Merge/Quick sort", parallelTime);
        if (do_sequential_sort) {
            printf("%25s: %10.6f\n", "Sequential Quick sort", sequentialTime);
            printf("%25s: %10.6f\n", "Boost ratio", sequentialTime / parallelTime);
        }
    }

    // Clean up.
    if (b != NULL) {
        free(b);
    }
    free(a);
}

int main(int argc, char* argv[]) {
    unsigned length = DEFAULT_LENGTH;
    if (argc > 1) {
        sscanf(argv[1], "%u", &length);
    }

    int do_sequential_sort = 1;
    if (argc > 2) {
        do_sequential_sort = (strcmp(argv[2], "false") != 0);
    }

    srand(41);
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == HOST) {
        host_entry(rank, size, length, do_sequential_sort);
    } else {
        node_entry(rank, size);
    }

    MPI_Finalize();
    return (EXIT_SUCCESS);
}
