#!/usr/bin/awk -f

BEGIN {FS = ":"}
/size/ {size = $2}
/length/ {}
/Validation of order/ {order = $2}
/Parallel/ {parallel = $2}
END {
	if (order ~ /PASSED/) {
		print size, parallel
	} else {
		exit 1
	}
}
