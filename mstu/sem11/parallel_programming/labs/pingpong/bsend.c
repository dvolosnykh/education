#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>

#define MESSAGE_LEN_MAX 256
#define TAG_PINGPONG 123
#define CYCLES_COUNT 1000
#define BUFFER_MESSAGE_COUNT 1024

void request_node(const int rank, const int teammate, const char* const request, char* const reply) {
	static char message[MESSAGE_LEN_MAX];

	strcpy(message, request);
	MPI_Bsend(message, strlen(message), MPI_CHAR, teammate, TAG_PINGPONG, MPI_COMM_WORLD);

	MPI_Status status;
	MPI_Recv(message, MESSAGE_LEN_MAX, MPI_CHAR, teammate, TAG_PINGPONG, MPI_COMM_WORLD, &status);
	strcpy(reply, message);
}

void reply_node(const int rank, const int teammate, const char* const reply, char* const request) {
	static char message[MESSAGE_LEN_MAX];

	MPI_Status status;
	MPI_Recv(message, MESSAGE_LEN_MAX, MPI_CHAR, teammate, TAG_PINGPONG, MPI_COMM_WORLD, &status);
	strcpy(request, message);

	strcpy(message, reply);
	MPI_Bsend(message, strlen(message), MPI_CHAR, teammate, TAG_PINGPONG, MPI_COMM_WORLD);
}

void pingpong(const int rank, const int size) {
	const int odd = rank & 0x1;
	const int teammate = rank + 1 - (odd << 1);

	if (teammate < size) {
		char message[MESSAGE_LEN_MAX];

		int packSize;
		MPI_Pack_size(4, MPI_CHAR, MPI_COMM_WORLD, &packSize);
		int bufferSize = MPI_BSEND_OVERHEAD + BUFFER_MESSAGE_COUNT * packSize;
		char* buffer = (char*)malloc(bufferSize);
		MPI_Buffer_attach(buffer, bufferSize);

		const double start = MPI_Wtime();
		int cycle;
		for (cycle = 0; cycle < CYCLES_COUNT; ++cycle) {
			if (!odd) {
				request_node(rank, teammate, "ping", message);
			} else {
				reply_node(rank, teammate, "pong", message);
			}
		}
		const double elapsedTime = MPI_Wtime() - start;

		printf("%d: %s from %d\n"
			"%d: Communication period was at average: %f\n",
			rank, message, teammate, rank, elapsedTime / CYCLES_COUNT
		);

		MPI_Buffer_detach(&buffer, &bufferSize);
	} else {
		printf("This node has no pair.\n");
	}
}

int main(int argc, char** argv) {
	MPI_Init(&argc, &argv);

	int size, rank;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	pingpong(rank, size);

	MPI_Finalize();

	return (EXIT_SUCCESS);
}
