gcc -fopenmp omp.c -o omp &&
for n in 1 2 4; do
	export OMP_NUM_THREADS=$n
	./omp
done
