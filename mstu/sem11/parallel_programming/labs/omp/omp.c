#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <assert.h>


//#define N 100000000
#define N 230000


//#define DEBUG_SIZE
//#define DEBUG_OFFSET_COUNT



int min(const int a, const int b) {
    return (a < b ? a : b);
}

int max(const int a, const int b) {
    return (a > b ? a : b);
}

int main() {
	const int n = N;
	int *const a = malloc(n * sizeof(*a));
	assert(a != NULL);

	int i;
	for (i = 0; i < n; ++i) {
		a[i] = i + 1;
	}

	int sum = 0;
	const double start = omp_get_wtime();
#if 0
	#pragma omp parallel for reduction(+:sum) private(i)
	for (i = 0; i < n; ++i) {
		sum += a[i] * a[i];
	}
#else

	#pragma omp parallel reduction(+:sum)
	{
		const int size = omp_get_num_threads();
#ifdef DEBUG_SIZE
		printf("size: %d\n", size);
#endif
		const size_t full_length = (n - 1) / size + 1;
		const size_t full_count = (n - 1) % size + 1;

		const int rank = omp_get_thread_num();
		const size_t offset = rank * (full_length - 1) + min(rank, full_count);
		const size_t count = full_length - (rank >= full_count);
#ifdef DEBUG_OFFSET_COUNT
		printf("rank:%4d --- offset: %u; count: %u\n", rank, offset, count);
#endif

		#pragma omp barrier
		sum = 0;
		size_t i;
		for (i = offset; i < offset + count; ++i) {
			sum += a[i] * a[i];
		}
	}
#endif
	const double elapsed = omp_get_wtime() - start;

	printf("Threads: %d\n", omp_get_max_threads());
	printf("Length: %d\n", N);
	printf("Sum: %d\n", sum);
	printf("Time: %.15lf\n\n", elapsed);

	free(a);
	return (EXIT_SUCCESS);
}
