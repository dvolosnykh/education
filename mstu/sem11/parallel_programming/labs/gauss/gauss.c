#include <stdio.h>
#include <time.h>

int n;
float a[2000][2001];

void forwardSubstitution() {

    int i, j, k, max;
    float t;
    
    for (i = 0; i < n; ++i) {   
	for (j = n; j >= i; --j)
	    for (k = i + 1; k < n; ++k)
	        a[k][j] -= a[k][i]/a[i][i] * a[i][j];
																																				
    }
}
																																													
void reverseElimination() {
    int i, j;
    
    for (i = n - 1; i >= 0; --i) {
    
	a[i][n] = a[i][n] / a[i][i];
	a[i][i] = 1;
	
	for (j = i - 1; j >= 0; --j) {
	    a[j][n] -= a[j][i] * a[i][n];
	    a[j][i] = 0;
	}
    }
}

void gauss() {
int i, j;

    forwardSubstitution();
    reverseElimination();
/*    
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n + 1; ++j)
	    printf("%.2f\t", a[i][j]);
	printf("\n");
    }
*/
}

void generate_matrix() {
        int i;
        for (i = 0; i < n; ++i) {
                int j;
                for (j = 0; j < n + 1; ++j) {
                        a[i][j] = rand() % 100;
                }
        }
}


int main(int argc, char *argv[]) {
    n = 2000;
    generate_matrix();

    const time_t start = time(NULL);
    gauss();
    const time_t end = time(NULL);
    const double elapsed = difftime(end, start);

    printf("Time[0] %lf\n", elapsed);

    return 0;
}
