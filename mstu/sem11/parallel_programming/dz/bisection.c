#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <omp.h>
#include <memory.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include <assert.h>


//#define DEBUG_MATRIX
//#define DEBUG_X
//#define DEBUG_NORM_A
//#define DEBUG_NORM_X
//#define DEBUG_Y
//#define DEBUG_PRODUCT
//#define DEBUG_MAX_ABS_VALUE
//#define DEBUG_SUM_SQUARES
//#define DEBUG_SUBMATRIX
//#define DEBUG_COLUMN_ROW
//#define DEBUG_ITERATION
//#define DEBUG_DETERMINANT
//#define DEBUG_INFINITE_NORM
#define DEBUG_LAMBDA
//#define DEBUG_LAMBDA_COUNT
//#define DEBUG_OFFSETS_COUNTS


#define DEFAULT_SIZE 9
#define PRECISION 5
#define EPSILON 1e-5
#define PADDING 1


enum {
    HOST
};

enum {
    TAG_LAMBDA = 0
};

#define FIELD_WIDTH (PADDING + 1 + 2 + 1 + PRECISION)
#define ROW_WIDTH(n) (FIELD_WIDTH * (n) + 1)

#define ROW_OFFSET(row, column_count) ((row) * (column_count) - (row) * ((row) - 1) / 2)
#define ROW(a, row, column_count) ((a) + ROW_OFFSET(row, column_count))
#define ELEMENT(a, row, column, column_count) *(ROW(a, row, column_count) + (column) - (row))

#define FULL_BLOCK_DEFAULT_LENGTH(length, size) (((length) - 1) / (size) + 1)
#define TOTAL_FULL_COUNT(length, size) (((length) - 1) % (size) + 1)


int min(const int a, const int b) {
    return (a < b ? a : b);
}

int max(const int a, const int b) {
    return (a > b ? a : b);
}

double calc_max(const double a[], const size_t length) {
#if 0
    double max_value = a[0];

    size_t i;
//#pragma omp parallel for private(i) if (length > 1000)
    for (i = 1; i < length; ++i) {
        if (a[i] > max_value) {
            max_value = a[i];
        }
    }
#else
    const int max_size = omp_get_max_threads();
    double max_values[max_size];
    size_t i;
    for (i = 0; i < max_size; ++i) {
        max_values[i] = -DBL_MAX;
    }

    #pragma omp parallel //if (length > 1000)
    {
        const int size = omp_get_num_threads();
        const size_t full_length = (length - 1) / size + 1;
        const size_t full_count = (length - 1) % size + 1;

        const int rank = omp_get_thread_num();
        const size_t offset = rank * (full_length - 1) + min(rank, full_count);
        const size_t count = full_length - (rank >= full_count);

        max_values[rank] = a[offset];
        size_t i;
        for (i = offset + 1; i < offset + count; ++i) {
            if (a[i] > max_values[rank]) {
                max_values[rank] = a[i];
            }
        }
    }

    double max_value = max_values[0];
    for (i = 1; i < max_size; ++i) {
        if (max_values[i] > max_value) {
            max_value = max_values[i];
        }
    }
#endif
    return (max_value);
}

double calc_sum_of_squares(const double a[], const size_t length) {
    double sum = 0.0;

    #pragma omp parallel reduction(+:sum) //if (length > 1000)
    {
        const int size = omp_get_num_threads();
        const size_t full_length = (length - 1) / size + 1;
        const size_t full_count = (length - 1) % size + 1;

        const int rank = omp_get_thread_num();
        const size_t offset = rank * (full_length - 1) + min(rank, full_count);
        const size_t count = full_length - (rank >= full_count);

        sum = 0;
        size_t i;
        for (i = offset; i < offset + count; ++i) {
            sum += a[i] * a[i];
        }
    }

    return (sum);
}

double calc_sum(const double a[], const size_t length) {
    double sum = 0.0;

    #pragma omp parallel reduction(+:sum) //if (length > 1000)
    {
        const int size = omp_get_num_threads();
        const size_t full_length = (length - 1) / size + 1;
        const size_t full_count = (length - 1) % size + 1;

        const int rank = omp_get_thread_num();
        const size_t offset = rank * (full_length - 1) + min(rank, full_count);
        const size_t count = full_length - (rank >= full_count);

        sum = 0;
        size_t i;
        for (i = offset; i < offset + count; ++i) {
            sum += a[i];
        }
    }

    return (sum);
}

void multiply_symmetric_matrix_by_vector(
    double y[], const double a[], const double x[], const size_t first_row,
    const size_t row_count, const size_t column_count,
    const int rank
) {
#ifdef DEBUG_MULTIPLY
    char buffer[16 + row_count * ROW_WIDTH(column_count)];
#endif
#ifdef DEBUG_MULTIPLY
    int count = sprintf(buffer, "rank:%4d ---\n", rank);
#endif
    size_t row;
#pragma omp parallel for private(row)
    for (row = 0; row < row_count; ++row) {
        y[first_row + row] = 0.0;
        size_t offset = first_row + row;

        size_t column = 0;
        for (; column < first_row + row; ++column) {
#ifdef DEBUG_MULTIPLY
            count += sprintf(buffer + count, "%*.*lf", FIELD_WIDTH, PRECISION, a[offset]);
#endif
            y[first_row + row] += a[offset] * x[column];
            offset += column_count - column - 1;
        }
        for (; column < column_count; ++column) {
#ifdef DEBUG_MULTIPLY
            count += sprintf(buffer + count, "%*.*lf", FIELD_WIDTH, PRECISION, a[offset]);
#endif
            y[first_row + row] += a[offset] * x[column];
            ++offset;
        }
#ifdef DEBUG_MULTIPLY
        count += sprintf(buffer + count, "\n");
#endif
    }
#ifdef DEBUG_MULTIPLY
    puts(buffer);
#endif
}

double calc_scalar_product(const double x[], const double y[], const size_t length) {
    double product = 0.0;

    #pragma omp parallel reduction(+:product) //if (length > 1000)
    {
        const int size = omp_get_num_threads();
        const size_t full_length = (length - 1) / size + 1;
        const size_t full_count = (length - 1) % size + 1;

        const int rank = omp_get_thread_num();
        const size_t offset = rank * (full_length - 1) + min(rank, full_count);
        const size_t count = full_length - (rank >= full_count);

        product = 0;
        size_t i;
        for (i = offset; i < offset + count; ++i) {
            product += x[i] * y[i];
        }
    }

    return (product);
}

void normalize(double x[], const size_t length, const double norm_x) {
    #pragma omp parallel //if (length > 1000)
    {
        const int size = omp_get_num_threads();
        const size_t full_length = (length - 1) / size + 1;
        const size_t full_count = (length - 1) % size + 1;

        const int rank = omp_get_thread_num();
        const size_t offset = rank * (full_length - 1) + min(rank, full_count);
        const size_t count = full_length - (rank >= full_count);

        size_t i;
        for (i = offset; i < offset + count; ++i) {
            x[i] /= norm_x;
        }
    }
}

#ifdef DEBUG_DETERMINANT
double determinant(const size_t n, const double a[n][n]) {
    double det = 0.0;

    switch (n) {
    case 1:
        det = a[0][0];
        break;
    case 2:
        det = a[0][0] * a[1][1] - a[0][1] * a[1][0];
        break;
    default: {
        double m[n - 1][n - 1];

        size_t count;
        for (count = 0; count < n; ++count) {
            size_t i;
            for(i = 1; i < n; ++i) {
                size_t j;
                for (j = 0; j < n; ++j) {
                    if (j != count) {
                        m[i - 1][j - (j > count)] = a[i][j];
                    }
                }
            }

            det += (count % 2 == 0 ? 1 : -1) * a[0][count] * determinant(n - 1, m);
        }
    }
    }

    return det;
}
#endif

void generate_symmetric_matrix(double a[], const size_t n) {
    size_t row;
    for (row = 0; row < n; ++row) {
        size_t column;
        for (column = row; column < n; ++column) {
            ELEMENT(a, row, column, n) = row + (column / 10.0);
        }
    }

//    const size_t length = n * (n + 1) / 2;
//    size_t i;
//    for (i = 0; i < length; ++i) {
//        a[i] = rand() / (double)RAND_MAX;
//    }
}

void print_symmetric_matrix(const double a[], const size_t n, const int rank) {
    char buffer[16 + n * ROW_WIDTH(n)];

    int count = sprintf(buffer, "rank:%4d ---\n", rank);
    size_t row;
    for (row = 0; row < n; ++row) {
        size_t column;
        for (column = 0; column < n; ++column) {
            if (column < row) {
                count += sprintf(buffer + count, "%*s", FIELD_WIDTH, "");
            } else {
                count += sprintf(buffer + count, "%*.*lf", FIELD_WIDTH, PRECISION, ELEMENT(a, row, column, n));
            }
        }
        count += sprintf(buffer + count, "\n");
    }
    puts(buffer);
}

void print_vector(const double a[], const size_t length, const int rank) {
    char buffer[16 + ROW_WIDTH(length)];

    int count = sprintf(buffer, "rank:%4d ---\n", rank);
    size_t i;
    for (i = 0; i < length; ++i) {
        count += sprintf(buffer + count, "%*.*lf", FIELD_WIDTH, PRECISION, a[i]);
    }
    count += sprintf(buffer + count, "\n");
    puts(buffer);
}

void print_ivector(const int a[], const size_t length, const int rank) {
    char buffer[16 + ROW_WIDTH(length)];

    int count = sprintf(buffer, "rank:%4d ---\n", rank);
    size_t i;
    for (i = 0; i < length; ++i) {
        count += sprintf(buffer + count, "%*d", FIELD_WIDTH, a[i]);
    }
    count += sprintf(buffer + count, "\n");
    puts(buffer);
}

void calc_counts_offsets(int counts[], int offsets[], const int size,
                         const size_t length) {
    const size_t full_block_length = FULL_BLOCK_DEFAULT_LENGTH(length, size);
    const size_t total_full_count = TOTAL_FULL_COUNT(length, size);

    int node = 0;
    offsets[node] = 0;
    counts[node] = full_block_length;
    while (++node < size) {
        offsets[node] = offsets[node - 1] + counts[node - 1];
        counts[node] = full_block_length - (node >= total_full_count);
    }
}

size_t find_row(const size_t offset, const size_t n) {
    const size_t length = n * (n + 1) / 2;

    size_t k_min = 0;
    size_t k_max = n - 1;
    size_t k;
    while (1) {
        k = (k_min + k_max) / 2;
        const size_t high = length - k * (k + 1) / 2;
        const size_t low = high - (k + 1);

        if (offset < low) {
            k_min = k + 1;
        } else if (high <= offset) {
            k_max = k - 1;
        } else {
            break;
        }
    }

    const size_t row = n - 1 - k;
    return (row);
}

double calc_x(double x[], const double a[], const int counts[],
              const int offsets[], const int rank, const int size) {
    size_t i;
    for (i = offsets[rank]; i < offsets[rank] + counts[rank]; ++i) {
        x[i] = a[i];
    }
    if (rank != HOST) {
        x[0] = a[0];
    }

    double sums[size];

    const size_t sum_offset = offsets[rank] + (rank == HOST);
    const size_t sum_count = counts[rank] - (rank == HOST);
    sums[rank] = calc_sum_of_squares(x + sum_offset, sum_count);
#ifdef DEBUG_SUM_SQUARES
    printf("rank:%4d --- local sum:%*.*lf\n", rank, FIELD_WIDTH, PRECISION, sums[rank]);
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

    if (size > 1) {
        MPI_Allgather(sums + rank, 1, MPI_DOUBLE, sums, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    }
    const double sum = calc_sum(sums, size);
#ifdef DEBUG_SUM_SQUARES
    if (rank == HOST) {
        printf("rank:%4d --- global sum:%*.*lf\n", rank, FIELD_WIDTH, PRECISION, sum);
    }
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

    const double norm_a = sqrt(x[0] * x[0] + sum);
#ifdef DEBUG_NORM_A
    printf("rank:%4d --- norm a:%*.*lf\n", rank, FIELD_WIDTH, PRECISION, norm_a);
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif
    x[0] -= norm_a;
#ifdef DEBUG_X
    if (rank == HOST) {
        print_vector(x, offsets[size - 1] + counts[size - 1], rank);
    }
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif
    const double norm_x = sqrt(x[0] * x[0] + sum);
#ifdef DEBUG_NORM_X
    printf("rank:%4d --- norm x:%*.*lf\n", rank, FIELD_WIDTH, PRECISION, norm_x);
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif
    normalize(x + offsets[rank], counts[rank], norm_x);

    if (size > 1) {
        MPI_Allgatherv(x + offsets[rank], counts[rank], MPI_DOUBLE, x, (int *)counts, (int *)offsets, MPI_DOUBLE, MPI_COMM_WORLD);
    }
#ifdef DEBUG_X
    if (rank == HOST) {
        print_vector(x, offsets[size - 1] + counts[size - 1], rank);
    }
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

    return (norm_a);
}

void calc_y(double y[], const double a[], const double x[], const size_t n,
            const int counts[], const int offsets[], const int rank, const int size) {
    multiply_symmetric_matrix_by_vector(y, a, x, offsets[rank], counts[rank], n, rank);

    double products[size];
    products[rank] = calc_scalar_product(x + offsets[rank], y + offsets[rank], counts[rank]);
    if (size > 1) {
        MPI_Allgather(products + rank, 1, MPI_DOUBLE, products, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    }
#ifdef DEBUG_PRODUCT
    print_vector(products, size, rank);
#endif
    const double product = calc_sum(products, size);
#ifdef DEBUG_PRODUCT
    if (rank == HOST) {
        printf("rank:%4d --- product:%*.*lf\n", rank, FIELD_WIDTH, PRECISION, product);
    }
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

    size_t i;
    for (i = offsets[rank]; i < offsets[rank] + counts[rank]; ++i) {
        y[i] = 2 * (y[i] - product * x[i]);
    }

    if (size > 1) {
        MPI_Allgatherv(y + offsets[rank], counts[rank], MPI_DOUBLE, y, (int *)counts, (int *)offsets, MPI_DOUBLE, MPI_COMM_WORLD);
    }
#ifdef DEBUG_Y
    if (rank == HOST) {
        print_vector(y, offsets[size - 1] + counts[size - 1], rank);
    }
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif
}

void calc_a(double a[], const double x[], const double y[], const size_t n,
            const int rank, const int size) {
    const size_t length = n * (n + 1) / 2;
    int counts[size], offsets[size];
    calc_counts_offsets(counts, offsets, size, length);

    size_t offset = offsets[rank];
    size_t row = find_row(offset, n);
    const size_t low = ROW_OFFSET(row, n);
    size_t high = low + (n - row);
    size_t column = row + (offset - low);
#ifdef DEBUG_COLUMN_ROW
    printf("rank:%4d --- offset:%4d; row:%4d; column:%4d\n", rank, offset, row, column);
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

    while (offset < offsets[rank] + counts[rank]) {
        a[offset] -= x[row] * y[column] + x[column] * y[row];

        if (++offset == high) {
            ++row;
            column = row;
            high = offset + (n - row);
        } else {
            ++column;
        }
    }

    if (size > 1) {
        MPI_Allgatherv(a + offsets[rank], counts[rank], MPI_DOUBLE, a, (int *)counts, (int *)offsets, MPI_DOUBLE, MPI_COMM_WORLD);
    }
}

void zero_row(double r[], const double norm_a, const int counts[],
              const int offsets[], const int rank, const int size) {
    if (rank == HOST) {
        r[0] = norm_a;
    }
    size_t i;
    for (i = (rank == HOST); i < counts[rank]; ++i) {
        r[offsets[rank] + i] = 0.0;
    }

    if (size > 1) {
        MPI_Allgatherv(r + offsets[rank], counts[rank], MPI_DOUBLE, r, (int *)counts, (int *)offsets, MPI_DOUBLE, MPI_COMM_WORLD);
    }
}

void tridiagonalization(double a[], double x[], double y[], const size_t n,
                        const int rank, const int size) {
    const size_t LOW_LIMIT = 500;

    int counts[size], offsets[size];

#ifdef DEBUG_DETERMINANT
    double aa[n][n];
    size_t i;
    for (i = 0; i < n; ++i) {
        size_t j;
        for (j = 0; j < n; ++j) {
            aa[i][j] = ELEMENT(a, min(i, j), max(i, j), n);
        }
    }
    double d = determinant(n, aa);
    if (rank == HOST) {
        printf("rank:%4d --- determinant:%*.*lf\n", rank, FIELD_WIDTH, PRECISION, d);
    }
#endif

    double *submatrix = a;
    size_t submatrix_n = n;

    while (submatrix_n > 2 && (submatrix_n > LOW_LIMIT || rank == HOST)) {
        const int iteration_size = (submatrix_n > LOW_LIMIT ? size : 1);

        submatrix += submatrix_n--;
#ifdef DEBUG_ITERATION
        if (rank == HOST) {
            printf("\n\nrank:%4d --- submatrix_n:%4d\n\n\n", rank, submatrix_n);
        }
        if (iteration_size > 1) {
            MPI_Barrier(MPI_COMM_WORLD);
        }
#endif

        calc_counts_offsets(counts, offsets, iteration_size, submatrix_n);
#ifdef DEBUG_OFFSETS_COUNTS
        print_ivector(offsets, iteration_size, rank);
        if (iteration_size > 1) {
            MPI_Barrier(MPI_COMM_WORLD);
        }
        print_ivector(counts, iteration_size, rank);
        if (iteration_size > 1) {
            MPI_Barrier(MPI_COMM_WORLD);
        }
#endif
#ifdef DEBUG_SUBMATRIX
        if (rank == HOST) {
            print_symmetric_matrix(submatrix, submatrix_n, rank);
        }
        if (iteration_size > 1) {
            MPI_Barrier(MPI_COMM_WORLD);
        }
#endif

        const double norm_a = calc_x(x, submatrix - submatrix_n, counts, offsets, rank, iteration_size);
        calc_y(y, submatrix, x, submatrix_n, counts, offsets, rank, iteration_size);
        calc_a(submatrix, x, y, submatrix_n, rank, iteration_size);
        zero_row(submatrix - submatrix_n, norm_a, counts, offsets, rank, iteration_size);
#ifdef DEBUG_MATRIX
        if (rank == HOST) {
            print_symmetric_matrix(a, n, rank);
        }
        if (iteration_size > 1) {
            MPI_Barrier(MPI_COMM_WORLD);
        }
#endif
    }

    if (size > 1) {
        const size_t length = n * (n + 1) / 2;
        const size_t max_seq_n = min(n, LOW_LIMIT);
        const size_t remaining_length = max_seq_n * (max_seq_n + 1) / 2;
        MPI_Bcast(a + length - remaining_length, remaining_length, MPI_DOUBLE, HOST, MPI_COMM_WORLD);
    }

#ifdef DEBUG_MATRIX
    if (rank == HOST) {
        print_symmetric_matrix(a, n, rank);
    }
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

#ifdef DEBUG_DETERMINANT
    for (i = 0; i < n; ++i) {
        size_t j;
        for (j = 0; j < n; ++j) {
            aa[i][j] = ELEMENT(a, min(i, j), max(i, j), n);
        }
    }
    d = determinant(n, aa);
    if (rank == HOST) {
        printf("rank:%4d --- determinant:%*.*lf\n", rank, FIELD_WIDTH, PRECISION, d);
    }
#endif
}

double calc_infinite_norm(const double a[], const size_t n, const int rank,
                          const int size) {
    int counts[size], offsets[size];
    calc_counts_offsets(counts, offsets, size, n - 1);

    double max_abs_rows_sums[size + 1];

    // Find local maximum absolute value.
    max_abs_rows_sums[rank] = 0.0;
    size_t offset = ROW_OFFSET(offsets[rank], n);
    size_t i;
    for (i = offsets[rank]; i < offsets[rank] + counts[rank]; ++i) {
        const double abs_row_sum = fabs(a[offset - (n - i)]) + fabs(a[offset]) + fabs(a[offset + 1]);
        if (abs_row_sum > max_abs_rows_sums[rank]) {
            max_abs_rows_sums[rank] = abs_row_sum;
        }
        offset += n - i;
    }
#ifdef DEBUG_MAX_ABS_VALUE
    printf("rank:%4d --- local max abs row sum:%*.*lf\n", rank, FIELD_WIDTH, PRECISION, max_abs_rows_sums[rank]);
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

    if (size > 1) {
        MPI_Allgather(max_abs_rows_sums + rank, 1, MPI_DOUBLE, max_abs_rows_sums, 1, MPI_DOUBLE, MPI_COMM_WORLD);
    }
    max_abs_rows_sums[size] = fabs(a[n * (n + 1) / 2  - 1]);
    const double max_abs_row_sum = calc_max(max_abs_rows_sums, size + 1);

#ifdef DEBUG_MAX_ABS_VALUE
    printf("rank:%4d --- global max abs row sum:%*.*lf\n", rank, FIELD_WIDTH, PRECISION, max_abs_row_sum);
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

    return (max_abs_row_sum);
}

size_t sign_change_count(const double a[], const size_t n, const double lambda) {
    double l = a[0] - lambda;
    size_t count = (l < 0);

    size_t offset = n;
    size_t i;
    for (i = 1; i < n; ++i) {
        const double b = a[offset - (n - i)];
        l = (a[offset] - lambda) - b * b / l;
        count += (l < 0);
        offset += n - i;
    }

    return (count);
}

typedef struct task_s {
    double from, to;
    struct task_s *prev;
} task_t;

#define PUSH_TASK(last_task, lower, upper) {    \
    task_t *const prev_task = last_task;        \
    last_task = malloc(sizeof(*last_task));     \
    assert(last_task != NULL);                  \
    last_task->from = lower;                    \
    last_task->to = upper;                      \
    last_task->prev = prev_task;                \
}

#define POP_TASK(last_task, lower, upper) {     \
    task_t *const prev_task = last_task->prev;  \
    upper = last_task->to;                      \
    lower = last_task->from;                    \
    free(last_task);                            \
    last_task = prev_task;                      \
}

void bisection(double lambda[], const double a[], const size_t n,
               const double lower_bound, const double upper_bound,
               const int rank, const int size) {
    const double part = (upper_bound - lower_bound) / size;
    size_t lambda_count = 0;

    task_t *last_task = NULL;

    double lower = lower_bound + rank * part;
    double upper = lower + part;
    PUSH_TASK(last_task, lower, upper);

    while (last_task != NULL) {
        POP_TASK(last_task, lower, upper);

        const size_t upper_count = sign_change_count(a, n, upper);
        const size_t lower_count = sign_change_count(a, n, lower);
        const size_t roots_count = upper_count - lower_count;
        if (roots_count > 0) {
            const double middle = 0.5 * (lower + upper);

            if (upper - lower > EPSILON) {
                PUSH_TASK(last_task, middle, upper);
                PUSH_TASK(last_task, lower, middle);
            } else {
                size_t i;
                for (i = 0; i < roots_count; ++i) {
                    lambda[lambda_count++] = middle;
                }
            }
        }
    }
#ifdef DEBUG_LAMBDA_COUNT
    printf("rank:%4d --- lambda count:%4u\n", rank, lambda_count);
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

    if (rank == HOST) {
        int node;
        for (node = 1; node < size; ++node) {
            MPI_Status status;
            MPI_Recv(lambda + lambda_count, n - lambda_count, MPI_DOUBLE, node, TAG_LAMBDA, MPI_COMM_WORLD, &status);
            int received;
            MPI_Get_count(&status, MPI_DOUBLE, &received);
            lambda_count += received;
        }
        assert(lambda_count == n);
    } else {
        MPI_Send(lambda, lambda_count, MPI_DOUBLE, HOST, TAG_LAMBDA, MPI_COMM_WORLD);
    }
}

void main_entry(double storage[], const size_t n, const int rank, const int size) {
    const size_t a_length = n * (n + 1) / 2;

    double *const a = storage;
    double *const x = a + a_length;
    double *const y = x + (n - 1);

    // Let host process to generate matrix.
    if (rank == HOST) {
        generate_symmetric_matrix(a, n);
    }
#ifdef DEBUG_MATRIX
    if (rank == HOST) {
        print_symmetric_matrix(a, n, rank);
    }
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

    const double start = MPI_Wtime();

    // Make everybody know the contents of the matrix.
    if (size > 1) {
        MPI_Bcast(a, a_length, MPI_DOUBLE, HOST, MPI_COMM_WORLD);
    }

    tridiagonalization(a, x, y, n, rank, size);

    const double infinite_norm = calc_infinite_norm(a, n, rank, size);
#ifdef DEBUG_INFINITE_NORM
    printf("rank:%4d --- infinite norm:%*.*lf\n", rank, FIELD_WIDTH, PRECISION, infinite_norm);
    if (size > 1) {
        MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

    double *const lambda = a + a_length;
    bisection(lambda, a, n, -infinite_norm, infinite_norm, rank, size);

    const double elapsed = MPI_Wtime() - start;

    if (rank == HOST) {
#ifdef DEBUG_LAMBDA
        print_vector(lambda, n, rank);
#endif
        printf("size:%4d\n", size);
        printf("elapsed time: %.3lf\n", elapsed);
    }
}

int main(int argc, char **argv) {
    unsigned n = DEFAULT_SIZE;
    if (argc > 1) {
        sscanf(argv[1], "%u", &n);
        --argc, ++argv;
    }

    srand(41);
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Make everybody know the size of the matrix.
    if (size > 1) {
        MPI_Bcast(&n, 1, MPI_UNSIGNED, HOST, MPI_COMM_WORLD);
    }

    // Allocate space for the whole matrix and helping vectors.
    const size_t storage_length = n * (n + 1) / 2 + 2 * (n - 1);
    double *const storage = malloc(storage_length * sizeof(*storage));
    assert(storage != NULL);

    main_entry(storage, n, rank, size);

    // Clean up.
    free(storage);

    MPI_Finalize();
    return (EXIT_SUCCESS);
}
