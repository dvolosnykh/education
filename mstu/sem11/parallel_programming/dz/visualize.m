function visualize(filename)
	data = load(filename);

	nodes = data(:, 1);
	parallel(nodes) = data(:, 2);
	sequential(nodes) = parallel(1);
	boost = sequential ./ parallel;

	subplot(2, 1, 1);
	plot(nodes, sequential, '-xb', nodes, parallel, '-xr');
	grid on;
	title('Time measurements of sequential and parallel algorithms (in seconds).');
	legend('sequential', 'parallel');

	subplot(2, 1, 2);
	plot(nodes, boost, '-xb');
	grid on;
	title('Boost ratio.');
end
