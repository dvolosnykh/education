#!/usr/bin/awk -f

BEGIN {FS = ":"}
/size/ {size = $2}
/elapsed/ {elapsed = $2}
END {
	print size, elapsed
}
