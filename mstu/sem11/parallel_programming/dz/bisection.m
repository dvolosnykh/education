function bisection
	n = 10;
	a = generate_matrix(n);
	a
	det(a)
	lambda = sort(eig(a)')

	save_a = a;

	disp('===================================================');

	if false
		a = tridiagonalization(a, n)
		det(a)
		lambda = sort(eig(a)')
	end

	if false
		disp('---------------------------------------------------');
		a = save_a;
		[housv, beta, zer] = housh(a(1, 1 : end), 2, 0)
		housv = housv / norm(housv)
		beta = 2;
		u = eye(n) - beta * housv' * housv;
		a = u * a * u
		det(a)
		lambda = sort(eig(a)')
	end

	if true
		disp('---------------------------------------------------');
		a = save_a;
		[dummy, a] = hess(a);
		a
		det(a)
		lambda = sort(eig(a)')
	end

	infinite_norm = norm(a, Inf)
	count = sign_change_count(a, n, infinite_norm)
end

function count = sign_change_count(a, n, lambda)
	l = zeros(1, n);
	count = 0;


	if true
		l(1)  = a(1, 1) - lambda;
		if xor(1 > 0, l(1) > 0)
			count = count + 1;
		end
		for i = 2 : n;
			l(i) = (a(i, i) - lambda) - a(i - 1, i) ^ 2 / l(i - 1);
			if xor(l(i) > 0, l(i - 1) > 0)
				count = count + 1;
			end
		end
	elseif false
		[ll, u, p] = lu(a - lambda * eye(n));
		u
		ll = diag(u)';

		l(1) = ll(1);
		if xor(1 > 0, l(1) > 0)
			count = count + 1;
		end
		for i = 2 : n;
			l(i) = prod(ll(1 : i));
			if xor(l(i) > 0, l(i - 1) > 0)
				count = count + 1;
			end
		end
	else
		l(1) = a(1, 1);
		if xor(1 > 0, l(1) > 0)
			count = count + 1;
		end
		for i = 2 : n;
			l(i) = det(a(1 : i, 1 : i) - lambda * eye(i));
			if xor(l(i) > 0, l(i - 1) > 0)
				count = count + 1;
			end
		end
	end

	l
end

function a = tridiagonalization(a, n)
	for k = 1 : n - 2;
		if false
			x = a(k, k + 1 : end);
			norm_a = norm(x)
			x(1) = x(1) - norm_a;
			x = x / norm(x);
			y = 2 * (a(k + 1 : end, k + 1 : end) * x');
			y = y - (x * y) * x';
			a(k + 1 : end, k + 1 : end) = a(k + 1 : end, k + 1 : end) - x' * y' - y * x;
			a(k + 1 : end, k) = a(k, k + 1 : end) = [norm_a, zeros(1, n - k - 1)];
		else
			x = [zeros(1, k), a(k, k + 1 : end)];
			x(k + 1) = x(k + 1) - norm(x);
			x = sign(x(k + 1)) * x / norm(x);
			y = 2 * (a * x');
			y = y - (x * y) * x';
			a = a - x' * y' - y * x;
%		u = eye(n) - 2 * x' * x;
%		a = u * a * u';
		end
	end

%	a(abs(a) < 0.000001) = 0;
end

function a = generate_matrix(n)
	a = zeros(n);

	for i = 1 : size(a, 1); j = i : size(a, 2);
		a(j, i) = a(i, j) = (i - 1) + (j - 1) / 10;
	end
end
