#define ROUTERS_NUM	4
#define NETWORKS_NUM	5

#include "network.pml"


// Networks' IDs.
#define N0	0
#define N1	1
#define N2	2
#define N3	3
#define N4	4

// Routers' IDs.
#define R0	0
#define R1	1
#define R2	2
#define R3	3


init {
	// Set up topology.
	connect(R0, N0);
	connect(R0, N1);
	connect(R1, N1);
	connect(R1, N2);
	connect(R1, N3);
	connect(R2, N2);
	connect(R2, N4);
	connect(R3, N3);
	connect(R3, N4);

	run start();
}
