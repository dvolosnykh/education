#define ROUTERS_NUM	3
#define NETWORKS_NUM	3

#include "network.pml"


// Networks' IDs.
#define N0	0
#define N1	1
#define N2	2

// Routers' IDs.
#define R0	0
#define R1	1
#define R2	2


init {
	// Set up topology.
	connect(R0, N0);
	connect(R0, N1);
	connect(R1, N1);
	connect(R1, N2);
	connect(R2, N2);

	run start();
}
