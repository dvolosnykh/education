#!/bin/bash

if [[ $# -lt 1 ]]; then
        echo "[ERROR] Name of the model is not specified."
        exit 1
fi

spin -Tpglrs $1.pml > log.txt
less log.txt
