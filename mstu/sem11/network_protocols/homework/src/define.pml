/**
 * Number of routers in the system.
 */
#ifndef ROUTERS_NUM
#error ROUTERS_NUM is undefined.
#endif

/**
 * Number of networks in the system.
 */
#ifndef NETWORKS_NUM
#error NETWORKS_NUM is undefined.
#endif

/**
 * Queues' buffer length (either for router's queue or network's media).
 * Default value is 20.
 */
#ifndef QUEUE_LEN
#define QUEUE_LEN       20
#endif

/**
 * Boolean macro: whether the router should process messages atomically.
 * This is useful for debugging purposes. This option is enabled by default.
 */
#ifndef PROCESS_MESSAGE_ATOMICALLY
#define PROCESS_MESSAGE_ATOMICALLY 1
#endif


/**
 * Special value for the network media message's "destination" field.
 * Schedules the message to be broadcasted to every node over the network.
 */
#define BROADCAST       -1

/**
 * Infinite metric.
 */
#define INFINITY	16

/**
 * No updates limit.
 */
#define NO_UPDATES_LIMIT	100
