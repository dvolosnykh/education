#include "define.pml"

/**
 * Router entity.
 */
typedef Router {
	/**
	 * List of networks which the router is connected to.
	 */
	chan networks = [NETWORKS_NUM] of {int};
	/**
	 * Queue of incoming events that are to modify RIP table.
	 * Format:
	 *	int from	--- Sender of the message.
	 *	int mask	--- Mask of the sender's network (which is effectively
	 *			    network's id).
	 *	int address	--- Address from RIP table entry.
	 *	byte metric	--- Metric from RIP table entry.
	 * NOTE: If we are to model UDP then we'd better have the "destination" field also.
	 * But it isn't actually used, so we can drop it.
	 */
	chan queue = [QUEUE_LEN] of {int, int, int, byte};
	/**
	 * RIP table entries of the reachable networks. Format:
	 * 	int address	--- Address of the destination network.
	 * 	int router	--- The first router along the route to the destination.
	 * 	byte metric	--- A number, indicating the distance to the destination.
	 *	bool update	--- Recent update flag.
	 */
	chan entries = [NETWORKS_NUM] of {int, int, byte, bool};
	chan lock = [0] of {bool};	/*< Lock for 'entries'. */
	bool running;	/*< If the router is still in action. */
}

/**
 * Network entity.
 */
typedef Network {
	/**
	 * List of routers that belong to this network.
	 */
	chan routers = [ROUTERS_NUM] of {int};
	/**
	 * Media for transmission of messages over this network. Format:
	 *	int from	--- Sender of the message.
	 *	int to		--- Destination of the messages. Broadcasting messages
	 *			    should set this field to BROADCAST.
	 *	int address	--- Address from RIP table entry.
	 *	byte metric	--- Metric from RIP table entry.
	 */
	chan media = [0] of {int, int, int, byte};
	bool running;	/*< If the network is still in action. */
}


Router routers[ROUTERS_NUM]	/*< Array of router entities. */
Network networks[NETWORKS_NUM]	/*< Array of network entities. */


/**
 * Macro for consistent connection of the router \arg rid to the network \arg nid.
 */
#define connect(rid, nid)		\
	routers[rid].networks ! nid;	\
	networks[nid].routers ! rid



/**
 * RIP router's client side.
 * Broadcasts the whole RIP table to directly accessible networks.
 * Also simulates invalidation of entries on timeouts.
 */
proctype RIP_router_client(int my_rid) {
	// Client's main loop.
	int entryIndex, entryCount;
	int networkIndex, networkCount;
	int no_updates_count = 0;
	bool no_updates;
end:	do
	:: no_updates_count < NO_UPDATES_LIMIT ->
//progress:
		no_updates = true;

		// Lock RIP table.
		routers[my_rid].lock ? true;

		// Iterate over the set of entries in the RIP table.
		entryIndex = 0;
		entryCount = len(routers[my_rid].entries);
		do
		:: entryIndex < entryCount ->
			int router, address; byte metric; bool update;
			// Get next entry from the RIP table.
			routers[my_rid].entries ? address, router, metric, update;

			// Simulate timeout of the entry.
			if
//			:: metric = INFINITY -> update = true;
			:: skip;
			fi;

			routers[my_rid].entries ! address, router, metric, false;

			// Send entry if it was recently updated.
			if
			:: update ->
				// Iterate over the set of directly accessible networks.
				networkIndex = 0;
				networkCount = len(routers[my_rid].networks);
				do
				:: networkIndex < networkCount ->
					// Get next network id from the `list`.
					int nid;
					routers[my_rid].networks ? nid;
					routers[my_rid].networks ! nid;

					// Broadcast entry data, but only if it is not
					// related to the network to which we are about to
					// send data. No need to spam over some network
					// about it's own reachability.
					if
					:: address != nid ->
						networks[nid].media ! my_rid, BROADCAST, address, metric;
						// Debug output.
						printf("\n\nR%d over N%d : N%d is %d hops away.", my_rid, nid, address, metric);
					:: else -> skip;
					fi;

					networkIndex++;
				:: else -> break;
				od;

				no_updates = false;
			:: else -> skip;
			fi;

			entryIndex++;
		:: else -> break;
		od;

		// Unlock RIP table.
		routers[my_rid].lock ! false;

		if
		:: no_updates -> no_updates_count++;
		:: else -> no_updates_count = 0;
		fi;

	:: else -> break;
	od;

	printf("\nR%d (client) has terminated.", my_rid);
}

/**
 * RIP router's server side.
 * Receives routing updates from its neighbours.
 */
proctype RIP_router_server(int my_rid) {
	int networkIndex, networkCount;
	// Lock RIP table.
	routers[my_rid].lock ? true;
	// Add entries for router's directly accessible networks.
	// Iterate over the set of networks this router is connected to.
	networkIndex = 0;
	networkCount = len(routers[my_rid].networks);
	do
	:: networkIndex < networkCount ->
		// Get next network id from the `list`.
		int nid;
		routers[my_rid].networks ? nid;
		routers[my_rid].networks ! nid;

		// Add entry for current network.
		routers[my_rid].entries !! nid, my_rid, 0, true;

		networkIndex++;
	:: else -> break;
	od;
	// Unlock RIP table.
	routers[my_rid].lock ! false;

	// Router's main working loop.
	int router, mask, address; byte metric; bool update;
end:	do
	// Process next routing update.
	::
#if PROCESS_MESSAGE_ATOMICALLY > 0
	atomic {
#endif
	routers[my_rid].queue ? router, mask, address, metric ->
//progress:
		// Lock RIP table.
		routers[my_rid].lock ? true;

		int old_router; byte old_metric; bool old_update;
		if
		// Ignore self-messages.
		:: router != my_rid ->
			printf("\n\nR%d <-- R%d over N%d :\tN%d is %d hops away.",
				my_rid, router, mask, address, metric);

			// Add cost of the network to the metric (which is always equal
			// to 1).
			if
			:: metric < INFINITY -> metric++;
			:: else -> skip;
			fi;

			// Ignore metrics for the set of directly accessible networks.
			if
			:: routers[my_rid].networks ?? [eval(address)] ->
				skip;

				// Debug output.
				printf("\n\tN%d is directly accessible. Ignoring.", address);

			// If there's entry for this network address, then compare it with
			// new routing update.
			// NOTE: We don't need "atomic" here, because RIP table entries are
			// considered to be private to the owner router.
			:: else -> if
			:: routers[my_rid].entries ?? [eval(address), old_router, old_metric, old_update] ->
				routers[my_rid].entries ?? eval(address), old_router, old_metric, old_update;

				// * If the new metric is from the router it has previously
				//   came, then update the metric in any case.
				// * If the new metric is better than the old one, then
				//   update it.
				// * If the new metric is worse than the old one, then
				//   ignore it.
				// Update entry if needed.
				if
				:: router == old_router || metric < old_metric ->
					routers[my_rid].entries !! address, router, metric, true;
				:: else ->
					routers[my_rid].entries !! address, old_router, old_metric, old_update;
				fi;

				// Debug output.
				if
				:: router == old_router ->
					printf("\n\tFrom old R%d. Updating.", old_router);
				:: else -> if
				:: metric < old_metric ->
					printf("\n\tBetter metric: %d < %d. Updating.",
						metric, old_metric);
				:: else ->
					printf("\n\tWorse metric: %d >= %d. Ignoring.",
						metric, old_metric);
				fi; fi;

			// If there's no entry for this network address, then merely add it.
			:: else ->
				routers[my_rid].entries !! address, router, metric, true;

				// Debug output.
				printf("\n\tDiscovered N%d through R%d. Adding.", address, router);
			fi; fi;
		:: else -> skip;
		fi;

		// Unlock RIP table.
		routers[my_rid].lock ! false;
#if PROCESS_MESSAGE_ATOMICALLY > 0
	}
#endif

	:: timeout -> break;
	od;

	routers[my_rid].running = false;

	printf("\nR%d (server) has terminated.", my_rid);
}

/**
 * RIP router's behaviour.
 * Merely starts its server and client parts. Then just keeps track of its table lock.
 */
proctype RIP_router(int my_rid) {
	byte count = 1;

	routers[my_rid].running = true;

	run RIP_router_server(my_rid);
	run RIP_router_client(my_rid);
	
end:	do
	:: count == 0 -> routers[my_rid].lock ? false; count++;
	:: else -> routers[my_rid].lock ! true; count--;
	od unless !routers[my_rid].running;

	printf("\nR%d has terminated.", my_rid);
}


/**
 * Network's behaviour.
 * We retransmit messages over the network's media, exploding the broadcasted ones.
 */
proctype network_segment(int my_nid) {
	networks[my_nid].running = true;

	// Main network's working loop.
	int from, to, address; byte metric;
	int routerIndex, routerCount;
end:	do
	// May be transmission of the packet is handled.
	:: networks[my_nid].media ? from, to, address, metric ->
		// Iterate over the set of routers connected to this network.
		routerIndex = 0;
		routerCount = len(networks[my_nid].routers);

		do
		:: routerIndex < routerCount ->
			// Get next router id from the `list`.
			int rid;
			networks[my_nid].routers ? rid;
			networks[my_nid].routers ! rid;

			// Send the message to the current router if it is the desired
			// destination or the message was marked as a broadcast one.
			if
			:: to == BROADCAST || to == rid ->
				// Simulate loss of packets at destination.
				if
				:: routers[rid].queue ! from, my_nid, address, metric;
//				:: skip;
				fi;
			:: else -> skip;
			fi;

			routerIndex++;
		:: else -> break;
		od;

	// or the packet may have lost at its source.
//	:: networks[my_nid].media ? from, to, address, metric -> skip;

	:: timeout -> break;	
	od;

	networks[my_nid].running = false;

	printf("\nN%d has terminated.", my_nid);
}

/**
 * Launches the model of the network.
 */
proctype start() {
	int index;

	// Iterate over all routers.
	index = 0;
	do
	:: index < ROUTERS_NUM ->
		run RIP_router(index);
		index++;
	:: else -> break;
	od;

	// Iterate over all networks.
	index = 0;
	do
	:: index < NETWORKS_NUM ->
		run network_segment(index);
		index++;
	:: else -> break;
	od;
}
