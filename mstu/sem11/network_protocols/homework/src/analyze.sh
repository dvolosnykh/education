#!/bin/bash

if [[ $# -lt 1 ]]; then
        echo "[ERROR] Name of the model is not specified."
        exit 1
fi

if true; then
	spin -a $1.pml && \
	gcc -DVECTORSZ=16384 -DSAFETY -DNOREDUCE -o $1 pan.c && \
	./$1
else
	spin -a $1.pml && \
	gcc -DVECTORSZ=16384 -DNP -o $1 pan.c && \
	./$1 -l
fi
