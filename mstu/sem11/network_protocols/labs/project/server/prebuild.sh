#!/bin/bash

if [[ $# -lt 1 ]]; then
        echo "usage: $0 build_dir [build_type]"
        exit 1
fi

declare -r BUILD_DIR="$1" BUILD_TYPE=${2:-Debug}

declare -r LIBRARY_DIR="server/lib"
declare -r SOURCE_DIR="../server"

declare -r PCRE_TAR="pcre-8.20.tar.bz2"
declare -r CUNIT_DIR="CUnit-2.1-2"
declare -r CUNIT_TAR="$CUNIT_DIR-src.tar.bz2"

declare -r TOP_DIR="$PWD"

mkdir "$BUILD_DIR" && \
cd $LIBRARY_DIR && \
tar -xjf $PCRE_TAR && \
tar -xjf $CUNIT_TAR && \
cd $CUNIT_DIR && \
./configure --prefix "$TOP_DIR/$BUILD_DIR/lib/$CUNIT_DIR" && make && make install && \
cd "$TOP_DIR/$BUILD_DIR" && \
cmake -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DSERVER_BUILD_TESTS=ON $SOURCE_DIR

