#!/bin/bash

if [[ $# -lt 1 ]]; then
        echo "usage: $0 tex_dir"
        exit 1
fi

declare -r TEX_DIR="$1"

make test_units 2>&1 | tee "${TEX_DIR}/test_units.log"
make test_system 2>&1 | tee "${TEX_DIR}/test_system.log"
make test_memory 2>&1 | tee "${TEX_DIR}/test_memory.log"
