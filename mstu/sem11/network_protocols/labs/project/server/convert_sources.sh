#!/bin/bash

if [[ $# -lt 1 ]]; then
        echo "usage: $0 src_dir"
        exit 1
fi

declare -r SRC_DIR="$1"

for file in "$SRC_DIR"/*; do
	if [ -f "$file" ]; then
		iconv -f=UTF-8 -t=KOI8-R -o "$file" "$file"
	fi
done
