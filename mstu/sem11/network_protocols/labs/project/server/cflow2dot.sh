#!/bin/bash

if [[ $# -lt 4 ]]; then
        echo "usage: $0 src_file cflow2dot_path cflow.ignore output_file"
        exit 1
fi

declare -r SRC_FILE="$1" CFLOW2DOT="$2" CFLOW_IGNORE="$3" OUTPUT_FILE="$4" 

cflow --level "0= " "$SRC_FILE" | grep -vf "$CFLOW_IGNORE" | "$CFLOW2DOT" > "$OUTPUT_FILE"
