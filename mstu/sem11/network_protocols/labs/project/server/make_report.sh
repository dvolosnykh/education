#!/bin/bash

if [[ $# -lt 1 ]]; then
        echo "usage: $0 main_tex"
        exit 1
fi

declare -r MAIN_TEX="$1"


pdflatex -interaction=nonstopmode ${MAIN_TEX}.tex
bibtex ${MAIN_TEX}
pdflatex -interaction=nonstopmode ${MAIN_TEX}.tex
pdflatex -interaction=nonstopmode ${MAIN_TEX}.tex
exit 0
