#ifndef COMMANDS_RE_H
#define COMMANDS_RE_H

/*!
 *  \file commands_re.h
 *  \brief Строковые литералы регулярных выражений команд SMTP-протокола
 *      (см. <a href="http://www.faqs.org/rfcs/rfc2821.html">RFC 2821</a>),
 *      а также вспомогательных лексем
 *      (см. <a href="http://www.faqs.org/rfcs/rfc822.html">RFC 822</a>).
 */

/*!
 *  \defgroup re_top Регулярные выражения
 *  @{
 */

/*!
 *  \defgroup re_lexical_tokens Регулярные выражения вспомогательных лексем
 *  Регулярные выражения вспомогательных лексем основаны на документе
 *  <a href="http://www.faqs.org/rfcs/rfc822.html">RFC 822</a> (см. раздел
 *  3.3. LEXICAL TOKENS).
 *  @{
 */

#define SPECIALS        "()<>@,;:\\\".[\\]"
#define ATOM            "[^" SPECIALS " [:cntrl:]]+"
#define QUOTED_TEXT     "[^\\r\\\"]" "|" "(?:\\r\\n\\h)+"
#define QUOTED_PAIR     "\\."
#define QUOTED_STRING   "\"" "(?:" QUOTED_TEXT "|" QUOTED_PAIR ")*" "\""
#define WORD            ATOM "|" QUOTED_STRING

/*! @} */   // re_lexical_tokens


/*!
 *  \defgroup re_command_argument_syntax Регулярные выражения аргументов команд
 *  Регулярные выражения аргументов команд SMTP-протокола основаны на документе
 *  <a href="http://www.faqs.org/rfcs/rfc2821.html">RFC 2821</a> (см. раздел
 *  4.1.2 Command Argument Syntax).
 *  @{
 */

#define STRING          WORD
#define DOT_STRING      ATOM "(?:" "\\." ATOM ")*"
#define LOCAL_PART      "(?:" DOT_STRING "|" QUOTED_STRING ")"

#define IPV4_NUM        "(?:[01]?[0-9]?[0-9]|2(?:[0-4][0-9]|5[0-5]))"
#define IPV4            IPV4_NUM "(?:" "\\." IPV4_NUM "){3}"

#define IPV6_HEX        "[[:xdigit:]]{1,4}"
#define IPV6_HEX_REPEAT(repeat)     IPV6_HEX "(?:" ":" IPV6_HEX "){" repeat "}"
#define IPV6_FULL       IPV6_HEX_REPEAT("7")
#define IPV6_COMP       "(?:" IPV6_HEX_REPEAT("0,5") ")?" "::" "(?:" IPV6_HEX_REPEAT("0,5") ")?"
#define IPV6V4_FULL     IPV6_HEX_REPEAT("5") ":" IPV4
#define IPV6V4_COMP     "(?:" IPV6_HEX_REPEAT("0,3") ")?" "::" "(?:" IPV6_HEX_REPEAT("0,3") ":" ")?" IPV4
#define IPV6            "IPv6:" "(?:" IPV6_FULL "|" IPV6_COMP "|" IPV6V4_FULL "|" IPV6V4_COMP ")"

#define ADDRESS_LITERAL "\\[" "(?:" IPV4 "|" IPV6 ")" "\\]"

#define SUB_DOMAIN      "[[:alnum:]](?:[[:alnum:]-]*[[:alnum:]])?"
#define DOMAIN          "(?:" SUB_DOMAIN "(?:" "\\." SUB_DOMAIN ")+" "|" ADDRESS_LITERAL ")"

#define AT_DOMAIN       "@" DOMAIN
#define AT_DOMAIN_LIST  AT_DOMAIN "(?:\\h*,\\h*" AT_DOMAIN ")*"

#define EMAIL           "\\A" LOCAL_PART "(@)" DOMAIN "\\z"
#define DUMMY_EMAIL     "(?<email>.*)"
#define PATH            "\\h*(?:" AT_DOMAIN_LIST "\\h*:\\h*)?" DUMMY_EMAIL
#define REVERSE_PATH    PATH
#define FORWARD_PATH    PATH
#define POSTMASTER      "(?i:postmaster)"

#define KEYWORD         "[[:alnum:]](?:[[:alnum:]-])*"
#define VALUE           "[^ =[:cntrl:]]+"
#define PARAMETER       "\\h+" KEYWORD "(?:=" VALUE ")?"
#define GET_PARAMETER   "\\A\\h+(" KEYWORD ")(?:=(" VALUE "))?"
#define PARAMETERS      PARAMETER "(?:" PARAMETER ")*"
#define MAIL_PARAMETERS PARAMETERS
#define RCPT_PARAMETERS PARAMETERS

/*! @} */   // re_command_argument_syntax


/*!
 *  \defgroup re_command_syntax Регулярные выражения команд
 *  Регулярные выражения команд SMTP-протокола основаны на документе
 *  <a href="http://www.faqs.org/rfcs/rfc2821.html">RFC 2821</a> (см. раздел
 *  4.1.1 Command Semantics and Syntax).
 *  @{
 */

#define SMTP_CMD_STR(name)      "\\A\\h*(?i:" name ")"
#define SMTP_END_CMD_STR        "\\h*\\z"

// HELO
#define SMTP_CMD_HELO_STR       SMTP_CMD_STR("HELO")
#define SMTP_ARGS_HELO_STR      "\\A\\h+(" DOMAIN ")" SMTP_END_CMD_STR
#define SMTP_PARAMS_HELO_STR    NULL

// EHLO
#define SMTP_CMD_EHLO_STR       SMTP_CMD_STR("EHLO")
#define SMTP_ARGS_EHLO_STR      "\\A\\h+(" DOMAIN ")" SMTP_END_CMD_STR
#define SMTP_PARAMS_EHLO_STR    NULL

// MAIL
#define SMTP_CMD_MAIL_STR       SMTP_CMD_STR("MAIL")
#define SMTP_ARGS_MAIL_STR      "\\A\\h+(?i:FROM):<(?:" REVERSE_PATH ")?>"
#define SMTP_PARAMS_MAIL_STR    "\\A(" MAIL_PARAMETERS ")?" SMTP_END_CMD_STR

// RCPT
#define SMTP_CMD_RCPT_STR       SMTP_CMD_STR("RCPT")
#define SMTP_ARGS_RCPT_STR      "\\A\\h+(?i:TO):<(?:" "(" POSTMASTER ")" "|" FORWARD_PATH ")>"
#define SMTP_PARAMS_RCPT_STR    "\\A(" RCPT_PARAMETERS ")?" SMTP_END_CMD_STR

// DATA
#define SMTP_CMD_DATA_STR       SMTP_CMD_STR("DATA")
#define SMTP_ARGS_DATA_STR      "\\A(?:\\h+(" STRING "))?" SMTP_END_CMD_STR
#define SMTP_PARAMS_DATA_STR    NULL

// RSET
#define SMTP_CMD_RSET_STR       SMTP_CMD_STR("RSET")
#define SMTP_ARGS_RSET_STR      "\\A(?:\\h+(" STRING "))?" SMTP_END_CMD_STR
#define SMTP_PARAMS_RSET_STR    NULL

// VRFY
#define SMTP_CMD_VRFY_STR       SMTP_CMD_STR("VRFY")
#define SMTP_ARGS_VRFY_STR      "\\A\\h+" DUMMY_EMAIL SMTP_END_CMD_STR
#define SMTP_PARAMS_VRFY_STR    NULL

// EXPN
#define SMTP_CMD_EXPN_STR       SMTP_CMD_STR("EXPN")
#define SMTP_ARGS_EXPN_STR      "\\A\\h+(" STRING ")" SMTP_END_CMD_STR
#define SMTP_PARAMS_EXPN_STR    NULL

// HELP
#define SMTP_CMD_HELP_STR       SMTP_CMD_STR("HELP")
#define SMTP_ARGS_HELP_STR      "\\A(?:\\h+(" STRING "))?" SMTP_END_CMD_STR
#define SMTP_PARAMS_HELP_STR    NULL

// NOOP
#define SMTP_CMD_NOOP_STR       SMTP_CMD_STR("NOOP")
#define SMTP_ARGS_NOOP_STR      "\\A(?:\\h+" STRING ")?" SMTP_END_CMD_STR
#define SMTP_PARAMS_NOOP_STR    NULL

// QUIT
#define SMTP_CMD_QUIT_STR       SMTP_CMD_STR("QUIT")
#define SMTP_ARGS_QUIT_STR      "\\A(?:\\h+(" STRING "))?" SMTP_END_CMD_STR
#define SMTP_PARAMS_QUIT_STR    NULL

/*! @} */   // re_command_syntax

/*! @} */   // re_top

#endif
