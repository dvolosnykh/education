#ifndef REPLIES_H
#define REPLIES_H

/*!
 *  \file replies.h
 *  \brief В данном файле содержатся определения кодов и сообщений ответов
 *      SMTP-сервера.
 */

/*!
 *  \defgroup reply_top Ответы SMTP-сервера
 *  @{
 */

/*!
 *  \defgroup reply_codes Коды ответов
 *  @{
 */

#define REPLY_CODE_SYSTEM_STATUS                            "211"
#define REPLY_CODE_HELP_MESSAGE                             "214"
#define REPLY_CODE_SERVICE_READY                            "220"
#define REPLY_CODE_SERVICE_CLOSING_TRANSMISSION_CHANNEL     "221"
#define REPLY_CODE_REQUESTED_MAIL_ACTION_OKAY_COMPLETED     "250"
#define REPLY_CODE_USER_NOT_LOCAL_WILL_FORWARD              "251"
#define REPLY_CODE_CANNOT_VRFY_USER_BUT_WILL_ACCEPT_MESSAGE "252"
#define REPLY_CODE_START_MAIL_INPUT                         "354"
#define REPLY_CODE_SERVICE_NOT_AVAILABLE                    "421"
#define REPLY_CODE_MAILBOX_TEMPORARILY_UNAVAILABLE          "450"
#define REPLY_CODE_LOCAL_ERROR_IN_PROCESSING                "451"
#define REPLY_CODE_INSUFFICIENT_SYSTEM_STORAGE              "452"
#define REPLY_CODE_SYNTAX_ERROR_COMMAND_UNRECOGNIZED        "500"
#define REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS  "501"
#define REPLY_CODE_COMMAND_NOT_IMPLEMENTED                  "502"
#define REPLY_CODE_BAD_SEQUENCE_OF_COMMANDS                 "503"
#define REPLY_CODE_COMMAND_PARAMETER_NOT_IMPLEMENTED        "504"
#define REPLY_CODE_MAILBOX_PERMANENTLY_UNAVAILABLE          "550"
#define REPLY_CODE_USER_NOT_LOCAL_PLEASE_TRY_FORWARD        "551"
#define REPLY_CODE_EXCEEDED_STORAGE_ALLOCATION              "552"
#define REPLY_CODE_MAILBOX_NAME_NOT_ALLOWED                 "553"
#define REPLY_CODE_TRANSACTION_FAILED                       "554"

#define IS_POSITIVE_COMPLETION(reply_code)              (reply_code[0] == '2')
#define IS_POSITIVE_INTERMEDIATE(reply_code)            (reply_code[0] == '3')
#define IS_TRANSIENT_NEGATIVE_COMPLETION(reply_code)    (reply_code[0] == '4')
#define IS_PERMANENT_NEGATIVE_COMPLETION(reply_code)    (reply_code[0] == '5')

#define IS_POSITIVE(reply_code) (IS_POSITIVE_COMPLETION(reply_code) || \
                                 IS_POSITIVE_INTERMEDIATE(reply_code))
#define IS_NEGATIVE(reply_code) (IS_TRANSIENT_NEGATIVE_COMPLETION(reply_code) || \
                                 IS_PERMANENT_NEGATIVE_COMPLETION(reply_code))

/*! @} */ // reply_codes

/*!
 *  \defgroup reply_messages Сообщения ответов
 *  @{
 */

#define REPLY_MSG(text) "%.3s%c" text

#define REPLY_MSG_COMMAND_UNRECOGNIZED          REPLY_MSG("Command unrecognized")
#define REPLY_MSG_ERROR_IN_ARGUMENTS            REPLY_MSG("Syntax error in arguments")
#define REPLY_MSG_ERROR_IN_PARAMETERS           REPLY_MSG("Syntax error in parameters")
#define REPLY_MSG_COMMAND_NOT_IMPLEMENTED       REPLY_MSG("Command not implemented")
#define REPLY_MSG_BAD_SEQUENCE_OF_COMMANDS      REPLY_MSG("Bad sequence of commands")
#define REPLY_MSG_PARAMETER_NOT_IMPLEMENTED     REPLY_MSG("Command parameter not implemented")
#define REPLY_MSG_ARGUMENT_NOT_SUPPORTED        REPLY_MSG("Command argument not supported")
#define REPLY_MSG_PARAMETER_MUST_HAVE_VALUE     REPLY_MSG("Parameter '%.*s' must have value")
#define REPLY_MSG_PARAMETER_MUST_NOT_HAVE_VALUE REPLY_MSG("Parameter '%.*s' must have value")
#define REPLY_MSG_PARAMETER_HAS_WRONG_VALUE     REPLY_MSG("Parameter '%.*s' has wrong value")

#define REPLY_MSG_WELCOME           REPLY_MSG("%s SMTP service ready")
#define REPLY_MSG_GREETING          REPLY_MSG("%s is on the air")
#define REPLY_MSG_GOODBYE           REPLY_MSG("Goodbye")
#define REPLY_MSG_NO_SMTP_SERVICE   REPLY_MSG("No SMTP service here")

/*!
 *  \defgroup reply_extensions Сообщения переговорной стадии о поддерживаемых расширениях
 *  @{
 */

#define REPLY_MSG_8BITMIME  REPLY_MSG("8BITMIME")
#define REPLY_MSG_SIZE      REPLY_MSG("SIZE %d")
#define REPLY_MSG_EXPN      REPLY_MSG("EXPN")
#define REPLY_MSG_HELP      REPLY_MSG("HELP")

/*! @} */ // reply_extensions

#define REPLY_MSG_OK                        REPLY_MSG("OK")
#define REPLY_MSG_INSUFFICIENT_STORAGE      REPLY_MSG("Insufficient system storage")
#define REPLY_MSG_NO_POSTMASTER_MAILBOX     REPLY_MSG("Postmaster mailbox is not supported")
#define REPLY_MSG_START_MAIL_INPUT          REPLY_MSG("Start mail input; end with <CRLF>.<CRLF>")
#define REPLY_MSG_TRANSACTION_FAILED        REPLY_MSG("Transaction failed")
#define REPLY_MSG_NO_VALID_RECIPIENTS       REPLY_MSG("No valid recipients")
#define REPLY_MSG_TOO_MUCH_DATA             REPLY_MSG("Too much data")
#define REPLY_MSG_MAILBOX_NAME_NOT_ALLOWED  REPLY_MSG("Mailbox name not allowed")
#define REPLY_MSG_CANNOT_VRFY_USER          REPLY_MSG("Cannot VRFY user, but will accept message")

/*
 * Multiple/single line markers.
 */
#define REPLY_MORE      '-'
#define REPLY_LAST      ' '
#define REPLY_SINGLE    REPLY_LAST

/*! @} */ // reply_messages

/*! @} */ // reply_top

#endif
