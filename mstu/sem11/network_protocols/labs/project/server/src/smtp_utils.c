#include "smtp_utils.h"
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>


#define TIMESTAMP_FORMAT    "%d %b %Y %T %z"


int make_timestamp(char buffer[], const size_t max,
                   const char remote_host[], const char address[],
                   const char this_domain[], const time_t timestamp)
{
    int length = snprintf(buffer, max, "Received: from %s (%s) by %s with SMTP; ",
                          remote_host, address, this_domain);
    if (length >= max) {
        length = -1;
        errno = ENOMEM;
        goto do_return;
    }

    struct tm local;
    const struct tm *const static_local = localtime_r(&timestamp, &local);
    if (static_local == NULL) {
        length = -1;
        goto do_return;
    }

    const size_t count = strftime(buffer + length, max - length, TIMESTAMP_FORMAT, &local);
    if (count == 0) {
        length = -1;
        errno = ENOMEM;
        goto do_return;
    }
    length += count;

    if (length + CRLF_LEN >= max) {
        length = -1;
        errno = ENOMEM;
        goto do_return;
    }

    strcpy(buffer + length, CRLF);
    length += CRLF_LEN;

do_return:
    return (length);
}

int make_return_path(char buffer[], const size_t max, const char mailbox[])
{
    int length = snprintf(buffer, max, "Return-Path: <%s>" CRLF, mailbox);
    if (length >= max) {
        length = -1;
        errno = ENOMEM;
        goto do_return;
    }

do_return:
    return (length);
}
