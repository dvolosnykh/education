#ifndef COMMANDS_H
#define COMMANDS_H

/*!
 *  \file commands.h
 *  \brief Структуры данных команд SMTP-протокола и функции их обработки.
 *
 *  В этом файле содержится перечисление поддерживаемых команд SMTP-протокола,
 *  для каждой из которых создаётся запись в словаре команд. А также функции их
 *  обработки.
 */


#include <pcre.h>
#include "smtpserver-fsm.h"


/// \cond
#define RE_CMD_EXEC_OPTIONS 0x0
/// \endcond


/*!
 *  Перечисление поддерживаемых команд SMTP-протокола.
 */
typedef enum command_id {
    SMTP_BAD_CMD = -1,  /*!< ID не поддерживаемой команды. */
    SMTP_CMD_HELO,      /*!< ID команды \em HELO. */
    SMTP_CMD_EHLO,      /*!< ID команды \em EHLO. */
    SMTP_CMD_MAIL,      /*!< ID команды \em MAIL. */
    SMTP_CMD_RCPT,      /*!< ID команды \em RCPT. */
    SMTP_CMD_DATA,      /*!< ID команды \em DATA. */
    SMTP_CMD_RSET,      /*!< ID команды \em RSET. */
    SMTP_CMD_VRFY,      /*!< ID команды \em VRFY. */
    SMTP_CMD_EXPN,      /*!< ID команды \em EXPN. */
    SMTP_CMD_HELP,      /*!< ID команды \em HELP. */
    SMTP_CMD_NOOP,      /*!< ID команды \em NOOP. */
    SMTP_CMD_QUIT,      /*!< ID команды \em QUIT. */
    SMTP_CMD_COUNT      /*!< Количество поддерживаемых команд. */
} command_id_t;

/*!
 *  Структура данных команды SMTP-протокола.
 */
typedef struct {
    const char *const cmd_pattern;      /*!< Шаблон регулярного выражения имени команды. */
    const char *const args_pattern;     /*!< Шаблон регулярного выражения аргументов команды. */
    const char *const params_pattern;   /*!< Шаблон регулярного выражения параметров команды. */
    pcre *cmd_re;       /*!< Скомпилированное регулярное выражение имени команды. */
    pcre *args_re;      /*!< Скомпилированное регулярное выражение аргументов команды. */
    pcre *params_re;    /*!< Скомпилированное регулярное выражение параметров команды. */
    /*!
     *  Дополнительный блок данных результата анализа скомпилированного выражения
     *  аргументов команды.
     */
    pcre_extra *args_extra;
    /*!
     *  Дополнительный блок данных результата анализа скомпилированного выражения
     *  параметров команды.
     */
    pcre_extra *params_extra;
    /*!
     *  Событие КА SMTP-протокола, соответствующее команде.
     */
    te_smtp_event event;
} command_t;

/*!
 *  Словарь поддерживаемых команд SMTP-протокола.
 */
extern command_t commands[];

/*!
 *  Скомпилированное регулярные выражение для параметра расширения.
 */
extern pcre *GET_PARAMETER_RE;
/*!
 *  Скомпилированное регулярные выражение для электронной почты.
 */
extern pcre *EMAIL_RE;

/*!
 *  Пытается распознать команду SMTP-протокола.
 *  \param[in] command Строка с командой.
 *  \param[in] length Длина строки с командой.
 *  \param[out] ovector Массив позиций начал и концов групп регулярного
 *      выражения команды.
 *  \param[in] ovecsize Длина массива \a ovector.
 *  \returns ID распознанной команды или ::SMTP_BAD_CMD, если \a command не
 *      соответствует синтаксису ни одной из поддерживаемых команд.
 */
command_id_t recognize_command(const char command[], int length, int ovector[],
                               int ovecsize);

#endif
