#include "connections.h"
#include "logapi.h"
#include "common.h"
#include "commands.h"
#include "replies.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <assert.h>


#define CRLF    "\r\n"
#define CRLF_LEN    2

#define DOT "."
#define DOT_LEN 1


void close_pending_connection(pending_connection_t *const connection)
{
    assert(connection != NULL);

    const int result = close(connection->fd);
    if (result != 0) {
        log_func(WARNING, "close");
    }

    kill(getppid(), SIGUSR1);
    free(connection);
}

void close_active_connection(active_connection_t *const connection)
{
    assert(connection != NULL);

    int result = close(connection->fd);
    if (result != 0) {
        log_func(WARNING, "close");
    }

    result = close(connection->timeout_fd);
    if (result != 0) {
        log_func(WARNING, "close (timeout_fd)");
    }

    free(connection->address);

    reset_connection(connection);
    kill(getppid(), SIGUSR1);
    free(connection);
}

void reset_connection(active_connection_t *const connection)
{
    clear_recipients(connection);
    clear_buffer(connection);
    connection->message_limit = LIMIT_MESSAGE_CONTENT - 1;
}

void clear_recipients(active_connection_t *const connection)
{
    while (!STAILQ_EMPTY(&connection->recipients)) {
        const recipient_t *const first = STAILQ_FIRST(&connection->recipients);
        STAILQ_REMOVE_HEAD(&connection->recipients, links);
        free((void *)first);
    }
}

void clear_buffer(active_connection_t *const connection)
{
    connection->line_start = 0;
    connection->buffer_offset = 0;
    connection->buffer_length = 0;
}

void submit_reply(active_connection_t *const connection, const char format[], ...)
{
    const size_t size = LIMIT_MESSAGE_CONTENT - connection->buffer_length;

    // Print line contents.
    va_list args;
    va_start(args, format);
    connection->buffer_length +=
        vsnprintf(connection->buffer + connection->buffer_length, size, format, args);
    va_end(args);

    assert(connection->buffer_length + CRLF_LEN < LIMIT_MESSAGE_CONTENT);

    // Terminate line with CRLF sequence.
    strcpy(connection->buffer + connection->buffer_length, CRLF);
    connection->buffer_length += CRLF_LEN;
}

/**
 * Принимает данные в буфер соединения.
 */
static int read_pending_data(active_connection_t *const connection)
{
    int state = 0;
    while (state == 0 && connection->buffer_length < LIMIT_MESSAGE_CONTENT) {
        const int read_count =
            read(connection->fd, connection->buffer + connection->buffer_length,
                 LIMIT_MESSAGE_CONTENT - connection->buffer_length - 1);
        if (read_count < 0) {
            switch (errno) {
            case EAGAIN:
#if (EWOULDBLOCK != EAGAIN)
            case EWOULDBLOCK:
#endif
                state = 1;
                break;
            case EINTR:
                break;
            default:
                log_func(WARNING, "read");
                state = -1;
            }
        } else if (read_count == 0) {
            state = -1;
        } else {
            connection->buffer_length += read_count;
        }
    }
    connection->buffer[connection->buffer_length] = '\0';

    if (state == 0) {
        errno = ENOMEM;
    }

    return (state > 0 ? 0 : -1);
}

/**
 * Отправляет данные из буфера соединения.
 */
static int write_pending_data(const int fd, active_connection_t *const connection)
{
    int state = 0;
    while (state == 0 && connection->buffer_offset < connection->buffer_length) {
        const int write_count =
            write(fd, connection->buffer + connection->buffer_offset,
                  connection->buffer_length - connection->buffer_offset);
        if (write_count < 0) {
            switch (errno) {
            case EAGAIN:
#if (EWOULDBLOCK != EAGAIN)
            case EWOULDBLOCK:
#endif
                state = 1;
                break;
            case EINTR:
                break;
            default:
                log_func(WARNING, "write");
                state = -1;
            }
        } else if (write_count == 0) {
            state = -1;
        } else {
            connection->buffer_offset += write_count;
        }
    }

    return (state);
}

int process_input(active_connection_t *const connection,
                  int *const toggle_duplex_mode)
{
    // '-1' becuase there may was single '\r' char at the end of last receiption.
    size_t scanned_length = (connection->buffer_length > 0 ? connection->buffer_length - 1 : 0);

    // Receive any pending incoming data.
    int result = read_pending_data(connection);
    if (result < 0) {
        switch (errno) {
        case ENOMEM:
            log_message(WARNING, "Remote peer writes too much data (ENOMEM).");

            if ((connection->flags & AC_CMD_MODE) != 0x0) {
                connection->smtp_state =
                    smtp_step(connection->smtp_state, SMTP_EV_UNKNOWN,
                              connection, SMTP_BAD_CMD, NULL, 0);
            } else {
                connection->smtp_state =
                    smtp_step(connection->smtp_state, SMTP_EV_REJECTED,
                              connection, SMTP_BAD_CMD, NULL, 0);
                connection->flags |= AC_CMD_MODE;
            }

            *toggle_duplex_mode = 1;
            break;
        default:
            break;
        }
        goto do_return;
    }

    if ((connection->flags & AC_CMD_MODE) != 0x0) {
        // Check if the full line has been received.
        const char *const eol = strstr(connection->buffer + scanned_length, CRLF);
        result = (eol == NULL);
        if (result != 0) {
            goto do_return;
        }

        const char *const command = connection->buffer + connection->line_start;
        const int command_length = eol - command;
        log_message(VERBOSE, "Received command (length=%d): %.*s", command_length,
                    command_length, command);

        // Try to parse out one of the expected commands in the current state of FSM.
        const int groups_count = 1;
        const int ovecsize = groups_count * 3;
        int ovector[ovecsize];
        const command_id_t command_id =
            recognize_command(command, command_length, ovector, ovecsize);
        log_message(DEBUG, "Command ID: %d", command_id);

        if (command_id != SMTP_BAD_CMD) {
            connection->smtp_state =
                smtp_step(connection->smtp_state, commands[command_id].event,
                          connection, command_id, command + ovector[1],
                          command_length - ovector[1]);
        } else {
            connection->smtp_state =
                smtp_step(connection->smtp_state, SMTP_EV_UNKNOWN,
                          connection, SMTP_BAD_CMD, NULL, 0);
        }

        *toggle_duplex_mode = 1;
    } else if (connection->buffer_length > connection->message_limit) {
        connection->smtp_state =
            smtp_step(connection->smtp_state, SMTP_EV_REJECTED,
                      connection, SMTP_BAD_CMD, NULL, 0);

        connection->flags |= AC_CMD_MODE;
        *toggle_duplex_mode = 1;
    } else {
        while (1) {
            // Check if the full line has been received.
            const char *const eol = strstr(connection->buffer + scanned_length, CRLF);
            result = (eol == NULL);
            if (result != 0) break;

            char *const line = connection->buffer + connection->line_start;
            const int line_length = eol - line;
            log_message(DEBUG, "Received line (length=%d): %.*s", line_length,
                        line_length, line);

            if (strncmp(line, DOT, DOT_LEN) != 0) {
                connection->line_start += line_length + CRLF_LEN;
                scanned_length = connection->line_start;
            } else if (strncmp(line + DOT_LEN, CRLF, CRLF_LEN) != 0) {
                memmove(line, line + DOT_LEN, connection->buffer_length - (connection->line_start + DOT_LEN));
                connection->buffer[connection->buffer_length -= DOT_LEN] = '\0';
                connection->line_start += line_length + CRLF_LEN - DOT_LEN;
                scanned_length = connection->line_start;
            } else {
                log_message(VERBOSE, "Received end of data indicator.");
                connection->buffer_length = connection->line_start;

                connection->smtp_state =
                    smtp_step(connection->smtp_state, SMTP_EV_COMPLETED,
                              connection, SMTP_BAD_CMD, NULL, 0);

                connection->flags |= AC_CMD_MODE;
                *toggle_duplex_mode = 1;
                break;
            }
        }
    }

do_return:
    return (result);
}

int process_output(active_connection_t *const connection,
                   int *const toggle_duplex_mode)
{
    // Send any pending outgoing data.
    int result = write_pending_data(connection->fd, connection);
    if (result < 0) {
        goto do_return;
    }

    // If all pending data has been sent then switch to read mode.
    *toggle_duplex_mode = (result == 0);
    if (*toggle_duplex_mode) {
        clear_buffer(connection);

        switch (connection->smtp_state) {
        case SMTP_ST_COLLECT_DATA:
            connection->flags &= ~AC_CMD_MODE;
            break;
        case SMTP_ST_DONE:
            result = -2;
            break;
        default:
            break;
        }
    }

do_return:
    return (result);
}
