#include "common.h"
#include "logapi.h"
#include <signal.h>
#include <sys/signalfd.h>
#include <sys/eventfd.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <assert.h>

/*!
 *  Типы дескрипторов, возаращаемых системными вызовами pipe() и socketpair().
 */
enum {
    READ,   /*!< Дескриптор открытый на чтение. */
    WRITE   /*!< Десприптор открытый на запись. */
};

pid_t open_channel_and_fork(int *const out_fd, const open_channel_t open_channel,
                            const char open_channel_name[])
{
    assert(out_fd != NULL);
    *out_fd = -1;

    int fd[2] = {-1, -1};
    int result = open_channel(fd);
    if (result != 0) {
        log_func(CRITICAL, open_channel_name);
        goto do_return;
    }

    pid_t pid = fork();
    if (pid < 0) {
        log_func(CRITICAL, "fork");

        result = close(fd[READ]);
        if (result != 0) {
            log_func(WARNING, "close");
        }
        result = close(fd[WRITE]);
        if (result != 0) {
            log_func(WARNING, "close");
        }
    } else if (pid == 0) {
        result = close(fd[WRITE]);
        if (result != 0) {
            log_func(WARNING, "close");
        }

        *out_fd = fd[READ];
    } else {
        result = close(fd[READ]);
        if (result != 0) {
            log_func(WARNING, "close");
        }

        *out_fd = fd[WRITE];
    }

do_return:
    return (pid);
}

void make_sigset(sigset_t *const mask, ...)
{
    sigemptyset(mask);

    va_list args;
    va_start(args, mask);
    while (1) {
        const int signo = va_arg(args, int);
        if (signo < 0) break;
        sigaddset(mask, signo);
    }
    va_end(args);
}

int listen_for_signals(const int epoll_fd)
{
    sigset_t mask;
    sigprocmask(0, NULL, &mask);
    int signal_fd = signalfd(-1, &mask, SFD_NONBLOCK);
    if (signal_fd < 0) {
        log_func(CRITICAL, "signalfd");
        goto do_return;
    }

    struct epoll_event event = {
        .events = EPOLLIN | EPOLLET,
        .data.fd = signal_fd
    };
    int result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, signal_fd, &event);
    if (result != 0) {
        log_func(CRITICAL, "epoll_ctl (signal_fd)");
        goto do_close_signal_fd;
    }
    goto do_return;

do_close_signal_fd:
    result = close(signal_fd);
    if (result != 0) {
        log_func(WARNING, "close (signal_fd)");
    }
    signal_fd = -1;
do_return:
    return (signal_fd);
}

int listen_for_event(const int epoll_fd)
{
    int event_fd = eventfd(0, EFD_NONBLOCK);
    if (event_fd < 0) {
        log_func(CRITICAL, "eventfd");
        goto do_return;
    }

    struct epoll_event event = {
        .events = EPOLLIN | EPOLLET,
        .data.fd = event_fd
    };
    int result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, event_fd, &event);
    if (result != 0) {
        log_func(CRITICAL, "epoll_ctl (event_fd)");
        goto do_close_event_fd;
    }
    goto do_return;

do_close_event_fd:
    result = close(event_fd);
    if (result != 0) {
        log_func(WARNING, "close (event_fd)");
    }
    event_fd = -1;
do_return:
    return (event_fd);
}

void raise_event(const int event_fd)
{
    const uint64_t count = 1;
    const int result = write(event_fd, &count, sizeof(count));
    if (result < 0) {
        log_func(WARNING, "write (event_fd)");
    }
}

int pass_connection(const int uds_fd, const int fd, const char address[],
                    const size_t address_len)
{
    struct iovec iov = {.iov_base = (void *)address, .iov_len = address_len};

    const size_t payload_len = sizeof(fd);
    char control[CMSG_SPACE(payload_len)];

    const struct msghdr msg = {
        .msg_iov = &iov,
        .msg_iovlen = 1,
        .msg_control = control,
        .msg_controllen = CMSG_LEN(payload_len)
    };

    struct cmsghdr *const cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = msg.msg_controllen;

    *(int *)CMSG_DATA(cmsg) = fd;

    const ssize_t count = sendmsg(uds_fd, &msg, 0x0);
    return (count);
}

int receive_connection(const int uds_fd, char address[], const size_t max_address_len)
{
    int fd = -1;
    struct iovec iov = {.iov_base = address, .iov_len = max_address_len};

    const size_t payload_len = sizeof(fd);
    char control[CMSG_SPACE(payload_len)];

    struct msghdr msg = {
        .msg_iov = &iov,
        .msg_iovlen = 1,
        .msg_control = control,
        .msg_controllen = CMSG_LEN(payload_len)
    };

    const ssize_t count = recvmsg(uds_fd, &msg, 0x0);
    if (count > 0) {
        struct cmsghdr *const cmsg = CMSG_FIRSTHDR(&msg);
        assert(cmsg->cmsg_type == SCM_RIGHTS);
        fd = *(int *)CMSG_DATA(cmsg);
    }

    return (fd);
}
