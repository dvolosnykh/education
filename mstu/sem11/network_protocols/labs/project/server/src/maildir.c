#include "maildir.h"
#include "smtp_limits.h"
#include "utils.h"
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>


#define MESSAGE_NAME_FORMAT     "%ld.%d.%s"

#define MAILDIR_TMP     "tmp"
#define MAILDIR_NEW     "new"
#define MAILDIR_CUR     "cur"
#define MAILDIR_SUBDIR_LEN  3
#define MAX_TRIES_COUNT     3


static pid_t MY_PID = -1;
static size_t MAILDIR_PATH_LEN = 0;
static char MAILBOX_PATH[PATH_MAX];


int maildir_set_path(const char path[])
{
    int result = 0;

    // Get canonical absolute path to maildir.
    const char *const abs_path = realpath(path, MAILBOX_PATH);
    if (abs_path == NULL) {
        result = -1;
        goto do_return;
    }

    MAILDIR_PATH_LEN = strlen(MAILBOX_PATH);
    assert(MAILDIR_PATH_LEN < PATH_MAX);

    // Check if it can be accessed.
    result = access(MAILBOX_PATH, R_OK | W_OK | X_OK);
    if (result != 0) {
        goto do_return;
    }

    // Add path directory separator.
    MAILBOX_PATH[MAILDIR_PATH_LEN++] = '/';

    // Get pid of self (this is to reduce the number of system calls).
    MY_PID = getpid();

do_return:
    return (result);
}

int maildir_save_message(const char mailbox[], const size_t mailbox_len,
                         const char host[], const time_t timestamp,
                         const char return_path[], const size_t return_path_len,
                         const char received[], const size_t received_len,
                         const char message[], const size_t message_len)
{
    // Construct path to the user's mailbox.
    strncpy(MAILBOX_PATH + MAILDIR_PATH_LEN, mailbox, mailbox_len);
    MAILBOX_PATH[MAILDIR_PATH_LEN + mailbox_len] = '\0';

    // Go there.
    int result = chdir(MAILBOX_PATH);
    if (result != 0) {
        goto do_return;
    }

    char tmp_name[NAME_MAX + 1];
    const int mode = (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
    int fd = -1;
    int try_no;
    for (try_no = 1; fd < 0 && try_no < MAX_TRIES_COUNT; ++try_no) {
        // Generate 'tmp' file name.
        const int name_len =
            snprintf(tmp_name, NAME_MAX + 1, MAILDIR_TMP "/" MESSAGE_NAME_FORMAT,
                     (long)timestamp + try_no, MY_PID, host);
        if (name_len > NAME_MAX) break;

        // Create 'tmp' file. Must not exist up to now.
        fd = open(tmp_name, O_CREAT | O_EXCL | O_WRONLY, mode);
        if (fd < 0) {
            // @todo: This is very rude way to ensure that on the second try there
            // will be no file with generated name.
            sleep(1);
        }
    }
    if (fd < 0) {
        result = -1;
        goto do_return;
    }

    // Write "Return-Path" field.
    result = write_buffer(fd, return_path, return_path_len);
    if (result < 0) {
        goto do_unlink_tmp_name;
    }

    // Write "Received" time stamp.
    result = write_buffer(fd, received, received_len);
    if (result < 0) {
        goto do_unlink_tmp_name;
    }

    // Write message.
    result = write_buffer(fd, message, message_len);
    if (result < 0) {
        goto do_unlink_tmp_name;
    }

    // Create 'new' file name basing on 'tmp'.
    char new_name[NAME_MAX + 1] = MAILDIR_NEW "/";
    strcpy(new_name + MAILDIR_SUBDIR_LEN + 1, tmp_name + MAILDIR_SUBDIR_LEN + 1);

    // Link 'new' name to the same file as 'tmp' name is linked to.
    result = link(tmp_name, new_name);
    if (result != 0) {
        goto do_close_fd;
    }
do_unlink_tmp_name:
    result = unlink(tmp_name);
do_close_fd:
    result = close(fd);
do_return:
    return (result);
}
