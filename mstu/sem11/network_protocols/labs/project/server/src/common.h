#ifndef COMMON_H
#define COMMON_H

/*!
 *  \file common.h
 *  \brief Набор функций, общих для разных типов процессов сетевой службы
 *      SMTP-сервер.
 *
 *  Этот файл представляет собой коллекцию функций, используемых в функциях
 *  разных типов процессов сетевой службы SMTP-сервер.
 */


#include <signal.h>


/*!
 *  Тип указателя на функцию, создающую некий однонаправленный канал связи.
 */
typedef int (*open_channel_t)(int fd[2]);


/*!
 *  Создаёт однонаправленный канал связи, после чего создаёт дочерний процесс,
 *  закрывая ненужные дескрипторы созданного канала.
 *  \param[out] out_fd Дескриптор однонаправленного канала связи с дочерним
 *      процессом.
 *  \param[in] open_channel Указатель на функцию создания однонаправленного
 *      канала связи с дочерним процессом.
 *  \param[in] open_channel_name Название однонаправленного канала связи с
 *      дочерним процессом.
 *  \returns PID дочернего процесса, в случае успеха. Иначе, возвращает -1.
 */
pid_t open_channel_and_fork(int *out_fd, open_channel_t open_channel,
                            const char open_channel_name[]);

/*!
 *  Создаёт маску сигналов. После указателя на маску \a mask, должен следовать
 *  список сигналов, завершающийся -1.
 *  \param[out] mask Указатель на создаваему маску сигналов.
 */
void make_sigset(sigset_t *const mask, ...);

/*!
 *  Создаёт и регистрирует в дескрипторе мультиплексирования \a epoll_fd
 *  дескриптор сигналов для обработки событий о поступивших сигналах.
 *  \param[in] epoll_fd Дескриптор мультиплексирования.
 *  \returns Дескриптор сигналов.
 */
int listen_for_signals(int epoll_fd);

/*!
 *  Создаёт и регистрирует в дескрипторе мультиплексирования \a epoll_fd
 *  дескриптор события.
 *  \param[in] epoll_fd Дескриптор мультиплексирования.
 *  \returns Дескриптор события.
 */
int listen_for_event(int epoll_fd);

/*!
 *  Увеличивает счётчик событий.
 *  \param[in] event_fd Дескриптор события.
 */
void raise_event(int event_fd);

/*!
 *  Посылает дескриптор нового соединения (а также IP-адрес источника) по сокету
 *  домена Unix.
 *  \param[in] uds_fd Дескриптор сокета домена Unix.
 *  \param[in] fd Передаваемый дескриптор соединения.
 *  \param[in] address Передаваемый IP-адрес источника соединения.
 *  \param[in] address_len Длина строки IP-адреса.
 *  \returns Результат выполнения функции
 *      <a href="http://linux.die.net/man/2/sendmsg">sendmsg()</a>, в случае
 *      ошибки \em errno сохраняется.
 */
int pass_connection(int uds_fd, int fd, const char address[], size_t address_len);

/*!
 *  Принимает дескриптор нового соединения по сокету домена Unix.
 *  \param[in] uds_fd Дескриптор сокета домена Unix.
 *  \param[out] address Буфер для строки принимаемого IP-адреса источника
 *      соединения.
 *  \param[in] max_address_len Размер буфера для строки IP-адреса.
 *  \returns Возвращает принятый дескриптор нового соединения. В случае ошибки
 *  возвращает -1 и код ошибки \em errno в результате выполнения функции
 *  <a href="http://linux.die.net/man/2/recvmsg">recvmsg()</a> сохраняется.
 */
int receive_connection(int uds_fd, char address[], size_t max_address_len);

#endif
