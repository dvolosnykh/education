#include "parameters_re.h"
#include "parameters.h"
#include <stdio.h>
#include <assert.h>


#define RE_COMPILE_OPTIONS  (0x0)

/*!
 *  Инициализатор структуры данных параметра команды SMTP-протокола в словаре
 *  поддерживаемых параметров.
 */
#define INIT_PARAM(cmd, name)                                   \
    [SMTP_##cmd##_PARAM_##name] = {                             \
        .name_pattern = SMTP_##cmd##_PARAM_##name##_STR,        \
        .value_pattern = SMTP_##cmd##_PARAM_##name##_VALUE_STR  \
    }


parameter_t mail_parameters[SMTP_MAIL_PARM_COUNT] = {
    INIT_PARAM(MAIL, SIZE)
};

#if 0
parameter_t rcpt_parameters[SMTP_RCPT_PARM_COUNT] = {
};
#endif

void __attribute__((constructor)) init_parameters_re()
{
    printf("Compiling regular expressions of parameters... ");

    int no_errors = 1;
    const char *error;
    int error_offset;
    int i;
    for (i = 0; no_errors && i < SMTP_MAIL_PARM_COUNT; ++i) {
        mail_parameters[i].name_re = pcre_compile(mail_parameters[i].name_pattern, RE_COMPILE_OPTIONS, &error, &error_offset, NULL);
        no_errors = (mail_parameters[i].name_re != NULL);
        if (!no_errors) {
            fprintf(stderr, "PARAM NAME %d: %s \nCompile error at %d: %s.\n", i, mail_parameters[i].name_pattern, error_offset, error);
        }

        if (no_errors) {
            mail_parameters[i].value_re = pcre_compile(mail_parameters[i].value_pattern, RE_COMPILE_OPTIONS, &error, &error_offset, NULL);
            no_errors = (mail_parameters[i].value_re != NULL);
            if (!no_errors) {
                fprintf(stderr, "PARAM VALUE %d: %s \nCompile error at %d: %s.\n", i, mail_parameters[i].value_pattern, error_offset, error);
            }
        }
    }
#if 0
    for (i = 0; no_errors && i < SMTP_RCPT_PARM_COUNT; ++i) {
        rcpt_parameters[i].name_re = pcre_compile(rcpt_parameters[i].name_pattern, RE_COMPILE_OPTIONS, &error, &error_offset, NULL);
        no_errors = (rcpt_parameters[i].name_re != NULL);
        if (!no_errors) {
            fprintf(stderr, "PARAM NAME %d: %s \nCompile error at %d: %s.\n", i, rcpt_parameters[i].name_pattern, error_offset, error);
        }

        if (no_errors) {
            rcpt_parameters[i].value_re = pcre_compile(rcpt_parameters[i].value_pattern, RE_COMPILE_OPTIONS, &error, &error_offset, NULL);
            no_errors = (rcpt_parameters[i].value_re != NULL);
            if (!no_errors) {
                fprintf(stderr, "PARAM VALUE %d: %s \nCompile error at %d: %s.\n", i, rcpt_parameters[i].value_pattern, error_offset, error);
            }
        }
    }
#endif
    printf("%s\n", (no_errors ? "Done" : "Failed"));
    if (!no_errors) exit(EXIT_FAILURE);
}

void __attribute__((destructor)) free_parameters_re()
{
    int i;
    for (i = 0; i < SMTP_MAIL_PARM_COUNT; ++i) {
        pcre_free(mail_parameters[i].name_re);
        pcre_free(mail_parameters[i].value_re);
    }
#if 0
    for (i = 0; i < SMTP_RCPT_PARM_COUNT; ++i) {
        pcre_free(rcpt_parameters[i].name_re);
        pcre_free(rcpt_parameters[i].value_re);
    }
#endif
}

mail_param_id_t recognize_mail_parameter(const char parameter[], int length,
        int ovector[], int ovecsize)
{
    int found = 0;
    int i = 0;
    while (!found && i < SMTP_MAIL_PARM_COUNT) {
        const int result =
            pcre_exec(mail_parameters[i].name_re, NULL, parameter, length, 0,
                      RE_MAIL_PARAM_EXEC_OPTIONS, ovector, ovecsize);
        found = (result >= 0);
        if (!found) ++i;
    }

    return (found ? i : SMTP_BAD_MAIL_PARAM);
}

rcpt_param_id_t recognize_rcpt_parameter(const char parameter[], int length,
        int ovector[], int ovecsize)
{
    int found = 0;
    int i = 0;
#if 0
    while (!found && i < SMTP_RCPT_PARM_COUNT) {
        const int result =
            pcre_exec(rcpt_parameters[i].name_re, NULL, parameter, length, 0,
                      RE_RCPT_PARAM_EXEC_OPTIONS, ovector, ovecsize);
        found = (result >= 0);
        if (!found) ++i;
    }
#endif
    return (found ? i : SMTP_BAD_RCPT_PARAM);
}
