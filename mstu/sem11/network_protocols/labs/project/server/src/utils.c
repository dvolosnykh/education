#include "utils.h"
#include <fcntl.h>
#include <unistd.h>


int set_flags(const int fd, const int flags)
{
    int result = fcntl(fd, F_GETFL);
    if (result >= 0) {
        result |= flags;
        result = fcntl(fd, F_SETFL, result);
    }

    return (result);
}

int clear_flags(const int fd, const int flags)
{
    int result = fcntl(fd, F_GETFL);
    if (result >= 0) {
        result &= ~flags;
        result = fcntl(fd, F_SETFL, result);
    }

    return (result);
}

int write_buffer(const int fd, const char buffer[], const size_t length)
{
    int result = 0;

    ssize_t total_count = 0;
    while (result == 0 && total_count < length) {
        const ssize_t count = write(fd, buffer + total_count, length - total_count);
        if (count < 0) {
            result = -1;
        } else {
            total_count += count;
        }
    }

    return (result);
}
