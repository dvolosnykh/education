#ifndef UTILS_H
#define UTILS_H

/*!
 *  \file utils.h
 *  \brief В этом файле находятся функции, которые по каким-то причинам не
 *      были помещены ни в какой другой.
 */


#include <stdlib.h>


/*!
 *  К флагам дескриптора \a fd добавляет флаги \a flags.
 *  \param[in] fd Дескриптор, к которому применяется операция.
 *  \param[in] flags Набор устанавливаемых флагов.
 *  \returns Результат выполнения последнего вызова функции
 *      <a href="http://linux.die.net/man/2/fcntl">fcntl()</a>.
 */
int set_flags(int fd, int flags);

/*!
 *  Удаляет флаги \a flags из флагов дескриптора \a fd.
 *  \param[in] fd Дескриптор, к которому применяется операция.
 *  \param[in] flags Набор очищаемых флагов.
 *  \returns Результат выполнения последнего вызова функции
 *      <a href="http://linux.die.net/man/2/fcntl">fcntl()</a>.
 */
int clear_flags(int fd, int flags);

/*!
 *  Пишет в дескриптор \a fd буфер \a buffer длины \a length.
 *  \param[in] fd Дескриптор, к которому применяется операция.
 *  \param[in] buffer Данные записываемые в дескриптор \a fd.
 *  \param[in] length Длина буфера данных.
 *  \returns 0 в случае успеха. В противном случае, возвращает отрицательное
 *      значение.
 */
int write_buffer(int fd, const char buffer[], size_t length);

#endif
