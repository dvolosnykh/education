#ifndef PARAMETERS_H
#define PARAMETERS_H

/*!
 *  \file parameters.h
 *  \brief Структуры данных параметров команд SMTP-протокола \em MAIL и
 *      \em RCPT, а также функции их обработки.
 *
 *  В этом файле содержится перечисление поддерживаемых параметров команд
 *  SMTP-протокола \em MAIL и \em RCPT. Для каждой команды, допускающей
 *  параметры, хранится словарь поддерживаемых параметров. Помимо этого здесь
 *  представлены функции обработки параметров.
 */


#include <pcre.h>


/// \cond
#define RE_MAIL_PARAM_EXEC_OPTIONS  0x0
#define RE_RCPT_PARAM_EXEC_OPTIONS  0x0
/// \endcond


/*!
 *  Структура данных параметра команды SMTP-протокола.
 */
typedef struct {
    const char *const name_pattern;     /*!< Шаблон регулярного выражения имени расширения. */
    const char *const value_pattern;    /*!< Шаблон регулярного выражения значения расширения. */
    pcre *name_re;  /*!< Скомпилированное регулярное выражение имени расширения. */
    pcre *value_re; /*!< Скомпилированное регулярное выражение значения расширения. */
} parameter_t;


/*!
 *  Перечисление поддерживаемых параметров команды \em MAIL.
 */
typedef enum {
    SMTP_BAD_MAIL_PARAM = -1,   /*!< ID не поддерживаемого параметра. */
    SMTP_MAIL_PARAM_SIZE,   /*!< ID параметра \em SIZE. */
    SMTP_MAIL_PARM_COUNT    /*!< Количество поддерживаемых параметров. */
} mail_param_id_t;

/*!
 *  Словарь поддерживаемых параметров команды \em MAIL.
 */
extern parameter_t mail_parameters[];

/*!
 *  Пытается распознать параметр команды \em MAIL.
 *  \param[in] parameter Строка с параметром.
 *  \param[in] length Длина строки с параметром.
 *  \param[out] ovector Массив позиций начал и концов групп регулярного
 *      выражения параметра.
 *  \param[in]  ovecsize    Длина массива \a ovector.
 *  \returns ID распознанного параметра или ::SMTP_BAD_MAIL_PARAM, если
 *      \a parameter не соответствует синтаксису ни одного из поддерживаемых
 *      параметров.
 */
mail_param_id_t recognize_mail_parameter(const char parameter[], int length,
        int ovector[], int ovecsize);


/*!
 *  Перечисление поддерживаемых параметров команды \em RCPT.
 */
typedef enum {
    SMTP_BAD_RCPT_PARAM = -1,   /*!< ID не поддерживаемого параметра. */
    SMTP_RCPT_PARM_COUNT    /*!< Количество поддерживаемых параметров. */
} rcpt_param_id_t;

#if 0
/*!
 *  Словарь поддерживаемых параметров команды \em RCPT.
 */
extern parameter_t rcpt_parameters[];
#endif

/*!
 *  Пытается распознать параметр команды \em RCPT.
 *  \param[in] parameter Строка с параметром.
 *  \param[in] length Длина строки с параметром.
 *  \param[out] ovector Массив позиций начал и концов групп регулярного
 *      выражения параметра.
 *  \param[in]  ovecsize    Длина массива \a ovector.
 *  \returns ID распознанного параметра или ::SMTP_BAD_RCPT_PARAM, если
 *      \a parameter не соответствует синтаксису ни одного из поддерживаемых
 *      параметров.
 */
rcpt_param_id_t recognize_rcpt_parameter(const char parameter[], int length,
        int ovector[], int ovecsize);

#endif
