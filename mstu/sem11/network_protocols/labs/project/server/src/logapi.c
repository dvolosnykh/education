#include "logapi.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <libgen.h>
#include <assert.h>
#include <arpa/inet.h>


log_level_t LOG_LEVEL = INFO;
int LOG_FD = STDERR_FILENO;



static void __log_message(char message[], int length, const char format[],
                          va_list args)
{
    length += vsnprintf(message + length, MAX_MESSAGE_LEN - length, format, args);

    if (length > 0) {
        if (MAX_MESSAGE_LEN - length <= 2) {
            length = MAX_MESSAGE_LEN - 2;
        }
        message[length] = '\n';
        message[++length] = '\0';

        int result = write(LOG_FD, message, length);
        if (result < 0) {
            switch (errno) {
            case EPIPE:
            case EBADF:
                LOG_FD = STDERR_FILENO;
                log_message(WARNING, "Logger not responding. Logging to STDERR.");
                result = write(LOG_FD, message, length);
                assert(result >= 0);
                break;
            default:
                log_func(WARNING, "write");
            }
        }
    }
}

#define _log_message(message, length, format)       \
{                                                   \
    va_list args;                                   \
    va_start(args, format);                         \
    __log_message(message, length, format, args);   \
    va_end(args);                                   \
}

/**
 * Печатает префикс степени важности сообщения.
 */
static int print_prefix(char message[], const log_level_t log_level)
{
    int length = 0;
    switch (log_level) {
    case DEBUG:
        length += snprintf(message, MAX_MESSAGE_LEN, "> ");
        break;
    case VERBOSE:
    case INFO:
        break;
    case WARNING:
        length += snprintf(message, MAX_MESSAGE_LEN, "WARNING ");
        break;
    case CRITICAL:
        length += snprintf(message, MAX_MESSAGE_LEN, "CRITICAL ");
        break;
    default:
        assert(0);
    }

    return (length);
}

void log_message(const log_level_t log_level, const char format[], ...)
{
    if (log_level >= LOG_LEVEL) {
        char message[MAX_MESSAGE_LEN];
        const int length = print_prefix(message, log_level);
        _log_message(message, length, format);
    }
}

void __log_func(const log_level_t log_level, const char filepath[],
                const size_t line, const char function[], const char format[], ...)
{
    if (log_level >= LOG_LEVEL) {
        char message[MAX_MESSAGE_LEN];
        int length = print_prefix(message, log_level);

        char *const path = strdup(filepath);
        const char *file = (path != NULL ? basename(path) : filepath);
        length += snprintf(message + length, MAX_MESSAGE_LEN - length, "%s:%zu: %s: ",
                           file, line, function);
        if (path != NULL) {
            free(path);
        }

        _log_message(message, length, format);
    }
}

void print_address(char buffer[], const size_t max,
                   struct sockaddr *const address, const socklen_t addr_len)
{
    const void *addr = NULL;
    switch (addr_len) {
    case sizeof(struct sockaddr_in6): {
        address->sa_family = AF_INET6;
        struct sockaddr_in6 *const ipv6 = (struct sockaddr_in6 *)address;
        addr = &(ipv6->sin6_addr);
        break;
    }
    case sizeof(struct sockaddr_in): {
        address->sa_family = AF_INET;
        struct sockaddr_in *const ipv4 = (struct sockaddr_in *)address;
        addr = &(ipv4->sin_addr);
        break;
    }
    default:
        fprintf(stderr, "print_address: sa_family = %d; addr_len = %u\n",
                address->sa_family, addr_len);
        assert(0);
    }

    inet_ntop(address->sa_family, addr, buffer, max);
}
