#include "manager.h"
#include "logger.h"
#include "worker.h"
#include "logapi.h"
#include "common.h"
#include "settings.h"
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/signalfd.h>
#include <sys/prctl.h>
#include <wait.h>
#include <unistd.h>
#include <netdb.h>
#include <fcntl.h>
#include <assert.h>
#include <limits.h>
#include <signal.h>
#include <search.h>
#include <time.h>
#include <netinet/tcp.h>
#include <pwd.h>
#include <grp.h>


/**
 * Информация о рабочих процессах, необходимая для работы управляющего процесса.
 */
typedef struct {
    pid_t pid;  /**< Идентификатор рабочего процесса. */
    int uds_fd; /**< Сокет домена Unix (к управляющему процессу). */
    /**
     * Степень загрузки рабочего процесса. Измеряется количеством дескрипторов
     * открытых сокетов.
     */
    size_t load;
} worker_info_t;

/**
 * Структура данных, необходимых для работы управляющего процесса.
 */
typedef struct {
    int quit;       /**< Флаг завершения работы. */
    int epoll_fd;   /**< Дескриптор ожидания событий. */
    int signal_fd;  /**< Дескриптор очереди сигналов. */
    int listen_fd;  /**< Десприптор слушающего сокета. */
    worker_info_t *workers; /**< Таблица данных о рабочих процессах. */
    size_t workers_count;   /**< Количество рабочих процессов. */
    pid_t workers_pgid;     /**< Группа рабочих процессов. */
} manager_data_t;


/**
 * Понижает привилегии основного процесса до прав пользователя \arg user и/или
 * прав группы \arg group.
 */
static int downgrade_privileges(const char *const user, const char *const group)
{
    int result = 0;

    if (result >= 0 && group != NULL) {
        char *end;
        long gid = strtol(group, &end, 10);
        if (*end != '\0') {
            errno = 0;
            gid = -1;

            struct group *const gr = getgrnam(group);
            if (gr == NULL) {
                if (errno != 0) {
                    log_func(CRITICAL, "getgrnam");
                } else {
                    log_func_custom(CRITICAL, "getgrnam", "Group not found");
                }
            } else {
                gid = gr->gr_gid;
            }
        }

        if (gid >= 0) {
            result = setgid(gid);
            if (result < 0) {
                log_func(CRITICAL, "setgid");
            }
        } else {
            result = -1;
        }
    }

    if (result >= 0 && user != NULL) {
        char *end;
        long uid = strtol(user, &end, 10);
        if (*end != '\0') {
            errno = 0;
            uid = -1;

            struct passwd *const pw = getpwnam(user);
            if (pw == NULL) {
                if (errno != 0) {
                    log_func(CRITICAL, "getpwnam");
                } else {
                    log_func_custom(CRITICAL, "getpwnam", "User not found");
                }
            } else {
                uid = pw->pw_uid;
            }
        }

        if (uid >= 0) {
            result = setuid(uid);
            if (result < 0) {
                log_func(CRITICAL, "setuid");
            }
        } else {
            result = -1;
        }
    }

    return (result);
}

/**
 * Создаёт журналирующий процесс, перемещая его в собственную отдельную группу.
 * Возвращает идентификатор созданного процесса, аналогично функции \a fork().
 */
static pid_t spawn_logger(const char *const user, const char *const group,
                          const char *const log_file, const int print)
{
    log_message(INFO, "Spawning logger...");

    int log_fd;
    const pid_t pid = open_channel_and_fork(&log_fd, pipe, "pipe");
    if (pid == 0) {
        int result = prctl(PR_SET_PDEATHSIG, SIGTERM);
        if (result < 0) {
            log_func(WARNING, "prctl");
        }

        if (log_file != NULL) {
            const int out_fd = open(log_file, O_CREAT | O_WRONLY | O_APPEND,
                                    S_IRUSR | S_IWUSR);
            if (out_fd < 0) {
                log_func(WARNING, "open");
            } else {
                result = dup2(out_fd, STDERR_FILENO);
                if (result != STDERR_FILENO) {
                    log_func(WARNING, "dup2 (out_fd)");
                }

                result = close(out_fd);
                if (result != 0) {
                    log_func(WARNING, "close (out_fd)");
                }
            }
        }

        result = downgrade_privileges(user, group);
        if (result < 0) {
            goto do_exit_logger;
        }

        result = dup2(log_fd, STDIN_FILENO);
        if (result != STDIN_FILENO) {
            log_func(CRITICAL, "dup2 (log_fd)");
            goto do_exit_logger;
        }

        result = close(log_fd);
        if (result != 0) {
            log_func(WARNING, "close (log_fd)");
        }

        const int logger_result = logger_entry(print);

do_exit_logger:
        exit(logger_result);
    } else if (pid > 0) {
        LOG_FD = (LOG_FD == STDERR_FILENO ? log_fd : LOG_FD);

        const time_t timestamp = time(NULL);
        int ok = 1;
        struct tm local;
        const struct tm *const static_local = localtime_r(&timestamp, &local);
        ok = (static_local != NULL);

        char buffer[64];
        if (ok) {
            const size_t count = strftime(buffer, 64, "%c", &local);
            ok = (count != 0);
        }

        log_message(INFO, "=================== %s ===================", (ok ? buffer : "[ERROR]"));
        log_message(INFO, "Spawned logger.");

        int result = setpgid(pid, 0);
        if (result != 0) {
            log_func(WARNING, "setpgid");
        }
    }

    return (pid);
}

/**
 * Завершает работу процесса с идентификатором \arg pid.
 */
static void terminate_process(const pid_t pid)
{
    int result = kill(pid, SIGTERM);
    if (result != 0) {
        log_func(WARNING, "kill (TERM)");
    }

    alarm(TERMINATION_TIMEOUT);
    siginfo_t info;
    result = waitid(P_PID, pid, &info, WEXITED);
    if (result != 0 && errno != EINTR) {
        log_func(WARNING, "waitid");
    }
    alarm(0);

    if (result != 0) {
        result = kill(pid, SIGKILL);
        if (result != 0) {
            log_func(WARNING, "kill (KILL)");
        }
    }
}

/**
 * Закрывает дескрипторы сокетов (для связи с рабочими процессами) в
 * управляющем процессе. Должна вызываться каждый раз после создания очередного
 * рабочего процесса. А также при завершении работы самого управляющего процесса.
 */
static void cleanup_uds_fds(const worker_info_t workers[], const int count)
{
    for (int i = 0; i < count; ++i) {
        const int fd = workers[i].uds_fd;
        if (fd >= 0) {
            const int result = close(fd);
            if (result != 0) {
                log_func(WARNING, "close");
            }
        }
    }
}

/**
 * Функция-обёртка для socketpair(), создающей пару сокетов домена Unix.
 */
static int socketpipe(int fd[2])
{
    return socketpair(AF_UNIX, SOCK_DGRAM, PF_UNIX, fd);
}

/**
 * Создаёт рабочий процесс, перемещая его в группу журналирующего процесса.
 * Возвращает идентификатор созданного процесса, аналогично функции \a fork().
 * Помимо этого возвращает через \arg out_fd дескриптор сокета для связи с
 * созданным рабочим процессом.
 */
static pid_t spawn_worker(int *const out_fd, manager_data_t *const manager,
                          const options_t *const options)
{
    log_message(INFO, "Spawning worker...");

    int uds_fd;
    const pid_t pid = open_channel_and_fork(&uds_fd, socketpipe, "socketpair");
    if (pid == 0) {
        int result = prctl(PR_SET_PDEATHSIG, SIGTERM);
        if (result < 0) {
            log_func(WARNING, "prctl");
        }

        result = close(manager->epoll_fd);
        if (result != 0) {
            log_func(WARNING, "close (epoll_fd)");
        }
        result = close(manager->signal_fd);
        if (result != 0) {
            log_func(WARNING, "close (signal_fd)");
        }
        result = close(manager->listen_fd);
        if (result != 0) {
            log_func(WARNING, "close (listen_fd)");
        }
        cleanup_uds_fds(manager->workers, manager->workers_count);
        free(manager->workers);

        const int worker_result =
            worker_entry(uds_fd, options->domain, options->mail_dir, options->queue_dir);

        result = close(uds_fd);
        if (result != 0) {
            log_func(WARNING, "close (uds_fd)");
        }
        result = close(LOG_FD);
        if (result != 0) {
            log_func(WARNING, "close (LOG_FD)");
        }

        exit(worker_result);
    } else if (pid > 0) {
        log_message(INFO, "Spawned worker.");

        int result = setpgid(pid, manager->workers_pgid);
        if (result != 0) {
            log_func(WARNING, "setpgid");
        }

        *out_fd = uds_fd;
    }

    return (pid);
}

/**
 * Создаёт \arg count рабочих процессов, перемещая их в группу \arg logger_pgid
 * журналирующего процесса. Возвращает действительное количество созданных
 * рабочих процессов.
 */
static int spawn_workers(manager_data_t *const manager,
                         const options_t *const options)
{
    int i = 0;
    do {
        manager->workers[i].load = 0;
        manager->workers[i].pid = spawn_worker(&manager->workers[i].uds_fd,
                                               manager, options);
    } while (manager->workers[i].pid >= 0 && ++i < manager->workers_count);

    return (i);
}

/**
 * Завершает работу рабочих процессов SMTP-сервера.
 */
static void terminate_workers(const pid_t workers_pgid, const int spawned_count)
{
    log_message(INFO, "Terminating workers...");

    int result = killpg(workers_pgid, SIGTERM);
    if (result != 0) {
        log_func(WARNING, "killpg (TERM)");
    }

    alarm(TERMINATION_TIMEOUT);
    int finished_count = 0;
    while (finished_count < spawned_count) {
        siginfo_t info;
        result = waitid(P_PGID, workers_pgid, &info, WEXITED);
        if (result < 0 && errno == EINTR) {
            break;
        }
        ++finished_count;
    }
    alarm(0);

    log_message(INFO, "%d workers have terminated.", finished_count);
    if (finished_count < spawned_count) {
        log_message(WARNING, "Not all workers have terminated, killing them.");

        result = killpg(workers_pgid, SIGKILL);
        if (result != 0) {
            log_func(WARNING, "killpg (KILL)");
        }
    }
}

/**
 * Ищет в списке адресов \arg address_list адреса заданного типа \arg family и
 * пытается создать и привязать сокет к одному из них. Возвращает дескриптор
 * сокета и указатель на структуру соответствующего адреса. В случае неудачи
 * возвращает '-1' и тогда out_address == NULL.
 */
static int find_socket(const int family, struct addrinfo *const address_list,
                       const struct addrinfo **out_address)
{
    int socket_fd = -1;

    const struct addrinfo *address;
    for (address = address_list; address != NULL && socket_fd < 0; address = address->ai_next) {
        if (address->ai_family != family) continue;

        log_message(DEBUG, "Family: %d, Protocol: %d", address->ai_family, address->ai_protocol);

        socket_fd = socket(address->ai_family, address->ai_socktype | SOCK_NONBLOCK,
                           address->ai_protocol);
        if (socket_fd < 0) {
            log_func(CRITICAL, "socket");
            continue;
        }

        const int on = 1;
        int result = setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
        if (result != 0) {
            log_func(WARNING, "setsockopt (SO_REUSEADDR)");
            goto do_close_socket_fd;
        }

        result = bind(socket_fd, address->ai_addr, address->ai_addrlen);
        if (result != 0) {
            log_func(WARNING, "bind");
            goto do_close_socket_fd;
        }

        break;

do_close_socket_fd:
        close(socket_fd);
        socket_fd = -1;
    }

    *out_address = (socket_fd > 0 ? address : NULL);
    return (socket_fd);
}

/**
 * Создаёт сокет и привязывает его к адресу хоста \arg host на порту \arg port.
 */
static int socket_bind(const char host[], const char port[])
{
    const struct addrinfo hints = {
        .ai_family = AF_UNSPEC,
        .ai_socktype = SOCK_STREAM,
        .ai_flags = AI_PASSIVE,
        .ai_protocol = PF_UNSPEC,
        0
    };
    struct addrinfo *address_list;
    int result = getaddrinfo(host, port, &hints, &address_list);
    if (result != 0) {
        log_func_custom(CRITICAL, "getaddrinfo", gai_strerror(result));
        goto do_return;
    }

    const struct addrinfo *address;
    int socket_fd = find_socket(AF_INET6, address_list, &address);
    if (socket_fd < 0) {
        socket_fd = find_socket(AF_INET, address_list, &address);
    }

    if (socket_fd >= 0) {
        char str_address[INET6_ADDRSTRLEN];
        print_address(str_address, sizeof(str_address), address->ai_addr, address->ai_addrlen);
        log_message(VERBOSE, "Binded to address %s on port %s.", str_address, port);
    }

    freeaddrinfo(address_list);
do_return:
    return (socket_fd);
}

/**
 * Создаёт сокет для входящих соединений и регистрирует его дескриптор в
 * дескрипторе ожидания событий.
 */
static int listen_for_connections(const int epoll_fd, const char host[],
                                  const char port[])
{
    int listen_fd = socket_bind(host, port);
    if (listen_fd < 0) {
        log_func_custom(CRITICAL, "socket_bind",
                        "Unable to bind to the given address or port.");
        goto do_return;
    }

    int result = listen(listen_fd, SOMAXCONN);
    if (result != 0) {
        log_func(CRITICAL, "listen");
        goto do_close_listen_fd;
    }

    struct epoll_event event = {
        .events = EPOLLIN | EPOLLET,
        .data.fd = listen_fd
    };
    result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, listen_fd, &event);
    if (result != 0) {
        log_func(CRITICAL, "epoll_ctl (listen_fd)");
        goto do_close_listen_fd;
    }
    goto do_return;

do_close_listen_fd:
    close(listen_fd);
    listen_fd = -1;
do_return:
    return (listen_fd);
}

/**
 * Возвращает индекс рабочего процесса, которому следует делегировать работу с
 * новым соединением.
 */
static size_t schedule_new_connection(const manager_data_t *const manager)
{
    // Find the least loaded worker.
    size_t least_loaded = 0;
    size_t i;
    for (i = 1; i < manager->workers_count; ++i) {
        if (manager->workers[i].load < manager->workers[least_loaded].load) {
            least_loaded = i;
        }
    }

    return (least_loaded);
}

/**
 * Обрабатывает накопившиеся в очереди входящие соединения.
 */
static int process_incoming_connections(manager_data_t *const manager)
{
    struct sockaddr_storage address = {0};
    socklen_t addr_len = 0;
    char str_address[INET6_ADDRSTRLEN];

    int state = 0;
    while (!manager->quit && state == 0) {
        const int new_fd = accept4(manager->listen_fd, (struct sockaddr *)&address,
                                   &addr_len, SOCK_NONBLOCK);
        if (new_fd < 0) {
            switch (errno) {
            case EAGAIN:
#if (EWOULDBLOCK != EAGAIN)
            case EWOULDBLOCK:
#endif
            case ENETDOWN:
            case EPROTO:
            case ENOPROTOOPT:
            case EHOSTDOWN:
            case ENONET:
            case EHOSTUNREACH:
            case EOPNOTSUPP:
            case ENETUNREACH:
                state = 1;
                break;
            default:
                log_func(WARNING, "accept4");
                state = -1;
            }
        } else {
            print_address(str_address, sizeof(str_address),
                          (struct sockaddr *)&address, addr_len);
            log_message(VERBOSE, "Accepted connection from %s.", str_address);

            const int on = 1;
            int result = setsockopt(new_fd, IPPROTO_TCP, TCP_NODELAY, &on, sizeof(on));
            if (result != 0) {
                log_func(WARNING, "setsockopt (TCP_NODELAY)");
            }

            const size_t index = schedule_new_connection(manager);
            log_message(VERBOSE, "Forwarding connection to worker %d", index);
            result = pass_connection(manager->workers[index].uds_fd, new_fd,
                                     str_address, strlen(str_address) + 1);
            if (result < 0) {
                log_func_custom(WARNING, "send_fd", "Unable to forward connection. Closing it.");
            } else {
                ++manager->workers[index].load;
            }
            result = close(new_fd);
            if (result != 0) {
                log_func(WARNING, "close (new_fd)");
            }
        }
    }

    return (state >= 0 ? 0 : -1);
}

/**
 * Возвращает 0, если структуры данных о рабочих процессах \arg left и
 * \arg right имеют одинаковые идентификаторы.
 */
int equal_pid(const void *const left, const void *const right)
{
    const worker_info_t *const left_info = (const worker_info_t *)left;
    const worker_info_t *const right_info = (const worker_info_t *)right;
    return (left_info->pid == right_info->pid ? 0 : 1);
}

/**
 * Обрабатывает пришедшие сигналы. Возвращает 0 в случае успешного завершения и
 * -1 --- в случае ошибки.
 */
static int process_signals(manager_data_t *const manager,
                           const options_t *const options)
{
    struct signalfd_siginfo siginfo;

    int state = 0;
    while (state == 0) {
        const int count = read(manager->signal_fd, &siginfo, sizeof(siginfo));
        if (count == sizeof(siginfo)) {
            if (manager->quit) {
                log_message(WARNING, "<manager> Unhandled signal %s",
                            strsignal(siginfo.ssi_signo));
            }

            switch (siginfo.ssi_signo) {
            case SIGCHLD: {
                const worker_info_t key = {.pid = siginfo.ssi_pid};
                worker_info_t *const crashed =
                    lfind(&key, manager->workers, &manager->workers_count,
                          sizeof(key), &equal_pid);

                if (crashed != NULL) {
                    log_message(WARNING, "One of the workers has crashed. Restarting.");

                    const int result = close(crashed->uds_fd);
                    if (result != 0) {
                        log_func(WARNING, "close (uds_fd)");
                    }
                    crashed->uds_fd = -1;

                    crashed->load = 0;
                    crashed->pid = spawn_worker(&crashed->uds_fd, manager, options);
                    if (crashed->pid < 0) {
                        log_message(WARNING, "Unable to start new worker.");
                    }
                } else {
                    // @todo: This is the logger who crashed. Respawn it?
                }
                break;
            }
            case SIGUSR1: {
                const worker_info_t key = {.pid = siginfo.ssi_pid};
                worker_info_t *const sender =
                    lfind(&key, manager->workers, &manager->workers_count,
                          sizeof(key), &equal_pid);

                if (sender != NULL) {
                    --sender->load;
                } else {
                    assert(0);
                }
                break;
            }
            case SIGINT:
            case SIGTERM:
            case SIGQUIT:
                manager->quit = 1;
                break;
            case SIGPIPE:
                // This 'case' is needed in order to see write's return value.
                break;
            default:
                log_message(WARNING, "<manager> Unexpected signal %s",
                            strsignal(siginfo.ssi_signo));
                assert(0);
            }
        } else if (count >= 0) {
            log_func_custom(WARNING, "read (signalfd_siginfo)", "Incomplete read.");
        } else {
            switch (errno) {
            case EAGAIN:
#if (EWOULDBLOCK != EAGAIN)
            case EWOULDBLOCK:
#endif
                state = 1;
                break;
            default:
                log_func(CRITICAL, "read (signalfd_siginfo)");
                state = -1;
            }
        }
    }

    return (state >= 0 ? 0 : -1);
}

#define MANAGER_EPOLL_EVENTS_COUNT 2

/**
 * Основной цикл управляющего процесса.
 */
static int manager_loop(manager_data_t *const manager,
                        const options_t *const options)
{
    struct epoll_event events[MANAGER_EPOLL_EVENTS_COUNT];

    int loop_result = 0;
    while (!manager->quit && loop_result == 0) {
        const int fds_count = epoll_wait(manager->epoll_fd, events, MANAGER_EPOLL_EVENTS_COUNT, -1);
        if (fds_count < 0) {
            switch (errno) {
            case EINTR:
                break;
            default:
                log_func(WARNING, "epoll_wait");
            }
            continue;
        }

        for (int i = 0; !manager->quit && loop_result == 0 && i < fds_count; ++i) {
            if ((events[i].events & EPOLLIN) != 0x0) {
                if (events[i].data.fd == manager->listen_fd) {
                    loop_result = process_incoming_connections(manager);
                } else if (events[i].data.fd == manager->signal_fd) {
                    loop_result = process_signals(manager, options);
                } else {
                    log_message(WARNING, "<manager> Unexpected epoll event on FD = %d.", events[i].data.fd);
                    assert(0);
                }
                loop_result = (loop_result == 0 && (events[i].events & (EPOLLHUP | EPOLLERR)) != 0x0);
            }
        }
    }

    return (loop_result);
}

int manager_entry(const options_t *const options)
{
    int initialized = 0;
    manager_data_t manager = {
        .quit = 0,
        .epoll_fd = -1,
        .signal_fd = -1,
        .listen_fd = -1,
        .workers = NULL,
        .workers_count = options->workers_count
    };

    LOG_LEVEL = options->log_level;

    const pid_t logger_pid = spawn_logger(options->user, options->group,
                                          options->log_file, options->print);
    if (logger_pid < 0) {
        goto do_return;
    }
    manager.workers_pgid = logger_pid;

    log_message(INFO, "Serving domain '%s'", options->domain);

    manager.epoll_fd = epoll_create1(0x0);
    if (manager.epoll_fd < 0) {
        log_func(CRITICAL, "epoll_create1");
        goto do_terminate_logger;
    }

    manager.listen_fd = listen_for_connections(manager.epoll_fd, options->host,
                        options->port);
    if (manager.listen_fd < 0) {
        goto do_close_epoll_fd;
    }

    int result = downgrade_privileges(options->user, options->group);
    if (result < 0) {
        goto do_close_listen_fd;
    }

    result = access(options->mail_dir, R_OK | W_OK | X_OK);
    if (result != 0) {
        log_func(CRITICAL, "access (mail_dir)");
        goto do_close_listen_fd;
    }

    result = access(options->queue_dir, R_OK | W_OK | X_OK);
    if (result != 0) {
        log_func(CRITICAL, "access (queue_dir)");
        goto do_close_listen_fd;
    }

    manager.signal_fd = listen_for_signals(manager.epoll_fd);
    if (manager.signal_fd < 0) {
        goto do_close_listen_fd;
    }

    manager.workers = malloc(manager.workers_count * sizeof(*manager.workers));
    if (manager.workers == NULL) {
        log_func_custom(CRITICAL, "malloc (worker_info_t)", "Unable to allocate memory.");
        goto do_close_signal_fd;
    }
    for (int i = 0; i < manager.workers_count; ++i) {
        manager.workers[i].pid = -1;
        manager.workers[i].uds_fd = -1;
    }

    log_message(INFO, "Spawning workers...");
    const int spawned_count = spawn_workers(&manager, options);
    if (spawned_count < manager.workers_count) {
        if (spawned_count == 0) {
            log_message(CRITICAL, "Unable to spawn any worker.");
            goto do_free_workers_info;
        }
        manager.workers = realloc(manager.workers, spawned_count * sizeof(*manager.workers));
        manager.workers_count = spawned_count;
        log_message(WARNING, "Less number of workers were spawned.");
    }
    log_message(INFO, "%d workers were spawned.", manager.workers_count);

    initialized = 1;
    const int loop_result = manager_loop(&manager, options);

    result = setpgid(logger_pid, getpgrp());
    const int logger_joined_manager = (result == 0);
    if (!logger_joined_manager) {
        log_func(WARNING, "setpgid");
        ++manager.workers_count;
    }

    terminate_workers(logger_pid, manager.workers_count);
    cleanup_uds_fds(manager.workers, manager.workers_count);
do_free_workers_info:
    free(manager.workers);
do_close_signal_fd:
    result = close(manager.signal_fd);
    if (result != 0) {
        log_func(WARNING, "close (signal_fd)");
    }
do_close_listen_fd:
    result = close(manager.listen_fd);
    if (result != 0) {
        log_func(WARNING, "close (listen_fd)");
    }
do_close_epoll_fd:
    result = close(manager.epoll_fd);
    if (result != 0) {
        log_func(WARNING, "close (epoll_fd)");
    }
do_terminate_logger:
    if (!initialized || logger_joined_manager) {
        terminate_process(logger_pid);
        result = close(LOG_FD);
        if (result != 0) {
            log_func(WARNING, "close (LOG_FD)");
        }
    }
do_return:
    if (!initialized) {
        log_message(CRITICAL, "Unable to start SMTP-server.");
    }
    return (initialized && loop_result == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
