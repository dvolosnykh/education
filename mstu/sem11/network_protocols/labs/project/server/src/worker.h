#ifndef WORKER_H
#define WORKER_H

/*!
 *  \file worker.h
 *  \brief В этом файле содержится описание точки входа рабочего процесса.
 */

/*!
 *  Точка входа рабочего процесса SMTP-сервера.
 *  \param[in] uds_fd Дескриптор сокета домена Unix. Используется для приёма от
 *      управляющего процесса дескрипторов сокетов новых соединений.
 *  \param[in] domain Имя обслуживаемого домена.
 *  \param[in] mail_dir Путь к каталогу локальной почты.
 *  \param[in] queue_dir Путь к каталогу очереди нелокальной почты.
 *  \returns Код возврата рабочего процесса. 0 - успешное завершение,
 *      иначе - аварийное.
 */
int worker_entry(int uds_fd, const char *domain, const char *mail_dir,
                 const char *queue_dir);

#endif
