#ifndef CONNECTIONS_H
#define CONNECTIONS_H

/*!
 *  \file connections.h
 *  \brief Структуры данных соединений и функции для работы с ними.
 *
 *  В этом файле содержится перечисление битовых масок флагов активного
 *  соединения, структуры данных содинений (активных и ожидающих обработки),
 *  вспомогательные структуры данных (элемент списка адресов получателей), а
 *  также функции для работы с ними.
 */


#include <sys/queue.h>
#include <stdint.h>
#include <stddef.h>
#include "smtpserver-fsm.h"
#include "smtp_limits.h"


/*!
 *  Перечисление битовых масок флагов активного соединения.
 */
enum {
    /*!
     *  Этот бит устанавливается в 1, когда активное соединение находится в
     *  командном режиме, и в 0, когда в режиме получения данных сообщения.
     */
    AC_CMD_MODE = 0x1,
    /**
     *  Этот бит устанавливается в 1, если клиент запросил соединение
     *  командой \em EHLO, и в 0, если командой \em HELO.
     */
    AC_EXTENSIONS = 0x2
};


/*!
 *  Структура данных адреса получателя.
 */
struct recipient {
    /*!
     *  Служебное поле однонаправленного списка получателей.
     */
    STAILQ_ENTRY(recipient) links;
    int at_position;                /*!< Индекс коммерческого \@ в адресе получателя.*/
    char target[LIMIT_EMAIL + 1];   /*!< Адрес получателя. */
};

/*!
 *  Структура данных активного соединения.
 */
struct active_connection {
    /*!
     *  Служебное поле двунаправленного списка активных соединений.
     */
    TAILQ_ENTRY(active_connection) links;
    int fd;             /*!< Дескриптор сокета соединения. */
    char *address;      /*!< Строка IP-адреса источника соединения. */
    size_t address_len; /*!< Длина строки IP-адреса источника соединения. */
    /*
     * Active connection's specific field follow:
     */
    int timeout_fd;             /*!< Дескриптор таймаута. */
    uint32_t flags;             /*!< Набор атрибутов соединения. */
    te_smtp_state smtp_state;   /*!< Состояние КА SMTP-протокола. */
    STAILQ_HEAD(recipients, recipient) recipients;  /*!< Список получателей. */
    int at_position;        /*!< Индекс коммерческого \@ в адресе отправителя.*/
    size_t line_start;      /*!< Позиция, с которой начинается текущая строка. */
    /*!
     *  Позиция, с которой следует обрабатывать содержимое в буффере.
     */
    size_t buffer_offset;
    size_t buffer_length;   /*!< Длина содержимого в буфере. */
    size_t message_limit;   /*!< Допустимая длина сообщения. */
    char domain[LIMIT_DOMAIN + 1];  /*!< Домен инициировавший сессию. */
    char source[LIMIT_EMAIL + 1];   /*!< Адрес отправителя. */
    /*!
     *  Буффер приёма команд и данных транзакции.
     */
    char buffer[LIMIT_MESSAGE_CONTENT];
};


/*!
 *  Структура данных соединения, ожидающего обработки.
 */
struct pending_connection {
    /*!
     *  Служебное поле однонаправленного списка соединений, ожидающих обработки.
     */
    STAILQ_ENTRY(pending_connection) links;
    int fd;             /*!< Дескриптор сокета соединения. */
    char *address;      /*!< Строка IP-адреса источника соединения. */
    size_t address_len; /*!< Длина строки IP-адреса источника соединения. */
};


/*!
 *  Закрывает соединение, ожидающее обработки.
 *  \param[in] connection Указатель на закрываемое соединение.
 */
void close_pending_connection(pending_connection_t *connection);
/*!
 *  Закрывает активное соединение.
 *  \param[in] connection Указатель на закрываемое соединение.
 */
void close_active_connection(active_connection_t *connection);

/*!
 *  Сброс соединения в инициализированное состояние.
 *  \param[in] connection Указатель на сбрасываемое в инициализированное
 *      состояние соединение.
 *  \sa clear_recipients(), clear_buffer().
 */
void reset_connection(active_connection_t *connection);
/*!
 *  Очищает список получателей у активного соединения.
 *  \param[in] connection Указатель на активное соединение.
 */
void clear_recipients(active_connection_t *connection);
/*!
 *  Очищает буфер активного соединения.
 *  \param[in] connection Указатель на активное соединение.
 */
void clear_buffer(active_connection_t *connection);
/*!
 *  Сохраняет ответы сервера для дальнейшей отправки. Текст ответов формируется
 *  по правилам функции <a href="http://linux.die.net/man/3/printf">printf()</a>.
 *  \param[in] connection Указатель на активное соединение.
 *  \param[in] format Форматирующая строка сохраняемого сообщениея. За ней
 *      следует список параметров в соответствии с форматирующей строкой.
 */
void submit_reply(active_connection_t *const connection, const char format[], ...);
/*!
 *  Обрабатывает входящие данные активного соединения.
 *  \param[in] connection Указатель на активное соединение.
 *  \param[out] toggle_duplex_mode Указатель на флаг необходимости переключить
 *      дуплексный режим работы соединения.
 *  \returns 0 в случае успеха. В противном случае, возвращает отрицательное
 *      значение.
 */
int process_input(active_connection_t *connection, int *toggle_duplex_mode);
/*!
 *  Обрабатывает исходящие данные активного соединения.
 *  \param[in] connection Указатель на активное соединение.
 *  \param[out] toggle_duplex_mode Указатель на флаг необходимости переключить
 *      дуплексный режим работы соединения.
 *  \returns 0 в случае успеха. В противном случае, возвращает отрицательное
 *      значение.
 */
int process_output(active_connection_t *connection, int *toggle_duplex_mode);

#endif
