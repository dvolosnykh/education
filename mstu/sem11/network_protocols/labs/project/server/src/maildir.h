#ifndef MAILDIR_H
#define MAILDIR_H

/*!
 *  \file maildir.h
 *  \brief В этом файле содержится набор функций, определяющих интерфейс к
 *  каталогу локальной почты.
 */


#include <stdlib.h>


/*!
 *  Задаёт корневой каталог почтовых ящиков пользователей.
 *  \param[in] path Путь к каталогу.
 *  \returns 0 в случае успеха. В противном случае, возвращает отрицательное
 *      значение.
 */
int maildir_set_path(const char path[]);

/*!
 *  Сохраняет сообщение \a message от хоста \a host в почтовый ящик \a mailbox.
 *  В начало сообщения, - согласно
 *  <a href="http://www.faqs.org/rfcs/rfc2821.html">RFC 2821</a> (см.
 *  раздел 4.4 Trace Information), - добавлются:
 *  \li строка обратного пути \a return_path;
 *  \li строка временной метки \a received соответствующая времени \a timestamp.
 *  \param[in] mailbox Почтовый ящик адресата сообщения.
 *  \param[in] mailbox_len Длина строки почтового ящика.
 *  \param[in] host Имя хоста, передавшего сообщение.
 *  \param[in] timestamp Временная метка момента передачи сообщения. Измеряется
 *      в секундах от начала отсчёта времени Unix, т.н. \em Epoch
 *      (1970-01-01T00:00:00Z ISO 8601).
 *  \param[in] return_path Обратный путь к почтовому ящику отправителя
 *      сообщения.
 *  \param[in] return_path_len Длина строки обратного пути к почтовому ящику
 *      отправителя сообщения.
 *  \param[in] received Строка временной метки, добавляемая SMTP-сервером в
 *      начало сообщения при его обработке.
 *  \param[in] received_len Длина строки временной метки.
 *  \param[in] message Текст сохраняемого сообщения.
 *  \param[in] message_len Длина сохраняемого сообщения.
 *  \returns 0 в случае успеха. В противном случае, возвращает отрицательное
 *      значение.
 */
int maildir_save_message(const char mailbox[], size_t mailbox_len,
                         const char host[], time_t timestamp,
                         const char return_path[], size_t return_path_len,
                         const char received[], size_t received_len,
                         const char message[], size_t message_len);

#endif
