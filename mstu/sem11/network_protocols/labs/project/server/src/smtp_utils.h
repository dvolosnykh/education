#ifndef SMTP_UTILS_H
#define SMTP_UTILS_H

/*!
 *  \file smtp_utils.h
 *  \brief В данном файле содержатся функции выполняющие разносортные
 *      вспомогательные задачи в интересах сетевой службы SMTP-сервер.
 */


#include <stdlib.h>


/// \cond
#define CRLF    "\r\n"
#define CRLF_LEN    2
/// \endcond

/*!
 *  Максимальная длина строки временной метки.
 */
#define TIMESTAMP_LEN   (34 + LIMIT_DOMAIN + INET6_ADDRSTRLEN + 26 + CRLF_LEN)
/*!
 *  Максимальная длина строки обратного пути.
 */
#define RETURN_PATH_LEN (15 + LIMIT_EMAIL + CRLF_LEN)


/*!
 *  Формирует строку временной метки, добавляемой в начало сообщения при его
 *  обработке.
 *  \param[out] buffer Выходной буффер для записи строки временной метки.
 *  \param[in] max Длина выходного буфера \a buffer.
 *  \param[in] remote_host Имя хоста передавшего сообщение.
 *  \param[in] address Строка IP-адреса хоста передавшего сообщение.
 *  \param[in] this_domain Имя обслуживаемого данной сетевой службой домена.
 *  \param[in] timestamp Временная метка момента передачи сообщения. Измеряется
 *      в секундах от начала отсчёта времени Unix, т.н. \em Epoch
 *      (1970-01-01T00:00:00Z ISO 8601).
 *  \returns Длину записанной строки в буфер \a buffer, которая не превышает
 *      \a max символов включая терминальный ноль. В случае ошибок, возвращает
 *      отрицательное значение.
 */
int make_timestamp(char buffer[], size_t max, const char remote_host[],
                   const char address[], const char this_domain[],
                   time_t timestamp);

/*!
 *  Формирует строку обратного пути, добавляюмую в начало сообщения при его
 *  окончательной доставке.
 *  \param[out] buffer Выходной буффер для записи строки обратного пути.
 *  \param[in] max Длина выходного буфера \a buffer.
 *  \param[in] mailbox Почтовый ящик адресата сообщения.
 *  \returns Длину записанной строки в буфер \a buffer, которая не превышает
 *      \a max символов включая терминальный ноль. В случае ошибок, возвращает
 *      отрицательное значение.
 */
int make_return_path(char buffer[], size_t max, const char mailbox[]);


#endif
