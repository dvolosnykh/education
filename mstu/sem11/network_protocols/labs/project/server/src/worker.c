#include "logapi.h"
#include "common.h"
#include "maildir.h"
#include "queuedir.h"
#include "utils.h"
#include "connections.h"
#include "commands.h"
#include "settings.h"
#include <stdlib.h>
#include <sys/queue.h>
#include <sys/signalfd.h>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <fcntl.h>
#include <netdb.h>
#include <signal.h>
#include <unistd.h>
#include <assert.h>


/**
 * Готовая структура данных для интервала времени тайматуа по отсутствию
 * какой-либо деятельности на активном соединении. Для удобства.
 */
static const struct itimerspec TIMEOUT = {.it_value = {CONNECTION_TIMEOUT}};


/**
 * Структура данных, необходимых для работы рабочих процессов.
 */
typedef struct {
    int quit;       /**< Флаг завершения работы. */
    int epoll_fd;   /**< Дескриптор ожидания различных событий. */
    int signal_fd;  /**< Дескриптор очереди сигналов. */
    int activation_fd;  /**< Дескриптор события активации соединений, ожидающих обработки. */
    int uds_fd;     /**< Дескриптор сокета домена Unix (от управляющего процесса). */
    int connections_fd; /**< Дескриптор ожидания событий от активных соединений. */
    /**
     * Дескриптор ожидания событий от таймеров активных соединений.
     */
    int timeouts_fd;
    /**
     * Очередь соединений, ожидающих обработки.
     */
    STAILQ_HEAD(pending, pending_connection) pending_connections;
    /**
     * Длина очереди соединений, ожидающих обработки.
     */
    size_t pending_length;
    /**
     * Очередь активных соединений.
     */
    TAILQ_HEAD(active, active_connection) active_connections;
    /**
     * Количество элементов в дереве активных соединений.
     */
    size_t active_length;
} worker_data_t;


/**
 * Переводит в активный режим как можно больше соединений, ожидающих обработки.
 */
static void activate_pending_connections(worker_data_t *const worker)
{
    uint64_t count; // unused
    int result = read(worker->activation_fd, &count, sizeof(count));
    if (result < 0) {
        log_func(WARNING, "read (activation_fd)");
    }

    result = 0;
    while (result == 0 && !STAILQ_EMPTY(&worker->pending_connections)) {
        pending_connection_t *const pending = STAILQ_FIRST(&worker->pending_connections);

        active_connection_t *const active = malloc(sizeof(*active));
        if (active == NULL) {
            log_func_custom(WARNING, "malloc", "Unable to allocate memory for connection to be activated.");
            result = -1;
            continue;
        }

        struct epoll_event event = {
            .events = EPOLLOUT | EPOLLRDHUP,
            .data.ptr = active
        };
        result = epoll_ctl(worker->connections_fd, EPOLL_CTL_ADD, pending->fd, &event);
        if (result != 0) {
            switch (errno) {
            case ENOMEM:
            case ENOSPC:
                break;
            default:
                log_func(CRITICAL, "epoll_ctl (add)");
            }
            goto do_free_active;
        }

        active->timeout_fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
        if (result < 0) {
            log_func(CRITICAL, "timerfd_create");
            goto do_ctl_del_connections_fd;
        }

        result = timerfd_settime(active->timeout_fd, 0x0, &TIMEOUT, NULL);
        if (result != 0) {
            log_func(CRITICAL, "timerfd_settime");
            goto do_ctl_del_connections_fd;
        }

        event.events = EPOLLIN | EPOLLET;
        result = epoll_ctl(worker->timeouts_fd, EPOLL_CTL_ADD, active->timeout_fd, &event);
        if (result != 0) {
            log_func(CRITICAL, "epoll_ctl");
            goto do_close_timeout_fd;
        }

        active->fd = pending->fd;
        active->address = pending->address;
        active->address_len = pending->address_len;
        active->flags = AC_CMD_MODE;
        STAILQ_INIT(&active->recipients);

        active->smtp_state =
            smtp_step(SMTP_ST_INIT, SMTP_EV_OPENNED, active, SMTP_BAD_CMD, NULL, 0);

        TAILQ_INSERT_TAIL(&worker->active_connections, active, links);
        ++worker->active_length;

        STAILQ_REMOVE_HEAD(&worker->pending_connections, links);
        --worker->pending_length;
        free(pending);
        continue;

do_close_timeout_fd:
        result = close(active->timeout_fd);
        if (result != 0) {
            log_func(CRITICAL, "close (timeout_fd)");
        }
do_ctl_del_connections_fd:
        result = epoll_ctl(worker->connections_fd, EPOLL_CTL_DEL, pending->fd, NULL);
        if (result != 0) {
            log_func(CRITICAL, "epoll_ctl (del)");
        }
do_free_active:
        free(active);
    }

    log_message(VERBOSE, "Active: %zu\tPending: %zu", worker->active_length,
                worker->pending_length);
}

/**
 * Принимает пришедшие дескрипторы сокетов новых соединений и регистрирует их
 * в дескрпиторе ожидания событий.
 */
static int receive_new_connections(worker_data_t *const worker)
{
    int state = 0;
    while (state == 0) {
        pending_connection_t *const connection = malloc(sizeof(*connection));
        if (connection == NULL) {
            log_func_custom(WARNING, "malloc", "Unable to allocate memory for new connection.");
            state = -1;
            errno = ENOMEM;
            continue;
        }

        connection->address = malloc(INET6_ADDRSTRLEN);
        if (connection->address == NULL) {
            log_func_custom(WARNING, "malloc", "Unable to allocate memory for new connection's address literal.");
            state = -1;
            errno = ENOMEM;
            goto do_free_connection;
        }

        const int new_fd =
            receive_connection(worker->uds_fd, connection->address, INET6_ADDRSTRLEN);
        if (new_fd < 0) {
            switch (errno) {
            case EAGAIN:
#if (EWOULDBLOCK != EAGAIN)
            case EWOULDBLOCK:
#endif
                state = 1;
                break;
            default:
                log_func(WARNING, "receive_fd");
                state = -1;
            }
            goto do_free_address;
        }

        log_message(DEBUG, "New socket FD = %d", new_fd);
        connection->fd = new_fd;

        STAILQ_INSERT_TAIL(&worker->pending_connections, connection, links);
        ++worker->pending_length;

        raise_event(worker->activation_fd);
        continue;

do_free_address:
        free(connection->address);
do_free_connection:
        free(connection);
    }

    return (state > 0 ? 0 : -1);
}

/**
 * Обрабатывает пришедшие сигналы. Возвращает 0 в случае успешного завершения и
 * -1 --- в случае ошибки.
 */
static int process_signals(worker_data_t *const worker)
{
    struct signalfd_siginfo siginfo;

    int state = 0;
    while (state == 0) {
        const int count = read(worker->signal_fd, &siginfo, sizeof(siginfo));
        if (count == sizeof(siginfo)) {
            if (worker->quit) {
                log_message(WARNING, "<worker> Unhandled signal %s",
                            strsignal(siginfo.ssi_signo));
            }

            switch (siginfo.ssi_signo) {
            case SIGINT:
            case SIGTERM:
            case SIGQUIT:
                worker->quit = 1;
                break;
            case SIGUSR1:
                state = -1;
                break;
            case SIGUSR2:
                // @todo use for testing purposes --- do something else.
                break;
            case SIGPIPE:
                // This 'case' is needed in order to see write's return value.
                break;
            default:
                log_message(WARNING, "<worker> Unexpected signal %s",
                            strsignal(siginfo.ssi_signo));
                assert(0);
            }
        } else if (count >= 0) {
            log_func_custom(WARNING, "read (signalfd_siginfo)", "Incomplete read.");
        } else {
            switch (errno) {
            case EAGAIN:
#if (EWOULDBLOCK != EAGAIN)
            case EWOULDBLOCK:
#endif
                state = 1;
                break;
            default:
                log_func(CRITICAL, "read (signalfd_siginfo)");
                state = -1;
            }
        }
    }

    return (state > 0 ? 0 : -1);
}


/**
 * Удаляет из очереди активное соединение и закрывает его, освобождая ресурсы.
 */
static void remove_active_connection(worker_data_t *const worker,
                                     active_connection_t *const connection)
{
    log_message(DEBUG, "Closing socket FD = %d", connection->fd);

    TAILQ_REMOVE(&worker->active_connections, connection, links);
    --worker->active_length;
    close_active_connection(connection);

    raise_event(worker->activation_fd);
}


#define ACTIVE_CONNECTIONS_EVENTS_COUNT 32

/**
 * Обрабатывает события по таймаутам активных соединений.
 */
static void process_timeouts(worker_data_t *const worker)
{
    struct epoll_event events[ACTIVE_CONNECTIONS_EVENTS_COUNT];

    while (1) {
        const int fds_count = epoll_wait(worker->timeouts_fd, events,
                                         ACTIVE_CONNECTIONS_EVENTS_COUNT, 0);
        if (fds_count < 0) {
            switch (errno) {
            case EINTR:
                break;
            default:
                log_func(WARNING, "epoll_wait");
            }
            continue;
        }
        if (fds_count == 0) break;
        log_message(DEBUG, "Timeouted connections: %d", fds_count);

        for (int i = 0; i < fds_count; ++i) {
            active_connection_t *const connection = events[i].data.ptr;

            uint64_t count = 0; // unused
            int result = read(connection->timeout_fd, &count, sizeof(count));
            if (result < 0) {
                log_func(WARNING, "read (activation_fd)");
            }
            assert(count > 0);

            remove_active_connection(worker, connection);
        }
    }
}

/**
 * Обрабатывает события, поступившие от активных соединений.
 */
static void process_active_connections(worker_data_t *const worker)
{
    struct epoll_event events[ACTIVE_CONNECTIONS_EVENTS_COUNT];

    while (1) {
        const int fds_count = epoll_wait(worker->connections_fd, events,
                                         ACTIVE_CONNECTIONS_EVENTS_COUNT, 0);
        if (fds_count < 0) {
            switch (errno) {
            case EINTR:
                break;
            default:
                log_func(WARNING, "epoll_wait");
            }
            continue;
        }
        if (fds_count == 0) break;

        for (int i = 0; i < fds_count; ++i) {
            const int transfer_events = events[i].events & (EPOLLOUT | EPOLLIN);
            active_connection_t *const connection = events[i].data.ptr;

            log_message(DEBUG, "Transfer mask: %#010x;\tFlags: %#010x;",
                        transfer_events, connection->flags);

            int toggle_duplex_mode = 0;
            int result = 0;
            if ((transfer_events & EPOLLIN) != 0x0) {
                log_message(DEBUG, "Alive connections: %d", fds_count);
                result = process_input(connection, &toggle_duplex_mode);
            } else {
                result = process_output(connection, &toggle_duplex_mode);
            }
            int should_close = (result < 0);

            should_close |= ((events[i].events & (EPOLLHUP | EPOLLERR | EPOLLRDHUP)) != 0x0);
            if (should_close) {
                if ((events[i].events & EPOLLHUP) != 0x0) {
                    log_message(WARNING, "Connection closed (HUP).");
                } else if ((events[i].events & EPOLLRDHUP) != 0x0) {
                    log_message(WARNING, "Remote peer has closed connection (RDHUP).");
                } else if ((events[i].events & EPOLLERR) != 0x0) {
                    log_message(WARNING, "Error when talking to remote peer (ERR).");
                }
            }

            if (!should_close && toggle_duplex_mode) {
                struct epoll_event event = {
                    .events = (transfer_events ^ (EPOLLOUT | EPOLLIN)) | EPOLLRDHUP,
                    .data.ptr = connection
                };
                result = epoll_ctl(worker->connections_fd, EPOLL_CTL_MOD, connection->fd, &event);
                if (result != 0) {
                    log_func(CRITICAL, "epoll_ctl");
                    should_close = 1;
                }

                if (!should_close && (transfer_events & EPOLLOUT) != 0x0) {
                    result = timerfd_settime(connection->timeout_fd, 0x0, &TIMEOUT, NULL);
                    if (result != 0) {
                        log_func(CRITICAL, "timerfd_settime");
                        should_close = 1;
                    }
                }
            }

            if (should_close) {
                remove_active_connection(worker, connection);
            }
        }
    }
}

#define WORKER_EPOLL_EVENTS_COUNT 3

/**
 * Основной цикл рабочего процесса.
 */
static int worker_loop(worker_data_t *const worker)
{
    struct epoll_event events[WORKER_EPOLL_EVENTS_COUNT];

    int loop_result = 0;
    while (!worker->quit && loop_result == 0) {
        const int fds_count = epoll_wait(worker->epoll_fd, events, WORKER_EPOLL_EVENTS_COUNT, -1);
        if (fds_count < 0) {
            switch (errno) {
            case EINTR:
                break;
            default:
                log_func(WARNING, "epoll_wait");
            }
            continue;
        }

        for (int i = 0; !worker->quit && loop_result == 0 && i < fds_count; ++i) {
            if ((events[i].events & EPOLLIN) != 0x0) {
                if (events[i].data.fd == worker->connections_fd) {
                    log_message(DEBUG, "process_active_connections");
                    process_active_connections(worker);
                } else if (events[i].data.fd == worker->activation_fd) {
                    log_message(DEBUG, "activate_pending_connections");
                    activate_pending_connections(worker);
                } else if (events[i].data.fd == worker->uds_fd) {
                    log_message(DEBUG, "receive_new_connections");
                    loop_result = receive_new_connections(worker);
                } else if (events[i].data.fd == worker->signal_fd) {
                    log_message(DEBUG, "process_signals");
                    loop_result = process_signals(worker);
                } else if (events[i].data.fd == worker->timeouts_fd) {
                    log_message(DEBUG, "process_timeouts");
                    process_timeouts(worker);
                } else {
                    log_message(WARNING, "<worker> Unexpected epoll event on FD = %d.", events[i].data.fd);
                    assert(0);
                }
            }
            loop_result = (loop_result == 0 && (events[i].events & (EPOLLHUP | EPOLLERR)) != 0x0);
        }
    }

    return (loop_result);
}

/**
 * Создаёт и регистрирует в основном дескрипторе событий \arg epoll_fd
 * дополнительный дескриптор событий.
 */
static int sub_epoll_create(const int epoll_fd)
{
    int connections_fd = epoll_create1(0x0);
    if (connections_fd < 0) {
        log_func(CRITICAL, "epoll_create1 (sub epoll)");
        goto do_return;
    }

    struct epoll_event event = {
        .events = EPOLLIN | EPOLLET,
        .data.fd = connections_fd
    };
    int result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, connections_fd, &event);
    if (result != 0) {
        log_func(CRITICAL, "epoll_ctl (sub epoll)");
        goto do_close_active_fd;
    }
    goto do_return;
do_close_active_fd:
    result = close(connections_fd);
    if (result != 0) {
        log_func(WARNING, "close (sub epoll)");
    }
    connections_fd = -1;
do_return:
    return (connections_fd);
}

/**
 * Очищает очередь соединений, ожидающих обработку.
 */
static void clear_pending_connections(worker_data_t *const worker)
{
    while (!STAILQ_EMPTY(&worker->pending_connections)) {
        pending_connection_t *const first = STAILQ_FIRST(&worker->pending_connections);
        STAILQ_REMOVE_HEAD(&worker->pending_connections, links);
        close_pending_connection(first);
    }
    worker->pending_length = 0;
}

/**
 * Очищает очередь активных соединений.
 */
static void clear_active_connections(worker_data_t *const worker)
{
    while (!TAILQ_EMPTY(&worker->active_connections)) {
        active_connection_t *const first = TAILQ_FIRST(&worker->active_connections);
        TAILQ_REMOVE(&worker->active_connections, first, links);
        close_active_connection(first);
    }
    worker->active_length = 0;
}

int worker_entry(const int uds_fd, const char *const domain,
                 const char *const mail_dir, const char *const queue_dir)
{
    int initialized = 0;
    worker_data_t worker = {
        .quit = 0,
        .epoll_fd = -1,
        .signal_fd = -1,
        .activation_fd = -1,
        .uds_fd = uds_fd,
        .connections_fd = -1,
        .timeouts_fd = -1,
        .pending_connections = STAILQ_HEAD_INITIALIZER(worker.pending_connections),
        .pending_length = 0,
        .active_connections = TAILQ_HEAD_INITIALIZER(worker.active_connections),
        .active_length = 0
    };

    SERVER_DOMAIN = domain;

    int result = maildir_set_path(mail_dir);
    if (result != 0) {
        log_func(CRITICAL, "maildir_set_path");
        goto do_clear_connections;
    }

    result = queuedir_set_path(queue_dir);
    if (result != 0) {
        log_func(CRITICAL, "queuedir_set_path");
        goto do_clear_connections;
    }

    worker.epoll_fd = epoll_create1(0x0);
    if (worker.epoll_fd < 0) {
        log_func(CRITICAL, "epoll_create1");
        goto do_clear_connections;
    }

    worker.signal_fd = listen_for_signals(worker.epoll_fd);
    if (worker.signal_fd < 0) {
        goto do_close_epoll_fd;
    }

    worker.activation_fd = listen_for_event(worker.epoll_fd);
    if (worker.activation_fd < 0) {
        goto do_close_signal_fd;
    }

    result = set_flags(worker.uds_fd, O_NONBLOCK);
    if (result < 0) {
        log_func(CRITICAL, "set_flags (uds_fd)");
        goto do_close_activation_fd;
    }

    struct epoll_event event = {
        .events = EPOLLIN | EPOLLET,
        .data.fd = worker.uds_fd
    };
    result = epoll_ctl(worker.epoll_fd, EPOLL_CTL_ADD, worker.uds_fd, &event);
    if (result != 0) {
        log_func(CRITICAL, "epoll_ctl (uds_fd)");
        goto do_close_signal_fd;
    }

    worker.connections_fd = sub_epoll_create(worker.epoll_fd);
    if (worker.connections_fd < 0) {
        goto do_close_signal_fd;
    }

    worker.timeouts_fd = sub_epoll_create(worker.epoll_fd);
    if (worker.timeouts_fd < 0) {
        goto do_close_connections_fd;
    }

    initialized = 1;
    const int loop_result = worker_loop(&worker);

    result = close(worker.timeouts_fd);
    if (result != 0) {
        log_func(WARNING, "close (timeouts_fd)");
    }
do_close_connections_fd:
    result = close(worker.connections_fd);
    if (result != 0) {
        log_func(WARNING, "close (connections_fd)");
    }
do_close_activation_fd:
    result = close(worker.activation_fd);
    if (result != 0) {
        log_func(WARNING, "close (activation_fd)");
    }
do_close_signal_fd:
    result = close(worker.signal_fd);
    if (result != 0) {
        log_func(WARNING, "close (signal_fd)");
    }
do_close_epoll_fd:
    result = close(worker.epoll_fd);
    if (result != 0) {
        log_func(WARNING, "close (epoll_fd)");
    }
do_clear_connections:
    clear_active_connections(&worker);
    clear_pending_connections(&worker);
    log_message(INFO, "worker has terminated.");
    return (initialized && loop_result == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
