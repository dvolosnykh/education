#ifndef PARAMETERS_RE_H
#define PARAMETERS_RE_H

/*! \file parameters_re.h
 *  \brief Строковые литералы регулярных выражений параметров команд
 *      SMTP-протокола.
 */


/*!
 *  \defgroup re_command_params Регулярные выражения параметров команд
 *  \ingroup re_top
 *  @{
 */

#define SMTP_PARAM_STR(name)    "\\A(?i:" name ")\\z"

#define SMTP_PARAM_VALUE_NUMBER     "\\A(?:0|[1-9][0-9]*)\\z"

/*!
 *  \defgroup re_mail_params Регулярные выражения параметров команды MAIL.
 *  @{
 */

#define SMTP_MAIL_PARAM_SIZE_STR            SMTP_PARAM_STR("SIZE")
#define SMTP_MAIL_PARAM_SIZE_VALUE_STR      SMTP_PARAM_VALUE_NUMBER

/*! @} */   // re_mail_params

/*! @} */   // re_command_params

#endif
