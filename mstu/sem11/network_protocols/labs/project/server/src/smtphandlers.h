#ifdef FSM_USER_HEADERS

#include "connections.h"
#include "commands.h"
#include "parameters.h"
#include "replies.h"
#include "maildir.h"
#include "queuedir.h"
#include "smtp_utils.h"
#include "logapi.h"
#include <assert.h>
#include <time.h>
#include <netdb.h>

#endif /*FSM_USER_HEADERS*/

#ifdef FSM_HANDLER_CODE

const char *SERVER_DOMAIN = NULL;


static const char *smtp_do_helo(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length)
{
    const char *reply_code;

    // Parse command arguments.
    const int groups_count = 2;
    const int ovecsize = groups_count * 3;
    int ovector[ovecsize];
    const int count =
        pcre_exec(commands[command_id].args_re, commands[command_id].args_extra,
                  arguments, length, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ERROR_IN_ARGUMENTS, reply_code, REPLY_SINGLE);
        return (reply_code);
    }
    assert(count > 0);

    // Extract arguments.
    const int domain_len = ovector[3] - ovector[2];
    const int result =
        pcre_copy_substring(arguments, ovector, count, 1, connection->domain,
                            domain_len + 1);
    assert(result == domain_len);

    connection->flags &= ~AC_EXTENSIONS;

    reply_code = REPLY_CODE_REQUESTED_MAIL_ACTION_OKAY_COMPLETED;
    reset_connection(connection);
    submit_reply(connection, REPLY_MSG_GREETING, reply_code, REPLY_SINGLE, SERVER_DOMAIN);
    return (reply_code);
}

static const char *smtp_do_ehlo(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length)
{
    const char *reply_code;

    // Parse command arguments.
    const int groups_count = 2;
    const int ovecsize = groups_count * 3;
    int ovector[ovecsize];
    const int count =
        pcre_exec(commands[command_id].args_re, commands[command_id].args_extra,
                  arguments, length, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ERROR_IN_ARGUMENTS, reply_code, REPLY_SINGLE);
        return (reply_code);
    }
    assert(count > 0);

    // Extract arguments.
    const int domain_len = ovector[3] - ovector[2];
    const int result =
        pcre_copy_substring(arguments, ovector, count, 1, connection->domain,
                            domain_len + 1);
    assert(result == domain_len);

    connection->flags |= AC_EXTENSIONS;

    reply_code = REPLY_CODE_REQUESTED_MAIL_ACTION_OKAY_COMPLETED;
    reset_connection(connection);
    submit_reply(connection, REPLY_MSG_GREETING, reply_code, REPLY_MORE, SERVER_DOMAIN);
//    submit_reply(connection, REPLY_MSG_8BITMIME, reply_code, REPLY_MORE);
    submit_reply(connection, REPLY_MSG_SIZE, reply_code, REPLY_MORE, LIMIT_MESSAGE_CONTENT - 1);
    submit_reply(connection, REPLY_MSG_EXPN, reply_code, REPLY_MORE);
    submit_reply(connection, REPLY_MSG_HELP, reply_code, REPLY_LAST);
    return (reply_code);
}

static const char *smtp_do_mail(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length)
{
    const char *reply_code;

    const int groups_count = 3;
    const int ovecsize = groups_count * 3;
    int ovector[ovecsize];

    // Parse command arguments.
    int count = pcre_exec(commands[command_id].args_re, commands[command_id].args_extra,
                          arguments, length, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ERROR_IN_ARGUMENTS, reply_code, REPLY_SINGLE);
        goto do_return;
    }
    assert(count > 0);

    const size_t args_len = ovector[1];
    const size_t mailbox_offset = ovector[2];
    const int mailbox_len = ovector[3] - mailbox_offset;

    // Check e-mail for correctness.
    count = pcre_exec(EMAIL_RE, NULL, arguments + mailbox_offset, mailbox_len, 0,
                      RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_MAILBOX_NAME_NOT_ALLOWED;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_MAILBOX_NAME_NOT_ALLOWED, reply_code, REPLY_SINGLE);
        goto do_return;
    }
    assert(count > 0);

    const int result =
        pcre_copy_substring(arguments + mailbox_offset, ovector, count, 0,
                            connection->source, mailbox_len + 1);
    assert(result == mailbox_len);
    connection->at_position = ovector[2];

    // Parse parameters.
    const char *params = arguments + args_len;
    count = pcre_exec(commands[command_id].params_re, commands[command_id].params_extra,
                      params, length - args_len, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ERROR_IN_PARAMETERS, reply_code, REPLY_SINGLE);
        goto do_return;
    }
    assert(count > 0);

    const int has_parameters = (count == 2);
    log_message(DEBUG, "Has %sparameters", (has_parameters ? "" : "no "));
    if (has_parameters) {
        const int params_len = ovector[3] - ovector[2];
        params += ovector[2];

        size_t offset = 0;
        while (offset < params_len) {
            count = pcre_exec(GET_PARAMETER_RE, NULL, params + offset,
                              params_len - offset, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
            assert(count > 0);

            const char *const name = params + offset + ovector[2];
            const size_t name_len = ovector[3] - ovector[2];

            log_message(DEBUG, "Parameter name: %.*s", (int)name_len, name);

            const mail_param_id_t mail_param_id =
                recognize_mail_parameter(name, name_len, NULL, 0);
            log_message(DEBUG, "MAIL param ID: %d", mail_param_id);
            if (mail_param_id == SMTP_BAD_MAIL_PARAM) {
                reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
                clear_buffer(connection);
                submit_reply(connection, REPLY_MSG_PARAMETER_NOT_IMPLEMENTED, reply_code, REPLY_SINGLE);
                goto do_return;
            }

            const int must_have_value = (mail_parameters[mail_param_id].value_re != NULL);
            const int has_value = (count == 3);
            if (must_have_value != has_value) {
                reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
                clear_buffer(connection);
                if (must_have_value) {
                    submit_reply(connection, REPLY_MSG_PARAMETER_MUST_HAVE_VALUE, reply_code, REPLY_SINGLE, name_len, name);
                } else {
                    submit_reply(connection, REPLY_MSG_PARAMETER_MUST_NOT_HAVE_VALUE, reply_code, REPLY_SINGLE, name_len, name);
                }
                goto do_return;
            }
            log_message(DEBUG, "must_have_value == has_value.");

            if (has_value) {
                const char *const value = params + offset + ovector[4];
                const size_t value_len = ovector[5] - ovector[4];
                count = pcre_exec(mail_parameters[mail_param_id].value_re, NULL,
                                  value, value_len, 0, RE_MAIL_PARAM_EXEC_OPTIONS,
                                  NULL, 0);
                if (count < 0) {
                    reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
                    clear_buffer(connection);
                    submit_reply(connection, REPLY_MSG_PARAMETER_HAS_WRONG_VALUE, reply_code, REPLY_SINGLE, name_len, name);
                    goto do_return;
                }

                log_message(DEBUG, "Parameter value: %.*s", (int)value_len, value);

                switch (mail_param_id) {
                case SMTP_MAIL_PARAM_SIZE: {
                    char *end;
                    const long size = strtol(value, &end, 10);
                    assert(end == value + value_len);

                    if (size > LIMIT_MESSAGE_CONTENT - 1) {
                        reply_code = REPLY_CODE_EXCEEDED_STORAGE_ALLOCATION;
                        clear_buffer(connection);
                        submit_reply(connection, REPLY_MSG_TOO_MUCH_DATA, reply_code, REPLY_SINGLE);
                        goto do_return;
                    }

                    connection->message_limit = (size_t)size;

                    break;
                }
                default:
                    assert(0);
                }
            } else {
                assert(0);
            }

            offset += ovector[1];
        }
    }

    reply_code = REPLY_CODE_REQUESTED_MAIL_ACTION_OKAY_COMPLETED;
    clear_buffer(connection);
    submit_reply(connection, REPLY_MSG_OK, reply_code, REPLY_SINGLE);
do_return:
    return (reply_code);
}

static const char *smtp_do_rcpt(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length)
{
    const char *reply_code;

    const int groups_count = 3;
    const int ovecsize = groups_count * 3;
    int ovector[ovecsize];

    // Parse command arguments.
    int count = pcre_exec(commands[command_id].args_re, commands[command_id].args_extra,
                          arguments, length, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ERROR_IN_ARGUMENTS, reply_code, REPLY_SINGLE);
        goto do_return;
    }
    assert(count > 0);

    const int is_postmaster = (ovector[3] > ovector[2]);
    if (is_postmaster) {
        reply_code = REPLY_CODE_MAILBOX_PERMANENTLY_UNAVAILABLE;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_NO_POSTMASTER_MAILBOX, reply_code, REPLY_SINGLE);
        goto do_return;
    }

    recipient_t *const recipient = malloc(sizeof(*recipient));
    if (recipient == NULL) {
        reply_code = REPLY_CODE_INSUFFICIENT_SYSTEM_STORAGE;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_INSUFFICIENT_STORAGE, reply_code, REPLY_SINGLE);
        goto do_return;
    }

    const size_t args_len = ovector[1];
    if (!is_postmaster) {
        // Check e-mail for correctness.
        const size_t mailbox_offset = ovector[4];
        const int mailbox_len = ovector[5] - mailbox_offset;
        count = pcre_exec(EMAIL_RE, NULL, arguments + mailbox_offset,
                          mailbox_len, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
        if (count < 0) {
            reply_code = REPLY_CODE_MAILBOX_NAME_NOT_ALLOWED;
            clear_buffer(connection);
            submit_reply(connection, REPLY_MSG_MAILBOX_NAME_NOT_ALLOWED, reply_code, REPLY_SINGLE);
            goto do_free_recipient;
        }
        assert(count > 0);

        const int result =
            pcre_copy_substring(arguments + mailbox_offset, ovector, count, 0,
                                recipient->target, mailbox_len + 1);
        assert(result == mailbox_len);
        recipient->at_position = ovector[2];
    } else {
        const int mailbox_len = ovector[3] - ovector[2];
        const int result =
            pcre_copy_substring(arguments, ovector, count, 1,
                                recipient->target, mailbox_len + 1);
        assert(result == mailbox_len);
        recipient->at_position = -1;
    }

    // Parse parameters.
    const char *params = arguments + args_len;
    count = pcre_exec(commands[command_id].params_re, commands[command_id].params_extra,
                      params, length - args_len, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ERROR_IN_PARAMETERS, reply_code, REPLY_SINGLE);
        goto do_free_recipient;
    }
    assert(count > 0);

    log_message(DEBUG, "General test passed (count=%d): '%.*s'", count,
                ovector[1] - ovector[0], params);

    const int has_parameters = (count == 2);
    if (has_parameters) {
        const int params_len = ovector[3] - ovector[2];
        params += ovector[2];

        size_t offset = 0;
        while (offset < params_len) {
            count = pcre_exec(GET_PARAMETER_RE, NULL, params + offset,
                              params_len - offset, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
            assert(count > 0);

            const char *const name = params + offset + ovector[2];
            const size_t name_len = ovector[3] - ovector[2];

            log_message(DEBUG, "Parameter name: %.*s", (int)name_len, name);

            const rcpt_param_id_t rcpt_param_id =
                recognize_rcpt_parameter(name, name_len, NULL, 0);
            log_message(DEBUG, "RCPT param ID: %d", rcpt_param_id);
            if (rcpt_param_id == SMTP_BAD_RCPT_PARAM) {
                reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
                clear_buffer(connection);
                submit_reply(connection, REPLY_MSG_PARAMETER_NOT_IMPLEMENTED, reply_code, REPLY_SINGLE);
                goto do_free_recipient;
            }
#if 0
            const int must_have_value = (rcpt_parameters[rcpt_param_id].value_re != NULL);
            const int has_value = (count == 3);
            if (must_have_value != has_value) {
                reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
                clear_buffer(connection);
                if (must_have_value) {
                    submit_reply(connection, REPLY_MSG_PARAMETER_MUST_HAVE_VALUE, reply_code, REPLY_SINGLE, name_len, name);
                } else {
                    submit_reply(connection, REPLY_MSG_PARAMETER_MUST_NOT_HAVE_VALUE, reply_code, REPLY_SINGLE, name_len, name);
                }
                goto do_free_recipient;
            }
            log_message(DEBUG, "must_have_value == has_value.");

            if (has_value) {
                const char *const value = params + offset + ovector[4];
                const size_t value_len = ovector[5] - ovector[4];
                count = pcre_exec(rcpt_parameters[rcpt_param_id].value_re, NULL,
                                  value, value_len, 0, RE_RCPT_PARAM_EXEC_OPTIONS,
                                  NULL, 0);
                if (count < 0) {
                    reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
                    clear_buffer(connection);
                    submit_reply(connection, REPLY_MSG_PARAMETER_HAS_WRONG_VALUE, reply_code, REPLY_SINGLE, name_len, name);
                    goto do_free_recipient;
                }

                log_message(DEBUG, "Parameter value: %.*s", (int)value_len, value);

                switch (rcpt_param_id) {
//                case SMTP_RCPT_PARAM_SIZE: {
//                    char *end;
//                    const long size = strtol(value, &end, 10);
//                    assert(end == value + value_len);

//                    if (size > LIMIT_MESSAGE_CONTENT - 1) {
//                        reply_code = REPLY_CODE_EXCEEDED_STORAGE_ALLOCATION;
//                        clear_buffer(connection);
//                        submit_reply(connection, REPLY_MSG_TOO_MUCH_DATA, reply_code, REPLY_SINGLE);
//                        goto do_free_recipient;
//                    }

//                    break;
//                }
                default:
                    assert(0);
                }
            } else {
                assert(0);
            }
#endif
            offset += ovector[1];
        }
    }

    STAILQ_INSERT_TAIL(&connection->recipients, recipient, links);

    reply_code = REPLY_CODE_REQUESTED_MAIL_ACTION_OKAY_COMPLETED;
    clear_buffer(connection);
    submit_reply(connection, REPLY_MSG_OK, reply_code, REPLY_SINGLE);
    goto do_return;

do_free_recipient:
    free(recipient);
do_return:
    return (reply_code);
}

static const char *smtp_do_data(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length)
{
    const char *reply_code;

    // Parse command arguments.
    const int groups_count = 2;
    const int ovecsize = groups_count * 3;
    int ovector[ovecsize];
    const int count =
        pcre_exec(commands[command_id].args_re, commands[command_id].args_extra,
                  arguments, length, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ERROR_IN_ARGUMENTS, reply_code, REPLY_SINGLE);
        return (reply_code);
    }
    assert(count > 0);

    const int has_arguments = (count == 2);
    if (has_arguments) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ARGUMENT_NOT_SUPPORTED, reply_code, REPLY_SINGLE, SERVER_DOMAIN);
        return (reply_code);
    }

    if (STAILQ_EMPTY(&connection->recipients)) {
        reply_code = REPLY_CODE_TRANSACTION_FAILED;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_NO_VALID_RECIPIENTS, reply_code, REPLY_SINGLE, SERVER_DOMAIN);
        return (reply_code);
    }

    reply_code = REPLY_CODE_START_MAIL_INPUT;
    clear_buffer(connection);
    submit_reply(connection, REPLY_MSG_START_MAIL_INPUT, reply_code, REPLY_SINGLE, SERVER_DOMAIN);
    return (reply_code);
}

static const char *smtp_do_rset(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length)
{
    const char *reply_code;

    // Parse command arguments.
    const int groups_count = 2;
    const int ovecsize = groups_count * 3;
    int ovector[ovecsize];
    const int count =
        pcre_exec(commands[command_id].args_re, commands[command_id].args_extra,
                  arguments, length, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ERROR_IN_ARGUMENTS, reply_code, REPLY_SINGLE);
        return (reply_code);
    }
    assert(count > 0);

    const int has_arguments = (count == 2);
    if (has_arguments) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ARGUMENT_NOT_SUPPORTED, reply_code, REPLY_SINGLE, SERVER_DOMAIN);
        return (reply_code);
    }

    reply_code = REPLY_CODE_REQUESTED_MAIL_ACTION_OKAY_COMPLETED;
    reset_connection(connection);
    submit_reply(connection, REPLY_MSG_OK, reply_code, REPLY_SINGLE, SERVER_DOMAIN);
    return (reply_code);
}

static const char *smtp_do_vrfy(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length)
{
    const char *reply_code;

    const int groups_count = 2;
    const int ovecsize = groups_count * 3;
    int ovector[ovecsize];

    // Parse command arguments.
    int count = pcre_exec(commands[command_id].args_re, commands[command_id].args_extra,
                          arguments, length, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ERROR_IN_ARGUMENTS, reply_code, REPLY_SINGLE);
        return (reply_code);
    }
    assert(count > 0);

    // Check e-mail for correctness.
    const size_t mailbox_offset = ovector[2];
    const int mailbox_len = ovector[3] - mailbox_offset;
    count = pcre_exec(EMAIL_RE, NULL, arguments + mailbox_offset, mailbox_len,
                      0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_MAILBOX_NAME_NOT_ALLOWED;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_MAILBOX_NAME_NOT_ALLOWED, reply_code, REPLY_SINGLE);
        return (reply_code);
    }
    assert(count > 0);

    reply_code = REPLY_CODE_CANNOT_VRFY_USER_BUT_WILL_ACCEPT_MESSAGE;
    clear_buffer(connection);
    submit_reply(connection, REPLY_MSG_CANNOT_VRFY_USER, reply_code, REPLY_SINGLE);
    return (reply_code);
}

static const char *smtp_do_expn(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length)
{
    const char *reply_code;

    const int groups_count = 2;
    const int ovecsize = groups_count * 3;
    int ovector[ovecsize];

    // Parse command arguments.
    int count = pcre_exec(commands[command_id].args_re, commands[command_id].args_extra,
                          arguments, length, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ERROR_IN_ARGUMENTS, reply_code, REPLY_SINGLE);
        return (reply_code);
    }
    assert(count > 0);

    // @todo: no support of mailing lists yet.

    reply_code = REPLY_CODE_COMMAND_NOT_IMPLEMENTED;
    clear_buffer(connection);
    submit_reply(connection, REPLY_MSG_COMMAND_NOT_IMPLEMENTED, reply_code, REPLY_SINGLE);
    return (reply_code);
}


static const char *smtp_do_help(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length)
{
    // @todo: if sometime I want to provide state-dependent help message, then I
    // will have to do it in autogenerated handlers. Search for more todo's.
    const char *const reply_code = REPLY_CODE_HELP_MESSAGE;
    clear_buffer(connection);
    submit_reply(connection, REPLY_MSG("No way!"), reply_code, REPLY_SINGLE);
    return (reply_code);
}


static const char *smtp_do_quit(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length)
{
    const char *reply_code;

    // Parse command arguments.
    const int groups_count = 2;
    const int ovecsize = groups_count * 3;
    int ovector[ovecsize];
    const int count =
        pcre_exec(commands[command_id].args_re, commands[command_id].args_extra,
                  arguments, length, 0, RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
    if (count < 0) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ERROR_IN_ARGUMENTS, reply_code, REPLY_SINGLE);
        return (reply_code);
    }
    assert(count > 0);

    const int has_arguments = (count == 2);
    if (has_arguments) {
        reply_code = REPLY_CODE_SYNTAX_ERROR_IN_ARGUMENTS_OR_PARAMETERS;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_ARGUMENT_NOT_SUPPORTED, reply_code, REPLY_SINGLE, SERVER_DOMAIN);
        return (reply_code);
    }

    reply_code = REPLY_CODE_SERVICE_CLOSING_TRANSMISSION_CHANNEL;
    reset_connection(connection);
    submit_reply(connection, REPLY_MSG_GOODBYE, reply_code, REPLY_SINGLE);
    return (reply_code);
}

static const char *smtp_handle_command(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length)
{
    const char *reply_code;

    switch (command_id) {
    case SMTP_CMD_HELO:
        reply_code = smtp_do_helo(connection, command_id, arguments, length);
        break;
    case SMTP_CMD_EHLO:
        reply_code = smtp_do_ehlo(connection, command_id, arguments, length);
        break;
    case SMTP_CMD_MAIL:
        reply_code = smtp_do_mail(connection, command_id, arguments, length);
        break;
    case SMTP_CMD_RCPT:
        reply_code = smtp_do_rcpt(connection, command_id, arguments, length);
        break;
    case SMTP_CMD_DATA:
        reply_code = smtp_do_data(connection, command_id, arguments, length);
        break;
    case SMTP_CMD_RSET:
        reply_code = smtp_do_rset(connection, command_id, arguments, length);
        break;
    case SMTP_CMD_VRFY:
        reply_code = smtp_do_vrfy(connection, command_id, arguments, length);
        break;
    case SMTP_CMD_EXPN:
        reply_code = smtp_do_expn(connection, command_id, arguments, length);
        break;
    case SMTP_CMD_HELP:
        reply_code = smtp_do_help(connection, command_id, arguments, length);
        break;
    case SMTP_CMD_NOOP:
        reply_code = REPLY_CODE_REQUESTED_MAIL_ACTION_OKAY_COMPLETED;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_OK, reply_code, REPLY_SINGLE);
        break;
    case SMTP_CMD_QUIT:
        reply_code = smtp_do_quit(connection, command_id, arguments, length);
        break;
    default:
        reply_code = REPLY_CODE_SYNTAX_ERROR_COMMAND_UNRECOGNIZED;
        clear_buffer(connection);
        submit_reply(connection, REPLY_MSG_COMMAND_UNRECOGNIZED, reply_code, REPLY_SINGLE);
    }

    return (reply_code);
}

static te_smtp_state smtp_do_init_openned(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code = REPLY_CODE_SERVICE_READY;
    reset_connection(connection);
    submit_reply(connection, REPLY_MSG_WELCOME, reply_code, REPLY_SINGLE, SERVER_DOMAIN);

    return (next_state);
}

#if 0
static te_smtp_state smtp_do_init_rejected(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code = REPLY_CODE_TRANSACTION_FAILED;
    reset_connection(connection);
    submit_reply(connection, REPLY_MSG_NO_SMTP_SERVICE, reply_code, REPLY_SINGLE);

    return (next_state);
}
#endif

static te_smtp_state smtp_do_negotiation_ehlo(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_negotiation_helo(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_negotiation_rset(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_negotiation_vrfy(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_negotiation_expn(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_negotiation_help(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    // @todo: provide state-dependent help message instead of smtp_handle_command().
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_negotiation_noop(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_negotiation_quit(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_negotiation_unknown(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_wait_transaction_mail(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_wait_transaction_helo(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_wait_transaction_ehlo(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_wait_transaction_rset(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_wait_transaction_vrfy(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_wait_transaction_expn(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_wait_transaction_help(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    // @todo: provide state-dependent help message instead of smtp_handle_command().
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_wait_transaction_noop(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_wait_transaction_quit(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_wait_transaction_unknown(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_recipients_rcpt(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_recipients_data(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_recipients_helo(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_recipients_ehlo(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_recipients_rset(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_recipients_vrfy(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_recipients_expn(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_recipients_help(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    // @todo: provide state-dependent help message instead of smtp_handle_command().
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_recipients_noop(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_recipients_quit(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_recipients_unknown(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}

static te_smtp_state smtp_do_collect_data_completed(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    int ok = 1;
    const time_t timestamp = time(NULL);

    // Construct timestamp line.
    char received[TIMESTAMP_LEN + 1];
    const int received_len =
        make_timestamp(received, sizeof(received), connection->domain,
                       connection->address, SERVER_DOMAIN, timestamp);
    ok = (received_len >= 0);
    if (!ok) {
        log_func(WARNING, "make_timestamp");
        goto do_reply;
    }
    log_message(DEBUG, "%s", received);

    // Construct return-path line.
    char return_path[RETURN_PATH_LEN + 1];
    const int return_path_len =
        make_return_path(return_path, sizeof(return_path), connection->source);
    ok = (return_path_len >= 0);
    if (!ok) {
        log_func(WARNING, "make_return_path");
        goto do_reply;
    }
    log_message(DEBUG, "%s", return_path);

    // Process mail data for each of the target recipients.
    while (ok && !STAILQ_EMPTY(&connection->recipients)) {
        const recipient_t *const first = STAILQ_FIRST(&connection->recipients);
        STAILQ_REMOVE_HEAD(&connection->recipients, links);

        const int local = (strcmp(first->target + first->at_position + 1, SERVER_DOMAIN) == 0);
        log_message(DEBUG, "recipient %s is %s", first->target, (local ? "local" : "remote"));

        int result = 0;
        if (local) {
            result = maildir_save_message(first->target, first->at_position,
                                          connection->domain, timestamp,
                                          return_path, return_path_len,
                                          received, received_len,
                                          connection->buffer, connection->buffer_length);
            if (result < 0) {
                log_func(WARNING, "maildir_save_message");
            }
        } else {
            result = queuedir_save_message(first->target, timestamp,
                                           received, received_len,
                                           connection->buffer, connection->buffer_length);
            if (result < 0) {
                log_func(WARNING, "queuedir_save_message");
            }
        }

        free((void *)first);

        ok = (result == 0);
    }

do_reply:
    if (ok) {
        const char *const reply_code = REPLY_CODE_REQUESTED_MAIL_ACTION_OKAY_COMPLETED;
        reset_connection(connection);
        submit_reply(connection, REPLY_MSG_OK, reply_code, REPLY_SINGLE);
    } else {
        clear_recipients(connection);
        const char *const reply_code = REPLY_CODE_TRANSACTION_FAILED;
        reset_connection(connection);
        submit_reply(connection, REPLY_MSG_TRANSACTION_FAILED, reply_code, REPLY_SINGLE);
    }

    return (next_state);
}

static te_smtp_state smtp_do_collect_data_rejected(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code = REPLY_CODE_EXCEEDED_STORAGE_ALLOCATION;
    reset_connection(connection);
    submit_reply(connection, REPLY_MSG_TOO_MUCH_DATA, reply_code, REPLY_SINGLE);

    return (next_state);
}

#if 0
static te_smtp_state smtp_do_broken_quit(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    const char *const reply_code =
        smtp_handle_command(connection, command_id, arguments, length);

    return (IS_POSITIVE(reply_code) ? next_state : current_state);
}
#endif

static te_smtp_state smtp_do_invalid(
    active_connection_t *const connection,
    const int command_id,
    const char arguments[],
    const int length,
    const te_smtp_state current_state,
    const te_smtp_state next_state,
    const te_smtp_event event)
{
    smtp_invalid_transition(current_state, event);

    const char *const reply_code = REPLY_CODE_BAD_SEQUENCE_OF_COMMANDS;
    clear_buffer(connection);

    switch (current_state) {
    case SMTP_ST_NEGOTIATION:
        submit_reply(connection, REPLY_MSG("Wrong. Waiting for negotiation (next: EHLO/HELO)"), reply_code, REPLY_SINGLE);
        break;
    case SMTP_ST_WAIT_TRANSACTION:
        submit_reply(connection, REPLY_MSG("Wrong. Waiting for transaction start (next: MAIL)"), reply_code, REPLY_SINGLE);
        break;
    case SMTP_ST_COLLECT_RECIPIENTS: {
        const char *const reply_message =
            (STAILQ_EMPTY(&connection->recipients) ?
             REPLY_MSG("Wrong. Collecting recipients (next: RCPT)") :
             REPLY_MSG("Wrong. Collecting recipients (next: RCPT/DATA)"));
        submit_reply(connection, reply_message, reply_code, REPLY_SINGLE);
        break;
    }
    default:
        submit_reply(connection, REPLY_MSG_BAD_SEQUENCE_OF_COMMANDS, reply_code, REPLY_SINGLE);
        break;
    }

    return (current_state);
}

static int smtp_invalid_transition(const te_smtp_state st, const te_smtp_event evt)
{
//    const char *const fmt = zSmtpStrings + SmtpFsmErr_off;
//    fprintf(stderr, fmt, st, SMTP_STATE_NAME(st), evt, SMTP_EVT_NAME(evt));
    return (0);
}

#endif /*FSM_HANDLER_CODE*/


#ifdef FSM_FINISH_STEP

#endif /*FSM_FINISH_STEP*/
