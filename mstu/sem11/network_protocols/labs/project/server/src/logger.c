#include "logapi.h"
#include "utils.h"
#include "common.h"
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/signalfd.h>
#include <sys/epoll.h>
#include <signal.h>


/**
 * Структура данных, необходимых для работы журналирующего процесса.
 */
typedef struct {
    int quit;       /**< Флаг завершения работы. */
    int epoll_fd;   /**< Дескриптор ожидания событий. */
    int signal_fd;  /**< Дескриптор очереди сигналов. */
    int print;      /**< Флаг вывода в консоль диагностических сообщений. */
} logger_data_t;


/**
 * Обрабатывает накопленные сообщения.
 */
static int process_pending_messages(logger_data_t *const logger)
{
    char message[MAX_MESSAGE_LEN];

    int state = 0;
    while (state == 0) {
        const int length = read(STDIN_FILENO, message, MAX_MESSAGE_LEN);
        if (length < 0) {
            switch (errno) {
            case EAGAIN:
#if (EWOULDBLOCK != EAGAIN)
            case EWOULDBLOCK:
#endif
                state = 1;
                break;
            default:
                log_func(CRITICAL, "read");
                state = -1;
            }
        } else {
            write(STDERR_FILENO, message, length);
            if (logger->print) {
                write(STDOUT_FILENO, message, length);
            }
        }
    }

    return (state >= 0 ? 0 : -1);
}

/**
 * Обрабатывает пришедшие сигналы. Возвращает 0 в случае успешного завершения и
 * -1 --- в случае ошибки.
 */
static int process_signals(logger_data_t *const logger)
{
    struct signalfd_siginfo siginfo;

    int state = 0;
    while (state == 0) {
        const int count = read(logger->signal_fd, &siginfo, sizeof(siginfo));
        if (count == sizeof(siginfo)) {
            if (logger->quit) {
                log_message(WARNING, "<logger> Unhandled signal %s",
                            strsignal(siginfo.ssi_signo));
            }

            switch (siginfo.ssi_signo) {
            case SIGINT:
            case SIGTERM:
            case SIGQUIT:
                logger->quit = 1;
                break;
            case SIGPIPE:
                // This 'case' is needed in order to see write's return value.
                break;
            default:
                log_message(WARNING, "<logger> Unexpected signal %s",
                            strsignal(siginfo.ssi_signo));
                assert(0);
            }
        } else if (count >= 0) {
            log_func_custom(WARNING, "read (signalfd_siginfo)", "Incomplete read.");
        } else {
            switch (errno) {
            case EAGAIN:
#if (EWOULDBLOCK != EAGAIN)
            case EWOULDBLOCK:
#endif
                state = 1;
                break;
            default:
                log_func(CRITICAL, "read (signalfd_siginfo)");
                state = -1;
            }
        }
    }

    return (state >= 0 ? 0 : -1);
}

#define LOGGER_EPOLL_EVENTS_COUNT 1

/**
 * Основной цикл журналирующего процесса.
 */
static int logger_loop(logger_data_t *const logger)
{
    process_pending_messages(logger);

    struct epoll_event events[LOGGER_EPOLL_EVENTS_COUNT];

    int loop_result = 0;
    while (!logger->quit && loop_result == 0) {
        const int fds_count = epoll_wait(logger->epoll_fd, events, LOGGER_EPOLL_EVENTS_COUNT, -1);
        if (fds_count < 0) {
            switch (errno) {
            case EINTR:
                break;
            default:
                log_func(WARNING, "epoll_wait");
            }
            continue;
        }

        for (int i = 0; !logger->quit && loop_result == 0 && i < fds_count; ++i) {
            if ((events[i].events & EPOLLIN) != 0x0) {
                if (events[i].data.fd == logger->signal_fd) {
                    loop_result = process_signals(logger);
                } else if (events[i].data.fd == STDIN_FILENO) {
                    loop_result = process_pending_messages(logger);
                } else {
                    log_message(WARNING, "<logger> Unexpected epoll event from FD = %d.", events[i].data.fd);
                    assert(0);
                }
                loop_result = (loop_result == 0 && (events[i].events & (EPOLLHUP | EPOLLERR)) != 0x0);
            }
        }
    }

    return (loop_result);
}

int logger_entry(const int print)
{
    int initialized = 0;
    logger_data_t logger = {
        .quit = 0,
        .epoll_fd = -1,
        .signal_fd = -1,
        .print = print
    };

    logger.epoll_fd = epoll_create1(0x0);
    if (logger.epoll_fd < 0) {
        log_func(CRITICAL, "epoll_create1");
        goto do_return;
    }

    logger.signal_fd = listen_for_signals(logger.epoll_fd);
    if (logger.signal_fd < 0) {
        goto do_close_epoll_fd;
    }

    int result = set_flags(STDIN_FILENO, O_NONBLOCK);
    if (result < 0) {
        log_func(CRITICAL, "set_flags (STDIN_FILENO)");
        goto do_close_signal_fd;
    }

    struct epoll_event event = {
        .events = EPOLLIN | EPOLLET,
        .data.fd = STDIN_FILENO
    };
    result = epoll_ctl(logger.epoll_fd, EPOLL_CTL_ADD, STDIN_FILENO, &event);
    if (result != 0) {
        log_func(CRITICAL, "epoll_ctl (STDIN_FILENO)");
        goto do_close_signal_fd;
    }

    initialized = 1;
    const int loop_result = logger_loop(&logger);

do_close_signal_fd:
    result = close(logger.signal_fd);
    if (result != 0) {
        log_func(WARNING, "close (signal_fd)");
    }
do_close_epoll_fd:
    result = close(logger.epoll_fd);
    if (result != 0) {
        log_func(WARNING, "close (epoll_fd)");
    }
do_return:
    log_message(INFO, "Logger has terminated.");
    return (initialized && loop_result == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
