#include "queuedir.h"
#include "smtp_limits.h"
#include "utils.h"
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>


#define MESSAGE_NAME_FORMAT     "%ld.%d.%s"

#define QUEUEDIR_TMP     "tmp"
#define QUEUEDIR_NEW     "new"
#define QUEUEDIR_SUBDIR_LEN  3


static pid_t MY_PID = -1;
static size_t QUEUEDIR_PATH_LEN = 0;
static char QUEUEDIR_PATH[PATH_MAX];


int queuedir_set_path(const char path[])
{
    int result = 0;

    // Get canonical absolute path to queuedir.
    const char *const abs_path = realpath(path, QUEUEDIR_PATH);
    if (abs_path == NULL) {
        result = -1;
        goto do_return;
    }

    QUEUEDIR_PATH_LEN = strlen(QUEUEDIR_PATH);
    assert(QUEUEDIR_PATH_LEN < PATH_MAX);

    // Check if it can be accessed.
    result = access(QUEUEDIR_PATH, R_OK | W_OK | X_OK);
    if (result != 0) {
        goto do_return;
    }

    // Get pid of self (this is to reduce the number of system calls).
    MY_PID = getpid();

do_return:
    return (result);
}

int queuedir_save_message(const char email[], const time_t timestamp,
                          const char received[], const size_t received_len,
                          const char message[], const size_t message_len)
{
    // Go to queuedir.
    int result = chdir(QUEUEDIR_PATH);
    if (result != 0) {
        goto do_return;
    }

    // Generate 'tmp' file name.
    char tmp_name[NAME_MAX + 1];
    const int name_len = snprintf(tmp_name, NAME_MAX + 1,
                                  QUEUEDIR_TMP "/" MESSAGE_NAME_FORMAT,
                                  (long)timestamp, MY_PID, email);
    if (name_len > NAME_MAX) {
        result = -1;
        goto do_return;
    }

    // Create 'tmp' file. Must not exist up to now.
    const int mode = (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
    const int fd = open(tmp_name, O_CREAT | O_EXCL | O_WRONLY, mode);
    if (fd < 0) {
        result = -1;
        goto do_return;
    }

    // Write "Received" time stamp.
    result = write_buffer(fd, received, received_len);
    if (result < 0) {
        goto do_unlink_tmp_name;
    }

    // Write message.
    result = write_buffer(fd, message, message_len);
    if (result < 0) {
        goto do_unlink_tmp_name;
    }

    // Create 'new' file name basing on 'tmp'.
    char new_name[NAME_MAX + 1] = QUEUEDIR_NEW "/";
    strcpy(new_name + QUEUEDIR_SUBDIR_LEN + 1, tmp_name + QUEUEDIR_SUBDIR_LEN + 1);

    // Link 'new' name to the same file as 'tmp' name is linked to.
    result = link(tmp_name, new_name);
    if (result != 0) {
        goto do_close_fd;
    }
do_unlink_tmp_name:
    result = unlink(tmp_name);
do_close_fd:
    result = close(fd);
do_return:
    return (result);
}
