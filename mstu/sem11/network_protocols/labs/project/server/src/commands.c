#include "commands_re.h"
#include "commands.h"
#include <stdio.h>
#include <assert.h>


#define RE_COMPILE_OPTIONS  (0x0)
#define RE_STUDY_OPTIONS    (0x0)

/*!
 *  Инициализатор структуры данных команды SMTP-протокола в словаре
 *  поддерживаемых команд.
 */
#define INIT_CMD(name)                              \
    [SMTP_CMD_##name] = {                           \
        .cmd_pattern = SMTP_CMD_##name##_STR,       \
        .args_pattern = SMTP_ARGS_##name##_STR,     \
        .params_pattern = SMTP_PARAMS_##name##_STR, \
        .event = SMTP_EV_##name,                    \
    }

command_t commands[SMTP_CMD_COUNT] = {
    INIT_CMD(HELO), INIT_CMD(EHLO), INIT_CMD(MAIL), INIT_CMD(RCPT),
    INIT_CMD(DATA), INIT_CMD(RSET), INIT_CMD(VRFY), INIT_CMD(EXPN),
    INIT_CMD(HELP), INIT_CMD(NOOP), INIT_CMD(QUIT)
};

pcre *GET_PARAMETER_RE;
pcre *EMAIL_RE;

void __attribute__((constructor)) init_commands_re()
{
    printf("Compiling regular expressions of commands... ");

    int no_errors = 1;
    const char *error;
    int error_offset;
    int i;
    for (i = 0; no_errors && i < SMTP_CMD_COUNT; ++i) {
        commands[i].cmd_re = pcre_compile(commands[i].cmd_pattern, RE_COMPILE_OPTIONS, &error, &error_offset, NULL);
        no_errors = (commands[i].cmd_re != NULL);
        if (!no_errors) {
            fprintf(stderr, "CMD %d: %s \nCompile error at %d: %s.\n", i, commands[i].cmd_pattern, error_offset, error);
        }

        if (no_errors) {
            commands[i].args_re = pcre_compile(commands[i].args_pattern, RE_COMPILE_OPTIONS, &error, &error_offset, NULL);
            no_errors = (commands[i].args_re != NULL);
            if (!no_errors) {
                fprintf(stderr, "ARGS %d: %s \nCompile error at %d: %s.\n", i, commands[i].args_pattern, error_offset, error);
            }
        }

        if (no_errors) {
            commands[i].args_extra = pcre_study(commands[i].args_re, RE_STUDY_OPTIONS, &error);
            no_errors = (error == NULL);
            if (!no_errors) {
                fprintf(stderr, "ARGS %d: %s \nStudying error: %s.\n", i, commands[i].args_pattern, error);
            }
        }

        if (no_errors && commands[i].params_pattern != NULL) {
            commands[i].params_re = pcre_compile(commands[i].params_pattern, RE_COMPILE_OPTIONS, &error, &error_offset, NULL);
            no_errors = (commands[i].params_re != NULL);
            if (!no_errors) {
                fprintf(stderr, "PARAMS %d: %s \nCompile error at %d: %s.\n", i, commands[i].params_pattern, error_offset, error);
            }
        }

        if (no_errors && commands[i].params_pattern != NULL) {
            commands[i].params_extra = pcre_study(commands[i].params_re, RE_STUDY_OPTIONS, &error);
            no_errors = (error == NULL);
            if (!no_errors) {
                fprintf(stderr, "PARAMS %d: %s \nStudying error: %s.\n", i, commands[i].params_pattern, error);
            }
        }
    }

    if (no_errors) {
        GET_PARAMETER_RE = pcre_compile(GET_PARAMETER, RE_COMPILE_OPTIONS, &error, &error_offset, NULL);
        no_errors = (GET_PARAMETER_RE != NULL);
        if (!no_errors) {
            fprintf(stderr, "GET_PARAMETER: %s \nCompile error at %d: %s.\n", GET_PARAMETER, error_offset, error);
        }
    }

    if (no_errors) {
        EMAIL_RE = pcre_compile(EMAIL, RE_COMPILE_OPTIONS, &error, &error_offset, NULL);
        no_errors = (EMAIL_RE != NULL);
        if (!no_errors) {
            fprintf(stderr, "EMAIL: %s \nCompile error at %d: %s.\n", EMAIL, error_offset, error);
        }
    }

    printf("%s\n", (no_errors ? "Done" : "Failed"));
    if (!no_errors) exit(EXIT_FAILURE);
}

void __attribute__((destructor)) free_commands_re()
{
    int i;
    for (i = 0; i < SMTP_CMD_COUNT; ++i) {
        pcre_free(commands[i].cmd_re);
        pcre_free(commands[i].args_re);
        pcre_free_study(commands[i].args_extra);
        pcre_free(commands[i].params_re);
        pcre_free_study(commands[i].params_extra);
    }

    pcre_free(EMAIL_RE);
    pcre_free(GET_PARAMETER_RE);
}

command_id_t recognize_command(const char command[], const int length,
                               int ovector[], const int ovecsize)
{
    int found = 0;
    int i = 0;
    while (!found && i < SMTP_CMD_COUNT) {
        const int result =
            pcre_exec(commands[i].cmd_re, NULL, command, length, 0,
                      RE_CMD_EXEC_OPTIONS, ovector, ovecsize);
        found = (result >= 0);
        if (!found) ++i;
    }

    return (found ? i : SMTP_BAD_CMD);
}
