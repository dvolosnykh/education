#include "commands.h"
#include "parameters.h"
#include "smtp_utils.h"
#include "smtp_limits.h"
#include <netdb.h>
#include <CUnit/Basic.h>
#include <string.h>

#ifdef VERBOSE_TESTS
#include <stdio.h>
#endif

/*
 * For each of the commands there are 3 correct spellings in various casings
 * and 1 wrong.
 */
#define CORRECT_COUNT   3


void test_recognize_command(void)
{
    const char *const cmd_str[SMTP_CMD_COUNT][CORRECT_COUNT + 1] = {
        {"helo", "HELO", "hElO", "helLo"},
        {"ehlo", "EHLO", "EhlO", "ehh"},
        {"mail", "MAIL", "MAil", "ismail"},
        {"rcpt", "RCPT", "rCPT", "recipient"},
        {"data", "DATA", "DATa", "DaTe"},
        {"rset", "RSET", "RsET", "ReST"},
        {"vrfy", "VRFY", "vrfY", "fyi"},
        {"expn", "EXPN", "expN", "Exp"},
        {"help", "HELP", "hElp", "HEELP!"},
        {"noop", "NOOP", "nOOp", "snOOp"},
        {"quit", "QUIT", "QuiT", "QuiD"}
    };

    for (command_id_t cmd_id = 0; cmd_id < SMTP_CMD_COUNT; ++cmd_id) {
        size_t i;
        for (i = 0; i < CORRECT_COUNT; ++i) {
#ifdef VERBOSE_TESTS
            printf("%s\n", cmd_str[cmd_id][i]);
#endif
            const command_id_t recognized_id =
                recognize_command(cmd_str[cmd_id][i], strlen(cmd_str[cmd_id][i]),
                                  NULL, 0);
            CU_ASSERT_EQUAL(recognized_id, cmd_id);
        }
#ifdef VERBOSE_TESTS
        printf("%s\n", cmd_str[cmd_id][i]);
#endif
        const command_id_t recognized_id =
            recognize_command(cmd_str[cmd_id][i], strlen(cmd_str[cmd_id][i]),
                              NULL, 0);
        CU_ASSERT_NOT_EQUAL(recognized_id, cmd_id);
    }
}

void test_recognize_mail_parameter(void)
{
    const char *const param_str[SMTP_MAIL_PARM_COUNT][CORRECT_COUNT + 1] = {
        {"size", "SIZE", "SiZe", "Seeze"}
    };

    for (mail_param_id_t param_id = 0; param_id < SMTP_MAIL_PARM_COUNT; ++param_id) {
        size_t i;
        for (i = 0; i < CORRECT_COUNT; ++i) {
#ifdef VERBOSE_TESTS
            printf("%s\n", param_str[param_id][i]);
#endif
            const mail_param_id_t recognized_id =
                recognize_mail_parameter(param_str[param_id][i],
                                         strlen(param_str[param_id][i]), NULL, 0);
            CU_ASSERT_EQUAL(recognized_id, param_id);
        }
#ifdef VERBOSE_TESTS
        printf("%s\n", param_str[param_id][i]);
#endif
        const mail_param_id_t recognized_id =
            recognize_mail_parameter(param_str[param_id][i],
                                     strlen(param_str[param_id][i]), NULL, 0);
        CU_ASSERT_NOT_EQUAL(recognized_id, param_id);
    }
}

#if 0
void test_recognize_rcpt_parameter(void)
{
    // @todo: repeat stuff from test_recognize_mail_parameter().
}
#endif

void test_make_timestamp(void)
{
    char received[TIMESTAMP_LEN + 1];
    const int length = make_timestamp(received, sizeof(received), "remote.host",
                                      "1.2.3.4", "this.domain", 1326463931);
    CU_TEST(length >= 0);

    const char expected[] = "Received: from remote.host (1.2.3.4) by this.domain with SMTP; 13 Jan 2012 18:12:11 +0400\r\n";
    CU_ASSERT_STRING_EQUAL(received, expected);
}

void test_make_return_path(void)
{
    char return_path[RETURN_PATH_LEN + 1];
    const int length = make_return_path(return_path, sizeof(return_path),
                                        "some.one@some.where");
    CU_TEST(length >= 0);

    const char expected[] = "Return-Path: <some.one@some.where>\r\n";
    CU_ASSERT_STRING_EQUAL(return_path, expected);
}

int main(void)
{
    if (CU_initialize_registry() != CUE_SUCCESS) {
        goto do_return;
    }

    CU_pSuite regexp_suite = CU_add_suite("RegExp suite", NULL, NULL);
    if (regexp_suite == NULL) {
        goto do_cleanup;
    }

    if (CU_add_test(regexp_suite, "test of recognize_command()", test_recognize_command) == NULL) {
        goto do_cleanup;
    }

    if (CU_add_test(regexp_suite, "test of recognize_mail_parameter()", test_recognize_mail_parameter) == NULL) {
        goto do_cleanup;
    }

#if 0
    if (CU_add_test(suite, "test of recognize_rcpt_parameter()", test_recognize_rcpt_parameter) == NULL) {
        goto do_cleanup;
    }
#endif

    CU_pSuite smtp_utils_suite = CU_add_suite("SMTP utils suite", NULL, NULL);
    if (smtp_utils_suite == NULL) {
        goto do_cleanup;
    }

    if (CU_add_test(smtp_utils_suite, "test of make_timestamp()", test_make_timestamp) == NULL) {
        goto do_cleanup;
    }

    if (CU_add_test(smtp_utils_suite, "test of make_return_path()", test_make_return_path) == NULL) {
        goto do_cleanup;
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
do_cleanup:
    CU_cleanup_registry();
do_return:
    return CU_get_error();
}
