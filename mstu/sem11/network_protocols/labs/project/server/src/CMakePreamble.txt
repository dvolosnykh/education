if(CMAKE_COMPILER_IS_GNUCC)
    add_definitions(-std=gnu99 -D_GNU_SOURCE -Wall -pedantic)
    if(NOT ${CMAKE_BUILD_TYPE} STREQUAL Debug)
        add_definitions(-Werror)
    endif()
else()
    message(FATAL_ERROR "Unsupported compiler.")
endif()

include_directories(BEFORE
    "${src_SOURCE_DIR}"
    "${src_BINARY_DIR}"
    "${PCRE_BINARY_DIR}"
    "${lib_BINARY_DIR}/${CUNIT_DIR}/include"
)
include_directories(BEFORE SYSTEM /usr/include)

link_directories(
    "${lib_BINARY_DIR}/${CUNIT_DIR}/lib"
)
