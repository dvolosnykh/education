#ifndef QUEUEDIR_H
#define QUEUEDIR_H

/*! \file queuedir.h
 *  \brief В этом файле содержится набор функций, определяющих интерфейс к
 *  каталогу очереди нелокальных сообщений.
 */


#include <stdlib.h>


/*!
 *  Задаёт корневой каталог очереди нелокальных сообщений.
 *  \param[in] path Путь к каталогу.
 *  \returns 0 в случае успеха. В противном случае, возвращает отрицательное
 *      значение.
 */
int queuedir_set_path(const char path[]);

/*!
 *  Сохраняет сообщение \a message от хоста \a host в почтовый ящик \a mailbox.
 *  В начало сообщения, - согласно
 *  <a href="http://www.faqs.org/rfcs/rfc2821.html">RFC 2821</a> (см.
 *  раздел 4.4 Trace Information), - добавлется строка временной метки
 *  \a received соответствующая времени \a timestamp.
 *  \param[in] email Адрес электронной почты нелокального адресата.
 *  \param[in] timestamp Временная метка момента передачи сообщения. Измеряется
 *      в секундах от начала отсчёта времени Unix, т.н. \em Epoch
 *      (1970-01-01T00:00:00Z ISO 8601).
 *  \param[in] received Строка временной метки, добавляемая SMTP-сервером в
 *      начало сообщения при его обработке.
 *  \param[in] received_len Длина строки временной метки.
 *  \param[in] message Текст сохраняемого сообщения.
 *  \param[in] message_len Длина сохраняемого сообщения.
 *  \returns 0 в случае успеха. В противном случае, возвращает отрицательное
 *      значение.
 */
int queuedir_save_message(const char email[], time_t timestamp,
                          const char received[], size_t received_len,
                          const char message[], size_t message_len);

#endif
