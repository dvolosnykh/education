#include "manager.h"
#include "common.h"
#include "settings.h"
#include "serveropts.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>


#define ADDRESS_PORT_SEP    ':'


/*!
 *  Осуществляет разбор параметров и аргументов командной строки.
 */
void parse_options(options_t *const options, int argc, char *argv[])
{
    const int options_count = optionProcess(&serverOptions, argc, argv);
    options->workers_count = OPT_VALUE_WORKERS_NUM;
    options->domain = OPT_ARG(DOMAIN);
    options->user = OPT_ARG(USER);
    options->group = OPT_ARG(GROUP);
    options->mail_dir = OPT_ARG(MAIL_DIR);
    options->queue_dir = OPT_ARG(QUEUE_DIR);
    options->log_file = OPT_ARG(LOG_FILE);
    options->log_level = OPT_VALUE_VERBOSITY;
    options->print = OPT_VALUE_PRINT;
    argc -= options_count;
    argv += options_count;

    if (argc > 0) {
        char *sep_pos = strchr(*argv, ADDRESS_PORT_SEP);
        if (sep_pos != NULL) *sep_pos = '\0';
        options->host = (sep_pos > *argv ? *argv : DEFAULT_ADDRESS);
        options->port = (sep_pos != NULL && *++sep_pos != '\0' ? sep_pos : DEFAULT_PORT);
        --argc;
        ++argv;
    } else {
        options->host = DEFAULT_ADDRESS;
        options->port = DEFAULT_PORT;
    }
}

/*!
 *  Фиктивный обработчик сигналов, необходимый для отмены действия по умолчанию.
 */
static void dummy_signal_handler(const int signo)
{
}

/*!
 *  Выполняет различные подготовительные действия в самом начале запуска
 *  сервера, необходимые для всех типов процессов службы.
 */
static void preamble()
{
    sigset_t mask;
    make_sigset(&mask, SIGINT, SIGTERM, SIGQUIT, SIGCHLD, SIGPIPE, SIGUSR1, -1);
    sigprocmask(SIG_BLOCK, &mask, NULL);

    struct sigaction action;
    action.sa_handler = &dummy_signal_handler;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0x0;
    sigaction(SIGALRM, &action, NULL);
}

/*!
 *  Главная точка входа сетевой службы SMTP-сервер.
 */
int main(int argc, char *argv[])
{
    options_t options;
    parse_options(&options, argc, argv);
    preamble();
    return manager_entry(&options);
}
