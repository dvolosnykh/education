#!/bin/bash

if [[ $# -lt 2 ]]; then
        echo "usage: $0 working_dir tests_dir [valgrind_cmd]"
        exit 1
fi

declare -r BIN_DIR="$1" TESTS_DIR="$2" VALGRIND_CMD="${3:-}"

declare -r TOOLS_DIR="../tools"

cd "$BIN_DIR" && \
"$TOOLS_DIR/create_db.sh" && \
${VALGRIND_CMD} ./src/server --domain=iu7.bmstu.ru :2525 &
sleep 5

cd "$BIN_DIR" && \
runtest --srcdir="$TESTS_DIR"

pkill -u $(whoami) -P $!
