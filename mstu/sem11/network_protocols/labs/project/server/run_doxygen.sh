#!/bin/bash

if [[ $# -lt 3 ]]; then
        echo "usage: $0 doxyfile_path output_dir main_tex"
        exit 1
fi

declare -r DOXYFILE_PATH="$1" OUTPUT_DIR="$2" DOC_BASE=$3

declare -r PDFLATEX="pdflatex -interaction=nonstopmode"
#declare -r RE_OUTPUT_DIR=$(sed 's/\//\\\//g' <<<"$OUTPUT_DIR" | sed 's/ /\\ /g')

#doxygen -w latex doxy_header.tex doxy_footer.tex doxygen.sty "$DOXYFILE_PATH" && \
doxygen "$DOXYFILE_PATH" && \
#egrep '^\\(chapter|input){' "$OUTPUT_DIR"/refman.tex | sed "s/^\\\\input{/\\\\input{$RE_OUTPUT_DIR\\//g" >doxy_body.tex && \
cd "$OUTPUT_DIR" && \
$PDFLATEX $DOC_BASE.tex
makeindex $DOC_BASE.idx
$PDFLATEX $DOC_BASE.tex
$PDFLATEX $DOC_BASE.tex
exit 0

#rm -f doxygen.tex && \
#for doc_file in $(ls -1 "$OUTPUT_DIR"/*.tex | grep -v refman.tex); do
#	echo "\\input{$doc_file}"
#done > doxygen.tex
