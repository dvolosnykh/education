#!/bin/bash

if [[ $# -lt 2 ]]; then
        echo "usage: $0 maildir mailbox"
        exit 1
fi

declare -r maildir=$1 mailbox=$2

if ! rm -rf $maildir/$mailbox; then
	echo "Could not destroy mailbox directory."
	exit 1
fi
