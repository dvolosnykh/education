#!/bin/bash

declare -r MAIL_DIR="${1:-maildir}" QUEUE_DIR="${2:-queuedir}"
declare -ra MAILBOX_LIST=(some.one some.one.else) 

rm -rf "$MAIL_DIR" "$QUEUE_DIR"

echo "Creating MailDir"
for mailbox in ${MAILBOX_LIST[@]}; do
	echo "Creating mailbox $mailbox"
	mkdir -p "$MAIL_DIR/$mailbox/tmp" && \
	mkdir -p "$MAIL_DIR/$mailbox/new" && \
	mkdir -p "$MAIL_DIR/$mailbox/cur"
done

echo "Creating remote messages queue"
mkdir -p "$QUEUE_DIR/tmp" && \
mkdir -p "$QUEUE_DIR/new"
