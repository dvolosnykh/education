#!/bin/bash

if [[ $# -lt 2 ]]; then
        echo "usage: $0 maildir mailbox"
        exit 1
fi

declare -r maildir=$1 mailbox=$2

if ! mkdir -p $maildir/$mailbox; then
	echo "Could not create mailbox directory."
	exit 1
fi

cd $maildir/$mailbox && \
	mkdir tmp && mkdir new && mkdir cur
ok=$?

if [[ $ok -ne 0 ]]; then
	echo "Could not create maildir subdirectories."
	exit 1
fi
