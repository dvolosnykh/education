#/bin/bash
declare -r port=${1:-2525} times=${2:-10}

for i in $(seq $times); do
	netcat localhost $port <<<"hello $i" &
done
