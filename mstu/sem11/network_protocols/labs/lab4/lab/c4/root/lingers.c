//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <libgen.h>
#include <arpa/inet.h> 
#include <netdb.h> 
#include <unistd.h>

int usage(char *name)
{
    fprintf(stderr, "Usage: %s <port> [y|n]\n", basename(name));
    return 1;
}

int main(int ac, char *av[]) 
{
    int reuseaddr = 1;
    int port, readup;

    if (ac != 3) exit(usage(av[0]));
    port = atoi(av[1]);
    readup = av[2][0] == 'y';

    struct sockaddr_in sa;
    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = INADDR_ANY;

    int s = socket(PF_INET, SOCK_STREAM, 0);
    if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(int)) < 0)
        perror("reuseaddr");
    if (bind(s, &sa, sizeof(sa)) < 0)
        perror("bind");
    if (listen(s, 1))
        perror("listen");

    char buf[10];
    int fd = accept(s, NULL, NULL);

    sleep(2);
    if (readup)
        recv(fd, buf, sizeof(buf), 0);
    int bc = send(fd, buf, sizeof(buf), 0);
    printf("Sent: %d bytes\n", bc);
    close(fd);

    return 0;
}
