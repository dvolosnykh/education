//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <libgen.h>
#include <arpa/inet.h> 
#include <netdb.h> 
#include <unistd.h>

int usage(char *name)
{
	fprintf(stderr, "Usage: %s <remote_ip> <remote_port>\n", basename(name));
	return 1;
}

int main(int ac, char *av[]) 
{
	char *ip;
	int port;
	if (ac != 3) exit(usage(av[0]));

	ip   = av[1];
	port = atoi(av[2]);

	char buf[10];

	struct sockaddr_in sa;
	memset(&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	inet_aton(ip, &sa.sin_addr.s_addr);

	int s = socket(PF_INET, SOCK_STREAM, 0);
	if (connect(s, &sa, sizeof(sa)) < 0)
		perror("connect");

	send(s, buf, sizeof(buf), 0);
	sleep(4);

	int bc = recv(s, buf, sizeof(buf), 0);
	if (bc < sizeof(buf)) perror("recv");
	printf("Read: %d bytes\n", bc);

	return 0;
}
