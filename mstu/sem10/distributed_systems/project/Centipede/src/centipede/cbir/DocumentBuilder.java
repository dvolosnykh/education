package centipede.cbir;


import centipede.cbir.DocumentUtils;
import centipede.cbir.features.Features;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;


public class DocumentBuilder
{
	public static final String FIELD_NAME_IDENTIFIER = "identifier";
	public static final String FIELD_NAME_INTENSITY_HISTOGRAM_MOMENTS = "IntensityHistogramMoments";
	public static final String FIELD_NAME_HUE_LAYOUT = "HueLayout";
	
	
	public Document createDocument( final InputStream input, final String id ) throws IOException
	{
		return createDocument( ImageIO.read( input ), id );
	}
	
	public Document createDocument( final BufferedImage image, final String id )
	{
		final Document document = new Document();
		if ( id != null )
			document.add( new Field( FIELD_NAME_IDENTIFIER, id, Field.Store.YES, Field.Index.NOT_ANALYZED ) );
		
		final Features features = ImageUtils.extractFeatures( image );
		
		byte[] bytes = DocumentUtils.doubleArrayToByteArray( features.IntensityHistogramMoments.Value );
		document.add( new Field( FIELD_NAME_INTENSITY_HISTOGRAM_MOMENTS, bytes, Field.Store.YES ) );
		
		bytes = DocumentUtils.doubleArrayToByteArray( features.HueLayout.Value );
		document.add( new Field( FIELD_NAME_HUE_LAYOUT, bytes, Field.Store.YES ) );
		
		return ( document );
	}
}