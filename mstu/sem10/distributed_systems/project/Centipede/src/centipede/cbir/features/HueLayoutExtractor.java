package centipede.cbir.features;


import java.awt.image.BufferedImage;

import centipede.cbir.ImageUtils;


public final class HueLayoutExtractor implements FeatureExtractor< double[] >
{
	private static final int DEFAULT_TILES_X_NUM = 8;
	private static final int DEFAULT_TILES_Y_NUM = 8;

	private final int _tilesXNum;
	private final int _tilesYNum;

	public HueLayoutExtractor()
	{
		this( DEFAULT_TILES_X_NUM, DEFAULT_TILES_Y_NUM );
	}

	public HueLayoutExtractor( final int tilesXNum, final int tilesYNum )
	{
		_tilesXNum = tilesXNum;
		_tilesYNum = tilesYNum;
	}

	@Override
	public HueLayout extract( final BufferedImage image )
	{
		final float tileWidth = image.getWidth() / ( float )_tilesXNum;
		final float tileHeight = image.getHeight() / ( float )_tilesYNum;

		final double[] averages = new double[ _tilesXNum * _tilesYNum ];

		float minX = 0.0f;
		int x = 0;
		for ( int i = 0; i < _tilesXNum; ++i )
		{
			final float maxX = minX + tileWidth;
			final int w = Math.round( maxX ) - x;

			float minY = 0.0f;
			int y = 0;
			for ( int j = 0; j < _tilesYNum; ++j )
			{
				final float maxY = minY + tileHeight;
				final int h = Math.round( maxY ) - y;
				
				final int[] pixels = image.getRaster().getPixels( x, y, w, h, ( int[] )null );
				averages[ j * _tilesXNum + i ] = ImageUtils.getRoundedAverage( pixels, Byte.MAX_VALUE - Byte.MIN_VALUE + 1 );

				minY = maxY;
				y += h;
			}

			minX = maxX;
			x += w;
		}

		return new HueLayout( averages );
	}
}