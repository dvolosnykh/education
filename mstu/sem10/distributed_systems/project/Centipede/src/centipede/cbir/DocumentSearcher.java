package centipede.cbir;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.TreeSet;

import javax.imageio.ImageIO;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;

import centipede.cbir.features.Features;


public class DocumentSearcher
{
	private static int DEFAULT_MAX_HITS_COUNT = 10;
	
	private int _maxHitsCount;
	private final IndexReader _reader;
	
	
	public DocumentSearcher( final IndexReader reader )
	{
		this( DEFAULT_MAX_HITS_COUNT, reader );
	}
	
	/**
	 * @param maxHitsCount	Maximum number of hits to return as a search result. Should be positive. 
	 */ 
	public DocumentSearcher( final int maxHitsCount, final IndexReader reader )
	{
		_maxHitsCount = maxHitsCount;
		_reader = reader;
	}
	
	public SearchResult[] search( final Document document ) throws CorruptIndexException, IOException
	{
		final Features features = DocumentUtils.getFeatures( document );
		
		return orderBySimilarity( features );
	}
	
	public SearchResult[] search( final File imageFile ) throws CorruptIndexException, IOException
	{
		SearchResult[] results;
		try
		{
			results = search( ImageIO.read( imageFile ) );
		}
		catch ( Exception e )
		{
			e.printStackTrace();
			results = new SearchResult[ 0 ];
		}
		
		return ( results );
	}
	
	public SearchResult[] search( final BufferedImage image ) throws CorruptIndexException, IOException
	{
		final Features features = ImageUtils.extractFeatures( image );
		
		return orderBySimilarity( features );
	}
	
	private SearchResult[] orderBySimilarity( final Features originalFeatures ) throws CorruptIndexException, IOException
	{
		double maxDistance = 0.0;
		final TreeSet< SearchResult > hits = new TreeSet< SearchResult >();
		
		for ( int documentIndex = 0; documentIndex < _reader.numDocs(); ++documentIndex )
		{
			if ( !_reader.hasDeletions() || !_reader.isDeleted( documentIndex ) )
			{
				final Document document = _reader.document( documentIndex );
				
				final Features features = DocumentUtils.getFeatures( document );
				final double distance = originalFeatures.getDistance( features );
				
				maxDistance = Math.max( distance, maxDistance );
				
				final SearchResult hit = new SearchResult( document, distance );
				if ( hits.size() < _maxHitsCount )
				{
					hits.add( hit );
				}
				else if ( distance < hits.last().getDistance() )
				{
					hits.remove( hits.last() );
					hits.add( hit );
				}
			}
		}
		
		final SearchResult[] results = hits.toArray( new SearchResult[ 0 ] );
		for ( final SearchResult result : results )
			result.setDistance( 1.0 - result.getDistance() / maxDistance );
		
		return ( results );
	}
}