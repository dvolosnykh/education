package centipede.cbir.features;


public final class Features
{
	public IntensityHistogramMoments IntensityHistogramMoments;
	public HueLayout HueLayout;
	
	public Features( final IntensityHistogramMoments moments, final HueLayout hueLayout )
	{
		IntensityHistogramMoments = moments;
		HueLayout = hueLayout;
	}
	
	public double getDistance( final Features other )
	{
		return ( IntensityHistogramMoments.getDistance( other.IntensityHistogramMoments ) +
			HueLayout.getDistance( other.HueLayout ) );
	}
}
