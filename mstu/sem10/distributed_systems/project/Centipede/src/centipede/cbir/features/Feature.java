package centipede.cbir.features;


public abstract class Feature< T >
{
	public T Value;
	
	public abstract double getDistance( final Feature< T > other );
}
