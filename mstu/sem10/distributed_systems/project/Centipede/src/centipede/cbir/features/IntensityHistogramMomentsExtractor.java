package centipede.cbir.features;


import java.awt.image.BufferedImage;

import javax.media.jai.Histogram;


public final class IntensityHistogramMomentsExtractor implements FeatureExtractor< double[] >
{
	private final static int DEFAULT_MOMENTS_NUM = 3;
	
	private final int _momentsNum;
	private final Histogram _histogram;


	public IntensityHistogramMomentsExtractor()
	{
		this( DEFAULT_MOMENTS_NUM );
	}
	
	public IntensityHistogramMomentsExtractor( final int momentsNum )
	{
		_momentsNum = momentsNum;
		_histogram = new Histogram( Byte.MAX_VALUE - Byte.MIN_VALUE + 1, 0, Byte.MAX_VALUE - Byte.MIN_VALUE, 1 );
	}
	
	@Override
	public IntensityHistogramMoments extract( final BufferedImage image )
	{
		_histogram.clearHistogram();
		_histogram.countPixels( image.getData(), null, 0, 0, 1, 1 );
		
		final double[] moments = new double[ _momentsNum ];
		moments[ 0 ] = _histogram.getMean()[ 0 ];
		for ( int momentIndex = 1; momentIndex < _momentsNum; ++momentIndex )
			moments[ momentIndex ] = _histogram.getMoment( momentIndex + 1, false, true )[ 0 ];
		
		return new IntensityHistogramMoments( moments );
	}
}
