package centipede.cbir;

import centipede.cbir.DocumentBuilder;
import centipede.gui.RecursiveFileExtensionFilter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexReader;


public class CBIR
{
	private static final String INDEX_PATH = "pirs_index";
	private static final int MAX_SEARCH_RESULTS = 20;
	public static final String[] ImageExtensions = new String[] { "jpg", "jpeg" };
	public static final RecursiveFileExtensionFilter Filter = new RecursiveFileExtensionFilter( CBIR.ImageExtensions );
	
	private FSDirectory _indexDir;
	private IndexReader _reader;
	private DocumentSearcher _searcher;
	
	
	public CBIR()
	{
	}
	
	public void openIndex() throws IOException
	{
		_indexDir = FSDirectory.open( new File( INDEX_PATH ) );
		_reader = IndexReader.open( _indexDir );
		_searcher = new DocumentSearcher( MAX_SEARCH_RESULTS, _reader );
	}
	
	public void closeIndex() throws IOException
	{
		_searcher = null;
		
		_reader.close();
		_reader = null;
		
		_indexDir.close();
		_indexDir = null;
	}
	
	public DocumentSearcher getSearcher()
	{
		return ( _searcher );
	}
	
	public static SearchResult[] searchIndex( final File imageFile ) throws IOException
	{
		final CBIR cbir = new CBIR();
		cbir.openIndex();
		final SearchResult[] results = cbir.getSearcher().search( imageFile );
		cbir.closeIndex();
		
        return ( results );
	}
	
	public static void createIndex( final File collectionDir, final boolean verbose ) throws IOException
	{
		if ( verbose )
			System.out.println( "Creation of index started." );
		
		final FSDirectory indexDir = FSDirectory.open( new File( INDEX_PATH ) );
		final IndexWriter writer = new IndexWriter( indexDir, new SimpleAnalyzer(), true, IndexWriter.MaxFieldLength.UNLIMITED );
		final DocumentBuilder builder = new DocumentBuilder();

		final Queue< File > tasks = new LinkedList< File >();
		tasks.add( collectionDir );

		while ( !tasks.isEmpty() )
		{
	    		final File task = tasks.poll();
			if ( task.isFile() )
			{
				final File file = task;
				
				if ( verbose )
					System.out.println( file.getName() );
				
				try
				{
					final FileInputStream input = new FileInputStream( file );
					final Document document = builder.createDocument( input, file.getName() );
					writer.addDocument( document );
				}
				catch ( Exception e )
				{
					e.printStackTrace();
				}
			}
			else
			{
				final File directory = task;
				
				final File [] files = directory.listFiles( CBIR.Filter );
				for ( final File file : files )
					tasks.add( file );
			}
		}

		writer.optimize();
		writer.close();
		indexDir.close();

		if ( verbose )
			System.out.println( "Creation of index is finished." );
	}
}
