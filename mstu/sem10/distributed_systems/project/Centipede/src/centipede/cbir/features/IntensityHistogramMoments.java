package centipede.cbir.features;


public final class IntensityHistogramMoments extends Feature< double[] >
{
	public IntensityHistogramMoments( final double[] moments )
	{
		Value = moments;
	}
	
	@Override
	public double getDistance( final Feature< double[] > other )
	{
		double distance = 0.0;
		for ( int i = 0; i < Value.length; ++i )
			distance += Math.abs( Value[ i ] - other.Value[ i ] );
		
		return ( distance );
	}
}
