package centipede.cbir.features;


import java.awt.image.BufferedImage;


public interface FeatureExtractor< T >
{
	public Feature< T > extract( final BufferedImage image );
}