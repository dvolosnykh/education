package centipede.cbir;


import org.apache.lucene.document.Document;


public class SearchResult implements Comparable< SearchResult >
{
	public final Document Document;
	private double _distance;

	public SearchResult( final Document document, final double distance )
	{
		Document = document;
		_distance = distance;
	}
	
	public double getDistance()
	{
		return ( _distance );
	}
	
	public void setDistance( final double distance )
	{
		_distance = distance;
	}

	@Override
	public int compareTo( SearchResult other )
	{
		int result = 0;
		if ( !other.equals( this ) )
		{
			result = ( int )Math.signum( _distance - other._distance );
		
			if ( result == 0 )
				result = Document.hashCode() - other.Document.hashCode();
		}
		
		return ( result );
	}
}