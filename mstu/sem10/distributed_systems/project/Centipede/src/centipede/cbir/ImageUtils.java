package centipede.cbir;


import centipede.cbir.features.Features;
import centipede.cbir.features.HueLayout;
import centipede.cbir.features.HueLayoutExtractor;
import centipede.cbir.features.IntensityHistogramMoments;
import centipede.cbir.features.IntensityHistogramMomentsExtractor;

import java.awt.image.BandedSampleModel;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.WritableRaster;

import javax.media.jai.IHSColorSpace;
import javax.media.jai.operator.BandSelectDescriptor;


public final class ImageUtils
{
	public static Features extractFeatures( final BufferedImage rgbImage )
	{
		final BufferedImage ihsImage = ImageUtils.RGBtoIHS( rgbImage );
		
		final IntensityHistogramMomentsExtractor ihmExtractor = new IntensityHistogramMomentsExtractor();
		final IntensityHistogramMoments moments = ihmExtractor.extract( ImageUtils.getSampledImage( ihsImage, 0 ) );
		
		final HueLayoutExtractor hlExtractor = new HueLayoutExtractor();
		final HueLayout hueLayout = hlExtractor.extract( ImageUtils.getSampledImage( ihsImage, 1 ) );
		
		return new Features( moments, hueLayout );
	}
	
	public static BufferedImage RGBtoIHS( BufferedImage rgbImage )
	{
		rgbImage = BandSelectDescriptor.create( rgbImage, new int[] { 0, 1, 2 }, null ).getAsBufferedImage();
		
		final IHSColorSpace ihsColorSpace = IHSColorSpace.getInstance();
		final ColorModel ihsColorModel = new ComponentColorModel( ihsColorSpace, false, false, ColorModel.OPAQUE, DataBuffer.TYPE_BYTE );
		final BandedSampleModel sampleModel = new BandedSampleModel( ihsColorModel.getTransferType(), rgbImage.getWidth(),
			rgbImage.getHeight(), ihsColorModel.getNumComponents() );
		
		final WritableRaster ihsRaster = WritableRaster.createWritableRaster( sampleModel, null );
		ihsColorSpace.fromRGB( rgbImage.getRaster(), null, ihsRaster, null );
		
		return new BufferedImage( ihsColorModel, ihsRaster, false, null );
	}
	
	public static BufferedImage getSampledImage( final BufferedImage image, final int sampleIndex )
	{
		return BandSelectDescriptor.create( image, new int[] { sampleIndex }, null ).getAsBufferedImage();
	}
	
	public static double getRoundedAverage( final int[] array, final int max )
	{
		int sum = 0;
		for ( final int value : array )
			sum += Math.min( value, max - value );
		
		return ( sum / ( double )array.length );
	}
	
	public static double getRoundedDistance( double a, double b, final double min, final double max, final boolean ensureFittingToInterval )
	{
		final double length = max - min;
		
		if ( ensureFittingToInterval )
		{
			a = fitToInterval( a, min, max );
			b = fitToInterval( b, min, max );
		}
		
		final double absDiff = Math.abs( a - b );
		return Math.min( absDiff, length - absDiff );
	}
	
	public static double fitToInterval( double a, final double min, final double max )
	{
		return ( a - Math.floor( ( a - min ) / ( max - min ) ) );
	}
}
