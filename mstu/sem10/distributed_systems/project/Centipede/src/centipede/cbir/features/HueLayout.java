package centipede.cbir.features;

import centipede.cbir.ImageUtils;


public final class HueLayout extends Feature< double[] >
{
	public HueLayout( final double[] averages )
	{
		Value = averages;
	}
	
	@Override
	public double getDistance( final Feature< double[] > other )
	{
		double totalDistance = 0.0;
		for ( int i = 0; i < other.Value.length; ++i )
		{
			final int max = Byte.MAX_VALUE - Byte.MIN_VALUE + 1;
			final double distance = ImageUtils.getRoundedDistance( Value[ i ], other.Value[ i ], 0, max, false );
			totalDistance += Math.pow( distance, 2 );
		}
		
		return ( totalDistance );
	}
}
