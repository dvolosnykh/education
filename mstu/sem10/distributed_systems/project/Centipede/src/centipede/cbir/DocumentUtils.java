package centipede.cbir;


import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;

import org.apache.lucene.document.Document;

import centipede.cbir.features.Features;
import centipede.cbir.features.HueLayout;
import centipede.cbir.features.IntensityHistogramMoments;


public final class DocumentUtils
{
	public static Features getFeatures( final Document document )
	{
		byte[] bytes = document.getBinaryValue( DocumentBuilder.FIELD_NAME_INTENSITY_HISTOGRAM_MOMENTS );
		double[] doubles = DocumentUtils.byteArrayToDoubleArray( bytes );
		final IntensityHistogramMoments moments = new IntensityHistogramMoments( doubles );
		
		bytes = document.getBinaryValue( DocumentBuilder.FIELD_NAME_HUE_LAYOUT );
		doubles = DocumentUtils.byteArrayToDoubleArray( bytes );
		final HueLayout hueLayout = new HueLayout( doubles );
		
		return new Features( moments, hueLayout );
	}
	
	public static byte[] doubleArrayToByteArray( final double[] doubles )
	{
		final byte[] bytes = new byte[ doubles.length * ( Double.SIZE / 8 ) ];
		final ByteBuffer byteBuffer = ByteBuffer.wrap( bytes );
		final DoubleBuffer doubleBuffer = byteBuffer.asDoubleBuffer();
		doubleBuffer.put( doubles );
        return ( bytes );
	}
	
	public static double[] byteArrayToDoubleArray( final byte[] bytes )
	{
		final ByteBuffer byteBuffer = ByteBuffer.wrap( bytes );
		final DoubleBuffer doubleBuffer = byteBuffer.asDoubleBuffer();
		final double[] doubles = new double[ bytes.length / ( Double.SIZE / 8 ) ];
		doubleBuffer.get( doubles );
		return ( doubles );
	}
	
	public static byte[] intArrayToByteArray( final int[] ints )
	{
		final byte[] bytes = new byte[ ints.length * ( Integer.SIZE / 8 ) ];
		final ByteBuffer byteBuffer = ByteBuffer.wrap( bytes );
		final IntBuffer intBuffer = byteBuffer.asIntBuffer();
		intBuffer.put( ints );
        return ( bytes );
	}
	
	public static int[] byteArrayToIntArray( final byte[] bytes )
	{
		final ByteBuffer byteBuffer = ByteBuffer.wrap( bytes );
		final IntBuffer intBuffer = byteBuffer.asIntBuffer();
		final int[] ints = new int [ bytes.length / ( Double.SIZE / 8 ) ];
		intBuffer.get( ints );
		return ( ints );
	}
}
