package centipede.gui;


import java.io.File;
import java.io.FileFilter;
import java.util.TreeSet;


public final class RecursiveFileExtensionFilter implements FileFilter
{
	TreeSet< String > _extensions;

	
	public RecursiveFileExtensionFilter( final String[] extensions )
	{
		_extensions = new TreeSet< String >();
		for ( final String extension : extensions )
			_extensions.add( extension.toLowerCase() );
	}

	@Override
	public boolean accept( final File file )
	{
		return ( file.isDirectory() || _extensions.contains( FileUtils.getExtension( file.getName() ).toLowerCase() ) );
	}
}