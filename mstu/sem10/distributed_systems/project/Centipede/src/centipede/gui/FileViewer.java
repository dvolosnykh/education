package centipede.gui;

import java.io.File;
import java.io.FileFilter;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.JTree;
import javax.swing.tree.*;


@SuppressWarnings( "serial" )
class FileViewer extends JTree
{
	private FileFilter _filter;
	
	public FileViewer( final FileFilter filter )
	{
		super( new Object[ 0 ] );
		
		_filter = filter;
		this.getSelectionModel().setSelectionMode( TreeSelectionModel.SINGLE_TREE_SELECTION );
	}

	public FileViewer( final File rootDir, final FileFilter filter )
	{
		this( filter );
		this.setRootDir( rootDir );
	}
	
	public void setRootDir( final File rootDir )
	{
		if ( rootDir != null )
		{
			final TreeNode rootNode = this.buildDirectoryTree( rootDir );
			final DefaultTreeModel treeModel = new DefaultTreeModel( rootNode );
			this.setModel( treeModel );
		}
		else
			this.setModel( null );
	}
	
	public File getSelectedFile()
	{
		File file = null;
		final TreePath path = this.getSelectionPath();
		if ( path != null )
		{
			final FileTreeNode node = ( FileTreeNode )path.getLastPathComponent();
			file = node.File;
		}
		
		return ( file );
	}
	
	private FileTreeNode buildDirectoryTree( final File rootDir )
	{
		assert ( rootDir.isDirectory() );
		
		class Task
		{
			public final File File;
			public final FileTreeNode ParentNode;
			
			public Task( final File file, final FileTreeNode parentNode )
			{
				File = file;
				ParentNode = parentNode;
			}
		}
		
		FileTreeNode rootNode = null;
		final Queue< Task > tasks = new LinkedList< Task >();
	    tasks.add( new Task( rootDir, rootNode ) );
	    
	    while ( !tasks.isEmpty() )
	    {
	    	final Task task = tasks.poll();
	    	
	    	FileTreeNode childNode = null;
			if ( task.File.isFile() )
			{
				final File file = task.File;
				
				childNode = new FileTreeNode( file.getName(), file, false );
			}
			else
			{
				final File directory = task.File;
				
				childNode = new FileTreeNode( directory.getName(), null, true );
				
				final File [] files = directory.listFiles( _filter );
				for ( final File file : files )
					tasks.add( new Task( file, childNode ) );
			}
			
			if ( task.ParentNode != null )
				task.ParentNode.add( childNode );
			else
				rootNode = childNode;
	    }
		
		return ( rootNode );
	}
}
