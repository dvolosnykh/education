package centipede.gui;


import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.*;
import java.awt.image.*;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import centipede.cbir.CBIR;
import centipede.cbir.ImageUtils;
import centipede.romip.ROMIP;


@SuppressWarnings( "serial" )
public final class GUI extends JFrame implements TreeSelectionListener, MouseListener
{
	private static final int IMAGE_PADDING = 3;
	private static final int IMAGE_ROWS = 2;
	private static final int IMAGE_COLUMNS = 2;
	
	private static final int LEFT_PANE_WIDTH = 200;
	
	private final FileViewer _fileViewer = new FileViewer( CBIR.Filter );
	private final JPanel _panelSource = new JPanel( true );
	private final JPanel _panelHue = new JPanel( true );
	private final JPanel _panelSaturation = new JPanel( true );
	private final JPanel _panelIntensity = new JPanel( true );
	
	
	public GUI() throws IOException
	{
		this.setVisible( true );
		this.setExtendedState( JFrame.MAXIMIZED_BOTH );
		this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		
		this.initializeComponents();
		
		final File collectionDir = getCollectionDirectory();
		if ( collectionDir != null )
			_fileViewer.setRootDir( collectionDir );
	}
	
	private void initializeComponents() throws IOException
	{
		// configure left panel.
		_fileViewer.addTreeSelectionListener( this );
		final JScrollPane viewPanel = new JScrollPane( _fileViewer );
		viewPanel.setAlignmentX( Component.CENTER_ALIGNMENT );
		
		final JButton saveButton = new JButton( "Сохранить" );
		saveButton.addMouseListener( this );
		saveButton.setAlignmentX( Component.CENTER_ALIGNMENT );
		saveButton.setMaximumSize( new Dimension( LEFT_PANE_WIDTH, 0 ) );
		
		final JPanel leftPanel = new JPanel();
		leftPanel.setLayout( new BoxLayout( leftPanel, BoxLayout.PAGE_AXIS ) );
		leftPanel.add( viewPanel );
		leftPanel.add( Box.createRigidArea( new Dimension( 0, 5 ) ) );
		leftPanel.add( saveButton );
		
		// configure right panel.
		final JPanel rightPanel = new JPanel( new GridLayout( IMAGE_ROWS, IMAGE_COLUMNS, IMAGE_PADDING, IMAGE_PADDING ) );
		rightPanel.add( _panelSource );
		rightPanel.add( _panelIntensity );
		rightPanel.add( _panelHue );
		rightPanel.add( _panelSaturation );
		
		// main pane.
		final JSplitPane mainPane = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT, leftPanel, rightPanel );
		mainPane.setDividerLocation( LEFT_PANE_WIDTH );
		
		this.setContentPane( mainPane );
	}

	public static File getCollectionDirectory()
	{
		final File cwd = new File( System.getProperty( "user.dir" ) );
		
		final JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory( cwd );
		chooser.setDialogTitle( "Specify collection directory." );
		chooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );

		final int choice = chooser.showOpenDialog( null );
		final File collectionDir = ( choice == JFileChooser.APPROVE_OPTION ? chooser.getSelectedFile() : null );

		return ( collectionDir );
	}
	
	public static File getSaveDirectory()
	{
		final File cwd = new File( System.getProperty( "user.dir" ) );
		
		final JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory( cwd );
		chooser.setDialogTitle( "Specify destination folder." );
		chooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );

		final int choice = chooser.showSaveDialog( null );
		final File saveDir = ( choice == JFileChooser.APPROVE_OPTION ? chooser.getSelectedFile() : null );
		
		return ( saveDir );
	}
	
	public static File getQueryImageFile()
	{
		return getFile( new FileNameExtensionFilter( "Images", CBIR.ImageExtensions ), "Choose image to search." );
	}
	
	public static File getSearchTasksFile()
	{
		return getFile( new FileNameExtensionFilter( "Xml", ROMIP.TasksExtensions ), "Specify tasks xml-file." );
	}
	
	public static File getResultsTasksFile()
	{
		return getFile( new FileNameExtensionFilter( "Xml", ROMIP.TasksExtensions ), "Specify results xml-file." );
	}
	
	private static File getFile( final FileNameExtensionFilter filter, final String promptText )
	{
		final File cwd = new File( System.getProperty( "user.dir" ) );
		
		final JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory( cwd );
		chooser.setDialogTitle( promptText );
		chooser.setFileSelectionMode( JFileChooser.FILES_ONLY );
		chooser.addChoosableFileFilter( filter );

		final int choice = chooser.showOpenDialog( null );
		final File file = ( choice == JFileChooser.APPROVE_OPTION ? chooser.getSelectedFile() : null );
		
		return ( file );
	}
	
	public void paint( Graphics _unused )
	{
		super.paint( _unused );
		
		try
		{
			BufferedImage rgbImage = null;
			final File file = _fileViewer.getSelectedFile();
			if ( file != null )
				rgbImage = ImageIO.read( file );
			
			if ( rgbImage != null )
			{
				final int viewWidth = _panelSource.getWidth();
				final int viewHeight = _panelSource.getHeight();
				
				final double scaleX = viewWidth / ( double )rgbImage.getWidth();
				final double scaleY = viewHeight / ( double )rgbImage.getHeight();
				final double scale = Math.min( scaleX, scaleY );
				
				final AffineTransform transform = AffineTransform.getScaleInstance( scale, scale );
			    final AffineTransformOp op = new AffineTransformOp( transform, AffineTransformOp.TYPE_BILINEAR );
			    rgbImage = op.filter( rgbImage, null );
				
			    final int originX = ( viewWidth - rgbImage.getWidth() ) / 2;
			    final int originY = ( viewHeight - rgbImage.getHeight() ) / 2;
				
			    JPanel panel = _panelSource;
			    Graphics g = panel.getGraphics();
			    g.fillRect( 0, 0, viewWidth, viewHeight );
				g.drawImage( rgbImage, originX, originY, null );
				g.dispose();
				
				final BufferedImage ihsImage = ImageUtils.RGBtoIHS( rgbImage );
				
				panel = _panelIntensity;
				g = panel.getGraphics();
				g.fillRect( 0, 0, viewWidth, viewHeight );
				g.drawImage( ImageUtils.getSampledImage( ihsImage, 0 ), originX, originY, null );
				g.dispose();
				
				panel = _panelHue;
				g = panel.getGraphics();
				g.fillRect( 0, 0, viewWidth, viewHeight );
				g.drawImage( ImageUtils.getSampledImage( ihsImage, 1 ), originX, originY, null );
				g.dispose();
				
				panel = _panelSaturation;
				g = panel.getGraphics();
				g.fillRect( 0, 0, viewWidth, viewHeight );
				g.drawImage( ImageUtils.getSampledImage( ihsImage, 2 ), originX, originY, null );
				g.dispose();
			}
		}
		catch ( Exception ex )
		{
			ex.printStackTrace();
		}
	}
	
	@Override
	public void valueChanged( TreeSelectionEvent event )
	{
		this.repaint();
	}

	@Override
	public void mouseClicked( MouseEvent e )
	{
		final File saveDir = getSaveDirectory();
		if ( saveDir != null )
		{
			try
			{
				BufferedImage rgbImage = null;
				final File file = _fileViewer.getSelectedFile();
				if ( file != null )
					rgbImage = ImageIO.read( file );
				
				if ( rgbImage != null )
				{
					final String[] parts = FileUtils.splitName( file.getName() );
					final String directory = saveDir.getPath();
					final String basename = parts[ 0 ];
					final String extension = parts[ 1 ];
					
					String filename = directory + File.separator + basename + "rgb" + "." + extension;
					ImageIO.write( rgbImage, extension, new File( filename ) );
					
					final BufferedImage ihsImage = ImageUtils.RGBtoIHS( rgbImage );
					
					filename = directory + File.separator + basename + "i" + "." + extension;
					ImageIO.write( ImageUtils.getSampledImage( ihsImage, 0 ), extension, new File( filename ) );
					
					filename = directory + File.separator + basename + "h" + "." + extension;
					ImageIO.write( ImageUtils.getSampledImage( ihsImage, 1 ), extension, new File( filename ) );
					
					filename = directory + File.separator + basename + "s" + "." + extension;
					ImageIO.write( ImageUtils.getSampledImage( ihsImage, 2 ), extension, new File( filename ) );
				}
				
				JOptionPane.showMessageDialog( this, "Изображения успешно сохранены." );
			}
			catch ( Exception ex )
			{
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void mousePressed( MouseEvent e )
	{
	}

	@Override
	public void mouseReleased( MouseEvent e )
	{
	}

	@Override
	public void mouseEntered( MouseEvent e )
	{
	}

	@Override
	public void mouseExited( MouseEvent e )
	{
	}

	/*
	try
	{
		String host = "vkontakte.ru";
		System.out.println( "getAllByName" );
		InetAddress[] addresses = InetAddress.getAllByName( host );
		for ( int i = 0; i < addresses.length; ++i )
			System.out.println( addresses[ i ].getHostAddress() );
		System.out.println( "getByName" );
		System.out.println( InetAddress.getByName( host ) );
	}
	catch ( Exception e )
	{
		System.out.println( e.getMessage() );
	}*/
}