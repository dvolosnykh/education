package centipede.gui;


import java.io.File;


public final class FileUtils
{
	public static String getBaseName( final String filename )
	{
		return splitName( filename )[ 0 ];
	}
	
	public static String getExtension( final String filename )
	{
		return splitName( filename )[ 1 ];
	}
	
	public static String[] splitName( final String filename )
	{
		final String[] parts = new String[ 2 ];
		
		final int dotIndex = filename.lastIndexOf( '.', filename.length() - 2 );
		if ( dotIndex > 0 )
		{
			parts[ 0 ] = filename.substring( 0, dotIndex );
			parts[ 1 ] = filename.substring( dotIndex + 1 );
		}
		else
		{
			parts[ 0 ] = filename;
			parts[ 1 ] = "";
		}
		
		return ( parts );
	}
	
	public static String[] splitPath( final String path )
	{
		final String[] parts = new String[ 2 ];
		
		final int lastSeparatorIndex = path.lastIndexOf( File.separator );		
	    if ( lastSeparatorIndex >= 0 )
	    {
	    	parts[ 0 ] = path.substring( 0, lastSeparatorIndex );
	        parts[ 1 ] = path.substring( lastSeparatorIndex + 1 );
	    }
	    else
	    {
	    	parts[ 0 ] = "";
	    	parts[ 1 ] = path;
	    }
	    
	    return ( parts );
	}
}
