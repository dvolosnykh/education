package centipede.gui;


import java.io.File;
import javax.swing.tree.*;


@SuppressWarnings( "serial" )
class FileTreeNode extends DefaultMutableTreeNode
{
	final public File File;
	
	public FileTreeNode( final String name, final File file, final boolean allowsChildren )
	{
		super( name, allowsChildren );
		File = file;
	}
}