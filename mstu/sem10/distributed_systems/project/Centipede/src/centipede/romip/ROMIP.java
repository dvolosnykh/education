package centipede.romip;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import centipede.cbir.CBIR;
import centipede.cbir.SearchResult;


public final class ROMIP
{
	public static final String SystemID = "puce";
	public static final String RunID = "1";
	public static final String NAMESPACE_PREFIX = "romip";

	public static final String[] TasksExtensions = { "xml" };
	
	
	public static void runSearchTasks( final File tasksFile, final File collectionDirectory, final File resultsFile, final boolean verbose ) throws ParserConfigurationException, SAXException, IOException, TransformerFactoryConfigurationError, TransformerException
	{
		final DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		
		// start creating file with results.
		final Document resultsDocument = builder.newDocument();
		final Element taskResultElement = formHeaderOfResultsDocument( resultsDocument );
		
		// read file with tasks.
		final Document tasksDocument = builder.parse( tasksFile );
		final NodeList taskList = tasksDocument.getElementsByTagName( "task" );
		
		final CBIR cbir = new CBIR();
		cbir.openIndex();
		for ( int i = 0; i < taskList.getLength(); ++i )
		{
			// get node for task.
			final Node taskNode = taskList.item( i );
			final String taskId = taskNode.getAttributes().getNamedItem( "id" ).getNodeValue();
			
			if ( verbose )
				System.out.printf( "Task %s:\t", taskId );
			
			// get node for query document of current task.
			final Node documentNode = taskNode.getFirstChild().getNextSibling();
			assert( documentNode != null );
			final String folder = documentNode.getAttributes().getNamedItem( "folder" ).getNodeValue();
			final String queryDocId = documentNode.getAttributes().getNamedItem( "docID" ).getNodeValue();
			
			if ( verbose )
				System.out.printf( "%s/%s\n", folder, queryDocId );
			
			final File queryImageFile = new File( new File( collectionDirectory, folder ), queryDocId );
			
			final SearchResult[] results = cbir.getSearcher().search( queryImageFile );
			
			final Element doclistElement = resultsDocument.createElement( "doclist" );
			taskResultElement.appendChild( doclistElement );
			doclistElement.setAttribute( "taskID", taskId );
			
			for ( final SearchResult result : results )
			{
				final String docId = result.Document.get( centipede.cbir.DocumentBuilder.FIELD_NAME_IDENTIFIER );
				
				if ( verbose )
					System.out.printf( "\t%s\n", docId );
				
				final Element docIdElement = resultsDocument.createElement( "docID" );
				doclistElement.appendChild( docIdElement );
				docIdElement.appendChild( resultsDocument.createTextNode( docId ) );
			}
		}
		cbir.closeIndex();
		
		saveDocumentToFile( resultsDocument, resultsFile );
	}
	
	private static Element formHeaderOfResultsDocument( final Document resultsDocument )
	{
		resultsDocument.setXmlVersion( "1.0" );
		resultsDocument.setXmlStandalone( false );
		
		final Element taskResultElement = resultsDocument.createElement( NAMESPACE_PREFIX + ":taskresult" );
		resultsDocument.appendChild( taskResultElement );
		taskResultElement.setAttribute( "xmlns:" + NAMESPACE_PREFIX, "http://www.romip.ru/data/adhoc" );
		taskResultElement.setAttribute( "collectionId", "ROMIP_IMG-FLICKR" );
		taskResultElement.setAttribute( "trackId", "image-cbir-2008" );
		
		final Element runElement = resultsDocument.createElement( NAMESPACE_PREFIX + ":run" );
		taskResultElement.appendChild( runElement );
		
		final Element systemIdElement = resultsDocument.createElement( NAMESPACE_PREFIX + ":systemID" );
		runElement.appendChild( systemIdElement );
		systemIdElement.appendChild( resultsDocument.createTextNode( SystemID ) );
		
		final Element runIdElement = resultsDocument.createElement( NAMESPACE_PREFIX + ":runID" );
		runElement.appendChild( runIdElement );
		runIdElement.appendChild( resultsDocument.createTextNode( RunID ) );
		
		return ( taskResultElement );
	}
	
	private static void saveDocumentToFile( final Document document, final File resultsFile ) throws TransformerFactoryConfigurationError, TransformerException, IOException
	{
		final DOMSource source = new DOMSource( document );
		final StreamResult result = new StreamResult( new FileOutputStream( resultsFile ) );
		
		final Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty( OutputKeys.INDENT, "yes");
		transformer.transform( source, result );

		result.getOutputStream().flush();
	}
}