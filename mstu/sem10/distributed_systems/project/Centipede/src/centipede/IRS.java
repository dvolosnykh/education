package centipede;


import java.io.File;
import java.util.Formatter;

import centipede.cbir.CBIR;
import centipede.gui.GUI;
import centipede.romip.ROMIP;

import javax.swing.UIManager;


enum Mode { CreateIndex, Search, Viewer, Undefined };


public class IRS
{
	public static void main( final String[] args )
	{
		final Mode mode = parseArgs( args );
		
		try
		{
			UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
			
			switch ( mode )
			{
			case CreateIndex:
				{
					final File collectionDir = GUI.getCollectionDirectory();
					if ( collectionDir != null )
						CBIR.createIndex( collectionDir, true );
				}
				break;
				
			case Search:
				{
					final File tasksFile = GUI.getSearchTasksFile();
					boolean ok = ( tasksFile != null );
					
					File collectionDirectory = null;
					if ( ok )
					{
						collectionDirectory = GUI.getCollectionDirectory();
						ok = ( collectionDirectory != null );
					}
					
					File resultsFile = null;
					if ( ok )
					{
						resultsFile = GUI.getResultsTasksFile();
						ok = ( tasksFile != null );
					}
						
					if ( ok )
						ROMIP.runSearchTasks( tasksFile, collectionDirectory, resultsFile, true );
				}
				break;
				
			case Viewer:
				new GUI();
				break;
				
			default:
				printUsageError();
				System.exit( 1 );
			}
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}
	}
	
	private static Mode parseArgs( final String[] args )
	{
		Mode mode = Mode.Undefined;
		
		if ( args.length == 1 )
		{
			final String modeStr = args[ 0 ];
			if ( modeStr.equals( "create" ) )
				mode = Mode.CreateIndex;
			else if ( modeStr.equals( "search" ) )
				mode = Mode.Search;
			else if ( modeStr.equals( "viewer" ) )
				mode = Mode.Viewer;
		}
		
		return ( mode );
	}
	
	private static void printUsageError()
	{
		final Formatter formatter = new Formatter();
		formatter.format( "Usage: java Centipede <mode>\n" );
		formatter.format( "\twhere possible modes are:\n" );
		formatter.format( "\tcreate --- Create index.\n" );
		formatter.format( "\tsearch --- Search.\n" );
		formatter.format( "\tviewer --- Run viewer.\n" );
		System.err.println( formatter );
	}
}
