package ru.cutephrog

import actors.Actor.actor
import actors.remote.Node
import actors.remote.RemoteActor._
import crawler.ScheduleURLs
import java.net.{MalformedURLException, URL}

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 07.09.11
 * Time: 23:29
 * To change this template use File | Settings | File Templates.
 */

object Controller {
  object Command extends Enumeration {
    val Role = Value(Runner.PROPERTY_ROLE)
    val Url = Value("url")
    val Stop = Value("stop")
    val Help = Value("help")
  }

  private[this] val HELP_MESSAGE =
    """|usage: remoteIP command [arg]
       |  commands:
       |    %s [%s, %s]
       |      switch role of the remote runner.
       |    %s [url]
       |      if the runner acts in crawler role, it will schedule given URL.
       |    %s
       |      stop the runner.
       |    %s
       |      print this message."""".stripMargin
    .format(Runner.PROPERTY_ROLE, Runner.Role.Crawler, Runner.Role.SearchEngine,
            Command.Url,
            Command.Stop,
            Command.Help)

  def main(args: Array[String]) {
    require(Range.inclusive(2, 3) contains args.size, HELP_MESSAGE)

    actor {
      val remoteAddress = args(0)
      val remotePort = 8000
      val remoteName = 'CutePhrog
      Console print "Connecting to remote address %s ... ".format(remoteAddress)
      val remoteRunner = select(Node(remoteAddress, remotePort), remoteName)
      Console println "Done"

      val command = try {
        Command.withName(args(1))
      } catch {
        case _ => Command.Help
      }
      command match {
        case Command.Role =>
          val role = Runner.Role.withName(args(2))
          Console println "Setting role to %s".format(role)
          remoteRunner ! SetRole(role)
        case Command.Url =>
          try {
            val url = new URL(args(2))
            Console println "Scheduling URL %s".format(url)
            remoteRunner ! WorkerRequest(ScheduleURLs(List(url)))
          } catch {
            case e: MalformedURLException =>
            Console println "Malformed URL."
          }
        case Command.Stop =>
          Console println "Stopping"
          remoteRunner ! 'Exit
        case Command.Help =>
          Console println HELP_MESSAGE
      }
    }
  }
}