package ru.cutephrog
package crawler

import java.net.URL
import com.mongodb.casbah.Imports._

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 08.09.11
 * Time: 10:39
 * To change this template use File | Settings | File Templates.
 */

private[crawler]
final class MongoFrontier(dbName: String, collectionName: String) extends Frontier {
  private[this] val urls = MongoConnection()(dbName)(collectionName)

  def addURLs(newUrls: List[URL]) {
    for (url <- newUrls; urlStr = url.toString;
         query = MongoDBObject(MongoFrontier.URL -> urlStr)
         if urls.findOne(query) == None) {
      urls += MongoDBObject(MongoFrontier.URL -> urlStr, MongoFrontier.VISITED -> false)
    }
  }

  def nextURL(): Option[(AnyRef, URL)] = {
    val query = MongoDBObject(MongoFrontier.VISITED -> false)
    val fields = MongoDBObject(MongoFrontier.URL -> "")
    urls.findOne(query, fields).map { obj =>
      val urlStr = obj.getAs[String](MongoFrontier.URL).get
      (obj._id.get, new URL(urlStr))
    }
  }

  def markAsVisited(id: AnyRef) {
    urls.update(MongoDBObject("_id" -> id), $set(MongoFrontier.VISITED -> true))
  }

  def isEmpty = urls.isEmpty
  def size = urls.size
}

private[crawler]
object MongoFrontier {
  private val URL = "url"
  private val VISITED = "visited"
}