package ru.cutephrog
package crawler

import scala.actors.{Actor, TIMEOUT}
import java.net.URL

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 08.09.11
 * Time: 10:21
 * To change this template use File | Settings | File Templates.
 */

private[cutephrog]
final case class ScheduleURLs(url: List[URL])


private[cutephrog]
final class CrawlerActor extends Crawler with Worker with Actor {
  protected[this] val frontier = new MongoFrontier(Worker.DB_NAME, "urls")
  protected[this] val role = Runner.Role.Crawler
  private[this] var running = false


  def act() {
    Console println "%s started.".format(getClass.getSimpleName)
    initialize()

    running = true
    while (running) {
      Console println "\nPeek into message queue."
      receiveWithin(0) { case message => onMessage(message) }
    }

    deinitialize()
    Console println "%s stopped.".format(getClass.getSimpleName)
  }

  private[this] def onMessage(message: Any) {
    message match {
      case TIMEOUT =>
        Console println "No messages. Continue crawling."
        if (!frontier.isEmpty) {
          frontier.nextURL() foreach { x => val (id, url) = x
            val (anchors, images) = crawl(url)
            frontier.markAsVisited(id)
            val groups = anchors groupBy  { url =>
              registries(Runner.Role.Crawler.id).memberInCharge(MembersRegistry.hash(url.getHost))
            }

            groups foreach { group => val (destination, urls) = group
              if (destination == channel.getAddress) {
                frontier.addURLs(urls)
              } else {
                registries(Runner.Role.Crawler.id).store(destination, urls) foreach { totalUrls =>
                  send(destination, ScheduleURLs(totalUrls))
                  Console println "Passed %d urls to %s".format(totalUrls.size, destination)
                }
              }
            }
          }
        } else {
          Console println "Nothing to crawl. Waiting for any message."
          Actor.receive { case msg => onMessage(msg) }
        }
      case ScheduleURLs(urls) =>
        Console println "Provided with %d URLs.".format(urls.size)
        frontier.addURLs(urls)
        Console println "Added urls"
      case 'Exit =>
        running = false
      case _ =>
        Console println "Unknown message."
    }
  }
}