package ru.cutephrog
package crawler

import java.net.{URI, URL}

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 15.09.11
 * Time: 21:07
 * To change this template use File | Settings | File Templates.
 */

private[crawler]
abstract class Frontier {
  def addURLs(newURLs: List[URL])
  def nextURL(): Option[(AnyRef, URL)]
  def markAsVisited(id: AnyRef)
  def isEmpty: Boolean
  def size: Int
}

private[crawler]
object Frontier {
  private[this] val PROTOCOL_HTTP = "http"

  def resolveURL(context: URL, strUrl: String): Option[URL] = {
    val url = try {
      Some(new URL(context, strUrl))
    } catch {
      case _ => None
    }
    url.flatMap(resolveURL)
  }

  def resolveURL(url: URL): Option[URL] = {
    if (url.getProtocol == PROTOCOL_HTTP) {
      Some(new URI(url.getProtocol, url.getAuthority, url.getPath, url.getQuery, null).toURL)
    } else {
      None
    }
  }
}