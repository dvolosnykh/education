package ru.cutephrog
package crawler

import scala.io.Source
import java.net.URL

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 15.09.11
 * Time: 21:22
 * To change this template use File | Settings | File Templates.
 */

private[cutephrog]
trait Crawler {
  protected[this] def frontier: Frontier

  protected[this]
  final def crawl(url: URL): (List[URL], List[URL]) = {
    Console println "Crawling URL: %s".format(url)
    connect(url) match {
      case Some(source) =>
        val result = PageParser.parse(url, source)
        Console println "Frontier size: %d".format(frontier.size)
        result
      case None =>
        (List.empty[URL], List.empty[URL])
    }
  }

  private[this]
  def connect(url: URL): Option[Source] = {
    val timeStart = System.currentTimeMillis

    val source = try {
      val connection = url.openConnection()
      connection setConnectTimeout Crawler.CONNECTION_TIMEOUT
      Some(Source fromInputStream connection.getInputStream)
    } catch {
      case _ => None
    }

    val elapsedTime = System.currentTimeMillis - timeStart
    val prefix = if (source != None) "Connected in" else "Failed after"
    Console println "%s %f seconds.".format(prefix, elapsedTime / 1000.0)

    source
  }
}

private[cutephrog]
object Crawler {
  private val CONNECTION_TIMEOUT = 3000
}