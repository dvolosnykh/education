package ru.cutephrog

import scala.actors.AbstractActor
import scala.actors.remote.JavaSerializer
import scala.collection.JavaConversions._
import scala.collection.mutable.{SynchronizedSet, HashSet}
import org.jgroups.{ReceiverAdapter, View, Message, JChannel, Address}
import com.mongodb.casbah.Imports._

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 12.09.11
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */

private[cutephrog]
final case class MyRole(role: Runner.Role.Value)


private[cutephrog]
trait Worker {
  worker: AbstractActor =>

  private[Worker]
  final class Receiver extends ReceiverAdapter {
    override def viewAccepted(view: View) {
      val allMembers = view.getMembers.toSet
      val enteredMembers = (allMembers /: registries ){ (newMembers, registry) =>
        val leavedMembers = registry.members &~ newMembers
        registry membersLeaved leavedMembers.toSet
        newMembers &~ registry.members
      }
      Console println registries

      if (channel.isConnected) {
        // This worker is already online, this handler is called because some one else has joined the group.
        // Tell this newbies about this worker's role.
        enteredMembers foreach { member =>
          send(member, MyRole(role))
        }
      }
    }

    override def receive(jmessage: Message) {
      val source = jmessage.getSrc
      val message = serializer deserialize jmessage.getBuffer

      message match {
        case MyRole(thatRole) =>
          for (role <- Runner.Role.values;
               registry = registries(role.id)) {
            if (role.id != thatRole.id) {
              if (registry.members contains source) {
                registry memberLeaved source
              }
            } else {
              if (!(registry.members contains source)) {
                registry memberEntered source
              }
            }
          }
          Console println registries
        case _ =>
          Console println "Passed message to worker."
          worker ! message
      }
    }
  }

  private[this] val db = MongoConnection()(Worker.DB_NAME)
  protected[this] val registries = List.fill(Runner.Role.maxId)(new MembersRegistry(db))
  private[this] val receiver = new Receiver
  protected[this] val channel = new JChannel("tcp.xml")
  private[this] val serializer = new JavaSerializer(null, getClass.getClassLoader)

  protected[this] def role: Runner.Role.Value


  protected[this]
  def initialize() {
    channel setReceiver receiver
    channel connect Worker.CHANNEL_NAME
    Console println "Connected to channel %s".format(Worker.CHANNEL_NAME)

    // This worker has just started, tell every one in the group that we are online from now on.
    multicast(MyRole(role))
  }

  protected[this]
  def deinitialize() {
    Console println "Closed channel %s".format(Worker.CHANNEL_NAME)
    channel.close()
  }

  protected[this]
  final def multicast(message: AnyRef) { send(null, message) }

  protected[this]
  final def send(destination: Address, message: AnyRef) {
    val buffer = serializer serialize message
    val jmessage = new Message(destination, null, buffer)
    channel send jmessage
  }
}

private[cutephrog]
object Worker {
  private val CHANNEL_NAME = "CutePhrog"
  val DB_NAME = "CutePhrog"
}