package ru.cutephrog.searchengine

import actors.{Actor, TIMEOUT}
import actors.Actor._

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 08.09.11
 * Time: 15:31
 * To change this template use File | Settings | File Templates.
 */

class SearchEngine extends Actor {
  def act() {
    Console println "I am the " + getClass.getCanonicalName + "!"

    var continue = true
    loopWhile(continue) {
      react {
//      reactWithin(0) {
        case 'Exit =>
          continue = false
//        case TIMEOUT =>
//
      }
    }
  }

  private def work(message: Any) {
    Console println "What? What you say me?"
  }
}