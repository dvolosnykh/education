package ru.cutephrog
package crawler

import scala.io.Source
import scala.collection.mutable.ListBuffer
import scala.xml.{NamespaceBinding, MetaData}
import scala.xml.parsing.XhtmlParser
import java.net.URL

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 10.09.11
 * Time: 11:04
 * To change this template use File | Settings | File Templates.
 */

private[crawler]
final class PageParser(sourceURL: URL, override val input: Source) extends XhtmlParser(input) {
  private var anchors =  ListBuffer.empty[URL]
  private var images = ListBuffer.empty[URL]

  override def elemStart(pos: Int, pre: String, label: String, attrs: MetaData, scope: NamespaceBinding) {
    val attributes = attrs.asAttrMap
    label match {
      case PageParser.TAG_ANCHOR =>
        attributes.get(PageParser.ATTR_HREF) foreach {
          Frontier.resolveURL(sourceURL, _) foreach { anchors += _ }
        }
      case PageParser.TAG_IMAGE =>
        attributes.get(PageParser.ATTR_SRC) foreach {
          Frontier.resolveURL(sourceURL, _) foreach { images += _ }
        }
      case _ =>
    }

    super.elemStart(pos, pre, label, attrs, scope)
  }
}

private[crawler]
object PageParser {
  private val TAG_ANCHOR = "a"
  private val TAG_IMAGE = "img"
  private val ATTR_HREF = "href"
  private val ATTR_SRC = "src"

  def parse(url: URL, input: Source): (List[URL], List[URL]) = {
    val timeStart = System.currentTimeMillis

    val parser = new PageParser(url, input)
    val ok = try {
      parser.initialize.document()
      true
    } catch {
      case _ => false
    }

    val elapsedTime = System.currentTimeMillis - timeStart
    val prefix = if (ok) "Downloaded and parsed in" else "Failed parsing after"
    Console println "%s %f seconds.".format(prefix, elapsedTime / 1000.0)
    Console println "Anchors found: %d".format(parser.anchors.size)
    Console println "Images found: %d".format(parser.images.size)

    (parser.anchors.toList, parser.images.toList)
  }
}