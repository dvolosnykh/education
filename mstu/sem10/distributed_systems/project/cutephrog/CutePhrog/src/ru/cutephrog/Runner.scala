package ru.cutephrog

import scala.actors.{Exit, Actor}
import scala.actors.Actor._
import scala.actors.remote.RemoteActor._
import java.util.Properties
import java.io.{File, FileOutputStream, FileInputStream}
import ru.cutephrog.crawler.CrawlerActor
import ru.cutephrog.searchengine.SearchEngine

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 07.09.11
 * Time: 20:03
 * To change this template use File | Settings | File Templates.
 */

private[cutephrog]
final case class SetRole(role: Runner.Role.Value)
private[cutephrog]
final case class WorkerRequest(message: Any)


final class Runner(port: Int) extends Actor {
  private[this] var worker: Option[Actor] = None
  private[this] val properties = new Properties

  def act() {
    alive(port)
    register('CutePhrog, self)
    self.trapExit = true

    Console println "%s started. Listening on port %d".format(getClass.getSimpleName, port)
    initialize()

    var running = true
    var exitRequested = false
    while (running) {
      Console println "%s blocking for any message.".format(getClass.getSimpleName)

      receive {
        case SetRole(newRole) =>
          if (worker.isEmpty) {
            Console println "Role is set to %s".format(newRole)
            properties.setProperty(Runner.PROPERTY_ROLE, newRole.toString)
            worker = Some(workerByRole(newRole))
            worker foreach { w => link(w.start()) }
          } else {
            Console println "Worker is still running. Ignoring request."
          }

        case WorkerRequest(request) =>
          worker foreach { _ ! request }
          Console println "Forwarded: %s".format(request)

        case Exit(from, reason) =>
          Console println "%s terminated with reason: %s".format(from.getClass.getSimpleName, reason)
          worker = worker.filter(_ != from)
          running = !exitRequested

        case 'Exit =>
          exitRequested = true
          worker match {
            case Some(w) => w ! 'Exit
            case None => running = false
          }

        case unknown =>
          Console println "Unknown message: " + unknown
      }
    }

    deinitialize()
    Console println "%s stopped.".format(getClass.getSimpleName)
  }

  private[this] def workerByRole(role: Runner.Role.Value) = {
    role match {
      case Runner.Role.Crawler => new CrawlerActor
      case Runner.Role.SearchEngine => new SearchEngine
    }
  }

  private[this] def initialize() {
    val file = new File(Runner.PROPERTIES_FILE)
    if (file.exists) {
      properties.load(new FileInputStream(file))
    }
    Option(properties.getProperty(Runner.PROPERTY_ROLE)) foreach { role =>
      this ! SetRole(Runner.Role.withName(role))
    }
  }

  private[this] def deinitialize() {
    properties.store(new FileOutputStream(Runner.PROPERTIES_FILE), "")
  }
}

private[cutephrog]
object Runner {
  object Role extends Enumeration {
    val Crawler = Value("crawler")
    val SearchEngine = Value("searchengine")
  }

  private val PROPERTIES_FILE = "runner.properties"
  val PROPERTY_ROLE = "role"

  def main(args: Array[String]) {
    val runner = new Runner(8000)
    try {
      runner.start()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
  }
}