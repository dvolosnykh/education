#!/bin/bash

if [[ $1 == "--help" ]]; then
	tee <<HELP
Usage:	$(basename $0) vmBaseName
    vmBaseName  Base name of VMs. If your VMs are named like 'vmX': 'vm0', 'vm1,
    		and so on, --- then you have to pass 'vm' as first arg.
HELP
	exit 0
fi


declare -r user=$1 vmBaseName=$2

function checkArguments() {
	local -r wordMessage="can consist only of alpha-numeric characters and underscore."

	[[ $vmBaseName == +([[:word:]]) ]]
	local vmBaseNameAccepted=$?
	if [[ $vmBaseNameAccepted -eq 0 ]]; then
		[[ -d $vmBaseName ]]; vmBaseNameAccepted=$?
		[[ $vmBaseNameAccepted -eq 0 ]] || echo "Directory '$vmBaseName' does not exist." >&2
	else
		echo "'vmBaseName' $wordMessage" >&2
	fi

	[[ $vmBaseNameAccepted -eq 0 ]]
}

if [[ checkArguments -ne 0 ]]; then exit 1; fi


source common.sh


if queryVMsRange "destroy"; then
	declare destroyedCount=0
	declare i
	for ((i = VM_FIRST_INDEX; i < VM_FIRST_INDEX + VM_COUNT; ++i)); do
		declare vmName=${VM_NAMES[i]}
		if destroyVM $vmBaseName $vmName; then
			((++destroyedCount))
		fi
	done

	if [[ $destroyedCount -lt $VM_COUNT ]]; then
		echo "Some of the VMs have not been destroyed. Try again or do it manually."
		echo "Consider looking into '$logFile'."
	fi
fi
