#!/bin/bash


declare -r FAKE=false


if [[ $1 == "--help" ]]; then
	tee <<HELP
Usage:	$(basename $0) vmBaseName baseIndex [bridge]
    vmBaseName  Base name of VMs. If you specify 'vm' then your VMs
                will have names like 'vmX': 'vm0', 'vm1, and so on.
    baseIndex   Base index of VMs. Consider it as a first value of 'X'
                in vmBaseName.
    bridge      Your virtual network's bridge, default is 'virbr0'.
HELP
	exit 0
fi


declare -r user=$1 vmBaseName=$2 baseIndex=$3 bridge=${4:-"virbr0"}

# Check if all the arguments are of the correct format.
function checkArguments() {
	local -r wordMessage="can consist only of alpha-numeric characters and underscore."
	local -r numberMessage="has to be a number."

	[[ $vmBaseName == +([[:word:]]) ]]
	local vmBaseNameAccepted=$?
	if [[ $vmBaseNameAccepted -eq 0 ]]; then
		[[ -d $vmBaseName ]]; vmBaseNameAccepted=$?
		[[ $vmBaseNameAccepted -eq 0 ]] || echo "Directory '$vmBaseName' does not exist." >&2
	else
		echo "'vmBaseName' $wordMessage" >&2
	fi

	[[ $baseIndex == +([[:digit:]]) ]]
	local -r baseIndexAccepted=$?
	[[ $baseIndexAccepted -eq 0 ]] || echo "'baseIndex' $numberMessage" >&2

	[[ $bridge == +([[:word:]]) ]]
	local -r bridgeAccepted=$?
	[[ $bridgeAccepted -eq 0 ]] || echo "'bridge' $wordMessage" >&2

	[[ $vmBaseNameAccepted -eq 0 ]] && [[ $baseIndexAccepted -eq 0 ]] && [[ $bridgeAccepted -eq 0 ]]
}

if [[ checkArguments -ne 0 ]]; then exit 1; fi


source common.sh


# List of files where we need to change hostname for cloned VMs.
declare -ar REPLACE_HOSTNAME_FILES=(/etc/hostname /etc/hosts)
# List of files where we need to change IP for cloned VMs.
declare -ar REPLACE_IP_FILES=(/etc/network/interfaces)
# List of needed packages to execute this script.
declare -ar NEEDED_PACKAGES=(qemu-kvm virtinst libvirt-bin openssh-server) # kvm-pxe

# Configuration file.
declare -r configFile="$PWD/$vmBaseName/$vmBaseName.cfg"
# Partioning file.
declare -r partitionFile="$PWD/$vmBaseName/$vmBaseName.part"
# First boot script.
declare -r firstbootFile="$PWD/$vmBaseName/firstboot.sh"
# First login script.
declare -r firstloginFile="$PWD/$vmBaseName/firstlogin.sh"
# Gateway IP-address.
declare -r gatewayIp=$(getIp $bridge)
# VMs' name format.
declare -r vmNameFormat="$vmBaseName%d"
# VMs' IP's format.
declare -r vmIpFormat="${gatewayIp%.*}.%d"


# Flag if notifications are enabled.
declare notifyEnabled=false

# Check if notification is enabled. If not, ask the user if he wants it enabled.
function maybeEnableNotification() {
	local -r NOTIFY_LIB="libnotify-bin"

	dpkg-query --status $NOTIFY_LIB &>/dev/null
	notifyEnabled=$?
	if [[ $notifyEnabled -ne 0 ]]; then
		promptYesNo "Package $NOTIFY_LIB is not installed. You may want it to make available handy notification on copmletion." "no"
		case $CHOICE in
			"yes")	echo "Yeah! You will enjoy it ;)"
				apt-get install $NOTIFY_LIB
				notifyEnabled=$?
				;;
			"no")	echo "You are right, no time for cosmetics!"
				;;
		esac
	fi
}

# Ensure that all needed packages are installed.
function checkForNeededPackages() {
	local installed=true
	for package in ${NEEDED_PACKAGES[@]}; do
		dpkg-query --status $package &>/dev/null 
		installed=$?
		if [[ $installed -ne 0 ]]; then
			echo -n "Package $package is not installed. Installing... "
			apt-get install $package &>>"$logFile"
			installed=$?
			if [[ $installed -eq 0 ]]; then echo "[Done]"; else echo "[Failed] Do it yourself :)"; fi
		fi
		[[ $installed -eq 0 ]] || break # Break if some package is still not installed.
	done
	
	maybeEnableNotification

	[[ $installed -eq 0 ]]
}


# Parameters requested from user.
declare firstIndex=0 count=1 originalIndex=-1
# Related stuff.
declare originalExists=false

# Demand set of parameters from the user.
function demandParameters() {
	# Maybe offer the user to choose the original VM, if there's some to
	# choose from.
	local ok=true
	if [[ -d $QEMU_DIR ]]; then
		if [[ ${#VM_NAMES[@]} -gt 0 ]]; then
			PS3="Choose one to be the original VM for the new ones: "
			echo "There are already some VMs with the 'vmBaseName' you have specified:"
			select originalVmName in ${VM_NAMES[@]} "[none]"; do
				[[ -n "$originalVmName" ]] && break
			done
			if [[ $originalVmName != "[none]" ]]; then
				originalIndex=${originalVmName#$vmBaseName}

				! vmIsRunning $originalVmName
				ok=$?
				if [[ $ok -ne 0 ]]; then
					echo "The original VM is running. Stop it and try again." >&2
				fi
			fi
		fi
	fi
	
	# Query for parameters until success.
	[[ $ok -eq 0 ]] && while true; do
		read -e -i ${firstIndex:-""} -p "Enter VMs' numbers, first and count (default is 1): " firstIndex count
		count=${count:-1}

		[[ $firstIndex == +([[:digit:]]) ]];	local firstIndexIsNum=$?
		[[ $count == +([[:digit:]]) ]];		local countIsNum=$?

	if [[ $firstIndexIsNum -eq 0 ]] && [[ $countIsNum -eq 0 ]]; then break; fi

		if [[ -z $firstIndex ]]; then
			echo "You have to sepcify at least 'first'." >&2
		else
			echo "'first' and 'count' have to be non-negative numbers." >&2
			if [[ $firstIndexIsNum -ne 0 ]]; then firstIndex=""; fi
			if [[ $countIsIsNum -ne 0 ]]; then count=""; fi
		fi
	done
	
	[[ $ok -eq 0 ]]
}

# Create new VM.
function createVM() {
	local -r vmName=$1 vmIp=$2

	# If VM exists, destroy it.
	if vmExists $vmName; then
		echo "Warning: '$vmName' already exists. Will be replaced." >&2
		destroyVM $vmBaseName $vmName
	fi
	
	# Create emtpy firstboot and firstlogin scripts if they do not exist.
	sudo -u $user touch "$firstbootFile"
	sudo -u $user touch "$firstloginFile"
		
	# Create VM.
	echo -n "Creating '$vmName' [$vmIp]... "
	SECONDS=0
if $FAKE; then
	true
else
	vmbuilder kvm ubuntu \
		--overwrite --verbose \
		--config="$configFile" \
		--part="$partitionFile" \
		--destdir="$vmBaseName/$vmName" \
		--ip=$vmIp \
		--gw=$gatewayIp \
		--dns=$gatewayIp \
		--hostname=$vmName \
		--firstboot="$firstbootFile" \
		--firstlogin="$firstloginFile" \
		&>>"$logFile"
	vmExists $vmName
fi
	local ok=$?
	
	# Report result of creation.
	if [[ $ok -eq 0 ]]; then
		echo "[Done in $SECONDS sec]"
	else
		echo "[Failed after $SECONDS sec]"
	fi
	
	[[ $ok -eq 0 ]]
}

# Clone the original VM.
function cloneVM() {
	local -r originalName=$1 originalIp=$2 cloneName=$3 cloneIp=$4

	# If VM exists, destroy it.
	if vmExists $vmName; then
		echo "Warning: '$vmName' already exists. Will be replaced." >&2
		destroyVM $vmBaseName $vmName
	fi

	# Clonve original VM.
	echo -n "Cloning '$cloneName' from '$originalName'... "
	SECONDS=0
if $FAKE; then
	true
else
	local -r cloneDiskFilePath="$vmBaseName/$cloneName/$cloneName.qcow2"
	sudo -u $user mkdir -p "$vmBaseName/$cloneName" &&
	sudo -u $user touch "$cloneDiskFilePath" &&
	virt-clone \
		--force \
		--original-xml="$QEMU_DIR/$originalName.xml" \
		--name=$cloneName \
		--file="$cloneDiskFilePath" \
		&>>"$logFile"
	vmExists $cloneName
fi
	local -r ok=$?

	# Report result of cloning.
	if [[ $ok -eq 0 ]]; then
		echo "[Done in $SECONDS sec]"
	else
		echo "[Failed after $SECONDS sec]"
	fi

	[[ $ok -eq 0 ]]
}

# Clone original VM.
function cloneVMs() {
	local -ar args=($@)
	local -r originalName=$1 originalIp=$2
	local -ar cloneIndices=(${args[@]:2})
	
	local i
	[[ $ok -eq 0 ]] && for ((i = 0; i < ${#cloneIndices[@]}; ++i)); do
		local cloneIndex=${cloneIndices[i]}
		local cloneName=$(printf "$vmNameFormat" $cloneIndex) \
			cloneIp=$(printf "$vmIpFormat" $((baseIndex + cloneIndex)))
		cloneVM $originalName $originalIp $cloneName $cloneIp
		[[ $? -eq 0 ]] || break # Break on first failure.
	done
	[[ $i -eq ${#cloneIndices[@]} ]]; ok=$?
	
	[[ $ok -eq 0 ]]
}

# Tune cloned VMs.
function tuneClonedVMs() {
	local -ar args=($@)
	local -r originalName=$1 originalIp=$2
	local -ar cloneIndices=(${args[@]:2})

	local i
	for ((i = 0; i < ${#cloneIndices[@]}; ++i)); do
		local cloneIndex=${cloneIndices[i]}
		local cloneName=$(printf "$vmNameFormat" $cloneIndex) \
			cloneIp=$(printf "$vmIpFormat" $((baseIndex + cloneIndex)))

		# Prevent SSH from warning about identification change.
		ssh-keygen -R $originalIp &>/dev/null

		# Start VM. Note: it's definition is temporary until shutdown.
		startVM $cloneName
		local ok=$?

		# Wait while VM is ready for SSH-session.
		if [[ $ok -eq 0 ]] && waitWhenReadyForSSH $cloneName $originalIp; then
			echo "Initializing SSH-session with '$cloneName' (root password may need)"
			echo "to make it having proper hostname and IP-address [$cloneIp]."

			# Tune VM's IP-address and hostname.
			ssh root@$originalIp 'bash -s' &>>"$logFile" <<-INPUT
				sed -i 's/$originalIp/$cloneIp/g' ${REPLACE_IP_FILES[@]}
				sed -i 's/$originalName/$cloneName/g' ${REPLACE_HOSTNAME_FILES[@]}
			INPUT
			ok=$?
			
			echo -n "Tuning of '$cloneName' is "
			if [[ $ok -eq 0 ]]; then echo "[Done]"; else echo "[Failed]"; fi
		fi

		# Shutdown VM in any case.
		shutdownVM $cloneName &&
		waitWhenShutOff $cloneName
		ok=$?
		#ssh-keygen -R $cloneIp &>>"$logFile"
		[[ $ok -eq 0 ]] || break # Break on first failure.
	done

	ssh-keygen -R $originalIp &>>"$logFile"

	[[ $i -eq ${#cloneIndices[@]} ]]
}

# Report to user
function report() {
	local -r success=$1 title=$2 textSuccess=$3 textFailure=$4
	
	if [[ $success -eq 0 ]]; then
		if [[ $notifyEnabled -eq 0 ]]; then
			notify-send -u normal "$title" "$textSuccess"
		fi
		echo "$title: $textSuccess"
	else
		if [[ $notifyEnabled -eq 0 ]]; then
			notify-send -u critical "$title" "$textFailure"
		fi
		echo "$title: $textFailure" >&2
	fi
}


if checkForNeededPackages && demandParameters; then
	[[ $originalIndex -ge 0 ]];	originalExists=$?
	if [[ $originalExists -ne 0 ]]; then
		# Use first VM as original when it is created.
		originalIndex=$firstIndex
	fi
	
	# VMs' last index.
	declare -r lastIndex=$((firstIndex + count - 1))
	# List of clones indices.
	declare -a cloneIndices=($(seq $firstIndex $lastIndex))
	if ((firstIndex <= originalIndex && originalIndex <= lastIndex)); then
		# Fix 'cloneIndices' array.
		cloneIndices=(${cloneIndices[@]:0:originalIndex - firstIndex} \
			${cloneIndices[@]:originalIndex - firstIndex + 1})
	fi
	
	#
	# Main actions about installing virtual subnetwork.
	#
	
	declare success=true
	declare originalName=$(printf "$vmNameFormat" $originalIndex) \
		originalIp=$(printf "$vmIpFormat" $((baseIndex + originalIndex)))
	
	# Start measuring elapsed time.
	SECONDS=0
	declare elapsedTime

	# If the original VM does not exist, create it.
	if [[ originalExists -ne 0 ]]; then
		createVM $originalName $originalIp
		success=$?
		
		elapsedTime=$SECONDS
		
		report $success "Original VM" \
			"was created successfully (elapsed time: $elapsedTime secs.)." \
			"failed to install (elapsed time: $elapsedTime secs.)."
	fi

	if [[ $success -eq 0 ]] && [[ ${#cloneIndices[@]} -gt 0 ]]; then
		# Offer the user to do some work from shell before continue cloning
		# the original VM.
		promptYesNo "Do you want to run some shell commands before creating clones\n(for example, in order to tune the original VM)?" "no"
		if [[ $CHOICE == "yes" ]]; then
			sudo -Eu $user bash
		fi

		# Make clones.
		cloneVMs $originalName $originalIp "${cloneIndices[@]}"
		success=$?
		
		if [[ $success -eq 0 ]]; then
			read -p "Finished creating VMs. Press any key when you are ready to perform tuning step... " -n 1 -s
			echo
if $FAKE; then
			true
else
			tuneClonedVMs $originalName $originalIp "${cloneIndices[@]}"
fi
			success=$?
		fi
	fi
	
	elapsedTime=$SECONDS

	report $success "Virtual machines" \
		"have been installed successfully (elapsed time: $elapsedTime secs.)." \
		"failed to install (elapsed time: $elapsedTime secs.)."

	[[ $success -eq 0 ]]
fi
