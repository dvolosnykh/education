#!/bin/bash

if [[ $1 == "--help" ]]; then
	tee <<HELP
Usage:	$(basename $0) command 
    commands:
        create  - create virtual subnetwork.
        destroy - destroy virtual subnetwork.
        start   - start virtual subnetwork.
        stop    - stop virtual subnetwork.

    For more information on each command, type:
        $(basename $0) command --help
HELP
	exit 0
fi

# Subcommand. Defines wich nested script is to be executed.
declare -r command=$1
# Move out the $1 argument. Unneeded any more.
shift

# Original user that started execution of this script. Needed in nested scripts,
# when creating different folders & files, so that they belong to him (not root).
declare -r user=$(whoami)

	
declare ok=true
case $command in
	create|destroy|start|stop)
		sudo -E bash $command.sh $user $@
		ok=$?
		;;
	*)
		echo "Unknown command. Try executing '$(basename $0) --help'." >&2
		ok=false
		;;
esac

[[ $ok -eq 0 ]]
