# Directory where VMs reside.
declare -r QEMU_DIR=/etc/libvirt/qemu
# TCP/SSH port
declare -r SSH_PORT=22
# Limit on tries count for different purposes.
declare -r MAX_TRIES=15
# Logging file name.
declare -r logFile="$vmBaseName/$vmBaseName.log"


# Create new or erase contents of the existing log file.
sudo -u $user touch "$logFile"
echo -n>"$logFile"


# usage: readChoice [choice=default] [default=yes]
function readChoice() {
	local choice=${1:-${2:-"yes"}}	# Assume 'default' if 'choice' is empty.
	if [[ $choice != "yes" ]] && [[ $choice != "no" ]]; then
		choice=""
	fi
	echo $choice
}

# usage: promptYesNo text default={y,n}
function promptYesNo() {
	local -r default=$(readChoice $2 "yes")
	local -r yesno="yes/no"

	while true; do
		printf "$1 [${yesno^^$default}] "
		read CHOICE
		CHOICE=$(readChoice "$CHOICE" $default)

	if [[ $CHOICE == "yes" ]] || [[ $CHOICE == "no" ]]; then break; fi

		echo "You have to enter 'yes' or 'no' or empty string, which is considered as '$default'." >&2
	done
}

# Evaluates interface's IP by its name, e.g. eth0.
# usage: getIp [interface=eth0]
function getIp() {
	local ip=$(/sbin/ifconfig ${1:-"eth0"})
	ip=${ip#*"inet addr:"}
	ip=${ip%% *}
	echo $ip

	[[ -n $ip ]]
}

#usage: startVM vmName
function startVM() {
	echo -n "Starting VM '$1'... "
	
	virsh create "$QEMU_DIR/$1.xml" &>>"$logFile"
	local -r ok=$?
	
	if [[ $ok -eq 0 ]]; then echo "[Done]"; else echo "[Failed]"; fi
	[[ $ok -eq 0 ]]
}

#usage: shutdownVM vmName
function shutdownVM() {
	echo -n "Shutting down VM '$1'... "
	
	virsh shutdown $1 &>>"$logFile"
	local -r ok=$?
	
	if [[ $ok -eq 0 ]]; then echo "[Done]"; else echo "[Failed]"; fi
	[[ $ok -eq 0 ]]
}

# usage: destroyVM vmBaseName vmName
function destroyVM() {
	echo -n "Destroying VM '$2'... "
	
	if true; then
		virsh destroy $2
		virsh undefine $2
		rm -rf "$1/$2"
	fi &>>"$logFile"
	local -r ok=$?
	
	if [[ $ok -eq 0 ]]; then echo "[Done]"; else echo "[Failed]"; fi
	[[ $ok -eq 0 ]]
}

# usage: vmExists vmName
function vmExists() {
	[[ -s "$QEMU_DIR/$1.xml" ]]
}

# usage: existingVMs vmBaseName
function existingVMs() {
	find $QEMU_DIR -type f -regex .*$1[0-9]+.xml | \
		sed 's_.*/\(.*\)\.xml_\1_' | sort
}

# usage: vmState vmName
function vmState() {
	virsh domstate $1
}

# usage: vmIsRunning vmName
function vmIsRunning() {
	local -r state=$(vmState $1)
	[[ $state == "работает" ]] || [[ $state == "running" ]]
}

# usage: vmIsShutOff vmName
function vmIsShutOff() {
	local -r state=$(vmState $1)
	[[ $state == "выключен" ]] || [[ $state == "shut off" ]]
}

# usage: vmIsReady vmName vmIp
function vmIsReady() {
	nc -z $2 $SSH_PORT
}

# usage: waitWrapper function args
# NOTE: not part of API.
function waitWrapper() {
	local -r function=$1 args=(${@:2:$#-2}) waitMessage="${@: -1}"

	local ok
	while true; do
		echo -n $waitMessage
		
		# Try several times.
		local tries
		for ((tries = 0; tries < MAX_TRIES; ++tries)); do
			sleep 1
			$function ${args[@]} && break
		done	
		[[ $tries -lt $MAX_TRIES ]]
		ok=$?

		if [[ $ok -eq 0 ]]; then
			echo "[Done]"
			break
		else
			echo "[Failed]"
			
			promptYesNo "Try again?" "yes"
			if [[ $CHOICE == "no" ]]; then
				break
			fi
		fi
	done

	[[ $ok -eq 0 ]]
}

# usage: waitWhenShutOff vmName
function waitWhenShutOff() {
	waitWrapper vmIsShutOff ${@:1} "Waiting for VM '$1' is shut off... "
}

# usage: waitWhenReadyForSSH vmName vmIp
function waitWhenReadyForSSH() {
	waitWrapper vmIsReady ${@:1} "Waiting for VM '$1' is ready for SSH-session... "
}

# Existing VMs' names.
declare -ar VM_NAMES=($(existingVMs $vmBaseName))
# Parameters requested from user.
declare VM_FIRST_INDEX=0 VM_COUNT=1

# Query VMs range.
# usage: queryVMsRange operation
function queryVMsRange() {
	if [[ ${#VM_NAMES[@]} -gt 0 ]]; then
		# Print list of existing VMs.
		echo "There are following VMs exists, that have basename '$vmBaseName':"
		local index=0
		for vmName in ${VM_NAMES[@]}; do
			echo "$index) $vmName ($(vmState $vmName))"
			((++index))
		done
	
		promptYesNo "Would you like to $1 all the listed VMs?" "yes"
		if [[ $CHOICE == "yes" ]]; then
			VM_FIRST_INDEX=0
			VM_COUNT=${#VM_NAMES[@]}
		else
			# Query for parameters until success.
			while true; do
				read -e -p "Enter VMs' numbers, first and count (default is 1): " VM_FIRST_INDEX VM_COUNT
				VM_COUNT=${VM_COUNT:-1}

				[[ $VM_FIRST_INDEX == +([[:digit:]]) ]];	local firstIndexIsNum=$?
				[[ $VM_COUNT == +([[:digit:]]) ]];		local countIsNum=$?

			if [[ $firstIndexIsNum -eq 0 ]] && [[ $countIsNum -eq 0 ]]; then break; fi

				if [[ -z $firstIndex ]]; then
					echo "You have to sepcify at least 'first'." >&2
				else
					echo "'first' and 'count' have to be non-negative numbers." >&2
					if [[ $firstIndexIsNum -ne 0 ]]; then VM_FIRST_INDEX=""; fi
					if [[ $countIsIsNum -ne 0 ]]; then VM_COUNT=""; fi
				fi
			done
		fi
	else
		echo "There are no VMs with the basename '$vmBaseName'."
	fi
	
	[[ ${#VM_NAMES[@]} -gt 0 ]]
}
