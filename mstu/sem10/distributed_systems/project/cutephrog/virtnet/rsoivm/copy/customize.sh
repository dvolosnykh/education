#!/bin/bash

if [[ $(whoami) != "root" ]]; then
	echo "You don't have root privilegies. Run with sudo."
	exit 1
fi

declare -r MONGO_DB="mongodb-10gen_2.0.0_amd64.deb"

echo "deb http://archive.canonical.com/ubuntu natty partner" >>/etc/apt/sources.list && \
apt-get update
apt-get install sun-java6-jre
dpkg --install $MONGO_DB

echo 'JAVA_HOME="/usr/lib/jvm/java-6-sun/jre"' >>/etc/environment
