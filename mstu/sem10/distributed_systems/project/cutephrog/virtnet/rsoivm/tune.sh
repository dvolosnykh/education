#!/bin/bash

declare -r login=${1%@*} IP=${1#*@} vm=$2

if [[ $# -ne 2 ]] || [[ $login == $IP ]]; then
	echo "usage: $(basename $0) login@ip vm"
	exit 1
fi

# Start VM.
virsh start $vm
while ! netcat -vz $IP 22; do
	sleep 1
done

# Tune VM.
ssh-keygen -R $IP
scp -r $(dirname $0)/copy $login@$IP:~/
ssh $login@$IP

# Shutdown VM.
virsh shutdown $vm
while [[ $(virsh domstate $vm) != "выключен" ]] && [[ $(virsh domstate $vm) != "stopped" ]]; do
	sleep 1
done
