package testscala

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 04.07.11
 * Time: 20:06
 * To change this template use File | Settings | File Templates.
 */

object Spiral {
  object Direction extends Enumeration {
    val Left, Up, Right, Down = Value
  }

  private[this] lazy val space = Element(" ")
  private[this] lazy val corner = Element("+")

  def apply(nEdges: Int, direction: Direction.Value): Element = {
    var sp = corner

    def spiralRecursive(nEdges: Int, direction: Direction.Value) {
      if (nEdges > 0) {
        val verticalBar = { Element('|', 1, sp.height) }
        val horizontalBar = { Element('-', sp.width, 1) }

        sp = direction match {
          case Direction.Left => (verticalBar above corner) beside (space above sp)
          case Direction.Up => (corner beside horizontalBar) above (sp beside space)
          case Direction.Right => (sp above space) beside (corner above verticalBar)
          case Direction.Down => (space beside sp) above (horizontalBar beside corner)
        }
        spiralRecursive(nEdges - 1, Direction((direction.id + 1) % 4))
      }
    }

    spiralRecursive(nEdges, direction)
    sp
  }

  def main(args: Array[String]) {
    (new ElementSpec).execute()
    val nSides = 11//args(0).toInt
    println(Spiral(nSides, Spiral.Direction.Left))
  }
}
