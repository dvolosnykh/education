package testscala

import java.io.File
import testscala.Rational._

class Pet(val name: String) {
  override def toString = name
}

class Dog(override val name: String) extends Pet(name)

object Pets {
  def copy[S <: D, D <: Pet](from: Array[S], to: Array[D]) {
    (0 until from.length).foreach { i => to(i) = from(i) }
  }

  def print[T <: Pet](pets: Array[T]) {
    pets.foreach(pet => Console.print(pet + " "))
    Console.println
  }
}

object Main {
  def main(args: Array[String]) {
	if (true) {
	  var x = 10
	  val f = { () => x }
	  println(f())
	  x = 30
	  println(f())
	}
	
    if (false) {
      val indexLen = 3
      val folder = "/media/Elements/education/Development/Designing/_books/oop_osc2book"
      val files = new File(folder).listFiles.filter { _.getName().endsWith(".PDF") }
      files.foreach { file =>
        val name = file getName
        val dotPos = name indexOf "."
        val newName = name.substring(0, dotPos - indexLen).toLowerCase +
          name.substring(dotPos - indexLen, dotPos) + name.substring(dotPos).toLowerCase

        val newPath = file.getParent + File.separator + newName;
        val result = file.renameTo(new File(newPath))
        println(name + " -> " + newPath + " : " + result)
      }
    }

    if (false) {
      val dogs = Array(new Dog("A"), new Dog("B"))
      val pets = new Array[Pet](2)

      Pets.copy(dogs, pets)

      Pets.print(pets)
      Pets.print(dogs)
    }

    if (false) {
      val array = Array(2, 3, 5, 1, 6, 4)
      val repeat = 1000000

      {
        var total: Long = 0
        for (i <- 1 to repeat) {
          val start = System.currentTimeMillis
          val sum = array.foldLeft(0) { (carryOver, elem) => carryOver + elem }
          total += System.currentTimeMillis - start
        }
        println(total)
      }

      {
        var total: Long = 0
        for (i <- 1 to repeat) {
          val start = System.currentTimeMillis
          val sum = (0 /: array) { (carryOver, elem) => carryOver + elem }
          total += System.currentTimeMillis - start
        }
        println(total)
      }

      {
        var total: Long = 0
        for (i <- 1 to repeat) {
          val start = System.currentTimeMillis
          val sum = simpleSum(array);
          total += System.currentTimeMillis - start
        }
        println(total)
      }
    }

    if (false) {
      for (i <- 1 to 3; j <- i to 6) {
        print("[" + i + "," + j + "] ")
      }
    }

    if (false) {
      println(maxList(List(1, 5, 3, 4)))
    }
  }

  def maxList[T <% Ordered[T]](elements: List[T]): T = {
	if (elements.isEmpty)
      throw new IllegalArgumentException("empty list!")
	
    def maxList_recursive[T <% Ordered[T]](candidate: T, elements: List[T]): T =
      elements match {
        case List() => candidate
        case x :: rest => maxList_recursive(if (x > candidate) x else candidate, rest)
      }

    maxList_recursive(elements.head, elements.tail)
  }

  def simpleSum(arr: Array[Int]) = {
    var result = 0
    for (i <- 0 until arr.length) {
      result += arr(i)
    }
    result
  }
}