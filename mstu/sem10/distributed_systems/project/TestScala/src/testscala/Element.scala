package testscala


object Element {
  private class ArrayElement(val contents: Array[String]) extends Element

  private class LineElement(s: String) extends Element {
    val contents = Array(s)
    override def width = s.length
    override def height = 1
  }

  private class UniformElement(
    ch: Char,
    override val width: Int,
    override val height: Int
  ) extends Element {
    require(width >= 0 && height >= 0, "Dimensions must be non-negative")
    def contents = {
      val line = ch.toString * width
      Array.fill(height){line}
    }
  }

  def apply(contents: Array[String]): Element = new ArrayElement(contents)
  def apply(char: Char, width: Int, height: Int): Element = new UniformElement(char, width, height)
  def apply(line: String): Element = new LineElement(line)
}

abstract class Element {
  def contents: Array[String]
  def width: Int = contents(0).length
  def height: Int = contents.length

  def above(that: Element): Element = {
    val upper = this widen that.width
    val lower = that widen this.width
    Element(upper.contents ++ lower.contents)
  }

  def beside(that: Element): Element = {
    val left = this heighten that.height
    val right = that heighten this.height
    Element(
      for ((leftLine, rightLine) <- left.contents zip right.contents)
        yield leftLine + rightLine
    )
  }

  private def widen(wantedWidth: Int): Element = {
    if (wantedWidth > width) {
      val left = Element(' ', (wantedWidth - width) / 2, height)
      val right = Element(' ', wantedWidth - width - left.width, height)
      left beside this beside right
    } else {
      this
    } ensuring(wantedWidth <= _.width)
  }

  private def heighten(wantedHeight: Int): Element = {
    if (wantedHeight > height) {
      val top = Element(' ', width, (wantedHeight - height) / 2)
      val bottom = Element(' ', width, wantedHeight - height - top.height)
      top above this above bottom
    } else {
      this
    } ensuring(wantedHeight <= _.height)
  }

  override def toString = contents mkString "\n"
}

import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers

class ElementSpec extends FlatSpec with ShouldMatchers {
  behavior of "A UniformElement"
  it should "have a width equal to the passed value" in {
    val element = Element('x', 2, 3)
    element.width should be (2)
  }
  it should "have a height equal to the passed value" in {
    val element = Element('x', 2, 3)
    element.height should be (3)
  }
  it should "throw an IAE if passed a negative dimensions" in {
    evaluating {
      Element('x', -2, 3)
    } should produce [IllegalArgumentException]
  }
}
