package testscala

import concurrent.pilib.Sum

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 04.07.11
 * Time: 20:08
 * To change this template use File | Settings | File Templates.
 */

sealed abstract class Expr
case class Var(name: String) extends Expr
case class Number(num: Double) extends Expr
case class UnOp(operator: String, arg: Expr) extends Expr
case class BinOp(operator: String, left: Expr, right: Expr) extends Expr

class ExpressionFormatter {
  def format(e: Expr): Element = format(e, 0)

  // Contains operators in groups of increasing precedence
  private val opGroups = Array(
    Set("|", "||"),
    Set("&", "&&"),
    Set("ˆ"),
    Set("==", "!="),
    Set("<", "<=", ">", ">="),
    Set("+", "-"),
    Set("*", "%")
  )
  // A mapping from operators to their precedence
  private val precedence = {
    val assocs =
      for { i <- 0 until opGroups.length
        op <- opGroups(i)
      } yield op -> i
    assocs.toMap
  }

  private val unaryPrecedence = opGroups.length
  private val fractionPrecedence = -1

  private def format(e: Expr, enclPrec: Int): Element =
    e match {
      case Var(name) =>
        Element(name)
      case Number(num) =>
        def stripDot(s: String) =
          if (s endsWith ".0") s.substring(0, s.length - 2) else s
        Element(stripDot(num.toString))
      case UnOp(op, arg) =>
        Element(op) beside format(arg, unaryPrecedence)
      case BinOp("/", left, right) =>
        val top = format(left, fractionPrecedence)
        val bottom = format(right, fractionPrecedence)
        val line = Element('-', top.width max bottom.width, 1)
        val fraction = top above line above bottom
        if (enclPrec != fractionPrecedence) {
          fraction
        } else {
          Element(" ") beside fraction beside Element(" ")
        }
      case BinOp(op, left, right) =>
        val opPrec = precedence(op)
        val l = format(left, opPrec)
        val r = format(right, opPrec + 1)
        val oper = l beside Element(" "+ op +" ") beside r
        if (enclPrec <= opPrec) {
          oper
        } else {
          Element("(") beside oper beside Element(")")
        }
    }
}

object Express extends App {
  val f = new ExpressionFormatter
  val e1 = BinOp("*", BinOp("/", Number(1), Number(2)), BinOp("+", Var("x"), Number(1)))
  val e2 = BinOp("+", BinOp("/", Var("x"), Number(2)), BinOp("/", Number(1.5), Var("x")))
  val e3 = BinOp("/", e1, e2)

  def show(e: Expr) = println(f.format(e)+ "\n\n")

  for (e <- Array(e1, e2, e3)) {
    show(e)
  }

  println()
}
