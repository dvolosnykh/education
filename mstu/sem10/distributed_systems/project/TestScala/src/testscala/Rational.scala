package testscala

class Rational private (n: Int, d: Int) extends Ordered[Rational] {
  def numerator = n
  def denominator = d
  
  def +(that: Rational) = Rational(n * that.denominator + d * that.numerator,
    d * that.denominator)
  def -(that: Rational) = this + (-that)
  def *(that: Rational) = Rational(n * that.numerator, d * that.denominator)
  def /(that: Rational) = Rational(n * that.denominator, d * that.numerator)
  def unary_- = Rational(-n, d)
  def unary_+ = this

  override def compare(that: Rational) = (n * that.denominator - d * that.numerator)
  
  override def equals(that: Any) = that match {
    case that: Rational if (this canEqual that) =>
      (n == that.numerator && d == that.denominator)
    case i: Int => this == Rational(i)
    case _ => false
  }
  
  private def canEqual(that: Any) = that.isInstanceOf[Rational]
  
  override def hashCode = 41 * (41 + n) + d
  
  def max(that: Rational) = if (this > that) this else that
  def min(that: Rational) = if (this < that) this else that

  override def toString = if (d != 1) "[" + n + "/" + d + "]" else n.toString
}

object Rational {
  def apply(n: Int, d: Int): Rational = {
    require(d != 0)
    val (norm_n, norm_d) = normalize(n, d)
    new Rational(norm_n, norm_d)
  }

  def apply(n: Int) = new Rational(n, 1)

  implicit def intToRational(x: Int) = Rational(x)

  private def normalize(n: Int, d: Int) = {
    val g = Util.gcd(n.abs, d.abs)
    ((if (d < 0) -n else n) / g, d.abs / g)
  }
  
  def main(args: Array[String]) {
    val x, y, z = Rational(1, 2)
    println("reflexive: " + (x == x))
    println("symmetric: " + (x == y && y == x))
    println("transient: " + (x == y && y == z && x == z))
    println("not equal to null: " + (x != null))
    println(4 == Rational(8,2))
    println(Rational(8,2) == 4)
    println(2 + (2 * x * 2 min x * y))
  }
}

object Util {
  def gcd(a: Int, b: Int): Int = { if (b == 0) a.abs else gcd(b, a % b) }
}
