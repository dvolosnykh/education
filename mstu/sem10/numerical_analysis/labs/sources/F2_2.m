function [ y, callsCount ] = F2_2( x1, x2, callsCount )
    fenceHeight = 1000;
    x1x2 = x1 .* x2;
    
    y = zeros( size( x1x2 ) );
    lg = log( 10 );
    for i = 1 : size( y, 1 )
        for j = 1 : size( y, 2 )
            if ( x1x2( i, j ) > 0 )
                y( i, j ) = ( lg * log( x1x2( i, j ) ) ) ^ 2 - x1x2( i, j ) + x1( i, j ) ^ 2 + x2( i, j ) ^ 4;
                callsCount = callsCount + 1;
            else
                y( i, j ) = fenceHeight;
            end
        end
    end
end