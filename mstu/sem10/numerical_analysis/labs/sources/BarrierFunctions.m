function [ xMin, yMin, callsCount, xEval ] = BarrierFunctions( f, g, xMin, eps, visualize, viewport, viewStep )
    callsCount = 0;
    xEval = xMin;
    eps2 = eps ^ 2;
    
    k = 1;
    while ( true )
        fun = @( x1, x2, c )( HelpingFunction( f, g, x1, x2, k, c ) );
        xPrev = xMin;
        [ xMin, yMin, internalCallsCount, ~ ] = Newton2( fun, xPrev, eps, true, false, viewport, viewStep );
        %[ xMin, yMin, internalCallsCount, ~ ] = ConjugateDirections( fun, xPrev, eps, 2, visualize, viewport, viewStep );
        callsCount = callsCount + internalCallsCount;
        xEval( end + 1, : ) = xMin;
        
        if ( visualize )
            PlotSteps( xEval, fun, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Newton method' );
            pause;
        end
        
        %dx = xMin - xPrev;
        dx = xEval( end, : ) - xEval( floor( size( xEval, 1 ) / 2 ), : );
    if ( dx * dx' < eps2 ), break; end
    
        k = k + 1;
    end
    k
end

function [ result, callsCount ] = HelpingFunction( f, g, x1, x2, k, callsCount )
    [ y, callsCount ] = f( x1, x2, callsCount );
    result = y + Barrier( g, x1, x2, k );
end

function result = Barrier( g, x1, x2, k )
    limit = 1000;
    result = zeros( size( x1 ) );
    for i = 1 : length( g )
        gValue = g{ i }( x1, x2 );
        term = Inf * ones( size( x1 ) );
        mask = ( gValue < 0 );
        term( mask ) = 1 ./ ( -gValue( mask ) * k );
        term( term > limit ) = limit;
        result = result + term;
    end
end