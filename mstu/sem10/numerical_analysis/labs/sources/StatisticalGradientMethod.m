function [ xMin, yMin, callsCount, xEval ] = StatisticalGradientMethod( f, xMin, eps, step, probesCount, visualize, viewport, viewStep )
    eps2 = eps ^ 2;
    xEval = xMin;
    [ yMin, callsCount ] = feval( f, xMin( 1 ), xMin( 2 ), 0 );
    while ( true )
        quit = false;
        t = 0;
        while ( ~quit )
            % evaluate statistical antigradient.
            direction = zeros( size( xMin ) );
            for i = 1 : probesCount
                varxi = randn( size( xMin ) );
                varxi = step * varxi / norm( varxi );
                xProbe = xMin + varxi;
                [ yProbe, callsCount ] = feval( f, xProbe( 1 ), xProbe( 2 ), callsCount );
                direction = direction + varxi * ( yMin - yProbe );
            end
            
            quit = ( direction * direction' < eps2 );
            if ( ~quit )
                % minimize along current direction.
                fun = @( t, c ) ( feval( f, xMin( 1 ) + t * direction( 1 ), xMin( 2 ) + t * direction( 2 ), c ) );
                [ t, yMin, ~, minimizationCallsCount, ~ ] = Newton( fun, eps, 0 );
                callsCount = callsCount + minimizationCallsCount;
                
                quit = ( t > 0 || step < eps );
                
                step = step / 2;
            end
        end
        
    if ( t <= 0 ), break; end
    
        xMin = xMin + t * direction;
        xEval( end + 1, : ) = xMin;
        
        % plot current direction.
        if ( visualize )
            PlotCurve( fun, 0, 2 * t, t, 'Newton method' );
            pause;
            PlotSteps( xEval, f, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Statistical gradient method' );
            pause;
        end
    end
end