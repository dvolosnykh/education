function [ grad, f00, callsCount ] = Gradient( f, x, h, f00, callsCount )
    x = repmat( x, 3, 1 );
    x( 1 : 2, : ) = x( 1 : 2, : ) + [ h, 0 ; 0, h ];
    
    if ( isempty( f00 ) )
        [ f00, callsCount ] = feval( f, x( 3, 1 ), x( 3, 2 ), callsCount );
    end
    y( 3 ) = f00;

    [ y( 1 : 2 ), callsCount ] = feval( f, x( 1 : 2, 1 ), x( 1 : 2, 2 ), callsCount );
    grad = [ ( y( 1 ) - y( 3 ) ) / h; ( y( 2 ) - y( 3 ) ) / h ];
end