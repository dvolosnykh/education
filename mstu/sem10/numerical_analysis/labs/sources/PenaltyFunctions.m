function [ xMin, yMin, callsCount, xEval ] = PenaltyFunctions( f, g, xMin, p, eps, visualize, viewport, viewStep )
    xEval = xMin;
    eps2 = eps ^ 2;
    callsCount = 0;
    
    k = 0;
    while ( true )
        fun = @( x1, x2, c )( HelpingFunction( f, g, x1, x2, k, p, c ) );
        xPrev = xMin;
        [ xMin, yMin, internalCallsCount, ~ ] = Newton2( fun, xPrev, eps, true, false, viewport, viewStep );
        callsCount = callsCount + internalCallsCount;
        xEval( end + 1, : ) = xMin;
        
        if ( visualize )
            PlotSteps( xEval, fun, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Newton method' );
            pause;
        end
        
        %dx = xMin - xPrev;
        dx = xEval( end, : ) - xEval( floor( size( xEval, 1 ) / 2 ), : );
    if ( dx * dx' < eps2 ), break; end

        k = k + 1;
    end
    k
end

function [ result, callsCount ] = HelpingFunction( f, g, x1, x2, k, p, callsCount )
    [ y, callsCount ] = f( x1, x2, callsCount );
    [ penalty, ~ ] = Penalty( g, x1, x2, p, 0 );
    result = y + ( k ^ p ) * penalty;
end

function [ result, callsCount ] = Penalty( g, x1, x2, p, callsCount )
    z = zeros( size( x1 ) );
    result = z;
    for i = 1 : length( g )
        term = g{ i }( x1, x2 );
        term( term < 0 ) = 0;
        term = term .^ p;
        result = result + term;
    end
end