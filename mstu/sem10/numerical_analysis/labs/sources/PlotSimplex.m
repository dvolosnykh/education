function PlotSimplex( iterations, f, x1Range, x2Range )
    [ X1, X2 ] = meshgrid( x1Range, x2Range );
    [ Y, ~ ] = feval( f, X1, X2, 0 );
    contour( X1, X2, Y, 50 );
    hold on
    for i = 1 : length( iterations )
        if ( i < length( iterations ) )
            color = 'g';
        else
            color = 'r';
        end
        x = [ iterations( i ).x( end, : ); iterations( i ).x ];
        plot( x( :, 1 ), x( :, 2 ), color );
    end
    hold off
end