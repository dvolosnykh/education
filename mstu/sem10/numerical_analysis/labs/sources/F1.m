function [ y, callsCount ] = F1( x, callsCount )
    y = ( 4 * x .^ 3 + 2 * x .^ 2 - 4 * x + 2 ) .^ sqrt( 2 ) + ...
        asin( 1 ./ ( -x .^ 2 + x + 5 ) ) - 5;
    callsCount = callsCount + numel( x );
end
