function lab12
    f = ...
    { ...
        @( x )( 2 * x + 1 ), ...
        @( x )( 0 ), ...
        @( x )( x .^ 2 - 3 ) ...
    };
    below = [ true, true, false ];
    g = BuildConstraints( f, below );
    
    x0 = [ 0, -sqrt( 5 ) ];
    eps = 0.001;
    visualize = false;
    viewport = [ -4, 1; -3, 6 ];
    viewStep = 0.01;
    
    [ xMin, yMin, callsCount, xEval ] = BarrierFunctions( @F2_1, g, x0, eps, visualize, viewport, viewStep );
    xMin
    yMin
    callsCount
    
    PlotSteps( xEval, @F2_1, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Penalty function method', f );
    
    % Optimization Toolbox
    options = optimset( 'Display', 'iter' );
    options = optimset( options, 'TolX', eps );
    options = optimset( options, 'Algorithm', 'interior-point' );
    fun = @( x ) feval( @F2_2, x( 1 ), x( 2 ), 0 );
    A = [ -2, 1; 0, 1 ];
    b = [ 1; 0 ];
    nonlcon = @( x )( NonlinearConstraints( x, g{ 3 } ) );
    [ x, fval, exitflag, output ] = fmincon( fun, x0, A, b, [], [], [ -Inf, -Inf ], [ Inf, Inf ], nonlcon, options );
    x
    hold on
    plot( x( 1 ), x( 2 ), 'ro' );
    hold off
end

function [ c, ceq ] = NonlinearConstraints( x, f )
    ceq = [];
    c = f( x( 1 ), x( 2 ) );
end