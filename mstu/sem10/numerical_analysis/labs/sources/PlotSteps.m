function PlotSteps( x, f, x1Range, x2Range, methodName, g )
    title( methodName );
    xlabel( 'x1' );
    ylabel( 'x2' );
    
    [ X1, X2 ] = meshgrid( x1Range, x2Range );
    [ Y, ~ ] = feval( f, X1, X2, 0 );
    contour( X1, X2, Y, 50 );
    hold on
    if ( exist( 'g', 'var' ) )
        for i = 1 : length( g )
            x2 = feval( g{ i }, x1Range );
            plot( x1Range, x2, 'g' );
        end
    end
    plot( x( :, 1 ), x( :, 2 ), 'r' );
    hold off
end