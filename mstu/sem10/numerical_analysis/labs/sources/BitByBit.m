function [ xMin, yMin, iterationNum, callsCount, xEval ] = BitByBit( f, a, b, eps, k )
    iterationsMax = 100;
    
    iterationNum = 0;
    xEval = [];
    
    delta = ( b - a ) / k;
    
    x0 = a;
    [ f0, callsCount ] = feval( f, x0, 0 );
    xEval = [ xEval, x0 ];
    while ( true )
        iterationNum = iterationNum + 1;
        
        stop = false;
        while ( ~stop )
            x1 = x0 + delta;
            if ( x1 < a )
                x1 = a;
                stop = true;
            elseif ( b < x1 )
                x1 = b;
                stop = true;
            end
            
            [ f1, callsCount ] = feval( f, x1, callsCount );
            xEval = [ xEval, x1 ];
            
        	stop = ( stop || f0 <= f1 );
            if ( ~stop )
                x0 = x1;
                f0 = f1;
            end
        end
        
    if ( abs( delta ) < eps || iterationNum > iterationsMax ), break; end
        
        x0 = x1;
        f0 = f1;
        delta = -delta / k;
    end
    
    if ( iterationNum > iterationsMax )
        disp( 'Iterations limit reached!' );
    end
    
    xMin = ( x0 + x1 ) / 2;
    [ yMin, callsCount ] = feval( f, xMin, callsCount );
end