function [ xMin, yMin, iterationNum, callsCount, xEval ] = GoldenRatio( f, a, b, eps )
    iterationsMax = 100;
    
    iterationNum = 0;
    
    tau = ( realsqrt( 5 ) - 1 ) / 2;
    
    xLeft = a;
    xRight = b;
    xGold( 1 ) = xRight - tau * ( xRight - xLeft );
    xGold( 2 ) = xLeft + xRight - xGold( 1 );
    
    [ yGold, callsCount ] = feval( f, xGold, 0 );
    xEval = xGold;
    while ( true )
        iterationNum = iterationNum + 1;
        
        if ( yGold( 1 ) < yGold( 2 ) )
            xRight = xGold( 2 );
        else
            xLeft = xGold( 1 );
        end
        
    if ( xRight - xLeft < eps || iterationNum > iterationsMax ), break; end
        
        if ( yGold( 1 ) < yGold( 2 ) )
            xGold( 2 ) = xGold( 1 );
            yGold( 2 ) = yGold( 1 );
            
            xGold( 1 ) = xLeft + xRight - xGold( 2 );
            [ yGold( 1 ), callsCount ] = feval( f, xGold( 1 ), callsCount );
            xEval = [ xEval, xGold( 1 ) ];
        else
            xGold( 1 ) = xGold( 2 );
            yGold( 1 ) = yGold( 2 );
            
            xGold( 2 ) = xLeft + xRight - xGold( 1 );
            [ yGold( 2 ), callsCount ] = feval( f, xGold( 2 ), callsCount );
            xEval = [ xEval, xGold( 2 ) ];
        end
    end
    
    if ( iterationNum > iterationsMax )
        disp( 'Iterations limit reached!' );
    end
    
    xMin = ( xLeft + xRight ) / 2;
    [ yMin, callsCount ] = feval( f, xMin, callsCount );
end