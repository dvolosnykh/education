function [ xMin, yMin, callsCount, iterations ] = IrregularSimplexMethod( f, x0, sideLength, reduceCoeff, eps, alpha, beta, gamma, rebuildRate, visualize, viewport, viewStep )
    x = BuildSimplex( x0, sideLength );
    [ y, callsCount ] = feval( f, x( :, 1 ), x( :, 2 ), 0 );

    simplexAge = 0;
    iterations( 1 ).x = x;
    while ( true )
        % plot current simplex.
        if ( visualize )
            PlotSimplex( iterations, f, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ) );
            pause;
        end

    if ( MaxSideLength( x ) < eps ), break; end
    
        [ ~, minIndex ] = min( y );
        
        % rebuild simplex.
        if ( simplexAge > rebuildRate )
            [ ~, subminIndex ] = min( y( 1 : end ~= minIndex ) );
            if ( subminIndex >= minIndex ), subminIndex = subminIndex + 1; end
            
            sideLength = norm( x( minIndex, : ) - x( subminIndex, : ) );
            x = BuildSimplex( x( minIndex, : ), sideLength );
            [ y, callsCount ] = feval( f, x( :, 1 ), x( :, 2 ), callsCount );
            simplexAge = 0;
        end
        
        % work on simplex.
        %     step 1. reflecting.
        [ ~, maxIndex ] = max( y );
        [ ~, submaxIndex ] = max( y( 1 : end ~= maxIndex ) );
        if ( submaxIndex >= maxIndex ), submaxIndex = submaxIndex + 1; end
        
        xCenter = sum( x( 1 : end ~= maxIndex, : ) ) / ( size( x, 1 ) - 1 );
        xNew = xCenter + alpha * ( xCenter - x( maxIndex, : ) );
        [ yNew, callsCount ] = feval( f, xNew( 1 ), xNew( 2 ), callsCount );

        if ( yNew <= y( submaxIndex ) )
            y( maxIndex ) = yNew;
            x( maxIndex, : ) = xNew;
            
            if ( yNew < y( minIndex ) )
                %     step 2. stretching.
                xNew = xCenter + beta * ( xNew - xCenter );
                [ yNew, callsCount ] = feval( f, xNew( 1 ), xNew( 2 ), callsCount );

                if ( yNew < y( minIndex ) )
                    y( maxIndex ) = yNew;
                    x( maxIndex, : ) = xNew;
                end
            end
        else
            %     step 3. compressing.
            if ( yNew < y( maxIndex ) )
                xNew = xCenter + gamma * ( xNew - xCenter );
            else
                xNew = xCenter + gamma * ( x( maxIndex, : ) - xCenter );
            end
            [ yNew, callsCount ] = feval( f, xNew( 1 ), xNew( 2 ), callsCount );
            
            if ( yNew < y( maxIndex ) )
                y( maxIndex ) = yNew;
                x( maxIndex, : ) = xNew;
            else
                %     step 4. reducing.
                for i = [ 1 : minIndex - 1, minIndex + 1 : size( x, 1 ) ]
                    x( i, : ) = x( minIndex, : ) + reduceCoeff * ( x( i, : ) - x( minIndex, : ) );
                    [ y( i ), callsCount ] = feval( f, x( i, 1 ), x( i, 2 ), callsCount );
                end
            end
        end

        % save new simplex.
        iterations( end + 1 ).x = x;
        simplexAge = simplexAge + 1;
    end
    
    xMin = sum( x ) / length( x );
    [ yMin, callsCount ] = feval( f, xMin( 1 ), xMin( 2 ), callsCount );
end

function maxLength = MaxSideLength( x )
    n = size( x, 1 );
    L = zeros( n );
    for i = 1 : n
        for j = 1 : n
            if ( i ~= j ), L( i, j ) = norm( x( i, : ) - x( j, : ) ); end
        end
    end
    
    maxLength = max( max( L ) );
end