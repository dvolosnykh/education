function [ grad, H, f00, callsCount ] = Gradient_Hessian( f, x, h, f00, callsCount )
    n = length( x );
    y = sparse( [], [], [], 3, 3, n * ( n + 1 ) / 2 );
    
    if ( isempty( f00 ) )
        [ f00, callsCount ] = feval( f, x( 1 ), x( 2 ), callsCount );
    end
    y( 1, 1 ) = f00;
    for i = 1 : 3
        for j = max( 3 - i, 1 ) : 4 - i
            [ y( i, j ), callsCount ] = feval( f, x( 1 ) + ( i - 1 ) * h, x( 2 ) + ( j - 1 ) * h, callsCount );
        end
    end

    H = zeros( n );
    for i1 = 1 : n
        for i2 = 1 : n
            e1 = [ i1 == 1, i1 == 2 ];
            e2 = [ i2 == 1, i2 == 2 ];
            H( i1, i2 ) = y( 1 + e1( 1 ) + e2( 1 ), 1 + e1( 2 ) + e2( 2 ) ) - y( 1 + e1( 1 ), 1 + e1( 2 ) ) - y( 1 + e2( 1 ), 1 + e2( 2 ) ) + y( 1, 1 );
        end
    end
    H = H / h ^ 2;
    
    grad = zeros( length( x ), 1 );
    for i = 1 : n
        d1 = ( i == 1 );
        d2 = ( i == 2 );
        grad( i ) = y( 1 + d1, 1 + d2 ) - y( 1, 1 );
    end
    grad = grad / h;
end