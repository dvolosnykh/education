function lab5
    x0 = [ 0, -sqrt( 5 ) ];
    sideLength = 1;
    reduceCoeff =  0.5;
    eps = 0.001;
    visualize = false;
    viewport = [ -4, 1; -3, 6 ];
    viewStep = 0.1;
    
    [ xMin, yMin, callsCount, iterations ] = RegularSimplexMethod( @F2_1, x0, sideLength, reduceCoeff, eps, visualize, viewport, viewStep );
    xMin
    yMin
    callsCount
    
    PlotSimplex( iterations, @F2_1, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ) );
    pause;

    x0 = [ 3, 3 ];
    sideLength = 1;
    reduceCoeff =  0.5;
    eps = 0.001;
    visualize = false;
    viewport = [ 0.01, 4; 0.01, 4 ];
    viewStep = 0.1;
    
    [ xMin, yMin, callsCount, iterations ] = RegularSimplexMethod( @F2_2, x0, sideLength, reduceCoeff, eps, visualize, viewport, viewStep );
    xMin
    yMin
    callsCount
    
    PlotSimplex( iterations, @F2_2, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ) );
end