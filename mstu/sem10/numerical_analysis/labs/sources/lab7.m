function lab7
    %%{
    x0 = [ 0, -sqrt( 5 ) ];
    eps = 0.001;
    step = 1;
    probesCount = 3;
    visualize = false;
    viewport = [ -4, 1; -3, 6 ];
    viewStep = 0.1;
    
    [ xMin, yMin, callsCount, iterations ] = StatisticalGradientMethod( @F2_1, x0, eps, step, probesCount, visualize, viewport, viewStep );
    xMin
    yMin
    callsCount
    
    PlotSteps( iterations, @F2_1, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Statistical gradient method' );
    
    % Optimization Toolbox
    options = optimset( 'LargeScale', 'off' );
    options = optimset( options, 'Display', 'iter' );
    options = optimset( options, 'TolX', eps );
    fun = @( x ) feval( @F2_1, x( 1 ), x( 2 ), 0 );
    [ x, fval, exitflag, output ] = fminunc( fun, x0, options );
    x
    
    pause;
    %%}
    
    x0 = [ 3, 3 ];
    eps = 0.001;
    step = 1;
    probesCount = 3;
    visualize = false;
    viewport = [ 0.01, 4; 0.01, 4 ];
    viewStep = 0.1;
    
    [ xMin, yMin, callsCount, iterations ] = StatisticalGradientMethod( @F2_2, x0, eps, step, probesCount, visualize, viewport, viewStep );
    xMin
    yMin
    callsCount
    
    PlotSteps( iterations, @F2_2, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Statistical gradient method' );
    
    % Optimization Toolbox
    options = optimset( 'LargeScale', 'off' );
    options = optimset( options, 'Display', 'iter' );
    options = optimset( options, 'TolX', eps );
    fun = @( x ) feval( @F2_2, x( 1 ), x( 2 ), 0 );
    [ x, fval, exitflag, output ] = fminunc( fun, x0, options );
    x
end