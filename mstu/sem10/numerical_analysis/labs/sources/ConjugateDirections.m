function [ xMin, yMin, callsCount, xEval ] = ConjugateDirections( f, xMin, eps, restartRate, visualize, viewport, viewStep )
    callsCount = 0;
    xEval = xMin;
    eps2 = eps ^ 2;
    h = eps2;
    yMin = [];
    iterationNum = 1;
    while ( true )
        [ grad, yMin, callsCount ] = Gradient( f, xMin, h, yMin, callsCount );
        w = -grad;
        w2 = w' * w;

    if ( w2 < eps2 ), break; end
        
        % determine descent direction.
        if ( mod( iterationNum, restartRate ) == 1 )
            direction = w';
        else
            gamma = w2 / wPrev2;
            direction = gamma * direction + w';
        end
        
        % minimize along current direction.
        fun = @( t, c )( feval( f, xMin( 1 ) + t * direction( 1 ), xMin( 2 ) + t * direction( 2 ), c ) );
        [ t, yMin, ~, minimizationCallsCount, tEval ] = Newton( fun, eps * 0.01, 0 );
        callsCount = callsCount + minimizationCallsCount;
        xMin = xMin + t * direction;
        xEval( end + 1, : ) = xMin;
        
        wPrev2 = w2;
        iterationNum = iterationNum + 1;
        
        % plot current direction.
        if ( visualize )
            PlotCurve( fun, min( tEval ), max( tEval ), t, 'Newton method' );
            pause;
            PlotSteps( xEval, f, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Conjugate directions method' );
            pause;
        end
    end
end