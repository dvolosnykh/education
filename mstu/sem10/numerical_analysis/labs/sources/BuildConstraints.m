function g = BuildConstraints( f, below )
    g = cell( 1, length( f ) );
    for i = 1 : length( f )
        if ( below( i ) )
            g{ i } = @( x1, x2 )( x2 - f{ i }( x1 ) );
        else
            g{ i } = @( x1, x2 )( f{ i }( x1 ) - x2 );
        end
    end
end