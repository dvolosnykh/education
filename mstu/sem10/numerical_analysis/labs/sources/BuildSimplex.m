function x = BuildSimplex( x0, a )
    dimension = length( x0 );
    x = zeros( dimension + 1, dimension );
    for i = 1 : dimension + 1
        for j = 1 : dimension
            if ( j < i - 1 )
                x( i, j ) = x0( j );
            elseif ( j == i - 1 )
                x( i, j ) = x0( j ) + a * sqrt( j / ( 2 * ( j + 1 ) ) );
            else
                x( i, j ) = x0( j ) - a / sqrt( 2 * j * ( j + 1 ) );
            end
        end
    end
end