function lab2
    a = 0;
    b = 1;
    eps = [ 0.01, 0.0001, 0.000001 ];
    
    for i = 1 : length( eps )
        [ xMin, yMin, ~, callsCount, xEval ] = GoldenRatio( @F1, a, b, eps( i ) );
        
        if ( i == 1 )
            PlotCurve( @F1, a, b, xEval, 'Golden ratio' );
        else
            PlotCurve( @F1, a, b, xMin, 'Golden ratio' );
        end
        
        fprintf( '%i %.6f %i %.7f %.9f\n', [ i, eps( i ), callsCount, xMin, yMin ] );
        pause;
    end
    
    eps = 10 .^ -6;
    [ xMin, ~, ~, ~ ] = GoldenRatio( @F1, a, b, eps );
    fprintf( '%.16f\t%.16f\n', eps, xMin );
    for digits = 7 : 16
        xMinPrev = xMin;
        eps = 10 .^ -digits;
        [ xMin, ~, ~, ~ ] = GoldenRatio( @F1, a, b, eps );
        fprintf( '%.16f\t%.16f\n', eps, xMin );
        
	if ( floor( ( xMin - xMinPrev ) * ( 10 .^ ( digits - 1 ) ) ) ~= 0 ), break; end
    end
    
    digits
end