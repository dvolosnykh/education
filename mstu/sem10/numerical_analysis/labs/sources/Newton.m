function [ xMin, yMin, iterationNum, callsCount, xEval ] = Newton( f, eps, xCur )
    iterationsMax = 100;
    
    iterationNum = 0;
    [ yCur, callsCount ] = feval( f, xCur, 0 );
    xEval = xCur;
    h = eps * 0.1;
    while ( true )
        iterationNum = iterationNum + 1;
        
        [ y, callsCount ] = feval( f, xCur + [ -h, h ], callsCount );
        xEval = [ xEval, xCur + [ -h, h ] ];
        
        d2f = ( y( 2 ) - 2 * yCur + y( 1 ) ) / h ^ 2;
        if ( d2f == 0 )
            %disp( 'Infinite Newton step!' );
            break;
        end
        df = ( y( 2 ) - y( 1 ) ) / ( 2 * h );
        step = -df / d2f;
        while ( true )
            xNext = xCur + step;
            [ yNext, callsCount ] = feval( f, xNext, callsCount );
            xEval = [ xEval, xNext ];
            
        if ( yNext < yCur || abs( step ) < eps ), break; end
        
            step = step / 2;
        end

        xCur = xNext;
        yCur = yNext;
        
    if ( abs( step ) < eps || iterationNum > iterationsMax ), break; end
    end
    
    if ( iterationNum > iterationsMax )
        disp( 'Iterations limit reached!' );
    end
    
    xMin = xCur;
    yMin = yCur;
end