function lab8
    x0 = [ 0, -sqrt( 5 ) ];
    eps = 0.001;
    restartRate = 3;
    visualize = false;
    viewport = [ -4, 1; -3, 6 ];
    viewStep = 0.1;
    
    [ xMin, yMin, callsCount, xEval ] = ConjugateDirections( @F2_1, x0, eps, restartRate, visualize, viewport, viewStep );
    xMin
    yMin
    callsCount
    
    PlotSteps( xEval, @F2_1, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Conjugate directions method' );
    pause;

    x0 = [ 3, 3 ];
    eps = 0.001;
    restartRate = 3;
    visualize = false;
    viewport = [ 0.01, 4; 0.01, 4 ];
    viewStep = 0.1;
    
    [ xMin, yMin, callsCount, xEval ] = ConjugateDirections( @F2_2, x0, eps, restartRate, visualize, viewport, viewStep );
    xMin
    yMin
    callsCount
    
    PlotSteps( xEval, @F2_2, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Conjugate directions method' );
end