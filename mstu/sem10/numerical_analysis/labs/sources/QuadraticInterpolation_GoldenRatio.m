function [ xMin, yMin, iterationNum, callsCount, xEval ] = QuadraticInterpolation_GoldenRatio( f, a, b, eps )
    iterationsMax = 100;
    
    iterationNum = 0;

    tau = ( realsqrt( 5 ) - 1 ) / 2;
    
    x = [ a, inf, b ];  % [ xLeft, xMiddle, xRight ]
    [ y( 1 ), callsCount ] = feval( f, x( 1 ), 0 );
	y( 2 ) = inf;
	[ y( 3 ), callsCount ] = feval( f, x( 3 ), callsCount );
    xEval = x( [ 1, 3 ] );
    
    while ( true )
        xGold( 1 ) = x( 3 ) - tau * ( x( 3 ) - x( 1 ) );
        xGold( 2 ) = x( 1 ) + x( 3 ) - xGold( 1 );
        [ yGold, callsCount ] = feval( f, xGold, callsCount );
        xEval = [ xEval, xGold ];

        % Golden ratio.
        while ( true )
            iterationNum = iterationNum + 1;
        
            if ( yGold( 1 ) < yGold( 2 ) )
                x( 2 : 3 ) = xGold;
                y( 2 : 3 ) = yGold;
            else
                x( 1 : 2 ) = xGold;
                y( 1 : 2 ) = yGold;
            end
            
            xStar = InterpolatedParabolaMin( y, x );
        
        if ( x( 3 ) - x( 1 ) < eps || iterationNum > iterationsMax || x( 1 ) <= xStar && xStar <= x( 3 ) ), break; end
        
            if ( yGold( 1 ) < yGold( 2 ) )
                xGold( 2 ) = xGold( 1 );
                yGold( 2 ) = yGold( 1 );

                xGold( 1 ) = x( 1 ) + x( 3 ) - xGold( 2 );
                [ yGold( 1 ), callsCount ] = feval( f, xGold( 1 ), callsCount );
                xEval = [ xEval, xGold( 1 ) ];
            else
                xGold( 1 ) = xGold( 2 );
                yGold( 1 ) = yGold( 2 );

                xGold( 2 ) = x( 1 ) + x( 3 ) - xGold( 1 );
                [ yGold( 2 ), callsCount ] = feval( f, xGold( 2 ), callsCount );
                xEval = [ xEval, xGold( 2 ) ];
            end
        end
        
    if ( x( 3 ) - x( 1 ) < eps || iterationNum > iterationsMax ), break; end
        
        % Quadratic interpolation.
        while ( true )
            iterationNum = iterationNum + 1;
             
            [ yStar, callsCount ] = feval( f, xStar, callsCount );
            xEval = [ xEval, xStar ];
            
            oldDelta = x( 3 ) - x( 1 );
            
            if ( xStar > x( 2 ) )
                if ( yStar > y( 2 ) )
                    x( 3 ) = xStar;
                    y( 3 ) = yStar;
                else
                    x( 1 : 2 ) = [ x( 2 ), xStar ];
                    y( 1 : 2 ) = [ y( 2 ), yStar ];
                end
            else
                if ( yStar > y( 2 ) )
                    x( 1 ) = xStar;
                    y( 1 ) = yStar;
                else
                    x( 2 : 3 ) = [ xStar, x( 2 ) ];
                    y( 2 : 3 ) = [ yStar, y( 2 ) ];
                end
            end
            
            convergence = ( x( 3 ) - x( 1 ) ) / oldDelta;
            
        if ( x( 3 ) - x( 1 ) < eps || iterationNum > iterationsMax || convergence > tau ), break; end
        
            xStar = InterpolatedParabolaMin( y, x );
        end
    end
    
    if ( iterationNum > iterationsMax )
        disp( 'Iterations limit reached!' );
    end
    
    xMin = ( x( 1 ) + x( 3 ) ) / 2;
    [ yMin, callsCount ] = feval( f, xMin, callsCount );
end

function xMin = InterpolatedParabolaMin( y, x )
    s12 = x( 1 ) - x( 2 );
    s23 = x( 2 ) - x( 3 );
    s31 = x( 3 ) - x( 1 );
    
    r12 = s12 * ( x( 1 ) + x( 2 ) );
    r23 = s23 * ( x( 2 ) + x( 3 ) );
    r31 = s31 * ( x( 3 ) + x( 1 ) );
    
    xMin = ( y( 1 ) * r23 + y( 2 ) * r31 + y( 3 ) * r12 ) / ...
        ( y( 1 ) * s23 + y( 2 ) * s31 + y( 3 ) * s12 ) / 2;
end