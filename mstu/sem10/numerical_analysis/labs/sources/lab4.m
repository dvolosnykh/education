function lab4
    a = 0;
    b = 1;
    eps = [ 0.01, 0.0001, 0.000001 ];
    x0 = 0.9;
    
    for i = 1 : length( eps )
        [ xMin, yMin, dummy, callsCount, xEval ] = Newton( @F1, eps( i ), x0 );
        
        if ( i == 1 )
            PlotCurve( @F1, a, b, xEval, 'Newton method with finite difference approximation' );
        else
            PlotCurve( @F1, a, b, xMin, 'Newton method with finite difference approximation' );
        end
        
        fprintf( '%i %.6f %i %.7f %.9f\n', [ i, eps( i ), callsCount, xMin, yMin ] );
        pause;
    end
    
    fun = @( x ) ( F1( x, 0 ) );
    [ xMin, yMin, exitflag, output ] = fminbnd( fun, a, b, optimset( 'TolX', eps( 3 ), 'MaxIter', 100 ) );
    switch ( exitflag )
        case 1
            fprintf( 'fminbnd: %i %.7f %.9f\n', [ output.funcCount, xMin, yMin ] );
            pause;
        otherwise
            fprintf( 'fminbnd: bad exitflag value!' );
    end
    
    eps = 10 ^ -6;
    [ xMin, dummy, dummy, dummy, dummy ] = Newton( @F1, eps, x0 );
    fprintf( '%.16f\t%.16f\n', eps, xMin );
    for digits = 7 : 16
        xMinPrev = xMin;
        eps = 10 .^ -digits;
        [ xMin, dummy, dummy, dummy ] = Newton( @F1, eps, x0 );
        fprintf( '%.16f\t%.16f\n', eps, xMin );
        PlotCurve( @F1, a, b, xEval, 'Newton method with finite difference approximation' );
        pause;
        
	if ( floor( ( xMin - xMinPrev ) * ( 10 .^ ( digits - 1 ) ) ) ~= 0 ), break; end
    end
    
    digits
end
