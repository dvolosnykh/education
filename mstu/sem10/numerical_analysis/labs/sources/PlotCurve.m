function PlotCurve( f, a, b, xmarks, methodName )
    step = 0.000001;
    x = a : step : b;
	[ y, foo ] = feval( f, x, 0 );
    [ ymarks, foo ] = feval( f, xmarks, 0 );
    plot( x, y, '-b', xmarks, ymarks, 'or' );
    title( methodName );
    xlabel( 'x' );
    ylabel( 'y' );
end