function lab11
    f = ...
    { ...
        @( x )( 2 * x + 1 ), ...
        @( x )( 0 ), ...
        @( x )( x .^ 2 - 3 ) ...
    };
    below = [ true, true, false ];
    g = BuildConstraints( f, below );
    
    x0 = [ 0, -sqrt( 5 ) ];
    p = 2;
    eps = 0.001;
    visualize = false;
    viewport = [ -4, 1; -3, 6 ];
    viewStep = 0.01;
    
    [ xMin, yMin, callsCount, xEval ] = PenaltyFunctions( @F2_1, g, x0, p, eps, visualize, viewport, viewStep );
    xMin
    yMin
    callsCount
    
    PlotSteps( xEval, @F2_1, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Penalty function method', f );
end