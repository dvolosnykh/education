function [ xMin, yMin, callsCount, iterations ] = RegularSimplexMethod( f, x0, sideLength, reduceCoeff, eps, visualize, viewport, viewStep )
    x = BuildSimplex( x0, sideLength );
    [ y, callsCount ] = feval( f, x( :, 1 ), x( :, 2 ), 0 );

    iterations( 1 ).x = x;
    while ( true )
        % plot current simplex.
        if ( visualize )
            PlotSimplex( iterations, f, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ) );
            pause;
        end
        
    if ( sideLength < eps ), break; end
        
        [ ~, sortedIndices ] = sort( y, 'descend' );
        
        % try reflecting every vertex.
        i = 1;
        successfulReflection = false;
        totalSum = sum( x );
        while ( i <= length( sortedIndices ) && ~successfulReflection )
            tryIndex = sortedIndices( i );
            xCenter = ( totalSum - x( tryIndex, : ) ) / ( size( x, 1 ) - 1 );
            xNew = 2 * xCenter - x( tryIndex, : );
            [ yNew, callsCount ] = feval( f, xNew( 1 ), xNew( 2 ), callsCount );
            if ( yNew < y( tryIndex ) )
                y( tryIndex ) = yNew;
                x( tryIndex, : ) = xNew;
                successfulReflection  = true;
            end
            
            i = i + 1;
        end
        % else reduce simplex size.
        if ( ~successfulReflection )
            minIndex = sortedIndices( end );
            for i = sortedIndices( 1 : end - 1 )'
                x( i, : ) = x( minIndex, : ) + reduceCoeff * ( x( i, : ) - x( minIndex, : ) );
                [ y( i ), callsCount ] = feval( f, x( i, 1 ), x( i, 2 ), callsCount );
            end
            sideLength = reduceCoeff * sideLength;
        end
        
        % save new simplex.
        iterations( end + 1 ).x = x;
    end
    
    xMin = sum( x ) / length( x );
    [ yMin, callsCount ] = feval( f, xMin( 1 ), xMin( 2 ), callsCount );
end