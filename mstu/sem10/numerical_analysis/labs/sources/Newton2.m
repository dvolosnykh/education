function [ xMin, yMin, callsCount, xEval ] = Newton2( f, xMin, eps, minimize, visualize, viewport, viewStep )
    callsCount = 0;
    xEval = xMin;
    eps2 = eps ^ 2;
    h = eps2;
    yMin = [];
    while ( true )
        [ grad, H, yMin, callsCount ] = Gradient_Hessian( f, xMin, h, yMin, callsCount );
        w = -grad;
        
        % ensure that H is positive definite.
        eigenValues = eig( H );
        positiveDefinite = all( eigenValues > 0 );
        if ( ~positiveDefinite )
            % regularization.
            mu = - min( eigenValues ) + eps;
            H = H + mu * eye( size( H ) );
        end
        
        % evaluate descent direction.
        good = ( ( cond( H ) ^ 2 ) * eps > 1 );
        if ( good )
            direction = linsolve( H, w );
        else
            direction = inv( H ) * w;
        end
        
        % minimize along current direction.
        if ( minimize )
            fun = @( t, c )( feval( f, xMin( 1 ) + t * direction( 1 ), xMin( 2 ) + t * direction( 2 ), c ) );
            [ t, yMin, ~, minimizationCallsCount, tEval ] = Newton( fun, eps, 0 );
            callsCount = callsCount + minimizationCallsCount;
            
            if ( visualize )
                PlotCurve( fun, min( tEval ), max( tEval ), tEval, 'Newton method' );
                pause;
            end
        else
            yMin = [];
            t = 1;
        end

        dx = t * direction';
        xMin = xMin + dx;
        xEval( end + 1, : ) = xMin;
        
        % plot current step.
        if ( visualize )
            PlotSteps( xEval, f, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Newton method' );
            pause;
        end
        
    if ( dx * dx' < eps2 ), break; end
    end
    
    if ( ~minimize )
        [ yMin, callsCount ] = feval( f, xMin( 1 ), xMin( 2 ), callsCount );
    end
end