function [ xMin, yMin, callsCount, xEval ] = DavidonFletcherPowell( f, xMin, eps, restartRate, visualize, viewport, viewStep )
    callsCount = 0;
    xEval = xMin;
    eps2 = eps ^ 2;
    h = eps2;
    yMin = [];
    iterationNum = 1;
    while ( true )
        [ grad, yMin, callsCount ] = Gradient( f, xMin, h, yMin, callsCount );
        w = -grad;

    if ( w' * w < eps2 ), break; end

        % update A.
        if ( mod( iterationNum, restartRate ) == 1 )
            A = eye( length( xMin ) );
        else
            dw = w - wPrev;
            Adw = A * dw;
            A = A - ( dx * dx' ) / ( dx * dw ) - ( Adw * Adw' ) / ( Adw' * dw );
        end

        % evaluate descent direction.
        direction = A * w;
        
        % minimize along current direction.
        fun = @( t, c )( feval( f, xMin( 1 ) + t * direction( 1 ), xMin( 2 ) + t * direction( 2 ), c ) );
        [ t, yMin, ~, minimizationCallsCount, tEval ] = Newton( fun, eps, 0 );
        callsCount = callsCount + minimizationCallsCount;
        
        dx = t * direction';
        xMin = xMin + dx;
        xEval( end + 1, : ) = xMin;
        
        wPrev = w;
        iterationNum = iterationNum + 1;

        % plot current step.
        if ( visualize )
            PlotCurve( fun, min( tEval ), max( tEval ), tEval, 'Newton method' );
            pause;
            PlotSteps( xEval, f, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Davidon-Fletcher-Powell method' );
            pause;
        end
    end
end