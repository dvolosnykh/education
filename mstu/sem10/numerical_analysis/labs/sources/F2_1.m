function [ y, callsCount ] = F2_1( x1, x2, callsCount )
    y = 7 .* x1 .^ 2 + 4 .* x2 .^ 2 + 4 .* x1 .* x2 + ...
        6 .* sqrt( 5 ) .* ( x1 - 2 .* x2 ) + 51;
    callsCount = callsCount + numel( y );
end