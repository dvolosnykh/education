function lab9
    x0 = [ 0, -sqrt( 5 ) ];
    eps = 0.001;
    visualize = true;
    viewport = [ -4, 1; -3, 6 ];
    viewStep = 0.1;
    
    [ xMin, yMin, callsCount, xEval ] = Newton2( @F2_1, x0, eps, false, visualize, viewport, viewStep );
    xMin
    yMin
    callsCount
    
    PlotSteps( xEval, @F2_1, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Newton method' );
    pause;

    x0 = [ 3, 3 ];
    eps = 0.001;
    visualize = false;
    viewport = [ 0.01, 4; 0.01, 4 ];
    viewStep = 0.1;
    
    [ xMin, yMin, callsCount, xEval ] = Newton2( @F2_2, x0, eps, true, visualize, viewport, viewStep );
    xMin
    yMin
    callsCount
    
    PlotSteps( xEval, @F2_2, viewport( 1, 1 ) : viewStep : viewport( 1, 2 ), viewport( 2, 1 ) : viewStep : viewport( 2, 2 ), 'Newton method' );
end