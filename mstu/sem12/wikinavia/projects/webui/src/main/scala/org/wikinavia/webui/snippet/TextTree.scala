package org.wikinavia.webui.snippet

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/14/12
 * Time: 12:02 PM
 * To change this template use File | Settings | File Templates.
 */


import scala.xml.NodeSeq
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.{S, DispatchSnippet}


object TextTree extends DispatchSnippet {
  val dispatch: DispatchIt = {
    case "variables" => variables
  }

  private def variables(xhtml: NodeSeq) = {
    val rootCategory = S ? (if (!MethodWrapper.embedded) "Root category" else "Evaluated category")
    Script(
      JsCrVar("WIKINAVIA_API_URL", "http://%s/api".format(S.hostName)) &
      JsCrVar("WIKIPEDIA_URL", S ? "Wikipedia URL") &
      JsCrVar("ROOT_CATEGORY", rootCategory) &
      JsCrVar("CATEGORY_PREFIX", S ? "Category") &
      JsCrVar("EXPAND", S ? "Expand") &
      JsCrVar("COLLAPSE", S ? "Collapse") &
      JsCrVar("LOADING", S ? "Loading") &
      JsCrVar("FAILED", S ? "Failed")
    )
  }
}
