package org.wikinavia.webui.snippet

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 6/14/12
 * Time: 8:26 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.util.BindHelpers._
import org.wikinavia.webui.snippet.helpers.Helpers.erase
import scala.xml.NodeSeq
import net.liftweb.http.{SHtml, S, DispatchSnippet}
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsCmds._
import org.wikinavia.webui.model.{SUSGrade, Method, SUSStatement}


object SUS extends DispatchSnippet {
  private val SCALE = Integer.parseInt(S ? "Statement grade scale")

  val dispatch: DispatchIt = {
    case "variables" => variables
    case "contents" => contents()
    case "functions" => functions
  }

  private def variables(xhtml: NodeSeq) =
    Script(
      JsCrVar("CONCERN_ALL_STATEMENTS", S ? "Concern all statements")
    )

  private def contents() = {
    "*" #> {
      val allowed = session.isDefined && tasks.isEmpty && !test.isDefined
      if (allowed) {
        ".method" #> (0 until Method.maxId).map { m =>
          val methodName = Method(m).toString.toLowerCase
          ".method [data-method]" #> methodName &
          "h1 *" #> (S ? methodName) &
          ".statement-block *" #> {
            val statements = SUSStatement.findAll()
            if (!statements.isEmpty) {
              statements.map { s =>
                val statementId = s.id.is.toString
                ".statement *" #> s.text.is &
                ".values *" #> (
                  <td></td> ++ (1 to SCALE).map(v => <td>{v.toString}</td>) ++ <td></td>
                ) &
                ".buttons *" #> (
                  <td class="label-cell">{S ? "totaly disagree"}</td> ++
                  (1 to SCALE).map(v =>
                    <td><input type="radio" name={"%s_%s".format(methodName, statementId)} value={v.toString}/></td>
                  ) ++
                  <td class="label-cell">{S ? "totaly agree"}</td>
                )
              }
            } else {
              ((_: NodeSeq) => <p class="empty-set">{S ? "No statements"}</p>) :: Nil
            }
          }
        }
      } else (_: NodeSeq) => <p>{S ? "Not all tasks are completed"}</p>
    }
  }

  private def functions(xhtml: NodeSeq) = Script(
    Function("SEND_SUS", "sus" :: Nil, SHtml.jsonCall(JsVar("sus"), { data =>
      data match {
        case obj: Map[String, Any] => (0 until Method.maxId).foreach { m =>
          val method = Method(m)
          val methodName = method.toString.toLowerCase
          obj(methodName) match {
            case grades: List[Map[String, Double]] =>
              session.foreach { s =>
                grades.foreach { grade =>
                  s.susGrades += SUSGrade.create.statementId(grade("statementId").toLong)
                    .value(grade("value").toInt).method(method)
                }
              }
            case _ =>
          }
        }
        case _ =>
      }

      session.is.get.save()
      session(None)
      Alert("Спасибо за участие!") & RedirectTo("/")
    })._2)
  )
}
