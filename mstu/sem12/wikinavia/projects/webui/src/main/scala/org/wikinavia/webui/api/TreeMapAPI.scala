package org.wikinavia.webui.api

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 4/26/12
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */


import java.io.{ByteArrayInputStream, SequenceInputStream}
import java.net.URLEncoder
import scala.math.log
import net.liftweb.mapper.DB
import net.liftweb.json.JsonDSL._
import net.liftweb.util.Html5
import net.liftweb.util.Helpers.tryo
import net.liftweb.http.provider.HTTPParam
import dispatch.{Http, url}
import net.liftweb.json.JsonParser
import net.liftweb.json.JsonAST._
import scala.collection.mutable.ListBuffer
import net.liftweb.common.{Loggable, Empty, Full, Box}
import net.liftweb.http.{RequestVar, BadResponse, NotImplementedResponse, JsonResponse, S}
import scala.util.Random


object TreeMapAPI extends Loggable {
  private lazy val LEAFS_LIMIT = (S ? "Leafs limit").toInt
  private lazy val DEPTH = (S ? "Tree depth").toInt
  private val DEFAULT_COLOR = (255, 255, 255)

  def handle(params: List[HTTPParam]) = {
    val response = params match {
      case HTTPParam("category", name :: Nil) :: Nil => category(name.replace(' ', '_')).map(JsonResponse(_))
      case HTTPParam("select", query :: Nil) :: Nil => select(query).map(JsonResponse(_))
      case HTTPParam("image", title :: Nil) :: Nil => image(title).map(JsonResponse(_))
      case _ => Full(NotImplementedResponse())
    }
    response or Full(BadResponse())
  }

  private def image(title: String) = {
    val u = ApiUtils.makeUrl(S ? "Wikipedia URL", None, List("w", "index.php"),
      Full(URLEncoder.encode("action=render&title=%s".format(title), "UTF-8")))
    val headers = Map(Bot.UserAgent)
    Http(url(u) <:< headers >> { in =>
      val full = new SequenceInputStream(
        new ByteArrayInputStream("<html><head><title>%s</title><head><body>".format(title).getBytes("UTF-8")),
        new SequenceInputStream(in, new ByteArrayInputStream("</body></html>".getBytes("UTF-8")))
      )
      val start = System.nanoTime()
      val p = Html5 parse full
      val parseTime = System.nanoTime() - start
      val optImg = p map { page =>
        val infoboxes = page \\ "table" filter(_.attribute("class").map(_.text contains "infobox") getOrElse false)
        (infoboxes \\ "img").headOption orElse
          (page \\ "img").find(_.attribute("class").map(_.text contains "thumbimage") getOrElse false)
      } match {
        case Full(Some(img)) => Full(img)
        case _ => Empty
      }
      val json = optImg.map(img =>
        img.attribute("src").map(_.text).map(u => JField("src", if (u startsWith "//") "http:" + u else u))
          .map(src => JObject(List(
              Some(src),
              img.attribute("width").map(a => JField("width", a.text.toLong)),
              img.attribute("height").map(a => JField("height", a.text.toLong))
          ).flatten))
      )
      val selectTime = System.nanoTime() - start - parseTime
      logger.info("PARSE %s: %.3f, %.3f".format(title, parseTime / 1e9, selectTime / 1e9))
      json
    })
  }

  private def category(noPrefixTitle: String) = rootJSON(noPrefixTitle)(categoryChildren)
  private def select(query: String) = rootJSON(query, isCategory = false)(selectChildren)

  private def rootJSON(parent: String, isCategory: Boolean = true)
                       (children: String => Box[(List[(String, Long)], List[(String, Long)])]) = {
    resetIDs
    childrenJSON(DEPTH, None)(children(parent)).map { case (ch, _) =>
      ("id" -> nextID) ~
        ("name" -> parent.replace('_', ' ')) ~
        ("data" ->
          ("type" -> (if (isCategory) "category" else "query")) ~
          ("title" -> (S ? (if (isCategory) "Category" else "Query") + ":" + parent)) ~
          ("$color" -> colorToStr(DEFAULT_COLOR))
        ) ~
        ("children" -> ch)
    }
  }

  private def childrenJSON(depth: Int, color: Option[(Int, Int, Int)])
                          (children: => Box[(List[(String, Long)], List[(String, Long)])]): Box[(List[JValue], Boolean)] =
    if (depth > 0) {
      children.map { case (subcategories, articles) =>
        val s = if (depth == 1 && LEAFS_LIMIT > 0)
          subcategories.sortWith((left, right) => left._2 > right._2).take(LEAFS_LIMIT)
        else subcategories
        val a = if (depth == 1 && LEAFS_LIMIT > 0)
          articles.sortWith((left, right) => left._2 > right._2).take(LEAFS_LIMIT - s.length)
        else articles
        (toJSON(s, depth - 1, color) ::: toJSON(a, color),
          s.length + a.length < subcategories.length + articles.length)
      }
    } else Empty

  private def toJSON(subcategories: List[(String, Long)], depth: Int, color: Option[(Int, Int, Int)]) =
    subcategories.filter { case (_, articles) =>
      articles > 0
    }.map { case (noPrefixTitle, articles) =>
      val c = color getOrElse colorByTitle(noPrefixTitle)
      childrenJSON(depth, Some(c))(categoryChildren(noPrefixTitle)).openOr((Nil, false)) match { case (ch, more) =>
        ("id" -> nextID) ~
          ("name" -> noPrefixTitle.replace('_', ' ')) ~
          ("data" ->
            ("type" -> "category") ~
              ("hasMoreChildren" -> more) ~
              ("title" -> (S ? "Category" + ":" + noPrefixTitle)) ~
              ("articles" -> articles) ~
              ("$area" -> log(9 * articles)) ~
              ("$color" -> colorToStr(c))
            ) ~
          ("children" -> ch)
      }
    }

  private def toJSON(articles: List[(String, Long)], color: Option[(Int, Int, Int)]) =
    articles.map { case (title, counter) =>
      ("id" -> nextID) ~
        ("name" -> title.replace('_', ' ')) ~
        ("data" ->
          ("type" -> "article") ~
            ("title" -> title) ~
            ("counter" -> counter) ~
            ("$area" -> log(9)) ~
            ("$color" -> colorToStr(color getOrElse DEFAULT_COLOR))
          ) ~
        ("children" -> JArray(Nil))
    }

  private def categoryChildren(noPrefixTitle: String) = {
    val subcategories = fetchSubcategories(noPrefixTitle)
    val articles = fetchArticles(noPrefixTitle)
    Full(subcategories, articles)
  }

  private def selectChildren(query: String) = {
    val u = ApiUtils.makeUrl(S ? "Wikipedia URL", None, List("w", "api.php"),
      Full(URLEncoder.encode("action=opensearch&search=%s&namespace=0|14&limit=100&format=json".format(query), "UTF-8")))
    val headers = Map(Bot.UserAgent)
    tryo(Http(url(u) <:< headers >- JsonParser.parse)).map { json =>
      val titles = json(1) match {
        case JArray(a) => a flatMap {
          case JString(s) => Some(s)
          case _ => None
        }
        case _ => Nil
      }

      val subcategories, articles = new ListBuffer[(String, Long)]
      titles.map { title =>
        val categoryPrefix = S ? "Category" + ":"
        val isCategory = title startsWith categoryPrefix
        if (isCategory) {
          val noPrefixTitle = title.substring(categoryPrefix.length).replace(' ', '_')
          fetchArticlesCount(noPrefixTitle).map(subcategories += noPrefixTitle -> _)
        } else {
          val t = title.replace(' ', '_')
          fetchArticleCounter(t).map(articles += t -> _)
        }
      }

      (subcategories.toList, articles.toList)
    }
  }

  private def fetchArticlesCount(noPrefixTitle: String) = {
    val query = "select cat_articles from category where cat_title = ?"
    val params = noPrefixTitle.getBytes("UTF-8") :: Nil
    val result = DB.performQuery(query, params)
    result._2.headOption.map(_(0).asInstanceOf[Long])
  }

  private def fetchArticleCounter(title: String) = {
    val query = "select page_counter from page where page_namespace = 0 and page_title = ?"
    val params = title.getBytes("UTF-8") :: Nil
    val result = DB.performQuery(query, params)
    result._2.headOption.map(_(0).asInstanceOf[Long])
  }

  private def fetchSubcategories(parent: String) = {
    val query = """|select page_title, cat_articles from categorylinks
                   |inner join page on page_id = cl_from
                   |inner join category on page_namespace = 14 and page_title = cat_title
                   |where cl_to = ?""".stripMargin
    val params = parent.getBytes("UTF-8") :: Nil
    val result = DB.performQuery(query, params)
    result._2 map { l =>
      val title = new String(l(0).asInstanceOf[Array[Byte]], "UTF-8")
      val articles = l(1).asInstanceOf[Long]
      (title, articles)
    }
  }

  private def fetchArticles(parent: String) = {
    val query = """|select page_title, page_counter from categorylinks
                   |inner join page on page_id = cl_from
                   |where page_is_redirect = 0 and page_namespace = 0 and cl_to = ?""".stripMargin
    val params = parent.getBytes("UTF-8") :: Nil
    val result = DB.performQuery(query, params)
    result._2.map { l =>
      val title = new String(l(0).asInstanceOf[Array[Byte]], "UTF-8")
      val counter = l(1).asInstanceOf[Long]
      (title, counter)
    }
  }

  private def colorByTitle(title: String) = {
    val hash = hash_js(title)
    Random.setSeed(hash)
    randomColor
  }
  private def hash_sdbm(str: String) = {
    str.foldLeft(0) { (h, c) => c + (h << 6) + (h << 16) - h } & 0x7FFFFFFF
  }
  private def hash_js(str: String) = {
    str.foldLeft(1315423911) { (h, c) => h ^ ((h << 5) + c + (h >> 2)) } & 0x7FFFFFFF
  }
  private def hash_rs(str: String) = {
    var a = 63689
    str.foldLeft(0) { (h, c) => val hh = h * a + c; a = a * 378551; hh } & 0x7FFFFFFF
  }
  private def randomColor = {
    var c = (0, 0, 0)
    while (c._1 == c._2 && c._2 == c._3) {
      c = (randomComponent, randomComponent, randomComponent)
    }
    c
  }
  private def randomComponent = { //Random.nextInt(256)
    val c = (Random.nextGaussian() + 1) * 128
    if (c < 0) 0 else if (c > 255) 255 else c.toInt
  }
  private def colorToStr(color: (Int, Int, Int)) = {
    "#" + (if (color._1 <= 0xf) "0" else "") + Integer.toHexString(color._1) +
      (if (color._2 <= 0xf) "0" else "") + Integer.toHexString(color._2) +
      (if (color._3 <= 0xf) "0" else "") + Integer.toHexString(color._3)
  }

  private object id extends RequestVar[Int](0)
  private def resetIDs = id(0)
  private def nextID = id(id.is + 1)
}
