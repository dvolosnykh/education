package org.wikinavia.webui.model

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/22/12
 * Time: 4:54 PM
 * To change this template use File | Settings | File Templates.
 */

object Method extends Sequence {
  val FullText, TextTree, TreeMap = Value
}

object Grades extends Enumeration {
  val Bad, Neutral, Good = Value
}
