package org.wikinavia.webui.model

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/22/12
 * Time: 3:43 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.mapper.{LongKeyedMetaMapper, LongKeyedMapper, IdPK, OneToMany}


class USession extends LongKeyedMapper[USession] with IdPK with OneToMany[Long, USession] {
  def getSingleton = USession

  object tests extends MappedOneToMany(UTest, UTest.sessionId)
  object susGrades extends MappedOneToMany(SUSGrade, SUSGrade.sessionId)
}

object USession extends USession with LongKeyedMetaMapper[USession]
