package org.wikinavia.webui.snippet

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 28.04.12
 * Time: 22:06
 * To change this template use File | Settings | File Templates.
 */


import scala.xml.NodeSeq
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.{S, DispatchSnippet}
import net.liftweb.util.BindHelpers._


object TreeMap extends DispatchSnippet {
  val dispatch: DispatchIt = {
    case "variables" => variables
    case "noResultsMessage" => noResultsMessage()
    case "exactMatch" => exactMatch()
  }

  private def variables(xhtml: NodeSeq) = {
    val rootCategory = S ? (if (!MethodWrapper.embedded) "Root category" else "Evaluated category")
    Script(
      JsCrVar("WIKINAVIA_API_URL", "http://%s/api".format(S.hostName)) &
      JsCrVar("WIKIPEDIA_URL", S ? "Wikipedia URL") &
      JsCrVar("ROOT_CATEGORY", rootCategory) &
      JsCrVar("INITIAL_CATEGORY", Str(S.param("category") openOr rootCategory)) &
      JsCrVar("CATEGORY_PREFIX", S ? "Category") &
      JsCrVar("GO_TO_ROOT", S ? "Go to root") &
      JsCrVar("GO_UP", S ? "Go up") &
      JsCrVar("QUERY_PLACEHOLDER", S ? "Enter query") &
      JsCrVar("QUERY", Str(S.param("select") openOr "")) &
      JsCrVar("DEPTH", (S ? "Tree depth").toInt) &
      JsCrVar("SHOW_ORIGINAL", S ? "Show original")
    )
  }

  private def noResultsMessage() = "span *" #> (S ? "No results")

  private def exactMatch() = "#exact-match [title]" #> (S ? "Exact match")
}
