package org.wikinavia.webui.api

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 6/1/12
 * Time: 7:19 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.common.Full
import net.liftweb.http.PlainTextResponse
import org.wikinavia.webui.model.{Method, Task, Question, USession}


object Viewer {
  def stats = {
    val response = new StringBuilder
    val header = "ID сессии" :: "Задание" :: "Метод" :: "Время" :: "Успех" :: Question.findAll().map(_.text) :::
      "Ответ" :: "Журнал действий" :: "Отзыв" :: Nil
    response append header.mkString("", "\t", "\n")
    USession.findAll().map { session =>
      session.tests.map { test =>
        Task.find(test.taskId) match {
          case Full(task) =>
            val duration = (test.finishTime - test.startTime) / 1000
            val grades = test.grades.map(_.value.id).toList
            val row = session.id :: task.text :: test.method :: duration :: (if (test.done) 1 else 0) ::
              (if (grades.length == 2) grades else List("", "")) ::: test.answer.replace('\n', ' ') ::
              test.actions.map(action => "%s %s".format(action.kind, action.subject)).mkString(" -> ") ::
              test.comment :: Nil
            response append row.mkString("", "\t", "\n")
          case _ =>
        }
      }
    }
    Full(PlainTextResponse(response.toString()))
  }

  def sus = {
    val response = new StringBuilder
    val header = "ID сессии" :: Method.values.toList.map(_.toString)
    response append  header.mkString("", "\t", "\n")
    USession.findAll().map { session =>
      val row = session.id :: calcSUS(session)
      response append row.mkString("", "\t", "\n")
    }
    Full(PlainTextResponse(response.toString()))
  }

  private def calcSUS(session: USession): List[Double] = {
    val allGrades = session.susGrades.groupBy(_.method.is)
    allGrades.keys.map { key =>
      val grades = allGrades(key)
      (grades.filter(_.statementId.is % 2 == 1).map(_.value.is - 1).sum +
        grades.filter(_.statementId.is % 2 == 0).map(5 - _.value.is).sum) * 2.5
    }.toList
  }
}

