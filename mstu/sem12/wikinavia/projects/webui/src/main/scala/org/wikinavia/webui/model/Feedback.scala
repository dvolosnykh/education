package org.wikinavia.webui.model

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/27/12
 * Time: 10:56 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.mapper.{ValidateLength, MappedTextarea, LongKeyedMetaMapper, LongKeyedMapper, IdPK}
import net.liftweb.http.S


class Feedback extends LongKeyedMapper[Feedback] with IdPK {
  def getSingleton = Feedback

  object text extends MappedTextarea(this, Limits.Task) with ValidateLength {
    override def displayName = S ? "Your feedback"
    override def textareaCols = 64
    override def textareaRows = (maxLen - 1) / textareaCols + 1
    override def toString() = is
    override def dbNotNull_? = true
  }
}

object Feedback extends Feedback with LongKeyedMetaMapper[Feedback]