package org.wikinavia.webui.snippet

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 29.04.12
 * Time: 17:57
 * To change this template use File | Settings | File Templates.
 */


import scala.xml.NodeSeq
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.{S, DispatchSnippet}


object FullText extends DispatchSnippet {
  val dispatch: DispatchIt = {
    case "variables" => variables
  }

  private def variables(xhtml: NodeSeq) = Script(
    JsCrVar("WIKINAVIA_API_URL", "http://%s/api".format(S.hostName)) &
    JsCrVar("WIKIPEDIA_URL", S ? "Wikipedia URL") &
    JsCrVar("QUERY_PLACEHOLDER", S ? "Enter query") &
    JsCrVar("QUERY", Str(S.param("q") openOr ""))
  )
}
