package org.wikinavia.webui.api

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 4/17/12
 * Time: 11:54 AM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.http.rest.RestHelper
import dispatch.{Http, url}
import net.liftweb.common.Full
import net.liftweb.http.{S, JsonResponse, InMemoryResponse}


object ApiDispatcher extends RestHelper {
  val SOLR_PORT = 8080

  serve { List("api") prefix {
    case JsonGet(path @ List("solr", "select"), r) =>
      val url = ApiUtils.makeUrl("http://localhost", Some(SOLR_PORT), path, r.request.queryString)
      proxy(url)

    case "wikipedia" :: Nil JsonGet r =>
      val url = ApiUtils.makeUrl(S ? "Wikipedia URL", None, List("w", "api.php"), r.request.queryString)
      proxy(url, Bot.UserAgent)

    case "treemap" :: Nil JsonGet r => TreeMapAPI handle r.request.params

    case "viewer" :: "stats" :: Nil Get r => Viewer.stats
    case "viewer" :: "sus" :: Nil Get r => Viewer.sus
  }}

  private def proxy(u: String, headers: (String, String)*) = {
    Http(url(u) <:< headers.toMap >> { is =>
      val bytes = Stream.continually(is.read).takeWhile(_ != -1).map(_.toByte).toArray
      val headers = ("Content-Length", bytes.length.toString) ::
        ("Content-Type", "application/json; charset=utf-8") :: JsonResponse.headers
      Full(InMemoryResponse(bytes, headers, JsonResponse.cookies, 200))
    })
  }
}
