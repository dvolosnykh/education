package org.wikinavia.webui.model

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/22/12
 * Time: 4:11 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.http.S
import org.wikinavia.webui.model.helpers.ULongCRUDify
import net.liftweb.mapper.{ValidateLength, MappedTextarea, MappedPoliteString, LongKeyedMetaMapper, OneToMany, IdPK, LongKeyedMapper}


class Question extends LongKeyedMapper[Question] with IdPK with OneToMany[Long, Question] {
  def getSingleton = Question

  object text extends MappedTextarea(this, Limits.Question) with ValidateLength {
    override def displayName = S ? "Question text"
    override def textareaCols = 64
    override def textareaRows = (maxLen - 1) / textareaCols + 1
    override def toString() = is
    override def dbNotNull_? = true
  }

  object bad extends MappedPoliteString(this, Limits.Grade) {
    override def displayName = S ? "Bad grade"
    override def defaultValue = S ? Grades.Bad.toString
    override def dbNotNull_? = true
  }
  object neutral extends MappedPoliteString(this, Limits.Grade) {
    override def displayName = S ? "Neutral grade"
    override def defaultValue = S ? Grades.Neutral.toString
    override def dbNotNull_? = true
  }
  object good extends MappedPoliteString(this, Limits.Grade) {
    override def displayName = S ? "Good grade"
    override def defaultValue = S ? Grades.Good.toString
    override def dbNotNull_? = true
  }

  object grades extends MappedOneToMany(Grade, Grade.questionId)
}

object Question extends Question with ULongCRUDify[Question] with LongKeyedMetaMapper[Question] {
  override def fieldOrder = text :: bad :: neutral :: good :: Nil

  override def calcPrefix = "questions" :: Nil

  override def showAllMenuName = S ? "Show questions"
  override def createMenuName = S ? "Create question"
//  override def editMenuName = S ? "Edit question"
//  override def deleteMenuName = S ? "Delete question"
//  override def viewMenuName = S ? "View question"
}
