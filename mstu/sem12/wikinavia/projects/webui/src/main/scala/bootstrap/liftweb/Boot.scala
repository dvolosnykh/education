package bootstrap.liftweb

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 2/14/12
 * Time: 8:36 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.sitemap.{SiteMap, Menu}
import net.liftweb.http.{S, LiftRules, Req, Html5Properties}
import net.liftweb.http.auth.{userRoles, HttpBasicAuthentication}
import net.liftweb.mapper.{Schemifier, DB, DefaultConnectionIdentifier}
import net.liftweb.util.Props
import org.wikinavia.webui.api.ApiDispatcher
import net.liftweb.db.{DBLogEntry, StandardDBVendor}
import org.wikinavia.webui.snippet.{Participate, MethodWrapper, TextTree, FullText, TreeMap}
import java.util.Locale
import net.liftweb.common.{Logger, Loggable}
import net.liftweb.sitemap.Loc.{Hidden, Template, LocGroup}
import org.wikinavia.webui.model.{SUSStatement, SUSGrade, Counter, Feedback, Grade, Question, USession, Roles, Task, UTest, Action, Method}


class Boot extends Loggable {
  def boot() {
    LiftRules.addToPackages("org.wikinavia.webui")
    LiftRules.configureLogging

    if (!DB.jndiJdbcConnAvailable_?) {
      DB.defineConnectionManager(DefaultConnectionIdentifier, DBVendor)
      LiftRules.unloadHooks.append(() => DBVendor.closeAllConnections_!())
    }

    Schemifier.schemify(true, Schemifier.infoF _, Task, UTest, Action, Grade, Question, USession, Feedback, Counter, SUSGrade, SUSStatement)
    S.addAround(DB.buildLoanWrapper())

    val forPublic = LocGroup("public")
    val sitemap = List(
      Menu.i("About") / "index" >> forPublic,
      Menu.i("Overview") / "overview" >> forPublic submenus (
        Menu("Full text", S ? "fulltext") / "fulltext" >> Template(() => <lift:MethodWrapper.render name="fulltext"/>),
        Menu("Text tree", S ? "texttree") / "texttree" >> Template(() => <lift:MethodWrapper.render name="texttree"/>),
        Menu("Tree map", S ? "treemap") / "treemap" >> Template(() => <lift:MethodWrapper.render name="treemap"/>)
      ),
      Menu.i("Participate") / "participate" >> forPublic,
      Menu.i("Feedback") / "feedback" >> forPublic >> Hidden,
      Menu.i("SUS") / "sus" >> forPublic >> Hidden
    ) ::: Task.menus ::: Question.menus ::: SUSStatement.menus
    LiftRules setSiteMap SiteMap(sitemap: _*)

    LiftRules.authentication = HttpBasicAuthentication("Authenticate yourself") {
      case (username, password, _) =>
        val isAdmin = (username == Props.get("admin").open_! && password == Props.get("password").open_!)
        if (isAdmin) userRoles(Roles.admin :: Nil)
        isAdmin
    }

    DB.addLogFunc { case (log, duration) =>
      logger.debug("Total query time : %d ms".format(duration))
      log.allEntries.foreach {
        case DBLogEntry(stmt, duration) => logger.debug(" %s in %d ms".format(stmt, duration))
      }
    }

//    LiftRules.ajaxStart = Full(() => LiftRules.jsArtifacts.show("ajax-loader").cmd)
//    LiftRules.ajaxEnd = Full(() => LiftRules.jsArtifacts.hide("ajax-loader").cmd)

    LiftRules.statelessDispatchTable.append(ApiDispatcher)

    LiftRules.snippetDispatch.append {
      case "TreeMap" => TreeMap
      case "TextTree" => TextTree
      case "FullText" => FullText
      case "Participate" => Participate
      case "MethodWrapper" => MethodWrapper
    }

    LiftRules.early.append(_ setCharacterEncoding "UTF-8")

    LiftRules.htmlProperties.default.set((r: Req) => new Html5Properties(r.userAgent))

    LiftRules.localeCalculator = req => new Locale("ru")

    prepareDB
  }

  private def prepareDB {
    Task.findAll().foreach { task =>
      val diff = Method.values -- task.counters.map(_.method.is).toSet
      Logger("PREPARE").info(diff.ids.toString())
      task.counters.append(diff.toList.map(method => Counter.create.method(method).taskId(task.id)): _*)
      task.save()
    }
  }
}

object DBVendor extends StandardDBVendor(
  Props.get("db.driver").open_!,
  Props.get("db.url").open_!,
  Props.get("db.user"),
  Props.get("db.password")
)