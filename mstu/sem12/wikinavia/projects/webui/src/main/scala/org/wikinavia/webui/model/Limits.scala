package org.wikinavia.webui.model

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/24/12
 * Time: 6:06 PM
 * To change this template use File | Settings | File Templates.
 */

object Limits {
  val Answer = 4095
  val Comment = 4095
  val Task = 1023
  val Kind = 15
  val Subject = 255
  val Question = 511
  val Grade = 31
}
