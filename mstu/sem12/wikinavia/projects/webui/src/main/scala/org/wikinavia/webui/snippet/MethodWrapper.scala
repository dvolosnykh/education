package org.wikinavia.webui.snippet

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/15/12
 * Time: 9:40 AM
 * To change this template use File | Settings | File Templates.
 */


import scala.xml.NodeSeq
import net.liftweb.http.js.JsCmds.{JsCrVar, Script}
import net.liftweb.http.{RequestVar, DispatchSnippet, S, RenderDispatch}


object MethodWrapper extends DispatchSnippet with RenderDispatch {
  object embedded extends RequestVar[Boolean](false)

  def render(xhtml: NodeSeq) = {
    val method = S.attr("name", _.toString).open_!
    embedded(S.attr("embedded", _.toBoolean) openOr false)

    if (!embedded) {
      val id = "%s_main".format(method)
      <lift:surround id={id} with="public" at="public_content">
        <lift:head>{Script(
          JsCrVar("EMBEDDED", embedded.is)
        )}</lift:head>
        <article>
          {
//          <header>
//            <h1>{S ? method}</h1>
//          </header>
          }
          <lift:embed what={method}/>
        </article>
      </lift:surround>
    } else {
      <lift:head>{Script(
        JsCrVar("EMBEDDED", embedded.is)
      )}</lift:head>
      <lift:embed what={method}/>
    }
  }
}
