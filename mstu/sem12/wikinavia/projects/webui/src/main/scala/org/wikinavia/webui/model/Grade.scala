package org.wikinavia.webui.model

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/22/12
 * Time: 4:39 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.mapper.{MappedEnum, MappedLongForeignKey, LongKeyedMetaMapper, IdPK, LongKeyedMapper}


class Grade extends LongKeyedMapper[Grade] with IdPK {
  def getSingleton = Grade

  object questionId extends MappedLongForeignKey(this, Question) {
    override def dbNotNull_? = true
  }
  object testId extends MappedLongForeignKey(this, UTest) {
    override def dbNotNull_? = true
  }
  object value extends MappedEnum(this, Grades) {
    override def defaultValue = Grades.Neutral
    override def dbNotNull_? = true
  }
}

object Grade extends Grade with LongKeyedMetaMapper[Grade]
