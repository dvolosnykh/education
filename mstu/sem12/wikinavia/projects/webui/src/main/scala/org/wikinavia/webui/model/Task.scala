package org.wikinavia.webui.model

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/7/12
 * Time: 12:14 PM
 * To change this template use File | Settings | File Templates.
 */


import scala.util.Random
import net.liftweb.http.S
import net.liftweb.common.{Empty, Full}
import org.wikinavia.webui.model.helpers.ULongCRUDify
import net.liftweb.mapper.{ValidateLength, OneToMany, IHaveValidatedThisSQL, LongKeyedMetaMapper, MappedTextarea, IdPK, LongKeyedMapper}
import scala.collection.mutable.Queue


class Task extends LongKeyedMapper[Task] with IdPK with OneToMany[Long, Task] {
  def getSingleton = Task

  object text extends MappedTextarea(this, Limits.Task) with ValidateLength {
    override def displayName = S ? "Task text"
    override def textareaCols = 64
    override def textareaRows = (maxLen - 1) / textareaCols + 1
    override def toString() = is
    override def dbNotNull_? = true
  }
  object answer extends MappedTextarea(this, Limits.Answer) with ValidateLength {
    override def displayName = S ? "True answer"
    override def textareaCols = 64
    override def textareaRows = 4
    override def toString() = is
    override def dbNotNull_? = true
  }

  object tests extends MappedOneToMany(UTest, UTest.taskId)
  object counters extends MappedOneToMany(Counter, Counter.taskId)
}

object Task extends Task with ULongCRUDify[Task] with LongKeyedMetaMapper[Task] {
  override def fieldOrder = text :: Nil

  override def calcPrefix = "tasks" :: Nil

  override def showAllMenuName = S ? "Show tasks"
  override def createMenuName = S ? "Create task"
//  override def editMenuName = S ? "Edit task"
//  override def deleteMenuName = S ? "Delete task"
//  override def viewMenuName = S ? "View task"

  def random = {
    val tasks = Task.findAll()
    if (tasks.length > 0) {
      Full(tasks(Random.nextInt(tasks.length)))
    } else Empty
//    val query = """|SELECT * FROM %1$s
//                   |WHERE %2$s >= (SELECT FLOOR(MAX(%2$s) * RAND()) FROM %1$s)
//                   |ORDER BY %2$s LIMIT 1""".stripMargin.format(dbTableName, id.dbColumnName)
//    findAllByInsecureSql(query, IHaveValidatedThisSQL("Dmitry F. Volosnykh", "10.05.2012")) match {
//      case first :: rest => Full(first)
//      case Nil => Empty
//    }
  }

  def leastUsed(count: Int) = synchronized {
    val queue = new Queue[(Long, Method.Value)]()

    var tasks = Task.findAll()
    if (!tasks.isEmpty) {
      (Method.values - Method.TextTree).foreach { method =>
        tasks = tasks.sortWith((left, right) =>
          left.counters.filter(_.method == method).head.value < right.counters.filter(_.method == method).head.value
        )
        val parts = tasks.splitAt(count)
        queue.enqueue(parts._1.map { t =>
          val c = t.counters.filter(_.method == method).head
          c.value(c.value.is + 1).save()
          (t.id.is, method)
        }: _*)
        tasks = parts._2
      }
    }

    queue
  }
}