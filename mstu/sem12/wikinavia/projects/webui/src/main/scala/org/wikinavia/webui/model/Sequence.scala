package org.wikinavia.webui.model

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/22/12
 * Time: 4:53 PM
 * To change this template use File | Settings | File Templates.
 */

abstract class Sequence extends Enumeration {
  lazy val first = apply(0)
  def next(v: Value) = apply(v.id + 1)
  def isLast(v: Value) = v.id + 1 == maxId
}
