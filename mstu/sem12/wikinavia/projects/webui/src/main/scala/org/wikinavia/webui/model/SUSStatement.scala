package org.wikinavia.webui.model

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 6/14/12
 * Time: 7:00 PM
 * To change this template use File | Settings | File Templates.
 */


import org.wikinavia.webui.model.helpers.ULongCRUDify
import net.liftweb.http.S
import net.liftweb.mapper.{ValidateLength, MappedTextarea, LongKeyedMetaMapper, OneToMany, IdPK, LongKeyedMapper}


class SUSStatement extends LongKeyedMapper[SUSStatement] with IdPK with OneToMany[Long, SUSStatement] {
  def getSingleton = SUSStatement

  object text extends MappedTextarea(this, Limits.Question) with ValidateLength {
    override def displayName = S ? "Statement text"
    override def textareaCols = 64
    override def textareaRows = (maxLen - 1) / textareaCols + 1
    override def toString() = is
    override def dbNotNull_? = true
  }

  object grades extends MappedOneToMany(SUSGrade, SUSGrade.statementId)
}

object SUSStatement extends SUSStatement with ULongCRUDify[SUSStatement] with LongKeyedMetaMapper[SUSStatement] {
  override def fieldOrder = text :: Nil

  override def calcPrefix = "statements" :: Nil

  override def showAllMenuName = S ? "Show statements"
  override def createMenuName = S ? "Create statement"
  //  override def editMenuName = S ? "Edit question"
  //  override def deleteMenuName = S ? "Delete question"
  //  override def viewMenuName = S ? "View question"
}
