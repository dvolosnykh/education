package org.wikinavia.webui.snippet


/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/28/12
 * Time: 1:43 AM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.util.BindHelpers._
import net.liftweb.http.{S, SHtml, DispatchSnippet}
import org.wikinavia.webui.model.{Feedback => UFeedback}


object Feedback extends DispatchSnippet {
  val dispatch: DispatchIt = {
    case "send" => send()
  }

  private def send() = {
    val feedback = UFeedback.create

    def saveFeedback() {
      S.notice(S ? "Thanks for you response")
      feedback.save()
    }

    ".text *" #> SHtml.textarea("", feedback.text(_), "rows" -> feedback.text.textareaRows.toString,
      "cols" -> feedback.text.textareaCols.toString) &
    ".send *" #> SHtml.submit(S ? "Send", saveFeedback)
  }
}
