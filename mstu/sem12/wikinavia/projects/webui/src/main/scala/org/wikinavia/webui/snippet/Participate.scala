package org.wikinavia.webui.snippet

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 2/14/12
 * Time: 8:54 PM
 * To change this template use File | Settings | File Templates.
 */


import scala.xml.NodeSeq
import scala.collection.mutable.Queue
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.{RequestVar, S, SHtml, SessionVar, DispatchSnippet}
import net.liftweb.util.BindHelpers._
import org.wikinavia.webui.snippet.helpers.Helpers.erase
import scala.util.Random
import net.liftweb.common.Logger
import org.wikinavia.webui.model.{Grade, Grades, Question, USession, Action, UTest, Task, Method}


private object session extends SessionVar[Option[USession]](None)
private object test extends SessionVar[Option[UTest]](None)
private object tasks extends SessionVar[Queue[(Long, Method.Value)]](new Queue)
private object hasStarted extends SessionVar[Boolean](false)


object Participate extends DispatchSnippet {
  private val TASKS_PER_METHOD = Integer.parseInt(S ? "Tasks per method")
  private object firstTask extends RequestVar[Boolean](false)
  private object newTest extends RequestVar[Boolean](false)

  val dispatch: DispatchIt = {
    case "logic" => {logic; erase}
    case "variables" => variables
    case "taskHeader" => taskHeader()
    case "instructionsForm" => instructionsForm()
    case "embeddedMethod" => embeddedMethod
    case "doneForm" => doneForm()
    case "functions" => functions
  }

  private def logic {
    firstTask(session.isEmpty)
    if (firstTask) {
      session(Some(USession.create))

      tasks(Random.shuffle(Task.leastUsed(TASKS_PER_METHOD)))
//      val t = Task.findAll()
//      if (!t.isEmpty) {
//        val parts = ids.splitAt(shift)
//        val shiftedIds = parts._2 ::: parts._1
//
//        val methodIds = Method.values.ids.toList
//        val t = for (method <- methodIds; i <- Random.shuffle(method until ids.length by Method.maxId)) yield i
//        val p = for (method <- methodIds; i <- method until ids.length by Method.maxId) yield shiftedIds(t(i))
//        val queueLen = ((p.length - 1) / Method.maxId + 1) * Method.maxId
//        tasks.enqueue(Stream.continually(shiftedIds.toStream).flatten.take(queueLen).toList: _*)
//      }
    }

    newTest(test.isEmpty)
    if (newTest) {
      hasStarted(false)
      val nextTaskId = if (!tasks.isEmpty) Some(tasks.dequeue()) else None
      nextTaskId match {
        case Some((id, method)) =>
          val t = UTest.create.taskId(id).method(method).startTime(System.currentTimeMillis())
          session.is.get.tests += t
          test(Some(t))
          session.is.get.save()
        case _ =>
          session(None)
      }
    }
  }

  private def variables(xhtml: NodeSeq) = if (test.isDefined) {
    Script(
      JsCrVar("START", S ? "Start") &
      JsCrVar("NEXT", S ? "Next")
    )
  } else NodeSeq.Empty

  private def taskHeader() = if (test.isDefined) {
    val current = TASKS_PER_METHOD * Method.maxId - tasks.is.length
    val total = TASKS_PER_METHOD * Method.maxId
    val currentTask = test.flatMap(t => Task.findByKey(t.taskId))
    "#task-counter *" #> (S ? "Task counter format").format(current, total) & (
      currentTask match {
        case Some(t) => "#task-text *" #> t.text.is
        case _ => "#task-text *" #> (S ? "No tasks") & "#task-text [class+]" #> "empty-set"
      }
    ) & "#done-button *" #> (S ? "Ready to answer") &
    "#giveup-button *" #> (S ? "Give up")
  } else erase

  private def instructionsForm() = if (test.isDefined) {
    "#instructions-form [title]" #> (S ? "Instructions")
  } else erase

  private def embeddedMethod(xhtml: NodeSeq) = if (test.isDefined) {
    val methodName = test.is.get.method.is.toString.toLowerCase
    <lift:MethodWrapper.render name={methodName} embedded="true"/>
  } else NodeSeq.Empty

  private def doneForm() = if (test.isDefined) {
    val currentTask = test.flatMap(t => Task.findByKey(t.taskId))

    "#done-form [title]" #> (S ? "Done") & (
      currentTask match {
        case Some(t) => "#done-task-text *" #> t.text.is
        case _ => "#done-task-text *" #> (S ? "No tasks") & "#done-task-text [class+]" #> "empty-set"
      }
      ) & (Question.findAll() match {
        // If no questions then replace form by warning paragraph.
        case Nil => "form" #> <p class="empty-set">{S ? "No questions"}</p>
        // ...else generate table-row for each of the questions.
        case questions => ("table *" #> ((_: NodeSeq) => questions.map { q =>
          val grades = q.bad.is :: q.neutral.is :: q.good.is :: Nil
          <tr>{
            (<td>{q.text.is}</td>) ++
              grades.view.zip(Grades.values.ids).map{ case (grade, value) =>
                <td><span class="grade">{
                  if (value == Grades.Neutral.id) {
                      <input type="radio" name={q.id.is.toString} value={value.toString} checked="checked"/>
                  } else {
                      <input type="radio" name={q.id.is.toString} value={value.toString}/>
                  }
                  }{grade}
                </span></td>
              }
            }</tr>
        }))
      })
  } else (_: NodeSeq) => <p class="empty-set">{S ? "No tasks"}</p>

  private def functions(xhtml: NodeSeq) = if (test.isDefined) {
    Script(
      Function("ON_START", "startTime" :: Nil, SHtml.jsonCall(JsVar("startTime"), { data =>
        test foreach { t => data match {
          case startTime: Double =>
            if (!hasStarted) {
              t.startTime(System.currentTimeMillis())
              session.is.get.save()
              hasStarted(true)
            }
          case _ =>
        }}
        Noop
      })._2) &
        Function("TRACK", "action" :: Nil, SHtml.jsonCall(JsVar("action"), { data =>
          test foreach { t => data match {
            case obj: Map[String, Any] =>
              (obj("kind"), obj("subject")) match {
                case (kind: String, subject: String) =>
                  t.actions += Action.create.kind(kind).subject(subject)
                  session.is.get.save()
              }
            case _ =>
          }}
          Noop
        })._2) &
        Function("ON_FINISH", "result" :: Nil, SHtml.jsonCall(JsVar("result"), { data =>
          test foreach { t => data match {
            case obj: Map[String, Any] =>
              (obj("done"), obj("finishTime")) match {
                case (done: Boolean, finishTime: Double) => t.done(done).finishTime(System.currentTimeMillis())
                case _ =>
              }

              (obj.get("answer"), obj.get("grades"), obj.get("comment")) match {
                case (Some(answer: String), Some(grades: List[Map[String, Double]]), Some(comment: String)) =>
                  t.answer(answer).comment(comment)
                  grades.foreach { grade =>
                    t.grades += Grade.create.questionId(grade("questionId").toLong)
                      .value(Grades(grade("value").toInt))
                  }
                case _ =>
              }

              session.is.get.save()
            case _ =>
          }}
          test(None)

          RedirectTo(if (!tasks.isEmpty) "/participate" else "/sus")
        })._2) &
        JsFunc("participate_init", !hasStarted, firstTask.is).cmd
    )
  } else NodeSeq.Empty
}