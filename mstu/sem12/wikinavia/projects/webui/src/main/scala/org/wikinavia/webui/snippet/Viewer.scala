package org.wikinavia.webui.snippet

import net.liftweb.http.{S, DispatchSnippet}
import net.liftweb.common.Full
import net.liftweb.util.BindHelpers._
import org.wikinavia.webui.snippet.helpers.Helpers.erase
import org.wikinavia.webui.model.USession
import scala.xml.NodeSeq

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 6/1/12
 * Time: 5:19 PM
 * To change this template use File | Settings | File Templates.
 */


object Viewer extends DispatchSnippet {
  val dispatch: DispatchIt = {
    case "show" => show()
  }

  private def show() = "div *" #>
    (S.param("session") match {
      case Full(idStr) =>
        try {
          val id = Integer.parseInt(idStr)
          showSession(id)
        } catch {
          case _ => showAllSessions
        }
      case _ => showAllSessions
    })

  private def showAllSessions = (_: NodeSeq) =>
    <table>
      <thead>
        <tr>
          <th>{"ID сессии"}</th><th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>{
        USession.findAll().map { s =>
          <tr><td>{s.id.toString}</td><td><a href={"/viewer?session=%d".format(s.id)}>{"Показать"}</a></td></tr>
        }
      }</tbody>
    </table>

  private def showSession(id: Int) = (_: NodeSeq) =>
    <table>
      <thead>
      </thead>
    </table>
}
