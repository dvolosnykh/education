package org.wikinavia.webui.model


/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 6/14/12
 * Time: 7:10 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.mapper.{MappedEnum, MappedInt, MappedLongForeignKey, LongKeyedMetaMapper, IdPK, LongKeyedMapper}


class SUSGrade extends LongKeyedMapper[SUSGrade] with IdPK {
  def getSingleton = SUSGrade

  object statementId extends MappedLongForeignKey(this, SUSStatement) {
    override def dbNotNull_? = true
  }
  object sessionId extends MappedLongForeignKey(this, USession) {
    override def dbNotNull_? = true
  }
  object value extends MappedInt(this) {
    override def dbNotNull_? = true
  }
  object method extends MappedEnum(this, Method) {
    override def dbNotNull_? = true
  }
}

object SUSGrade extends SUSGrade with LongKeyedMetaMapper[SUSGrade]