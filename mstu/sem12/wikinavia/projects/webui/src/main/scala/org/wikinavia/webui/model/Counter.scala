package org.wikinavia.webui.model

import net.liftweb.mapper.{MappedLong, MappedEnum, MappedLongForeignKey, LongKeyedMetaMapper, IdPK, LongKeyedMapper}

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 6/12/12
 * Time: 10:52 PM
 * To change this template use File | Settings | File Templates.
 */

class Counter extends LongKeyedMapper[Counter] with IdPK {
  def getSingleton = Counter

  object taskId extends MappedLongForeignKey(this, Task) {
    override def dbNotNull_? = true
  }
  object method extends MappedEnum(this, Method) {
    override def dbNotNull_? = true
  }
  object value extends MappedLong(this) {
    override def dbNotNull_? = true
  }
}

object Counter extends Counter with LongKeyedMetaMapper[Counter]
