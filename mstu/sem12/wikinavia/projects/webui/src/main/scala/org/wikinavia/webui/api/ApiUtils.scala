package org.wikinavia.webui.api

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/11/12
 * Time: 11:50 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.common.Box


object ApiUtils {
  def makeUrl(host: String, port: Option[Int], path: List[String], queryString: Box[String]) =
    host + port.map(":" + _).getOrElse("") + "/" + path.mkString("/") + queryString.map("?" + _).openOr("")
}
