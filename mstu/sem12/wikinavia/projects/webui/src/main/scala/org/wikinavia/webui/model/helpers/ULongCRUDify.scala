package org.wikinavia.webui.model.helpers

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/22/12
 * Time: 4:25 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.http.S
import net.liftweb.sitemap.Loc.{HttpAuthProtected, LocGroup, Hidden}
import net.liftweb.common.Full
import org.wikinavia.webui.model.Roles
import net.liftweb.mapper.{KeyedMetaMapper, KeyedMapper, LongCRUDify}
import scala.xml.NodeSeq


trait ULongCRUDify[CrudType <: KeyedMapper[Long, CrudType]] extends LongCRUDify[CrudType] {
  self: CrudType with KeyedMetaMapper[Long, CrudType] =>

  override def editButton = S ? "Save"
  override def deleteButton = S ? "Delete"
  override def createButton = S ? "Create"

  override def previousWord = S ? "Previous"
  override def nextWord = S ? "Next"

  override def viewMenuLocParams = Hidden :: Nil
  override def editMenuLocParams = Hidden :: Nil
  override def deleteMenuLocParams = Hidden :: Nil
  override def addlMenuLocParams = LocGroup("admin") :: HttpAuthProtected(req => Full(Roles.admin)) :: Nil

  override def pageWrapper(body: NodeSeq) =
    <lift:surround with="admin" at="admin_content">
      <lift:head>
          <link rel="stylesheet" href="/css/crud.css"/>
      </lift:head>
      {body}
    </lift:surround>

  override def _showAllTemplate =
    <lift:crud.all>
      <table id={showAllId} class={showAllClass}>
        <thead>
          <tr>
            <crud:header_item><th><crud:name/></th></crud:header_item>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <crud:row>
            <tr>
              <crud:row_item><td><crud:value/></td></crud:row_item>
              <td><a crud:view_href="">{S ? "View"}</a></td>
              <td><a crud:edit_href="">{S ? "Edit"}</a></td>
              <td><a crud:delete_href="">{S ? "Delete"}</a></td>
            </tr>
          </crud:row>
        </tbody>
      </table>
      <div class="pager_links">
        <span class="previous"><crud:prev>{previousWord}</crud:prev></span>
        <span class="next"><crud:next>{nextWord}</crud:next></span>
      </div>
    </lift:crud.all>
}
