package org.wikinavia.webui.model

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/14/12
 * Time: 1:18 AM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.mapper.{MappedPoliteString, MappedLongForeignKey, LongKeyedMetaMapper, IdPK, LongKeyedMapper}


class Action extends LongKeyedMapper[Action] with IdPK {
  def getSingleton = Action

  object testId extends MappedLongForeignKey(this, UTest) {
    override def dbNotNull_? = true
  }
  object kind extends MappedPoliteString(this, Limits.Kind) {
    override def dbNotNull_? = true
  }
  object subject extends MappedPoliteString(this, Limits.Subject) {
    override def dbNotNull_? = true
  }
}

object Action extends Action with LongKeyedMetaMapper[Action]
