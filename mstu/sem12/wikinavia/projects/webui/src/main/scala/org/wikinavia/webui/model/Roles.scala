package org.wikinavia.webui.model

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/7/12
 * Time: 6:04 PM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.http.auth.AuthRole


object Roles {
  val admin = AuthRole("admin")
}
