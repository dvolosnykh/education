package org.wikinavia.webui.api

/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/14/12
 * Time: 5:23 PM
 * To change this template use File | Settings | File Templates.
 */

object Bot {
  val Name = "wikinavia"
  val Version = "0.1"
  val Email = "wikinavia@gmail.com"

  val UserAgent = "User-Agent" -> "%s/%s (%s)".format(Bot.Name, Bot.Version, Bot.Email)
}
