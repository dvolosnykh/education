package org.wikinavia.webui.model


/**
 * Created with IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 5/14/12
 * Time: 12:58 AM
 * To change this template use File | Settings | File Templates.
 */


import net.liftweb.http.S
import net.liftweb.mapper.{MappedEnum, MappedLong, OneToMany, MappedLongForeignKey, MappedBoolean, MappedPoliteString,
  LongKeyedMetaMapper, IdPK, LongKeyedMapper}


class UTest extends LongKeyedMapper[UTest] with IdPK with OneToMany[Long, UTest] {
  def getSingleton = UTest

  object sessionId extends MappedLongForeignKey(this, USession) {
    override def dbNotNull_? = true
  }
  object taskId extends MappedLongForeignKey(this, Task) {
    override def dbNotNull_? = true
  }
  object method extends MappedEnum(this, Method) {
    override def dbNotNull_? = true
  }
  object done extends MappedBoolean(this) {
    override def dbNotNull_? = true
  }
  object answer extends MappedPoliteString(this, Limits.Answer) {
    override def displayName = S ? "Answer"
    override def toString() = is
    override def dbNotNull_? = true
  }
  object comment extends MappedPoliteString(this, Limits.Comment) {
    override def displayName = S ? "Comment"
    override def toString() = is
    override def dbNotNull_? = true
  }
  object startTime extends MappedLong(this) {
    override def dbNotNull_? = true
  }
  object finishTime extends MappedLong(this) {
    override def dbNotNull_? = true
  }

  object actions extends MappedOneToMany(Action, Action.testId)
  object grades extends MappedOneToMany(Grade, Grade.testId)
}

object UTest extends UTest with LongKeyedMetaMapper[UTest]
