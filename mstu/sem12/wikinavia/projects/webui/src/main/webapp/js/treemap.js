var labelType, useGradients, nativeTextSupport, animate;
(function () {
  var ua = navigator.userAgent,
      iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
      typeOfCanvas = typeof HTMLCanvasElement,
      nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
      textSupport = nativeCanvasSupport 
        && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
  //I'm setting this based on the fact that ExCanvas provides text support for IE
  //and that as of today iPhone/iPad current text support is lame
  labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
  nativeTextSupport = labelType == 'Native';
  useGradients = nativeCanvasSupport;
  animate = !(iStuff || !nativeCanvasSupport);
})();

var EXTRACT_LEN = 511;
var extractParams = jQuery.validator.format('action=query&titles={0}&prop=extracts&exchars={1}&exintro&explaintext&format=json');
var autocompleteParams = jQuery.validator.format('action=opensearch&search={0}&namespace=0|14&limit=15&format=json');
var imageTag = jQuery.validator.format('<img src="{0}" {1}="{2}" class="on-white"/>')

//var wikipediaApiUrl = WIKIPEDIA_URL + '/w/api.php?callback=?&';
var wikipediaApiUrl = WIKINAVIA_API_URL + '/wikipedia?';
var treemapApiUrl = WIKINAVIA_API_URL + '/treemap?';

function RequestsManager() {
    this.updateRequest = null;
    this.tipImageRequest = null;
    this.tipTextRequest = null;
    this.nodeImageRequests = [];

    this.abortTipRequests = function() {
        if (this.tipImageRequest != null) {
            this.tipImageRequest.abort();
            this.tipImageRequest = null;
        }
        if (this.tipTextRequest != null) {
            this.tipTextRequest.abort();
            this.tipTextRequest = null;
        }
    }

    this.abortNodeImageRequests = function() {
        while (this.nodeImageRequests.length > 0) {
            this.nodeImageRequests.pop().abort();
        }
    }

    this.abortUpdateRequests = function() {
        this.abortTipRequests();
        this.abortNodeImageRequests();
        if (this.updateRequest != null) {
            this.updateRequest.abort();
            this.updateRequest = null;
        }
    }
}

function treemap_init() {
    var firstTime = true;

    var $path = jQuery('#path');
    var $exactContent = jQuery('#exact-content');
    var $goToRoot = jQuery('.goToRoot');
    var $goUp = jQuery('.goUp');

    var path = [];
    var current;

    function tweakPageUrl(query) {
        if (!EMBEDDED) window.history.replaceState(null, null, '?' + query.type + '=' + query.value);
    }

    function buildPath() {
        $path.empty();
        jQuery.each(path, function(index, parent) {
            jQuery('<span class="path-parent">' + parent.value + '</span>')
                .addClass('path-' + parent.type).appendTo($path)
                .click(function() {
                    path.length = index;
                    update(tm, parent);
                });
            $path.append('&nbsp;&gt; ');
        });
        jQuery('<span class="path-current">' + current.value + '</span>')
            .addClass('path-' + current.type).appendTo($path);
    }

    var rm = new RequestsManager;
    var exactNode = null;

    function onExactMatch(node) {
        var markup =
            '<div class="exact-header">' +
                '<img class="exact-spinner" src="/images/tip-loader.gif"/>' +
                 '<span class="exact-title">' + node.name + '</span>' +
            '</div>' +
            '<div class="exact-image"/><div class="exact-text"/>';
        var $linkToOriginal = jQuery('<a>' + SHOW_ORIGINAL + '</a>').click(function() {
            var pageUrl = WIKIPEDIA_URL + '/wiki/' + node.name;
            window.open(pageUrl);
            if (typeof TRACK == 'function') TRACK({kind: 'view', subject: node.name});
        })
        $exactContent.html(markup).append(jQuery('<div class="exact-link"/>').html($linkToOriginal));
        $exactDialog.dialog('open');

        var requestsCount = 2;
        var requestFinished = function() {
            if (--requestsCount == 0) {
                jQuery('.exact-spinner', $exactContent).hide();
            }
        }

        var url = treemapApiUrl + 'image=' + node.data.title;
        rm.tipImageRequest = jQuery.ajax(encodeURI(url)).always(requestFinished).done(function(response) {
            var dim = (response.width > response.height ? 'width' : 'height');
            jQuery('.exact-image', $exactContent).html(imageTag(response.src, dim, 300));
        });

        url = wikipediaApiUrl + extractParams(node.data.title, EXTRACT_LEN);
        rm.tipTextRequest = jQuery.getJSON(encodeURI(url)).always(requestFinished).done(function(response) {
            if (typeof response.query != 'undefined' && typeof response.query.pages != 'undefined') {
                var pages = response.query.pages;
                for (pageId in pages) {
                    if (pages.hasOwnProperty(pageId)) {
                        var text = pages[pageId].extract;
                        jQuery('.exact-text', $exactContent).html(text);
                        break;
                    }
                }
            }
        });
    }

    function update(tm, query, onSuccess) {
        rm.abortUpdateRequests();

        var url = treemapApiUrl + query.type + '=' + query.value;
        rm.updateRequest = jQuery.ajax(encodeURI(url), {
            beforeSend: function() {
                jQuery('.no-results').hide();
                jQuery('.ajax-loader').show();
            },
            complete: function() {
                jQuery('.ajax-loader').hide();
            }
        }).done(function(root) {
            if (root.children.length > 0) {
                tweakPageUrl(query);
                if (typeof TRACK == 'function') {
                    TRACK({kind: query.type, subject: (query.type == 'category' ? CATEGORY_PREFIX + ':' : '') + query.value});
                }
                if (typeof onSuccess == 'function') onSuccess();

                tm.loadJSON(root);
                current = query;
                exactNode = null;
                tm.refresh();
                if (exactNode != null) {
                    onExactMatch(exactNode);
                    exactNode = null;
                }

                var atInitial = (path.length == 0);
                var atRoot = (current.type == 'category' && current.value == ROOT_CATEGORY);
                var parentName = ROOT_CATEGORY;
                if (!atInitial) parentName = path[path.length - 1].value;
                $goToRoot.prop('disabled', atRoot);
                $goUp.prop('disabled', atRoot).html(GO_UP + ': ' + parentName);

                buildPath();
            } else if (firstTime) {
                update(tm, {type: 'category', value: ROOT_CATEGORY});
            } else {
                jQuery('.no-results').show();
            }

            firstTime = false;
        });
    }

    function downThePath() {
        path.push(current);
    }
    function upThePath() {
        path.pop();
    }

    function walkDown(node) {
        switch (node.data.type) {
        case 'category':
            update(tm, {type: 'category', value: node.name}, downThePath);
            break;
        case 'article':
            var pageUrl = WIKIPEDIA_URL + '/wiki/' + node.data.title;
            window.open(pageUrl);
            if (typeof TRACK == 'function') TRACK({kind: 'view', subject: node.name});
            break;
        }
    }
    function walkUp(node) {
        var atInitial = (path.length == 0);
        var atRoot = (current.value == ROOT_CATEGORY);
        if (!atInitial || !atRoot) {
            var parent = {type: 'category', value: ROOT_CATEGORY};
            if (!atInitial) parent = path[path.length - 1];
            update(tm, parent, upThePath);
        }
    }

    var tm = new $jit.TM.Centrified({
        injectInto: 'treemap',
        titleHeight: 20,
        animate: false,
        cushion: useGradients,
        levelsToShow: DEPTH,
        offset: 1,
        Events: { enable: true, onClick: walkDown, onRightClick: walkUp },
        Node: {
            CanvasStyles: {
                shadowBlur: 7,
                shadowColor: '#000000'
            }
        },
        Tips: { enable: true, offsetX: 15, offsetY: 15,
            onShow: function(tip, node, label) {
                rm.abortTipRequests();

                var markup =
                    '<div class="tip-header">' +
                        '<img class="tip-spinner" src="/images/tip-loader.gif"/>' +
                         '<span class="tip-title">' + node.name + '</span>' +
                    '</div>' +
                    '<div class="tip-image"/><div class="tip-text"/>';
                jQuery(tip).html(markup);

                var requestsCount = 2;
                var requestFinished = function() {
                    if (--requestsCount == 0) {
                        jQuery('.tip-spinner', tip).hide();
                    }
                }

                var url = treemapApiUrl + 'image=' + node.data.title;
                rm.tipImageRequest = jQuery.ajax(encodeURI(url)).always(requestFinished).done(function(response) {
                    var dim = (response.width > response.height ? 'width' : 'height');
                    jQuery('.tip-image', tip).html(imageTag(response.src, dim, 200));
                });

                url = wikipediaApiUrl + extractParams(node.data.title, EXTRACT_LEN);
                rm.tipTextRequest = jQuery.getJSON(encodeURI(url)).always(requestFinished).done(function(response) {
                    if (typeof response.query != 'undefined' && typeof response.query.pages != 'undefined') {
                        var pages = response.query.pages;
                        for (pageId in pages) {
                            if (pages.hasOwnProperty(pageId)) {
                                var text = pages[pageId].extract;
                                jQuery('.tip-text', tip).html(text);
                                break;
                            }
                        }
                    }
                });
            }
        },
        onCreateLabel: function(domElement, node) {
            // Exact match dialog setup.
            var exactMatch = (current.type == 'select' && current.value.toLowerCase() == node.name.toLowerCase() &&
                node.data.type == 'article');
            if (exactMatch && (exactNode == null || exactNode.data.counter < node.data.counter)) {
                exactNode = node;
            }

            var nodeWidth = node.getData('width');
            var $nodeTitle = jQuery('<span class="' + node.data.type + '-node"/>');

            if (!tm.leaf(node)) {
                $nodeTitle.prop('style', 'display: none;').appendTo('body');

                (function fitTitle() {
                    var moreTail = (node.data.hasMoreChildren ? ' (+)' : '');
                    var words = node.name.split(' ');

                    var title = '';
                    var i = 0;
                    for (; i < words.length; ++i) {
                        var word = (i > 0 ? ' ' : '') + words[i];
                        $nodeTitle.text(title + word + (i < words.length - 1 ? '…' : '') + moreTail);
                        if ($nodeTitle.width() > nodeWidth) break;
                        title += word;
                    }

                    $nodeTitle.text(title + (i < words.length ? '…' : '') + moreTail);
                })();

                var titleWidth = $nodeTitle.width();
                $nodeTitle.detach();
            } else {
                $nodeTitle.text(node.name);
            }

            if (exactMatch) {
                jQuery(domElement).addClass('exact-node');
                jQuery('<div/>').append($nodeTitle).appendTo(domElement);
            } else {
                function getContrast(rgb) {
                    var r = rgb[0], g = rgb[1], b = rgb[2];
                    var y = r * 0.299 + g * 0.587 + b * 0.114;   // YIQ
//                    var l = (Math.max.apply(Math, rgb) + Math.min.apply(Math, rgb)) / 2;  // HSL
//                    var i = (r + g + b) / 3;  // HSI
                    return (y >= 160) ? 'black' : 'white';
                }
                var bgColor = node.getData('color');
                var fgColor = getContrast($jit.util.hexToRgb(bgColor));
                jQuery('<div/>').append($nodeTitle.css('color', fgColor)).appendTo(domElement);
            }

            var lowerLimit = 70;
            // At this point $nodeTitle.height() is not yet calculated (thus, returns 0), so I am using titleHeight,
            // even though it is not fully correct, because title can span several lines.
            // Reason: domElement is not yet inserted into the DOM tree, this happens after this function returns,
            // unfortunately. This is why I put these calculations into success-handler of the AJAX-call.
            var nodeWidth = node.getData('width');
            var nodeHeight = node.getData('height') - this.titleHeight;
            var nodeAspect = nodeWidth / nodeHeight;
            var dim = (nodeAspect > 1 ?
                {name: 'width', value: nodeWidth} :
                {name: 'height', value: nodeHeight});
            if (node.data.type == 'article' && dim.value >= lowerLimit) {
                var url = treemapApiUrl + 'image=' + node.data.title;

                var $preview = jQuery('<div class="image-preview"/>').appendTo(domElement)
                    .html('<img class="preview-spinner" src="/images/tip-loader.gif"/>');

                var imageRequest = jQuery.ajax(encodeURI(url)).done(function(response) {
                    var nodeWidth = node.getData('width');
                    var nodeHeight = node.getData('height') - $nodeTitle.height();
                    var nodeAspect = nodeWidth / nodeHeight;
                    var imageAspect = response.width / response.height;
                    var dim = (imageAspect > nodeAspect ?
                        {name: 'width', value: nodeWidth} :
                        {name: 'height', value: nodeHeight});
                    if (dim.value >= lowerLimit) {
                        $preview.html(imageTag(response.src, dim.name, dim.value));
                    } else {
                        $preview.empty();
                    }
                }).fail(function() {$preview.empty();});
                rm.nodeImageRequests.push(imageRequest);
            }
        }
    });

    $goToRoot.html(GO_TO_ROOT + ': ' + ROOT_CATEGORY)
        .click(function() {
            path = [];
            update(tm, {type: 'category', value: ROOT_CATEGORY});
        });
    $goUp.html(GO_UP).click(walkUp);


    var doSearch = function(query) {
        query = query.trim();
        $query.val(query);
        if (query) update(tm, {type: 'select', value: query}, downThePath);
    }
//    var opened = false;
    var $query = jQuery('.query').prop('placeholder', QUERY_PLACEHOLDER)
        .keydown(function(e) {
            switch (e.which) {
//            case 27:
//                if (opened) {
//                    jQuery(this).autocomplete('close');
//                    e.preventDefault();
//                }
//                break;
            case 13:
//                if (!opened) {
                    doSearch(jQuery(this).val());
//                }
                break;
            }
        });//.autocomplete({
//            autoFocus: true,
//            source: function(request, callback) {
//                var url = wikipediaApiUrl + autocompleteParams(request.term);
//                jQuery.getJSON(encodeURI(url))
//                    .done(function(response) {callback(response[1]);})
//                    .fail(function() {callback([]);});
//            },
//            open: function() {opened = true;},
//            close: function() {opened = false;},
//            select: function(e, ui) {doSearch(ui.item.value);}
//        });
//    jQuery('#above').autocomplete('option', 'position', { my : 'right top', at: 'right bottom', collision: 'flip' });
//    jQuery('#below').autocomplete('option', 'position', { my : 'right bottom', at: 'right top', collision: 'flip' });

    jQuery('.search').each(function(i, domElement) {
        jQuery(domElement).click($query[i], function(event) {doSearch(jQuery(event.data).val());});
    });

    var $exactDialog = jQuery('#exact-match').dialog({
        autoOpen: false,
        modal: true,
        width: 600
    });

    QUERY = QUERY.trim();
    if (QUERY) {
        $query.val(QUERY);
        update(tm, {type: 'select', value: QUERY});
    } else {
        update(tm, {type: 'category', value: INITIAL_CATEGORY});
    }
}
