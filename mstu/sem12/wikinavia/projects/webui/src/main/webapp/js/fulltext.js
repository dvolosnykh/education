var autocompleteParams = jQuery.validator.format('action=opensearch&search={0}&namespace=0&limit=15&format=json');
var queryParams = jQuery.validator.format('action=query&list=search&srsearch={0}&srlimit={1}&srprop=snippet|titlesnippet&format=json');
var resultTemplate = jQuery.validator.format(
    '<li class="doc ui-widget">' +
        '<div class="ui-widget-content ui-corner-all">' +
            '<h2 class="ui-widget-header ui-corner-all"><a href="{0}" title="{1}" target="_blank">{2}</a></h2>' +
            '<p>{3}</p>' +
        '</div>' +
    '</li>');

//var RESULTS_LIMIT = 50;
//var wikipediaApiUrl = WIKIPEDIA_URL + '/w/api.php?callback=?&';
var RESULTS_LIMIT = 500;
var wikipediaApiUrl = WIKINAVIA_API_URL + '/wikipedia?';

function search(query) {
    var $query = jQuery('#query');
    var $noDocs = jQuery('#no-docs');
    var $badQuery = jQuery('#bad-query');
    var $results = jQuery('#results');
    var $spinner = jQuery('.ajax-loader');

    var url = wikipediaApiUrl + queryParams(query, RESULTS_LIMIT);
    jQuery.ajax(encodeURI(url), {
        beforeSend: function() {
            $query.autocomplete('disable');
            $noDocs.hide();
            $badQuery.hide();
            $results.empty();
            $spinner.show();
        },
        complete: function() {
            $spinner.hide();
            $query.autocomplete('close').autocomplete('enable');
        }
    }).done(function(response) {
        if (typeof response.query != 'undefined' && typeof response.query.search != 'undefined') {
            var docs = response.query.search;
            $results.append('<ol id="docs" class="pagination"></ol>');
            var $docs = jQuery('#docs');
            if (docs.length > 0) {
                jQuery.each(docs, function(index, doc) {
                    var pageUrl = WIKIPEDIA_URL + '/wiki/' + doc.title;
                    var $doc = jQuery(resultTemplate(encodeURI(pageUrl), doc.title, doc.titlesnippet || doc.title, doc.snippet));

                    if (typeof TRACK == 'function') {
                        jQuery('a', $doc).click(doc.title, function(event) {
                            TRACK({kind: 'view', subject: event.data});
                        });
                    }

                    $docs.append($doc);
                });

                $docs.quickPager({pageSize: 5, pagerLocation: 'both'});
                $results.show();
            } else {
                $noDocs.show();
            }

            window.history.replaceState(null, null, '?q=' + encodeURI(query));

            if (typeof TRACK == 'function') TRACK({kind: 'query', subject: query});
        } else {
            $badQuery.show();
        }
    });
}

function fulltext_init() {
    var opened = false;

    var doSearch = function(query) {
        query = query.trim();
        $query.val(query);
        if (query) search(query);
    }

    var $query = jQuery('#query').prop('placeholder', QUERY_PLACEHOLDER)
        .keydown(function(e) {
            if (e.which == 27) {
                if (opened) {
                    $query.autocomplete('close');
                    e.preventDefault();
                }
            } else if (e.which == 13) {
                if (!opened) doSearch(jQuery(this).val());
            }
        }).autocomplete({
            autoFocus: true,
            source: function(request, callback) {
                var url = wikipediaApiUrl + autocompleteParams(request.term);
                jQuery.getJSON(encodeURI(url))
                    .done(function(response) {callback(response[1]);})
                    .fail(function() {callback([]);});
            },
            open: function() {opened = true;},
            close: function() {opened = false;},
            select: function(e, ui) {doSearch(ui.item.value);}
        });
    jQuery('#search').click(function () {doSearch($query.val());});

    doSearch(QUERY);
}
