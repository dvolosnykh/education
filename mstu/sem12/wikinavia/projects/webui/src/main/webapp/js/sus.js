function sus_init() {
    function validate() {
        var ok = true;
        jQuery('.method').each(function(index, domMethod) {
            jQuery('.grades', domMethod).each(function(index, domGrade) {
                var $grade = jQuery(':radio:checked', domGrade);
                ok = ok && $grade.length > 0;
            });
        });
        return (ok);
    }

    jQuery('button').click(function() {
        if (validate()) {
            var sus = {};
            jQuery('.method').each(function(index, domMethod) {
                var method = domMethod.dataset['method'];
                sus[method] = jQuery('.grades', domMethod).map(function(index, domGrade) {
                    var $grade = jQuery(':radio:checked', domGrade);
                    return {statementId: +$grade.prop('name').split('_')[1], value: +$grade.val()};
                }).get();
            });

            SEND_SUS(sus);
        } else {
            alert(CONCERN_ALL_STATEMENTS);
        }
    });
}