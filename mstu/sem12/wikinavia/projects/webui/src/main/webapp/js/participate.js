function participate_init(justStarted, firstTask) {
    var result = {};

    if (justStarted && !firstTask) {
        jQuery(function() {
            ON_START(Date.now());
            console.log('Time has been logged.');
        });
    }
    jQuery('#instructions-form').dialog({
        autoOpen: justStarted && firstTask,
        modal: true,
        width: 600,
        buttons: [
            { text: START, click: function(event) {
                jQuery(this).dialog('close');
                ON_START(event.timeStamp);
                console.log('Time has been logged.');
            } }
        ],
        closeOnEscape: false,
        open: function() {jQuery('.ui-dialog-titlebar-close', this.parentNode).hide();}
    });

    jQuery('#done-button').button().click(function(event) {
        result.done = true;
        result.finishTime = event.timeStamp;
        $doneForm.dialog('open');
    });
    jQuery('#giveup-button').button().click(function(event) {
        result.done = false;
        result.finishTime = event.timeStamp;
        ON_FINISH(result);
    });

    var $doneForm = jQuery('#done-form').dialog({
        autoOpen: false,
        modal: true,
        width: 800,
        buttons: [
            { text: NEXT, click: function(event) {
                result.answer = jQuery('#answer').val();
                result.grades = jQuery('tr', this).map(function(index, domElement) {
                    var $grade = jQuery(':radio:checked', domElement);
                    return {questionId: +$grade.prop('name'), value: +$grade.val()};
                }).get();
                result.comment = jQuery('#comment').val();
                ON_FINISH(result);
            } }
        ]
    });
}