var membersParams = jQuery.validator.format(
    'action=query&list=categorymembers&cmtitle={0}&cmtype=subcat|page&cmprop=title&cmlimit=max&format=json'
);

var collapsedSign = '[<b>+</b>]';
var expandedSign = '[<b>-</b>]';
var categoryTemplate = jQuery.validator.format(
    '<li><span class="toggle"/> <a href="{0}" target="_blank">{1}</a><ul class="sublist"/></li>'
);
var articleTemplate = jQuery.validator.format(
    '<li><a href="{0}" target="_blank">{1}</a></li>'
);

var rePrefix = new RegExp('^' + CATEGORY_PREFIX + ':');

//var wikipediaApiUrl = WIKIPEDIA_URL + '/w/api.php?callback=?&';
var wikipediaApiUrl = WIKINAVIA_API_URL + '/wikipedia?';

function texttree_init() {
    function expand(parent) {
        var categoryTitle = CATEGORY_PREFIX + ':' + jQuery(parent).children('li > a').text();
        var $sublist = jQuery('.sublist', parent);

        var url = wikipediaApiUrl + membersParams(categoryTitle);
        jQuery.ajax(encodeURI(url), {
            beforeSend: function() {
                $sublist.html('<li>' + LOADING + '</li>');
            }
        }).done(function(response) {
            if (typeof response.query != 'undefined' && typeof response.query.categorymembers != 'undefined') {
                var members = response.query.categorymembers;

                var categories = [];
                var articles = [];
                jQuery.each(members, function(index, member) {
                    var memberUrl = WIKIPEDIA_URL + '/wiki/' + member.title;

                    if (member.ns == 14) {
                        var category = jQuery(categoryTemplate(memberUrl, member.title.replace(rePrefix, '')))[0];
                        categories.push(category);

                        jQuery(category).children('li > .toggle')
                            .attr('title', EXPAND)
                            .html(collapsedSign)
                            .click(function() {expand(category);});

                        if (typeof TRACK == 'function') {
                            jQuery(category).children('li > a').click(member.title, function(event) {
                                TRACK({kind: 'view', subject: event.data});
                            });
                        }
                    } else if (member.ns == 0) {
                        var article = jQuery(articleTemplate(memberUrl, member.title))[0];
                        articles.push(article);

                        if (typeof TRACK == 'function') {
                            jQuery(article).children('li > a').click(member.title, function(event) {
                                TRACK({kind: 'view', subject: event.data});
                            });
                        }
                    }
                });

                $sublist.empty();
                jQuery(categories).appendTo($sublist);
                jQuery(articles).appendTo($sublist);

                jQuery(parent).children('li > .toggle')
                    .attr('title', COLLAPSE)
                    .html(expandedSign)
                    .off('click').click(function() {collapse(parent);});

                if (typeof TRACK == 'function') TRACK({kind: 'expand', subject: categoryTitle});
            } else {
                $sublist.html('<li>' + FAILED + '</li>');
            }
        }).fail(function() {
            $sublist.html('<li>' + FAILED + '</li>');
        });
    }

    function collapse(parent) {
        var categoryTitle = CATEGORY_PREFIX + ':' + jQuery(parent).children('li > a').text();
        var $sublist = jQuery('.sublist', parent);

        $sublist.empty();
        jQuery(parent).children('li > .toggle')
            .attr('title', EXPAND)
            .html(collapsedSign)
            .off('click').click(function() {expand(parent);});

        if (typeof TRACK == 'function') TRACK({kind: 'collapse', subject: categoryTitle});
    }

    var topUrl = WIKIPEDIA_URL + '/wiki/' + CATEGORY_PREFIX + ':' + ROOT_CATEGORY;
    var topCategory = jQuery(categoryTemplate(topUrl, ROOT_CATEGORY))[0];
    jQuery('#texttree').html(jQuery('<ul></ul>').html(topCategory));

    jQuery('.toggle', topCategory)
        .attr('title', EXPAND)
        .html(collapsedSign)
        .click(function() {expand(topCategory)}).click();
}