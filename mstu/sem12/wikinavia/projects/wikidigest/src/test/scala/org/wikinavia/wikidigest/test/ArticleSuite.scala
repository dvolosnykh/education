package org.wikinavia.wikidigest.test

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 4/2/12
 * Time: 14:01 AM
 * To change this template use File | Settings | File Templates.
 */

final class ArticleSuite extends ParseSuite {
  tryParse("Article starting with special block.") {
    """|== h2 ==
       |=== h3 ===
       |==== h4 ====
       |Why need so many headers?
       |===== h5 =====
       |====== h6 ======
       |----
       |Huh?""".stripMargin
  } {
    Page(None,
      Heading(2, " h2 ") ::
        Heading(3, " h3 ") ::
        Heading(4, " h4 ") ::
        Paragraph(Text("Why need so many headers?")) ::
        Heading(5, " h5 ") ::
        Heading(6, " h6 ") ::
        HorizontalRule("") ::
        Paragraph(Text("Huh?")) ::
        List.empty
    )
  }

  tryParse("Article starting with paragraph.") {
    """|Some preamble
       |== The very important info ==
       |Let's read some conversation:
       |- Hi, man!
       |- Hi, dude!
       |---- This text is of no use ----""".stripMargin
  } {
    Page(None,
      Paragraph(Text("Some preamble")) ::
        Heading(2, " The very important info ") ::
        Paragraph(
          Text("Let's read some conversation:"),
          Text("- Hi, man!"),
          Text("- Hi, dude!")
        ) ::
        HorizontalRule(" This text is of no use ----") ::
        List.empty
    )
  }
}
