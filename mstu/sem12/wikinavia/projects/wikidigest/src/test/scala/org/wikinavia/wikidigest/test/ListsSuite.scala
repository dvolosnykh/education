package org.wikinavia.wikidigest.test

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 4/2/12
 * Time: 14:01 AM
 * To change this template use File | Settings | File Templates.
 */

final class ListsSuite extends ParseSuite {
  tryParse("Plain enumerated list.") {
    """|#one
       |#two
       |#three""".stripMargin
  } {
    Page(None,
      EnumeratedList(
        ListItem(Text("one") :: Nil, NoList),
        ListItem(Text("two") :: Nil, NoList),
        ListItem(Text("three") :: Nil, NoList)
      ) :: List.empty
    )
  }

  tryParse("Plain bulletted list.") {
    """|*one
       |*two
       |*three""".stripMargin
  } {
    Page(None,
      BullettedList(
        ListItem(Text("one") :: Nil, NoList),
        ListItem(Text("two") :: Nil, NoList),
        ListItem(Text("three") :: Nil, NoList)
      ) :: List.empty
    )
  }

  tryParse("Plain indented list.") {
    """|:one
       |:two
       |:three""".stripMargin
  } {
    Page(None,
      IndentedList(
        ListItem(Text("one") :: Nil, NoList),
        ListItem(Text("two") :: Nil, NoList),
        ListItem(Text("three") :: Nil, NoList)
      ) :: List.empty
    )
  }

  tryParse("Multilevel enumerated list.") {
    """|#1
       |##1.1
       |###1.1.1
       |##1.2
       |###1.2.1
       |#2
       |###2.1.1""".stripMargin
  } {
    Page(None,
      EnumeratedList(
        ListItem(Text("1") :: Nil, EnumeratedList(
          ListItem(Text("1.1") :: Nil, EnumeratedList(
            ListItem(Text("1.1.1") :: Nil, NoList)
          )),
          ListItem(Text("1.2") :: Nil, EnumeratedList(
            ListItem(Text("1.2.1") :: Nil, NoList)
          ))
        )),
        ListItem(Text("2") :: Nil, EnumeratedList(
          ListItem(Nil, EnumeratedList(
            ListItem(Text("2.1.1") :: Nil, NoList)
          ))
        ))
      ) :: List.empty
    )
  }

  tryParse("Multilevel bulletted list.") {
    """|*1
       |**1.1
       |***1.1.1
       |**1.2
       |***1.2.1
       |*2
       |***2.1.1""".stripMargin
  } {
    Page(None,
      BullettedList(
        ListItem(Text("1") :: Nil, BullettedList(
          ListItem(Text("1.1") :: Nil, BullettedList(
            ListItem(Text("1.1.1") :: Nil, NoList)
          )),
          ListItem(Text("1.2") :: Nil, BullettedList(
            ListItem(Text("1.2.1") :: Nil, NoList)
          ))
        )),
        ListItem(Text("2") :: Nil, BullettedList(
          ListItem(Nil, BullettedList(
            ListItem(Text("2.1.1") :: Nil, NoList)
          ))
        ))
      ) :: List.empty
    )
  }

  tryParse("Multilevel indented list.") {
    """|:1
       |::1.1
       |:::1.1.1
       |::1.2
       |:::1.2.1
       |:2
       |:::2.1.1""".stripMargin
  } {
    Page(None,
      IndentedList(
        ListItem(Text("1") :: Nil, IndentedList(
          ListItem(Text("1.1") :: Nil, IndentedList(
            ListItem(Text("1.1.1") :: Nil, NoList)
          )),
          ListItem(Text("1.2") :: Nil, IndentedList(
            ListItem(Text("1.2.1") :: Nil, NoList)
          ))
        )),
        ListItem(Text("2") :: Nil, IndentedList(
          ListItem(Nil, IndentedList(
            ListItem(Text("2.1.1") :: Nil, NoList)
          ))
        ))
      ) :: List.empty
    )
  }

  tryParse("Definition list.") {
    """|;item 1
       |: definition 1
       |;item 2
       |: definition 2-1
       |: definition 2-2""".stripMargin
  } {
    Page(None,
      DefinitionList(
        TermDefinition("item 1",
          ListItem(Text(" definition 1") :: Nil, NoList)
        ),
        TermDefinition("item 2",
          ListItem(Text(" definition 2-1") :: Nil, NoList),
          ListItem(Text(" definition 2-2") :: Nil, NoList)
        )
      ) :: List.empty
    )
  }

  tryParse("Inlined definition list.") {
    """|;Semicolon at line begining: and following
       |: colon
       |;make: 2-level deep list.""".stripMargin
  } {
    Page(None,
      DefinitionList(
        TermDefinition("Semicolon at line begining",
          ListItem(Text(" and following") :: Nil, NoList),
          ListItem(Text(" colon") :: Nil, NoList)
        ),
        TermDefinition("make",
          ListItem(Text(" 2-level deep list.") :: Nil, NoList)
        )
      ) :: List.empty
    )
  }

  tryParse("Multilevel mixture of different types of list. No definitions.") {
    """|#one
       |#*two
       |#*:three
       |#one
       |
       |*one
       |*:#two""".stripMargin
  } {
    Page(None,
      EnumeratedList(
        ListItem(Text("one") :: Nil, BullettedList(
          ListItem(Text("two") :: Nil, IndentedList(
            ListItem(Text("three") :: Nil, NoList)
          ))
        )),
        ListItem(Text("one") :: Nil, NoList)
      ) :: BullettedList(
        ListItem(Text("one") :: Nil, IndentedList(
          ListItem(Nil, EnumeratedList(
            ListItem(Text("two") :: Nil, NoList)
          ))
        ))
      ) :: List.empty
    )
  }

  tryParse("Multilevel mixture of different types of list. With definitions.") {
    """|# one
       |# two
       |#* two point one
       |#* two point two
       |# three
       |#; three item one
       |#: three def one
       |# four
       |#: four def one
       |#: this looks like a continuation
       |#: and is often used
       |#: instead<br/>of <nowiki><br/></nowiki>
       |# five
       |## five sub 1
       |### five sub 1 sub 1
       |## five sub 2""".stripMargin
  } {
    Page(None,
      EnumeratedList(
        ListItem(Text(" one") :: Nil, NoList),
        ListItem(Text(" two") :: Nil, BullettedList(
          ListItem(Text(" two point one") :: Nil, NoList),
          ListItem(Text(" two point two") :: Nil, NoList)
        )),
        ListItem(Text(" three") :: Nil, DefinitionList(
          TermDefinition(" three item one",
            ListItem(Text(" three def one") :: Nil, NoList)
          )
        )),
        ListItem(Text(" four") :: Nil, IndentedList(
          ListItem(Text(" four def one") :: Nil, NoList),
          ListItem(Text(" this looks like a continuation") :: Nil, NoList),
          ListItem(Text(" and is often used") :: Nil, NoList),
          ListItem(Text(" instead<br/>of <nowiki><br/></nowiki>") :: Nil, NoList)
        )),
        ListItem(Text(" five") :: Nil, EnumeratedList(
          ListItem(Text(" five sub 1") :: Nil, EnumeratedList(
            ListItem(Text(" five sub 1 sub 1") :: Nil, NoList)
          )),
          ListItem(Text(" five sub 2") :: Nil, NoList)
        ))
      ) :: List.empty
    )
  }

  tryParse("Definition list of terms having multiple definitions.") {
    """|;item 1
       |:* definition 1-1
       |:* definition 1-2
       |:
       |;item 2
       |:# definition 2-1
       |:# definition 2-2""".stripMargin
  } {
    Page(None,
      DefinitionList(
        TermDefinition("item 1",
          ListItem(Nil, BullettedList(
            ListItem(Text(" definition 1-1") :: Nil, NoList),
            ListItem(Text(" definition 1-2") :: Nil, NoList)
          )),
          ListItem(Nil, NoList)
        ),
        TermDefinition("item 2",
          ListItem(Nil, EnumeratedList(
            ListItem(Text(" definition 2-1") :: Nil, NoList),
            ListItem(Text(" definition 2-2") :: Nil, NoList)
          ))
        )
      ) :: List.empty
    )
  }
}
