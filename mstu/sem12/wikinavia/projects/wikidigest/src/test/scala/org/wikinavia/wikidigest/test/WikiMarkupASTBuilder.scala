package org.wikinavia.wikidigest
package test

import javax.swing.text.html.parser.Entity

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 3/30/12
 * Time: 10:37 PM
 * To change this template use File | Settings | File Templates.
 */

sealed abstract class MarkupEntity

abstract class Block extends MarkupEntity

abstract class InlineElement extends MarkupEntity

abstract class Link extends MarkupEntity

abstract class SpecialBlock extends Block

case class ArticleLink(interwiki: String, namespace: String, path: String, title: String) extends Link

case class Redirect(link: MarkupEntity) extends MarkupEntity

case class HorizontalRule(text: String) extends SpecialBlock

case class Heading(level: Int, text: String) extends SpecialBlock

case class SpaceBlock(lines: List[MarkupEntity]*) extends SpecialBlock

case class ListItem(item: List[MarkupEntity], sublist: MarkupEntity*) extends MarkupEntity

case object NoList extends SpecialBlock

case class EnumeratedList(items: MarkupEntity*) extends SpecialBlock

case class BullettedList(items: MarkupEntity*) extends SpecialBlock

case class IndentedList(items: MarkupEntity*) extends SpecialBlock

case class DefinitionList(items: MarkupEntity*) extends SpecialBlock

case class TermDefinition(term: String, definition: MarkupEntity*) extends MarkupEntity

case class Text(content: String) extends InlineElement

case class Paragraph(lines: MarkupEntity*) extends Block

case class Page(redirect: Option[MarkupEntity], article: List[MarkupEntity]) extends MarkupEntity


final class WikiMarkupASTBuilder extends WikiMarkupListener with WikiMarkupParser {
  type Entity = MarkupEntity

  @inline protected def onPage(link: Option[Entity], article: List[Entity]) = Page(link map Redirect, article)

  @inline protected def onArticleLink(interwiki: String, namespace: String, path: String, title: String) =
    ArticleLink(interwiki, namespace, path, title)

  @inline protected def onHorizontalRule(text: String) = HorizontalRule(text)

  @inline protected def onHeading(level: Int, text: String) = Heading(level, text)

  @inline protected def onSpaceBlock(lines: List[List[Entity]]) = SpaceBlock(lines: _*)

  @inline protected def onEnumeratedList(items: List[Entity]) = EnumeratedList(items: _*)

  @inline protected def onBullettedList(items: List[Entity]) = BullettedList(items: _*)

  @inline protected def onIndentedList(items: List[Entity]) = IndentedList(items: _*)

  @inline protected def onDefinitionList(items: List[Entity]) = DefinitionList(items: _*)

  @inline protected def onListItem(item: List[Entity], sublist: Option[Entity]) =
    ListItem(item, sublist.getOrElse(NoList))

  @inline protected def onTermDefinition(term: String, definition: List[Entity]) = TermDefinition(term, definition: _*)

  @inline protected def onParagraph(lines: List[Entity]) = Paragraph(lines: _*)

  @inline protected def onText(content: String) = Text(content)
}
