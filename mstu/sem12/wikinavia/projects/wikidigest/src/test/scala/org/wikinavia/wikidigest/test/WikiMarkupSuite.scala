package org.wikinavia.wikidigest.test

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 3/28/12
 * Time: 11:49 AM
 * To change this template use File | Settings | File Templates.
 */

import org.scalatest.Suites

final class WikiMarkupSuite extends Suites(
  new RedirectSuite,
  new HorizontalRulesSuite,
  new HeadingsSuite,
  new SpaceBlocksSuite,
  new ListsSuite,
  new ArticleSuite
)
