package org.wikinavia.wikidigest.test

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 4/2/12
 * Time: 14:01 AM
 * To change this template use File | Settings | File Templates.
 */

final class RedirectSuite extends ParseSuite {
  tryParse("Redirect link is terminated by pipe. Article follows.") {
    """#REDireCTnon%^sense[[foo|and this is parsed as article content"""
  } {
    Page(
      Some(Redirect(ArticleLink("", "", "", "foo"))),
      Paragraph(Text("and this is parsed as article content")) :: List.empty
    )
  }

  tryParse("Redirect link is terminated by ']]'.") {
    """#redirect[[Article Title]]"""
  } {
    Page(Some(Redirect(ArticleLink("", "", "", "Article Title"))), List.empty)
  }

  tryParse("Redirect link is terminated by EOF.") {
    """#redirect[[Article Title"""
  } {
    Page(Some(Redirect(ArticleLink("", "", "", "Article Title"))), List.empty)
  }
}
