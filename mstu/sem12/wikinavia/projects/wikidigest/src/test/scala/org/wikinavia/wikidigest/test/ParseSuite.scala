package org.wikinavia.wikidigest.test

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 4/2/12
 * Time: 14:01 AM
 * To change this template use File | Settings | File Templates.
 */

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

class ParseSuite extends FunSuite with ShouldMatchers {
  private val builder = new WikiMarkupASTBuilder

  protected def tryParse(name: String)(markup: => String)(ast: => Page) {
    test(name) {
      val result = builder.parseAll(builder.page, markup)
      result should be('successful)
      result.get should equal(ast)
    }
  }
}
