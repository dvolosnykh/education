package org.wikinavia.wikidigest.test

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 4/2/12
 * Time: 14:01 AM
 * To change this template use File | Settings | File Templates.
 */

final class HeadingsSuite extends ParseSuite {
  tryParse("Rules are balanced.") {
    (1 to 7).map {
      level => List.fill(2)(rule(level)) mkString title(level)
    } mkString "\n"
  } {
    Page(None,
      (1 to 6).map {
        level => Heading(level, title(level))
      }.toList :::
        Heading(6, "=" + title(7) + "=") ::
        List.empty
    )
  }

  tryParse("Left rule is shorter.") {
    (1 to 7).map {
      level => List(rule(level - 1), rule(level)) mkString title(level)
    } mkString "\n"
  } {
    Page(None,
      SpaceBlock(
        Text("h1 =") :: Nil
      ) :: (2 to 6).map {
        level => Heading(level - 1, title(level) + "=")
      }.toList :::
        Heading(6, title(7) + "=") ::
        List.empty
    )
  }

  tryParse("Right rule is shorter.") {
    (1 to 7).map {
      level => List(rule(level), rule(level - 1)) mkString title(level)
    } mkString "\n"
  } {
    Page(None,
      Paragraph(Text("= h1 ")) ::
        (2 to 6).map {
          level => Heading(level - 1, "=" + title(level))
        }.toList :::
        Heading(6, "=" + title(7)) ::
        List.empty
    )
  }

  tryParse("Title contains same-length rule.") {
    (1 to 7).map {
      level => rule(level) + title(level) + rule(level) + title(level) + rule(level)
    } mkString "\n"
  } {
    Page(None,
      (1 to 6).map {
        level => Heading(level, title(level) + rule(level) + title(level))
      }.toList :::
        Heading(6, "=" + title(7) + rule(7) + title(7) + "=") ::
        List.empty
    )
  }

  tryParse("Title contains longer rule.") {
    (1 to 7).map {
      level => rule(level) + title(level) + rule(level + 1) + title(level) + rule(level)
    } mkString "\n"
  } {
    Page(None,
      (1 to 6).map {
        level => Heading(level, title(level) + rule(level + 1) + title(level))
      }.toList :::
        Heading(6, "=" + title(7) + rule(8) + title(7) + "=") ::
        List.empty
    )
  }

  private def title(level: Int) = " h" + level + " "

  private def rule(level: Int) = "=" * level
}
