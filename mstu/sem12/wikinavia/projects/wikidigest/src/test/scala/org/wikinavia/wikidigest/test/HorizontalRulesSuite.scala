package org.wikinavia.wikidigest.test

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 4/2/12
 * Time: 14:01 AM
 * To change this template use File | Settings | File Templates.
 */

import annotation.switch

final class HorizontalRulesSuite extends ParseSuite {
  tryParse("Horizontal rules.") {
    (0 to 7).map {
      count =>
        val description = " " + count + " dashes -> " + ((count: @switch) match {
          case 0 => "space block"
          case 1 | 2 | 3 => "text"
          case _ => "horizontal rule"
        })
        rule(count) + description
    } mkString "\n"
  } {
    Page(None,
      SpaceBlock(
        Text("0 dashes -> space block") :: Nil
      ):: Paragraph(
        (1 to 3).map {
          count => Text("-" * count + " " + count + " dashes -> text")
        }: _*
      ) ::
        (4 to 7).map {
          count => HorizontalRule(" " + count + " dashes -> horizontal rule")
        }.toList
    )
  }

  private def rule(count: Int) = "-" * count
}
