package org.wikinavia.wikidigest.test

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 4/2/12
 * Time: 14:01 AM
 * To change this template use File | Settings | File Templates.
 */

final class SpaceBlocksSuite extends ParseSuite {
  tryParse("Space block one-liner.") {
    " Some preformatted text."
  } {
    Page(None,
      SpaceBlock(
        Text("Some preformatted text.") :: Nil
      ) :: List.empty
    )
  }

  tryParse("Multiline space block.") {
    """| Line one
       | Line two
       | Line three""".stripMargin
  } {
    Page(None,
      SpaceBlock(
        Text("Line one") :: Nil,
        Text("Line two") :: Nil,
        Text("Line three") :: Nil
      ) :: List.empty
    )
  }

  tryParse("Several space blocks.") {
    """| This is
       | the first
       | space block
       |""".stripMargin +
      " \n" +
      """| And this is
   | the second one""".stripMargin
  } {
    Page(None,
      SpaceBlock(
        Text("This is") :: Nil,
        Text("the first") :: Nil,
        Text("space block") :: Nil,
        Nil,
        Text("And this is") :: Nil,
        Text("the second one") :: Nil
      ) :: List.empty
    )
  }
}
