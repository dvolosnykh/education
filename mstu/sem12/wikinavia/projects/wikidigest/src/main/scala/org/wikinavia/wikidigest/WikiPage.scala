package org.wikinavia.wikidigest

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 3/25/12
 * Time: 10:25 AM
 * To change this template use File | Settings | File Templates.
 */

class WikiPage {
  var title = ""
  var namespace = Int.MinValue
  var id: Option[Int] = None
  var isRedirect = false
  var text: Option[String] = None

  def reset() {
    title = ""
    namespace = Int.MinValue
    id = None
    isRedirect = false
    text = None
  }
}
