package org.wikinavia.wikidigest

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 3/30/12
 * Time: 7:07 PM
 * To change this template use File | Settings | File Templates.
 */

object RegexUtils {
  def not(s: String) = "[^" + s + "]"

  implicit def string2re(s: String) = new RegexUtils(s)
}

final class RegexUtils(private val s: String) {
  import RegexUtils._

  def fix = "[" + s + "]"

  def rep = s + "*"

  def rep1 = s + "+"

  def &(right: String) = s + "&&" + right

  def \ = "[^" + s + "]"

  def &\(right: String) = s & (right.\)

  def escape = s.foldLeft("")(_ + "\\" + _)

  def untilNearest(suffix: String) = (s + "?(?=" + suffix + ")").r

  def untilFarthest(suffix: String) = (s + "(?=" + suffix + ")").r
}

