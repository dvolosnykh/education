package org.wikinavia.wikidigest

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 3/26/12
 * Time: 1:29 AM
 * To change this template use File | Settings | File Templates.
 */

object WikiNamespace {
  private val canonical = Map(
    -2 -> "Media",
    -1 -> "Special",
    0 -> "",
    1 -> "Talk",
    2 -> "User",
    3 -> "User talk",
    4 -> "Project",
    5 -> "Project talk",
    6 -> "File",
    7 -> "File talk",
    8 -> "MediaWiki",
    9 -> "MediaWiki talk",
    10 -> "Template",
    11 -> "Template talk",
    12 -> "Help",
    13 -> "Help talk",
    14 -> "Category",
    15 -> "Category talk",
    100 -> "Портал",
    101 -> "Обсуждение портала",
    102 -> "Инкубатор",
    103 -> "Обсуждение инкубатора",
    104 -> "Проект",
    105 -> "Обсуждение проекта"
  )
  private val synonyms = Map(
    "Медиа" -> -2,
    "Служебная" -> -1,
    "Обсуждение" -> 1,
    "Участник" -> 2, "Участница" -> 2,
    "Обсуждение участника" -> 3, "Обсуждение участницы" -> 3,
    "Википедия" -> 4, "ВП" -> 4,
    "Обсуждение Википедии" -> 5,
    "Файл" -> 6, "Image" -> 6, "Изображение" -> 6,
    "Обсуждение файла" -> 7, "Image talk" -> 7, "Обсуждение изображения" -> 7,
    "Обсуждение MediaWiki" -> 9,
    "Шаблон" -> 10,
    "Обсуждение шаблона" -> 11,
    "Справка" -> 12,
    "Обсуждение справки" -> 13,
    "Категория" -> 14,
    "Обсуждение категории" -> 15,
    "Incubator" -> 102, "И" -> 102,
    "Incubator talk" -> 103,
    "ПРО" -> 104, "Wikiproject" -> 104,
    "Wikiproject talk" -> 105
  )
  private val id = canonical.map(_.swap) ++ synonyms

  def apply(id: Int) = canonical(id)
  def apply(name: String) = id(name)
}
