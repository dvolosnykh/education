package org.wikinavia.wikidigest

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 3/26/12
 * Time: 1:55 AM
 * To change this template use File | Settings | File Templates.
 */

import org.xml.sax.helpers.DefaultHandler
import org.xml.sax.Attributes


final class WikiXMLHandler(private val processor: WikiPageProcessor) extends DefaultHandler {
  private val text = new StringBuilder
  private val page = new WikiPage
  private var inRevision = false

  override def startDocument() {
    processor.initialize()
  }

  override def endDocument() {
    processor.deinitialize()
  }

  override def startElement(uri: String, localName: String, qname: String, attributes: Attributes) {
    qname match {
      case WikiXMLTag.REVISION => inRevision = true
      case _ =>
    }
    text.clear()
  }

  override def endElement(uri: String , localName: String, qname: String) {
    qname match {
      case WikiXMLTag.TITLE => page.title = consumeText
      case WikiXMLTag.ID => if (!inRevision) page.id = Some(consumeText.toInt)
      case WikiXMLTag.NAMESPACE => page.namespace = consumeText.toInt
      case WikiXMLTag.REDIRECT => page.isRedirect = true
      case WikiXMLTag.TEXT => page.text = Some(consumeText)
      case WikiXMLTag.REVISION => inRevision = false
      case WikiXMLTag.PAGE =>
        processor process page
        page.reset()
      case _ =>
    }
  }

  override def characters(chars: Array[Char], start: Int, length: Int) {
    text.appendAll(chars, start, length)
  }

  override def ignorableWhitespace(chars: Array[Char], start: Int, length: Int) {
    text.appendAll(chars, start, length)
  }

  private def consumeText: String = {
    val t = text.result
    text.clear()
    t
  }


  private object WikiXMLTag {
    val PAGE = "page"
    val ID = "id"
    val REDIRECT = "redirect"
    val NAMESPACE = "ns"
    val REVISION = "revision"
    val TITLE = "title"
    val TEXT = "text"
  }
}