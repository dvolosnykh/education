package org.wikinavia.wikidigest

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 3/18/12
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */

import java.util.zip.GZIPInputStream
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream
import org.xml.sax.InputSource
import org.xml.sax.helpers.XMLReaderFactory
import java.io.{OutputStream, FileOutputStream, InputStream, FileInputStream}

object WikiDumpParser extends App {
  private val source = args(0)
  private val destination = args(1)

  try {
    translate(source, destination)
  } catch {
    case e =>
      Console println e
      Console println e.getStackTraceString
  }

  private def translate(source: String, destination: String) {
    val input = openDump(source)
    val output = new FileOutputStream(destination)
    translate(input, output)
    output.close()
    input.close()
  }

  private def translate(input: InputStream, output: OutputStream) {
    val handler = new WikiXMLHandler(new WikiPageProcessor(output))
    val reader = XMLReaderFactory.createXMLReader()
    reader.setContentHandler(handler)
    reader parse new InputSource(input)
  }

  private def openDump(filename: String): InputStream = {
    val input = new FileInputStream(filename)
    if (filename endsWith ".bz2") new BZip2CompressorInputStream(input)
    else if (filename endsWith ".gz") new GZIPInputStream(input)
    else input
  }
}

