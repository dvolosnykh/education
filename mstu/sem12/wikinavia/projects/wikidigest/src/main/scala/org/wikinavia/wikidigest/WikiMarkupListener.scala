package org.wikinavia.wikidigest

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 3/30/12
 * Time: 10:00 PM
 * To change this template use File | Settings | File Templates.
 */

abstract class WikiMarkupListener {
  type Entity

  protected def onPage(redirect: Option[Entity], article: List[Entity]): Entity

  protected def onArticleLink(interwiki: String, namespace: String, path: String, title: String): Entity

  protected def onHorizontalRule(text: String): Entity

  protected def onHeading(level: Int, text: String): Entity

  protected def onSpaceBlock(lines: List[List[Entity]]): Entity

  protected def onEnumeratedList(items: List[Entity]): Entity

  protected def onBullettedList(items: List[Entity]): Entity

  protected def onIndentedList(items: List[Entity]): Entity

  protected def onDefinitionList(items: List[Entity]): Entity

  protected def onListItem(item: List[Entity], sublist: Option[Entity]): Entity

  protected def onTermDefinition(term: String, definition: List[Entity]): Entity

  protected def onParagraph(lines: List[Entity]): Entity

  protected def onText(text: String): Entity
}
