package org.wikinavia.wikidigest

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 3/25/12
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */

import javax.xml.stream.{XMLEventFactory, XMLOutputFactory}
import java.io.{OutputStream, BufferedWriter, OutputStreamWriter}
import java.util.Arrays


final class WikiPageProcessor(private val output: OutputStream) {
  private val CHARSET = "UTF-8"
  private val VERSION = "1.0"
  private val PRINT_RATE = 10000

  private val outputFactory = XMLOutputFactory.newFactory()
  private val eventFactory = XMLEventFactory.newFactory()
  private val buffer = new BufferedWriter(new OutputStreamWriter(output, CHARSET))
  private val writer = outputFactory.createXMLEventWriter(buffer)
  private val docStart = eventFactory.createStartElement("", "", "doc")
  private val docEnd = eventFactory.createEndElement("", "", "doc")
  private val namespaces = Arrays.asList().iterator
  private val fieldStart_id = eventFactory.createStartElement("", "", "field",
    Arrays.asList(eventFactory.createAttribute("name", "id")).iterator, namespaces)
  private val fieldStart_title = eventFactory.createStartElement("", "", "field",
    Arrays.asList(eventFactory.createAttribute("name", "title")).iterator, namespaces)
  private val fieldStart_text = eventFactory.createStartElement("", "", "field",
    Arrays.asList(eventFactory.createAttribute("name", "text")).iterator, namespaces)
  private val fieldEnd = eventFactory.createEndElement("", "", "field")

  private val mainNS = WikiNamespace("")

  private var total = 0
  private var skipped = 0

  def initialize() {
    writer add eventFactory.createStartDocument(CHARSET, VERSION, true)
    writer add eventFactory.createStartElement("", "", "add")
  }

  def deinitialize() {
    writer add eventFactory.createEndElement("", "", "add")
    writer add eventFactory.createEndDocument()
    writer.close()
    total = 0
    skipped = 0
  }

  def process(page: WikiPage) {
    if (page.namespace == mainNS && !page.isRedirect) {
      writer add docStart

      writer add fieldStart_id
      writer add eventFactory.createCharacters(page.id.getOrElse(-1).toString)
      writer add fieldEnd

      writer add fieldStart_title
      writer add eventFactory.createCharacters(page.title)
      writer add fieldEnd

      writer add fieldStart_text
      writer add eventFactory.createCharacters(page.text.getOrElse("").filter(c => c.isLetterOrDigit || c.isWhitespace))
      writer add fieldEnd

      writer add docEnd
    } else {
      skipped += 1
    }

    total += 1
    if (total % PRINT_RATE == 0) {
      Console println "total: %d\tskipped: %d".format(total, skipped)
    }
  }
}
