package org.wikinavia.wikidigest

/**
 * Created by IntelliJ IDEA.
 * User: dvolosnykh
 * Date: 3/26/12
 * Time: 10:01 AM
 * To change this template use File | Settings | File Templates.
 */

import scala.util.parsing.combinator.{RegexParsers, PackratParsers}
import org.omg.CORBA.DefinitionKind
import com.sun.org.apache.bcel.internal.classfile.ConstantLong


trait WikiMarkupParser extends RegexParsers with PackratParsers { this: WikiMarkupListener =>
  import WikiMarkupParser._
  import RegexUtils._

  override val skipWhitespace = false

  private val text = regex(HARMLESS_CHARS.r)
  private val redirectTag = regex(REDIRECT_TAG)
  private val articleTitle = regex(TITLE)
  private val internalLinkStart = literal(INTERNAL_LINK_START)
  private val internalLinkEnd = literal(INTERNAL_LINK_END)
  private val pipe = literal(PIPE)
  private val space = literal(SPACE)
  private val colon = literal(COLON)
  private val listMark = regex(LIST_MARK.r)
  private val newline = regex(NEWLINE)
  private val eof = regex(EOF)
  private val spaceTabs = regex(SPACE_TABS)

  private val eol = newline | eof

  private val articleLink =
    opt(INTERWIKI <~ colon) ~ opt(NAMESPACE <~ colon) ~ articleTitle ^^ {
      case interwiki ~ namespace ~ title =>
        onArticleLink(interwiki.getOrElse(""), namespace.getOrElse(""), "", title)
    } | "/" ~ articleTitle ^^ {
      case path ~ title => onArticleLink("", "", path, title)
    } | """(?:\.\.\/)+""" ~ articleTitle.? ^^ {
      case path ~ title => onArticleLink("", "", path, title.getOrElse(""))
    }
  private val redirect =
    redirectTag ~> internalLinkStart ~> articleLink <~ (internalLinkEnd | pipe | eol)

  private val formatting = regex("'+".r)
  private val textWithFormatting = text ^^ { case content => onText(content) }
  private val inlineElement = textWithFormatting
  private val horizontalRule = HORIZONTAL_RULE ~> text.? <~ eol ^^ {
    case unusedText => onHorizontalRule(unusedText.getOrElse(""))
  }
  private def heading(level: Int) = {
    val ruleStr = "=" * level
    val rule = literal(ruleStr)
    rule ~> (HARMLESS_CHARS untilFarthest ruleStr) <~ rule ^^ { case header => onHeading(level, header) }
  }
  private val headings =
    (1 to HEADINGS_MAX_LEVEL).reverse.map(heading).reduceLeft(_ | _) <~ spaceTabs.? <~ eol
  private val spaceBlock = (space ~> inlineElement.+ <~ eol) ~ (space ~> inlineElement.* <~ eol).* ^^ {
    case first ~ rest => onSpaceBlock(first :: rest)
  }

  private val term = regex(DEFINITION_MARK.fix.rep.r) ~> regex(TERM_CHARS.r)
  private def termDefinition(prefix: String) =
    (term ~ (COLON ~> inlineElement.* ^^ { case i => onListItem(i, None) }).? <~ eol) ~
      (prefix ~> listItem(prefix)).* ^^ { case t ~ first ~ rest => onTermDefinition(t, first.toList ::: rest) }
  private def listItem(prefix: String) = list(prefix) ^^ { case l => onListItem(List.empty, Some(l)) } |
    (inlineElement.* <~ eol) ~ (prefix ~> list(prefix)).? ^^ { case i ~ l => onListItem(i, l) }
  private def list(prefix: String): Parser[Entity] = listMark >> { mark =>
    val newPrefix = prefix + mark
    val item = if (mark == DEFINITION_MARK) termDefinition(prefix + INDENT_MARK) else listItem(newPrefix)
    rep1sep(item, newPrefix) ^^ { case l => mark match {
      case NUMBER_MARK => onEnumeratedList(l)
      case BULLET_MARK => onBullettedList(l)
      case INDENT_MARK => onIndentedList(l)
      case DEFINITION_MARK => onDefinitionList(l)
    }}
  }

  private val specialBlock = horizontalRule | headings | spaceBlock | list("") //| table
  private val paragraph = rep1sep(not(specialBlock) ~> inlineElement.+, newline) ^^ { case p => onParagraph(p.flatten) }
  private val paragraphs = newline.* ~> rep1sep(paragraph, newline.+) <~ newline.*
  private val specialBlocks = newline.* ~> (specialBlock <~ newline.*).+
  private val article =
    specialBlocks ~ (paragraphs ~ specialBlocks).* ~ paragraphs.? |
      paragraphs ~ (specialBlocks ~ paragraphs).* ~ specialBlocks.?

//  private val table = regex("""{|.*?|}""".r)

  val page = redirect.? ~ article.? ^^ {
    case r ~ a => onPage(r, a match {
      case Some(s ~ l ~ p) => s ::: l.map(_ match { case x ~ y => x ::: y }).flatten ::: p.getOrElse(List.empty)
      case None => List.empty
    })
  }
}

object WikiMarkupParser {
  import RegexUtils._

  private val HEADINGS_MAX_LEVEL = 6

  private val PIPE = "|"
  private val SPACE = " "
  private val COLON = ":"
  private val NUMBER_MARK = "#"
  private val BULLET_MARK = "*"
  private val INDENT_MARK = COLON
  private val DEFINITION_MARK = ";"

  private val INTERNAL_LINK_START = "[["
  private val INTERNAL_LINK_END = "]]"
  private val ITALICS = "'" * 2
  private val BOLD = "'" * 3
  private val BOLD_ITALICS = "'" * 5

  private val TITLE_LEGAL_CHARS = """\p{Alnum} %!\"$&'()*,\-.\/:;=?@\\^_`~"""
  private val HARMLESS_CHARS = """\p{Print}&&[^\r\n]""".fix.rep1
  private val TERM_CHARS = (HARMLESS_CHARS &\ COLON).fix.rep1

  private val LIST_MARK = (NUMBER_MARK + BULLET_MARK + INDENT_MARK + DEFINITION_MARK).fix
  private val INTERWIKI = """[^\p{Space}:|]*""".r
  private val NAMESPACE = """[^\p{Space}:|]*""".r
  private val REDIRECT_TAG = """#(?i:redirect).*""" untilFarthest INTERNAL_LINK_START.escape
  private val TITLE = TITLE_LEGAL_CHARS.fix.rep1.r
  private val NEWLINE = """\r\n?|\n\r?""".r
  private val EOF = """$""".r
  private val HORIZONTAL_RULE = """-{4,}""".r
  private val SPACE_TABS = """[ \t]+""".r
}

