Setting up workspace.

1. Install JDK 7.
2. Install IntelliJ IDEA (at least Version: 11.1.1 Build: 117.117).
3. Create new project from external model (Gradle). Select ROOT/projects/build.gradle
4. Set language level to 7. Press 'Finish'.
5. Go to project settings (Ctrl+Alt+Shift+S) and create Scala facet for 'wikidigest' project (probably for 'webui' also).
6. Wait while IDEA resolves gradle dependencies.

Test run configuration.
1. Select 'Edit configurations...' near the run tool button.
2. Name: Test
3. Test kind: Class (default).
4. Test Class: org.wikinavia.wikidigest.test.WikiMarkupSuite
5. Select module 'wikidigest'.
6. Press 'OK'. Run 'Test' configuration.
