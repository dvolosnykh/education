

Download Wikipedia's dump files.


1. Run
    time db/download.sh <path to dumps dir> <wiki> <date>
   for example:
    time db/download.sh ../ruwiki ruwiki 20120420


Load SQL-dumps.

1. Prepare MySQL. Edit /etc/mysql/my.cnf:
    [client]
    default-character-set = utf8
    [mysql]
    default-character-set = utf8
    [mysqld]
    default-character-set = utf8
    default-collation = utf8_unicode_ci
    character-set-server  = utf8
    collation-server  = utf8_unicode_ci

2. Run
    time db/load.sh ../ruwiki ruwiki 20120420


Configure Jetty (version 8.1.2).


1. Duplicate following files (at $JETTY_HOME/etc):
    * jetty.xml;
    * jetty-deploy.xml;
    * jetty-contexts.xml;
    * jetty-webapps.xml;
    * jetty-logging.xml;
   replacing 'jetty' to 'public' in filenames.
2. Edit contents in all those files (except for public-logging.xml) replacing:
    * paths from '/webapps' or '/contexts' to '/webapps/public' or '/contexts/public'
      respectively;
    * original id="Server" to id="Public".
3. Set connector's port in public.xml to well-known http port (i.e. 80).
4. Repeat all previous steps but for 'private'. Also the port in private.xml should be 8080.
5. Edit start.ini:
    * comment out all default *.xml. Add recently created ones.
    * uncomment --exec option and add line for -Drun.mode=production (skip this step if deploying for development).
6. Edit $JETTY_HOME/etc/jetty.conf: comment out '--pre=jetty-logging.xml' and add lines:
    --pre=etc/public-logging.xml
    --pre=etc/private-logging.xml
7. [Optional] You may delete following list of files and directories:
    * webapps/test.war;
    * contexts/javadoc.xml;
    * contexts/test.xml;
    * contexts/test.d;
    * contexts-available;
    * javadoc.


Run Jetty container as a daemon at system boot.


1. Copy '$JETTY_HOME/bin/jetty.sh' to /etc/init.d and set execution persmissions.
2. Register with:
    $> sudo update-rc.d jetty.sh defaults
3. Usage:
    $> sudo /etc/init.d/jetty.sh [start/stop/restart/check]


Deploy a Solr core.


1. Copy $WIKINAVIA_HOME/solr to $JETTY_HOME.
2. Copy $WIKINAVIA_HOME/solr.war to $JETTY_HOME/webapps/private.


Deploy web interface.


1. Build wars (from 'projects' directory):
    $> ./gradlew war
2. Copy webui/build/libs/webui.war to $JETTY_HOME/webapps/public/root.war.


Indexing full Wikipedia's dump.


1. Place ruwiki-DATE-pages-articles.xml.bz2 into 'ruwiki' directory above the $WIKINAVIA_HOME.
2.
  * a) Rename it removing date:
        from   ruwiki-DATE-pages-articles.xml.bz2
        to     ruwiki-pages-articles.xml.bz2

    b) Run translator (from 'projects' directory):
        $> ./gradlew :wikidigest:run
    You will get ruwiki/ruwiki-pages-texts.xml

  * a) Alternatively you can build distribution archive (from 'projects' directory):
        $> ./gradlew :wikidigest:distZip
       It will reside in projects/wikidigest/build/distributions.
    b) Unzip it and run:
        $> $ARCHIVE_ROOT/bin/wikidigest <original> <translated>
       where:
         original - path to ruwiki-DATE-pages-articles.xml.bz2, and
         translated - path to resulting XML in Solr 'add' schema
             (it is strongly advised to be 'ruwiki/ruwiki-pages-texts.xml' at one level
             above $WIKINAVIA_HOME);
       respectively.
    
3. Edit Jetty's copy of solr/conf/wiki_import.xml and set 'url' attribute to absolute path of 'ruwiki-pages-texts.xml'.
4. Launch Jetty and issue command:
    $> curl http://localhost:8080/solr/wiki_import?command=full-import
5. Follow the progress by running:
    $> curl http://localhost:8080/solr/wiki_import?indent=on


Load Wikipedia's SQL-dumps:


1. 
