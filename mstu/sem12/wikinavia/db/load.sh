#!/bin/bash

if [ $# -ne 3 ]; then
  tee <<HELP
Usage: $0 <path> <wiki> <date>
    There should exists a list of files:
    1) <path>/<wiki>-<date>-page.sql.gz
    2) <path>/<wiki>-<date>-pagelinks.sql.gz
    3) <path>/<wiki>-<date>-category.sql.gz
    4) <path>/<wiki>-<date>-categorylinks.sql.gz
HELP
  exit 1
fi

declare -r path=$1 wiki=$2 date=$3

declare -ar tables=(page pagelinks category categorylinks)

declare user='' password=''
while [[ "o$user" == 'o' ]]; do
  read -p 'Enter MySQL user: ' user
done
while [[ "o$password" == 'o' ]]; do
  read -sp 'Enter MySQL password: ' password
done

declare -r mysql="mysql --password=$password -u $user"

pushd $(dirname $0) > /dev/null
SCRIPTPATH=$(pwd)
popd > /dev/null

function file {
  local -r table=$1
  echo "$path/$wiki-$date-$table.sql.gz"
}

$mysql -e "create database if not exists $wiki" &>/dev/null
echo "Database created. Loading tables..."

for table in ${tables[*]}; do
  echo -e "\nTable '$table'..."

  declare file=$(file $table)
  time if [[ "$table" == "category" ]]; then
    "$SCRIPTPATH/patch_category.sh" $file
  else
    zcat $file
  fi | $mysql $wiki
done

echo -e "\nAll tables are loaded. Preparing... "
time $mysql $wiki <"$SCRIPTPATH/prepare.sql" &>/dev/null

echo -e "\nLoad finished."
