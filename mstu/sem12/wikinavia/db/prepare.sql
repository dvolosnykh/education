drop procedure if exists reach_all;
delimiter $$
create procedure reach_all(seed_id int(8) unsigned)
  reads sql data
  modifies sql data
begin
  set @seed := seed_id;
  set @sql := 'create table reached_prev (
      page_id int(8) unsigned not null primary key,
      page_title varbinary(255) not null
    ) engine = heap
    select page_id, page_title from page
      inner join categorylinks on page_namespace = 14 and page_title = cl_to
      where cl_from = ?';
  prepare base from @sql;
  drop table if exists reached_prev;
  execute base using @seed;

  set @sql := 'create table reached (
      page_id int(8) unsigned not null primary key,
      page_title varbinary(255) not null
    ) engine = heap
    select * from reached_prev';
  prepare base from @sql;
  drop table if exists reached;
  execute base;

  set @sql := 'create table reached_next (
      page_id int(8) unsigned not null primary key,
      page_title varbinary(255) not null
    ) engine = heap
    select distinct page.page_id, page.page_title from reached_prev
      inner join categorylinks on cl_from = reached_prev.page_id
      inner join page on page_namespace = 14 and page.page_title = cl_to';
  prepare next from @sql;

  drop table if exists reached_next;
  repeat
    execute next;

    insert ignore into reached
      select * from reached_next;
    set @rows := row_count();

    drop table reached_prev;
    alter table reached_next rename to reached_prev;
  until @rows = 0 end repeat;
  drop table reached_prev;

  update category inner join reached on cat_title = page_title
    set category.cat_articles = category.cat_articles + 1;

  select row_count() as reached_count;
--  select page_title, cat_pages, cat_subcats, cat_files, cat_articles from category inner join reached on cat_title = page_title;

  drop table reached;
end$$
delimiter ;

update category set cat_articles = cat_pages - cat_subcats - cat_files;

drop table if exists seeds;
create table seeds (
  page_id int(8) unsigned not null primary key,
  page_title varbinary(255) not null
) select page_id, page_title from page where page_namespace = 0;

drop procedure if exists calc_articles_counts;
delimiter $$
create procedure calc_articles_counts()
  reads sql data
  modifies sql data
begin
  declare seed_id int(8) unsigned;
  declare seed_title varbinary(255);

  declare no_more_rows boolean;
  declare seed_cur cursor for select page_id, page_title from seeds;

  declare continue handler for not found set no_more_rows = true;

  open seed_cur;
  seed_loop: loop
    fetch seed_cur into seed_id, seed_title;

/*      select concat('*** ', seed_title, ' ***') as seed_title;
      call reach_all(seed_id);
      close seed_cur;
      leave seed_loop;
*/
    if no_more_rows then
      close seed_cur;
      leave seed_loop;
    end if;

    select concat('*** ', seed_title, ' ***') as seed_title;
    call reach_all(seed_id);
  end loop seed_loop;
end$$
delimiter ;

call calc_articles_counts();

drop table seeds;

drop procedure reach_all;
drop procedure calc_articles_counts;
