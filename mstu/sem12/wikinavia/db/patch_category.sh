#!/bin/bash

if [ $# -ne 1 ]; then
  echo "Usage: $0 WIKI-DATE-category.sql.gz"
  exit 1
fi

zcat $1 | sed -e '/PRIMARY KEY/i \
  `cat_articles` int(11) NOT NULL DEFAULT '\''0'\'',' -e '/DISABLE KEYS/,/ENABLE KEYS/s/,0)/,0,0)/g'
