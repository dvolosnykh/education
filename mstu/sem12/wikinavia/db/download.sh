#!/bin/bash

if [ $# -ne 3 ]; then
  tee <<HELP
Usage: $0 <path> <wiki> <date>
HELP
  exit 1
fi

declare -r path=$1 wiki=$2 date=$3

declare -ar tables=(page pagelinks category categorylinks)
declare -r BASE_URL="http://dumps.wikimedia.org/$wiki/$date/$wiki-$date"

mkdir -p $path && cd $path &&
for table in ${tables[*]}; do
  wget $BASE_URL-$table.sql.gz
done

wget $BASE_URL-pages-articles.xml.bz2
