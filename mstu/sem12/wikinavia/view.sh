#!/bin/bash

curl -s http://wikinavia.org/api/viewer | awk -F'\t' '/^[0-9]+/ {t[$1]++} END{for (i in t) {h[t[i]]++}; for (i in h) {print i, h[i]};}'
