#!/bin/bash

if [[ $(whoami) != "root" ]]; then
	echo "Run with 'sudo'"
	exit 1
fi

/etc/init.d/jetty.sh stop
declare -r pid=$(pgrep -f run.mode=production)
if [[ "$pid" != "" ]]; then kill -KILL $pid; fi
