// Form of storing:
// *) vector vA: contains non-zero elements;
// *) vector IA: contains row-indeces of non-zero elements;
// *) list JA: contains indeces of first non-zero elements in each column
//		(if there is not any non-zero element in a column the stored index
//		is equal to -1);

// Simulate multiplication of a vector-string on a matrix. Both are
//	stored in the specified form. Output the product-vector in the same
// form.

// Do the multiplication in an ordinary way also.
// Compare work time and used memory for both methods.

// Goal: working with rarefied matrices.
// Possible user errors:
// *) when defining the size of matrix, type only positive values;
// *) all elements are integers;
// *) coordinates and indeces of elements are non-negative.

#include <stdio.h>
#include <conio.h>
#include <dos.h>

#define quit   '0'
#define test1  '1'
#define test2  '2'
#define test3  '3'
#define manual '4'

#define times 100000

struct list
{
	int index;
	list* next;
};

struct rarefied
{
	int* v;
	int* I;
	unsigned num;
	list* J;
};

int* multiply_ordinary (const int* const V, const int** const M,
		const unsigned m, const unsigned n);
rarefied multiply_rarefied (const rarefied rV, const rarefied rM,
		const unsigned n);
void alloclist(list* &src, const unsigned quantity);
void convert_m (const int** const M, const unsigned m, const unsigned n,
		rarefied &rM);
void convert_v (const int* const V, const unsigned m, rarefied &rV);
int** init_matrix (const unsigned m, const unsigned n);
int* init_vector (const unsigned m);
void print_matrix (const int** const M, const unsigned m, const unsigned n);
void print_vector (const int* const V, const unsigned n);
void print_list (list* listJ, const unsigned m);
struct time elapsed (const struct time &finish, const struct time &start);
long int seconds (const struct time &moment);
void deletefwlist (list* const element);
char menu ();

int main ()
{
	for (;;)
	{
		unsigned m, n;
		rarefied rM, rV;
		int** M;
		int* V;

		char choice = menu();
		if (choice == manual)
		{
			clrscr();
			printf("Input the size of matrix, m x n: ");
			scanf("%u%u", &m, &n);

			M = init_matrix(m, n);
			V = init_vector(m);

			clrscr();
			printf("--- MATRIX ---\n\n");
			printf("Input number of non-zero elements: ");
			scanf("%u", &rM.num);

			printf("\nInput non-zero elements:\n");

			register unsigned k;
			for (k = 0; k < rM.num; k++)
			{
				unsigned i, j;
				printf("\nCoordinates: ");
				scanf("%u%u", &i, &j);

				printf("Value: ");
				scanf("%i", *(M + i) + j);
			}

			clrscr();
			printf("--- VECTOR ---\n\n");
			printf("Input number of non-zero elements: ");
			scanf("%u", &rV.num);

			printf("\nInput non-zero elements:\n");

			for (k = 0; k < rV.num; k++)
			{
				unsigned i;
				printf("\nIndex: ");
				scanf("%u", &i);

				printf("Value: ");
				scanf("%i", V + i);
			}
		}
		else if (choice != quit)
		{
			char testfile[] = "testX.txt";
			testfile[4] = choice;

			FILE *F = fopen(testfile, "r");
			fscanf(F, "%u%u", &m, &n);

			M = init_matrix(m, n);
			V = init_vector(m);

			fscanf(F, "%u", &rM.num);

			register unsigned k;
			for (k = 0; k < rM.num; k++)
			{
				unsigned i, j;
				fscanf(F, "%u%u", &i, &j);
				fscanf(F, "%i", *(M + i) + j);
			}

			fscanf(F, "%u", &rV.num);

			for (k = 0; k < rV.num; k++)
			{
				unsigned i;
				fscanf(F, "%u", &i);
				fscanf(F, "%i", V + i);
			}
		}
		else
			break;

		struct time start, finish_o, finish_r;

		clrscr();
		//printf("===== ORDINARY METHOD ======\n");

		int* P;

		gettime(&start);
		for (register unsigned long om = 0;;)
		{
			P = multiply_ordinary(V, M, m, n);

		if (++om == times) break;

			delete P;
		}
		gettime(&finish_o);

		finish_o = elapsed(finish_o, start);

		//printf("\nMatrix M:\n");
		//print_matrix(M, m, n);

		//printf("\n\nVector V:\n");
		//print_vector(V, m);

		//printf("\n\nVector P:\n");
		//print_vector(P, n);

		//printf("\n\n===== RAREFIED MATRICIES METHOD =====\n");

		rM.v = new int [rM.num];
		rM.I = new int [rM.num];
		convert_m(M, m, n, rM);

		rV.v = new int [rV.num];
		convert_v(V, m, rV);

		rarefied rP;

		gettime(&start);
		for (register unsigned long rm = 0;;)
		{
			rP = multiply_rarefied(rV, rM, n);

		if (++rm == times) break;

			delete rP.v;
			deletefwlist(rP.J);
		}
		gettime(&finish_r);

		finish_r = elapsed(finish_r, start);

		printf("\nVector vM:\n");
		print_vector(rM.v, rM.num);

		printf("\nVector IM:\n");
		print_vector(rM.I, rM.num);

		printf("\nList JM:\n");
		print_list(rM.J, n);

		printf("\n\nVector vV:\n");
		print_vector(rV.v, rV.num);

		printf("\nList JV:\n");
		print_list(rV.J, m);

		printf("\n\nVector vP:\n");
		print_vector(rP.v, rP.num);

		printf("\nList JP:\n");
		print_list(rP.J, n);

		getch();

		printf("\n\n===== RAREFIED MATRICIES METHOD =====\n");
		printf("Done in %2d:%02d:%02d.%02d\n", finish_r.ti_hour,
			finish_r.ti_min, finish_r.ti_sec, finish_r.ti_hund);

		unsigned long memsize_r = 2 * sizeof(rarefied) +
				(2 * rM.num + rV.num + rP.num) * sizeof(int) +
				(2 * n + m) * sizeof(list);

		printf("Memory used: %u bytes\n", memsize_r);

		printf("\n\n===== ORDINARY METHOD ======\n");
		printf("Done in %2d:%02d:%02d.%02d\n", finish_o.ti_hour,
			finish_o.ti_min, finish_o.ti_sec, finish_o.ti_hund);

		unsigned long memsize_o = (m * (n + 1) + n) * sizeof(int) +
				(m + 2) * sizeof(int*) + sizeof(int**);

		printf("Memory used: %u bytes\n", memsize_o);

		// Deleting allocated memory.
		register unsigned i;

		for (i = 0; i < m; i++)
			delete M[i];
		delete M;
		delete rM.v;
		delete rM.I;
		deletefwlist(rM.J);

		delete V;
		delete rV.v;
		deletefwlist(rV.J);

		delete P;
		delete rP.v;
		deletefwlist(rP.J);

		getch();
	}

	return 0;
}

// Ordinary multiplication of a vector-string on a matrix.
int* multiply_ordinary (const int* const V, const int** const M,
		const unsigned m, const unsigned n)
{
	int* result = new int [n];

	for (register unsigned j = 0; j < n; j++)
	{
		result[j] = 0;

		for (register unsigned i = 0; i < m; i++)
			result[j] += V[i] * M[i][j];
	}

	return result;
}

rarefied multiply_rarefied (const rarefied rV, const rarefied rM,
		const unsigned n)
{
	rarefied rP;
	alloclist(rP.J, n);

	unsigned max = 0;
	for (list* temp = rM.J; temp; temp = temp->next)
		if (temp->index >= 0) max++;
	rP.v = new int [max];

	list* tempMJ;
	list* tempPJ;
	for (tempMJ = rM.J, tempPJ = rP.J, rP.num = 0; tempMJ != NULL;
		  tempMJ = tempMJ->next, tempPJ = tempPJ->next)
	{
		int cur = 0;

		if (tempMJ->index >= 0)  // The column in a matrix is non-zero.
		{
			list* tempVJ = rV.J;

			 // Finding the begining index of (n)ext (c)olumn.
			for (list* temp = tempMJ->next; temp && temp->index == -1;
				  temp = temp->next);

			unsigned nc = temp ? temp->index : rM.num;

			// Going by the rows, where non-zero elements in a given
			// column are located.
			for (register int i = tempMJ->index, k = 0; i < nc; i++)
			{
				// Going to a needed element of list of j's of vector V.
				for (; k < rM.I[i]; k++)
					tempVJ = tempVJ->next;

				// If this element exists.
				if (tempVJ->index >= 0)
					cur += rM.v[i] * rV.v[tempVJ->index];
			}
		}

		if (cur != 0)
			// Saving a non-zero element of a product-vector.
			rP.v[tempPJ->index = rP.num++] = cur;
		else
			tempPJ->index = -1;
	}

	return rP;
}

void alloclist(list* &src, const unsigned quantity)
{
	list* temp = src = new list;
	for (register unsigned j = 1; j < quantity; j++)
		temp = temp->next = new list;
	temp->next = NULL;
}

void convert_m (const int** const M, const unsigned m, const unsigned n,
		 rarefied &rM)
{
	register unsigned i, j, p;

	// Saving non-zero elements.
	for (j = p = 0; j < n && p < rM.num; j++)
		for (i = 0; i < m && p < rM.num; i++)
			if (M[i][j] != 0)
			{
				rM.v[p] = M[i][j];  // Saving value.
				rM.I[p++] = i;	   // Saving row.
			}

	// Allocation of memory for list.
	alloclist(rM.J, n);

	// Finding beginings of columns.
	list* temp;
	for (j = 0, temp = rM.J; j < n; j++, temp = temp->next)
	{
		for (i = 0; i < m && M[i][j] == 0; i++);

		if (i < m)
		{
			unsigned c, jj;

			// How many non-zero elements in the row before the first
			// non-zero element in the column.
			for (jj = 0, c = 0; jj < j; jj++)
				if (M[i][jj] != 0) c++;

			// Index of the first element in the column.
			register unsigned k = 0;
			for (register unsigned l = 0; l <= c; l++, k++)
				for (; rM.I[k] != i; k++);

			// Here is index of the first element in the j-th column.
			temp->index = --k;
		}
		else  // There is no non-zero elements in a column.
			temp->index = -1;
	}
}

void convert_v (const int* const V, const unsigned m, rarefied &rV)
{
	register unsigned j, p;

	// Saving non-zero elements.
	for (j = p = 0; j < m && p < rV.num; j++)
		if (V[j] != 0)
			rV.v[p++] = V[j];  // Saving value to vB.


	// Allocation of memory for list JB.
	alloclist(rV.J, m);

	// Finding beginings of columns.
   list* temp;
	for (j = 0, temp = rV.J; j < m; j++, temp = temp->next)
		if (V[j] != 0)
		{
			unsigned c, jj;

			// How many non-zero elements in the row before the first
			// non-zero element in the column.
			for (jj = 0, c = 0; jj < j; jj++)
				if (V[jj] != 0) c++;

			// Here is index of the first element in the j-th column.
			temp->index = c;
		}
		else  // There is no non-zero elements in a column.
			temp->index = -1;
}

// Initializes matrix.
int** init_matrix (const unsigned m, const unsigned n)
{
	int** M = new int* [n];

	for (register unsigned i = 0; i < m; i++)
	{
		M[i] = new int [n];

		for (register unsigned j = 0; j < n; j++)
			M[i][j] = 0;
	}

	return M;
}

int* init_vector (const unsigned m)
{
	int* V = new int [m];

	for (register unsigned j = 0; j < m; j++)
		V[j] = 0;

	return V;
}

// Prints a matrix.
void print_matrix (const int** const M, const unsigned m, const unsigned n)
{
	for (register unsigned i = 0; i < m; i++)
		{
			for (register unsigned j = 0; j < n; j++)
				printf("%4i", M[i][j]);
			printf("\n");
		}
}

// Prints a vector.
void print_vector (const int* const V, const unsigned n)
{
	for (register unsigned i = 0; i < n; i++)
		printf("%4i", V[i]);
}

// Prints a list.
void print_list (list* listJ, const unsigned n)
{
	for (register unsigned j = 0; j < n; j++, listJ = listJ->next)
		printf("%4i", listJ->index);
}

// Length of time interval from start to finish.
struct time elapsed (const struct time &finish, const struct time &start)
{
	long int diff = seconds(finish) - seconds(start);

	struct time interval;

	interval.ti_hund = diff % 100;
	interval.ti_sec  = (diff /= 100) % 60;
	interval.ti_min  = (diff /= 60) % 60;
	interval.ti_hour = diff /= 60;

	return interval;
}

long int seconds (const struct time &moment)
{
	return ((moment.ti_hour * 60 + moment.ti_min) * 60 +
			  moment.ti_sec) * 100 + moment.ti_hund;
}

// Deletes forward-linked list.
void deletefwlist (list* const element)
{
	if (element->next)
		deletefwlist (element->next);

	delete element;	
}

char menu ()
{
	clrscr();

	printf("Menu:\n\n");
	printf("1. Run test #1;\n");
	printf("2. Run test #2;\n");
	printf("3. Run test #3;\n");
	printf("4. Manual input;\n");
	printf("0. Quit.\n");

	char c;

	do
		c = getch();
	while ( c < '0' || '4' < c);

	return c;
}
