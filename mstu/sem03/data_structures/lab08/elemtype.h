#ifndef ELEMTYPE_H
	#define ELEMTYPE_H

	#define LEN 16

	struct cell
	{
		char key[LEN];
		cell* next;
	};

#endif