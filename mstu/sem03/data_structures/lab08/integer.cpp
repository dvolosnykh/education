#include <ctype.h>
#include <conio.h>

#include "integer.h"

#define issign(c) ((c) == '-' || (c) == '+')

// Inputs an integer.
int integer ()
{
	int MN = 0;
	int Result = 0;
	int firstdigit = -1;

	for (char NCM = 0, c; !isspace(c = getch()) || NCM == 0 || NCM == 1 && MN == 0; )
		// Working with digit.
		if (isdigit(c))
		{
			// Preceeding zeros are not allowed.
			if (!(MN == 1 && firstdigit == 0))
			{
				if (MN++ == 0) firstdigit = c - '0';

				NCM++;
				putch(c);
				(Result *= 10) += c - '0';
			}
		} // Working with sign.
		else if (issign(c))
		{
			if (NCM == 0)
			{
				Result = (c == '-') ? -Result : Result;
				NCM++;
				putch(c);
			}
		} // Deleting char by Backspace key from the end.
		else if (c == '\b' && NCM > 0)
		{
			if (MN > 0) MN--;
			if (--NCM == 0) firstdigit = -1;
			Result /= 10;
			putch(c);
			putch(' ');
			putch(c);
		}

	return Result;
}

// Inputs a non-negative integer.
unsigned nninteger ()
{
	unsigned Result = 0; // Result;

	for (char MN = 0, c; !isspace(c = getch()) || MN == 0; )
	{
		// Working with digit.
		if (isdigit(c))
		{
			// Preceeding zeros are not allowed.
			if (!(MN == 0 && c == '0'))
			{
				MN++;
				putch(c);
				(Result *= 10) += c - '0';
			}
		} // Deleting char by Backspace key from the end.
		else if (c == '\b' && MN > 0)
		{
			MN--;
			Result /= 10;
			putch(c);
			putch(' ');
			putch(c);
		}
	}

	return Result;
}