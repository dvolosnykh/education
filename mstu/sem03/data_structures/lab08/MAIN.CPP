#include <stdlib.h>
#include <conio.h>

#include "elemtype.h"
#include "nodetype.h"
#include "viewtree.h"
#include "balanced.h"
#include "memory.h"
#include "utils.h"
#include "menu.h"

void main ()
{
	char filename[13] = "test2.txt";
	unsigned m;
	cell** table = NULL;
	node* tree = NULL;

	for (char choice; (choice = menu(filename)) != '0'; )
		switch (choice)
		{
		case '1':
			fspecify(filename);
			break;

		case '2':
			build_table(filename, table, m);
			break;

		case '3':
			build_tree(filename, tree);
			break;

		case '4':
			clrscr();

			if (table && tree)
				find(table, tree, m);
			else
			{
				clrscr();
				if (!table)
					error_table();
				if (!tree)
					error_tree();
			}

			break;

		case '5':
			if (table && tree)
				remove_words(table, tree, m);
			else
			{
				clrscr();
				if (!table)
					error_table();
				if (!tree)
					error_tree();
			}

			break;

		case '6':
			clrscr();

			if (table)
			{
				view_table(table, m);
				getch();
			}
			else
				error_table();
			break;

		case '7':
			clrscr();

			if (tree)
			{
				view_RAL(tree, 0, 'A');
				getch();
			}
			else
				error_tree();
			break;
		}

	if (table)
		delete_table(table, m);

	if (tree)
		del_baltree(tree);
}