#ifndef BALANCED_H
	#define BALANCED_H

	#include "nodetype.h"

	void rotate_LL (node* &vertex);
	void rotate_LR (node* &vertex);
	void balance_L (node* &vertex);

	void rotate_RR (node* &vertex);
	void rotate_RL (node* &vertex);
	void balance_R (node* &vertex);

	unsigned insert (const char* const &x, node* &vertex, unsigned &isnew);
	node* search (const char* const &x, node* &vertex, unsigned &counter);
	unsigned remove (const char &x, node* &vertex, unsigned &found);
	unsigned rightest (node* &vertex, node* const &dest);
	void save (const char* const &x, node* &vertex);

	void del_baltree (node* const &vertex);

#endif