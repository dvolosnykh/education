#ifndef NODETYPE_H
	#define NODETYPE_H

	struct node
	{
		char* key;
		unsigned count;
		int balance;

		node* left;
		node* right;
	};

#endif