#ifndef INTEGER_H
	#define INTEGER_H

	#define BKSP '\b'
	#define ESC  27

	#define issign(c) ((c) == '-' || (c) == '+')

	int integer (unsigned & quit);
	unsigned nninteger (unsigned & quit);
	unsigned pinteger (unsigned & quit);
	int ninteger (unsigned & quit);
	int npinteger (unsigned & quit);

	void bksp ();

#endif