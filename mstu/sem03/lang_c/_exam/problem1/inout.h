#ifndef INOUT_H
	#define INOUT_H

	void scan( const char * parameter[], char * const format, ... );
	void print( const char * parameter[], char * const format, ... );

#endif