#include <stdlib.h>
#include <string.h>

#include "blist.h"
#include "plist.h"

belement_t * findprev( belement_t * current );
void delete_block( belement_t * & deleted );
belement_t * new_block( char & key );

unsigned param_is_given( name_t name );

block_t & getblock( belement_t * & e );
belement_t * & getnext( belement_t * & e );
belement_t * & gotonext( belement_t * & e );

char & getkey( block_t & block );
plist_t & getparameters( block_t & block );


belement_t * add( blist_t & blocks, char & key )
{
	belement_t * added = new_block( key );

	if ( isempty( blocks ) )  // was no elements
		gethead( blocks ) = getnext( added ) = added;
	else
	{
		getnext( added ) = gethead( blocks );
		getnext( findprev( gethead( blocks ) ) ) = added;
	}

	return added;
}

void remove( blist_t & blocks, belement_t * & removed )
{
	belement_t * prev = findprev( removed );

	if ( prev == removed )  // there is a single element
		init( blocks );
	else
		gethead( blocks ) = getnext( prev ) = getnext( removed );  // there are multiple elements

	delete_block( removed );
}

pelement_t * findadd( blist_t & blocks, char key, name_t name,
		unsigned size )
{
	belement_t * block_ptr = search( blocks, key );

	if ( !block_ptr )
		block_ptr = add( blocks, key );

	return findadd( getparameters( getblock( block_ptr ) ), name, size );
}

void remove_block_param( blist_t & blocks, char key, name_t name )
{
	belement_t * block_ptr = search( blocks, key );

	if ( block_ptr )
		if ( param_is_given( name ) )
			remove_param( getparameters( getblock( block_ptr ) ), name );
		else
			remove( blocks, block_ptr );
}

unsigned param_is_given( name_t name )
{
	return strcmp( name, "" ) != 0;
}

belement_t * findprev( belement_t * current )
{
	belement_t * e;

	for ( e = current; e && getnext( e ) != current;
		e = getnext( e ) == current ? NULL : getnext( e ) );

	return e;
}

unsigned isempty( blist_t & blocks )
{
	return gethead( blocks ) == NULL;
}

unsigned isinlist( blist_t & blocks, char & key )
{
	return search( blocks, key ) != NULL;
}

belement_t * search( blist_t & blocks, char & key )
{
	belement_t * e;

	for ( e = gethead( blocks ); e && getkey( getblock( e ) ) != key;
		e = getnext( e ) == gethead( blocks ) ? NULL : getnext( e ) );

	return e;
}

blist_t & init( blist_t & blocks )
{
	gethead( blocks ) = NULL;

	return blocks;
}

blist_t & clear( blist_t & blocks )
{
	for ( belement_t * e = gethead( blocks ); e; )
	{
		belement_t * temp = e;
		e = getnext( e ) == gethead( blocks ) ? NULL : getnext( e );

		delete_block( temp );
	}

	return init( blocks );	
}

belement_t * new_block( char & key )
{
	belement_t * added = ( belement_t * ) malloc( sizeof( belement_t ) );

	getkey( getblock( added ) ) = key;
	init( getparameters( getblock( added ) ) );

	return added;
}

void delete_block( belement_t * & deleted )
{
	clear( getparameters( getblock( deleted ) ) );
	free( ( void * ) deleted );
}

belement_t * & gethead( blist_t & blocks )
{
	return blocks.head;
}

block_t & getblock( belement_t * & e )
{
	return e->block;
}

belement_t * & getnext( belement_t * & e )
{
	return e->next;
}

belement_t * & gotonext( belement_t * & e )
{
	return e = e->next;
}

char & getkey( block_t & block )
{
	return block.key;
}

plist_t & getparameters( block_t & block )
{
	return block.parameters;
}