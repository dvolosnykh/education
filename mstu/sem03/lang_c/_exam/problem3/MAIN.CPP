#include <stdio.h>
#include <conio.h>

#include "plist.h"

#define QUIT '0'

char menu();
char choose( const char * const information );

void main()
{
	plist_t parameters;
	init( parameters );

	for ( ; ; )
	{
		clrscr();

		char choice = menu();

	if ( choice == QUIT ) break;

		switch( choice )
		{
		case '1':
			{
			clrscr();

			printf( "Input:\n\n" );

			printf( "* Name of the parameter: " );
			name_t name;
			scanf( "%s", name );

			if ( choose( "\nInput the size of the parameter [y/n] ? " ) == 'y' )
			{
				printf( "\n* The size: " );
				unsigned size;
				scanf( "%u", &size );

				findadd( parameters, name, size );
			}
			else
				findadd( parameters, name );

			break;
			}
		case '2':
			{
			clrscr();

			printf( "Input the name of the parameter: " );
			name_t name;
			scanf( "%s", name );

			remove_param( parameters, name );

			break;
			}
		}
	}

	clear( parameters );
}

char menu()
{
	printf( "1. Find/add;\n" );
	printf( "2. Remove;\n" );
	printf( "\n0. Quit.\n" );

	char choice;

	do
	{
		choice = getch();
	}
	while ( choice < '0' || '2' < choice );

	return choice;
}

char choose( const char * const information )
{
	printf ( information );

	char c;

	do
	{
		c = getch();
	}
	while ( c != 'y' && c != 'n' );

	putch( c );

	return c;
}