#ifndef NLIST_H
	#define NLIST_H

	#define MAXLEN 10

	typedef char name_t[ MAXLEN + 1 ];

	struct nelement_t
	{
		name_t name;
		nelement_t * next;
	};

	struct nlist_t
	{
		nelement_t * first;
	};

	nelement_t * add( nlist_t & names, name_t name );
	void remove( nlist_t & names, nelement_t * removed );

	unsigned isempty( nlist_t & names );

	unsigned isinlist( nlist_t & names, name_t name );
	nelement_t * search( nlist_t & names, name_t name );

	nlist_t & init( nlist_t & names );
	nlist_t & clear( nlist_t & names );

	nelement_t * & getfirst( nlist_t & names );

#endif