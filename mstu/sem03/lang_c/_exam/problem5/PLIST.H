#ifndef PLIST_H
	#define PLIST_H

	#include "nlist.h"

	struct param_t
	{
		nlist_t names;
		void * value_ptr;
	};

	struct pelement_t
	{
		param_t parameter;
		pelement_t * next;
	};

	struct plist_t
	{
		pelement_t * head;
	};

	pelement_t * add( plist_t & parameters, name_t name, unsigned size );
	void remove( plist_t & parameters, pelement_t * removed );

	void * findadd( plist_t & parameters, name_t name, unsigned size = 0 );
	void remove_param( plist_t & parameters, name_t param_name = "" );

	unsigned alias( pelement_t * & param_ptr, name_t newname );

	unsigned isempty( plist_t & parameters );

	unsigned isinlist( plist_t & parameters, name_t name );
	pelement_t * search( plist_t & parameters, name_t name );

	plist_t & init( plist_t & parameters );
	plist_t & clear( plist_t & parameters );

	pelement_t * & gethead( plist_t & parameters );

#endif