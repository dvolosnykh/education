#ifndef DERIVATE_H
	#define  DERIVATE_H

	float derivative (funct* const F, const float x, const float Eps);
	float derivative (funct* const F, const float x, const float dx, const float Eps);
    
#endif