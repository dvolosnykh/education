#include <conio.h>

#include "tools\getkey.h"

unsigned getkey ()
{
	unsigned key;

	if ( !( key = getch () ) ) key = 256 + getch ();

	return key;
}