#ifndef SKIP_H
	#define SKIP_H

	unsigned skip_nonspaces_r ( unsigned & i, const char * const & str,
			const unsigned & num );
	unsigned skip_nonspaces_l ( unsigned & i, const char * const & str );

	unsigned skip_spaces_r ( unsigned & i, const char * const & str,
			const unsigned & num );
	unsigned skip_spaces_l ( unsigned & i, const char * const & str );

	unsigned wordleft( const unsigned pos, const char * const & str );
	unsigned wordright( const unsigned pos, const char * const & str,
			const unsigned & num );

#endif
