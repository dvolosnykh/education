#ifndef INPUT_H
	#define INPUT_H

	#include "tools\cursor.h"

	void deletes( char * & str, const unsigned pos,
			unsigned & num, const unsigned count = 1 );
	void backspaces( char * & str, unsigned pos,
			unsigned & num, const unsigned count = 1 );
	void inserts( char * & str, const unsigned & key,
			const unsigned pos, unsigned & num );
	void overwrites( char * & str, const unsigned & key,
			unsigned pos, unsigned & num );

	void outputchar( char * & str, const unsigned & key,
			cursor_t & cursor, unsigned & num );

#endif
