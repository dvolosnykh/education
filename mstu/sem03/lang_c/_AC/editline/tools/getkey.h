#ifndef GETKEY_H
	#define GETKEY_H

	const unsigned BKSP      =   8;
	const unsigned CTRLBKSP	 = 127;
	const unsigned DEL       =  83 + 256;
	const unsigned CTRLDEL   = 147 + 256;
	const unsigned LEFT      =  75 + 256;
	const unsigned RIGHT     =  77 + 256;
	const unsigned ESC       =  27;
	const unsigned ENTER     =  13;
	const unsigned HOME      =  71 + 256;
	const unsigned END       =  79 + 256;
	const unsigned INSERT    =  82 + 256;
	const unsigned CTRLLEFT  = 115 + 256;
	const unsigned CTRLRIGHT = 116 + 256;
	const unsigned ALTL      =  38 + 256;
	const unsigned ALTR      =  19 + 256;
	const unsigned ALTM      =  50 + 256;
	const unsigned ALTJ      =  36 + 256;

	unsigned getkey ();

#endif
