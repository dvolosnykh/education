#ifndef LIST_H
	#define LIST_H

	#include "element.h"

	struct list_t
	{
		element_t * first;
		element_t * last;
	};

	unsigned isempty( list_t & L );

	element_t * add( list_t & L, data_t & x );
	void remove( list_t & L, data_t & x );

	unsigned isinlist( data_t & x, list_t & L );
	element_t * search( data_t & x, list_t & L );

	list_t & listcpy( list_t & dest, list_t & src );

	list_t & init_list( list_t & L );
	list_t & clear_list( list_t & L );

	void output_list( list_t & L );

	element_t * & getfirst( list_t & L );
	element_t * & getlast( list_t & L );

#endif