#include <stdio.h>

#include "list.h"

#include "element.h"

element_t * new_element( )
{
	return new element_t;
}

void delete_element( element_t * & e )
{
	delete e;
}

data_t & datacpy( data_t & a, data_t & b )
{
	getvalue( a ) = getvalue( b );

	return a;
}

int comp( data_t & a, data_t & b )
{
	return getvalue( a ) - getvalue( b );
}

unsigned isempty( list_t & L )
{
	return getfirst( L ) == NULL;
}

element_t * add( list_t & L, data_t & x )
{
	element_t * inserted = new_element();
	datacpy( getdata( inserted ), x );

	element_t * right = search( x, L );

	if ( right )
		if ( right == getfirst( L ) )
		{
			// add to the begining of non-empty list.

			getfirst( L ) = inserted;
			getprev( right ) = inserted;

			getprev( inserted ) = NULL;
			getnext( inserted ) = right;
		}
		else  // add into the middle.
		{
			element_t * left = getprev( right );

			getprev( right ) = inserted;
			getnext( left ) = inserted;

			getnext( inserted ) = right;
			getprev( inserted ) = left;
		}
	else if ( getlast( L ) )  // add to the end.
	{
		element_t * left = getlast( L );

		getlast( L ) = inserted;
		getnext( left ) = inserted;

		getprev( inserted ) = left;
		getnext( inserted ) = NULL;
	}
	else  // add ot an empty list.
	{
		getlast( L ) = inserted;
		getfirst( L ) = inserted;

		getprev( inserted ) = NULL;
		getnext( inserted ) = NULL;
	}

	return inserted;
}

void remove( list_t & L, data_t & x )
{
	element_t * dest = search( x, L );

	if ( dest )
	{
		if ( dest == getfirst( L ) )
			if ( dest == getlast( L ) )  // remove the single element.
			{
				getfirst( L ) = NULL;
				getlast( L ) = NULL;
			}
			else  // remove from the begining.
			{
				element_t * right = getnext( dest );
				getfirst( L ) = right;
				getprev( right ) = NULL;
			}
		else if ( dest == getlast( L ) )  // remove from the end.
		{
			element_t * left = getprev( dest );
			getlast( L ) = left;
			getnext( left ) = NULL;
		}
		else  // remove from the middle.
		{
			element_t * right = getnext( dest );
			element_t * left = getprev( dest );

			getnext( left ) = right;
			getprev( right ) = left;
		}

		delete_element( dest );
	}
}

unsigned isinlist( data_t & x, list_t & L )
{
	element_t * result = search( x, L );

	if ( result && comp( getdata( result ), x ) != 0 ) result = NULL;

	return result != NULL;
}

element_t * search( data_t & x, list_t & L )
{
	element_t * element;

	for ( element = getfirst( L );
			element && comp( getdata( element ), x ) < 0;
			gotonext( element ) );

	return element;
}

list_t & listcpy( list_t & dest, list_t & src )
{
	clear_list( dest );

	for ( element_t * e = getfirst( src ); e; gotonext( e ) )
		add( dest, getdata( e ) );

	return dest;
}

list_t & init_list( list_t & L )
{
	getfirst( L ) = NULL;
	getlast( L ) = NULL;

	return L;
}

list_t & clear_list( list_t & L )
{
	for (element_t * temp; temp = getfirst( L ); delete_element( temp ) )
		gotonext( getfirst( L ) );
	
	getlast( L ) = NULL;

	return L;
}

void output_list( list_t & L )
{
	for (element_t * e = getfirst( L ); e; gotonext( e ) )
		printf( "%i ", getvalue( e ) );

	printf( "\n" );
}

element_t * & getfirst( list_t & L )
{
	return L.first;
}

element_t * & getlast( list_t & L )
{
	return L.last;
}