#ifndef VELEMENT_H
	#define VELEMENT_H

	struct vardata_t
	{
		char * name;
		double value;
	};

	struct var_t
	{
		vardata_t data;

		var_t * prev;
		var_t * next;
	};

	//------------------------------------------------------------

	vardata_t & getdata( var_t * & e );

	char * & getname( vardata_t & data );
	char * & getname( var_t * & e );

	double & getvalue( vardata_t & data );
	double & getvalue( var_t * & e );

	//------------------------------------------------------------

	var_t * & getnext( var_t * & e );
	var_t * & gotonext( var_t * & e );

	var_t * & getprev( var_t * & e );
	var_t * & gotoprev( var_t * & e );

#endif