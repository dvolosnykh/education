#ifndef VLIST_H
	#define VLIST_H

	#include "vars\velement.h"

	struct varlist_t
	{
		var_t * first;
		var_t * last;
	};

	//------------------------------------------------------------

	unsigned isempty( varlist_t & L );

	var_t * add( varlist_t & L, vardata_t & x );
	void remove( varlist_t & L, vardata_t & x );

	unsigned isinlist( char * & x, varlist_t & L );
	unsigned isinlist( vardata_t & x, varlist_t & L );

	var_t * search( char * & x, varlist_t & L );
	var_t * search( vardata_t & x, varlist_t & L );

	varlist_t & listcpy( varlist_t & dest, varlist_t & src );

	//------------------------------------------------------------

	void init_list( varlist_t & L );
	void clear_list( varlist_t & L );

	//------------------------------------------------------------

	int comp( vardata_t & a, vardata_t & b );
	vardata_t & datacpy( vardata_t & src, vardata_t & dest );
	var_t * new_element( vardata_t & x );
	void delete_element( var_t * & e );

	//------------------------------------------------------------

	var_t * & getfirst( varlist_t & L );
	var_t * & getlast( varlist_t & L );

#endif