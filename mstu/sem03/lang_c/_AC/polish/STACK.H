#ifndef STACK_H
	#define STACK_H

	#include "selem.h"
	#include "lexem.h"

	struct stack_t
	{
		selem_t * top;
	};

	void push( stack_t & stack, lexem_t & lexem );
	lexem_t pop( stack_t & stack );

	unsigned isempty( stack_t & stack );

	void init_stack( stack_t & stack);
	void clear_stack( stack_t & stack );

	selem_t * & gettop( stack_t & stack );

#endif