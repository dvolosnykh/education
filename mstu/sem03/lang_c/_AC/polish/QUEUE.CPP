#include <stdlib.h>

#include "queue.h"

#include "selem.h"
#include "lexem.h"

void add( queue_t & queue, lexem_t & lexem )
{
	qelem_t * added = new qelem_t;
	getlexem( added ) = lexem;
	getnext( added ) = NULL;

	getlast( queue ) = ( isempty( queue ) ? getfirst( queue ) : 
			getnext( getlast( queue ) ) ) = added;
}

lexem_t serve( queue_t & queue )
{
	lexem_t served = getlexem( getfirst( queue ) );

	qelem_t * temp = getfirst( queue );
	gotonext( getfirst( queue ) );
	delete temp;

	return served;
}

unsigned isempty( queue_t & queue )
{
	return getfirst( queue ) == NULL;
}

void init_queue( queue_t & queue )
{
	getlast( queue ) = getfirst( queue ) = NULL;
}

void clear_queue( queue_t & queue )
{
	for ( ; !isempty( queue ); serve( queue ) );
}

qelem_t * & getfirst( queue_t & queue )
{
	return queue.first;
}

qelem_t * & getlast( queue_t & queue )
{
	return queue.last;
}