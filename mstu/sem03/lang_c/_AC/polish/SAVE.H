#ifndef SAVE_H
	#define SAVE_H

	#include "stack.h"
	#include "queue.h"
	#include "funcs\flist.h"
	#include "vars\vlist.h"

	void savefunc( stack_t & stack, char * x, funclist_t & Funcs,
			unsigned & binary );
	void savevar( queue_t & queue, char * x, varlist_t & Vars );
	void savenumber( queue_t & queue, double x );
	void saveopenbracket( stack_t & stack );

	unsigned funcpriority( char * x, funclist_t & Funcs);

#endif