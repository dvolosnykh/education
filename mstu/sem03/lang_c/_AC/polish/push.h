#ifndef PUSH_H
	#define PUSH_H

	#include "stack.h"
	#include "queue.h"

	void pushoutall( queue_t & queue, stack_t & stack );
	void pushout( queue_t & queue, stack_t & stack,
			const unsigned priority );
	void pushoutbrackets( queue_t & queue, stack_t & stack );

#endif