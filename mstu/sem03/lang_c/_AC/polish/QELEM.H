#ifndef QELEM_H
	#define QELEM_H

	#include "lexem.h"

	struct qelem_t
	{
		lexem_t lexem;
		qelem_t * next;
	};

	lexem_t & getlexem( qelem_t * & element );

	qelem_t * & getnext( qelem_t * & element );
	qelem_t * & gotonext( qelem_t * & element );

#endif