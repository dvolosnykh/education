#include "selem.h"

#include "lexem.h"

lexem_t & getlexem( selem_t * & element )
{
	return element->lexem;
}

selem_t * & getprev( selem_t * & element )
{
	return element->prev;
}

selem_t * & gotoprev( selem_t * & element )
{
	return element = element->prev;
}