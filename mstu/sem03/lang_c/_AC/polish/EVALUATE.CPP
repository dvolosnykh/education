#include <math.h>
#include <string.h>

#include "evaluate.h"

#include "stack.h"
#include "queue.h"
#include "lexem.h"
#include "read.h"

double evaluate( queue_t & queue )
{
	stack_t stack;  init_stack( stack );

	for (; !isempty( queue ); )
	{
		lexem_t lexem = serve( queue );

		switch ( getcode( lexem ) )
		{
		case number: worknum( stack, lexem ); break;
		case function: workfunc( stack, lexem ); break;
		}
	}

	double result = getvalue( pop( stack ) );

	clear_stack( stack );

	return result;
}

void worknum( stack_t & stack, lexem_t & num )
{
	push( stack, num );
}

void workfunc( stack_t & stack, lexem_t & func )
{
	lexem_t result = pop( stack );

	getvalue( result ) = getbinary( func )
		? calc( getvalue( pop( stack ) ), getvalue( result ),
			getname( func ) )
		: calc( getvalue( result ), getname( func ) );

	push( stack, result );
}

double calc( const double & a, const double & b, char * & func )
{
	return  thatis( func, PLUS )      ?  a + b
			: thatis( func, MINUS )     ?  a - b
			: thatis( func, MULTIPLY )  ?  a * b
			: thatis( func, DIVIDE )    ?  a / b
			: thatis( func, POWER )     ?  pow( a, b )
			: 0;
}

double calc( const double & a, char * & func )
{
	return  thatis( func, MINUS )  ?  -a
			: thatis( func, SIN )    ?  sin( a )
			: 0;
}

unsigned thatis( char * a, char * b )
{
	return strcmp( a, b ) == 0;
}