#include "qelem.h"

#include "lexem.h"

lexem_t & getlexem( qelem_t * & element )
{
	return element->lexem;
}

qelem_t * & getnext( qelem_t * & element )
{
	return element->next;
}

qelem_t * & gotonext( qelem_t * & element )
{
	return element = element->next;
}