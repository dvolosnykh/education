#ifndef FELEMENT_H
	#define FELEMENT_H

	struct funcdata_t
	{
		char * name;
		unsigned priority;
	};

	struct func_t
	{
		funcdata_t data;

		func_t * prev;
		func_t * next;
	};

	//------------------------------------------------------------

	funcdata_t & getdata( func_t * & e );

	char * & getname( funcdata_t & data );
	char * & getname( func_t * & e );

	unsigned & getpriority( funcdata_t & e );
	unsigned & getpriority( func_t * & e );

	//------------------------------------------------------------

	func_t * & getnext( func_t * & e );
	func_t * & gotonext( func_t * & e );

	func_t * & getprev( func_t * & e );
	func_t * & gotoprev( func_t * & e );

#endif