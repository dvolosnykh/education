#ifndef CHOICES_H
	#define CHOICES_H

	#define YES 'y'
	#define NO  'n'

	unsigned yesno( const char wanted );

#endif