#include <conio.h>
#include <stdio.h>
#include <io.h>

#include "files.h"

unsigned recnum( char * filename, const unsigned recsize )
{
	FILE * temp = fopen( filename, "rb" );
	unsigned result = filelength( fileno( temp ) ) / recsize;
	fclose( temp );

	return result;
}

void add( char * filename, void * record, const unsigned recsize )
{
	FILE * dest = fopen( filename, "ab" );
	fwrite( record, recsize, 1, dest );
	fclose( dest );
}

unsigned isempty( char * filename )
{
	FILE * stream = fopen( filename, "rb" );
	unsigned size = filelength( fileno( stream ) );
	fclose( stream );

	return size == 0;
}