#ifndef VIEWC_H
	#define VIEWC_H

	#include "customer\customer.h"
	#include "date.h"

	void view_customers();
	void sum_for_period( customer_t customer, date_t & low, date_t & high );

#endif