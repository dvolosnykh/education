#ifndef DATE_H
	#define DATE_H

	struct date_t
	{
		long day;
		long month;
		long year;
	};

	void print_date( date_t & dates );
	date_t & strtodate( date_t & dates, char * str );
	date_t scan_date();

	int datecmp( date_t & a, date_t & b );
	unsigned isinrange( date_t & x, date_t & low, date_t & high );

	long & get_day( date_t & dates );
	long & get_month( date_t & dates );
	long & get_year( date_t & dates );

#endif