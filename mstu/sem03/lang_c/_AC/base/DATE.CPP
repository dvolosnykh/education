#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

#include "date.h"

/*
char * datetostr( char * str, date_t & dates )
{
	char * store = str;

	*str = '0';
	str += get_day( dates ) < 10 ? 1 : 0;
	itoa( get_day( dates ), str, 10 );

	*( str += 2 ) = '.';

	*++str = '0';
	str += get_month( dates ) < 10 ? 1 : 0;
	itoa( get_month( dates ), str, 10 );

	*( str += 2 ) = '.';

	itoa( get_year, ++str, 10 );

	return store;
}
*/

void print_date( date_t & dates )
{
	printf( "%02lu.%02lu.%04lu", get_day( dates ),
		get_month( dates ), get_year( dates ) );
}

date_t & strtodate( date_t & dates, char * str )
{
	get_day( dates ) = strtol( str, & str, 10 );
	str++;
	get_month( dates ) = strtol( str, & str, 10 );
	str++;
	get_year( dates ) = strtol( str, & str, 10 );

	return dates;
}

date_t scan_date()
{
	date_t dates;
	char temp[11];
   flushall();
	gets( temp ) [10] = '\0';

	return strtodate( dates, temp );
}

int daycmp( date_t & a, date_t & b )
{
	return get_day( a ) - get_day( b );
}

int monthcmp( date_t & a, date_t & b )
{
	return get_month( a ) - get_month( b );
}

int yearcmp( date_t & a, date_t & b )
{
	return get_year( a ) - get_year( b );
}

int datecmp( date_t & a, date_t & b )
{
	int result = yearcmp( a, b );
	if ( result == 0 ) result = monthcmp( a, b );
	if ( result == 0 ) result = daycmp( a, b );

	return result;
}

unsigned isinrange( date_t & x, date_t & low, date_t & high )
{
	return datecmp( low, x ) <= 0 && datecmp( x, high ) <= 0;
}

long & get_day( date_t & dates )
{
	return dates.day;
}

long & get_month( date_t & dates )
{
	return dates.month;
}

long & get_year( date_t & dates )
{
	return dates.year;
}