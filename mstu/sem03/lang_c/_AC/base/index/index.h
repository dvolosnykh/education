#ifndef INDEX_H
	#define INDEX_H

	#define FILE_INDEX "data\\index.dat"

	struct index_t
	{
		unsigned num;
		unsigned goods_num;
		unsigned quantity;
	};

	const unsigned SIZE_INDEX = sizeof( index_t );


	unsigned & get_num( index_t & index );
	unsigned & get_goods_num( index_t & index );
	unsigned & get_quantity( index_t & index );

#endif