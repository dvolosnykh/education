#ifndef FILES_H
	#define FILES_H

	unsigned recnum( char * filename, const unsigned recsize );
	void add( char * filename, void * record, const unsigned recsize );
	unsigned isempty( char * filename );

#endif