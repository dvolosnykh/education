#ifndef BILL_H
	#define BILL_H

	#include "date.h"

	#define FILE_BILLS "data\\bills.dat"

	struct bill_t
	{
		unsigned num;
		unsigned customer_num;
		date_t billdate;
	};

	const unsigned SIZE_BILL = sizeof( struct bill_t );


	void add_bill();
	void view( bill_t & bill );

	unsigned & get_num( bill_t & bill );
	unsigned & get_customer_num( bill_t & bill );
	date_t & get_date( bill_t & bill );

#endif