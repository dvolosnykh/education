#include <stdio.h>
#include <conio.h>

#include "tools\refresh.h"
#include "tools\cursor.h"

void refresh( list_t & text )
{
	clrscr();

	for ( element_t * line = getfirst( text ); line; gotonext( line ) )
		puts( getvalue( line ) );
}

void refresh ( char * & str, const unsigned pos, const unsigned count )
{
	cursor_t cursor = { pos, wherey() - 1, _NORMALCURSOR };
	setcursor(cursor);

	printf ( "%s", str + pos );
	put( ' ', count );
}

void put( const char c, const int num )
{
	for ( register int i = 0; i < num; i++ )
		putch ( c );
}