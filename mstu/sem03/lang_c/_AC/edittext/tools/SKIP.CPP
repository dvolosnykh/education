#include "tools\skip.h"

unsigned skip_nonspaces_r ( unsigned & i, const char * const & str,
		const unsigned & num )
{
	for ( ; i < num && str[i] != ' '; i++ );

	return i;
}

unsigned skip_nonspaces_l ( unsigned & i, const char * const & str )
{
	for ( ; i > 0 && str[i - 1] != ' '; i-- );

	return i;
}

unsigned skip_spaces_r ( unsigned & i, const char * const & str,
		const unsigned & num )
{
	for ( ; i < num && str[i] == ' '; i++ );

	return i;
}

unsigned skip_spaces_l ( unsigned & i, const char * const & str )
{
	for ( ; i > 0 && str[i - 1] == ' '; i-- );

	return i;
}

unsigned wordleft( const unsigned pos, const char * const & str )
{
	unsigned temp = pos;

	skip_spaces_l( temp, str );
	skip_nonspaces_l( temp, str );

	return pos - temp;
}

unsigned wordright( const unsigned pos, const char * const & str,
		const unsigned & num )
{
	unsigned temp = pos;

	skip_nonspaces_r( temp, str, num );
	skip_spaces_r( temp, str, num );

	return temp - pos;
}