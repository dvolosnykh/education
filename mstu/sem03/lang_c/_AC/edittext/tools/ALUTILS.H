#ifndef ALUTILS_H
	#define ALUTILS_H

	unsigned countwords( const char * const & str, const unsigned & num );

	void trim( char * const & str, unsigned & num );
	void trim_l( char * const & str, unsigned & num );
	void trim_r( char * const & str, unsigned & num );

	char * strcpy_b( char * const & str, const unsigned count,
			const unsigned num );

#endif
