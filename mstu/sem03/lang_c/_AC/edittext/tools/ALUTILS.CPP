#include <conio.h>
#include <stdio.h>
#include <string.h>

#include "tools\alutils.h"

#include "tools\skip.h"

unsigned countwords( const char * const & str, const unsigned & num )
{
	unsigned count = 0;

	for ( unsigned i = 0; skip_spaces_r( i, str, num ) < num; )
	{
		skip_nonspaces_r( i, str, num );
		count++;
	}

	return count;
}

void trim( char * const & str, unsigned & num )
{
	trim_r( str, num );
	trim_l( str, num );
}

void trim_l( char * const & str, unsigned & num )
{
	unsigned s = skip_spaces_r( s = 0, str, num );

	strcpy( str, str + s );

	num -= s;
}

void trim_r( char * const & str, unsigned & num )
{
	str[ num = skip_spaces_l( num, str ) ] = '\0';
}

char * strcpy_b( char * const & str, const unsigned n,
		const unsigned num )
{
	for ( register int i = num; i >= 0; i-- )
		str[i + n] = str[i];

	return str + n;
}