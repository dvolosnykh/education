#include <stdio.h>

#include "file\load.h"

#include "tools\input.h"
#include "list\list.h"
#include "max.h"

void load( list_t & text, char * filename )
{
	FILE * src = fopen( filename, "rt" );

	data_t line;
	getvalue( line ) = new char [ XMAX + 1 ];

	for ( ; fgets( getvalue( line ), XMAX + 1, src ); )
		add( text, line, getlast( text ) );

	fclose( src );
}