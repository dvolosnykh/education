#include <alloc.h>

#include "fmatrix.h"

float * * new_matrix (const unsigned m, const unsigned n)
{
	float * * A = new float * [m];
	for (register unsigned i = 0; i < m; i++)
		A[i] = new float [n];

	return A;
}

void delete_matrix (float * * A, const unsigned m)
{
	for (register unsigned i = 0; i < m; i++)
		delete A[i];
	delete A;
}

float** matrixcpy (float ** & dest, const float * const * const &src,
		const unsigned m, const unsigned n)
{
	for (register unsigned i = 0; i < m; i++)
		for (register unsigned j = 0; j < n; j++)
			dest[i][j] = src[i][j];

	return dest;
}

void columncpy (float** const &dest, const float** const &src,
		const unsigned j, const unsigned n)
{
	for (register unsigned i = 0; i < n; i++)
			dest[i][j] = src[i][n];
}