#include <math.h>

#include "det.h"
#include "fmatrix.h"

float det (const float** const &A, const unsigned &n)
{
	float d;

	if (n > 1)
	{
		float** minor = new_matrix(n - 1, n - 1);

		d = 0;
		for (register unsigned i = 0; i < n; i++)
			d += A[i][0] * msign(i, 0) * det(Mij(minor, i, 0, A, n), n - 1);

		delete_matrix(minor, n - 1);
	}
	else
		d = A[0][0];

	return d;
}

float** Mij (float** const minor, const unsigned excepti,
		const unsigned exceptj, const float** const src,
		const unsigned n)
{
	for (register unsigned i = 0, mi = 0; i < n; i++)
		if (i != excepti)
		{
			for (register unsigned j = 0, mj = 0; j < n; j++)
				if (j != exceptj)
					minor[mi][mj++] = src[i][j];

			mi++;
		}

	return minor;
}

int msign (const unsigned i, const unsigned j)
{
	return 1 - 2 * ((i + j) % 2);
}