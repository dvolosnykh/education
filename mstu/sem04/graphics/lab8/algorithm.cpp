#include "stdafx.h"
#include "algorithm.h"
#include "myLine.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define M_PI	3.14159265358979323846


typedef CPoint vector_t;
typedef vector< CPoint > points_t;
typedef int ( *cmp_t )( const void * p1, const void * p2 );


void DrawLine( CDC * pDC, const CmyLine & line, const COLORREF & color )
{
	CPen pencil( PS_SOLID, 1, color );
	CGdiObject * pOldPen = pDC->SelectObject( &pencil );

	pDC->MoveTo( line.dot1 );
	pDC->LineTo( line.dot2 );
	pDC->SetPixel( line.dot2, color );

	pDC->SelectObject( pOldPen );
}

void DrawCutter( CDC * pDC, const cutter_t & cutter, const COLORREF & color,
				bool closed /*= true */ )
{
	CPen pencil( PS_SOLID, 1, color );
	CGdiObject * pOldPen = pDC->SelectObject( &pencil );
	CGdiObject * pOldBrush = pDC->SelectStockObject( NULL_BRUSH );

	pDC->MoveTo( cutter[ 0 ] );
	for ( size_t i = 1; i < cutter.size(); i++ )
		pDC->LineTo( cutter[ i ] );

	if ( closed )
		pDC->LineTo( cutter[ 0 ] );

	pDC->SelectObject( pOldPen );
	pDC->SelectObject( pOldBrush );
}


static int sign( long x )
{
	return ( x < 0 ? -1 : x > 0 ? 1 : 0 );
}

static int anglesign( const CPoint & dot1, const CPoint & dot2, const CPoint & dot3 )
{
	vector_t a = dot1 - dot2;
	vector_t b = dot3 - dot2;

	return sign( b.x * a.y - a.x * b.y );
}

bool isConvex( const cutter_t & cutter )
{
	const size_t num = cutter.size();

	const int first = anglesign( cutter[ 0 ], cutter[ 1 ], cutter[ 2 ] );

	bool convex = true;
	for ( size_t i = 1; i < num; i++ )
		convex = convex && anglesign( cutter[ i ], cutter[ ( i + 1 ) % num ],
			cutter[ ( i + 2 ) % num ] ) == first;

	return ( convex );
}

static long scal( const vector_t & a, const vector_t & b )
{
	return ( a.x * b.x + a.y * b.y );
}


void AlgoKirusBeck( CDC * pDC, const lines_t & lines, const cutter_t & cutter )
{
	const int s = anglesign( cutter[ 0 ], cutter[ 1 ], cutter[ 2 ] );

	for ( size_t i = 0; i < lines.size(); i++ )
	{
		CmyLine line = lines[ i ];
		vector_t D = line.dot2 - line.dot1;

		double ti = 0;
		double tf = 1;

		bool bad = false;
		for ( size_t j = 0; j < cutter.size() && !bad; j++ )
		{
			CmyLine edge( cutter[ j ], cutter[ ( j + 1 ) % cutter.size() ] );
			const coeff_t eqn = edge.FindCoeffs();

			vector_t n = vector_t( s * eqn.A, s * eqn.B );
			vector_t W = line.dot1 - cutter[ j ];

			const long Dsc = scal( D, n );
			const long Wsc = scal( W, n );

			if ( Dsc == 0 )
				bad = Wsc < 0;
			else
			{
				const double t = - Wsc / ( double )Dsc;

				if ( Dsc > 0 )
				{
					if ( !( bad = t > 1 ) && t > ti )
						ti = t;
				}
				else
					if ( !( bad = t < 0 ) && t < tf )
						tf = t;
			}
		}

		bad = bad || ti > tf;

		// drawing
		if ( bad )
			DrawLine( pDC, line, out_color );
		else
		{
			CmyLine cutted( line.FixPoint( ti ), line.FixPoint( tf ) );

			DrawLine( pDC, CmyLine( line.dot1, cutted.dot1 ), out_color );
			DrawLine( pDC, CmyLine( cutted.dot2, line.dot2 ), out_color );
			DrawLine( pDC, cutted, in_color );
		}
	}
}


static bool insegment( const CPoint & dot, const CmyLine & line )
{
	const long xmin = min( line.dot1.x, line.dot2.x );
	const long xmax = max( line.dot1.x, line.dot2.x );
	const long ymin = min( line.dot1.y, line.dot2.y );
	const long ymax = max( line.dot1.y, line.dot2.y );
	
	return ( xmin <= dot.x && dot.x <= xmax &&
		ymin <= dot.y && dot.y <= ymax );
}

static int qcmpx( const void * p1, const void * p2 )
{
	const CPoint & dot1 = *( CPoint * )p1;
	const CPoint & dot2 = *( CPoint * )p2;

	return ( dot1.x - dot2.x );
}

static int qcmpy( const void * p1, const void * p2 )
{
	const CPoint & dot1 = *( CPoint * )p1;
	const CPoint & dot2 = *( CPoint * )p2;

	return ( dot1.y - dot2.y );
}

static cmp_t defqcmp( CmyLine & line )
{
	return ( line.IsVertical() || fabs( line.Inclination() ) > M_PI / 4 ? qcmpy : qcmpx );
}

static bool inlimits( CmyLine & edge, const long y )
{
	const long ymin = min( edge.dot1.y, edge.dot2.y );
	const long ymax = max( edge.dot1.y, edge.dot2.y );

	return ( ymin < y && y <= ymax );
}

static long round( const double x )
{
	return ( long )( x + 0.5 );
}

static long findX( const long y, const CmyLine & line )
{
	const CPoint & dot1 = line.dot1;
	const CPoint & dot2 = line.dot2;

	return round( ( y - dot1.y ) * ( dot2.x - dot1.x ) /
		( double )( dot2.y - dot1.y ) + dot1.x );
}

static bool isborder( CmyLine & edge, const CPoint & dot )
{
	return ( !edge.IsHorizontal() && inlimits( edge, dot.y )
		&& findX( dot.y, edge ) < dot.x );
}

static bool extremum( CmyLine & line, CmyLine & edge1, CmyLine& edge2 )
{
	const coeff_t eqn = line.FindCoeffs();

	const int s1 = sign( eqn.A * edge1.dot1.x + eqn.B * edge1.dot1.y + eqn.C );
	const int s2 = sign( eqn.A * edge2.dot2.x + eqn.B * edge2.dot2.y + eqn.C );

	return ( s1 == s2 );
}

void AlgoSmth( CDC * pDC, const lines_t & lines, const cutter_t & cutter )
{
	for ( size_t i = 0; i < lines.size(); i++ )
	{
		CmyLine line = lines[ i ];
		const cmp_t qcmp = defqcmp( line );
		const CPoint & testdot = qcmp( &line.dot1, &line.dot2 ) < 0 ? line.dot1 : line.dot2;

		points_t points;
		points.push_back( line.dot1 );
		points.push_back( line.dot2 );

		bool inside = false;
		for ( size_t j = 0; j < cutter.size(); j++ )
		{
			CmyLine cur( cutter[ j ], cutter[ ( j + 1 ) % cutter.size() ] );

			if ( !line.IsParallelTo( cur ) )
			{
				CPoint I = line.IntersectWith( cur );

				if ( insegment( I, cur ) && insegment( I, line ) )
				{
					CmyLine next( cutter[ ( j + 1 ) % cutter.size() ],
						cutter[ ( j + 2 ) % cutter.size() ] );

					if ( !( I == cur.dot2 && extremum( line, cur, next ) )
							&& I != cur.dot1 )
						points.push_back( I );
				}
			}

			if ( isborder( cur, testdot ) )
				inside = !inside;
		}

		qsort( &points.front(), points.size(), sizeof( points.front() ), qcmp );

		// drawing
		for ( size_t i = 1; i < points.size(); i++ )
		{
			DrawLine( pDC, CmyLine( points[ i - 1 ], points[ i ] ),
				inside ? in_color : out_color );
			inside = !inside;
		}
	}
}
