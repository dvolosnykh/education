#include "stdafx.h"

#include <math.h>
#include "myLine.h"

#define PI	3.141592653589793

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//CmyLine

CmyLine::CmyLine()
{
}

CmyLine::CmyLine( const CPoint p1, const CPoint p2 )
{
	dot1 = p1;
	dot2 = p2;
}

CmyLine::~CmyLine()
{
}

long CmyLine::Ymin()
{
	return min( dot1.y, dot2.y );
}

long CmyLine::Ymax()
{
	return max( dot1.y, dot2.y );
}

long CmyLine::Xmin()
{
	return min( dot1.x, dot2.x );
}

long CmyLine::Xmax()
{
	return max( dot1.x, dot2.x );
}

double CmyLine::Length()
{
	long dx = dot2.x - dot1.x;
	long dy = dot2.y - dot1.y;

	return ( hypot( dx, dy ) );
}

bool CmyLine::IsValid()
{
	return ( Length() > 0 );
}

bool CmyLine::IsVertical()
{
	return ( dot2.x == dot1.x );
}

bool CmyLine::IsHorizontal()
{
	return ( dot2.y == dot1.y );
}

bool CmyLine::IsInclined()
{
	return ( !IsVertical() && !IsHorizontal() );
}

double CmyLine::Inclination()
{
	return atan( ( dot2.y - dot1.y ) / ( double )( dot2.x - dot1.x ) );
}

bool CmyLine::IsParallelTo( CmyLine & line )
{
	return ( line.IsVertical() && IsVertical() || 
		line.IsHorizontal() && IsHorizontal() || 
		line.Inclination() == Inclination() );
}

coeff_t CmyLine::FindCoeffs()
{
	coeff_t eqn;

	eqn.A = dot1.y - dot2.y;
	eqn.B = dot2.x - dot1.x;
	eqn.C = dot1.x * dot2.y - dot2.x * dot1.y;

	return ( eqn );
}

static long round( const double x )
{
	return ( long )( x + 0.5 );
}

CPoint CmyLine::FixPoint( const double t )
{
	return CPoint( round( dot1.x + t * ( dot2.x - dot1.x ) ),
		round( dot1.y + t * ( dot2.y - dot1.y ) ) );
}

CPoint CmyLine::IntersectWith( CmyLine & line )
{
	const coeff_t l1 = FindCoeffs();
	const coeff_t l2 = line.FindCoeffs();

	CPoint I;
	I.x = round( ( double )( l1.C * l2.B - l1.B * l2.C ) /
		( l1.B * l2.A - l1.A * l2.B ) );
	I.y = round( ( double )( l1.C * l2.A - l1.A * l2.C ) /
		( l1.A * l2.B - l1.B * l2.A ) );

	return ( I );
}

void CmyLine::Draw( CDC * pDC )
{
	pDC->MoveTo( dot1 );
	pDC->LineTo( dot2 );
}

CmyLine CmyLine::operator = ( const CmyLine & line )
{
	dot1 = line.dot1;
	dot2 = line.dot2;

	return ( *this );
}

bool CmyLine::operator == ( const CmyLine & line )
{
	return ( dot1 == line.dot1 && dot2 == line.dot2 );
}

bool CmyLine::operator != ( const CmyLine & line )
{
	return !( *this == line );
}