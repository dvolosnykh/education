#include "stdafx.h"
#include "algorithm.h"
#include "myLine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static enum type_t
{
	VERTICAL,
	INCLINED,
	HORIZONTAL
};

static enum vision_t
{
	VISIBLE,
	PARTICIALLY,
	INVISIBLE
};


void DrawLine( CDC * pDC, const CmyLine & line, const COLORREF & color )
{
	CPen pencil( PS_SOLID, 1, color );
	CGdiObject * pOldPen = pDC->SelectObject( &pencil );

	pDC->MoveTo( line.dot1 );
	pDC->LineTo( line.dot2 );
	pDC->SetPixel( line.dot2, color );

	pDC->SelectObject( pOldPen );
}

void DrawCutter( CDC * pDC, const CRect & cutter, const COLORREF & color )
{
	CPen pencil( PS_SOLID, 1, color );
	CGdiObject * pOldPen = pDC->SelectObject( &pencil );
	CGdiObject * pOldBrush = pDC->SelectStockObject( NULL_BRUSH );

	pDC->Rectangle( cutter );

	pDC->SelectObject( pOldPen );
	pDC->SelectObject( pOldBrush );
}

static int sign( long x )
{
	return ( x < 0 ? -1 : x > 0 ? 1 : 0 );
}

static BOOL outside( CmyLine & line, const CRect & cutter )
{
	const long & xmin = cutter.left;
	const long & xmax = cutter.right;
	const long & ymin = cutter.top;
	const long & ymax = cutter.bottom;

	const coeff_t eqn = line.FindCoeffs();

	const long xminA = eqn.A * xmin;
	const long xmaxA = eqn.A * xmax;
	const long yminB = eqn.B * ymin;
	const long ymaxB = eqn.B * ymax;

	const int s[ 4 ] =
	{
		sign( xminA + yminB + eqn.C ),
		sign( xminA + ymaxB + eqn.C ),
		sign( xmaxA + yminB + eqn.C ),
		sign( xmaxA + ymaxB + eqn.C )
	};

	BOOL out = true;
	for ( unsigned i = 1; i < 4; i++ )
		out = out && s[ i ] == *s;

	return ( out );
}

static vision_t testline( CmyLine & line, const CRect & cutter )
{
	BOOL v1 = cutter.PtInRect( line.dot1 );
	BOOL v2 = cutter.PtInRect( line.dot2 );

	return ( v1 && v2 ? VISIBLE :
		!v1 && !v2 && outside( line, cutter ) ? INVISIBLE :
		PARTICIALLY );
}

static long round( double x )
{
	return ( long )( x + 0.5 );
}

void AlgoSazerlendKoen( CDC * pDC, lines_t & lines, const CRect & cutter )
{
	const long & xmin = cutter.left;
	const long & xmax = cutter.right;
	const long & ymin = cutter.top;
	const long & ymax = cutter.bottom;

	const long O[ 4 ] = { xmin, xmax - 1, ymin, ymax - 1 };

	for ( size_t i = 0; i < lines.size(); i++ )
	{
		CmyLine line = lines[ i ];
		CPoint & dot1 = line.dot1;
		CPoint & dot2 = line.dot2;

		const type_t type = line.IsVertical() ? VERTICAL :
			line.IsHorizontal() ? HORIZONTAL : INCLINED;

		double m;
		if ( type == INCLINED )
			m = ( dot2.y - dot1.y ) / ( double )( dot2.x - dot1.x );

		switch ( testline( line, cutter ) )
		{
		case VISIBLE:
			DrawLine( pDC, line, in_color );
			break;
		case INVISIBLE:
			DrawLine( pDC, line, out_color );
			break;
		case PARTICIALLY:
			{
			for ( unsigned j = 0; j < 4; j++ )
			{
				// code for dot1
				const BOOL t1[ 4 ] =
				{
					dot1.x < xmin, dot1.x > xmax,
					dot1.y < ymin, dot1.y > ymax
				};

				// code for dot2
				const BOOL t2[ 4 ] =
				{
					dot2.x < xmin, dot2.x > xmax,
					dot2.y < ymin, dot2.y > ymax
				};

				if ( t1[ j ] != t2[ j ] )
				{
					CPoint & dot = t1[ j ] == 0 ? dot2 : dot1;

					switch( type )
					{
						case VERTICAL:
							dot.y = O[ j ];
							break;
						case HORIZONTAL:
							dot.x = O[ j ];
							break;
						case INCLINED:
							if ( j < 2 )
							{
								dot.y = round( dot.y + ( O[ j ] - dot.x ) * m );
								dot.x = O[ j ];
							}
							else
							{
								dot.x = round( dot.x + ( O[ j ] - dot.y ) / m );
								dot.y = O[ j ];
							}
							break;
					}
				}
			}

			DrawLine( pDC, CmyLine( lines[ i ].dot1, line.dot1 ), out_color );
			DrawLine( pDC, CmyLine( line.dot2, lines[ i ].dot2 ), out_color );
			DrawLine( pDC, line, in_color );
			}
			break;
		}
	}

	DrawCutter( pDC, cutter, cutter_color );
}