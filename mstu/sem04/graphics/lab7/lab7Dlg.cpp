// lab7Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab7.h"
#include "lab7Dlg.h"
#include ".\lab7dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static enum
{
	INP_NONE,
	INP_LINE,
	INP_CUTTER
} input;

static CMetaFileDC * pmfsave;
static CPoint mouse;  // coordinates of mouse pointer


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// Clab7Dlg dialog



Clab7Dlg::Clab7Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(Clab7Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Clab7Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control( pDX, IDC_PICTURE, m_picture );
}

BEGIN_MESSAGE_MAP(Clab7Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CUT, OnBnClickedCut)
	ON_BN_CLICKED(IDC_RESET, OnBnClickedReset)
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
END_MESSAGE_MAP()


// Clab7Dlg message handlers

BOOL Clab7Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	OnBnClickedReset();
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Clab7Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Clab7Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Clab7Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void Clab7Dlg::OnBnClickedCut()
{
	m_picture.Clear();

	AlgoSazerlendKoen( m_picture.pmf, lines, cutter );

	m_picture.Invalidate( false );
}

void Clab7Dlg::OnBnClickedReset()
{
	cutter.SetRectEmpty();

	input = INP_NONE;
	if ( !lines.empty() )
	{
		lines.clear();

		HMETAFILE hmfsave = pmfsave->Close();
		DeleteMF( pmfsave, hmfsave );
	}

	m_picture.Clear();
	CRect area( m_picture.m_DPrect );
	area.DeflateRect( 1, 1, 1, 1 );
	m_picture.pmf->IntersectClipRect( area );
	m_picture.Invalidate( false );

	pmfsave = NewMF( m_picture.pmf );
}

void Clab7Dlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) && input != INP_CUTTER )
	{
		if ( input == INP_NONE )
		{
			lines.push_back( CmyLine( point, CPoint() ) );

			input = INP_LINE;
		}
		else
		{
			// reseting picture
			ResetMF( m_picture.pmf );
			PlayMF( m_picture.pmf, pmfsave );

			DrawLine( m_picture.pmf, lines.back(), def_color );

			// saving current picture
			ResetMF( pmfsave );
			PlayMF( pmfsave, m_picture.pmf );

			if ( !cutter.IsRectNull() )
				DrawCutter( m_picture.pmf, cutter, cutter_color );

			input = INP_NONE;
		}
	}

	CDialog::OnLButtonDown(nFlags, point);
}

void Clab7Dlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) && input != INP_LINE )
	{
		if ( input == INP_NONE )
		{
			cutter.left = point.x;
			cutter.top = point.y;

			input = INP_CUTTER;
		}
		else
		{
			if ( cutter.Width() < 0 )
				swap( cutter.left, cutter.right );

			if ( cutter.Height() < 0 )
				swap( cutter.top, cutter.bottom );

			input = INP_NONE;
		}
	}

	CDialog::OnRButtonDown(nFlags, point);
}

static void swap( long & a, long & b )
{
	long temp = a;
	a = b;
	b = temp;
}

static void MakeHorVer( CPoint & point, const CPoint & dot1 )
{
	if ( abs( point.x - dot1.x ) < abs( point.y - dot1.y ) )
		point.x = dot1.x;
	else
		point.y = dot1.y;
}

void Clab7Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	mouse = point;
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) && input != INP_NONE )
	{
		// reseting picture
		ResetMF( m_picture.pmf );
		PlayMF( m_picture.pmf, pmfsave );

		switch ( input )
		{
		case INP_LINE:
			{
			// for drawing hor/vert segments
			if ( nFlags == MK_CONTROL && !lines.empty() )
				MakeHorVer( point, lines.back().dot1 );

			lines.back().dot2 = point;
			DrawLine( m_picture.pmf, lines.back(), def_color );

			if ( !cutter.IsRectNull() )
				DrawCutter( m_picture.pmf, cutter, cutter_color );
			}
			break;
		case INP_CUTTER:
			{
			cutter.right = point.x + 1;
			cutter.bottom = point.y + 1;
			
			DrawCutter( m_picture.pmf, cutter, cutter_color );
			}
			break;
		}

		m_picture.SetFocus();
		m_picture.Invalidate( false );
	}

	CDialog::OnMouseMove(nFlags, point);
}

void Clab7Dlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if ( nChar == VK_CONTROL )
		OnMouseMove( MK_CONTROL, mouse );

	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}

void Clab7Dlg::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if ( nChar == VK_CONTROL )
		OnMouseMove( 0, mouse );

	CDialog::OnKeyUp(nChar, nRepCnt, nFlags);
}
