#pragma once

#include "myLine.h"
#include <vector>

using namespace std;

const COLORREF cutter_color = RGB( 160, 160, 255 );
const COLORREF    def_color = RGB(   0,   0,   0 );
const COLORREF    out_color = RGB( 192, 192, 192 );
const COLORREF     in_color = RGB( 255,   0,   0 );

typedef vector< CmyLine > lines_t;

void DrawLine( CDC * pDC, const CmyLine & line, const COLORREF & color );
void DrawCutter( CDC * pDC, const CRect & cutter, const COLORREF & color );

void AlgoSazerlendKoen( CDC * pDC, lines_t & lines, const CRect & cutter );