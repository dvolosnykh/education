#include "stdafx.h"
#include "lab2.h"
#include "lab2Dlg.h"
#include ".\lab2dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


void Clab2Dlg::VisibleTabOffset( bool visible /*= true*/ )
{
	m_dx.ShowWindow( visible );
	m_dy.ShowWindow( visible );
	m_st_dx.ShowWindow( visible );
	m_st_dy.ShowWindow( visible );
}

void Clab2Dlg::VisibleTabRotate( bool visible /*= true*/ )
{
	m_cx.ShowWindow( visible );
	m_cy.ShowWindow( visible );
	m_phi.ShowWindow( visible );
	m_st_cx.ShowWindow( visible );
	m_st_cy.ShowWindow( visible );
	m_st_phi.ShowWindow( visible );
}

void Clab2Dlg::VisibleTabScale( bool visible /*= true*/ )
{
	m_mx.ShowWindow( visible );
	m_my.ShowWindow( visible );
	m_kx.ShowWindow( visible );
	m_ky.ShowWindow( visible );
	m_st_mx.ShowWindow( visible );
	m_st_my.ShowWindow( visible );
	m_st_kx.ShowWindow( visible );
	m_st_ky.ShowWindow( visible );
}

void Clab2Dlg::VisibleTab( UINT nTab )
{
	switch ( nTab )
	{
	case TAB_OFFSET:
		VisibleTabOffset( true );
		VisibleTabRotate( false );
		VisibleTabScale( false );
		break;
	case TAB_ROTATE:
		VisibleTabOffset( false );
		VisibleTabRotate( true );
		VisibleTabScale( false );
		break;
	case TAB_SCALE:
		VisibleTabOffset( false );
		VisibleTabRotate( false );
		VisibleTabScale( true );
		break;
	}
}