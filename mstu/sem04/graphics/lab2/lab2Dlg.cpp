// lab2Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab2.h"
#include "lab2Dlg.h"
#include ".\lab2dlg.h"

#include "stack.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// Clab2Dlg dialog



Clab2Dlg::Clab2Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(Clab2Dlg::IDD, pParent),
	m_resolution( 0 )
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

Clab2Dlg::~Clab2Dlg()
{
	Undo.Clear();
	Redo.Clear();
}

void Clab2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OPS, m_ops);

	DDX_Control(pDX, IDC_EDIT_DX, m_dx);
	DDX_Control(pDX, IDC_EDIT_DY, m_dy);
	DDX_Control(pDX, IDC_ST_DX, m_st_dx);
	DDX_Control(pDX, IDC_ST_DY, m_st_dy);

	DDX_Control(pDX, IDC_EDIT_CX, m_cx);
	DDX_Control(pDX, IDC_EDIT_CY, m_cy);
	DDX_Control(pDX, IDC_EDIT_PHI, m_phi);
	DDX_Control(pDX, IDC_ST_CX, m_st_cx);
	DDX_Control(pDX, IDC_ST_CY, m_st_cy);
	DDX_Control(pDX, IDC_ST_PHI, m_st_phi);

	DDX_Control(pDX, IDC_EDIT_MX, m_mx);
	DDX_Control(pDX, IDC_EDIT_MY, m_my);
	DDX_Control(pDX, IDC_EDIT_KX, m_kx);
	DDX_Control(pDX, IDC_EDIT_KY, m_ky);
	DDX_Control(pDX, IDC_ST_MX, m_st_mx);
	DDX_Control(pDX, IDC_ST_MY, m_st_my);
	DDX_Control(pDX, IDC_ST_KY, m_st_ky);
	DDX_Control(pDX, IDC_ST_KX, m_st_kx);

	DDX_Control(pDX, IDC_PICTURE, m_picture);
	DDX_Control(pDX, IDC_DO, m_do);
	DDX_Control(pDX, IDC_REDO, m_redo);
	DDX_Control(pDX, IDC_UNDO, m_undo);

	DDX_Radio(pDX, IDC_NORMAL, m_resolution);
}

BEGIN_MESSAGE_MAP(Clab2Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_OPS, OnTcnSelchangeOps)
	ON_BN_CLICKED(IDC_DO, OnBnClickedDo)
	ON_BN_CLICKED(IDC_RESET, OnBnClickedReset)
	ON_BN_CLICKED(IDC_UNDO, OnBnClickedUndo)
	ON_BN_CLICKED(IDC_REDO, OnBnClickedRedo)
	ON_BN_CLICKED(IDC_CHANGE, OnBnClickedChange)
END_MESSAGE_MAP()


bool ChangeScreenRes( UINT width, UINT height )
{
 	DEVMODE screen;
	EnumDisplaySettings( NULL, ENUM_CURRENT_SETTINGS, &screen );
 	screen.dmPelsWidth = width;
 	screen.dmPelsHeight = height;
	
 	return ( ChangeDisplaySettings ( &screen, CDS_FULLSCREEN ) == 
		DISP_CHANGE_SUCCESSFUL );
}

// Clab2Dlg message handlers

BOOL Clab2Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// Tab control
	m_ops.InsertItem( TAB_OFFSET, "�������" );
	m_ops.InsertItem( TAB_ROTATE, "�������" );
	m_ops.InsertItem( TAB_SCALE, "���������������" );
	m_ops.SetCurSel( TAB_OFFSET );
	SwitchTab( TAB_OFFSET );

	// Loading default data
	DefaultData();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Clab2Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Clab2Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Clab2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void Clab2Dlg::OnTcnSelchangeOps(NMHDR *pNMHDR, LRESULT *pResult)
{
	SwitchTab( m_ops.GetCurSel() );

	*pResult = 0;
}

void Clab2Dlg::OnBnClickedDo()
{
	if ( CorrectTab( m_ops.GetCurSel(), true ) )
	{
		Undo.Push( Oper( m_ops.GetCurSel() ) );
		m_undo.EnableWindow( true );

		Redo.Clear();
		m_redo.EnableWindow( false );

		m_picture.Invalidate( false );
	}
}

void Clab2Dlg::OnBnClickedReset()
{
	DefaultData();

	if ( !Undo.IsEmpty() )
	{
		m_picture.current = Undo.SaveFirstClear();
		m_undo.EnableWindow( false );
	}

	Redo.Clear();
	m_redo.EnableWindow( false );

	m_picture.Invalidate( false );
}

void Clab2Dlg::OnBnClickedUndo()
{
	if ( !Undo.IsEmpty() )
	{
		Redo.Push( m_picture.current );
		m_redo.EnableWindow( true );

		housecpy( m_picture.current, Undo.Pop() );
		m_undo.EnableWindow( !Undo.IsEmpty() );

		m_picture.Invalidate( false );
	}
}

void Clab2Dlg::OnBnClickedRedo()
{
	if ( !Redo.IsEmpty() )
	{
		Undo.Push( m_picture.current );
		m_undo.EnableWindow( true );

		housecpy( m_picture.current, Redo.Pop() );
		m_redo.EnableWindow( !Redo.IsEmpty() );

		m_picture.Invalidate( false );
	}
}

void Clab2Dlg::OnBnClickedChange()
{
	UpdateData( true );
	
	switch ( m_resolution )
	{
	case 0:
		ChangeScreenRes( 800, 600 );
		break;
	case 1:
		ChangeScreenRes( 1024, 768 );
		break;
	}
}

void Clab2Dlg::DefaultData()
{
	m_dx.SetWindowText( "10" );
	m_dy.SetWindowText( "10" );

	m_cx.SetWindowText( "250" );
	m_cy.SetWindowText( "220" );
	m_phi.SetWindowText( "60" );

	m_mx.SetWindowText( "250" );
	m_my.SetWindowText( "220" );
	m_kx.SetWindowText( "2" );
	m_ky.SetWindowText( "2" );
}