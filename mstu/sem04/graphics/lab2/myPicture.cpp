#include "stdafx.h"
#include "lab2.h"

#include "math.h"

#include "myPicture.h"
#include "myPoint.h"
#include "stack.h"


// CmyPicture

IMPLEMENT_DYNAMIC( CmyPicture, CStatic )

CmyPicture::CmyPicture()
{
}

CmyPicture::~CmyPicture()
{
}


BEGIN_MESSAGE_MAP( CmyPicture, CStatic )
	ON_WM_PAINT()
END_MESSAGE_MAP()


void ConvertDot( CmyPoint & point, oper_t & oper )
{
	dot_t p1 = { 0, 0, 0 };
	dot_t p = { point.x, point.y, 1 };
	int i;

	// converting
	for ( i = 0; i < 3; i++ )
		for ( int j = 0; j < 3; j++ )
			p1[ i ] += p[ j ] * oper[ j ][ i ];

	// saving
	point.SetPoint( p1[ 0 ], p1[ 1 ] );
}

void ConvertArray( CmyPoint * poly, int n, oper_t & oper )
{
	for ( int i = 0; i < n; i++ )
		ConvertDot( poly[ i ], oper );
} 

void RoundPoint( CPoint & ipoint, CmyPoint & point )
{
	ipoint.SetPoint( round( point.x ), round( point.y ) );
}

void RoundArray( CPoint * ipoly, CmyPoint * poly, int n )
{
	for ( int i = 0; i < n; i++ )
		RoundPoint( ipoly[ i ], poly[ i ] );
}

double findX( double a, double phi, double cx )
{
	return ( a * cos( phi ) + cx );
}

double findY( double b, double phi, double cy )
{
	return ( b * sin( phi ) + cy );
}

void approx( CmyPoint * ell, int n, ellipse_t & param,
			double start = 0, double finish = 2 * M_PI )
{
	for ( int i = 0; i < n; i++ )
	{
		double phi = start + i * ( finish - start ) / ( n - 1 );
		ell[ i ].SetPoint( findX( param.a, phi, param.c.x ), 
			findY( param.b, phi, param.c.y ) );
	}
}


house_t CmyPicture::Convert( oper_t & oper )
{
	house_t save;
	housecpy( save, current );

	ConvertArray( current.wall, 4, oper );
	ConvertArray( current.ceiling, 3, oper );
	ConvertArray( current.frame, 4, oper );
	ConvertArray( current.line1, 2, oper );
	ConvertArray( current.arc1, NARC + 1, oper );
	ConvertArray( current.line21, 2, oper );
	ConvertArray( current.line22, 2, oper );
	ConvertArray( current.ell2, 2 * NARC + 1, oper );
	ConvertArray( current.ell3, 2 * NARC + 1, oper );

	return ( save );
}

void CmyPicture::Create()
{
	// zero of drawing
	CmyPoint zero( 190, 150 );

	// wall
	current.wall[ 0 ].SetPoint( zero.x +   0, zero.y +  60 );
	current.wall[ 1 ].SetPoint( zero.x + 120, zero.y +  60 );
	current.wall[ 2 ].SetPoint( zero.x + 120, zero.y + 140 );
	current.wall[ 3 ].SetPoint( zero.x +   0, zero.y + 140 );

	// ceiling
	current.ceiling[ 0 ].SetPoint( zero.x +  60, zero.y +  0 );
	current.ceiling[ 1 ].SetPoint( zero.x + 120, zero.y + 60 );
	current.ceiling[ 2 ].SetPoint( zero.x +   0, zero.y + 60 );

	// window #1
	current.frame[ 0 ].SetPoint( zero.x + 20, zero.y +  90 );
	current.frame[ 1 ].SetPoint( zero.x + 50, zero.y +  90 );
	current.frame[ 2 ].SetPoint( zero.x + 50, zero.y + 120 );
	current.frame[ 3 ].SetPoint( zero.x + 20, zero.y + 120 );
	current.line1[ 0 ].SetPoint( zero.x + 35, zero.y +  80 );
	current.line1[ 1 ].SetPoint( zero.x + 35, zero.y + 120 );
	ellipse_t param = { 15, 10, CmyPoint( zero.x + 35, zero.y + 90 ) };
	approx( current.arc1, NARC + 1, param, M_PI, 2 * M_PI );

	// window #2
	current.line21[ 0 ].SetPoint( zero.x +  85, zero.y +  80 );
	current.line21[ 1 ].SetPoint( zero.x +  85, zero.y + 120 );
	current.line22[ 0 ].SetPoint( zero.x +  70, zero.y + 100 );
	current.line22[ 1 ].SetPoint( zero.x + 100, zero.y + 100 );
	param.a = 15;
	param.b = 20;
	param.c = CmyPoint( zero.x + 85, zero.y + 100 );
	approx( current.ell2, 2 * NARC + 1, param );

	// window #3
	param.a = 15;
	param.b = 15;
	param.c = CmyPoint( zero.x + 60, zero.y + 35 );
	approx( current.ell3, 2 * NARC + 1, param );
}

void CmyPicture::Draw( CPaintDC * pDC )
{
	// Selecting GDI objects
	CGdiObject * pOldBrush = pDC->SelectStockObject( NULL_BRUSH );
	CGdiObject * pOldPen = pDC->SelectStockObject( BLACK_PEN );

	// rounding points
	static CPoint iwall[ 4 ];
	static CPoint iceiling[ 3 ];
	static CPoint iframe[ 4 ];
	static CPoint iline1[ 2 ];
	static CPoint iarc1[ NARC + 1 ];
	static CPoint iline21[ 2 ];
	static CPoint iline22[ 2 ];
	static CPoint iell2[ 2 * NARC + 1 ];
	static CPoint iell3[ 2 * NARC + 1 ];

	RoundArray( iwall, current.wall, 4 );
	RoundArray( iceiling, current.ceiling, 3 );
	RoundArray( iframe, current.frame, 4 );
	RoundArray( iline1, current.line1, 2 );
	RoundArray( iarc1, current.arc1, NARC + 1 );
	RoundArray( iline21, current.line21, 2 );
	RoundArray( iline22, current.line22, 2 );
	RoundArray( iell2, current.ell2, 2 * NARC + 1 );
	RoundArray( iell3, current.ell3, 2 * NARC + 1 );

	// Drawing
	pDC->Polygon( iwall, 4 );
	pDC->Polygon( iceiling, 3 );
	pDC->Polygon( iframe, 4 );
	pDC->Polyline( iline1, 2 );
	pDC->Polyline( iarc1, NARC + 1 );
	pDC->Polyline( iline21, 2 );
	pDC->Polyline( iline22, 2 );
	pDC->Polygon( iell2, 2 * NARC + 1 );
	
	CBrush Hatch( HS_BDIAGONAL, RGB( 0, 0, 0 ) );
	pDC->SelectObject( &Hatch );
	pDC->Polygon( iell3, 2 * NARC + 1 );

	// Deselecting GDI objects
	pDC->SelectObject( pOldBrush );
	pDC->SelectObject( pOldPen );
}

void CmyPicture::Clear( CPaintDC * pDC )
{
	// Selecting GDI objects
	CGdiObject * pOldBrush = pDC->SelectStockObject( WHITE_BRUSH );
	CGdiObject * pOldPen = pDC->SelectStockObject( BLACK_PEN );

	// Clearing
	pDC->Rectangle( m_DPrect );

	// Deselecting GDI objects
	pDC->SelectObject( pOldBrush );
	pDC->SelectObject( pOldPen );
}


// CmyPicture message handlers

void CmyPicture::OnPaint()
{
	CPaintDC dc( this );
	Clear( &dc );

	m_DPrect.DeflateRect( 1, 1, 1, 1 );
	dc.IntersectClipRect( m_DPrect );
	m_DPrect.InflateRect( 1, 1, 1, 1 );
	
	Draw( &dc );

	// Do not call CStatic::OnPaint() for painting messages
}

void CmyPicture::PreSubclassWindow()
{
	GetClientRect( m_DPrect );
	Create();

	CStatic::PreSubclassWindow();
}
