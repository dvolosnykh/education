#include "stdafx.h"
#include "lab2.h"
#include "lab2Dlg.h"
#include ".\lab2dlg.h"
#include "myPicture.h"

#include "math.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


double edittof( CEdit & edit )
{
	const unsigned length = edit.GetWindowTextLength() + 1;
	char * temp = new char [ length ];
	edit.GetWindowText( temp, length );

	double value = atof( temp );

	delete temp;

	return ( value );
}

double DegreeToRad( double degree )
{
	return ( degree / 180 * M_PI );
}


house_t Clab2Dlg::OperOffset()
{
	double dx = edittof( m_dx );
	double dy = edittof( m_dy );

	oper_t oper = 
	{
		{  1,  0, 0 },
		{  0,  1, 0 },
		{ dx, dy, 1 }
	};

	return ( m_picture.Convert( oper ) );
}

house_t Clab2Dlg::OperRotate()
{
	double cx = edittof( m_cx );
	double cy = edittof( m_cy );
	double phi = DegreeToRad( edittof( m_phi ) );

	double cos_phi = cos( phi );
	double sin_phi = sin( phi );

	double dx = cx * ( 1 - cos_phi ) - cy * sin_phi;
	double dy = cy * ( 1 - cos_phi ) + cx * sin_phi;

	oper_t oper =
	{
		{ cos_phi, - sin_phi, 0 },
		{ sin_phi,   cos_phi, 0 },
		{      dx,        dy, 1 }
	};

	return ( m_picture.Convert( oper ) );
}

house_t Clab2Dlg::OperScale()
{
	double mx = edittof( m_mx );
	double my = edittof( m_my );
	double kx = edittof( m_kx );
	double ky = edittof( m_ky );

	double dx = ( 1 - kx ) * mx;
	double dy = ( 1 - ky ) * my;

	oper_t oper =
	{
		{ kx,  0, 0 },
		{  0, ky, 0 },
		{ dx, dy, 1 }
	};

	return ( m_picture.Convert( oper ) );
}

house_t Clab2Dlg::Oper( UINT nTab )
{
	house_t house;

	switch ( nTab )
	{
	case TAB_OFFSET:
		house = OperOffset();
		break;
	case TAB_ROTATE:
		house = OperRotate();
		break;
	case TAB_SCALE:
		house = OperScale();
		break;
	}

	return ( house );
}
