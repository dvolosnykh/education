#pragma once

#include "element.h"
#include "myPoint.h"
#include "oper.h"

#define M_PI	3.14159265358979323846


struct ellipse_t
{
	double a;
	double b;
	CmyPoint c;
};


// CmyPicture

class CmyPicture : public CStatic
{
	DECLARE_DYNAMIC( CmyPicture )

public:
	CRect m_DPrect;
	house_t current;

public:
	CmyPicture();
	virtual ~CmyPicture();

	house_t Convert( oper_t & oper );

private:
	void Create();
	void Draw( CPaintDC * pDC );
	void Clear( CPaintDC * pDC );

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnPaint();
protected:
	virtual void PreSubclassWindow();
};


inline int round( double x )
{
	return ( int )( x + 0.5 );
}
