#pragma once

#include "myPoint.h"

#define NARC	50


struct house_t
{
	CmyPoint wall[ 4 ];
	CmyPoint ceiling[ 3 ];
	CmyPoint frame[ 4 ];
	CmyPoint line1[ 2 ];
	CmyPoint arc1[ NARC + 1 ];
	CmyPoint line21[ 2 ];
	CmyPoint line22[ 2 ];
	CmyPoint ell2[ 2 * NARC + 1];
	CmyPoint ell3[ 2 * NARC + 1];
};

class CElement
{
public:
	house_t house;
	CElement * prev;

public:
	house_t & gethouse();
	CElement * & getprev();
};
