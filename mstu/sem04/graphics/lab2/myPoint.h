#pragma once


// CmyPoint

class CmyPoint
{
public:
	double x;
	double y;

public:
	CmyPoint();
	CmyPoint( double newx, double newy );
	~CmyPoint();

	void SetPoint( double newx, double newy );
};