//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by lab2.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MAINWND                     102
#define IDR_MAINFRAME                   128
#define IDC_OPS                         1002
#define IDC_EDIT_DX                     1003
#define IDC_EDIT_DY                     1004
#define IDC_EDIT_CX                     1005
#define IDC_EDIT_CY                     1006
#define IDC_EDIT_PHI                    1007
#define IDC_DO                          1008
#define IDC_ST_DX                       1009
#define IDC_ST_DY                       1010
#define IDC_ST_CX                       1011
#define IDC_ST_CY                       1012
#define IDC_PICTURE                     1013
#define IDC_RESET                       1015
#define IDC_ST_MX                       1016
#define IDC_ST_PHI                      1017
#define IDC_EDIT_MX                     1018
#define IDC_ST_MY                       1019
#define IDC_EDIT_MY                     1020
#define IDC_ST_KX                       1021
#define IDC_EDIT_KX                     1022
#define IDC_ST_KY                       1023
#define IDC_EDIT_KY                     1024
#define IDC_NORMAL                      1025
#define IDC_BIG                         1026
#define IDC_CHANGE                      1027
#define IDC_UNDO                        1029
#define IDC_REDO                        1030

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
