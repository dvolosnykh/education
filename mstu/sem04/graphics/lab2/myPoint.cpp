#include "stdafx.h"

#include "myPoint.h"

// CmyPoint
CmyPoint::CmyPoint()
{
}

CmyPoint::CmyPoint( double newx, double newy )
{
	SetPoint( newx, newy );
}

CmyPoint::~CmyPoint()
{
}

void CmyPoint::SetPoint( double newx, double newy )
{
	x = newx;
	y = newy;
}
