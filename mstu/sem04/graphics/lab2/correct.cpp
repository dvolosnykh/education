#include "stdafx.h"
#include "lab2.h"
#include "lab2Dlg.h"
#include ".\lab2dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


bool correctf( CEdit & edit )
{
	const unsigned length = edit.GetWindowTextLength() + 1;
	char * temp = new char [ length ];
	edit.GetWindowText( temp, length );

	char * pError;
	strtod( temp, &pError );

	bool correct = ( pError != temp && *pError == '\0' );

	delete temp;

	return ( correct );
}


bool Clab2Dlg::CorrectTabOffset( bool notify /*= false*/ )
{
	bool dx_isCorrect = correctf( m_dx );
	bool dy_isCorrect = correctf( m_dy );

	bool correct = dx_isCorrect && dy_isCorrect;

	if ( notify && !correct )
	{
		CString msg( "" );

		bool dx_isEmpty = m_dx.GetWindowTextLength() == 0;
		bool dy_isEmpty = m_dy.GetWindowTextLength() == 0;
    
		if ( !dx_isCorrect )
			msg += CString( "�������� �� X " ) + 
				CString( dx_isEmpty ? "�� �������." : "������� �� ���������." );
		
		if ( !dy_isCorrect )
		{
			if ( !dx_isCorrect )
				msg += CString( "\n" );

			msg += CString( "�������� �� Y " ) + 
				CString( dy_isEmpty ? "�� �������." : "������� �� ���������." );
		}

		MessageBox( msg, "������" );

		Retype( !dx_isCorrect ? m_dx : m_dy );
	}

	return ( correct );
}

bool Clab2Dlg::CorrectTabRotate( bool notify /*= false*/ )
{
	bool cx_isCorrect = correctf( m_cx );
	bool cy_isCorrect = correctf( m_cy );
	bool phi_isCorrect = correctf( m_phi );

	bool correct = cx_isCorrect && cy_isCorrect && phi_isCorrect;

	if ( notify && !correct )
	{
		CString msg( "" );

		bool cx_isEmpty = m_cx.GetWindowTextLength() == 0;
		bool cy_isEmpty = m_cy.GetWindowTextLength() == 0;
		bool phi_isEmpty = m_phi.GetWindowTextLength() == 0;

		if ( !cx_isCorrect )
			msg += CString( "���������� X ������ " ) +
				CString( cx_isEmpty ? "�� �������." : "������� �� ���������." );
		
		if ( !cy_isCorrect )
		{
			if ( !cx_isCorrect )
				msg += CString( "\n" );

			msg += CString( "���������� Y ������ " ) +
				CString( cy_isEmpty ? "�� �������." : "������� �� ���������." );
		}

		if ( !phi_isCorrect )
		{
			if ( !cx_isCorrect || !cy_isCorrect )
				msg += CString( "\n" );

			msg += CString( "���� �������� " ) + 
				CString( phi_isEmpty ? "�� �����." : "����� �� ���������." );
		}

		MessageBox( msg, "������" );

		Retype( !cx_isCorrect ? m_cx : !cy_isCorrect ? m_cy : m_phi );
	}

	return ( correct );
}

bool Clab2Dlg::CorrectTabScale( bool notify /*= false*/ )
{
	bool mx_isCorrect = correctf( m_mx );
	bool my_isCorrect = correctf( m_my );
	bool kx_isCorrect = correctf( m_kx );
	bool ky_isCorrect = correctf( m_ky );
	
	bool correct = mx_isCorrect && my_isCorrect && kx_isCorrect && ky_isCorrect;

	if ( notify && !correct )
	{
		CString msg( "" );

		bool mx_isEmpty = m_mx.GetWindowTextLength() == 0;
		bool my_isEmpty = m_my.GetWindowTextLength() == 0;
		bool kx_isEmpty = m_kx.GetWindowTextLength() == 0;
		bool ky_isEmpty = m_ky.GetWindowTextLength() == 0;

		if ( !mx_isCorrect )
			msg += CString( "���������� X ������ " ) +
				CString( mx_isEmpty ? "�� �������." : "������� �� ���������." );
		
		if ( !my_isCorrect )
		{
			if ( !mx_isCorrect )
				msg += CString( "\n" );

			msg += CString( "���������� Y ������ " ) +
				CString( my_isEmpty ? "�� �������." : "������� �� ���������." );
		}

		if ( !kx_isCorrect )
		{
			if ( !mx_isCorrect || !my_isCorrect )
				msg += CString( "\n" );

			msg += CString( "����������� ��������������� �� X " ) + 
				CString( kx_isEmpty ? "�� �����." : "����� �� ���������." );
		}

		if ( !ky_isCorrect )
		{
			if ( !mx_isCorrect || !my_isCorrect || !kx_isCorrect )
				msg += CString( "\n" );

			msg += CString( "����������� ��������������� �� Y " ) + 
				CString( ky_isEmpty ? "�� �����." : "����� �� ���������." );
		}

		MessageBox( msg, "������" );

		Retype( !mx_isCorrect ? m_mx : !my_isCorrect ? m_my : 
			!kx_isCorrect ? m_kx : m_ky );
	}

	return ( correct );
}

bool Clab2Dlg::CorrectTab( UINT nTab, bool notify /*= false*/ )
{
	bool correct;

    switch ( nTab )
	{
	case TAB_OFFSET:
		correct = CorrectTabOffset( notify );
		break;
	case TAB_ROTATE:
		correct = CorrectTabRotate( notify );
		break;
	case TAB_SCALE:
		correct = CorrectTabScale( notify );
		break;
	}

	return ( correct );
}
