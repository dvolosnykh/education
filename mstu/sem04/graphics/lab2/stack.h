#pragma once

#include "element.h"


class CStack
{
public:
	CElement * top;

public:
	CStack();
	~CStack();

	bool IsEmpty();

	void Push( house_t & house );
	house_t Pop();
	house_t SaveFirstClear();
	void Clear();
};

house_t & housecpy( house_t & dest, house_t & src );