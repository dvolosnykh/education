#include "stdafx.h"
#include "lab2.h"
#include "lab2Dlg.h"
#include ".\lab2dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

void Clab2Dlg::ResizeTab( int cx, int cy )
{
	m_ops.SetWindowPos( NULL, 0, 0, cx, cy, SWP_NOZORDER | SWP_NOMOVE );
}

void Clab2Dlg::MoveBtnDo( int x, int y )
{
	m_do.SetWindowPos( NULL, x, y, 0, 0, SWP_NOZORDER | SWP_NOSIZE );
}


void Clab2Dlg::SwitchTabOffset()
{
	// Resizing Tab
	CRect rectTab;
	m_ops.GetWindowRect( rectTab );
	rectTab.bottom = rectTab.top + 136;
	ResizeTab( rectTab.Width(), rectTab.Height() );

	// Moving button DO
	CRect rectBtn;
	m_do.GetWindowRect( rectBtn );
	rectBtn.MoveToXY( 570, 116 );
	MoveBtnDo( rectBtn.left, rectBtn.top );	

	// Turn on/off tabs
	VisibleTab( TAB_OFFSET );
}

void Clab2Dlg::SwitchTabRotate()
{
	// Resizing Tab
	CRect rectTab;
	m_ops.GetWindowRect( rectTab );
	rectTab.bottom = rectTab.top + 165;
	ResizeTab( rectTab.Width(), rectTab.Height() );

	// Moving button DO
	CRect rectBtn;
	m_do.GetWindowRect( rectBtn );
	rectBtn.MoveToXY( 570, 145 );
	MoveBtnDo( rectBtn.left, rectBtn.top );	

	// Turn on/off tabs
	VisibleTab( TAB_ROTATE );
}

void Clab2Dlg::SwitchTabScale()
{
	// Resizing Tab
	CRect rectTab;
	m_ops.GetWindowRect( rectTab );
	rectTab.bottom = rectTab.top + 195;
	ResizeTab( rectTab.Width(), rectTab.Height() );

	// Moving button DO
	CRect rectBtn;
	m_do.GetWindowRect( rectBtn );
	rectBtn.MoveToXY( 570, 175 );
	MoveBtnDo( rectBtn.left, rectBtn.top );	

	// Turn on/off tabs
	VisibleTab( TAB_SCALE );
}

void Clab2Dlg::SwitchTab( UINT nTab )
{
	switch ( nTab )
	{
	case TAB_OFFSET:
		SwitchTabOffset();
		break;
	case TAB_ROTATE:
		SwitchTabRotate();
		break;
	case TAB_SCALE:
		SwitchTabScale();
		break;
	}
}
