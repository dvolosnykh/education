#include "stdafx.h"

#include "stack.h"
#include "element.h"
#include "myPoint.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CStack::CStack()
{
	top = NULL;
}

CStack::~CStack()
{
	Clear();
}


CElement * new_element( )
{
	return ( new CElement );
}

void delete_element( CElement * & e )
{
	delete ( e );
}

void arraycpy( CmyPoint * dest, CmyPoint * src, int n )
{
	for ( int i = 0; i < n; i++ )
		dest[ i ] = src[ i ];
}

house_t & housecpy( house_t & dest, house_t & src )
{
	arraycpy( dest.wall, src.wall, 4 );
	arraycpy( dest.ceiling, src.ceiling, 3 );
	arraycpy( dest.frame, src.frame, 4 );
	arraycpy( dest.line1, src.line1, 2 );
	arraycpy( dest.arc1, src.arc1, NARC + 1 );
	arraycpy( dest.line21, src.line21, 2 );
	arraycpy( dest.line22, src.line22, 2 );
	arraycpy( dest.ell2, src.ell2, 2 * NARC + 1 );
	arraycpy( dest.ell3, src.ell3, 2 * NARC + 1 );

	return ( dest );
}

bool CStack::IsEmpty()
{
	return ( top == NULL );
}

void CStack::Push( house_t & house )
{
	CElement * inserted = new_element();
	housecpy( inserted->house, house );

	inserted->prev = top;
	top = inserted;
}

house_t CStack::Pop()
{
	house_t house;
	housecpy( house, top->house );

	CElement * temp = top;
	top = top->prev;
	delete_element( temp );

	return ( house );
}

house_t CStack::SaveFirstClear()
{
	for ( ; top->prev; Pop() );
	house_t house = Pop();

	return ( house );
}

void CStack::Clear()
{
	for ( ; top; Pop() );
}