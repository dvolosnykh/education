// lab2Dlg.h : header file
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"

#include "myPicture.h"
#include "stack.h"
#include "oper.h"

enum
{
	TAB_OFFSET,
	TAB_ROTATE,
	TAB_SCALE
};


// Clab2Dlg dialog
class Clab2Dlg : public CDialog
{
private:
	struct offset_data_t
	{
		CString dx;
		CString dy;
	};

	struct rotate_data_t
	{
		CString cx;
		CString cy;
		CString phi;
	};

	struct scale_data_t
	{
		CString mx;
		CString my;
		CString kx;
		CString ky;
	};

private:
	CStack Undo;
	CStack Redo;

	int m_resolution;

	CmyPicture m_picture;

	CTabCtrl m_ops;

	CEdit m_dx;	
	CEdit m_dy;
	CStatic m_st_dx;
	CStatic m_st_dy;

	CEdit m_cx;
	CEdit m_cy;
	CEdit m_phi;
	CStatic m_st_cx;
	CStatic m_st_cy;
	CStatic m_st_phi;

	CEdit m_mx;
	CEdit m_my;
	CEdit m_kx;
	CEdit m_ky;
	CStatic m_st_mx;
	CStatic m_st_my;
	CStatic m_st_kx;
	CStatic m_st_ky;

	CButton m_do;
	CButton m_redo;
	CButton m_undo;

	offset_data_t offset_data;
	rotate_data_t rotate_data;
	scale_data_t scale_data;

// Construction
public:
	Clab2Dlg(CWnd* pParent = NULL);	// standard constructor
	~Clab2Dlg();

// Dialog Data
	enum { IDD = IDD_MAINWND };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation
private:
	void DefaultData();

	// switch.cpp
	void ResizeTab( int cx, int cy );
	void MoveBtnDo( int x, int y );
	void SwitchTabOffset();
	void SwitchTabRotate();
	void SwitchTabScale();
	void SwitchTab( UINT nTab );

	// visible.cpp
	void VisibleTabOffset( bool visible = true );
	void VisibleTabRotate( bool visible = true );
	void VisibleTabScale( bool visible = true );
	void VisibleTab( UINT nTab );
		
	// correct.cpp
	bool CorrectTabOffset( bool notify = false );
	bool CorrectTabRotate( bool notify = false );
	bool CorrectTabScale( bool notify = false );
	bool CorrectTab( UINT nTab, bool notify = false );

	// oper.cpp
	house_t OperOffset();
	house_t OperRotate();
	house_t OperScale();
	house_t Oper( UINT nTab );

protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnTcnSelchangeOps(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedDo();
	afx_msg void OnBnClickedReset();
	afx_msg void OnBnClickedUndo();
	afx_msg void OnBnClickedRedo();
	afx_msg void OnBnClickedChange();
};

inline void Retype( CEdit & edit )
{
	edit.SetFocus();
	edit.SetSel( 0, -1, true );
}
