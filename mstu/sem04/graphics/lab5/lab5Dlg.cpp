// lab5Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab5.h"
#include "lab5Dlg.h"
#include ".\lab5dlg.h"
#include "myPicture.h"
#include <math.h>
#include "ResearchDlg.h"
#include ".\researchdlg.h"
#include "algorithm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static bool firstdot = true;
static CMetaFileDC * pmfsave;


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// Clab5Dlg dialog

Clab5Dlg::Clab5Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(Clab5Dlg::IDD, pParent),
	m_algorithm( ALGO_BREZENHEM ),
	m_figure( FIG_CIRCLE ),
	m_color( RGB( 0, 0, 0 ) )
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

Clab5Dlg::~Clab5Dlg()
{
}

void Clab5Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PICTURE, m_picture);
	DDX_Control(pDX, IDC_MOUSEPOS, m_mousepos);
	DDX_Control(pDX, IDC_COLOR, m_drawcolor);	
	DDX_Radio(pDX, IDC_BREZENHEM, m_algorithm);
	DDX_Control(pDX, IDC_DRAWSTYLE, m_drawstyle);
	DDX_Radio(pDX, IDC_CIRCLE, m_figure);

	DDX_Control(pDX, IDC_ST_CENTER, m_st_center);
	DDX_Control(pDX, IDC_EDIT_CX, m_cx);
	DDX_Control(pDX, IDC_EDIT_CY, m_cy);
	DDX_Control(pDX, IDC_ST_HALFAXISES, m_st_halfaxises);
	DDX_Control(pDX, IDC_EDIT_A, m_a);
	DDX_Control(pDX, IDC_EDIT_B, m_b);

	DDX_Control(pDX, IDC_ST_STEP, m_st_step);
	DDX_Control(pDX, IDC_EDIT_STEP, m_step);
}

BEGIN_MESSAGE_MAP(Clab5Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_DRAWSTYLE, OnTcnSelchangeDrawstyle)
	ON_BN_CLICKED(IDC_DRAW, OnBnClickedDraw)
	ON_BN_CLICKED(IDC_CLEAR, OnBnClickedClear)
	ON_BN_CLICKED(IDC_CHOOSE, OnBnClickedChoose)
	ON_BN_CLICKED(IDC_RESEARCH, OnBnClickedResearch)
	ON_BN_CLICKED(IDC_CIRCLE, OnBnClickedCircle)
	ON_BN_CLICKED(IDC_ELLIPSE, OnBnClickedEllipse)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()



// Clab5Dlg message handlers

BOOL Clab5Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// Tab control
	m_drawstyle.InsertItem( TAB_SINGLE, "������" );
	m_drawstyle.InsertItem( TAB_TARGET, "������" );
	m_drawstyle.SetCurSel( TAB_SINGLE );
	SwitchTab( TAB_SINGLE );

	// Reseting program to initial state
	DefaultData();
	OnBnClickedClear();
	OnBnClickedCircle();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Clab5Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Clab5Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Clab5Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void Clab5Dlg::OnTcnSelchangeDrawstyle(NMHDR *pNMHDR, LRESULT *pResult)
{
	SwitchTab( m_drawstyle.GetCurSel() );

	*pResult = 0;
}

static int edittoi( CEdit & edit )
{
	const unsigned length = edit.GetWindowTextLength() + 1;
	char * temp = new char [ length ];
	edit.GetWindowText( temp, length );

	int value = atoi( temp );

	delete temp;

	return ( value );
}

void Clab5Dlg::OnBnClickedDraw()
{
	UpdateData( true );
	pAlgo Algo = ChooseAlgo( m_algorithm );

	ellipse_t ellipse;
	ellipse.c.SetPoint( edittoi( m_cx ), edittoi( m_cy ) );
	ellipse.a = edittoi( m_a );
	ellipse.b = m_figure == FIG_ELLIPSE ? edittoi( m_b ) : ellipse.a;

	switch( m_drawstyle.GetCurSel() )
	{
	case TAB_SINGLE:
		Algo( m_picture.pmf, ellipse, m_color );
		break;
	case TAB_TARGET:
		const unsigned step = edittoi( m_step );
		for ( ; ellipse.a >= 0 && ellipse.b >= 0;
				ellipse.a -= step, ellipse.b -= step )
			Algo( m_picture.pmf, ellipse, m_color );
		break;
	}

	m_picture.Invalidate( false );
}

void Clab5Dlg::OnBnClickedClear()
{
	if ( !firstdot )
	{
		OnLButtonDown( 0, CPoint( 12, 12 ) );
		firstdot = true;
	}

	m_picture.Clear();
	CRect area( m_picture.m_DPrect );
	area.DeflateRect( 1, 1, 1, 1 );
	m_picture.pmf->IntersectClipRect( area );

	m_picture.Invalidate( false );

	RepresentColor( m_color );
}

void Clab5Dlg::OnBnClickedChoose()
{
	COLORREF defcolors[ 16 ];
	for ( int i = 0; i < 16; i++ )
		defcolors[ i ] = RGB( 255, 255, 255 );

	CHOOSECOLOR colors;
	colors.lStructSize = sizeof( CHOOSECOLOR );
	colors.hwndOwner = NULL;
	colors.Flags = CC_RGBINIT;
	colors.rgbResult = RGB( 0, 0, 0 );
	colors.lpCustColors = defcolors;

	ChooseColor( &colors );

	RepresentColor( m_color = colors.rgbResult );
}

void Clab5Dlg::DefaultData()
{
	m_cx.SetWindowText( "250" );
	m_cy.SetWindowText( "250" );
	m_a.SetWindowText( "87" );
	m_b.SetWindowText( "150" );
	m_step.SetWindowText( "5" );
}

void Clab5Dlg::RepresentColor( COLORREF color )
{
	ResetMF( m_drawcolor.pmf );

	CBrush newColor( color );
	CGdiObject * pOldPen = m_drawcolor.pmf->SelectStockObject( BLACK_PEN );
	CGdiObject * pOldBrush = m_drawcolor.pmf->SelectObject( &newColor );

	m_drawcolor.pmf->Rectangle( m_drawcolor.m_DPrect );

	m_drawcolor.pmf->SelectObject( pOldPen );
	m_drawcolor.pmf->SelectObject( pOldBrush );

	m_drawcolor.Invalidate( false );
}

void Clab5Dlg::OnBnClickedResearch()
{
	CResearchDlg dlg( this );
	dlg.DoModal();
}

void Clab5Dlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) )
	{
		if ( firstdot )
		{
			pmfsave = NewMF( m_picture.pmf );

			CString coord( "" );
			coord.Format( "%li", point.x );
			m_cx.SetWindowText( coord );
			coord.Format( "%li", point.y );
			m_cy.SetWindowText( coord );

			m_picture.Invalidate( false );
		}
		else
		{
			HMETAFILE hmfsave = pmfsave->Close();
			DeleteMF( pmfsave, hmfsave );
		}

		firstdot = !firstdot;
	}

	CDialog::OnLButtonDown(nFlags, point);
}

void Clab5Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) )
	{
		CString coord( "" );
		coord.Format( "%li : %li", point.x, point.y );
		m_mousepos.SetWindowText( coord );

		if ( !firstdot )
		{
			ResetMF( m_picture.pmf );
			PlayMF( m_picture.pmf, pmfsave );

			CPoint center( edittoi( m_cx ), edittoi( m_cy ) );
			switch ( m_figure )
			{
			case FIG_CIRCLE:
				{
				long r = round( hypot( point.x - center.x, point.y - center.y ) );
				CString radius( "" );
				radius.Format( "%li", r );
				m_a.SetWindowText( radius );
				}
				break;
			case FIG_ELLIPSE:
				{
				long a = abs( point.x - center.x );
				long b = abs( point.y - center.y );
				CString axis( "" );
				axis.Format( "%li", a );
				m_a.SetWindowText( axis );
				axis.Format( "%li", b );
				m_b.SetWindowText( axis );
				}
				break;
			}

			OnBnClickedDraw();
		}
	}

	CDialog::OnMouseMove(nFlags, point);
}
