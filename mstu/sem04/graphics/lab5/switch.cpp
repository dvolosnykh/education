#include "stdafx.h"
#include "lab5.h"
#include "lab5Dlg.h"
#include ".\lab5dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


void Clab5Dlg::SwitchTabSingle()
{
	// Turn on/off tabs
	VisibleTab( TAB_SINGLE );
}

void Clab5Dlg::SwitchTabTarget()
{
	// Turn on/off tabs
	VisibleTab( TAB_TARGET );
}

void Clab5Dlg::SwitchTab( UINT nTab )
{
	switch ( nTab )
	{
	case TAB_SINGLE:
		SwitchTabSingle();
		break;
	case TAB_TARGET:
		SwitchTabTarget();
		break;
	}
}
