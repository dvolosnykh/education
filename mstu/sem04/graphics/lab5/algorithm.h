#pragma once

enum
{
	ALGO_BREZENHEM,
	ALGO_MIDPOINT,
	ALGO_EQUATION,
	ALGO_STANDARD
};


struct ellipse_t
{
	CPoint c;
	int a;
	int b;
};


typedef void ( *pAlgo )( CDC * pDC, const ellipse_t & ellipse, COLORREF color );

long round( double x );

void AlgoBrezenhem( CDC * pDC, const ellipse_t & ellipse, COLORREF color );
void AlgoMidpoint( CDC * pDC, const ellipse_t & ellipse, COLORREF color );
void AlgoEquation( CDC * pDC, const ellipse_t & ellipse, COLORREF color );
void AlgoStandard( CDC * pDC, const ellipse_t & ellipse, COLORREF color );

pAlgo ChooseAlgo( const int nAlgorithm );
