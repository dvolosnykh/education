#include "stdafx.h"
#include <math.h>
#include "algorithm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


enum dir_t
{
	HORIZONTAL,
	VERTICAL,
	DIAGONAL
};


long sqr( long x )
{
	return ( x * x );
}

double fsqr( double x )
{
	return ( x * x );
}

long round( double x )
{
	return ( long )( x + 0.5 );
}

void put4dots( CDC * pDC, CPoint dot, const CPoint & center, const COLORREF & color )
{
	for ( unsigned i = 0; i < 2; i++ )
	{
		dot.y = - dot.y;
		pDC->SetPixel( dot + center, color );
		dot.x = - dot.x;
		pDC->SetPixel( dot + center, color );
	}
}

void AlgoBrezenhem( CDC * pDC, const ellipse_t & ellipse, COLORREF color )
{
	const CPoint & center( ellipse.c );
	const int & r = ellipse.a;

	int d = 2 * ( 1 - r );

	for ( CPoint dot( 0, r ); ; )
	{
		put4dots( pDC, dot, center, color );

	if ( dot.y == 0 ) break;

		// defining direction where to make a step
		dir_t direction;
		if ( d < 0 )  // diagonal pixel is inside circle
		{
			int h = sqr( dot.x + 1 ) + sqr( dot.y ) - sqr( r );
			direction = abs( h ) < abs( d ) ? HORIZONTAL : DIAGONAL;
		}
		else if ( d > 0 )  // diagonal pixel is outside circle
		{
			int v = sqr( dot.x ) + sqr( dot.y - 1 ) - sqr( r );
			direction = abs( v ) < abs( d ) ? VERTICAL : DIAGONAL;
		}
		else  // exactly diagonal
			direction = DIAGONAL;

		// making step in just defined direction
		switch ( direction )
		{
		case HORIZONTAL:
			dot.Offset( 1, 0 );
			d += 2 * dot.x + 1;
			break;
		case VERTICAL:
			dot.Offset( 0, -1 );
			d += -2 * dot.y + 1;
			break;
		case DIAGONAL:
			dot.Offset( 1, -1 );
			d += 2 * ( dot.x - dot.y + 1 );
			break;
		}
	}
}

void AlgoMidpoint( CDC * pDC, const ellipse_t & ellipse, COLORREF color )
{
	const CPoint & center( ellipse.c );
	const int & a = ellipse.a;
	const int & b = ellipse.b;
	const int a2 = sqr( a );
	const int b2 = sqr( b );
	const int da2 = 2 * a2;
	const int db2 = 2 * b2;

	// 1st part
	CPoint dot( 0, b );
	double f = b2 + a2 * ( 0.25 - b );

	pDC->SetPixel( CPoint( 0,  b ) + center, color );
	pDC->SetPixel( CPoint( 0, -b ) + center, color );

	for ( ; b2 * ( dot.x + 1 ) < a2 * dot.y && dot.x != a;  )
	{
		// making step
		if ( f > 0 )
			f += -da2 * --dot.y;
		f += db2 * ++dot.x + b2;

		put4dots( pDC, dot, center, color );
	}

	// changing value because of change in algorithm
	f += 0.75 * ( a2 - b2 ) - b2 * dot.x - a2 * dot.y;

	// 2nd part
	for ( ; dot.y > 0; )
	{
		// making step
		if ( f < 0 )
			f += db2 * ++dot.x;
		f += -da2 * --dot.y + a2;

		put4dots( pDC, dot, center, color );
	}

	// if left part which looks like a simple line
	for ( ; dot.x++ < a; )
		put4dots( pDC, dot, center, color );
}

void AlgoEquation( CDC * pDC, const ellipse_t & ellipse, COLORREF color )
{
	const CPoint & center( ellipse.c );
	const int & a = ellipse.a;
	const int & b = ellipse.b;
	const int a2 = sqr( a );
	const int b2 = sqr( b );

	// 1st part
	CPoint dot( 0, b );

	for ( ; b2 * dot.x < a2 * dot.y && dot.x <= a; dot.x++ )
	{
		// finding y-coordinate
		dot.y = round( b * sqrt( 1 - sqr( dot.x ) / ( double )a2 ) );

		put4dots( pDC, dot, center, color );
	}

	// 2nd part
	for ( ; dot.y >= 0; dot.y-- )
	{
		// finding x-coordinate
		dot.x = round( a * sqrt( 1 - sqr( dot.y ) / ( double )b2 ) );

		put4dots( pDC, dot, center, color );
	}
}

void AlgoStandard( CDC * pDC, const ellipse_t & ellipse, COLORREF color )
{
	CPen myPen( PS_SOLID, 1, color );
	CGdiObject * pOldPen = pDC->SelectObject( &myPen );
	CGdiObject * pOldBrush = pDC->SelectStockObject( NULL_BRUSH );

	CPoint topLeft( ellipse.c.x - ellipse.a,
		ellipse.c.y - ellipse.b );
	CPoint bottomRight( ellipse.c.x + ellipse.a + 1,
		ellipse.c.y + ellipse.b + 1 );
	CRect rect( topLeft, bottomRight );

	pDC->Ellipse( rect );

	pDC->SelectObject( pOldPen );
	pDC->SelectObject( pOldBrush );
}

pAlgo ChooseAlgo( const int nAlgorithm )
{
	pAlgo choosed;
	
	switch ( nAlgorithm )
	{
	case ALGO_BREZENHEM:
		choosed = AlgoBrezenhem;
		break;
	case ALGO_MIDPOINT:
		choosed = AlgoMidpoint;
		break;
	case ALGO_EQUATION:
		choosed = AlgoEquation;
		break;
	case ALGO_STANDARD:
		choosed = AlgoStandard;
		break;
	}

	return ( choosed );
}
