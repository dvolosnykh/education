#include "stdafx.h"
#include "lab5.h"
#include "lab5Dlg.h"
#include ".\lab5dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


void Clab5Dlg::OnBnClickedCircle()
{
	m_st_halfaxises.SetWindowText( "������:" );
	m_b.ShowWindow( false );

	CButton * pBrezenhem = new CButton;
	pBrezenhem = ( CButton * )GetDlgItem( IDC_BREZENHEM );
	pBrezenhem->EnableWindow( true );
}

void Clab5Dlg::OnBnClickedEllipse()
{
	m_figure = FIG_ELLIPSE;

	m_st_halfaxises.SetWindowText( "�������:" );
	m_b.ShowWindow( true );

	CButton * pBrezenhem = new CButton;
	pBrezenhem = ( CButton * )GetDlgItem( IDC_BREZENHEM );
	pBrezenhem->EnableWindow( false );
	if ( m_algorithm == ALGO_BREZENHEM )
	{
		m_algorithm++;
		UpdateData( false );
	}
}

void Clab5Dlg::VisibleTabSingle()
{
	m_st_step.ShowWindow( false );
	m_step.ShowWindow( false );
}

void Clab5Dlg::VisibleTabTarget()
{
	m_st_step.ShowWindow( true );
	m_step.ShowWindow( true );
}

void Clab5Dlg::VisibleTab( UINT nTab )
{
	switch ( nTab )
	{
	case TAB_SINGLE:
		VisibleTabSingle();
		break;
	case TAB_TARGET:
		VisibleTabTarget();
	}
}