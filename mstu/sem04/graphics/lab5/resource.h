//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by lab5.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MAINWND                     102
#define IDR_MAINFRAME                   128
#define IDD_RESEARCH                    129
#define IDC_PICTURE                     1001
#define IDC_BREZENHEM                   1003
#define IDC_MIDPOINT                    1004
#define IDC_EQUATION                    1005
#define IDC_STANDARD                    1007
#define IDC_EDIT_BEAMS                  1010
#define IDC_EDIT_X1                     1011
#define IDC_EDIT_Y1                     1012
#define IDC_EDIT_A                      1013
#define IDC_EDIT_B                      1014
#define IDC_SPIN                        1015
#define IDC_ST_BEAMS                    1016
#define IDC_ST_DOT1                     1017
#define IDC_ST_HALFAXISES               1018
#define IDC_DRAW                        1019
#define IDC_EDIT_LEN                    1020
#define IDC_ST_LEN                      1021
#define IDC_DRAWSTYLE                   1022
#define IDC_CLEAR                       1023
#define IDC_CHOOSE                      1026
#define IDC_COLOR                       1027
#define IDC_EDIT_CX                     1029
#define IDC_EDIT_CY                     1030
#define IDC_ST_CENTER                   1031
#define IDC_RESEARCH                    1032
#define IDC_GRAPH                       1033
#define IDC_ALGO_COLOR                  1034
#define IDC_EDIT_STEP                   1036
#define IDC_ST_STEP                     1038
#define IDC_CIRCLE                      1039
#define IDC_ELLIPSE                     1040
#define IDC_MOUSEPOS                    1041

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1042
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
