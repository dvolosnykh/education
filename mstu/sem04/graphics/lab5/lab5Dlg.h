// lab5Dlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "lab5.h"

#include "myPicture.h"
#include "algorithm.h"


enum
{
	TAB_SINGLE,
	TAB_TARGET
};

enum
{
	FIG_CIRCLE,
	FIG_ELLIPSE
};


// Clab5Dlg dialog
class Clab5Dlg : public CDialog
{
private:
//public:
	CmyPicture m_picture;
	CStatic m_mousepos;
	CmyPicture m_drawcolor;
	int m_algorithm;
	int m_figure;
	CTabCtrl m_drawstyle;

	CStatic m_st_center;
	CEdit m_cx;
	CEdit m_cy;
	CStatic m_st_halfaxises;
	CEdit m_a;
	CEdit m_b;

	CStatic m_st_step;
	CEdit m_step;

	COLORREF m_color;

// Construction
public:
	Clab5Dlg(CWnd* pParent = NULL);	// standard constructor
	~Clab5Dlg();

	// switch.cpp
	void SwitchTabSingle();
	void SwitchTabTarget();
	void SwitchTab( UINT nTab );

	// visible.cpp
	afx_msg void OnBnClickedCircle();
	afx_msg void OnBnClickedEllipse();
	void VisibleTabSingle();
	void VisibleTabTarget();
	void VisibleTab( UINT nTab );

	void DefaultData();
	void RepresentColor( COLORREF color );

// Dialog Data
	enum { IDD = IDD_MAINWND };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTcnSelchangeDrawstyle(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedDraw();
	afx_msg void OnBnClickedClear();
	afx_msg void OnBnClickedChoose();
	afx_msg void OnBnClickedResearch();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
