// ResearchDlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab5.h"
#include "ResearchDlg.h"
#include ".\researchdlg.h"
#include "myPicture.h"
#include ".\mypicture.h"
#include "algorithm.h"


#define TIMES	20

static const COLORREF color[ ALGO_STANDARD - ALGO_BREZENHEM + 1 ] =
{
	RGB( 255,   0,   0 ),
	RGB(   0, 255,   0 ),
	RGB(   0,   0, 255 ),
	RGB( 255,   0, 255 )
};


// CResearchDlg dialog

IMPLEMENT_DYNAMIC(CResearchDlg, CDialog)
CResearchDlg::CResearchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CResearchDlg::IDD, pParent)
{
}

CResearchDlg::~CResearchDlg()
{
}

void CResearchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRAPH, m_graph);
	DDX_Control(pDX, IDC_ALGO_COLOR, m_algocolor);
}


BEGIN_MESSAGE_MAP(CResearchDlg, CDialog)
END_MESSAGE_MAP()


// CResearchDlg message handlers

BOOL CResearchDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ShowColor( m_algocolor.pmf );
	Draw( m_graph.pmf );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CResearchDlg::ShowColor( CDC * pDC )
{
	// preparing set of colors for each of algorithms
	COLORREF color[ ALGO_STANDARD - ALGO_BREZENHEM + 1 ] =
	{
		RGB( 255,  64,  64 ),
		RGB(  64, 255,  64 ),
		RGB(  64,  64, 255 ),
		RGB( 255,  64, 255 )
	};

	// drawing markers
	CGdiObject * pOldBrush = pDC->SelectStockObject( WHITE_BRUSH );

	for ( int i = ALGO_BREZENHEM, y = -5; i <= ALGO_STANDARD; i++ )
	{
		CPen pencil( PS_SOLID, 3, color[ i ] );
		pDC->SelectObject( &pencil );
		pDC->MoveTo( 10, y += 13 );
		pDC->LineTo( 90, y );
	}

	pDC->SelectObject( pOldBrush );
}

DWORD findmax( table_t x , const unsigned m, const unsigned n )
{
	DWORD maximum = **x;

	for ( unsigned i = 0; i < m; i++ )
		for ( unsigned j = 0; j < n; j++ )
			if ( x[ i ][ j ] > maximum )
				maximum = x[ i ][ j ];

	return ( maximum );
}

void CResearchDlg::CoordSystem( CDC * pDC )
{
	int width = m_graph.m_DPrect.Width();
	int height = m_graph.m_DPrect.Height();

	pDC->SetMapMode( MM_ISOTROPIC );
	pDC->SetViewportExt( width, - height );
	pDC->SetViewportOrg( 0, height );

	const int size = 1000;
	const int blank_l = 200;
	const int blank_b = 100;
	const int blank_r = 200;
	const int blank_t = 150;
	
	width = size * width / height;
	height = size;

	pDC->SetWindowExt( width, height );
	pDC->SetWindowOrg( - blank_l, - blank_b );

	const int max_x = width - blank_l - blank_r;
	const int max_y = height - blank_b - blank_t;

	// axises
	pDC->MoveTo( 0, max_y );
	pDC->LineTo( 0, 0 );	
	pDC->LineTo( max_x, 0 );
	pDC->TextOut( -100, max_y + 90, "t, ����" );
	pDC->TextOut( max_x + 20, 40, "R" );

	// filling arrows
	CBrush fill( RGB( 192, 192, 192 ) );
	CGdiObject * pOldBrush = pDC->SelectObject( &fill );

	// drawing arrows
	CPoint triangle[ 3 ];
	triangle[ 0 ].SetPoint( 0, max_y );
	triangle[ 1 ].SetPoint( -10, max_y - 50 );
	triangle[ 2 ].SetPoint( +20, max_y - 50 );
	pDC->Polygon( triangle, 3 );

	triangle[ 0 ].SetPoint( max_x, 0 );
	triangle[ 1 ].SetPoint( max_x - 50, -20 );
	triangle[ 2 ].SetPoint( max_x - 50, +20 );
	pDC->Polygon( triangle, 3 );

	// deselectin my GDI object
	pDC->SelectObject( pOldBrush );
}

void CResearchDlg::DrawTimes( CDC * pDC, table_t time )
{
	const DWORD maxtime = findmax( time, ALGO_STANDARD - ALGO_BREZENHEM + 1,
		MAXRADIUS - MINRADIUS + 1 );

	const unsigned highest = 600;
	const unsigned step = 1800 / ( MAXRADIUS - MINRADIUS );

	// printing someting like scales
	CString numstr( "0" );
	pDC->TextOut( -40, -5, numstr );
	numstr.Format( "%4u", maxtime );
	if ( maxtime > 0 )
		pDC->TextOut( -190, highest + 40, numstr );
	numstr.Format( "%4u", MAXRADIUS );
	pDC->TextOut( 1700, -5, numstr );
	
	// saving old GDI object
	CGdiObject * pOldPen = pDC->SelectStockObject( NULL_PEN );

	for ( unsigned i = ALGO_BREZENHEM, x = 0; i <= ALGO_STANDARD; i++ )
	{
		CPen pencil( PS_SOLID, 7, color[ i ] );
		pDC->SelectObject( &pencil );
		pDC->MoveTo( 0, maxtime > 0 ? highest * time[ i ][ 0 ] / maxtime : 0 );
		for ( unsigned j = 1, x = step; j <= MAXRADIUS - MINRADIUS;
				j++, x += step )
			pDC->LineTo( x, maxtime > 0 ? highest * time[ i ][ j ] / maxtime : 0 );
	}

	// deselecting my GDI object
	pDC->SelectObject( pOldPen );
}

void CResearchDlg::Draw( CMetaFileDC * & pDC )
{
	// === EVALUATE ===
	// evaluating time charachteristics
	ellipse_t ellipse;
	ellipse.c.SetPoint( 150, 150 );
	
	table_t time;

	for ( unsigned i = ALGO_BREZENHEM; i <= ALGO_STANDARD; i++ )
	{
		pAlgo Algo = ChooseAlgo( i );

		ellipse.a = ellipse.b = MINRADIUS;
		for ( unsigned j = 0; j <= MAXRADIUS - MINRADIUS; j++, ellipse.a = ellipse.b++ )
		{
			DWORD start = GetTickCount();

			for ( unsigned k = 0; k < TIMES; k++ )
				Algo( pDC, ellipse, RGB( 0, 0, 0 ) );

			time[ i ][ j ] = GetTickCount() - start;
		}
	}

	// === DRAW ===
	// erasing what just have been drawn (multiple segments) while testing
	m_graph.Clear();

	CoordSystem( pDC );
	DrawTimes( pDC, time );
}
