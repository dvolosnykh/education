#pragma once
#include "myPicture.h"
#include ".\mypicture.h"
#include "algorithm.h"


#define MINRADIUS	0
#define MAXRADIUS	100

typedef DWORD table_t [ ALGO_STANDARD - ALGO_BREZENHEM + 1 ]
		[ MAXRADIUS - MINRADIUS + 1 ];


// CResearchDlg dialog

class CResearchDlg : public CDialog
{
	DECLARE_DYNAMIC(CResearchDlg)

public:
	CResearchDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CResearchDlg();

	void ShowColor( CDC * pDC );
	void CoordSystem( CDC * pDC );
	void DrawTimes( CDC * pDC, table_t time );
	void Draw( CMetaFileDC * & pDC );

// Dialog Data
	enum { IDD = IDD_RESEARCH };

private:
	CmyPicture m_graph;
	CmyPicture m_algocolor;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
};
