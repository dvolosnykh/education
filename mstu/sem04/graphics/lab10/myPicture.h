#pragma once


// My functions for meta-files

CMetaFileDC * NewMF();
CMetaFileDC * NewMF( CMetaFileDC * & src );
void DeleteMF( CMetaFileDC * pmf, HMETAFILE hmf );
void ReloadMF( CMetaFileDC * & pmf, HMETAFILE hmf );
void PlayMF( CDC * pDC, CMetaFileDC * & pmf );
void ResetMF( CMetaFileDC * & pmf );


// CmyPicture

class CmyPicture : public CStatic
{
	DECLARE_DYNAMIC( CmyPicture )

public:
	CRect m_DPrect;
	CMetaFileDC * pmf;

public:
	CmyPicture();
	virtual ~CmyPicture();

	void Clear();
	void PrepareCoordSystem();

protected:
	DECLARE_MESSAGE_MAP()

protected:
	virtual void PreSubclassWindow();
public:
	afx_msg void OnPaint();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};
