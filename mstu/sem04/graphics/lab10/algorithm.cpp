#include "stdafx.h"
#include "algorithm.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


enum flag_t
{
	LOWER = -2,
	ONBOTTOM = -1,
	INVISIBLE = 0,
	ONTOP = 1,
	HIGHER = 2
};

const double eps = 0.0001;


static long * top;
static long * bottom;

#define K	0.95

static double scale;
static double Xo;
static double Yo;


double DegreeToRad( double degree )
{
	return ( degree * M_PI / 180 );
}

void mult( matrix_t & a, const matrix_t & b )
{
	matrix_t c;

	for ( register unsigned i = 0; i < 4; i++ )
		for ( register unsigned j = 0; j < 4; j++ )
		{
			c[ i ][ j ] = 0;
			for ( register unsigned k = 0; k < 4; k++ )
				c[ i ][ j ] += a[ i ][ k ] * b[ k ][ j ];
		}

	memmove( a, c, sizeof( matrix_t ) );
}


static long round( const double x )
{
	return ( long )( x + 0.5 );
}

static int sign( const double x )
{
	return ( x > 0 ? 1 : x < 0 ? -1 : 0 );
}


static void refresh( const CPoint dot )
{
	top[ dot.x ] = max( top[ dot.x ], dot.y );
	bottom[ dot.x ] = min( bottom[ dot.x ], dot.y );
}

static void horizont( CPoint dot1, CPoint dot2 )
{
	if ( dot1.x == dot2.x )
		refresh( dot2 );
	else
	{
		const int dx = sign( dot2.x - dot1.x );
		const double m = ( dot2.y - dot1.y ) / ( double )( dot2.x - dot1.x );

		CPoint dot;
		for ( dot.x = dot1.x; dot.x != dot2.x; dot.x += dx )
		{
			dot.y = round( m * ( dot.x - dot1.x ) + dot1.y );
			refresh( dot );
		}
	}
}

static void workedge( const CPoint & dot, CPoint & edgedot )
{
	if ( edgedot.x != -1 )
		horizont( edgedot, dot );

	edgedot = dot;
}

//========================================================

static CPoint intersection( const CPoint & dot1, const CPoint & dot2, long * limit )
{
	CPoint I;

	if ( dot1.x == dot2.x )
		I.y = limit[ I.x = dot2.x ];
	else
	{
		const int dx = sign( dot2.x - dot1.x );
		const double m = dx * ( dot2.y - dot1.y ) / ( double )( dot2.x - dot1.x );

		long xi = dot1.x;
		double yi = dot1.y;
		const int ysign = sign( yi - limit[ xi ] );

		for ( ; xi != dot2.x && sign( ( yi += m ) - limit[ xi += dx ] ) == ysign; );

		if ( fabs( yi - m - limit[ xi - dx ] ) <= fabs( yi - limit[ xi ] ) )
		{
			xi -= dx;
			yi -= m;
		}

		I.x = xi;
		I.y = round( yi );
	}

	return ( I );
}

static void LineFull( CDC * pDC, const CPoint & dot1, const CPoint & dot2,
					 const COLORREF & color )
{
	CPen pencil( PS_SOLID, 1, color );
	CGdiObject * pOldPen = pDC->SelectObject( &pencil );

	pDC->MoveTo( dot1 );
	pDC->LineTo( dot2 );
	pDC->SetPixel( dot2, color );

	pDC->SelectObject( pOldPen );
}

//========================================================

static void transform( vector_t & v, const matrix_t & m )
{
	vector_t temp = { 0, 0, 0, 0 };

	for ( register unsigned i = 0; i < 4; i++ )
		for ( register unsigned j = 0; j < 4; j++ )
			temp[ i ] += v[ j ] * m[ j ][ i ];

	memmove( v, temp, sizeof( vector_t ) );
}

static CPoint ScreenPoint( const double & x, const double & y, const double & z,
						  const matrix_t & matrix )
{
	vector_t point = { x, y, z, 1 };
	transform( point, matrix );

	CPoint dot;
	dot.x = round( point[ 0 ] * scale + Xo );
	dot.y = round( point[ 1 ] * scale + Yo);

	return ( dot );
}

static void init_array( long * a, const long n, const long value )
{
	for ( register long i = 0; i < n; i++ )
		a[ i ] = value;
}

static flag_t visibility( const CPoint & dot )
{
	return ( dot.y > top[ dot.x ] ? HIGHER :
		dot.y < bottom[ dot.x ] ? LOWER :
		INVISIBLE );
}

void AlgoFloatingHorizont( CDC * pDC, func_t f, const data_t & data )
{
	const double & xmin = data.limit.xmin;
	const double & xmax = data.limit.xmax;
	const double & zmin = data.limit.zmin;
	const double & zmax = data.limit.zmax;
	const steps_t & step = data.step;
	const long & height = data.screen.height;
	const long & width = data.screen.width;
	const matrix_t & matrix = data.matrix;

	//
	scale = K * width / hypot( xmax - xmin, zmax - zmin );
	Xo = ( width - ( xmax + xmin ) * scale ) / 2;
	Yo = ( double )height / 2;
	//

	top = new long [ width ];
	bottom = new long [ width ];

	CPoint left( -1, -1 );
	CPoint right( -1, -1 );
	CPoint prev;

	// ---------------------------------------------

	init_array( top, width, 0 );
	init_array( bottom, width, height );

	for ( double x, y, z = zmax; z >= zmin - eps; z -= step.z )
	{
		x = xmin;
		y = f( x, z );
		prev = ScreenPoint( x, y, z, matrix );
		flag_t prevflag = visibility( prev );

		workedge( prev, left );

		for ( ; ( x += step.x ) <= xmax + eps; )
		{
			y = f( x, z );
			CPoint cur = ScreenPoint( x, y, z, matrix );

			flag_t curflag = visibility( cur );

			if ( curflag != prevflag )
			{
				CPoint I;

				switch ( curflag )
				{
				case HIGHER:
					switch ( prevflag )
					{
					case INVISIBLE:  // INVISIBLE -> HIGHER
						I = intersection( prev, cur, top );
						LineFull( pDC, I, cur, top_color );
						horizont( I, cur );
						break;
					case LOWER:  // LOWER -> HIGHER
						I = intersection( prev, cur, bottom );
						LineFull( pDC, prev, I, bottom_color );
						horizont( prev, I );

						I = intersection( prev, cur, top );
						LineFull( pDC, I, cur, top_color );
						horizont( I, cur );
						break;
					}
					break;
				case INVISIBLE:
					switch ( prevflag )
					{
					case HIGHER:  // HIGHER -> INVISIBLE
						I = intersection( prev, cur, top );
						LineFull( pDC, prev, I, top_color );
						horizont( prev, I );
						break;
					case LOWER:  // LOWER -> INVISIBLE
						I = intersection( prev, cur, bottom );
						LineFull( pDC, prev, I, bottom_color );
						horizont( prev, I );
						break;
					}
					break;
				case LOWER:
					switch ( prevflag )
					{
					case HIGHER:  // HIGHER -> LOWER
						I = intersection( prev, cur, top );
						LineFull( pDC, prev, I, top_color );
						horizont( prev, I );

						I = intersection( prev, cur, bottom );
						LineFull( pDC, I, cur, bottom_color );
						horizont( I, cur );
						break;
					case INVISIBLE:  // INVISIBLE -> LOWER
						I = intersection( prev, cur, bottom );
						LineFull( pDC, I, cur, bottom_color );
						horizont( I, cur );
						break;
					}
					break;
				}
			}
			else switch ( curflag )
			{
			case LOWER:
				LineFull( pDC, prev, cur, bottom_color );
				horizont( prev, cur );
				break;
			case HIGHER:
				LineFull( pDC, prev, cur, top_color );
				horizont( prev, cur );
				break;
			}

			prevflag = curflag;
			prev = cur;
		}
        
		workedge( prev, right );
	}

	// ---------------------------------------------

	delete top;
	delete bottom;
}