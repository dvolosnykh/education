#pragma once

#define M_PI	3.14159265358979323846

const COLORREF bottom_color = RGB(   0,   0, 255 );
const COLORREF    top_color = RGB(   0,   0, 255 );  //RGB( 255,   0,   0 );

typedef double ( *func_t )( const double x, const double z );
typedef double vector_t[ 4 ];
typedef double matrix_t[ 4 ][ 4 ];

struct limits_t
{
	double xmin;
	double xmax;
	double zmin;
	double zmax;
};

struct steps_t
{
	double x;
	double z;
};

struct screen_t
{
	long width;
	long height;
};

struct data_t
{
	limits_t limit;
	steps_t step;
	screen_t screen;
	matrix_t matrix;
};


double DegreeToRad( double degree );
void mult( matrix_t & a, const matrix_t & b );

void AlgoFloatingHorizont( CDC * pDC, func_t f, const data_t & data );