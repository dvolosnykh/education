// lab10Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab10.h"
#include "lab10Dlg.h"
#include ".\lab10dlg.h"
#include "algorithm.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//==========================================

static const double XMIN = 0;
static const double XMAX = 2 * M_PI;
static const double XSTEP = 0.1;

static const double ZMIN = 0;
static const double ZMAX = 2 * M_PI;
static const double ZSTEP = 0.1;

static const double alpha = 10;
static const struct
{
	double x;
	double y;
	double z;
} c = 
{
	( XMIN + XMAX ) / 2,
	                  0,
	( ZMIN + ZMAX ) / 2,
};

static double sqr( const double x )
{
	return ( x * x );
}

double norm( const double x, const double z )
{
	const double a = sqr( x - M_PI ) + sqr( z - M_PI );
	return ( sin( x ) * cos( z ) / 5 - 3 / 2 * cos( 7 * a / 4 ) * exp( -a ) );

	//return ( exp( - ( sqr( x ) + sqr( z ) ) / 2 ) );
	//return cos( x - z ) * sin( x ) * cos( z );
}

//==========================================

#define OX_PLUS 	VK_END
#define OX_MINUS	VK_HOME
#define OY_PLUS 	VK_NEXT
#define OY_MINUS	VK_DELETE
#define OZ_PLUS		VK_INSERT
#define OZ_MINUS	VK_PRIOR


static data_t data;

//==========================================


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// Clab10Dlg dialog



Clab10Dlg::Clab10Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(Clab10Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Clab10Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control( pDX, IDC_PICTURE, m_picture );
}

BEGIN_MESSAGE_MAP(Clab10Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_KEYDOWN()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// Clab10Dlg message handlers

static void set( matrix_t & w, const double & a, const double & b, const double & c,
				const double & d, const double & e, const double & f, const double & g,
				const double & h, const double & i, const double & j, const double & k,
				const double & l, const double & m, const double & n, const double & o,
				const double & p )
{
	w[ 0 ][ 0 ] = a;	w[ 0 ][ 1 ] = b;	w[ 0 ][ 2 ] = c;	w[ 0 ][ 3 ] = d;
	w[ 1 ][ 0 ] = e;	w[ 1 ][ 1 ] = f;	w[ 1 ][ 2 ] = g;	w[ 1 ][ 3 ] = h;
	w[ 2 ][ 0 ] = i;	w[ 2 ][ 1 ] = j;	w[ 2 ][ 2 ] = k;	w[ 2 ][ 3 ] = l;
	w[ 3 ][ 0 ] = m;	w[ 3 ][ 1 ] = n;	w[ 3 ][ 2 ] = o;	w[ 3 ][ 3 ] = p;
}


BOOL Clab10Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	//---------------------------------------
	// data initialization
	data.limit.xmin = XMIN;
	data.limit.xmax = XMAX;
	data.step.x = XSTEP;

	data.limit.zmin = ZMIN;
	data.limit.zmax = ZMAX;
	data.step.z = ZSTEP;

	data.screen.width = m_picture.m_DPrect.Width();
	data.screen.height = m_picture.m_DPrect.Height();

	set( data.matrix, 1, 0, 0, 0,
		              0, 1, 0, 0,
		              0, 0, 1, 0,
		              0, 0, 0, 1 );

	Redraw();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Clab10Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Clab10Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Clab10Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void Clab10Dlg::Redraw()
{
	m_picture.Clear();
	AlgoFloatingHorizont( m_picture.pmf, norm, data );
	m_picture.Invalidate( false );
}

void Clab10Dlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if ( nChar == OX_PLUS || nChar == OX_MINUS
		|| nChar == OY_PLUS || nChar == OY_MINUS
		|| nChar == OZ_PLUS || nChar == OZ_MINUS )
	{
		matrix_t transform;

		set( transform,  1,    0,    0,    0,
		                 0,    1,    0,    0,
		                 0,    0,    1,    0,
		              -c.x, -c.y, -c.z,    1 );

		mult( data.matrix, transform );

		switch ( nChar )
		{
		case OX_PLUS:	case OX_MINUS:
			{
			const double phi = DegreeToRad( nChar == OX_PLUS ? alpha : -alpha );
			const double c = cos( phi );
			const double s = sin( phi );

			set( transform,  1,  0,  0,  0,
				             0,  c,  s,  0,
				             0, -s,  c,  0,
				             0,  0,  0,  1 );
			}
			break;
		case OY_PLUS:	case OY_MINUS:
			{
			const double phi = DegreeToRad( nChar == OY_PLUS ? alpha : -alpha );
			const double c = cos( phi );
			const double s = sin( phi );

			set( transform,  c,  0, -s,  0,
				             0,  1,  0,  0,
				             s,  0,  c,  0,
				             0,  0,  0,  1 );
			}
			break;
		case OZ_PLUS:	case OZ_MINUS:
			{
			const double phi = DegreeToRad( nChar == OZ_PLUS ? alpha : -alpha );
			const double c = cos( phi );
			const double s = sin( phi );

			set( transform,  c,  s,  0,  0,
				            -s,  c,  0,  0,
				             0,  0,  1,  0,
				             0,  0,  0,  1 );
			}
			break;
		}

		mult( data.matrix, transform );

		set( transform,  1,   0,   0,   0,
		                 0,   1,   0,   0,
		                 0,   0,   1,   0,
			           c.x, c.y, c.z,   1 );

		mult( data.matrix, transform );

		Redraw();
	}
}

void Clab10Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	m_picture.SetFocus();

	CDialog::OnMouseMove(nFlags, point);
}
