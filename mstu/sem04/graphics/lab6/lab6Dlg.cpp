// lab6Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab6.h"
#include "lab6Dlg.h"
#include ".\lab6dlg.h"
#include "myPicture.h"
#include "math.h"
#include "algorithm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define TIMES 5


static size_t first;  // index of first point of new polygon
static bool closed;  // whether current polygon is closed
static bool seed;  // whether seed-point is defined

static CMetaFileDC * pmfsave;  // work variable
static data_t data;  // input data
static CPoint mouse;  // coordinates of mouse pointer


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// Clab6Dlg dialog

Clab6Dlg::Clab6Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(Clab6Dlg::IDD, pParent),
	m_algorithm( ALGO_SORTEDGELIST ),
	m_input( INP_AREA ),
	m_color( RGB( 255, 0, 0 ) ),
	m_delay( FALSE ),
	active( true )
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

Clab6Dlg::~Clab6Dlg()
{
	data.polygon.clear();

	// shutting pmfsave
	HMETAFILE hmfsave = pmfsave->Close();
	DeleteMF( pmfsave, hmfsave );
}

void Clab6Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PICTURE, m_picture);
	DDX_Control(pDX, IDC_MOUSEPOS, m_mousepos);
	DDX_Control(pDX, IDC_COLOR, m_drawcolor);	
	DDX_Radio(pDX, IDC_SORTEDGELIST, m_algorithm);
	DDX_Radio(pDX, IDC_AREA, m_input);
	DDX_Check(pDX, IDC_DELAY, m_delay);
}

BEGIN_MESSAGE_MAP(Clab6Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_DRAW, OnBnClickedDraw)
	ON_BN_CLICKED(IDC_CLEAR, OnBnClickedClear)
	ON_BN_CLICKED(IDC_RESET, OnBnClickedReset)
	ON_BN_CLICKED(IDC_CHOOSE, OnBnClickedChoose)
	ON_BN_CLICKED(IDC_RESEARCH, OnBnClickedResearch)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_BN_CLICKED(IDC_AREA, OnBnClickedArea)
	ON_BN_CLICKED(IDC_DOT, OnBnClickedDot)
	ON_BN_CLICKED(IDC_SORTEDGELIST, OnBnClickedSortedgelist)
	ON_BN_CLICKED(IDC_BYEDGES, OnBnClickedByedges)
	ON_BN_CLICKED(IDC_PARTITION, OnBnClickedPartition)
	ON_BN_CLICKED(IDC_FLAG, OnBnClickedFlag)
	ON_BN_CLICKED(IDC_SEEDING, OnBnClickedSeeding)
END_MESSAGE_MAP()



// Clab6Dlg message handlers

BOOL Clab6Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// Reseting program to initial state
	OnBnClickedReset();
	OnBnClickedSortedgelist();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Clab6Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Clab6Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Clab6Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


static void DrawLine( CDC * pDC, const CmyLine & line )
{
	CGdiObject * pOldPen = pDC->SelectStockObject( BLACK_PEN );

	pDC->MoveTo( line.dot1 );
	pDC->LineTo( line.dot2 );

	pDC->SelectObject( pOldPen );
}

static void DrawDot( CDC * pDC, const CPoint & dot, const COLORREF & color )
{
	CPen pencil( PS_SOLID, 2, color );
	CGdiObject * pOldPen = pDC->SelectObject( &pencil );

	pDC->MoveTo( dot );
	pDC->LineTo( dot );

	pDC->SelectObject( pOldPen );
}

void Clab6Dlg::OnBnClickedDraw()
{
	// reseting picture
	ResetMF( m_picture.pmf );

	UpdateData( true );
	pAlgo Algo = ChooseAlgo( m_algorithm );

	CClientDC dc( &m_picture );

	if ( m_algorithm == ALGO_SEEDING ? seed : true &&
		!data.polygon.empty() && closed )
	{
		Clear( &dc );
		DrawPoly( &dc, data.polygon, RGB( 0, 0, 0 ) );
	}

	Algo( &dc, data, m_color, m_delay );

	m_picture.Invalidate( false );
}

void Clab6Dlg::OnBnClickedClear()
{
	m_picture.Clear();
	CRect area( m_picture.m_DPrect );
	area.DeflateRect( 1, 1, 1, 1 );
	m_picture.pmf->IntersectClipRect( area );

	DrawPoly( m_picture.pmf, data.polygon, RGB( 0, 0, 0 ) );

	if ( m_algorithm == ALGO_SEEDING )
		DrawDot( m_picture.pmf, data.dot, m_color );

	m_picture.Invalidate( false );
}

void Clab6Dlg::OnBnClickedReset()
{
	m_picture.Clear();
	CRect area( m_picture.m_DPrect );
	area.DeflateRect( 1, 1, 1, 1 );
	m_picture.pmf->IntersectClipRect( area );

	m_picture.Invalidate( false );

	RepresentColor( m_color );
	ResetData();
}

void Clab6Dlg::ResetData()
{
	data.dot.SetPoint( -10, -10 );

	if ( !data.polygon.empty() )
	{
		data.polygon.clear();

		// shutting pmfsave
		HMETAFILE hmfsave = pmfsave->Close();
		DeleteMF( pmfsave, hmfsave );
	}

	pmfsave = NewMF( m_picture.pmf );

	first = 0;
	closed = true;
	seed = false;

	GetDlgItem( IDC_DRAW )->EnableWindow( false );
	GetDlgItem( IDC_RESEARCH )->EnableWindow( false );
}

void Clab6Dlg::OnBnClickedChoose()
{
	COLORREF defcolors[ 16 ];
	for ( int i = 0; i < 16; i++ )
		defcolors[ i ] = RGB( 255, 255, 255 );

	CHOOSECOLOR colors;
	colors.lStructSize = sizeof( CHOOSECOLOR );
	colors.hwndOwner = NULL;
	colors.Flags = CC_RGBINIT;
	colors.rgbResult = RGB( 0, 0, 0 );
	colors.lpCustColors = defcolors;

	active = false;
	ChooseColor( &colors );
	RepresentColor( m_color = colors.rgbResult );
	active = true;

	if ( m_algorithm == ALGO_SEEDING ? seed : true &&
		!data.polygon.empty() && closed )
	{
		CClientDC dc( &m_picture );
		Clear( &dc );
		OnBnClickedDraw();
	}
}

void Clab6Dlg::RepresentColor( COLORREF color )
{
	ResetMF( m_drawcolor.pmf );

	CBrush newColor( color );
	CGdiObject * pOldPen = m_drawcolor.pmf->SelectStockObject( BLACK_PEN );
	CGdiObject * pOldBrush = m_drawcolor.pmf->SelectObject( &newColor );
	
	m_drawcolor.pmf->Rectangle( m_drawcolor.m_DPrect );

	m_drawcolor.pmf->SelectObject( pOldPen );
	m_drawcolor.pmf->SelectObject( pOldBrush );

	m_drawcolor.Invalidate( false );
}

void Clab6Dlg::Clear( CDC * pDC )
{
	// Selecting GDI objects
	CGdiObject * pOldBrush = pDC->SelectStockObject( WHITE_BRUSH );
	CGdiObject * pOldPen = pDC->SelectStockObject( BLACK_PEN );

	// Clearing
	pDC->Rectangle( m_picture.m_DPrect );

	// Deselecting GDI objects
	pDC->SelectObject( pOldBrush );
	pDC->SelectObject( pOldPen );
}

void Clab6Dlg::OnBnClickedResearch()
{
	const COLORREF pencolor = RGB( 0, 0, 0 );

	DWORD time[ ALGO_SEEDING - ALGO_SORTEDGELIST + 1 ];

	CClientDC dc( &m_picture );

	for ( unsigned i = ALGO_SORTEDGELIST; i <= ALGO_SEEDING; i++ )
	{
		pAlgo Algo = ChooseAlgo( i );

		DWORD start = GetTickCount();
		for ( unsigned j = 0; j < TIMES; j++ )
		{
			Clear( &dc );
			DrawPoly( &dc, data.polygon, pencolor );
			Algo( &dc, data, m_color, m_delay );
		}
		time[ i ] = GetTickCount() - start;
	}

	CString text( "���������:\n" );
	CString temp( "" );
	temp.Format( "*) � ������������� ������� ���� - %u ����;\n", time[ ALGO_SORTEDGELIST ] );
	text += temp;
	temp.Format( "*) �� ����� - %u ����;\n", time[ ALGO_BYEDGES ] );
	text += temp;
	temp.Format( "*) � ������������ - %u ����;\n", time[ ALGO_PARTITION ] );
	text += temp;
	temp.Format( "*) � ������ - %u ����;\n", time[ ALGO_FLAG ] );
	text += temp;
	temp.Format( "*) ����������� ���������� - %u ����.", time[ ALGO_SEEDING ] );
	text += temp;

	MessageBox( text, "��������� ��������������:" );
}

static void MakeHorVer( CPoint & point, const CPoint & dot1 )
{
	if ( abs( point.x - dot1.x ) < abs( point.y - dot1.y ) )
		point.x = dot1.x;
	else
		point.y = dot1.y;
}

void Clab6Dlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) && active )
	{
		switch ( m_input )
		{
		case INP_AREA:
			{
			// for drawing hor/vert segments
			if ( nFlags == ( MK_CONTROL | MK_LBUTTON ) &&
					!closed && !data.polygon.empty() )
				MakeHorVer( point, data.polygon.back().dot1 );

			if ( closed )
				closed = false;
			else
			{
				// reseting picture
				ResetMF( m_picture.pmf );
				PlayMF( m_picture.pmf, pmfsave );

				// saving point
				data.polygon.back().dot2 = point;
				DrawLine( m_picture.pmf, data.polygon.back() );

				// saving current picture
				ResetMF( pmfsave );
				PlayMF( pmfsave, m_picture.pmf );
			}

			data.polygon.push_back( CmyLine( point, CPoint() ) );
			}
			break;
		case INP_DOT:
			data.dot = point;
			seed = true;

			if( !data.polygon.empty() && closed )
			{
				GetDlgItem( IDC_DRAW )->EnableWindow( true );
				GetDlgItem( IDC_RESEARCH )->EnableWindow( true );
			}
			break;
		}

		if ( m_algorithm = ALGO_SEEDING && seed )
			DrawDot( m_picture.pmf, data.dot, m_color );
		m_picture.Invalidate( false );
	}

	CDialog::OnLButtonDown(nFlags, point);
}

void Clab6Dlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) && active )
	{
		switch ( m_input )
		{
		case INP_AREA:
			if ( closed )
			{
				if ( m_algorithm == ALGO_SEEDING )
				{
					m_input = INP_DOT;
					UpdateData( false );
				}
			}
			else if ( data.polygon.size() - first >= 3 )
			{
				data.polygon.back().dot2 = data.polygon[ first ].dot1;
				first = data.polygon.size();
				closed = true;

				// reseting picture
				ResetMF( m_picture.pmf );
				PlayMF( m_picture.pmf, pmfsave );

				DrawLine( m_picture.pmf, data.polygon.back() );

				// saving current picture
				ResetMF( pmfsave );
				PlayMF( pmfsave, m_picture.pmf );

				if ( m_algorithm = ALGO_SEEDING && seed )
					DrawDot( m_picture.pmf, data.dot, m_color );
				m_picture.Invalidate( false );

				if ( m_algorithm == ALGO_SEEDING ? seed : true )
				{
					GetDlgItem( IDC_DRAW )->EnableWindow( true );
					GetDlgItem( IDC_RESEARCH )->EnableWindow( true );
				}
			}
			else
			{
				GetDlgItem( IDC_DRAW )->EnableWindow( false );
				GetDlgItem( IDC_RESEARCH )->EnableWindow( false );
			}
			break;
		case INP_DOT:
			UpdateData( true );
			m_input = INP_AREA;
			UpdateData( false );
			break;
		}
	}

	CDialog::OnRButtonDown(nFlags, point);
}

void Clab6Dlg::ShowCoord( const CPoint & point )
{
	CString coord( "" );
	coord.Format( "%li : %li", point.x, point.y );
	m_mousepos.SetWindowText( coord );
}

void Clab6Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	mouse = point;
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) && active )
	{
		// for drawing hor/vert segments
		if ( nFlags == MK_CONTROL && !closed && !data.polygon.empty() )
			MakeHorVer( point, data.polygon.back().dot1 );

		ShowCoord( point );

		if ( !closed && !data.polygon.empty() )
		{
			// reseting picture
			ResetMF( m_picture.pmf );
			PlayMF( m_picture.pmf, pmfsave );

			// drawing current segment
			data.polygon.back().dot2 = point;

			DrawLine( m_picture.pmf, data.polygon.back() );
			if ( m_algorithm = ALGO_SEEDING && seed )
				DrawDot( m_picture.pmf, data.dot, m_color );

			m_picture.Invalidate( false );
		}

		m_picture.SetFocus();
	}

	CDialog::OnMouseMove(nFlags, point);
}

void Clab6Dlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if ( nChar == VK_CONTROL )
		OnMouseMove( MK_CONTROL, mouse );

	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}

void Clab6Dlg::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if ( nChar == VK_CONTROL )
		OnMouseMove( 0, mouse );

	CDialog::OnKeyUp(nChar, nRepCnt, nFlags);
}

void Clab6Dlg::OnBnClickedArea()
{
	m_input = INP_AREA;
	UpdateData( false );
}

void Clab6Dlg::OnBnClickedDot()
{
	m_input = INP_DOT;
	UpdateData( false );
}

void Clab6Dlg::OnBnClickedSortedgelist()
{
	m_algorithm = ALGO_SORTEDGELIST;
	m_input = INP_AREA;
	UpdateData( false );

	GetDlgItem( IDC_DOT )->EnableWindow( false );
	GetDlgItem( IDC_DRAW )->EnableWindow( !data.polygon.empty() && closed );
	GetDlgItem( IDC_RESEARCH )->EnableWindow( !data.polygon.empty() && closed );
}

void Clab6Dlg::OnBnClickedByedges()
{
	m_algorithm = ALGO_BYEDGES;
	m_input = INP_AREA;
	UpdateData( false );

	GetDlgItem( IDC_DOT )->EnableWindow( false );
	GetDlgItem( IDC_DRAW )->EnableWindow( !data.polygon.empty() && closed );
	GetDlgItem( IDC_RESEARCH )->EnableWindow( !data.polygon.empty() && closed );
}

void Clab6Dlg::OnBnClickedPartition()
{
	m_algorithm = ALGO_PARTITION;
	m_input = INP_AREA;
	UpdateData( false );

	GetDlgItem( IDC_DOT )->EnableWindow( false );
	GetDlgItem( IDC_DRAW )->EnableWindow( !data.polygon.empty() && closed );
	GetDlgItem( IDC_RESEARCH )->EnableWindow( !data.polygon.empty() && closed );
}

void Clab6Dlg::OnBnClickedFlag()
{
	m_algorithm = ALGO_FLAG;
	m_input = INP_AREA;
	UpdateData( false );

	GetDlgItem( IDC_DOT )->EnableWindow( false );
	GetDlgItem( IDC_DRAW )->EnableWindow( !data.polygon.empty() && closed );
	GetDlgItem( IDC_RESEARCH )->EnableWindow( !data.polygon.empty() && closed );
}

void Clab6Dlg::OnBnClickedSeeding()
{
	m_algorithm = ALGO_SEEDING;
	UpdateData( false );

	GetDlgItem( IDC_DOT )->EnableWindow( true );
	GetDlgItem( IDC_DRAW )->EnableWindow( m_algorithm == ALGO_SEEDING ? seed : true &&
		!data.polygon.empty() && closed );
	GetDlgItem( IDC_RESEARCH )->EnableWindow( m_algorithm == ALGO_SEEDING ? seed : true &&
		!data.polygon.empty() && closed );
}
