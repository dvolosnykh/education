#pragma once


struct coeff_t
{
	double A;
	double B;
	double C;
};


// CmyLine

class CmyLine
{
public:
	CPoint dot1;
	CPoint dot2;

public:
	CmyLine();
	CmyLine( CPoint & p1, CPoint & p2 );
	~CmyLine();

	long Ymin();
	long Ymax();
	long Xmin();
	long Xmax();
	double Length();
	bool IsValid();
	bool IsVertical();
	bool IsHorizontal();
	double Inclination();
	bool IsParallelTo( CmyLine & line );
	CPoint IntersectWith( CmyLine & line );
	coeff_t FindCoeffs();

	void Draw( CDC * pDC );

	CmyLine operator = ( const CmyLine & line );
	bool operator == ( const CmyLine & line );
	bool operator != ( const CmyLine & line );
};

