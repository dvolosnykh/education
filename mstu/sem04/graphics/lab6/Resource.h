//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by lab6.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MAINWND                     102
#define IDR_MAINFRAME                   128
#define IDD_RESEARCH                    129
#define IDC_PICTURE                     1001
#define IDC_SORTEDGELIST                1003
#define IDC_BYEDGES                     1004
#define IDC_PARTITION                   1005
#define IDC_FLAG                        1006
#define IDC_SEEDING                     1007
#define IDC_DRAW                        1019
#define IDC_CLEAR                       1023
#define IDC_CHOOSE                      1026
#define IDC_COLOR                       1027
#define IDC_RESEARCH                    1032
#define IDC_GRAPH                       1033
#define IDC_ALGO_COLOR                  1034
#define IDC_MOUSEPOS                    1041
#define IDC_RESET                       1044
#define IDC_AREA                        1045
#define IDC_DOT                         1046
#define IDC_DELAY                       1047

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1048
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
