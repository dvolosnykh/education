#include "stdafx.h"
#include "algorithm.h"
#include "myLine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

typedef vector< CPoint > array_t;

// ========== SORTED EDGE LIST ==========

static void swap( CPoint & a, CPoint & b )
{
	CPoint temp = a;
	a = b;
	b = temp;
}

static void reverse( CmyLine & line )
{
	swap( line.dot1, line.dot2 );
}

static void JustifyY( polygon_t & poly )
{
	for ( size_t i = 0; i < poly.size(); i++ )
		if ( poly[ i ].dot1.y > poly[ i ].dot2.y )
			reverse( poly[ i ] );
}

static long findYmax( const polygon_t & poly )
{
	long ymax = poly[ 0 ].dot2.y;

	for ( size_t i = 1; i < poly.size(); i++ )
		if ( poly[ i ].dot2.y > ymax )
			ymax = poly[ i ].dot2.y;

	return ( ymax );
}

static void Intersect( array_t & point, const polygon_t & poly )
{
	const long ymin = poly.front().dot1.y;
	const long ymax = findYmax( poly );

	size_t first = 0;
	for ( long y = ymin; y <= ymax; y++ )
	{
		// finding intersection points with current active edges
		for ( size_t i = first; i < poly.size(); i++ )
		{
			CmyLine cur = poly[ i ];

			if ( !cur.IsHorizontal() && cur.dot1.y < y && y <= cur.dot2.y )
			{
				CmyLine hor( CPoint( 0, y ), CPoint( 100, y ) );
				CPoint I = hor.IntersectWith( cur );

				if ( !point.empty() && I.y == point.back().y &&
						abs( I.x - point.back().x ) <= 1 )
					point.erase( &point[ point.size() - 1 ] );
				else
					point.push_back( I );
			}
		}

		// separating worked out edges
		for ( ; y == poly[ first ].dot2.y; first++ );
	}
}

static size_t LastActiveDot( const array_t & point, size_t first )
{
	const long & y0 = point[ first ].y;

	size_t last;
	for ( last = first + 1; last < point.size() &&
		point[ last ].y == y0; last++ );

	return ( --last );
}

// sorting dots in y-increasing order
// dots with same y-coordinates are sorted in x-increasing order
static int dotcmp( const void * d1, const void * d2 )
{
	const CPoint & dot1 = *( CPoint * )d1;
	const CPoint & dot2 = *( CPoint * )d2;

	return ( dot1.y < dot2.y ? -1:
		dot1.y > dot2.y ?  1 :
		dot1.x < dot2.x ? -1 :
		dot1.x > dot2.x ?  1 :
		0 );
}

// sorting lines in ymax-increasing order
// lines with same ymax-values are sorted in ymin-increasing order
static int qcmp( const void * l1, const void * l2 )
{
	const CmyLine & line1 = *( CmyLine * )l1;
	const CmyLine & line2 = *( CmyLine * )l2;

	const long & ymin1 = line1.dot1.y;
	const long & ymax1 = line1.dot2.y;
	const long & ymin2 = line2.dot1.y;
	const long & ymax2 = line2.dot2.y;
	
	return ( ymin1 < ymin2 ? -1 :
		ymin1 > ymin2 ?  1 :
		ymax1 < ymax2 ? -1 :
		ymax1 > ymax2 ?  1 :
		0 );
}

static void fill( CDC * pDC, const array_t & point, const COLORREF & color,
				 BOOL delay )
{
	for ( size_t i = 0; i < point.size(); )
	{
		size_t last = LastActiveDot( point, i );

		for ( ; i <= last; i++ )
			for ( CPoint dot = point[ i++ ]; ++dot.x < point[ i ].x; )
				pDC->SetPixel( dot, color );

		if ( delay )
			Sleep( PAUSE );
	}
}


void AlgoSortEdgeList( CDC * pDC, data_t data, const COLORREF & color, BOOL delay )
{
	const COLORREF bgcolor = RGB( 255, 255, 255 );
	const COLORREF pencolor = RGB( 0, 0, 0 );

	// arranging and sorting edges
	polygon_t & poly = data.polygon;
	JustifyY( poly );
	qsort( &poly.front(), poly.size(), sizeof( poly.front() ), qcmp );

	// defining and sorting intersection points
	array_t point;
	Intersect( point, poly );
	qsort( &point.front(), point.size(), sizeof( point.front() ), dotcmp );

	// filling areas and drawing border
	fill( pDC, point, color, delay );
	DrawPoly( pDC, poly, pencolor );
}
