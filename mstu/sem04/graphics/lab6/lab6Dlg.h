// lab6Dlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "lab6.h"

#include "myPicture.h"
#include "algorithm.h"


enum
{
	INP_AREA,
	INP_DOT
};


// Clab6Dlg dialog
class Clab6Dlg : public CDialog
{
private:
	CmyPicture m_picture;
	CStatic m_mousepos;
	CmyPicture m_drawcolor;
	int m_algorithm;
	int m_input;
	BOOL m_delay;
	bool active;

	COLORREF m_color;

	data_t data;

// Construction
public:
	Clab6Dlg(CWnd* pParent = NULL);	// standard constructor
	~Clab6Dlg();

	void ResetData();
	void RepresentColor( COLORREF color );
	void Clear( CDC * pDC );

// Dialog Data
	enum { IDD = IDD_MAINWND };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
private:
	void ShowCoord( const CPoint & point );

protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTcnSelchangeDrawstyle(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedDraw();
	afx_msg void OnBnClickedClear();
	afx_msg void OnBnClickedReset();
	afx_msg void OnBnClickedChoose();
	afx_msg void OnBnClickedResearch();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnBnClickedArea();
	afx_msg void OnBnClickedDot();
	afx_msg void OnBnClickedSortedgelist();
	afx_msg void OnBnClickedByedges();
	afx_msg void OnBnClickedPartition();
	afx_msg void OnBnClickedFlag();
	afx_msg void OnBnClickedSeeding();
};
