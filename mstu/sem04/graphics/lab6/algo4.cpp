#include "stdafx.h"
#include "algorithm.h"
#include "myLine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ========== WITH FLAGS ==========

static void swap( CPoint & a, CPoint & b )
{
	CPoint temp = a;
	a = b;
	b = temp;
}

static void reverse( CmyLine & line )
{
	swap( line.dot1, line.dot2 );
}

static void JustifyX( polygon_t & poly )
{
	for ( size_t i = 0; i < poly.size(); i++ )
		if ( poly[ i ].dot1.x > poly[ i ].dot2.x )
			reverse( poly[ i ] );
}

static void JustifyY( polygon_t & poly )
{
	for ( size_t i = 0; i < poly.size(); i++ )
		if ( poly[ i ].dot1.y > poly[ i ].dot2.y )
			reverse( poly[ i ] );
}


static long findXmin( const polygon_t & poly )
{
	long xmin = poly[ 0 ].dot1.x;

	for ( size_t i = 1; i < poly.size(); i++ )
		if ( poly[ i ].dot1.x < xmin )
			xmin = poly[ i ].dot1.x;

	return ( xmin );
}

static long findXmax( const polygon_t & poly )
{
	long xmax = poly[ 0 ].dot2.x;

	for ( size_t i = 1; i < poly.size(); i++ )
		if ( poly[ i ].dot2.x > xmax )
			xmax = poly[ i ].dot2.x;

	return ( xmax );
}

static long findYmin( const polygon_t & poly )
{
	long ymin = poly[ 0 ].dot1.y;

	for ( size_t i = 1; i < poly.size(); i++ )
		if ( poly[ i ].dot1.y < ymin )
			ymin = poly[ i ].dot1.y;

	return ( ymin );
}

static long findYmax( const polygon_t & poly )
{
	long ymax = poly[ 0 ].dot2.y;

	for ( size_t i = 1; i < poly.size(); i++ )
		if ( poly[ i ].dot2.y > ymax )
			ymax = poly[ i ].dot2.y;

	return ( ymax );
}


static void fill( CDC * pDC, polygon_t & poly, const COLORREF & color, BOOL delay )
{
	const COLORREF bgcolor = RGB( 255, 255, 255 );
	const COLORREF pencolor = RGB( 0, 0, 0 );

	// defining limits
	JustifyX( poly );
	const long xmin = findXmin( poly );
	const long xmax = findXmax( poly );

	JustifyY( poly );
	const long ymin = findYmin( poly );
	const long ymax = findYmax( poly );

	// drawing
	for ( long y = ymin; y <= ymax; y++ )
	{
		// finding intersection points with current active edges
		for ( size_t i = 0; i < poly.size(); i++ )
		{
			CmyLine cur = poly[ i ];

			if ( !cur.IsHorizontal() && cur.dot1.y < y && y <= cur.dot2.y )
			{
				CmyLine hor( CPoint( 0, y ), CPoint( 100, y ) );
				CPoint I = hor.IntersectWith( cur );

				pDC->SetPixel( I, pDC->GetPixel( I ) == pencolor ? bgcolor : pencolor );
			}
		}

		// filling line
		bool inside = false;
		for ( long x = xmin; x < xmax; x++ )
			if ( pDC->GetPixel( x, y ) == pencolor )
				inside = !inside;
			else
				pDC->SetPixel( x, y, inside ? color : bgcolor );

		if ( delay )
			Sleep( PAUSE );
	}
}


void AlgoFlag( CDC * pDC, data_t data, const COLORREF & color, BOOL delay )
{
	const COLORREF bgcolor = RGB( 255, 255, 255 );
	const COLORREF pencolor = RGB( 0, 0, 0 );

	polygon_t & poly = data.polygon;

	// erasing borders
	DrawPoly( pDC, poly, bgcolor );

	fill( pDC, poly, color, delay );

	// drawing borders
	DrawPoly( pDC, poly, pencolor );
}
