#include "stdafx.h"
#include "algorithm.h"
#include "myLine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static void DrawLine( CDC * pDC, const CmyLine line )
{
	pDC->MoveTo( line.dot1 );
	pDC->LineTo( line.dot2 );
}

void DrawPoly( CDC * pDC, const polygon_t & poly, const COLORREF color )
{
	CPen pencil( PS_SOLID, 1, color );
	CGdiObject * pOldPen = pDC->SelectObject( &pencil );

	for ( size_t i = 0; i < poly.size(); i++ )
		DrawLine( pDC, poly[ i ] );

	pDC->SelectObject( pOldPen );
}

pAlgo ChooseAlgo( const int nAlgorithm )
{
	pAlgo choosed;
	
	switch ( nAlgorithm )
	{
	case ALGO_SORTEDGELIST:
		choosed = AlgoSortEdgeList;
		break;
	case ALGO_BYEDGES:
		choosed = AlgoByEdges;
		break;
	case ALGO_PARTITION:
		choosed = AlgoPartition;
		break;
	case ALGO_FLAG:
		choosed = AlgoFlag;
		break;
	case ALGO_SEEDING:
		choosed = AlgoSeeding;
		break;
	}

	return ( choosed );
}
