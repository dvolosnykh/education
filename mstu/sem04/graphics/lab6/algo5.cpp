#include "stdafx.h"
#include "algorithm.h"
#include "myLine.h"

#include <stack>
using namespace std;

typedef stack< CPoint > stack_t;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ========== SEEDING ==========

// fills from current point to the left and returns leftmost x
static long FillToLeft( CDC * pDC, CPoint cur, const COLORREF & color,
					   const BOOL delay )
{
	const COLORREF pencolor = RGB( 0, 0, 0 );

	for ( ; ; )
	{
		cur.x--;

	if ( pDC->GetPixel( cur ) == pencolor ) break;

		pDC->SetPixel( cur, color );
	}

	return ( ++cur.x );
}

// fills from current point to the right and returns rightmost x
static long FillToRight( CDC * pDC, CPoint cur, const COLORREF & color,
						const BOOL delay )
{
	const COLORREF pencolor = RGB( 0, 0, 0 );

	for ( ; ; )
	{
		cur.x++;

	if ( pDC->GetPixel( cur ) == pencolor ) break;

		pDC->SetPixel( cur, color );
	}

	return ( --cur.x );
}

static void CheckLine( stack_t & seeds, CDC * pDC, CPoint & cur, const long left,
					  const long right, const COLORREF & color )
{
	const COLORREF pencolor = RGB( 0, 0, 0 );

	for ( cur.x = left; cur.x <= right; )
	{
		bool found;
		COLORREF curcolor;

		for ( found = false; ; cur.x++ )
		{
			curcolor = pDC->GetPixel( cur );

		if ( curcolor == pencolor || curcolor == color ||
				cur.x == right )
			break;

			found = true;
		}

		if ( found )
		{
			seeds.push( curcolor == pencolor || curcolor == color ?
				CPoint( cur.x - 1, cur.y ) : cur );

			found = false;
		}
		else if ( curcolor != pencolor && curcolor != color )
			seeds.push( cur );

		// skipping filled segment
		long enter = cur.x;

		for ( ; ; cur.x++ )
		{
			curcolor = pDC->GetPixel( cur );

		if ( curcolor != pencolor && curcolor != color || cur.x == right ) break;
		}

		if ( cur.x == enter )
			cur.x++;
    }
}

static void CheckUpper( stack_t & seeds, CDC * pDC, CPoint cur, const long left,
					   const long right, const COLORREF & color )
{
	cur.y++;

	CheckLine( seeds, pDC, cur, left, right, color );
}

static void CheckLower( stack_t & seeds, CDC * pDC, CPoint cur, const long left,
					   const long right, const COLORREF & color )
{
	cur.y--;

	CheckLine( seeds, pDC, cur, left, right, color );
}


void AlgoSeeding( CDC * pDC, data_t data, const COLORREF & color, BOOL delay )
{
	const COLORREF pencolor = RGB( 0, 0, 0 );

	stack_t seeds;
	seeds.push( data.dot );

	do
	{
		CPoint cur = seeds.top();
		seeds.pop();

		pDC->SetPixel( cur, color );

		const long left = FillToLeft( pDC, cur, color, delay );
		const long right = FillToRight( pDC, cur, color, delay );

		CheckUpper( seeds, pDC, cur, left, right, color );
		CheckLower( seeds, pDC, cur, left, right, color );

		if ( delay )
			Sleep( PAUSE );
	}
	while ( !seeds.empty() );
}
