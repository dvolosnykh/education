#pragma once

#include <vector>
#include "myLine.h"

using namespace std;

#define PAUSE 1

enum
{
	ALGO_SORTEDGELIST,
	ALGO_BYEDGES,
	ALGO_PARTITION,
	ALGO_FLAG,
	ALGO_SEEDING
};


typedef vector< CmyLine > polygon_t;

struct data_t
{
	polygon_t polygon;
	CPoint dot;
};

typedef void ( *pAlgo )( CDC * pDC, data_t data, const COLORREF & color, BOOL delay );

//void wait( const DWORD msec );
void DrawPoly( CDC * pDC, const polygon_t & poly, const COLORREF color );

void AlgoSortEdgeList( CDC * pDC, data_t data, const COLORREF & color, BOOL delay );
void AlgoByEdges( CDC * pDC, data_t data, const COLORREF & color, BOOL delay );
void AlgoPartition( CDC * pDC, data_t data, const COLORREF & color, BOOL delay );
void AlgoFlag( CDC * pDC, data_t data, const COLORREF & color, BOOL delay );
void AlgoSeeding( CDC * pDC, data_t data, const COLORREF & color, BOOL delay );

pAlgo ChooseAlgo( const int nAlgorithm );
