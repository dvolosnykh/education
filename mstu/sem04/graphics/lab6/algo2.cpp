#include "stdafx.h"
#include "algorithm.h"
#include "myLine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ========== BY EDGES ==========

static long round( double x )
{
	return ( long )( x + 0.5 );
}

static void swap( CPoint & a, CPoint & b )
{
	CPoint temp = a;
	a = b;
	b = temp;
}

static void reverse( CmyLine & line )
{
	swap( line.dot1, line.dot2 );
}

static void JustifyX( polygon_t & poly )
{
	for ( size_t i = 0; i < poly.size(); i++ )
		if ( poly[ i ].dot1.x > poly[ i ].dot2.x )
			reverse( poly[ i ] );
}

static void JustifyY( polygon_t & poly )
{
	for ( size_t i = 0; i < poly.size(); i++ )
		if ( poly[ i ].dot1.y > poly[ i ].dot2.y )
			reverse( poly[ i ] );
}

static long findXmax( const polygon_t & poly )
{
	long xmax = poly[ 0 ].dot2.x;

	for ( size_t i = 1; i < poly.size(); i++ )
		if ( poly[ i ].dot2.x > xmax )
			xmax = poly[ i ].dot2.x;

	return ( xmax );
}

static void fill( CDC * pDC, polygon_t & poly,
				 const COLORREF & color, BOOL delay )
{
	const COLORREF bgcolor = RGB( 255, 255, 255 );

	JustifyX( poly );
	const long xmax = findXmax( poly );
	JustifyY( poly );

	for ( size_t i = 0; i < poly.size(); i++ )
	{
		const int deltax = poly[ i ].dot2.x - poly[ i ].dot1.x;
		const int deltay = poly[ i ].dot2.y - poly[ i ].dot1.y;
		const int L = max( abs( deltax ), abs( deltay ) );
		double dx = deltax / ( double )L;
		double dy = deltay / ( double )L;

		CPoint dot( poly[ i ].dot1 );

		long prevY = dot.y;

		bool quit = false;
		for ( double x = dot.x, y = dot.y; !quit; x += dx, y += dy )
		{
			dot.x = round( x );
			dot.y = round( y );

			if ( dot == poly[ i ].dot2 )
				quit = true;

			if ( dot.y != prevY )
			{
				// inverting pixels in a row
				for ( ; dot.x <= xmax; dot.x++ )
					pDC->SetPixel( dot, pDC->GetPixel( dot ) == color ? bgcolor : color );

				prevY = dot.y;

				if ( delay )
					Sleep( PAUSE );
			}
		}
	}
}


void AlgoByEdges( CDC * pDC, data_t data, const COLORREF & color, BOOL delay )
{
	const COLORREF bgcolor = RGB( 255, 255, 255 );
	const COLORREF pencolor = RGB( 0, 0, 0 );

	polygon_t & poly = data.polygon;

	fill( pDC, poly, color, delay );

	DrawPoly( pDC, poly, pencolor );
}
