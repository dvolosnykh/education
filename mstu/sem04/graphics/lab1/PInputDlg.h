#pragma once

#include "mySet.h"
#include "myPoint.h"
#include "myListCtrl.h"


// CPInputDlg dialog

class CPInputDlg : public CDialog
{
	DECLARE_DYNAMIC( CPInputDlg )

public:
	CEdit m_editX;
	CEdit m_editY;
	CmyPoint newpoint;

private:
	bool hasdefault;
	CmyListCtrl * pParentWnd;

public:
	CPInputDlg( CWnd* pParent );   // standard constructor
	CPInputDlg( CWnd* pParent, CmyPoint & defpoint );
	virtual ~CPInputDlg();

// Dialog Data
	enum { IDD = IDD_POINTINPUT };

protected:
	virtual void DoDataExchange( CDataExchange* pDX );    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
};
