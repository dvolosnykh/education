#include "stdafx.h"

#include "mySet.h"
#include "myPoint.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CmySet

CmySet::CmySet()
{
	n = 0;
}

CmySet::~CmySet()
{
}

void CmySet::Add( const CmyPoint & point )
{
	dot[ n++ ] = point;
}

void CmySet::Replace( const int index, const CmyPoint & point )
{
	dot[ index ] = point;
}

void CmySet::Delete( const int index )
{
	for ( int i = index + 1; i < n; i++ )
		dot[ i - 1 ] = dot[ i ];

	n--;
}

void CmySet::DeleteAll()
{
	n = 0;
}