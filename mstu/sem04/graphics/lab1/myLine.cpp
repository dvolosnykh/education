#include "stdafx.h"

#include "math.h"
#include "myLine.h"

#define PI 3.141592653589793

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//CmyLine

CmyLine::CmyLine()
{
}

CmyLine::CmyLine( CmyPoint & p1, CmyPoint & p2 )
{
	dot[ ONE ] = p1;
	dot[ TWO ] = p2;
}

CmyLine::~CmyLine()
{
}

bool CmyLine::IsValid()
{
	return ( Length() > 0 );
}

double CmyLine::Length()
{
	double dx = dot[ ONE ].x - dot[ TWO ].x;
	double dy = dot[ ONE ].y - dot[ TWO ].y;

	return ( hypot( dx, dy ) );
}

bool CmyLine::IsVertical()
{
	return ( dot[ ONE ].x == dot[ TWO ].x  );
}

double CmyLine::Inclination()
{
	double dx = dot[ ONE ].x - dot[ TWO ].x;
	double dy = dot[ ONE ].y - dot[ TWO ].y;

	if ( dy < 0 )
	{
		dy *= -1;
		dx *= -1;
	}

	return ( dx == 0 ? 0 : atan2( dy, dx ) );
}

CmyPoint CmyLine::IntersectWith( CmyLine & line )
{
	FindCoeffs();
	line.FindCoeffs();

	CmyPoint I;
	I.x = ( line.eqn.B * eqn.C - eqn.B * line.eqn.C ) / ( line.eqn.A * eqn.B - eqn.A * line.eqn.B );
	I.y = - ( eqn.A * I.x + eqn.C ) / eqn.B;

	return ( I );
}

void CmyLine::FindCoeffs()
{
	CmyPoint & dot1 = dot[ ONE ];
	CmyPoint & dot2 = dot[ TWO ];

	eqn.A = dot1.y - dot2.y;
	eqn.B = dot2.x - dot1.x;
	eqn.C = dot1.x * dot2.y - dot2.x * dot1.y;
}

double FixCoord( const double a, const double b, const double t )
{
	return ( a + t * ( b - a ) );
}

CmyPoint CmyLine::FixPoint( const id_t ID, const double t )
{
	CmyPoint & dot1 = dot[ ID ];
	CmyPoint & dot2 = dot[ ID == ONE ? TWO : ONE ];
	CmyPoint fix;

	fix.x = FixCoord( dot1.x, dot2.x, t );
	fix.y = FixCoord( dot1.y, dot2.y, t );

	return ( fix );
}