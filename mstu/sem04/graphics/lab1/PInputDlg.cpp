// PInputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab1.h"

#include "PInputDlg.h"
#include ".\PInputDlg.h"


// CPInputDlg dialog

IMPLEMENT_DYNAMIC( CPInputDlg, CDialog )

CPInputDlg::CPInputDlg( CWnd* pParent )
	: CDialog( CPInputDlg::IDD, pParent )
{
	hasdefault = false;
	pParentWnd = ( CmyListCtrl * )pParent;
}

CPInputDlg::CPInputDlg( CWnd* pParent, CmyPoint & defpoint )
	: CDialog( CPInputDlg::IDD, pParent )
{
	newpoint = defpoint;
	hasdefault = true;
	pParentWnd = ( CmyListCtrl * )pParent;
}

CPInputDlg::~CPInputDlg()
{
}

void CPInputDlg::DoDataExchange( CDataExchange* pDX )
{
	CDialog::DoDataExchange( pDX );
	DDX_Control( pDX, IDC_EDITX, m_editX );
	DDX_Control( pDX, IDC_EDITY, m_editY );
}


BEGIN_MESSAGE_MAP( CPInputDlg, CDialog )
END_MESSAGE_MAP()

// My own

double GetValue( CEdit & edit, bool & correct )
{
	const int length = edit.GetWindowTextLength() + 1;
	char * temp = new char [ length ];
	edit.GetWindowText( temp, length );

	char * pError;
	double dest = strtod( temp, &pError );

	correct = ( pError != temp && *pError == '\0' );

	delete temp;

	return ( dest );
}

// CPInputDlg message handlers

BOOL CPInputDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	if ( hasdefault )
	{
		CString temp;
		temp.Format( "%.*lf", pParentWnd->precision, newpoint.x );
		m_editX.SetWindowText( temp );
		temp.Format( "%.*lf", pParentWnd->precision, newpoint.y );
		m_editY.SetWindowText( temp );

		UpdateData( false );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

static void Retype( CEdit & edit )
{
	edit.SetFocus();
	edit.SetSel( 0, -1, true );
}

void CPInputDlg::OnOK()
{
	UpdateData( true );

	bool XisEmpty = m_editX.GetWindowTextLength() == 0;
	bool YisEmpty = m_editY.GetWindowTextLength() == 0;

	bool XisCorrect;
	bool YisCorrect;
	newpoint.x = GetValue( m_editX, XisCorrect );
	newpoint.y = GetValue( m_editY, YisCorrect );

	if ( XisCorrect && YisCorrect )
		CDialog::OnOK();
    else 
	{
		CString msg( "" );

		if ( !XisCorrect )
			msg += CString( "���������� X " ) + CString( XisEmpty ? "�� �������." : "������� �� ���������." );
		
		if ( !YisCorrect )
		{
			if ( !XisCorrect )
				msg += CString( "\n" );

			msg += CString( "���������� Y " ) + CString( YisEmpty ? "�� �������." : "������� �� ���������." );
		}

		MessageBox( msg, "������" );

		Retype( !XisCorrect ? m_editX : m_editY );
	}
}
