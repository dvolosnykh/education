#pragma once


// CmyMenu

class CmyMenu : public CMenu
{
	DECLARE_DYNAMIC( CmyMenu )

public:
	CmyMenu();
	virtual ~CmyMenu();

	void Initialize( CWnd * pNewParent );
	void ToggleItems();
	void DropMenu();

private:
	CWnd * pTempParent;
};
