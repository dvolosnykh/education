//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by lab1.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MAINWND                     102
#define IDS_TOOLTIP                     102
#define IDR_MAINFRAME                   128
#define IDR_POPUP                       129
#define IDD_POINTINPUT                  131
#define IDD_PRECISION                   134
#define IDC_PICTURE                     1001
#define IDC_SETA                        1002
#define IDC_EDITX                       1003
#define IDC_SETB                        1004
#define IDC_EDITY                       1004
#define IDC_SOLVE                       1006
#define IDC_SPIN                        1010
#define IDC_PRECISION                   1011
#define IDC_TOOLTIP                     1012
#define IDC_RESET                       1013
#define IDC_BUTTON1                     1014
#define IDC_DEFAULT                     1014
#define IDR_POPUP_DELETEALL             32822
#define IDR_POPUP_DELETE                32823
#define IDR_POPUP_EDIT                  32824
#define IDR_POPUP_ADD                   32825
#define IDR_POPUP_PRECISION             32827

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32840
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
