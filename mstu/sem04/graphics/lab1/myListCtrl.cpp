#include "stdafx.h"
#include "lab1.h"

#include "myListCtrl.h"
#include ".\myListCtrl.h"
#include "PInputDlg.h"
#include "lab1Dlg.h"
#include "PrecDlg.h"


// CmyListCtrl

IMPLEMENT_DYNAMIC( CmyListCtrl, CListCtrl )
CmyListCtrl::CmyListCtrl()
{
	precision = DEFAULTPRECISION;
}

CmyListCtrl::~CmyListCtrl()
{
}


BEGIN_MESSAGE_MAP( CmyListCtrl, CListCtrl )
	ON_COMMAND( IDR_POPUP_ADD, OnPopupAdd )
	ON_COMMAND( IDR_POPUP_EDIT, OnPopupEdit )
	ON_COMMAND( IDR_POPUP_DELETE, OnPopupDelete )
	ON_COMMAND( IDR_POPUP_DELETEALL, OnPopupDeleteall )
	ON_COMMAND( IDR_POPUP_PRECISION, OnPopupPrecision )
	ON_WM_CONTEXTMENU()

	ON_NOTIFY_REFLECT( LVN_KEYDOWN, OnLvnKeydown )
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
END_MESSAGE_MAP()


void CmyListCtrl::Initialize( CWnd * pParentWnd )
{
	SetExtendedStyle( LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );
	InsertColumn( 1, "�", LVCFMT_CENTER, 30, 0 );
	InsertColumn( 2, "X", LVCFMT_RIGHT, 70, 0 );
	InsertColumn( 3, "Y", LVCFMT_RIGHT, 70, 0 );

	pParent = pParentWnd;

	m_pmenu.Initialize( this );
}

void CmyListCtrl::AddPoint( CmyPoint & newpoint )
{
	const int num = GetItemCount();
	CString str;

	str.Format( "%d", num + 1 );
	InsertItem( num, str );
	PutCoordinates( num, newpoint );

	m_set.Add( newpoint );

	pParent->PostMessage( WM_SETCHANGED );
}

void CmyListCtrl::ReplacePoint( const int & index, CmyPoint & newpoint )
{
	PutCoordinates( index, newpoint );
	m_set.Replace( index, newpoint );
}

void CmyListCtrl::DeletePoint( const int & index )
{
	DeleteItem( index );
	m_set.Delete( index );
	pParent->PostMessage( WM_SETCHANGED );
}

void CmyListCtrl::DeleteAllPoints()
{
	DeleteAllItems();
	m_set.DeleteAll();
	pParent->PostMessage( WM_SETCHANGED );
}


void CmyListCtrl::PutCoordinates( const int & index, CmyPoint & point )
{
	CString str;
	str.Format( "%.*lf", precision, point.x );
	SetItemText( index, 1, str );
	str.Format( "%.*lf", precision, point.y );
	SetItemText( index, 2, str );
}

void CmyListCtrl::Renumerate( const int & first )
{
	for ( unsigned i = first, count = GetItemCount(); i < count; i++ )
	{
		CString str;
		str.Format( "%i", i + 1 );
		SetItemText( i, 0, str );
	}
}

int CmyListCtrl::GetFirstSelectedIndex()
{
	POSITION pos = GetFirstSelectedItemPosition();
	return ( GetNextSelectedItem( pos ) );
}


// Working with menu

void CmyListCtrl::OnPopupAdd()
{
	if ( GetItemCount() == CmySet::NMAX )
		MessageBox( "���������� ����������� ���������� �����.", "���������" );
	else
	{
		CPInputDlg dlg( this );
		
		if ( dlg.DoModal() == IDOK )
			AddPoint( dlg.newpoint );
	}
}

void CmyListCtrl::OnPopupEdit()
{
	if ( GetItemCount() == 0 )
		MessageBox( "��������� ����� �����.", "���������" );
	else if ( GetFirstSelectedIndex() == -1 )
		MessageBox( "�� ���� ����� �� �������.", "���������" );
	else
	{
		for ( POSITION pos = GetFirstSelectedItemPosition(); ; )
		{
			const int index = GetNextSelectedItem( pos );

			CPInputDlg dlg( this, m_set.dot[ index ] );

			if ( dlg.DoModal() == IDOK )
				ReplacePoint( index, dlg.newpoint );

		if ( pos == NULL ) break;
		}
	}
}

void CmyListCtrl::OnPopupDelete()
{
	if ( GetItemCount() == 0 )
		MessageBox( "��������� ����� �����.", "���������" );
	else 
	{
		const int first = GetFirstSelectedIndex();

		if ( first == -1 )
			MessageBox( "�� ���� ����� �� �������.", "���������" );
		else
		{
			for ( POSITION pos = GetFirstSelectedItemPosition(); ; pos-- )
			{
				DeletePoint( GetNextSelectedItem( pos ) );

			if ( pos == NULL ) break;
			}
			
			Renumerate( first );
		}
	}
}

void CmyListCtrl::OnPopupDeleteall()
{
	if ( GetItemCount() == 0 )
		MessageBox( "��������� ����� �����.", "���������" );
	else
	{
		DeleteAllPoints();
		MessageBox( "��������� ����� ��������.", "���������" );
	}
}

void CmyListCtrl::OnPopupPrecision()
{
	CPrecDlg dlg( precision, this );
		
	if ( dlg.DoModal() == IDOK )
	{
		precision = dlg.newprecision;

		for ( int i = 0; i < GetItemCount(); i++ )
			PutCoordinates( i, m_set.dot[ i ] );
	}
}

void CmyListCtrl::OnContextMenu( CWnd* /*pWnd*/, CPoint /*point*/ )
{
	m_pmenu.ToggleItems();
	m_pmenu.DropMenu();
}


// Hotkeys

void CmyListCtrl::OnLvnKeydown( NMHDR *pNMHDR, LRESULT *pResult )
{
	LPNMLVKEYDOWN pLVKeyDown = reinterpret_cast<LPNMLVKEYDOWN>( pNMHDR );

	switch ( pLVKeyDown->wVKey )
	{
	case VK_INSERT:
		OnPopupAdd();
		break;
	case 'E':	case 'e':	case '�':	case '�':
        OnPopupEdit();
		break;
	case VK_DELETE:
		OnPopupDelete();
		break;
	case 'A':	case 'a':	case '�':	case '�':
		OnPopupDeleteall();
		break;
	case VK_UP:
		SetSelectionMark( GetSelectionMark() - 1 );
		break;
	case VK_DOWN:
		SetSelectionMark( GetSelectionMark() + 1 );
		break;
	}

	*pResult = 0;
}


// Switching view

void CmyListCtrl::OnSetFocus( CWnd* pOldWnd )
{
	CListCtrl::OnSetFocus( pOldWnd );

	CHeaderCtrl * pHeader = GetHeaderCtrl();
	pHeader->ModifyStyle( 0, HDS_BUTTONS );
}

void CmyListCtrl::OnKillFocus( CWnd* pNewWnd )
{
	CListCtrl::OnKillFocus( pNewWnd );

	CHeaderCtrl * pHeader = GetHeaderCtrl();
	pHeader->ModifyStyle( HDS_BUTTONS, 0 );
}
