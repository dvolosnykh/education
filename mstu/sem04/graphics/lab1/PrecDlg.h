#pragma once

#include "myListCtrl.h"

// CPrecDlg dialog

class CPrecDlg : public CDialog
{
	DECLARE_DYNAMIC(CPrecDlg)
private:
	enum { MAXPRECISION = 6 };

public:
	UINT newprecision;

public:
	CPrecDlg( UINT precision, CWnd* pParent = NULL );
	virtual ~CPrecDlg();

// Dialog Data
	enum { IDD = IDD_PRECISION };

protected:
	virtual void DoDataExchange( CDataExchange* pDX );    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
public:
	afx_msg void OnBnClickedDefault();
};
