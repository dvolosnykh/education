#pragma once

#include "myPoint.h"


// CmySet

class CmySet
{
public:
	enum { NMAX = 100 };

public:
	CmyPoint dot[ NMAX ];
	int n;

public:
	CmySet();
	~CmySet();

	void Add( const CmyPoint & point );
	void Replace( const int index, const CmyPoint & point );
	void Delete( const int index );
	void DeleteAll();
};
