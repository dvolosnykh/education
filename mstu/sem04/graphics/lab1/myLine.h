#pragma once

#include "myPoint.h"


// CmyLine

class CmyLine
{
public:
	struct coeff_t
	{
		double A;
		double B;
		double C;
	};

	// indices of dots
	enum id_t
	{
		ONE,
		TWO
	};

public:
	CmyPoint dot[ 2 ];
	coeff_t eqn;

public:
	CmyLine();
	CmyLine( CmyPoint & p1, CmyPoint & p2 );
	~CmyLine();

	bool IsValid();
	double Length();
	bool IsVertical();
	double Inclination();
	CmyPoint IntersectWith( CmyLine & line );
	void FindCoeffs();
	CmyPoint FixPoint( const id_t ID, const double t );
};

