// lab1Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab1.h"

#include "lab1Dlg.h"
#include ".\lab1Dlg.h"
#include "myPicture.h"
#include "mySolution.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange( CDataExchange* pDX );    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog( CAboutDlg::IDD )
{
}

void CAboutDlg::DoDataExchange( CDataExchange* pDX )
{
	CDialog::DoDataExchange( pDX );
}

BEGIN_MESSAGE_MAP( CAboutDlg, CDialog )
END_MESSAGE_MAP()


// Clab1Dlg dialog

Clab1Dlg::Clab1Dlg( CWnd* pParent /*=NULL*/ )
	: CDialog( Clab1Dlg::IDD, pParent )
{
	m_hIcon = AfxGetApp()->LoadIcon( IDR_MAINFRAME );
}

void Clab1Dlg::DoDataExchange( CDataExchange* pDX )
{
	CDialog::DoDataExchange( pDX );
	DDX_Control( pDX, IDC_SETA, m_setA );
	DDX_Control( pDX, IDC_SETB, m_setB );
	DDX_Control( pDX, IDC_PICTURE, m_picture );
	DDX_Control( pDX, IDC_TOOLTIP, m_tooltip );
}

BEGIN_MESSAGE_MAP( Clab1Dlg, CDialog )
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()

	ON_BN_CLICKED( IDC_SOLVE, OnBnClickedSolve )
	ON_BN_CLICKED( IDC_RESET, OnBnClickedReset )

	ON_MESSAGE( WM_SETCHANGED, OnSetChanged )
END_MESSAGE_MAP()


static int round( double x )
{
	return ( int )( x + 0.5 );
}


// Clab1Dlg message handlers

BOOL Clab1Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT( ( IDM_ABOUTBOX & 0xFFF0 ) == IDM_ABOUTBOX );
	ASSERT( IDM_ABOUTBOX < 0xF000 );

	CMenu* pSysMenu = GetSystemMenu( FALSE );
	if ( pSysMenu != NULL )
	{
		CString strAboutMenu;
		strAboutMenu.LoadString( IDS_ABOUTBOX );
		if ( !strAboutMenu.IsEmpty() )
		{
			pSysMenu->AppendMenu( MF_SEPARATOR );
			pSysMenu->AppendMenu( MF_STRING, IDM_ABOUTBOX, strAboutMenu );
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon( m_hIcon, TRUE );		// Set big icon
	SetIcon( m_hIcon, FALSE );		// Set small icon

	// List controls
	m_setA.Initialize( this );
	m_setB.Initialize( this );
	m_picture.Initialize( m_setA.m_set, m_setB.m_set );

	// Tooltip static text
	CStatic * pTooltip = ( CStatic * )GetDlgItem( IDC_TOOLTIP );
	CString tooltip;
	tooltip.LoadString( IDS_TOOLTIP );
	pTooltip->SetWindowText( tooltip );

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Clab1Dlg::OnSysCommand( UINT nID, LPARAM lParam )
{
	if ( ( nID & 0xFFF0 ) == IDM_ABOUTBOX )
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand( nID, lParam );
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Clab1Dlg::OnPaint() 
{
	if ( IsIconic() )
	{
		CPaintDC dc( this ); // device context for painting

		SendMessage( WM_ICONERASEBKGND, reinterpret_cast<WPARAM>( dc.GetSafeHdc() ), 0 );

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics( SM_CXICON );
		int cyIcon = GetSystemMetrics( SM_CYICON );
		CRect rect;
		GetClientRect( &rect );
		int x = ( rect.Width() - cxIcon + 1 ) / 2;
		int y = ( rect.Height() - cyIcon + 1 ) / 2;

		// Draw the icon
		dc.DrawIcon( x, y, m_hIcon );
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Clab1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>( m_hIcon );
}

void Clab1Dlg::OnBnClickedSolve()
{
	// Solving problem itself
	m_picture.m_soln.Solve();

	// Converting data to suitable for drawing
	CmySolution::data_t * pMin = &( m_picture.m_soln.min );
	CmyPicture::data_t * pAns = &( m_picture.ans );

	// Copy triangles
	for ( unsigned i = CmyTriangle::ONE; i <= CmyTriangle::THREE; i++ )
	{
		pAns->tA[ i ].x = round( pMin->tA.dot[ i ].x );
		pAns->tA[ i ].y = round( pMin->tA.dot[ i ].y );
		pAns->tB[ i ].x = round( pMin->tB.dot[ i ].x );
		pAns->tB[ i ].y = round( pMin->tB.dot[ i ].y );
	}

	// Copy line
	for ( /*unsigned*/ i = CmyLine::ONE; i <= CmyLine::TWO; i++ )
	{
		pAns->l[ i ].x = round( pMin->l.dot[ i ].x );
		pAns->l[ i ].y = round( pMin->l.dot[ i ].y );
	}

	// Copy inclination angle and flags
	pAns->phi = pMin->phi;
	pAns->solved = m_picture.m_soln.solved;
	pAns->no_tA = m_picture.m_soln.no_tA;
	pAns->no_tB = m_picture.m_soln.no_tB;
	pAns->no_l = m_picture.m_soln.no_l;

	// Calling painter
	m_picture.Invalidate( false );

	// Writing answer
	CString text;
	if ( pAns->no_tA && pAns->no_tB )
		text = "������� ����������� �� ������ �� ������ ������������.";
	else if ( pAns->no_tA )
		text = "���������� A �� ������ �� ������ ������������.";
	else if ( pAns->no_tB )
		text = "���������� B �� ������ �� ������ ������������.";
	else if ( pAns->no_l )
		text = "������� ������ ��������� � �����.";
	else /* if ( pAns->solved ) */
		text.Format( "���� ������� phi = %lf", pAns->phi );

	m_tooltip.SetWindowText( text );
	MessageBox( text, "�����:" );
}

void Clab1Dlg::OnBnClickedReset()
{
	CStatic * pTooltip = ( CStatic * )GetDlgItem( IDC_TOOLTIP );
	CString tooltip;
	tooltip.LoadString( IDS_TOOLTIP );
	pTooltip->SetWindowText( tooltip );

	m_setA.DeleteAllPoints();
	m_setB.DeleteAllPoints();

	m_picture.m_soln.solved = false;
	m_picture.Invalidate( false );
}

void Clab1Dlg::ToggleBtnSolve()
{
	CButton * pSolve = ( CButton * )GetDlgItem( IDC_SOLVE );
	pSolve->EnableWindow( m_setA.GetItemCount() >= 3 && m_setB.GetItemCount() >= 3 );
}

void Clab1Dlg::ToggleBtnReset()
{
	CButton * pReset = ( CButton * )GetDlgItem( IDC_RESET );
	pReset->EnableWindow( m_setA.GetItemCount() > 0 || m_setB.GetItemCount() > 0 );
}

LRESULT Clab1Dlg::OnSetChanged( WPARAM wParam, LPARAM lParam )
{
	ToggleBtnSolve();
	ToggleBtnReset();

	return ( 0L );
}