#pragma once

#include "afxwin.h"
#include "afxcmn.h"

#include "myPicture.h"
#include "myListCtrl.h"


// Clab1Dlg dialog
class Clab1Dlg : public CDialog
{
// Construction
public:
	Clab1Dlg( CWnd* pParent = NULL );	// standard constructor

// Dialog Data
	enum { IDD = IDD_MAINWND };

protected:
	virtual void DoDataExchange( CDataExchange* pDX );	// DDX/DDV support

public:
	CmyListCtrl m_setA;
	CmyListCtrl m_setB;
	
private:
	CmyPicture m_picture;
	CStatic m_tooltip;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand( UINT nID, LPARAM lParam );
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBnClickedSolve();
	afx_msg void OnBnClickedReset();
	afx_msg LRESULT OnSetChanged( WPARAM wParam, LPARAM lParam );

private:
	void ToggleBtnSolve();
	void ToggleBtnReset();
};
