// myMenu.cpp : implementation file
//

#include "stdafx.h"
#include "lab1.h"

#include "myMenu.h"
#include "myListCtrl.h"


// CmyMenu

IMPLEMENT_DYNAMIC( CmyMenu, CMenu )

CmyMenu::CmyMenu()
{
}

CmyMenu::~CmyMenu()
{
}


void CmyMenu::Initialize( CWnd  * pNewParent )
{
	pTempParent = pNewParent;

	LoadMenu( IDR_POPUP );
	GetSubMenu( 0 )->EnableMenuItem( IDR_POPUP_ADD, MF_ENABLED );
	GetSubMenu( 0 )->EnableMenuItem( IDR_POPUP_PRECISION, MF_ENABLED );
}

void CmyMenu::ToggleItems()
{
	CmyListCtrl * pParent = ( CmyListCtrl * )pTempParent;
	const unsigned count = pParent->GetSelectedCount();

	GetSubMenu( 0 )->EnableMenuItem( IDR_POPUP_EDIT, 
		count > 0 ? MF_ENABLED : MF_GRAYED );
	GetSubMenu( 0 )->EnableMenuItem( IDR_POPUP_DELETE, 
		count > 0 ? MF_ENABLED : MF_GRAYED );
	GetSubMenu( 0 )->EnableMenuItem( IDR_POPUP_DELETEALL, 
		pParent->GetItemCount() > 0 ? MF_ENABLED : MF_GRAYED );
}

void CmyMenu::DropMenu()
{
	CPoint point;
	GetCursorPos( &point );
	GetSubMenu( 0 )->TrackPopupMenu( TPM_LEFTALIGN, point.x, point.y, pTempParent );
}
