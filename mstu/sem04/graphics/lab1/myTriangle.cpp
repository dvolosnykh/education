#include "stdafx.h"

#include "math.h"

#include "myTriangle.h"
#include "myLine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CmyTriangle

CmyTriangle::CmyTriangle()
{
}

CmyTriangle::CmyTriangle( CmyPoint & p1, CmyPoint & p2, CmyPoint & p3 )
{
	SetPoints( p1, p2, p3 );
}

CmyTriangle::~CmyTriangle()
{
}


void CmyTriangle::SetPoints( CmyPoint & p1, CmyPoint & p2, CmyPoint & p3 )
{
	dot[ ONE ] = p1;
	dot[ TWO ] = p2;
	dot[ THREE ] = p3;
}


bool CmyTriangle::IsValid()
{
	return ( Area() > 0 );
}

CmyPoint CmyTriangle::BisectorsIntersection()
{
	CmyLine bisector1 = Bisector( ONE );
	CmyLine bisector2 = Bisector( TWO );

	CmyPoint I = bisector1.IntersectWith( bisector2 );

	return ( I );
}

CmyLine CmyTriangle::Bisector( const id_t ID )
{
	CmyPoint & dot1 = dot[ ID == ONE ? TWO : ID == TWO ? THREE : ONE ];
	CmyPoint & dot2 = dot[ ID == ONE ? THREE : ID == TWO ? ONE : TWO ];
	CmyPoint & dot3 = dot[ ID ];

	CmyLine side1( dot3, dot1 );
	CmyLine side2( dot3, dot2 );
	CmyLine side3( dot1, dot2 );

	const double k = side1.Length() / side2.Length();
	CmyPoint fixpoint = side3.FixPoint( CmyLine::ONE, k / ( k + 1 ) );
	CmyLine bisector( dot3, fixpoint );

	return ( bisector );
}

double CmyTriangle::Area()
{
	CmyPoint & dot1 = dot[ ONE ];
	CmyPoint & dot2 = dot[ TWO ];
	CmyPoint & dot3 = dot[ THREE ];

	return fabs( dot1.x * dot2.y + dot2.x * dot3.y + dot3.x * dot1.y 
		- dot1.y * dot2.x - dot2.y * dot3.x - dot3.y * dot1.x ) / 2;
}