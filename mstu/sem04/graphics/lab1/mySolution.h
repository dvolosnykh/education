#pragma once

#include "mySet.h"
#include "myLine.h"
#include "myTriangle.h"


// CmySolution

class CmySolution
{
public:
	struct data_t
	{
		CmyTriangle tA;  
		CmyTriangle tB;
		CmyLine l;
		double phi;
	};

public:
	data_t min;
	bool solved;
	bool no_tA;
	bool no_tB;
	bool no_l;

private:
	data_t temp;
	CmySet * pSetA;
	CmySet * pSetB;

public:
	CmySolution();
	~CmySolution();

	void Initialize( CmySet & setA, CmySet & setB );
	void Solve();

private:
	void TryTriangleB();
	void Evaluate();
	void Save();
};