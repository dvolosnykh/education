#include "stdafx.h"
#include "lab1.h"

#include "mySet.h"
#include "myPicture.h"
#include "mySolution.h"


// CmyPicture

IMPLEMENT_DYNAMIC( CmyPicture, CStatic )

CmyPicture::CmyPicture()
{
}

CmyPicture::~CmyPicture()
{
}


BEGIN_MESSAGE_MAP( CmyPicture, CStatic )
	ON_WM_PAINT()
END_MESSAGE_MAP()


int yminlimit( CPoint * polyline, const unsigned & n )
{
	int ymin = polyline[ 0 ].y;
	
	for ( unsigned i = 1; i < n; i++ )
		ymin = min ( polyline[ i ].y, ymin );

	return ( ymin );
}

int xminlimit( CPoint * polyline, const unsigned & n )
{
	int xmin = polyline[ 0 ].x;
	
	for ( unsigned i = 1; i < n; i++ )
		xmin = min( polyline[ i ].x, xmin );

	return ( xmin );
}

int ymaxlimit( CPoint * polyline, unsigned n )
{
	int ymax = polyline[ 0 ].y;
	
	for ( unsigned i = 1; i < n; i++ )
		ymax = max( polyline[ i ].y, ymax );

	return ( ymax );
}

int xmaxlimit( CPoint * polyline, unsigned n )
{
	int xmax = polyline[ 0 ].x;
	
	for ( unsigned i = 1; i < n; i++ )
		xmax = max( polyline[ i ].x, xmax );

	return ( xmax );
}

CRect getlimits( CPoint * polyline, unsigned n )
{
	CRect zoom;

	zoom.bottom = ymaxlimit( polyline, n );
	zoom.top = yminlimit( polyline, n );
	zoom.right = xmaxlimit( polyline, n );
	zoom.left = xminlimit( polyline, n );

	return ( zoom );
}

void CmyPicture::GetAnsLimits()
{
	CRect rect_tA( getlimits( ans.tA, 3 ) );
	CRect rect_tB( getlimits( ans.tB, 3 ) );

	m_LPrect.UnionRect( &rect_tA, &rect_tB );

	if ( m_LPrect.Height() > m_LPrect.Width() )
	{
		m_LPrect.left -= ( m_LPrect.Height() - m_LPrect.Width() ) / 2;
		m_LPrect.right = m_LPrect.left + m_LPrect.Height();
	}
	else
	{
		m_LPrect.top -= ( m_LPrect.Width() - m_LPrect.Height() ) / 2;
		m_LPrect.bottom = m_LPrect.top + m_LPrect.Width();
	}

	int marginX = ( int )( 0.1 * m_LPrect.Width() );
	int marginY = ( int )( 0.1 * m_LPrect.Height() );
	m_LPrect.DeflateRect( - marginX, - marginY );
}

void CmyPicture::Initialize( CmySet & setA, CmySet & setB )
{
	pSetA = &setA;
	pSetB = &setB;

	m_soln.Initialize( setA, setB );
}

void CmyPicture::Clear( CPaintDC * pDC )
{
	// Selecting GDI objects
	CGdiObject * pOldBrush = pDC->SelectStockObject( WHITE_BRUSH );
	CGdiObject * pOldPen = pDC->SelectStockObject( BLACK_PEN );

	// Clearing
	pDC->Rectangle( m_DPrect );

	// Deselecting GDI objects
	pDC->SelectObject( pOldBrush );
	pDC->SelectObject( pOldPen );
}

void CmyPicture::PrepareCoordSystem( CPaintDC * pDC )
{
	pDC->SetMapMode( MM_ISOTROPIC );

	GetAnsLimits();

	// Order of below functions has no meaning...
	pDC->SetWindowExt( m_LPrect.Width(), m_LPrect.Height() );
	pDC->SetWindowOrg( m_LPrect.left, m_LPrect.top );
		
	pDC->SetViewportExt( m_DPrect.right, - m_DPrect.bottom );
	pDC->SetViewportOrg( 0, m_DPrect.bottom );
}

void CmyPicture::Drawing( CPaintDC * pDC )
{
	PrepareCoordSystem( pDC );

	// Extending line
	ExtendLine();
	
	// Defining drawing pens
	CPen trPen( PS_SOLID, 2, RGB( 0, 0, 255 ) );
	CPen lPen( PS_SOLID, 2, RGB( 255, 0, 0 ) );

	// Drawing
	CBrush * pOldBrush = ( CBrush * )pDC->SelectStockObject( NULL_BRUSH );
	CPen * pOldPen = ( CPen * )pDC->SelectObject( &trPen );
	pDC->Polygon( ans.tA, 3 );
	pDC->Polygon( ans.tB, 3 );

	pDC->SelectObject( &lPen );
	pDC->Polyline( ans.l, 2 );

	// Deselecting GDI objects
	pDC->SelectObject( pOldBrush );
	pDC->SelectObject( pOldPen );
}

static int round( double x )
{
	return ( int )( x + 0.5 );
}

void CmyPicture::ExtendLine()
{
	m_LPrect.DeflateRect( 1, 1 );

	m_soln.min.l.FindCoeffs();
	double & A = m_soln.min.l.eqn.A;
	double & B = m_soln.min.l.eqn.B;
	double & C = m_soln.min.l.eqn.C;

	if ( A == 0 )
	{
		ans.l[ CmyLine::ONE ].y = m_LPrect.left;
		ans.l[ CmyLine::TWO ].y = m_LPrect.right;
	}
	else if ( B == 0 )
	{
		ans.l[ CmyLine::ONE ].x = m_LPrect.top;
		ans.l[ CmyLine::TWO ].x = m_LPrect.bottom;
	}
	else
	{
		ans.l[ CmyLine::ONE ].x = round( - ( B * m_LPrect.bottom + C ) / A );
		if ( ans.l[ CmyLine::ONE ].x < m_LPrect.left )
		{
			ans.l[ CmyLine::ONE ].x = m_LPrect.left;
			ans.l[ CmyLine::ONE ].y = round( - ( A * m_LPrect.left + C ) / B );
		}
		else if ( ans.l[ CmyLine::ONE ].x > m_LPrect.right )
		{
			ans.l[ CmyLine::ONE ].x = m_LPrect.right;
			ans.l[ CmyLine::ONE ].y = round( - ( A * m_LPrect.right + C ) / B );
		}
		else
			ans.l[ CmyLine::ONE ].y = m_LPrect.bottom;

		ans.l[ CmyLine::TWO ].x = round( - ( B * m_LPrect.top + C ) / A );
		if ( ans.l[ CmyLine::TWO ].x < m_LPrect.left )
		{
			ans.l[ CmyLine::TWO ].x = m_LPrect.left;
			ans.l[ CmyLine::TWO ].y = round( - ( A * m_LPrect.left + C ) / B );
		}
		else if ( ans.l[ CmyLine::TWO ].x > m_LPrect.right )
		{
			ans.l[ CmyLine::TWO ].x = m_LPrect.right;
			ans.l[ CmyLine::TWO ].y = round( - ( A * m_LPrect.right + C ) / B );
		}
		else
			ans.l[ CmyLine::TWO ].y = m_LPrect.top;
	}

	m_LPrect.DeflateRect( -1, -1 );
}

// CmyPicture message handlers

void CmyPicture::OnPaint()
{
	CPaintDC dc( this );
	Clear( &dc );

	if ( m_soln.solved || m_soln.no_l && !m_soln.no_tA && !m_soln.no_tB )
		Drawing( &dc );		

	// Do not call CStatic::OnPaint() for painting messages
}

void CmyPicture::PreSubclassWindow()
{
	GetClientRect( m_DPrect );

	CStatic::PreSubclassWindow();
}
