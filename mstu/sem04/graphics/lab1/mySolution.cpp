#include "stdafx.h"

#include "mySet.h"
#include "myLine.h"
#include "myTriangle.h"
#include "mySolution.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CmySolution

CmySolution::CmySolution()
{
	solved = false;
}

CmySolution::~CmySolution()
{
}


void CmySolution::Initialize( CmySet & setA, CmySet & setB )
{
	pSetA = &setA;
	pSetB = &setB;
}

void CmySolution::Solve()
{
	solved = false;
	no_tA = no_tB = no_l = true;

	for ( int i = 0; i < pSetA->n - 2; i++ )
		for ( int j = i + 1; j < pSetA->n - 1; j++ )
			for ( int k = j + 1; k < pSetA->n; k++ )
			{
				temp.tA.SetPoints( pSetA->dot[ i ], pSetA->dot[ j ], pSetA->dot[ k ] );
				no_tA = !temp.tA.IsValid();
				TryTriangleB();
			}
}

void CmySolution::TryTriangleB()
{
	for ( int i = 0; i < pSetB->n - 2; i++ )
		for ( int j = i + 1; j < pSetB->n - 1; j++ )
			for ( int k = j + 1; k < pSetB->n; k++ )
			{
				temp.tB.SetPoints( pSetB->dot[ i ], pSetB->dot[ j ], pSetB->dot[ k ] );
				no_tB = !temp.tB.IsValid();

				if ( !no_tA && !no_tB )
					Evaluate();
			}
}

void CmySolution::Evaluate()
{
	temp.l.dot[ CmyLine::ONE ] = temp.tA.BisectorsIntersection();
	temp.l.dot[ CmyLine::TWO ] = temp.tB.BisectorsIntersection();
	
	bool valid = temp.l.IsValid();
	bool vertical = temp.l.IsVertical();
	
	if ( valid )
		no_l = false;

	if ( !vertical )
		temp.phi = temp.l.Inclination();

	if ( !solved || valid && ( !min.l.IsValid() || !vertical && 
		( min.l.IsVertical() || temp.phi < min.phi ) ) )
	{
		Save();
		solved = true;
	}
}

void CmySolution::Save()
{
	min = temp;
}
