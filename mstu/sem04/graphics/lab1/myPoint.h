#pragma once


// CmyPoint

class CmyPoint
{
public:
	double x;
	double y;

public:
	CmyPoint();
	CmyPoint( double initx, double inity );
	~CmyPoint();

	void SetPoint( double setx, double sety );
	CmyPoint operator = ( CmyPoint point );
};