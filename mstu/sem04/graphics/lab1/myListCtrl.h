#pragma once

#include "afxwin.h"
#include "afxcmn.h"

#include "myPoint.h"
#include "mySet.h"
#include "myMenu.h"


#define WM_SETCHANGED	WM_USER + 5


// CmyListCtrl

class CmyListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC( CmyListCtrl )
public:
	enum { DEFAULTPRECISION = 3 };

public:
	CmySet m_set;
	CmyMenu m_pmenu;
	unsigned precision;

private:
	CWnd * pParent;

public:
	CmyListCtrl();
	virtual ~CmyListCtrl();

	void Initialize( CWnd * pParent );
	void AddPoint( CmyPoint & newpoint );
	void ReplacePoint( const int & index, CmyPoint & newpoint );
	void DeletePoint( const int & index );
	void DeleteAllPoints();
	
	
private:
	void PutCoordinates( const int & index, CmyPoint & point );
	void Renumerate( const int & first );
	int GetFirstSelectedIndex();

protected:
	DECLARE_MESSAGE_MAP()

public:
	// Working with menu
	afx_msg void OnPopupAdd();
	afx_msg void OnPopupEdit();
	afx_msg void OnPopupDelete();
	afx_msg void OnPopupDeleteall();
	afx_msg void OnPopupPrecision();
	afx_msg void OnContextMenu( CWnd* /*pWnd*/, CPoint /*point*/ );

	// Hotkeys
	afx_msg void OnLvnKeydown( NMHDR *pNMHDR, LRESULT *pResult );

	// Switching view
	afx_msg void OnSetFocus( CWnd* pOldWnd );
	afx_msg void OnKillFocus( CWnd* pNewWnd );
};
