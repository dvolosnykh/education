#pragma once

#include "mySet.h"
#include "mySolution.h"


// CmyPicture

class CmyPicture : public CStatic
{
	DECLARE_DYNAMIC( CmyPicture )

public:
	struct data_t
	{
		CPoint tA[ 3 ];
		CPoint tB[ 3 ];
		CPoint l[ 2 ];
		double phi;
		bool solved;
		bool no_tA;
		bool no_tB;
		bool no_l;
	};

public:
	CmySet * pSetA;
	CmySet * pSetB;
	CmySolution m_soln;
	data_t ans;

private:
	CRect m_DPrect;
	CRect m_LPrect;

public:
	CmyPicture();
	virtual ~CmyPicture();

	void Initialize( CmySet & setA, CmySet & setB );
	void GetAnsLimits();

private:
	void Clear( CPaintDC * pDC );
	void PrepareCoordSystem( CPaintDC * pDC );
	void Drawing( CPaintDC * pDC );
	void ExtendLine();

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnPaint();
protected:
	virtual void PreSubclassWindow();
};
