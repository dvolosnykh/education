#include "stdafx.h"

#include "myPoint.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CmyPoint

CmyPoint::CmyPoint()
{
}

CmyPoint::CmyPoint( double initx, double inity )
{
	SetPoint( initx, inity );
}

CmyPoint::~CmyPoint()
{
}

void CmyPoint::SetPoint( double setx, double sety )
{
	x = setx;
	y = sety;
}

CmyPoint CmyPoint::operator = ( CmyPoint point )
{
	SetPoint( point.x, point.y );

	return ( point );
}
