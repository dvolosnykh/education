// PrecDlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab1.h"

#include "PrecDlg.h"
#include ".\PrecDlg.h"
#include "myListCtrl.h"


// CPrecDlg dialog

IMPLEMENT_DYNAMIC( CPrecDlg, CDialog )

CPrecDlg::CPrecDlg( UINT precision, CWnd* pParent /*=NULL*/ )
	: CDialog( CPrecDlg::IDD, pParent )
	, newprecision( precision )
{
}

CPrecDlg::~CPrecDlg()
{
}

void CPrecDlg::DoDataExchange( CDataExchange* pDX )
{
	CDialog::DoDataExchange( pDX );
	DDX_Text( pDX, IDC_PRECISION, newprecision );
}


BEGIN_MESSAGE_MAP( CPrecDlg, CDialog )
	ON_BN_CLICKED(IDC_DEFAULT, OnBnClickedDefault)
END_MESSAGE_MAP()


// CPrecDlg message handlers

BOOL CPrecDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	( ( CSpinButtonCtrl * )GetDlgItem( IDC_SPIN ) )->SetRange( 0, MAXPRECISION );

	UpdateData( false );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPrecDlg::OnOK()
{
	UpdateData( true );

	CDialog::OnOK();
}

void CPrecDlg::OnBnClickedDefault()
{
	newprecision = CmyListCtrl::DEFAULTPRECISION;
	UpdateData( false );
}
