#pragma once

#include "myPoint.h"
#include "myLine.h"


// CmyTriangle

class CmyTriangle
{
public:
	// indices of dots
	enum id_t
	{
		ONE,
		TWO,
		THREE
	};

public:
	CmyPoint dot[ 3 ];

public:
	CmyTriangle();
	CmyTriangle( CmyPoint & p1, CmyPoint & p2, CmyPoint & p3 );
	~CmyTriangle();

	void SetPoints( CmyPoint & p1, CmyPoint & p2, CmyPoint & p3 );
	bool IsValid();
	CmyPoint BisectorsIntersection();	
	CmyLine Bisector( const id_t ID );
	double Area();
};