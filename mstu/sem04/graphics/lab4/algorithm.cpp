#include "stdafx.h"
#include <math.h>

#include "algorithm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


bool AlgoCDA( CDC * pDC, const line_t & line, COLORREF color,
			 unsigned & steps )
{
	const int deltax = line.dot2.x - line.dot1.x;
	const int deltay = line.dot2.y - line.dot1.y;
	const int L = max( abs( deltax ), abs( deltay ) );
	const double dx = deltax / ( double )L;
	const double dy = deltay / ( double )L;

	CPoint dot( line.dot1 );
	steps = 0;
	double x = dot.x;
	double y = dot.y;
	
	for ( int i = 0; ; )
	{
		pDC->SetPixel( dot, color );

	if ( ++i > L ) break;

		CPoint prev( dot );
		dot.SetPoint( round( x += dx ), round( y += dy ) );

		if ( dot.x != prev.x && dot.y != prev.y )
			steps++;
	}

	return ( dot.x == line.dot2.x && dot.y == line.dot2.y );
}

static int sign( const double & x )
{
	return ( x < 0 ? -1 : x > 0 ? +1 : 0 );
}

static void swap( int & a, int & b )
{
	int temp = a;
	a = b;
	b = temp;
}

bool AlgoBrezFloat( CDC * pDC, const line_t & line, COLORREF color,
				   unsigned & steps )
{
	// preparation
	int dx = line.dot2.x - line.dot1.x;
	int dy = line.dot2.y - line.dot1.y;
	int signx = sign( dx );
	int signy = sign( dy );
	const int L = max( dx = abs( dx ), dy = abs( dy ) );
	
	CPoint dot( line.dot1 );
	long * px;
	long * py;
	
	bool usual = dx > dy;
	if ( usual )
	{
		px = &dot.x;
		py = &dot.y;
	}
	else
	{
		px = &dot.y;
		py = &dot.x;

		swap( dx, dy );
		swap( signx, signy );
	}

	float m = dy / ( float )( dx );
	float e = m - 0.5;
	steps = 0;

	// algorithm itself
	for ( int i = 0; ; )
	{
		pDC->SetPixel( dot, color );

	if ( ++i > L ) break;

		// should we make step along virtual vertical axis?
		if ( e >= 0 )
		{
			*py += signy;
			e--;
			steps++;
		}
		
		// making step along virtual horizontal axis
		*px += signx;
		e += m;
	}

	return ( dot.x == line.dot2.x && dot.y == line.dot2.y );
}

bool AlgoBrezInt( CDC * pDC, const line_t & line, COLORREF color,
				 unsigned & steps )
{
	// preparation
	int dx = line.dot2.x - line.dot1.x;
	int dy = line.dot2.y - line.dot1.y;
	int signx = sign( dx );
	int signy = sign( dy );
	const int L = max( dx = abs( dx ), dy = abs( dy ) );
	
	CPoint dot( line.dot1 );
	long * px;
	long * py;
	
	bool usual = dx > dy;
	if ( usual )
	{
		px = &dot.x;
		py = &dot.y;
	}
	else
	{
		px = &dot.y;
		py = &dot.x;

		swap( dx, dy );
		swap( signx, signy );
	}

	int e = ( dy *= 2 ) - dx;
	dx *= 2;
	steps = 0;

	// algorithm itself
	for ( int i = 0; ; )
	{
		pDC->SetPixel( dot, color );

	if ( ++i > L ) break;

		// should we make step along virtual vertical axis?
		if ( e >= 0 )
		{
			*py += signy;
			e -= dx;
			steps++;
		}
		
		// making step along virtual horizontal axis
		*px += signx;
		e += dy;
	}

	return ( dot.x == line.dot2.x && dot.y == line.dot2.y );
}

bool AlgoBrezNostep( CDC * pDC, const line_t & line, COLORREF color,
					unsigned & steps )
{
	// preparation
	int dx = line.dot2.x - line.dot1.x;
	int dy = line.dot2.y - line.dot1.y;
	int signx = sign( dx );
	int signy = sign( dy );
	const int L = max( dx = abs( dx ), dy = abs( dy ) );
	
	CPoint dot( line.dot1 );
	long * px;
	long * py;
	
	bool usual = dx > dy;
	if ( usual )
	{
		px = &dot.x;
		py = &dot.y;
	}
	else
	{
		px = &dot.y;
		py = &dot.x;

		swap( dx, dy );
		swap( signx, signy );
	}

	const double m = dy / ( double )( dx );
	const double w = 1 - m;
	double e = 0.5;
	steps = 0;

	// algorithm itself
	for ( int i = 0; ; )
	{
		BYTE red   = round( 255 - e * ( 255 - GetRValue( color ) ) );
		BYTE green = round( 255 - e * ( 255 - GetGValue( color ) ) );
		BYTE blue  = round( 255 - e * ( 255 - GetBValue( color ) ) );
		
		pDC->SetPixel( dot, RGB( red, green, blue ) );

	if ( ++i > L ) break;
		
		// should we make step along virtual vertical axis?
		if ( e >= w )
		{
			*py += signy;
			e--;
			steps++;
		}

		// making step along virtual horizontal axis
		*px += signx;
		e += m;
	}

	return ( dot.x == line.dot2.x && dot.y == line.dot2.y );
}

bool AlgoStandard( CDC * pDC, const line_t & line, COLORREF color,
				  unsigned & steps )
{
	CPen myPen( PS_SOLID, 1, color );
	CGdiObject * pOldPen = pDC->SelectObject( &myPen );

	pDC->MoveTo( line.dot1 );
	pDC->LineTo( line.dot2 );

	pDC->SelectObject( pOldPen );

	return ( true );
}

pAlgo ChooseAlgo( const int nAlgorithm )
{
	pAlgo choosed;
	
	switch ( nAlgorithm )
	{
	case ALGO_CDA:
		choosed = AlgoCDA;
		break;
	case ALGO_BREZ_FLOAT:
		choosed = AlgoBrezFloat;
		break;
	case ALGO_BREZ_INT:
		choosed = AlgoBrezInt;
		break;
	case ALGO_BREZ_NOSTEP:
		choosed = AlgoBrezNostep;
		break;
	case ALGO_STANDARD:
		choosed = AlgoStandard;
		break;
	}

	return ( choosed );
}

void Mark( CDC * pDC, CPoint & dot )
{
	CPen marker( PS_SOLID, 1, RGB( 255, 0, 0 ) );
	CGdiObject * pOldPen = pDC->SelectObject( &marker );

	pDC->Ellipse( dot.x - 2, dot.y - 2, dot.x + 3, dot.y + 3 );

	pDC->SelectObject( pOldPen );
}

long round( double x )
{
	return ( long )( x + 0.5 );
}

CPoint Rotate( const CPoint & dot, const CPoint & c, const double phi )
{
	double Cos = cos( phi );
	double Sin = sin( phi );
	CPoint newdot( dot );

	newdot.x = round( c.x + ( dot.x - c.x ) * Cos + ( dot.y - c.y ) * Sin );
	newdot.y = round( c.y - ( dot.x - c.x ) * Sin + ( dot.y - c.y ) * Cos );

	return ( newdot );
}