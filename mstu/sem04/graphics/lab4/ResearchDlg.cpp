// ResearchDlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab4.h"
#include "ResearchDlg.h"
#include ".\researchdlg.h"
#include "myPicture.h"
#include ".\mypicture.h"
#include "algorithm.h"


#define TIMES	200


static const COLORREF color[ ALGO_STANDARD - ALGO_CDA + 1 ] =
{
	RGB( 255, 160, 160 ),
	RGB( 160, 255, 160 ),
	RGB( 160, 160, 255 ),
	RGB( 255, 255, 160 ),
	RGB( 255, 160, 255 )
};

// CResearchDlg dialog

IMPLEMENT_DYNAMIC(CResearchDlg, CDialog)
CResearchDlg::CResearchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CResearchDlg::IDD, pParent)
{
}

CResearchDlg::~CResearchDlg()
{
}

void CResearchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRAPH, m_graph);
	DDX_Control(pDX, IDC_ALGO_COLOR, m_algocolor);
}


BEGIN_MESSAGE_MAP(CResearchDlg, CDialog)
END_MESSAGE_MAP()


// CResearchDlg message handlers

BOOL CResearchDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ShowColor( m_algocolor.pmf );
	Draw( m_graph.pmf );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CResearchDlg::ShowColor( CDC * pDC )
{
	// preparing set of colors for each of algorithms
	COLORREF color[ ALGO_STANDARD - ALGO_CDA + 1 ] =
	{
		RGB( 255,  64,  64 ),
		RGB(  64, 255,  64 ),
		RGB(  64,  64, 255 ),
		RGB( 255, 255,  64 ),
		RGB( 255,  64, 255 )
	};

	// saving old GDI object
	CGdiObject * pOldBrush = pDC->SelectStockObject( NULL_BRUSH );

	// drawing markers
	for ( int i = ALGO_CDA, y = -5; i <= ALGO_STANDARD; i++ )
	{
		CPen pencil( PS_SOLID, 3, color[ i ] );
		pDC->SelectObject( &pencil );
		pDC->MoveTo( 10, y += 13 );
		pDC->LineTo( 90, y );
	}

	// restoring old GDI object
	pDC->SelectObject( pOldBrush );
}

DWORD findmax( DWORD * x, const unsigned n )
{
	DWORD maximum = *x;

	for ( unsigned i = 1; i < n; i++ )
		if ( x[ i ] > maximum )
			maximum = x[ i ];

	return ( maximum );
}

unsigned findmax( unsigned * x, const unsigned n )
{
	unsigned maximum = *x;

	for ( unsigned i = 1; i < n; i++ )
		if ( x[ i ] > maximum )
			maximum = x[ i ];

	return ( maximum );
}

void CResearchDlg::CoordSystem( CDC * pDC )
{
	int width = m_graph.m_DPrect.Width();
	int height = m_graph.m_DPrect.Height();

	pDC->SetMapMode( MM_ISOTROPIC );
	pDC->SetViewportExt( width, - height );
	pDC->SetViewportOrg( 0, height );

	const int size = 1000;
	const int blank_l = 200;
	const int blank_b = 100;
	const int blank_r = 200;
	const int blank_t = 150;
	
	width = size * width / height;
	height = size;

	pDC->SetWindowExt( width, height );
	pDC->SetWindowOrg( - blank_l, - blank_b );

	const int max_x = width - blank_l - blank_r;
	const int max_y = height - blank_b - blank_t;

	// axises
	pDC->MoveTo( 0, max_y );
	pDC->LineTo( 0, 0 );	
	pDC->LineTo( max_x, 0 );
	pDC->TextOut( -180, max_y + 90, "���������" );
	pDC->TextOut( max_x + 20, 40, "����" );

	// filling arrows
	CBrush fill( RGB( 192, 192, 192 ) );
	CGdiObject * pOldBrush = pDC->SelectObject( &fill );

	// drawing arrows
	CPoint triangle[ 3 ];
	triangle[ 0 ].SetPoint( 0, max_y );
	triangle[ 1 ].SetPoint( -10, max_y - 50 );
	triangle[ 2 ].SetPoint( +20, max_y - 50 );
	pDC->Polygon( triangle, 3 );

	triangle[ 0 ].SetPoint( max_x, 0 );
	triangle[ 1 ].SetPoint( max_x - 50, -20 );
	triangle[ 2 ].SetPoint( max_x - 50, +20 );
	pDC->Polygon( triangle, 3 );

	// deselectin my GDI object
	pDC->SelectObject( pOldBrush );
}

void CResearchDlg::DrawTimes( CDC * pDC, DWORD * time )
{
	const unsigned highest = 600;
	const unsigned w = 300;
	const unsigned offset = 50;

	const DWORD maxtime = findmax( time, ALGO_STANDARD - ALGO_CDA + 1 );

	// saving old GDI objects
	CGdiObject * pOldPen = pDC->SelectStockObject( NULL_PEN );
	CGdiObject * pOldBrush = pDC->SelectStockObject( NULL_BRUSH );

	for ( unsigned i = ALGO_CDA, x = 0; i <= ALGO_STANDARD; i++ )
	{
		CRect rect( 0, 0, 0, 0 );
		rect.right  = x += offset;
		rect.left   = x += w;
		rect.top    = 0;
		rect.bottom = highest * time[ i ] / maxtime;

		CPen pencil( PS_SOLID, 1, color[ i ] );
		CBrush brush( color[ i ] );
		pDC->SelectObject( &pencil );
		pDC->SelectObject( &brush );
		pDC->Rectangle( rect );

		CString timestr( "" );
		timestr.Format( "%4u", time[ i ] );
		pDC->TextOut( rect.right + 60, rect.bottom + 100, timestr );
	}

	// deselecting my GDI objects
	pDC->SelectObject( pOldPen );
	pDC->SelectObject( pOldBrush );
}

void CResearchDlg::DrawSteps( CDC * pDC, unsigned * steps )
{
	const unsigned highest = 600;

	const unsigned maxsteps = findmax( steps, 46 );

	// printing someting like scales
	CString numstr( "0" );
	pDC->TextOut( -40, -5, numstr );
	numstr.Format( "%4u", maxsteps );
	if ( maxsteps > 0 )
		pDC->TextOut( -190, highest + 40, numstr );
	numstr = "45";
	pDC->TextOut( 1700, -5, numstr );

	// saving old GDI object
	CGdiObject * pOldPen = pDC->SelectStockObject( NULL_PEN );

	CPen pencil( PS_SOLID, 7, RGB( 0, 0, 0 ) );
	pDC->SelectObject( &pencil );
	const int dx = 1750 / 45;
	pDC->MoveTo( 0, highest * steps[ 0 ] / maxsteps );
	for ( unsigned i = 1, x = dx; i <= 45; i++, x += dx )
		pDC->LineTo( x, highest * steps[ i ] / maxsteps );

	// deselecting my GDI objects
	pDC->SelectObject( pOldPen );
}

void CResearchDlg::Draw( CMetaFileDC * & pDC )
{
	// === EVALUATE ===
	unsigned steps[ 46 ];

	// evaluating time charachteristics
	line_t line;
	line.dot1.SetPoint( 0, 0 );
	line.dot2.SetPoint( 314, 143 );

	DWORD time[ ALGO_STANDARD - ALGO_CDA + 1 ];

	for ( int i = ALGO_CDA; i <= ALGO_STANDARD; i++ )
	{
		pAlgo Algo = ChooseAlgo( i );

		DWORD start = GetTickCount();

		for ( unsigned j = 0; j < TIMES; j++ )
			Algo( pDC, line, RGB( 0, 0, 0 ), steps[ i ] );

		time[ i ] = GetTickCount() - start;
	}

	// evaluating step-property
	line.dot2.SetPoint( 1000, 0 );
	CPoint savedot( line.dot2 );

	for ( int i = 0; ; )
	{
		AlgoCDA( pDC, line, RGB( 0, 0, 0 ), steps[ i ] );

	if ( ++i > 45 ) break;

		line.dot2 = Rotate( savedot, line.dot1, - i * M_PI / 180 );
	}

	// === DRAW ===
	m_graph.Clear();

	CoordSystem( pDC );
	DrawTimes( pDC, time );
	DrawSteps( pDC, steps );
}
