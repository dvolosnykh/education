#pragma once

#define M_PI	3.14159265358979323846


enum
{
	ALGO_CDA,
	ALGO_BREZ_FLOAT,
	ALGO_BREZ_INT,
	ALGO_BREZ_NOSTEP,
	ALGO_STANDARD
};


struct line_t
{
	CPoint dot1;
	CPoint dot2;
};


typedef bool ( *pAlgo )( CDC * pDC, const line_t & line, COLORREF color,
						unsigned & steps );

bool AlgoCDA( CDC * pDC, const line_t & line, COLORREF color,
			 unsigned & steps );
bool AlgoBrezFloat( CDC * pDC, const line_t & line, COLORREF color,
				   unsigned & steps );
bool AlgoBrezInt( CDC * pDC, const line_t & line, COLORREF color,
				 unsigned & steps );
bool AlgoBrezNostep( CDC * pDC, const line_t & line, COLORREF color,
					unsigned & steps );
bool AlgoStandard( CDC * pDC, const line_t & line, COLORREF color,
				  unsigned & steps );

pAlgo ChooseAlgo( const int nAlgorithm );

void Mark( CDC * pDC, CPoint & dot );
long round( double x );
CPoint Rotate( const CPoint & dot, const CPoint & c, const double phi );