#include "stdafx.h"
#include "lab4.h"

#include "myPicture.h"
#include ".\mypicture.h"


// My functions for metafiles

CMetaFileDC * NewMF()
{
	CMetaFileDC * pmf = new CMetaFileDC();
	pmf->Create();

	return ( pmf );
}

CMetaFileDC * NewMF( CMetaFileDC * & src )
{
	CMetaFileDC * pmf = NewMF();
	PlayMF( pmf, src );

	return ( pmf );
}

void DeleteMF( CMetaFileDC * pmf, HMETAFILE hmf )
{
	DeleteMetaFile( hmf );
	delete pmf;
}

void ReloadMF( CMetaFileDC * & pmf, HMETAFILE hmf )
{
	CMetaFileDC * pmfnew = NewMF();
	pmfnew->PlayMetaFile( hmf );
	DeleteMF( pmf, hmf );
	pmf = pmfnew;
}

void PlayMF( CDC * pDC, CMetaFileDC * & pmf )
{
	HMETAFILE hmf = pmf->Close();
	pDC->PlayMetaFile( hmf );
	ReloadMF( pmf, hmf );
}

void ResetMF( CMetaFileDC * & pmf )
{
	HMETAFILE hmf = pmf->Close();
	DeleteMetaFile( hmf );
	pmf->Create();
}


// CmyPicture

IMPLEMENT_DYNAMIC( CmyPicture, CStatic )


BEGIN_MESSAGE_MAP( CmyPicture, CStatic )
	ON_WM_PAINT()
END_MESSAGE_MAP()


CmyPicture::CmyPicture()
{
	pmf = NewMF();
}

CmyPicture::~CmyPicture()
{
	HMETAFILE hmf = pmf->Close();
	DeleteMF( pmf, hmf );
}


void CmyPicture::Clear()
{
	// Selecting GDI objects
	CGdiObject * pOldBrush = pmf->SelectStockObject( WHITE_BRUSH );
	CGdiObject * pOldPen = pmf->SelectStockObject( BLACK_PEN );

	// Clearing
	ResetMF( pmf );
	pmf->Rectangle( m_DPrect );

	// Deselecting GDI objects
	pmf->SelectObject( pOldBrush );
	pmf->SelectObject( pOldPen );
}

// CmyPicture message handlers

void CmyPicture::PreSubclassWindow()
{
	GetClientRect( m_DPrect );

	CStatic::PreSubclassWindow();
}

void CmyPicture::OnPaint()
{
	CPaintDC dc( this );
	PlayMF( &dc, pmf );
}
