// lab4Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab4.h"
#include "lab4Dlg.h"
#include ".\lab4dlg.h"
#include "myPicture.h"
#include <math.h>
#include "ResearchDlg.h"
#include ".\researchdlg.h"
#include "algorithm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define MAXRAYS	100
#define R		3

static bool firstdot = true;
static CMetaFileDC * pmfsave;


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// Clab4Dlg dialog

Clab4Dlg::Clab4Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(Clab4Dlg::IDD, pParent),
	m_algorithm( ALGO_CDA ),
	m_color( RGB( 0, 0, 0 ) )
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

Clab4Dlg::~Clab4Dlg()
{
}

void Clab4Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PICTURE, m_picture);
	DDX_Control(pDX, IDC_MOUSEPOS, m_mousepos);
	DDX_Control(pDX, IDC_COLOR, m_drawcolor);
	DDX_Radio(pDX, IDC_CDA, m_algorithm);
	DDX_Control(pDX, IDC_DRAWSTYLE, m_drawstyle);
	
	DDX_Control(pDX, IDC_ST_DOT2, m_st_dot1);
	DDX_Control(pDX, IDC_EDIT_X1, m_x1);
	DDX_Control(pDX, IDC_EDIT_Y1, m_y1);
	DDX_Control(pDX, IDC_ST_DOT1, m_st_dot2);
	DDX_Control(pDX, IDC_EDIT_X2, m_x2);
	DDX_Control(pDX, IDC_EDIT_Y2, m_y2);

	DDX_Control(pDX, IDC_ST_CENTER, m_st_center);
	DDX_Control(pDX, IDC_EDIT_CX, m_cx);
	DDX_Control(pDX, IDC_EDIT_CY, m_cy);
	DDX_Control(pDX, IDC_ST_BEAMS, m_st_beams);
	DDX_Control(pDX, IDC_EDIT_BEAMS, m_beams);
	DDX_Control(pDX, IDC_SPIN, m_spin);
	DDX_Control(pDX, IDC_ST_LEN, m_st_len);
	DDX_Control(pDX, IDC_EDIT_LEN, m_len);
}

BEGIN_MESSAGE_MAP(Clab4Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_DRAWSTYLE, OnTcnSelchangeDrawstyle)
	ON_BN_CLICKED(IDC_DRAW, OnBnClickedDraw)
	ON_BN_CLICKED(IDC_CLEAR, OnBnClickedClear)
	ON_BN_CLICKED(IDC_CHOOSE, OnBnClickedChoose)
	ON_BN_CLICKED(IDC_RESEARCH, OnBnClickedResearch)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()



// Clab4Dlg message handlers

BOOL Clab4Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// Spin control
	m_spin.SetRange( 0, MAXRAYS );

	// Tab control
	m_drawstyle.InsertItem( TAB_SEGMENT, "�������" );
	m_drawstyle.InsertItem( TAB_SUN, "������" );
	m_drawstyle.SetCurSel( TAB_SEGMENT );
	SwitchTab( TAB_SEGMENT );

	// Reseting program to initial state
	DefaultData();
	OnBnClickedClear();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Clab4Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Clab4Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Clab4Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void Clab4Dlg::OnTcnSelchangeDrawstyle(NMHDR *pNMHDR, LRESULT *pResult)
{
	SwitchTab( m_drawstyle.GetCurSel() );

	*pResult = 0;
}

static int edittoi( CEdit & edit )
{
	const unsigned length = edit.GetWindowTextLength() + 1;
	char * temp = new char [ length ];
	edit.GetWindowText( temp, length );

	int value = atoi( temp );

	delete temp;

	return ( value );
}

void Clab4Dlg::OnBnClickedDraw()
{
	UpdateData( true );
	pAlgo Algo = ChooseAlgo( m_algorithm );

	switch ( m_drawstyle.GetCurSel() )
	{
	case TAB_SEGMENT:
		{
		line_t line;
        line.dot1.SetPoint( edittoi( m_x1 ), edittoi( m_y1 ) );
		line.dot2.SetPoint( edittoi( m_x2 ), edittoi( m_y2 ) );

		unsigned steps;  // unneeded
		bool precise = Algo( m_picture.pmf, line, m_color, steps );

		if ( !precise )
			Mark( m_picture.pmf, line.dot2 );
		}
		break;
	case TAB_SUN:
		{
		int n = edittoi( m_beams );
		int L = edittoi( m_len );
		line_t line;
		line.dot1.SetPoint( edittoi( m_cx ), edittoi( m_cy ) );
		line.dot2.SetPoint( line.dot1.x + L, line.dot1.y );
		CPoint savedot( line.dot2 );

		for ( int i = 1; ; )
		{
			unsigned steps;  // unneeded
			bool precise = Algo( m_picture.pmf, line, m_color, steps );

			if ( !precise )
				Mark( m_picture.pmf, line.dot2 );

		if ( ++i > n ) break;

			line.dot2 = Rotate( savedot, line.dot1, ( i - 1 ) * 2 * M_PI / n );
		}
		}
		break;
	}

	m_picture.Invalidate( false );
}

void Clab4Dlg::OnBnClickedClear()
{
	if ( !firstdot )
	{
		HMETAFILE hmfsave = pmfsave->Close();
		DeleteMF( pmfsave, hmfsave );
		firstdot = true;
	}

	m_picture.Clear();
	CRect area( m_picture.m_DPrect );
	area.DeflateRect( 1, 1, 1, 1 );
	m_picture.pmf->IntersectClipRect( area );

	m_picture.Invalidate( false );

	RepresentColor( m_color );
}

void Clab4Dlg::OnBnClickedChoose()
{
	COLORREF defcolors[ 16 ];
	for ( int i = 0; i < 16; i++ )
		defcolors[ i ] = RGB( 255, 255, 255 );

	CHOOSECOLOR colors;
	colors.lStructSize = sizeof( CHOOSECOLOR );
	colors.hwndOwner = NULL;
	colors.Flags = CC_RGBINIT;
	colors.rgbResult = RGB( 0, 0, 0 );
	colors.lpCustColors = defcolors;

	ChooseColor( &colors );

	RepresentColor( m_color = colors.rgbResult );
}

void Clab4Dlg::DefaultData()
{
	m_x1.SetWindowText( "10" );
	m_y1.SetWindowText( "10" );
	m_x2.SetWindowText( "250" );
	m_y2.SetWindowText( "50" );

	m_cx.SetWindowText( "250" );
	m_cy.SetWindowText( "250" );
	m_beams.SetWindowText( "90" );
	m_len.SetWindowText( "150" );
}

void Clab4Dlg::RepresentColor( COLORREF color )
{
	ResetMF( m_drawcolor.pmf );

	CBrush newColor( color );
	CGdiObject * pOldPen = m_drawcolor.pmf->SelectStockObject( BLACK_PEN );
	CGdiObject * pOldBrush = m_drawcolor.pmf->SelectObject( &newColor );

	m_drawcolor.pmf->Rectangle( m_drawcolor.m_DPrect );

	m_drawcolor.pmf->SelectObject( pOldPen );
	m_drawcolor.pmf->SelectObject( pOldBrush );

	m_drawcolor.Invalidate( false );
}

void Clab4Dlg::OnBnClickedResearch()
{
	CResearchDlg dlg( this );
	dlg.DoModal();
}

void Clab4Dlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) )
	{
		if ( firstdot )
		{
			pmfsave = NewMF( m_picture.pmf );

			CEdit & m_x = m_drawstyle.GetCurSel() == TAB_SUN ? m_cx : m_x1;
			CEdit & m_y = m_drawstyle.GetCurSel() == TAB_SUN ? m_cy : m_y1;
			CString coord( "" );
			coord.Format( "%li", point.x );
			m_x.SetWindowText( coord );
			coord.Format( "%li", point.y );
			m_y.SetWindowText( coord );
		}
		else
		{
			HMETAFILE hmfsave = pmfsave->Close();
			DeleteMF( pmfsave, hmfsave );
		}

		firstdot = !firstdot;
	}

	CDialog::OnLButtonDown(nFlags, point);
}

void Clab4Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) )
	{
		CString coord( "" );
		coord.Format( "%li : %li", point.x, point.y );
		m_mousepos.SetWindowText( coord );

		if ( !firstdot )
		{
			ResetMF( m_picture.pmf );
			PlayMF( m_picture.pmf, pmfsave );

			switch ( m_drawstyle.GetCurSel() )
			{
			case TAB_SEGMENT:
				{
				CString coord( "" );
				coord.Format( "%li", point.x );
				m_x2.SetWindowText( coord );
				coord.Format( "%li", point.y );
				m_y2.SetWindowText( coord );
				}
				break;
			case TAB_SUN:
				{
				CPoint center( edittoi( m_cx ), edittoi( m_cy ) );
				long Rvalue = round( hypot( point.x - center.x, point.y - center.y ) );
				CString Rstr( "" );
				Rstr.Format( "%li", Rvalue );
				m_len.SetWindowText( Rstr );
				}
				break;
			}

			OnBnClickedDraw();
		}
	}

	CDialog::OnMouseMove(nFlags, point);
}
