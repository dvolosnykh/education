#include "stdafx.h"
#include "lab4.h"
#include "lab4Dlg.h"
#include ".\lab4dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


void Clab4Dlg::SwitchTabSegment()
{
	// Turn on/off tabs
	VisibleTab( TAB_SEGMENT );
}

void Clab4Dlg::SwitchTabSun()
{
	// Turn on/off tabs
	VisibleTab( TAB_SUN );
}

void Clab4Dlg::SwitchTab( UINT nTab )
{
	switch ( nTab )
	{
	case TAB_SEGMENT:
		SwitchTabSegment();
		break;
	case TAB_SUN:
		SwitchTabSun();
		break;
	}
}
