#include "stdafx.h"
#include "lab4.h"
#include "lab4Dlg.h"
#include ".\lab4dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


void Clab4Dlg::VisibleTabSegment( bool visible /*= true*/ )
{
	m_st_dot1.ShowWindow( visible );
	m_x1.ShowWindow( visible );
	m_y1.ShowWindow( visible );
	m_st_dot2.ShowWindow( visible );
	m_x2.ShowWindow( visible );
	m_y2.ShowWindow( visible );
}

void Clab4Dlg::VisibleTabSun( bool visible /*= true*/ )
{
	m_st_center.ShowWindow( visible );
	m_cx.ShowWindow( visible );
	m_cy.ShowWindow( visible );
	m_st_beams.ShowWindow( visible );
	m_beams.ShowWindow( visible );
	m_spin.ShowWindow( visible );
	m_st_len.ShowWindow( visible );
	m_len.ShowWindow( visible );
}

void Clab4Dlg::VisibleTab( UINT nTab )
{
	switch ( nTab )
	{
	case TAB_SEGMENT:
		VisibleTabSegment( true );
		VisibleTabSun( false );
		break;
	case TAB_SUN:
		VisibleTabSegment( false );
		VisibleTabSun( true );
		break;
	}
}