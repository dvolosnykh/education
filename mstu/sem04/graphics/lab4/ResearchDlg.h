#pragma once
#include "mypicture.h"


// CResearchDlg dialog

class CResearchDlg : public CDialog
{
	DECLARE_DYNAMIC(CResearchDlg)

public:
	CResearchDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CResearchDlg();

	void ShowColor( CDC * pDC );
	void CoordSystem( CDC * pDC );
	void DrawTimes( CDC * pDC, DWORD * time );
	void DrawSteps( CDC * pDC, unsigned * steps );
	void Draw( CMetaFileDC * & pDC );

// Dialog Data
	enum { IDD = IDD_RESEARCH };

private:
	CmyPicture m_graph;
	CmyPicture m_algocolor;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
};
