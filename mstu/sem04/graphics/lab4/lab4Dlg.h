// lab4Dlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "lab4.h"
#include "myPicture.h"


enum
{
	TAB_SEGMENT,
	TAB_SUN
};


// Clab4Dlg dialog
class Clab4Dlg : public CDialog
{
private:
//public:
	CmyPicture m_picture;
	CStatic m_mousepos;
	CmyPicture m_drawcolor;
	int m_algorithm;
	CTabCtrl m_drawstyle;
	
	CStatic m_st_dot1;
	CEdit m_x1;
	CEdit m_y1;
	CStatic m_st_dot2;
	CEdit m_x2;
	CEdit m_y2;

	CStatic m_st_center;
	CEdit m_cx;
	CEdit m_cy;
	CStatic m_st_beams;
	CEdit m_beams;
	CSpinButtonCtrl m_spin;
	CStatic m_st_len;
	CEdit m_len;

	COLORREF m_color;

// Construction
public:
	Clab4Dlg(CWnd* pParent = NULL);	// standard constructor
	~Clab4Dlg();

	// switch.cpp
	void SwitchTabSegment();
	void SwitchTabSun();
	void SwitchTab( UINT nTab );

	// visible.cpp
	void VisibleTabSegment( bool visible = true );
	void VisibleTabSun( bool visible = true );
	void VisibleTab( UINT nTab );

	void DefaultData();
	void RepresentColor( COLORREF color );

// Dialog Data
	enum { IDD = IDD_MAINWND };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTcnSelchangeDrawstyle(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedDraw();
	afx_msg void OnBnClickedClear();
	afx_msg void OnBnClickedChoose();
	afx_msg void OnBnClickedResearch();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
