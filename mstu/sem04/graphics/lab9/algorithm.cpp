#include "stdafx.h"
#include "algorithm.h"
#include "myLine.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define M_PI	3.14159265358979323846


typedef CPoint vector_t;
typedef vector< CPoint > points_t;
typedef int ( *cmp_t )( const void * p1, const void * p2 );


static void DrawLine( CDC * pDC, const CmyLine & line, const COLORREF & color )
{
	CPen pencil( PS_SOLID, 1, color );
	CGdiObject * pOldPen = pDC->SelectObject( &pencil );

	pDC->MoveTo( line.dot1 );
	pDC->LineTo( line.dot2 );
	pDC->SetPixel( line.dot2, color );

	pDC->SelectObject( pOldPen );
}

void DrawPoly( CDC * pDC, const polygon_t & polygon, const COLORREF & color,
				bool closed /*= true */ )
{
	CPen pencil( PS_SOLID, 1, color );
	CGdiObject * pOldPen = pDC->SelectObject( &pencil );
	CGdiObject * pOldBrush = pDC->SelectStockObject( NULL_BRUSH );

	pDC->MoveTo( polygon[ 0 ] );
	for ( size_t i = 1; i < polygon.size(); i++ )
		pDC->LineTo( polygon[ i ] );

	if ( closed )
		pDC->LineTo( polygon[ 0 ] );

	pDC->SelectObject( pOldPen );
	pDC->SelectObject( pOldBrush );
}


static int sign( long x )
{
	return ( x < 0 ? -1 : x > 0 ? 1 : 0 );
}

static bool insegment( const CPoint & dot, const CmyLine & line )
{
	const long xmin = min( line.dot1.x, line.dot2.x );
	const long xmax = max( line.dot1.x, line.dot2.x );
	const long ymin = min( line.dot1.y, line.dot2.y );
	const long ymax = max( line.dot1.y, line.dot2.y );
	
	return ( xmin <= dot.x && dot.x <= xmax &&
		ymin <= dot.y && dot.y <= ymax );
}

static int qcmpx( const void * p1, const void * p2 )
{
	const CPoint & dot1 = *( CPoint * )p1;
	const CPoint & dot2 = *( CPoint * )p2;

	return ( dot1.x - dot2.x );
}

static int qcmpy( const void * p1, const void * p2 )
{
	const CPoint & dot1 = *( CPoint * )p1;
	const CPoint & dot2 = *( CPoint * )p2;

	return ( dot1.y - dot2.y );
}

static cmp_t defqcmp( CmyLine & line )
{
	return ( line.IsVertical() || fabs( line.Inclination() ) > M_PI / 4 ? qcmpy : qcmpx );
}

static bool inlimits( CmyLine & edge, const long y )
{
	const long ymin = min( edge.dot1.y, edge.dot2.y );
	const long ymax = max( edge.dot1.y, edge.dot2.y );

	return ( ymin < y && y <= ymax );
}

static long round( const double x )
{
	return ( long )( x + 0.5 );
}

static long findX( const long y, const CmyLine & line )
{
	const CPoint & dot1 = line.dot1;
	const CPoint & dot2 = line.dot2;

	return round( ( y - dot1.y ) * ( dot2.x - dot1.x ) /
		( double )( dot2.y - dot1.y ) + dot1.x );
}

static bool isborder( CmyLine & edge, const CPoint & dot )
{
	return ( !edge.IsHorizontal() && inlimits( edge, dot.y )
		&& findX( dot.y, edge ) < dot.x );
}

static bool extremum( CmyLine & line, CmyLine & edge1, CmyLine& edge2 )
{
	const coeff_t eqn = line.FindCoeffs();

	const int s1 = sign( eqn.A * edge1.dot1.x + eqn.B * edge1.dot1.y + eqn.C );
	const int s2 = sign( eqn.A * edge2.dot2.x + eqn.B * edge2.dot2.y + eqn.C );

	return ( s1 == s2 );
}

void AlgoSmth( CDC * pDC, const polygon_t & poly, const polygon_t & cutter,
			  const COLORREF & in_color, const COLORREF & out_color )
{
	for ( size_t i = 0; i < poly.size(); i++ )
	{
		CmyLine line( poly[ i ], poly[ ( i + 1 ) % poly.size() ] );
		const cmp_t qcmp = defqcmp( line );
		const CPoint & testdot = qcmp( &line.dot1, &line.dot2 ) < 0 ? line.dot1 : line.dot2;

		points_t points;
		points.push_back( line.dot1 );
		points.push_back( line.dot2 );

		bool inside = false;
		for ( size_t j = 0; j < cutter.size(); j++ )
		{
			CmyLine cur( cutter[ j ], cutter[ ( j + 1 ) % cutter.size() ] );

			if ( !line.IsParallelTo( cur ) )
			{
				CPoint I = line.IntersectWith( cur );

				if ( insegment( I, cur ) && insegment( I, line ) )
				{
					CmyLine next( cutter[ ( j + 1 ) % cutter.size() ],
						cutter[ ( j + 2 ) % cutter.size() ] );

					if ( !( I == cur.dot2 && extremum( line, cur, next ) )
							&& I != cur.dot1 )
						points.push_back( I );
				}
			}

			if ( isborder( cur, testdot ) )
				inside = !inside;
		}

		qsort( &points.front(), points.size(), sizeof( points.front() ), qcmp );

		// drawing
		for ( size_t i = 1; i < points.size(); i++ )
		{
			DrawLine( pDC, CmyLine( points[ i - 1 ], points[ i ] ),
				inside ? in_color : out_color );
			inside = !inside;
		}
	}
}
