#pragma once


struct coeff_t
{
	long A;
	long B;
	long C;
};


// CmyLine

class CmyLine
{
public:
	CPoint dot1;
	CPoint dot2;

public:
	CmyLine();
	CmyLine( const CPoint p1, const CPoint p2 );
	~CmyLine();

	long Ymin();
	long Ymax();
	long Xmin();
	long Xmax();
	double Length();
	bool IsValid();
	bool IsVertical();
	bool IsHorizontal();
	bool IsInclined();
	double Inclination();
	bool IsParallelTo( CmyLine & line );
	coeff_t FindCoeffs();
	CPoint FixPoint( const double t );
	CPoint IntersectWith( CmyLine & line );

	void Draw( CDC * pDC );

	CmyLine operator = ( const CmyLine & line );
	bool operator == ( const CmyLine & line );
	bool operator != ( const CmyLine & line );
};

