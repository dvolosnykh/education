// lab9Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "lab9.h"
#include "lab9Dlg.h"
#include ".\lab9dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static enum
{
	INP_NONE,
	INP_POLYGON,
	INP_CUTTER
} input;

static CPoint mouse;  // mouse coordinates
static CPoint last;  // last inputted dot


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// Clab9Dlg dialog



Clab9Dlg::Clab9Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(Clab9Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Clab9Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control( pDX, IDC_PICTURE, m_picture );
}

BEGIN_MESSAGE_MAP(Clab9Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CUT, OnBnClickedCut)
	ON_BN_CLICKED(IDC_RESET, OnBnClickedReset)
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
END_MESSAGE_MAP()


// Clab9Dlg message handlers

BOOL Clab9Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	OnBnClickedReset();
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Clab9Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Clab9Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Clab9Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void Clab9Dlg::OnBnClickedCut()
{
	if ( cutter.empty() || poly.empty() )
	{
		CString text( "" );

		if ( cutter.empty() )
			text = "���������� �� �����.";

		if ( cutter.empty() && poly.empty() )
			text += "\n";

		if ( poly.empty() )
			text += "������������� �� �����.";

		MessageBox( text, "������" );
	}
	else
	{
		m_picture.Clear();

		AlgoSmth( m_picture.pmf, poly, cutter, in_color, out_color );
		AlgoSmth( m_picture.pmf, cutter, poly, in_color, cutter_color );
	}

	m_picture.Invalidate( false );
}

void Clab9Dlg::OnBnClickedReset()
{
	m_picture.Clear();
	ResetData();
}

void Clab9Dlg::ResetData()
{
	m_picture.Clear();
	CRect area( m_picture.m_DPrect );
	area.DeflateRect( 1, 1, 1, 1 );
	m_picture.pmf->IntersectClipRect( area );
	m_picture.Invalidate( false );

	if ( !poly.empty() )
		poly.clear();

	if ( !cutter.empty() )
		cutter.clear();

	input = INP_NONE;
}

void Clab9Dlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) )
		switch ( input )
		{
		case INP_NONE:
			last = point;
				
			if ( !poly.empty() )
			{
				poly.clear();

				m_picture.Clear();

				if ( !cutter.empty() )
					DrawPoly( m_picture.pmf, cutter, cutter_color );
				
				m_picture.Invalidate( false );
			}

			input = INP_POLYGON;
		case INP_POLYGON:
			poly.push_back( last );
			break;
		case INP_CUTTER:
			m_picture.Clear();

			if ( !poly.empty() )
				DrawPoly( m_picture.pmf, poly, def_color );
			DrawPoly( m_picture.pmf, cutter, cutter_color );

			m_picture.Invalidate( false );

			input = INP_NONE;
			break;
		}

	CDialog::OnLButtonDown(nFlags, point);
}

void Clab9Dlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) )
		switch ( input )
		{
		case INP_NONE:
			last = point;
				
			if ( !cutter.empty() )
			{
				cutter.clear();

				m_picture.Clear();

				if ( !poly.empty() )
					DrawPoly( m_picture.pmf, poly, def_color );

				m_picture.Invalidate( false );
			}

			input = INP_CUTTER;
		case INP_CUTTER:
			cutter.push_back( last );
			break;
		case INP_POLYGON:
			m_picture.Clear();

			DrawPoly( m_picture.pmf, poly, def_color );
			if ( !cutter.empty() )
				DrawPoly( m_picture.pmf, cutter, cutter_color );

			m_picture.Invalidate( false );

			input = INP_NONE;
			break;
		}

	CDialog::OnRButtonDown(nFlags, point);
}

static void MakeHorVer( CPoint & point, const CPoint & dot1 )
{
	if ( abs( point.x - dot1.x ) < abs( point.y - dot1.y ) )
		point.x = dot1.x;
	else
		point.y = dot1.y;
}

void Clab9Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// making point coordinates correct
	mouse = point;
	point.Offset( -11, -11 );

	if ( m_picture.m_DPrect.PtInRect( point ) && input != INP_NONE )
	{
		m_picture.Clear();

		switch ( input )
		{
		case INP_POLYGON:
			{
			// for drawing hor/vert segments
			if ( nFlags == MK_CONTROL && !poly.empty() )
				MakeHorVer( point, poly.back() );

			poly.push_back( last = point );
			DrawPoly( m_picture.pmf, poly, def_color, false );
			poly.pop_back();

			if ( !cutter.empty() )
				DrawPoly( m_picture.pmf, cutter, cutter_color );
			}
			break;
		case INP_CUTTER:
			{
			// for drawing hor/vert segments
			if ( nFlags == MK_CONTROL && !cutter.empty() )
				MakeHorVer( point, cutter.back() );

			if ( !poly.empty() )
				DrawPoly( m_picture.pmf, poly, def_color );

			cutter.push_back( last = point );
			DrawPoly( m_picture.pmf, cutter, cutter_color, false );
			cutter.pop_back();
			}
			break;
		}

		m_picture.SetFocus();
		m_picture.Invalidate( false );
	}

	CDialog::OnMouseMove(nFlags, point);
}

void Clab9Dlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if ( nChar == VK_CONTROL )
		OnMouseMove( MK_CONTROL, mouse );

	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}

void Clab9Dlg::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if ( nChar == VK_CONTROL )
		OnMouseMove( 0, mouse );

	CDialog::OnKeyUp(nChar, nRepCnt, nFlags);
}
