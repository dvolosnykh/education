#pragma once

#include "myLine.h"
#include <vector>

using namespace std;

const COLORREF cutter_color = RGB( 160, 160, 255 );
const COLORREF    def_color = RGB(   0,   0,   0 );
const COLORREF    out_color = RGB( 192, 192, 192 );
const COLORREF     in_color = RGB( 255,   0,   0 );

typedef vector< CPoint > polygon_t;

void DrawPoly( CDC * pDC, const polygon_t & polygon, const COLORREF & color,
				bool closed = true );

void AlgoSmth( CDC * pDC, const polygon_t & poly, const polygon_t & cutter,
			  const COLORREF & in_color, const COLORREF & out_color );