include	utils.asm


;--------------------------------------------------------------------------------
;  @ save_handle:	saves handle specified by code.
;
;  parameters:	code - code of the handle.
;--------------------------------------------------------------------------------

save_handle	Macro	code

	.errb	< code >

	push_ex	< AX, BX, ES >

	mov	AX, 35&code
	int	21h
	mov	word ptr CS:old_int&code, BX
	mov	word ptr CS:old_int&code + 2, ES

	pop_ex	< ES, BX, AX >
EndM

;--------------------------------------------------------------------------------
;  @ set_handle:	sets handle specified by code.
;
;  parameters:	code - code of the handle.
;--------------------------------------------------------------------------------

set_handle	Macro	code

	.errb	< code >

	push_ex	< AX, DX, DS >

	mov	DX, offset CS:new_int&code
	mov	AX, seg CS:new_int&code
	mov	DS, AX
	mov	AX, 25&code
	int	21h

	pop_ex	< DS, DX, AX >
EndM

;--------------------------------------------------------------------------------
;  @ restore_handle:	restores handle specified by code.
;
;  parameters:	code - code of the handle.
;--------------------------------------------------------------------------------

restore_handle	Macro	code

	.errb	< code >

	push_ex	< AX, DX, DS >

	mov	DX, offset CS:old_int&code
	mov	AX, seg CS:old_int&code
	mov	DS, AX
	mov	AX, 25&code
	int	21h

	pop_ex	< DS, DX, AX >
EndM



CSeg	Segment		'CODE'
	Assume	CS:CSeg

;********************************** DATA ****************************************

text		db	"Wish you the best...", 0
text_len	equ	$ - text - 1

TSR_run		db	0
timeout		db	0
busy_int10h	db	0
busy_int13h	db	0

indos_segm	dw	?
indos_offs	dw	?

error_segm	dw	?
error_offs	dw	?

old_int09h	dd	?
old_int10h	dd	?
old_int13h	dd	?
old_int1Ch	dd	?
old_int28h	dd	?
old_int2Fh	dd	?

;================================================================================
;  # new_int09h:	new handler for int 09h.
;================================================================================

new_int09h	Proc	Near

	pushf
	call	dword ptr CS:old_int09h

	push	AX

	mov	AH, 01h				; get code of the key.
	int	16h
	jz	end_switch_AX			; no key is pressed.

	switch_AX:
	case_5400h:
		cmp	AX, 5400h		; check if < Shift + F1 > pressed.
		jne	case_5500h

		cmp	CS:TSR_run, 0		; check if TSR is already running.
		jne	cancel

		mov	CS:timeout, 18h		; set the timer.
	cancel:
		jmp	end_switch_AX

	case_5500h:
		cmp	AX, 5500h		; check if < Shift + F2 > pressed.
		jne	end_switch_AX

		;mov	AH, MUX			; unload TSR-program.
		;mov	AL, 1
		;int	2Fh
	end_switch_AX:

	pop	AX
	iRet

new_int09h	EndP

;================================================================================
;  # new_int10h:	new handler for int 10h.
;================================================================================

new_int10h	Proc	Near

	pushf
	inc	CS:busy_int10h

	call	dword ptr CS:old_int10h

	dec	CS:busy_int10h
	iRet

new_int10h	EndP

;================================================================================
;  # new_int13h:	new handler for int 13h.
;================================================================================

new_int13h	Proc	Near

	pushf
	inc	CS:busy_int13h

	call	dword ptr CS:old_int13h

	dec	CS:busy_int13h
	iRet

new_int13h	EndP

;================================================================================
;  # new_int1Ch:	new handler for int 1Ch.
;================================================================================

new_int1Ch	Proc	Near

	pushf
	call	dword ptr CS:old_int1Ch

	push_ex	< DS, SI >

	cmp	CS:timeout, 0		; check if timeout is reached.
	je	exit_int1Ch

	cmp	CS:busy_int10h, 0	; check if screen service is free.
	ja	pre_exit_int1Ch

	cmp	CS:busy_int13h, 0	; check if disk service is free.
	ja	pre_exit_int1Ch

	mov	DS, CS:indos_segm	; check indos flag.
	mov	SI, CS:indos_offs
	cmp	byte ptr [ SI ], 0
	jne	pre_exit_int1Ch

	mov	ES, CS:error_segm	; check critical DOS error.
	mov	DI, CS:error_offs
	cmp	byte ptr [ SI ], 0
	jne	pre_exit_int1Ch

	mov	CS:timeout, 0		; reset timer in case of success.
	call	wish			; action to be done by TSR-program.
	jmp	exit_int1Ch

pre_exit_int1Ch:
	dec	CS:timeout		; decrease timer in case of fail.

exit_int1Ch:
	pop_ex	< SI, DS >
	iRet

new_int1Ch	EndP

;================================================================================
;  # new_int28h:	new handler for int 28h.
;================================================================================

new_int28h	Proc	Near

	pushf
	call	dword ptr CS:old_int28h

	push_ex	< DS, SI >

	cmp	CS:timeout, 0		; check if timeout is reached.
	je	exit_int28h

	cmp	CS:busy_int10h, 0	; check if screen service is free.
	ja	pre_exit_int28h

	cmp	CS:busy_int13h, 0	; check if disk service is free.
	ja	pre_exit_int28h

	mov	DS, CS:error_segm	; check critical DOS error.
	mov	SI, CS:error_offs
	cmp	byte ptr [ SI ], 0
	jne	pre_exit_int1Ch

	call	wish			; action to be done by TSR-program.
	mov	CS:timeout, 0		; reset timer in case of success.
	jmp	exit_int28h

pre_exit_int28h:

exit_int28h:
	pop_ex	< SI, DS >
	iRet

new_int28h	EndP

;================================================================================
;  # new_int2Fh:	new handler for int 2Fh.
;================================================================================

new_int2Fh	Proc	Near

	pushf
	call	dword ptr CS:old_int2Fh

	cmp	AH, MUX			; check if mux-code is our.
	je	switch_AL
	jmp	not_80h

	switch_AL:
	case_0:
		cmp	AL, 0
		jne	case_1

		mov	AL, 0FFh	; mux-code is reserved.

		jmp	end_switch_AL
	case_1:
		cmp	AL, 1
		jne	end_switch_AL

;		cli
;			restore_handle	09h
;			restore_handle	10h
;			restore_handle	13h
;			restore_handle	1Ch
;			restore_handle	28h
;			restore_handle	2Fh
;		sti

		jmp	end_switch_AL
	end_switch_AL:

not_80h:
	iRet

new_int2Fh	EndP

;================================================================================
;  # wish:	presents a wish.
;
;  parameters:	< nothing >
;
;  return:	< nothing >
;================================================================================

wish	Proc	Near

	mov	CS:TSR_run, 1			; TSR-program is running.

	clrscr

	push_ex	< seg CS:text, offset CS:text >
	push_ex < ( 80 - text_len ) / 2, 12 >
	call	output
	add	SP, 8

	mov	CS:TSR_run, 0			; TSR-program finished.

	RetN

wish	EndP

;================================================================================
;  # output:	outputs the text directly through the video-memory.
;
;  parameters:	SEGM - segment of the text.
;		OFFS - offset of the text.
;		( X, Y ) - coordinates of the begining of the text.
;
;  return:	< nothing >
;================================================================================

output	Proc	Near

	push	BP
	mov	BP, SP

Y	equ	word ptr [ BP ][  4 ]
X	equ	word ptr [ BP ][  6 ]
OFFS	equ	word ptr [ BP ][  8 ]
SEGM	equ	word ptr [ BP ][ 10 ]

	push_ex	< AX, DS, SI, ES, DI >
	pushf

	mov	DS, SEGM		; source text.
	mov	SI, OFFS

	multiply	Y, 80		; defining offset in video memory.
	add	AX, X
	shl	AX, 1
	mov	DI, AX

	mov	AX, 0B800h		; tuning to the video memory.
	mov	ES, AX

	cld
	mov	AH, 00000111b		; set white color.
	cycle:				; convert text to useful format.
		lodsb
		cmp	AL, 0
		je	end_cycle
		stosw
	jmp	cycle
end_cycle:

	popf
	pop_ex	< DI, ES, SI, DS, AX, BP >
	RetN

output	EndP

;================================================================================
;  # main:	main body.
;
;  parameters:	< nothing >
;
;  return:	AL - return-code:
;			0 - success.
;================================================================================

main:
	mov	AH, MUX			; check if TSR-program is already active.
	mov	AL, 0
	int	2Fh
	cmp	AL, 0
	je	not_loaded
	cmp	AL, 1
	je	cannot_load

	print_label	CS:active	; is already active.
	getch
	halt	0

cannot_load:
	print_label	CS:unable	; unable to load.
	getch
	halt	0

not_loaded:				; able to load.
	push	ES
	mov	AH, 34h			; address of the indos flag.
	int	21h
	mov	CS:indos_segm, ES
	mov	CS:indos_offs, BX
	pop	ES

	mov	AX, 5D06h		; address of the critical DOS error.
	int	21h
	mov	CS:error_segm, DS
	mov	CS:error_offs, SI

	pushf
	cli
		save_handle	09h
		set_handle	09h

		save_handle	10h
		set_handle	10h

		save_handle	13h
		set_handle	13h

		save_handle	1Ch
		set_handle	1Ch

		save_handle	28h
		set_handle	28h

		save_handle	2Fh
		set_handle	2Fh
	popf

	mov	DX, offset main + psp_len - 1	; count para-length of TSR-program.
	mov	CL, 4
	shr	DX, CL
	inc	DX
	mov	AX, 3100h		; run as TSR and close.
	int	21h

;********************************** DATA ****************************************

MUX		equ	80h
psp_len		equ	100h

active		db	"TSR-program is already active.", 0Ah, 0Dh, '$'
unable		db	"TSR-program is not active. But cannot be loaded."
		db	0Ah, 0Dh, '$'

CSeg	EndS


END	main