

;/******************************************************************************/
;/*                             MACRO UTILS                                    */
;/******************************************************************************/



;--------------------------------------------------------------------------------
;  @ push_ex:	pushes a list of data into stack.
;		NOTE: data __CAN__ be a simple literal-constant value.
;
;  parameters:	data_list - a list of data.
;--------------------------------------------------------------------------------

push_ex		Macro	data_list

	irp	data, < data_list >
	.errb	< data >

	push	data

	endm
EndM


;--------------------------------------------------------------------------------
;  @ pop_ex:	pops a list of data from stack.
;		NOTE: data __CANNOT__ be a simple literal-constant value.
;
;  parameters:	data_list - a list of data.
;--------------------------------------------------------------------------------

pop_ex		Macro	data_list

	irp	data, < data_list >
	.errb	< data >

	pop	data

	endm
EndM


;--------------------------------------------------------------------------------
;  @ print_label:	prints a string.
;
;  parameters:	string - label of a string.
;--------------------------------------------------------------------------------

print_label	Macro	string

	.errb	< string >

	push_ex	< AX, DS, DX >

	mov	AX, seg string
	mov	DS, AX
	mov	DX, offset string
	mov	AH, 09h
	int	21h

	pop_ex	< DX, DS, AX >
EndM

;--------------------------------------------------------------------------------
;  @ print_addr:	prints a string.
;
;  parameters:	str_segm - segment of a string.
;		str_offs - offset of a string.
;--------------------------------------------------------------------------------

print_addr	Macro	str_segm, str_offs

	.errb	< str_segm >
	.errb	< str_offs >

	push	AX

	ifdif	< str_segm >, < DS >
		push	DS
		mov	AX, str_segm
		mov	DS, AX
	endif
	ifdif	< str_offs >, < DX >
		push	DX
		mov	DX, str_offs
	endif

	mov	AH, 09h
	int	21h

	ifdif	< str_offs >, < DX >
		pop	DX
	endif
	ifdif	< str_segm >, < DS >
		pop	DS
	endif

	pop	AX
EndM


;--------------------------------------------------------------------------------
;  @ getch:	gets one symbol without echo.
;
;  parameters:	< nothing >
;
;  return:	AL - symbol.
;--------------------------------------------------------------------------------

getch	Macro

	mov	AH, 07h
	int	21h
EndM


;--------------------------------------------------------------------------------
;  @ getche:	gets one symbol with echo.
;
;  parameters:	< nothing >
;
;  return:	AL - symbol.
;--------------------------------------------------------------------------------

getche	Macro

	mov	AH, 01h
	int	21h
EndM


;--------------------------------------------------------------------------------
;  @ putch:	echoes a list of byte-containers.
;
;  parameters:	symbol_list - a list of byte-containers.
;--------------------------------------------------------------------------------

putch	Macro	symbol_list

	.errb	< symbol_list >

	push_ex	< AX, DX >

	irp	symbol, < symbol_list >

		.errb	< symbol >

		ifdif	< symbol >, < DL >
			mov	DL, symbol
		else
			pop	DX
			push	DX
		endif

		mov	AH, 02h
		int	21h
	endm

	pop_ex	< DX, AX >
EndM


;--------------------------------------------------------------------------------
;  @ endl:	moves cursor to the next line
;
;  parameters:	< nothing >
;--------------------------------------------------------------------------------

endl	Macro

	putch	< 10, 13 >
EndM


;--------------------------------------------------------------------------------
;  @ divide:	divides A by B.
;
;  parameters:	A - divisible.
;		B - dividor.
;
;  return:	AX - integer part.
;		DX - reminder.
;--------------------------------------------------------------------------------

divide	Macro	A, B

	.errb	< A >
	.errb	< B >

	ifdif	< A >, < AX >
		mov	AX, A
	endif
	ifdif	< B >, < BX >
		push	BX
		mov	BX, B
	endif

	mov	DX, 0
	div	BX

	ifdif	< B >, < BX >
		pop	BX
	endif
EndM

;--------------------------------------------------------------------------------
;  @ multiply:	multiplies A by B.
;
;  parameters:	A - left multiplier.
;		B - right multiplier.
;
;  return:	AX - multiplication.
;--------------------------------------------------------------------------------

multiply	Macro	A, B

	.errb	< A >
	.errb	< B >

	ifdif	< A >, < AX >
		mov	AX, A
	endif
	ifdif	< B >, < BX >
		push	BX
		mov	BX, B
	endif

	mul	BX

	ifdif	< B >, < BX >
		pop	BX
	endif
EndM

;--------------------------------------------------------------------------------
;  @ halt:	close the program.
;
;  parameters:	code - return code.
;
;  return:	< nothing >
;--------------------------------------------------------------------------------

halt	Macro	code

	ifnb	< code >
		mov	AL, code
	endif

	mov	AH, 4Ch
	int	21h
EndM

;--------------------------------------------------------------------------------
;  @ set_bit:	sets specified bit in a bit-field.
;
;  parameters:	field - bit-field (size of byte).
;		pos - specified bit (size of byte).
;
;  return:	< nothing >
;--------------------------------------------------------------------------------

set_bit	Macro	field, pos

	.errb	< field >
	.errb	< pos >

	ifdif	< pos >, < CL >
		push	CX
		mov	CL, pos
	endif

	push	AX

	mov	AL, 00000001b
	shl	AL, CL
	or	field, AL

	pop	AX

	ifdif	< pos >, < CL >
		pop	CX
	endif
EndM

;--------------------------------------------------------------------------------
;  @ clr_bit:	clears specified bit in a bit-field.
;
;  parameters:	field - bit-field (size of byte).
;		pos - specified bit (size of byte).
;
;  return:	< nothing >
;--------------------------------------------------------------------------------

clr_bit	Macro	field, pos

	.errb	< field >
	.errb	< pos >

	ifdif	< pos >, < CL >
		push	CX
		mov	CL, pos
	endif

	push	AX

	mov	AL, 11111110b
	rol	AL, CL
	and	field, AL

	pop	AX

	ifdif	< pos >, < CL >
		pop	CX
	endif
EndM

;--------------------------------------------------------------------------------
;  @ tgl_bit:	toggles specified bit in a bit-field.
;
;  parameters:	field - bit-field (size of byte).
;		pos - specified bit (size of byte).
;
;  return:	< nothing >
;--------------------------------------------------------------------------------

tgl_bit	Macro	field, pos

	.errb	< field >
	.errb	< pos >

	ifdif	< pos >, < CL >
		push	CX
		mov	CL, pos
	endif

	push	AX

	mov	AL, 00000001b
	shl	AL, CL
	xor	field, AL

	pop	AX

	ifdif	< pos >, < CL >
		pop	CX
	endif
EndM

;--------------------------------------------------------------------------------
;  @ get_bit:	gets specified bit in a bit-field.
;
;  parameters:	field - bit-field (size of byte).
;		pos - specified bit (size of byte).
;
;  return:	< nothing >
;--------------------------------------------------------------------------------

get_bit	Macro	field, pos

	.errb	< field >
	.errb	< pos >

	ifdif	< pos >, < CL >
		push	CX
		mov	CL, pos
	endif

	push	AX

	mov	AL, 00000001b
	shl	AL, CL
	and	field, AL
	shr	field, CL

	pop	AX

	ifdif	< pos >, < CL >
		pop	CX
	endif
EndM

;--------------------------------------------------------------------------------
;  @ clrscr:	clears screen.
;
;  parameters:	< nothing >
;
;  return:	< nothing >
;--------------------------------------------------------------------------------

clrscr	Macro

	push	AX

	mov	AX, 0003h
	int	10h

	pop	AX
EndM

