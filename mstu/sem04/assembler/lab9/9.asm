include	utils.asm


SSeg	Segment		Stack 'STACK'
	db	100h dup( ? )
SSeg	EndS


DSeg	Segment		'DATA'

source		db	"SOURCE.TXT", 0

label	src_full	byte
src_name	db	8 dup( ? )
src_ext		db	3 dup( ? )

buff_size	equ	200h
buffer		db	buff_size dup( ? ), '$'

FAT1		db	1200h dup ( ? )
root		db	200h dup ( ? )
file_size	dd	?
sect_size	dw	?
clust_size	db	?
root_size	dw	?
fat_count	db	?
reserve_sects	dw	?
fat_size	dw	?

data		db	200h dup( ? )

file_not_found	db	"< File not found >", '$'
cannot_access	db	"< Cannot access file >", '$'
file_empty	db	"< File is empty >", '$'

DSeg	EndS


CSeg	Segment		'CODE'
	Assume	SS:SSeg, ES:DSeg, CS:CSeg, DS:DSeg

;================================================================================
;  # main:	main body.
;
;  parameters:	< nothing >
;
;  return:	AL - return-code:
;			0 - success.
;--------------------------------------------------------------------------------
;  locals:	BX - offset of element corresponding to a file.
;================================================================================

main:
	mov	AX, DSeg
	mov	DS, AX
	mov	ES, AX

	call	make_name

	mov	AL, 0
	mov	DX, 0
	mov	CX, 1
	lea	BX, root
	int	25h
	pop	AX
	jnc	calculate
	jmp	access_failed
calculate:
	mov	AX, word ptr root[ 0Bh ]
	mov	sect_size, AX
	mov	AH, root[ 0Dh ]
	mov	clust_size, AH
	add	clust_size, '0'

	mov	AX, word ptr root[ 0Eh ]
	mov	reserve_sects, AX
	mov	AH, root[ 10h ]
	mov	fat_count, AH
	mov	AX, word ptr root[ 11h ]
	mov	root_size, AX
	mov	AX, word ptr root[ 16h ]
	mov	fat_size, AX

	mov	AX, root_size			; number of root sectors.
	multiply	AX, 20h
	divide	AX, sect_size

	mov	DX, AX

	mov	AX, fat_size			; begining of the data.
	mul	fat_count
	add	AX, DX
	add	AX, reserve_sects

	push	AX				; saving first sector for data.

	mov	DX, 0013h
	mov	CX, 000Eh
	cycle_search:				; searching for file.
		push	CX
		mov	AL, 00h
		mov	CX, 1
		lea	BX, buffer
		int	25h
		pop_ex	< AX, CX >
		jnc	access_cycle_search
		jmp	access_failed
	access_cycle_search:
		push	CX
		mov	CX, 10h
		cycle_element:
			push_ex	< CX, -1, BX >
			call	compare
			add	SP, 2
			pop_ex	< AX, CX >

			cmp	AX, 0
			je	continue_search
			pop	CX
			jmp	end_cycle_search
		continue_search:
			add	BX, 20h
			loop	cycle_element
		end_cycle_element:
		pop	CX

		inc	DX
		loop	cycle_search
	end_cycle_search:

	cmp	AX, 0				; no such a file.
	jne	found
	jmp	not_found
found:

	putch	'#'
	print_addr	DS, BX
	putch	'#'
	endl
	getch

	mov	AX, [ BX ][ 1Ch ]
	mov	word ptr file_size, AX
	mov	AX, [ BX ][ 1Ch + 2 ]
	mov	word ptr ( file_size + 2 ), AX
	cmp	file_size, dword ptr 0		; file is empty.
	jne	pos_length
	jmp	zero_length
pos_length:

	push	BX
	mov	AL, 0				; reading FAT1 table.
	mov	DX, 1
	mov	CX, 9
	lea	BX, FAT1
	int	25h
	pop_ex	< AX, BX >
	jnc	access_ok
	jmp	access_failed
access_ok:

	mov	AX, [ BX ][ 1Ah ]		; first cluster.
	cycle:
		mov	DI, AX			; save current number.

		sub	AX, 2
		mul	clust_size
		pop	DX			; evaluate first sector for data.
		push	DX
		add	AX, DX

		push	BX
		mov	DX, AX
		mov	CH, 0
		mov	CL, clust_size
		mov	AL, 0
		lea	BX, data
		int	25h
		pop_ex	< AX, BX >
		jnc	access_cycle
		jmp	access_failed
	access_cycle:

		mov	CX, 200h		; output
		mov	SI, 0
		output_cycle:
			putch	< data[ SI ] >
			inc	SI

			dec	file_size
			jz	end_output_cycle

			loop	output_cycle
		end_output_cycle:

		; get number of the next cluster.
		multiply	DI, 3		; offset in a FAT1 table.
		shr	AX, 1

		mov	DX, DI
		mov	DI, AX
		mov	AX, word ptr FAT1[ DI ]
		and	DX, 0001h
		jz	even
	odd:
		mov	CL, 4
		shr	AX, CL
		jmp	continue
	even:
		and	AX, 0FFFh
		jmp	continue
	continue:
		
		cmp	AX, 0FFFh		; end of chain of clusters.
		je	end_cycle
		
		jmp	cycle
	end_cycle:

	jmp	exit

not_found:
	print_label	file_not_found
	jmp	exit

access_failed:
	print_label	cannot_access
	jmp	exit

zero_length:
	print_label	file_empty
	jmp	exit

exit:
	getch
	halt	0

;================================================================================
;  # make_name:	makes name and extension of the source file completing by
;		trailing zeros.
;
;  parameters:	< nothing >
;
;  return:	< nothing >
;--------------------------------------------------------------------------------
;  locals:	SI - index for source file.
;		DI - index for current file.
;================================================================================

make_name	Proc	Near

	push_ex	< SI, DI >

	; extracting name.
	mov	SI, 0
	mov	DI, 0
	mov	CX, 8
	cycle_name:
		cmp	source[ SI ], 0
		je	make_leave

		cmp	source[ SI ], '.'
		je	end_cycle_name

		mov	AL, source[ SI ]
		mov	src_name[ DI ], AL
		inc	SI
		inc	DI
		loop	cycle_name
	end_cycle_name:

	; add trailing spaces to name.
	cmp	CX, 0
	je	skip_fill_name
	fill_name:
		mov	src_name[ DI ], ' '
		inc	DI
		loop	fill_name
	end_fill_name:
skip_fill_name:

	; extracting extension.
	inc	SI
	mov	DI, 0
	mov	CX, 3
	cycle_ext:
		cmp	source[ SI ], 0
		je	make_leave

		mov	AL, source[ SI ]
		mov	src_ext[ DI ], AL
		inc	SI
		inc	DI
		loop	cycle_ext
	end_cycle_ext:

	; add trailing spaces to extension.
	cmp	CX, 0
	je	skip_fill_ext
	fill_ext:
		mov	src_ext[ DI ], ' '
		inc	DI
		loop	fill_ext
	end_fill_ext:
skip_fill_ext:

make_leave:
	pop_ex	< DI, SI >
	RetN

make_name	EndP


;================================================================================
;  # compare:	checks if current file is the source file.
;
;  parameters:	BUFF - offset in a buffer of current element.
;
;  return:	FOUND - 0 if not found, else 1.
;--------------------------------------------------------------------------------
;  locals:	SI - index for source file.
;		DI - index for current file.
;================================================================================

compare		Proc	Near

	push	BP
	mov	BP, SP

FOUND	equ	word ptr [ BP ][ 6 ]
BUFF	equ	word ptr [ BP ][ 4 ]

	push_ex	< SI, DI >
	pushf

	mov	DI, BUFF		; check full filename.
	lea	SI, src_full

	mov	CX, 8 + 3
	cld
	repe	cmpsb
	jne	cmp_false

cmp_true:
	mov	FOUND, 1
	jmp	cmp_leave
cmp_false:
	mov	FOUND, 0
	jmp	cmp_leave

cmp_leave:
	popf
	pop_ex	< DI, SI, BP >
	RetN

compare		EndP

CSeg	EndS


END	main