TITLE	lab4
SUBTTL	lab4_3
PAGE	25


SSeg	Segment		Stack 'STACK'
	db	100h dup( ? )
SSeg	EndS


DSegA	Segment		'DATA'

A	db	'A'

DSegA	EndS


CSeg	Segment		Public 'CODE'
	Assume	SS:SSeg, CS:CSeg

start:	Assume	DS:DSegA
	mov	AX, DSegA
	mov	DS, AX

	mov	AH, 02h
	mov	DL, A
	int	21h

	mov	AH, 07h
	int	21h

	Extrn 	print_b: Near
	call	print_b

	mov	AX, 4C00h
	int	21h

CSeg	EndS


DSegB	Segment		Public 'DATA'

Public	B
B	db	'B'

DSegB	EndS


END	start