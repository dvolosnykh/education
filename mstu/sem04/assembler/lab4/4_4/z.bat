tasm /l /ml /zi		%1%2.asm;
if errorlevel 1 (
	echo "Error(s) in module %2"
	goto end
)

tasm /l /ml /zi		%1%3.asm;
if errorlevel 1 (
	echo "Error(s) in module %3"
	goto end
)

tlink	%1%2.obj + %1%3.obj, %1.exe;
if errorlevel 1 (
	echo "Error(s) when linking"
	goto end
)

4_4.exe
if errorlevel 1 (
	echo "Non-zero exit code: " %errorlevel%
)

:end
