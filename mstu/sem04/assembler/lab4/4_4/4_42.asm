DSegB	Segment		Public 'DATA'

Extrn	B: Byte

DSegB	EndS


CSeg	Segment		Public 'CODE'
	Assume	CS:CSeg

print_b		Proc	Near
	Public	print_b

	Assume	DS:DSegB
	mov	AX, DSegB
	mov	DS, AX

	mov	AH, 02h
	mov	DL, B
	int	21h

	mov	AH, 07h
	int	21h

	RetN

print_b		EndP

CSeg	EndS

END