SSeg	Segment		Stack 'STACK'
	db	100h dup( ? )
SSeg	EndS


DSegA	Segment		'DATA'

A	db	'A'

DSegA	EndS


CSeg	Segment		'CODE'
	Assume	SS:SSeg, CS:CSeg

start:	Assume	DS:DSegA
	mov	AX, DSegA
	mov	DS, AX

	mov	AH, 02h
	mov	DL, A
	int	21h

	mov	AH, 07h
	int	21h

	Assume	DS:DSegB
	mov	AX, DSegB
	mov	DS, AX

	mov	AH, 02h
	mov	DL, B
	int	21h

	mov	AH, 07h
	int	21h

	mov	AX, 4C00h
	int	21h

CSeg	EndS


DSegB	Segment		'DATA'

B	db	'B'

DSegB	EndS


END	start