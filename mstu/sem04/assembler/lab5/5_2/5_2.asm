include	utils.asm


SSeg	Segment		Stack 'STACK'
	db	100h dup( ? )
SSeg	EndS


DSeg	Segment		'DATA'

a	db	'a-1+++2+1', '$'
b	db	'2+3+5+9+3*4/9+1:7', '$'
c	db	'1-1-1-1-1-1-1+', '$'
d	db	'52*46+7+43/3-5', '$'
e	db	'$'

eqn	equ	b

;************************************** DATA ************************************
BAD	equ	'b'
DIGIT	equ	'd'
OPER	equ	'o'
MINUS	equ	'-'
table_code	db	42 dup( BAD )
		db	2 dup( OPER )
		db	2 dup( BAD, OPER )
		db	10 dup( DIGIT )
		db	198 dup( BAD )

DSeg	EndS



;/******************************************************************************/
;/*                             MAIN CODE                                      */
;/******************************************************************************/



CSeg	Segment		'CODE'
	Assume	SS:SSeg, DS:DSeg, CS:CSeg, ES:DSeg

;================================================================================
;  # main:	main body.
;
;  parameters:	< nothing >
;
;  return:	AL - return-code:
;			0 - success.
;================================================================================

Extrn	unsigned_decimal:	Near

main	Proc	Near

	mov	AX, DSeg
	mov	DS, AX

	print_label	eqn		; print equation
	endl

	push_ex	< 0, offset eqn, seg eqn >
	call	check
	add	SP, 6

	add	AL, '0'
	putch	AL			; print return code

	cmp	AX, '0'
	je	skip
	endl

	push	SI			; print index of last analysed symbol
	call	unsigned_decimal
	add	SP, 2
skip:
	getch
	halt	0

main	EndP

;================================================================================
;  # check:	checks if equation is satisfying.
;
;  parameters:	EQN_SEGM - segment of the equation.
;		EQN_OFFS - offset of the equation.
;		START - index of starting symbol.
;
;  return:	AX - return-code:
;			0 - satisfying
;			1 - first symbol neither digit nor minus
;			2 - not satisfying
;		SI - index of stop-symbol.
;--------------------------------------------------------------------------------
;  locals:	AH - type of previos symbol.
;		AL - type of current symbol.
;		BX - offset of the equation.
;		SI - index of current symbol.
;================================================================================

check	Proc	Near

	push	BP
	mov	BP, SP

START		equ	word ptr [ BP ][ 8 ]
EQN_OFFS	equ	word ptr [ BP ][ 6 ]
EQN_SEGM	equ	word ptr [ BP ][ 4 ]

	push_ex	< ES, DS, BX >

	mov	SI, START
	mov	BX, EQN_OFFS
	mov	DS, EQN_SEGM

	mov	AX, seg table_code
	mov	ES, AX

	; initial state
	mov	AH, BAD

	; analyzing first symbol
	mov	AL, [ BX ][ SI ]
	cmp	AL, '$'
	je	code_2

	cmp	AL, MINUS	; if first symbol is MINUS
	jne	not_MINUS
	mov	AH, OPER	; setting type to OPER
	jmp	continue

not_MINUS:
	push	BX
	mov	BX, offset table_code
	xlat
	pop	BX

	cmp	AL, DIGIT	; if first symbol is DIGIT
	jne	code_1
	mov	AH, DIGIT	; setting type to DIGIT
continue:

cycle:
	inc	SI
	mov	AL, [ BX ][ SI ]
	cmp	AL, '$'
	je	endcycle

	push	BX
	mov	BX, offset table_code
	xlat
	pop	BX

	; switch ( AL )
	case_OPER:
		cmp	AL, OPER
		jne	case_DIGIT

		cmp	AH, DIGIT	; previous symbol must be DIGIT
		jne	code_2

		jmp	endswitch

	case_DIGIT:
		cmp	AL, DIGIT
		jne	case_BAD

		cmp	AH, BAD		; previous symbol must not be BAD
		je	code_2

		jmp	endswitch

	case_BAD:
		jmp	code_2
	endswitch:

	mov	AH, AL		; saving type of current symbol
	jmp	cycle
endcycle:

	cmp	AH, DIGIT
	jne	code_2

; setting return-code
code_0:	mov	AX, 0
	jmp	stop
code_1:	mov	AX, 1
	jmp	stop
code_2:	mov	AX, 2

stop:	pop_ex	< BX, DS, ES, BP >
	RetN

check	EndP


CSeg	EndS


END	main