include	utils.asm


SSeg	Segment		Stack 'STACK'
	db	100h dup( ? )
SSeg	EndS


DSeg	Segment		'DATA'

a	db	'~(((~(~A)!B)&Q))', '$'
b	db	'(A&B)&C)', '$'
c	db	'(A!B~)', '$'
d	db	'(A&(~C!~D)', '$'
e	db	'~A!B&(C!~D&(E!F))&G.', '$'
f	db	'((A))&B', '$'
g	db	'a)', '$'

eqn	equ	a

BAD	equ	'b'
LETTER	equ	'l'
INVERT	equ	'~'
BINARY	equ	'2'
OPEN	equ	'('
CLOSE	equ	')'
table_code	db	33 dup( BAD )
		db	BINARY
		db	4 dup( BAD )
		db	BINARY
		db	BAD
		db	OPEN, CLOSE
		db	17 dup( BAD )
		db	6 dup( BAD )
		db	26 dup( LETTER )
		db	35 dup( BAD )
		db	INVERT
		db	130 dup( BAD )

DSeg	EndS



;/******************************************************************************/
;/*                             MAIN CODE                                      */
;/******************************************************************************/



CSeg	Segment		'CODE'
	Assume	SS:SSeg, DS:DSeg, CS:CSeg

;================================================================================
;  # main:	main body.
;
;  parameters:	< nothing >
;
;  return:	AL - return-code:
;			0 - success.
;================================================================================

Extrn	unsigned_decimal:	Near

main	Proc	Near

	mov	AX, DSeg
	mov	DS, AX

	print_label	eqn	; print equation
	endl

	push_ex	< 0, 0, offset eqn, seg eqn >
	call	check
	add	SP, 4
	pop_ex	< SI, DX >

	add	AL, '0'
	putch	AL		; print return code

	cmp	AL, '1'
	je	skip
	endl

	push	SI		; print index of last analyzed symbol
	call	unsigned_decimal
	add	SP, 2
skip:
	getch
	halt	0

main	EndP

;================================================================================
;  # check:	checks if expression is satisfying.
;
;  parameters:	EQN_SEGM - segment of the equation.
;		EQN_OFFS - offset of the equation.
;		START - index of starting symbol.
;		DEPTH - type of previous symbol : depth of recursion.
;
;  return:	AX - return-code:
;			0 - not satisfying
;			1 - satisfying
;		START - index of stop-symbol.
;--------------------------------------------------------------------------------
;  locals:	AL - type of current symbol.
;		DS - segment of the equation.
;		BX - offset of the equation.
;		DL - depth of recursion.
;		DH - type of previos symbol.
;		SI - index of current symbol.
;================================================================================

check	Proc	Near

	push	BP
	mov	BP, SP

DEPTH		equ	word ptr [ BP ][ 10 ]
START		equ	word ptr [ BP ][  8 ]
EQN_OFFS	equ	word ptr [ BP ][  6 ]
EQN_SEGM	equ	word ptr [ BP ][  4 ]

	push_ex	< ES, DS, BX, DX >

	mov	DS, EQN_SEGM
	mov	BX, EQN_OFFS
	mov	SI, START
	mov	DX, DEPTH

	mov	AX, seg table_code
	mov	ES, AX

	cmp	DL, 0
	jne	skip_init
	mov	DH, BINARY
skip_init:

cycle:
	mov	AL, [ BX ][ SI ]
	cmp	AL, '$'
	jne	continue
	jmp	end_cycle
continue:
	push	BX
	mov	BX, offset table_code
	xlat
	pop	BX

	; switch ( AL )
	case_INVERT:
		cmp	AL, INVERT
		jne	case_LETTER

		cmp	DH, BINARY
		je	do_INVERT
		cmp	DH, OPEN
		je	do_INVERT
		jmp	code_0

		do_INVERT:
		inc	SI
		mov	DH, AL

		jmp	endswitch

	case_LETTER:
		cmp	AL, LETTER
		jne	case_OPEN

		cmp	DH, LETTER
		je	do_LETTER
		cmp	DH, INVERT
		je	do_LETTER
		cmp	DH, BINARY
		je	do_LETTER
		cmp	DH, OPEN
		je	do_LETTER
		jmp	code_0
		
		do_LETTER:
		inc	SI
		mov	DH, AL

		jmp	endswitch

	case_OPEN:
		cmp	AL, OPEN
		jne	case_CLOSE

		cmp	DH, INVERT
		je	do_OPEN
		cmp	DH, BINARY
		je	do_OPEN
		cmp	DH, OPEN
		je	do_OPEN
		jmp	code_0

		do_OPEN:
		inc	SI
		inc	DL
		mov	DH, AL

		push_ex < DX, SI, BX, DS >
		call	check
		add	SP, 4
		pop_ex	< SI, DX >

		cmp	AX, 1
		jne	code_0

		jmp	endswitch

	case_CLOSE:
		cmp	AL, CLOSE
		jne	case_BINARY

		cmp	DL, 0
		je	code_0
		cmp	DH, LETTER
		je	do_CLOSE
		cmp	DH, CLOSE
		je	do_CLOSE
		jmp	code_0

		do_CLOSE:
		inc	SI
		dec	DL
		mov	DH, AL

		jmp	code_1

	case_BINARY:
		cmp	AL, BINARY
		jne	case_BAD

		cmp	DH, LETTER
		je	do_BINARY
		cmp	DH, CLOSE
		je	do_BINARY
		jmp	code_0

		do_BINARY:
		inc	SI
		mov	DH, AL

		jmp	endswitch

	case_BAD:
		jmp	code_0
	endswitch:

	jmp	cycle
end_cycle:

	cmp	DL, 0
	jne	code_0
	cmp	DH, LETTER
	je	code_1
	cmp	DH, CLOSE
	je	code_1

; setting return-code
code_0:	mov	AX, 0
	jmp	stop
code_1:	mov	AX, 1

stop:	mov	START, SI
	mov	DEPTH, DX
	pop_ex	< DX, BX, DS, ES, BP >
	RetN

check	EndP

CSeg	EndS


END	main