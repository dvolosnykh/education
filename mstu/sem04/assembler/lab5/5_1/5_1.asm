include	utils.asm


SSeg	Segment		Stack 'STACK'
	db	100h dup( ? )
SSeg	EndS


DSeg	Segment		'DATA'

a	db	'asbdghdasdgdsagfdsaggjkdghkdhgjkasdgbjgjdglkdaglkdghacvasdgf', 10, 13, '$'
na	dw	60

b	db	'adsgghdasadgagafdsaggjkdghdfgsjkajjdgfdgjjklktvvmdsafghhsdgf', 10, 13, '$'
nb	dw	60

d	db	'098765432109876543210987654321098765432109876543210987654321', 10, 13, '$'
nd	dw	60

s	db	'54321', 10, 13, '$'
ns	dw	5

x	equ	d
n	equ	nd

DSeg	EndS



;/******************************************************************************/
;/*                             MAIN CODE                                      */
;/******************************************************************************/



CSeg	Segment		'CODE'
	Assume	SS:SSeg, DS:DSeg, CS:CSeg

;================================================================================
;  # main:	main body.
;
;  parameters:	< nothing >
;
;  return:	AL - return-code:
;			0 - success.
;================================================================================

main	Proc	Near

	mov	AX, DSeg
	mov	DS, AX

	print_label	x		; print initial array

	push_ex	< n, offset x, seg x >
	call	sort
	add	SP, 6

	print_label	x		; print sorted array

	getch
	halt	0

main	EndP

;================================================================================
;  # sort:	sorts byte array of specified length.
;		On every step, maximum element is swaped with the last unsorted,
;		thus decreasing amount of unsorted elements from the end.
;
;  parameters:	ARRAY_SEGM - segment of array.
;		ARRAY_OFFS - offset of array.
;		NUM - length of array.
;
;  return:	< nothing >
;--------------------------------------------------------------------------------
;  locals:	BX - offset of array.
;		CX - length of array.
;		DI - index of currently last unsorted element.
;		SI - index of maximum element among unsorted.
;================================================================================

sort	Proc	Near

	push	BP
	mov	BP, SP

NUM		equ	word ptr [ BP ][ 8 ]
ARRAY_OFFS	equ	word ptr [ BP ][ 6 ]
ARRAY_SEGM	equ	word ptr [ BP ][ 4 ]

	push_ex	< AX, BX, CX, DI, SI >

	mov	CX, NUM
	mov	BX, ARRAY_OFFS
	mov	DS, ARRAY_SEGM

	cmp	CX, 1
	jbe	stop

	mov	DI, CX
	
iter:	dec	DI

	push_ex	< -1, CX, BX, DS >
	call	findmax
	add	SP, 6
	pop	SI

	cmp	SI, DI		; check if there is a need in swaping
	je	skip

	mov	AH, [ BX + SI ]	; swap elements
	xchg	AH, [ BX + DI ]
	mov	[ BX + SI ], AH

skip:	loop	iter

stop:	pop_ex	< SI, DI, CX, BX, AX, BP >
	RetN

sort	EndP

;================================================================================
;  # findmax:	finds index of maximum element in byte array of specified length.
;
;  parameters:	ARRAY_SEGM - segment of array.
;		ARRAY_OFFS - offset of array.
;		NUM - length of array.
;
;  return:	MAX - index of resulting maximum element.
;--------------------------------------------------------------------------------
;  locals:	BX - offset of array.
;		CX - length of array.
;		DI - index of currently analyzed element.
;		SI - index of currently maximum element.
;================================================================================

findmax		Proc	Near

	push	BP
	mov	BP, SP

MAX		equ	word ptr [ BP ][ 10 ]
NUM		equ	word ptr [ BP ][  8 ]
ARRAY_OFFS	equ	word ptr [ BP ][  6 ]
ARRAY_SEGM	equ	word ptr [ BP ][  4 ]

	push_ex	< AX, BX, CX, DI, SI >

	mov	CX, NUM
	mov	BX, ARRAY_OFFS
	mov	DS, ARRAY_SEGM
	mov	SI, MAX

	mov	DI, CX
	dec	DI
	mov	SI, DI

	cmp	CX, 1
	je	stop2

	dec	CX

iter2:	dec	DI

	mov	AH, [ BX + SI ]	; check if there is a need in saving current index
	cmp	AH, [ BX + DI ]
	jnb	skip2

	mov	SI, DI		; save current index

skip2:	loop	iter2

stop2:	mov	MAX, SI
	pop_ex	< SI, DI, CX, BX, AX, BP >
	RetN

findmax		EndP


CSeg	EndS


END	main