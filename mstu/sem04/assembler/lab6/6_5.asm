include utils.asm


CSeg	Segment		'CODE'
	Assume	CS:CSeg

;================================================================================
;  # signed_decimal:	converts and outputs a word-container as a signed
;			decimal value.
;
;  parameters:	VALUE - value.
;
;  return:	< nothing >
;================================================================================

Extrn	unsigned_decimal:	Near

signed_decimal	Proc	Near
	Public	signed_decimal

	push	BP
	mov	BP, SP

VALUE	equ	word ptr [ BP ][ 4 ]

	push	DX

	mov	DX, VALUE

	cmp	DX, 0
	jge	skip
	neg	DX
	putch	'-'
skip:
	push	DX
	call	unsigned_decimal
	add	SP, 2

	pop_ex	< DX, BP >
	RetN

signed_decimal	EndP

CSeg	EndS


END