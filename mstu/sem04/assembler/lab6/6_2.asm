include utils.asm


SSeg	Segment		Stack 'STACK'
SSeg	EndS


DS_2	Segment		'DATA'

str	db	17 dup( ? )

DS_2	EndS



CSeg	Segment		'CODE'
	Assume	DS:DS_2, CS:CSeg, SS:SSeg

;================================================================================
;  # unsigned_binary:	converts and outputs a word-container as an unsigned
;			binary value.
;
;  parameters:	VALUE - value.
;
;  return:	< nothing >
;--------------------------------------------------------------------------------
;  locals:	CX - counter of quantity of unworked bits.
;		DX - value.
;		SI - index of current bit.
;================================================================================

unsigned_binary		Proc	Near
	Public	unsigned_binary

	push	BP
	mov	BP, SP

VALUE	equ	word ptr [ BP ][ 4 ]

	push_ex	< CX, DX, DS, SI >

	mov	DX, VALUE

	mov	CX, DS_2
	mov	DS, CX

	cmp	DX, 0		; *** ZERO ***
	jne	non_zero
	mov	str[ 0 ], '0'
	mov	str[ 1 ], '$'
	jmp	output

non_zero:			; *** NON-ZERO ***
	mov	CX, 16

	skip_0:
		shl	DX, 1		; skipping leading zeros
		jc	end_skip_0
	loop	skip_0
end_skip_0:

	mov	SI, 0
	cycle:	
		jnc	bit_0		; main cycle
		mov	str[ SI ], '1'
		jmp	continue
	bit_0:	mov	str[ SI ], '0'
	continue:
		inc	SI

	dec	CX		; break cycle
	jz	end_cycle

		shl	DX, 1
	jmp	cycle
end_cycle:

	mov	str[ SI ], '$'

output:
	print_label	str

	pop_ex	< SI, DS, DX, CX, BP >
	RetN

unsigned_binary		EndP

CSeg	EndS


END