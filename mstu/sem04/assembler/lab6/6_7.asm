include utils.asm


CSeg	Segment		'CODE'
	Assume	CS:CSeg

;================================================================================
;  # signed_hexadecimal:	converts and outputs a word-container as a signed
;				hexadecimal value.
;
;  parameters:	VALUE - value.
;
;  return:	< nothing >
;================================================================================

Extrn	unsigned_hexadecimal:	Near

signed_hexadecimal	Proc	Near
	Public	signed_hexadecimal

	push	BP
	mov	BP, SP

VALUE	equ	word ptr [ BP ][ 4 ]

	push	DX

	mov	DX, VALUE

	cmp	DX, 0
	jge	skip
	neg	DX
	putch	'-'
skip:
	push	DX
	call	unsigned_hexadecimal
	add	SP, 2

	pop_ex	< DX, BP >
	RetN

signed_hexadecimal	EndP

CSeg	EndS


END