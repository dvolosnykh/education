include utils.asm


SSeg	Segment		Stack 'STACK'
SSeg	EndS


MINUS	equ	'-'


CSeg	Segment		'CODE'
	Assume	CS:CSeg, SS:SSeg

;================================================================================
;  # input:	input of value.
;
;  parameters:	< nothing >
;
;  return:	VALUE - value.
;--------------------------------------------------------------------------------
;  locals:	AX ( AL ) - current digit.
;		BX ( BL ) - currently inputted digit.
;		DX - current value.
;		SI - sign of value.
;================================================================================

input	Proc	Near
	Public	input

	push	BP
	mov	BP, SP

VALUE	equ	word ptr [ BP ][ 4 ]

	push_ex	< AX, BX, DX, SI >

	mov	DX, VALUE

	mov	BH, 0

	getche		; work on first symbol
	mov	BL, AL

	cmp	BL, MINUS
	jne	not_MINUS
	mov	SI, 1
	mov	DX, 0
	jmp	cycle
not_MINUS:
	mov	SI, 0
	sub	BX, '0'
	mov	DX, BX

	cycle:
		getch		; main cycle
		cmp	AL, 13
		jne	continue
		endl
		endl
		jmp	end_cycle

	continue:
		mov	BL, AL
		cmp	BL, '0'
		jb	cycle
		cmp	BL, '9'
		ja	cycle
		putch	BL

		sub	BL, '0'
		mov	AX, DX
		mov	DX, 10
		mul	DX
		add	AX, BX
		mov	DX, AX
	jmp	cycle
end_cycle:

	cmp	SI, 0
	je	positive
	neg	DX		; make negative
positive:

	mov	VALUE, DX
	pop_ex	< SI, DX, BX, AX, BP >
	RetN

input	EndP

CSeg	EndS


END