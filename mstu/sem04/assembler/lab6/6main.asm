include utils.asm


SSeg	Segment		Stack 'STACK'
	db	100h dup( ? )
SSeg	EndS


DS_main		Segment		'DATA'

X	dw	?

invite	db	'Make a choice: ', '$'

func	dw	show_menu
	dw	input
	dw	unsigned_binary
	dw	  signed_binary
	dw	unsigned_decimal
	dw	  signed_decimal
	dw	unsigned_hexadecimal
	dw	  signed_hexadecimal

DS_main		EndS



CSeg	Segment		'CODE'
	Assume	SS:SSeg, DS:DS_main, CS:CSeg

;================================================================================
;  # main:	main body.
;
;  parameters:	< nothing >
;
;  return:	AL - return-code:
;			0 - success.
;================================================================================

Extrn	show_menu:		Near
Extrn	input:			Near
Extrn	unsigned_binary:	Near
Extrn	  signed_binary:	Near
Extrn	unsigned_decimal:	Near
Extrn	  signed_decimal:	Near
Extrn	unsigned_hexadecimal:	Near
Extrn	  signed_hexadecimal:	Near

main	Proc	Near

	mov	AX, DS_main
	mov	DS, AX

	cycle:	call	choice
		endl
		endl

		switch_SI:
		case_0:				; show menu
			cmp	SI, 0
			jne	case_1

			call	show_menu
			endl

			jmp	end_switch_SI
		case_1:				; input
			cmp	SI, 1
			jne	case_8

			push	X
			call	input
			pop	X

			jmp	end_switch_SI
		case_8:
			cmp	SI, 8
			jne	default

			halt	0

			jmp	end_switch_SI
		default:			; output
			shl	SI, 1

			push	X
			call	func[ SI ]
			add	SP, 2

			endl
			endl
		end_switch_SI:
	jmp	cycle

main	EndP

;================================================================================
;  # choice:	Asks user for a choice.
;
;  parameters:	< nothing >
;
;  return:	SI - number of choice.
;================================================================================

choice	Proc	Near

	push	AX

	print_label	invite

again:
	getch
	cmp	AL, '0'
	jb	again
	cmp	AL, '8'
	ja	again

	putch	AL

	mov	AH, 0
	mov	SI, AX
	sub	SI, '0'

stop:	pop	AX
	RetN

choice	EndP

CSeg	EndS


END	main