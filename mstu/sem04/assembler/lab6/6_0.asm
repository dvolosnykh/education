include utils.asm


SSeg	Segment		Stack 'STACK'
SSeg	EndS


DS_0	Segment		'DATA'

menu_txt	db	'0) Show menu.', 10, 13
		db	'1) Input integer into X:WORD.', 10, 13
		db	'2) Output X as unsigned binary integer.', 10, 13
		db	'3) Output X as   signed binary integer.', 10, 13
		db	'4) Output X as unsigned decimal integer.', 10, 13
		db	'5) Output X as   signed decimal integer.', 10, 13
		db	'6) Output X as unsigned hexadecimal integer.', 10, 13
		db	'7) Output X as   signed hesadecimal integer.', 10, 13
		db	'8) Quit.', 10, 13, '$'

DS_0	EndS



CSeg	Segment		'CODE'
	Assume	DS:DS_0, CS:CSeg, SS:SSeg

;================================================================================
;  # show_menu:	shows menu.
;
;  parameters:	< nothing >
;
;  return:	< nothing >
;================================================================================

show_menu	Proc	Near
	Public	show_menu

	push_ex	< AX, DS >

	mov	AX, DS_0
	mov	DS, AX

	print_label	menu_txt

	pop_ex	< DS, AX >
	RetN

show_menu	EndP

CSeg	EndS


END