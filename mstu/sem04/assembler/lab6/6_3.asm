include utils.asm


CSeg	Segment		'CODE'
	Assume	CS:CSeg

;================================================================================
;  # signed_binary:	converts and outputs a word-container as a signed
;			binary value.
;
;  parameters:	VALUE - value.
;
;  return:	< nothing >
;================================================================================

Extrn	unsigned_binary:	Near

signed_binary	Proc	Near
	Public	signed_binary

	push	BP
	mov	BP, SP

VALUE	equ	word ptr [ BP ][ 4 ]

	push	DX

	mov	DX, VALUE

	cmp	DX, 0
	jge	skip
	neg	DX
	putch	'-'
skip:
	push	DX
	call	unsigned_binary
	add	SP, 2

	pop_ex	< DX, BP >
	RetN

signed_binary	EndP

CSeg	EndS


END