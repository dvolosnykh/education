include utils.asm


SSeg	Segment		Stack 'STACK'
SSeg	EndS


DS_4	Segment		'DATA'

str	db	6 dup( ? )

DS_4	EndS



CSeg	Segment		'CODE'
	Assume	DS:DS_4, CS:CSeg, SS:SSeg

;================================================================================
;  # unsigned_decmial:	converts and outputs a word-container as an unsigned
;			decimal value.
;
;  parameters:	VALUE - value.
;
;  return:	< nothing >
;--------------------------------------------------------------------------------
;  locals:	AX - integer part.
;		DX - reminder.
;		DI - ( used: while reversing string ) index of mirror digit.
;		SI - index of current digit.
;================================================================================

unsigned_decimal	Proc	Near
	Public	unsigned_decimal

	push	BP
	mov	BP, SP

VALUE	equ	word ptr [ BP ][ 4 ]

	push_ex	< AX, DX, DS, DI, SI >

	mov	DX, VALUE

	mov	AX, DS_4
	mov	DS, AX

	cmp	DX, 0		; *** ZERO ***
	jne	non_zero
	mov	str[ 0 ], '0'
	mov	str[ 1 ], '$'
	jmp	output

non_zero:			; *** NON-ZERO ***
	mov	SI, 0
	mov	AX, DX
	cycle:				; main cycle
		cmp	AX, 0
		je	end_cycle

		divide	AX, 10
		add	DX, '0'
		mov	str[ SI ], DL
		inc	SI
	jmp	cycle
end_cycle:

	mov	str[ SI ], '$'

	mov	DI, 0
	reverse:			; reverse string
		dec	SI

	cmp	SI, DI
	jna	end_reverse

		mov	AH, str[ SI ]
		xchg	AH, str[ DI ]
		mov	str[ SI ], AH

		inc	DI
	jmp	reverse
end_reverse:

output:
	print_label	str

	pop_ex	< SI, DI, DS, DX, AX, BP >
	RetN

unsigned_decimal	EndP

CSeg	EndS


END