include utils.asm


SSeg	Segment		Stack 'STACK'
SSeg	EndS


DS_6	Segment		'DATA'

str		db	5 dup( ? )
table_digit	db	'0123456789ABCDEF'

DS_6	EndS



CSeg	Segment		'CODE'
	Assume	DS:DS_6, CS:CSeg, SS:SSeg

;================================================================================
;  # unsigned_hexadecmial:	converts and outputs a word-container as an
;				unsigned hexadecimal value.
;
;  parameters:	VALUE - value.
;
;  return:	< nothing >
;--------------------------------------------------------------------------------
;  locals:	AL - current hexadecimal digit.
;		CX - counter of quantity of unworked digits.
;		DX - value.
;		SI - index of current digit.
;================================================================================

unsigned_hexadecimal	Proc	Near
	Public	unsigned_hexadecimal

	push	BP
	mov	BP, SP

VALUE	equ	word ptr [ BP ][ 4 ]

	push_ex	< AX, CX, DX, DS, SI >

	mov	DX, VALUE

	mov	AX, DS_6
	mov	DS, AX
	mov	ES, AX

	cmp	DX, 0		; *** ZERO ***
	jne	non_zero
	mov	str[ 0 ], '0'
	mov	str[ 1 ], '$'
	jmp	output

non_zero:			; *** NON-ZERO ***
	mov	CX, 4
	skip_0:
		rol	DX, 4		; skipping leading zeros
		mov	AL, DL
		and	AL, 0Fh
		cmp	AL, 0
		jnz	end_skip_0
	loop	skip_0
end_skip_0:

	mov	SI, 0
	lea	BX, table_digit
	cycle:				; main cycle
		xlat
		mov	str[ SI ], AL
		inc	SI

	dec	CX
	jz	end_cycle

		rol	DX, 4
		mov	AL, DL
		and	AL, 0Fh
	jmp	cycle
end_cycle:

	mov	str[ SI ], '$'

output:
	print_label	str

	pop_ex	< SI, DS, DX, CX, AX, BP >
	RetN

unsigned_hexadecimal	EndP

CSeg	EndS


END