SSeg	Segment 	Stack
	db	100h dup( ? )
SSeg	EndS


DSeg	Segment		Common

A	Label	Byte
ORG	12

DSeg	EndS


CSeg	Segment
	Assume	CS:CSeg, DS:DSeg, SS:SSeg

M1:	mov	AX, DSeg
	mov	DS, AX

	mov	AH, 07h
	int	21h

	mov	A, AL

	Extrn	M2: Far
	jmp	M2

CSeg	EndS


END	M1