SSeg	Segment		Stack
	db	100h dup(?)
SSeg	EndS


DSeg	Segment

A1$	db	'Variable A1', 10, 13, '$'

DSeg	EndS


CSeg	Segment
	Assume	CS:CSeg, DS:DSeg, SS:SSeg

PP1	Proc	Far

	push	DS
	push	0

	mov	AX, DSeg
	mov	DS, AX
	
	mov	DX, offset A1$
	mov	AH, 09h
	int	21h

PP1	EndP

CSeg	EndS


END	PP1	