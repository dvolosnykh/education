SSeg	Segment
	db	100h dup(?)
SSeg	EndS


DSeg	Segment

A2$	db	'Variable A2', 10, 13, '$'

DSeg	EndS


CSeg	Segment		Byte
	Assume	DS:DSeg, CS:CSeg

PP2	Proc	Far

	mov	AX, DSeg
	mov	DS, AX

	mov	DX, offset A2$
	mov	AH, 09h
	int	21h

	mov	AH, 07h
	int	21h

	RetF

PP2	EndP

CSeg	EndS


END