DSeg	Segment

A1$	db	'Variable A1', 10, 13, '$'

DSeg	EndS


CSeg	Segment
	Assume	CS:CSeg, DS:DSeg

PP1	Proc	Near

	mov	AX, DSeg
	mov	DS, AX
	
	mov	DX, offset A1$
	mov	AH, 09h
	int	21h

PP1	EndP

CSeg	EndS


END	PP1	