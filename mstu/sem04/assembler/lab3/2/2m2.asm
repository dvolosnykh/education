CSeg	Segment		Byte
	Assume	DS:DSeg, CS:CSeg

PP2	Proc	Near

	mov	AX, DSeg
	mov	DS, AX
	
	mov	DX, offset A2$
	mov	AH, 09h
	int	21h

	mov	AH, 07h
	int	21h
	
	mov	AX, 4C00h
	int	21h

PP2	EndP

CSeg	EndS


DSeg	Segment

A2$	db	'Variable A2', 10, 13, '$'

DSeg	EndS


END