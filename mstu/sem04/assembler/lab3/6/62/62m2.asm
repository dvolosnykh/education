DSeg	Segment		Public

D2	db	'd'

DSeg	EndS


ESeg	Segment		Public

E2	db	'e'

ESeg	EndS


CSeg	Segment		Public
	Assume	CS:CSeg, DS:DSeg, ES:ESeg

PP2	Proc	Far
	Public	PP2

	push	DS
	push	0h

	Extrn	D1: Byte

	mov	AX, DSeg
	mov	DS, AX
	cmp	AX, Seg D1
	jne	E

	mov	DL, D1
	jmp	C

E:	mov	AX, ESeg
	mov	ES, AX
	mov	DL, E2

C:	mov	AH, 02h
	int	21h

	mov	AH, 07h
	int	21h

	RetF

PP2	EndP

CSeg	EndS


END