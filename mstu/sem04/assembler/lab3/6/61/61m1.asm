SSeg	Segment 	Stack
	db	100h dup( ? )
SSeg	EndS


DSeg	Segment

Public	D1
D1	db	'D'

DSeg	EndS


CSeg	Segment
	Assume	CS:CSeg, DS:DSeg, SS:SSeg

PP1	Proc	Far

	push	DS
	push	0h

	Extrn	PP2: Far
	call	PP2

	RetF

PP1	EndP

CSeg	EndS


END	PP1