Extrn	D1: Byte


DSeg	Segment

D2	db	'd'

DSeg	EndS


ESeg	Segment

E2	db	'e'

ESeg	EndS


CSeg	Segment
	Assume	CS:CSeg, DS:DSeg, ES:ESeg

PP2	Proc	Far
	Public	PP2

	push	DS
	push	0h

	mov	AX, ESeg
	mov	ES, AX

	mov	AX, DSeg
	mov	DS, AX
	cmp	AX, seg D1
	jne	E

	mov	DL, D1
	jmp	C

E:	mov	AX, ESeg
	mov	ES, AX
	mov	DL, E2

C:	mov	AH, 02h
	int	21h

	mov	AH, 07h
	int	21h

	RetF

PP2	EndP

CSeg	EndS


END