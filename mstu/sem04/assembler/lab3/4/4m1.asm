CSeg	Segment
	Assume	CS:CSeg, DS:DSeg

PP1	Proc	Far

	push	DS
	push	0h

	mov	AX, DSeg
	mov	DS, AX

	mov	DX, offset A1$
	mov	AH, 09h
	int	21h

	mov	DX, offset A2$

PP1	EndP

CSeg	EndS


DSeg	Segment

A1$	db	'Variable A1', 10, 13, '$'
A2$	db	'Variable A2', 10, 13, '$'

DSeg	EndS


END	PP1	