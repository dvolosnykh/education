include utils.asm


SSeg	Segment		Stack 'STACK'
	db	100h dup( ? )
SSeg	EndS


DSeg	Segment		'DATA'

a	db	'A'
b	db	'BC'

DSeg	EndS



;--------------------------------------------------------------------------------
;  @ PUTSTR:	outputs a bit-field of specified length.
;
;  parameters:	field - offset of the bit-field.
;		len - length of the bit-field.
;--------------------------------------------------------------------------------
;  locals:	AH - current bit of current byte as char.
;		CL - number of bits in the current byte to be worked.
;		AL - current byte.
;		SI - offset of current byte.
;--------------------------------------------------------------------------------

PUTSTR	Macro	field, len
	Local	cycle_byte, cycle_bit, continue, multiple_of_8, end_cycle_byte

	.errb	< field >
	.errb	< len >

	push	SI

	divide	len, 8
	mov	SI, AX
	mov	CX, DX

	jcxz	multiple_of_8		; bit-field IS NOT multiple of byte.
	mov	DL, field[ SI ]
	ror	DL, CL

	cycle_byte:				; cycle for bytes.
		cycle_bit:			; cycle for bits.
			shl	DL, 1
			lahf
			and	AH, 00000001b
			add	AH, '0'
			putch	AH
		loop	cycle_bit
multiple_of_8:
		dec	SI
		js	end_cycle_byte

		mov	CL, 8
		mov	DL, field[ SI ]
	jmp	cycle_byte
end_cycle_byte:

	pop	SI
EndM

;--------------------------------------------------------------------------------
;  @ SET1:	serves the Set1 function (sets a specified bit).
;
;  parameters:	pos - index of the bit that is to be set.
;		field - label of the bit-field.
;--------------------------------------------------------------------------------

SET1	Macro	pos, field

	.errb	< pos >
	.errb	< field >

	push_ex	< offset field, seg field, pos >
	call	Set1
	add	SP, 6
EndM

;--------------------------------------------------------------------------------
;  @ KONSTRUKTOR:	sets a list of specified bits.
;
;  parameters:	field - offset of the bit-field.
;		list - a list of indices of the bits that are to be set.
;--------------------------------------------------------------------------------

KONSTRUKTOR	Macro	field, list

	.errb	< field >

	irp	pos, < list >
	.errb	< pos >

	SET1	pos, field

	endm
EndM

;--------------------------------------------------------------------------------
;  @ SET0:	serves the Set0 function (clears a specified bit).
;
;  parameters:	pos - index of the bit that is to be set.
;		field - label of the bit-field.
;--------------------------------------------------------------------------------

SET0	Macro	pos, field

	.errb	< pos >
	.errb	< field >

	push_ex	< offset field, seg field, pos >
	call	Set0
	add	SP, 6
EndM

;--------------------------------------------------------------------------------
;  @ COUNT:	serves the Count function (counts quantity of setted bits
;		in a bit-field).
;
;  parameters:	field - label of the bit-field.
;		len - length of the bit-field.
;--------------------------------------------------------------------------------

COUNT	Macro	field, len

	.errb	< field >
	.errb	< len >

	push_ex	< len, offset field, seg field >
	call	Count
	add	SP, 6
EndM

;/******************************************************************************/
;/*                               MAIN CODE                                    */
;/******************************************************************************/


CSeg	Segment		'CODE'
	Assume	SS:SSeg, DS:DSeg, CS:CSeg

;================================================================================
;  # main:	main body.
;
;  parameters:	< nothing >
;
;  return:	AL - return-code:
;			0 - success.
;================================================================================

main:	mov	AX, DSeg
	mov	DS, AX

	PUTSTR	a, 7
	endl
	COUNT	a, 7
	endl

	KONSTRUKTOR	a, < 2, 4 >
	PUTSTR	a, 7
	endl
	COUNT	a, 7
	endl
	endl

	PUTSTR	b, 16
	endl
	COUNT	b, 16
	endl

	SET0	1, b
	PUTSTR	b, 16
	endl
	COUNT	b, 16
	endl

	getch
	halt	0

;================================================================================
;  # Set1:	sets a specified bit.
;
;  parameters:	POS - index of the bit that is to be set.
;		FIELD_SEGM - segment of the bit-field.
;		FIELD_OFFS - offset of the bit-field.
;
;  returns:	< nothing >
;--------------------------------------------------------------------------------
;  locals:	AL - index of bit in the byte, where it is located.
;		DL - byte, where bit is located.
;		SI - index of byte, where bit is located.
;================================================================================

Set1	Proc	Near

	push	BP
	mov	BP, SP

FIELD_OFFS	equ	word ptr [ BP ][ 8 ]
FIELD_SEGM	equ	word ptr [ BP ][ 6 ]
POS		equ	word ptr [ BP ][ 4 ]

	push_ex < DS, SI >

	mov	DS, FIELD_SEGM
	mov	BX, FIELD_OFFS

	divide	POS, 8
	mov	SI, AX
	mov	AX, DX

	set_bit	< byte ptr [ BX + SI ] >, AL

	pop_ex < SI, DS, BP >
	RetN

Set1	EndP

;================================================================================
;  # Set0:	clears a specified bit.
;
;  parameters:	POS - index of the bit that is to be cleared.
;		FIELD_SEGM - segment of the bit-field.
;		FIELD_OFFS - offset of the bit-field.
;
;  returns:	< nothing >
;--------------------------------------------------------------------------------
;  locals:	AL - index of bit in the byte, where it is located.
;		DL - byte, where bit is located.
;		SI - index of byte, where bit is located.
;================================================================================

Set0	Proc	Near

	push	BP
	mov	BP, SP

FIELD_OFFS	equ	word ptr [ BP ][ 8 ]
FIELD_SEGM	equ	word ptr [ BP ][ 6 ]
POS		equ	word ptr [ BP ][ 4 ]

	push_ex < DS, SI >

	mov	DS, FIELD_SEGM
	mov	BX, FIELD_OFFS

	divide	POS, 8
	mov	SI, AX
	mov	AX, DX

	clr_bit	< byte ptr [ BX + SI ] >, AL

	pop_ex < SI, DS, BP >
	RetN

Set0	EndP

;================================================================================
;  # Count:	counts quantity of setted bits in a bit-field.
;
;  parameters:	FIELD_SEGM - segment of the bit-field.
;		FIELD_OFFS - offset of the bit-field.
;		LEN - length of the bit-field.
;
;  return:	< nothing >
;--------------------------------------------------------------------------------
;  locals:	AH - counter of 1's.
;		CL - number of bits in the current byte to be worked.
;		DL - current byte.
;		SI - index of current byte.
;================================================================================

Count	Proc	Near

	push	BP
	mov	BP, SP

LEN		equ	word ptr [ BP ][ 8 ]
FIELD_OFFS	equ	word ptr [ BP ][ 6 ]
FIELD_SEGM	equ	word ptr [ BP ][ 4 ]

	push_ex	< DS, SI >

	mov	DS, FIELD_SEGM
	mov	BX, FIELD_OFFS

	divide	LEN, 8
	mov	SI, AX
	mov	CX, DX

	jcxz	multiple_of_8
	mov	DL, [ BX + SI ]		; bit-field IS NOT multiple of byte
	ror	DL, CL

	mov	AH, 0
	cycle_byte:				; cycle for bytes
		cycle_bit:			; cycle for bits
			shl	DL, 1

			jnc	skip_count
			inc	AH
		skip_count:

		loop	cycle_bit

multiple_of_8:
		dec	SI
		js	end_cycle_byte

		mov	CL, 8
		mov	DL, [ BX + SI ]
	jmp	cycle_byte
end_cycle_byte:

	add	AH, '0'
	putch	AH

	pop_ex < SI, DS, BP >
	RetN

Count	EndP

CSeg	EndS


END	main